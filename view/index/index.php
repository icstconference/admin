<?php 
    $actual_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER[REQUEST_URI];
    function cut_string($str, $len, $more) {
        if ($str == "" || $str == NULL)
            return $str;
        if (is_array($str))
            return $str;
        $str = trim($str);
        if (strlen($str) <= $len)
            return $str;
        $str = substr($str, 0, $len);
        if ($str != "") {
            if (!substr_count($str, " ")) {
                if ($more)
                    $str .= " ...";
                return $str;
            }
            while (strlen($str) && ($str[strlen($str) - 1] != " ")) {
                $str = substr($str, 0, -1);
            }
            $str = substr($str, 0, -1);
            if ($more)
                $str .= " ...";
        }
        return $str;
    }

    $configurl = $this->configurl;
?>
<section id="page-top">
        <div id="main-slide1" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="item active">
                    <img class="img-responsive" src="<?php echo URL;?>public/img/slider1.png" alt="slider">
                    <div class="slider-content">
                        <div class="col-md-12 text-center">
                            <h1  style="margin-bottom:25px;font-size:50px;color:#fff;" id="sliderlon">
                                <div style="line-height: 80px;font-weight: 300;font-size: 36px;">
                                    November 28-30 2016<br/>
                                    <div style="font-weight:400;">
                                    The Third  International Conference on<br/>
                                    Computational Science and Engineering (ICCSE-3)
                                    </div>
                                </div>
                            </h1>
                            <a class="slider btn buttonblue btn-min-block" href="#">REGISTER NOW</a>
                             <a class="slider btn buttonblue btn-min-block" href="#">SIGN IN</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<section id="feature" class="feature-section">
        <div class="container" style="padding:0;">
            <div class="col-sm-6 col-md-6 col-xs-12">
                <a href="<?php echo URL;?>iccse-history-page-4.html">
                    <h3 class="conference">
                        About ICCSE
                    </h3>
                </a>
                <div class="line"></div>
                <div class="row textunderline" style="margin: 0 0 10px;">
                    The International Conference on Computational Science and Engineering (ICCSE) is organized biyearly by the Institute for Computational Science and Technology (ICST).<br/>
                    ICCSE aims to discuss research achievements in the field of computational science and technology, foster application of those achievements and in other areas, and initiate training and research cooperation among national and international academies.<br/>
                    The third International Conference on Computational Science and Engineering (ICCSE-3) will cover following areas, but not limited to, Computational Biophysics and Medicine, Computational Chemistry, Applied Mathematics, and Environmental Computational Sciences.
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-xs-12">
                <h3 class="conference">
                    highlight
                </h3>
                <div class="line"></div>
                <div class="row textunderline" style="margin: 0 0 10px;display:none;">
                    <center>
                        <img src="<?php echo URL;?>public/img/about.png" class="img-responsive" />
                    </center>
                </div>
                <div class="row textunderline" style="margin: 0 0 10px;">
                    <ul class="listhighlight">
                        <li>
                            <i class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="#">You've inspired new consumer, racked up click-thru's, blown-up brand enes.</a>
                        </li>
                        <li>
                            <i class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="#">You've inspired new consumer, racked up click-thru's, blown-up brand enes.</a>
                        </li>
                        <li>
                            <i class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="#">You've inspired new consumer, racked up click-thru's, blown-up brand enes.</a>
                        </li>
                        <li>
                            <i class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="#">You've inspired new consumer, racked up click-thru's, blown-up brand enes.</a>
                        </li>
                        <li>
                            <i class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="#">You've inspired new consumer, racked up click-thru's, blown-up brand enes.</a>
                        </li>
                        <li>
                            <i class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="#">You've inspired new consumer, racked up click-thru's, blown-up brand enes.</a>
                        </li>
                        <li>
                            <i class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="#">You've inspired new consumer, racked up click-thru's, blown-up brand enes.</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section id="deadline" class="latest-news-section" style="background:#f1f4f4;padding: 30px 0;" >
        <div class="container" style="padding:0;">
            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom:10px;">
                <h3 class="conference">
                    Important dates
                </h3>
                <div class="line"></div>
            </div>
            <div class="row" style="margin:0;">
                <div class="latest-news" id="partner3">
                    <div class="col-md-12 col-sm-12 col-xs-12 deadline">
                        <h3 class="deadlinetext">October 10, 2016</h3>
                        <div class="deadlinecontainlink">
                            <a href="#" class="deadlinelink">
                                Submission deadline
                            </a>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 deadline">
                        <h3 class="deadlinetext">October 25, 2016</h3>
                        <div class="deadlinecontainlink">
                            <a href="#" class="deadlinelink">
                                Acceptance notification
                            </a>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 deadline">
                        <h3 class="deadlinetext">November 8, 2016</h3>
                        <div class="deadlinecontainlink">
                            <a href="#" class="deadlinelink">
                                Registration deadline
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="invitetalk" class="feature-section">
        <div class="container" style="padding:0;">
            <div class="row" style="margin:0 0 10px;">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h3 class="conference">
                        invited talks
                    </h3>
                    <div class="line"></div>
                </div>
            </div>
            <div class="row" style="margin:0;">
                <div class="col-md-4 col-sm-4 col-xs-12" style="font-weight: 300;font-size:16px;line-height:24px;color:#888;">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.lobortis nisl ut aliquip ex ea commodo consequat aliquip ex ea commodo consequat.
                    <br/>
                    <a class="slider btn buttonblue1 btn-min-block" href="#">View all speakers</a>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12" style="font-weight: 300;">
                    <center>
                        <img src="<?php echo URL;?>public/img/invitedtalks.png" class="img-responsive"/>
                    </center>
                    <div class="row" style="margin:10px 0;">
                        <h3>Prof. Dave Thirumalai</h3>
                        <p style="color: #888;font-weight: 400;">
                            Robert A. Welch Chair in Chemistry Department of Chemistry, University of Texas at Austin, USA
                        </p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12" style="font-weight: 300;">
                    <center>
                        <img src="<?php echo URL;?>public/img/invitedtalks.png" class="img-responsive"/>
                    </center>
                    <div class="row" style="margin:10px 0;">
                        <h3>Prof. Dave Thirumalai</h3>
                        <p style="color: #888;font-weight: 400;">
                            Robert A. Welch Chair in Chemistry Department of Chemistry, University of Texas at Austin, USA
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="schedule" class="latest-news-section" style="background:#f1f4f4;padding: 30px 0;" >
        <div class="container" style="padding:0;">
            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom:10px;">
                <h3 class="conference">
                    PROGRAMME
                </h3>
                <div class="line"></div>
            </div>
            <div class="row" style="margin:0;">
                <div class="wpb_tabs wpb_content_element widget" data-interval="0">
                    <div class="wpb_wrapper wpb_tour_tabs_wrapper tz_meetup_wpb_tour_tabs ui-tabs vc_clearfix">
                        <div class="tz_tabs_meetup">
                            <ul class="wpb_tabs_nav ui-tabs-nav vc_clearfix tz_meetup_tabs text-center">
                                <li><a href="#tab1">day 1 - november 28</a></li>
                                <li><a href="#tab2">day 2 - november 29</a></li>
                                <li><a href="#tab3">day 3 - november 30</a></li>
                            </ul>
                        </div>
                        <div id="tab1" class="wpb_tab ui-tabs-panel wpb_ui-tabs-hide vc_clearfix">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="tz_event_meetup">
                                    <div class="tz_box_event_meetup">
                                        <h3>
                                            day 1       
                                        </h3>
                                        <h3 class="tz_event_meetup_subtitle">
                                            <em>november 28, 2016</em>        
                                        </h3>
                                        <div class="tz_event_meettup_box_content">
                                            <div class="tz_event_meetup_content">
                                                <div class="tz_meetup_box_detail">
                                                    <div class="tz_meetup_box_detail_custom">
                                                        <span class="tz_meetup_start_time">
                                                            9:00AM        
                                                        </span>
                                                        <h4>
                                                            Opening and two plenary talks.    
                                                        </h4>
                                                        <p class="tz_event_time">
                                                            <i class="fa fa-clock-o"></i>
                                                            9:00AM -
                                                            12:00AM
                                                            <p>
                                                            Opening and two plenary talks PANORAMA room (11th floor), EdenStar Saigon Hotel.
                                                            </p>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="tz_meetup_box_detail">
                                                    <div class="tz_meetup_box_detail_custom">
                                                        <span class="tz_meetup_start_time">
                                                            12:00AM        
                                                        </span>
                                                        <h4>
                                                            Lunch break     
                                                        </h4>
                                                        <p class="tz_event_time">
                                                            <i class="fa fa-clock-o"></i>
                                                            12:00AM -
                                                            14:00PM
                                                            <p>
                                                            MANGO restaurant (M floor), EdenStar Saigon Hotel
                                                            </p>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="tz_meetup_box_detail">
                                                    <div class="tz_meetup_box_detail_custom">
                                                        <span class="tz_meetup_start_time">
                                                            14:00PM        
                                                        </span>
                                                        <h4>
                                                            Two parallel tracks      
                                                        </h4>
                                                        <p class="tz_event_time">
                                                            <i class="fa fa-clock-o"></i>
                                                            14:00PM -
                                                            16:00PM
                                                            <p>
                                                            ● Computational Biophysics, Medicine and Chemistry Track PANAROMA room (11th floor), EdenStar Saigon Hotel<br/>
                                                            ● Applied Mathematics and Computational Environmental Science Track HARMONY room (M floor), Harmony Saigon Hotel 
                                                            </p>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="tz_meetup_box_detail">
                                                    <div class="tz_meetup_box_detail_custom">
                                                        <span class="tz_meetup_start_time">
                                                            16:00PM        
                                                        </span>
                                                        <h4>
                                                            Poster section    
                                                        </h4>
                                                        <p class="tz_event_time">
                                                            <i class="fa fa-clock-o"></i>
                                                            16:00PM -
                                                            17:30PM
                                                            <p>Poster section</p>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="tz_meetup_box_detail">
                                                    <div class="tz_meetup_box_detail_custom">
                                                        <span class="tz_meetup_start_time">
                                                            18:30PM        
                                                        </span>
                                                        <h4>
                                                            Banquet    
                                                        </h4>
                                                        <p class="tz_event_time">
                                                            <i class="fa fa-clock-o"></i>
                                                            18:30PM -
                                                            22:00PM
                                                            <p>SKY POOL room (12th floor), EdenStar Saigon Hotel</p>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab2" class="wpb_tab ui-tabs-panel wpb_ui-tabs-hide vc_clearfix">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="tz_event_meetup">
                                    <div class="tz_box_event_meetup">
                                        <h3>
                                            day 2       
                                        </h3>
                                        <h3 class="tz_event_meetup_subtitle">
                                            <em>november 29, 2016</em>        
                                        </h3>
                                        <div class="tz_event_meettup_box_content">
                                            <div class="tz_event_meetup_content">
                                                <div class="tz_meetup_box_detail">
                                                    <div class="tz_meetup_box_detail_custom">
                                                        <span class="tz_meetup_start_time">
                                                            9:00AM        
                                                        </span>
                                                        <h4>
                                                            Two parallel tracks     
                                                        </h4>
                                                        <p class="tz_event_time">
                                                            <i class="fa fa-clock-o"></i>
                                                            9:00AM -
                                                            12:00AM
                                                            <p>
                                                            ● Computational Biophysics, Medicine and Chemistry Track PANAROMA room (11th floor), EdenStar Saigon Hotel<br/>
                                                            ● Applied Mathematics and Computational Environmental Science Track HARMONY room (M floor), Harmony Saigon Hotel 
                                                            </p>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="tz_meetup_box_detail">
                                                    <div class="tz_meetup_box_detail_custom">
                                                        <span class="tz_meetup_start_time">
                                                            12:00AM        
                                                        </span>
                                                        <h4>
                                                            Lunch break     
                                                        </h4>
                                                        <p class="tz_event_time">
                                                            <i class="fa fa-clock-o"></i>
                                                            12:00AM -
                                                            14:00PM
                                                            <p>
                                                            MANGO restaurant (M floor), EdenStar Saigon Hotel
                                                            </p>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="tz_meetup_box_detail">
                                                    <div class="tz_meetup_box_detail_custom">
                                                        <span class="tz_meetup_start_time">
                                                            14:00PM        
                                                        </span>
                                                        <h4>
                                                            Two parallel tracks      
                                                        </h4>
                                                        <p class="tz_event_time">
                                                            <i class="fa fa-clock-o"></i>
                                                            14:00PM -
                                                            17:00PM
                                                            <p>
                                                            ● Computational Biophysics, Medicine and Chemistry Track PANAROMA room (11th floor), EdenStar Saigon Hotel<br/>
                                                            ● Applied Mathematics and Computational Environmental Science Track HARMONY room (M floor), Harmony Saigon Hotel 
                                                            </p>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="tz_meetup_box_detail">
                                                    <div class="tz_meetup_box_detail_custom">
                                                        <span class="tz_meetup_start_time">
                                                            17:00PM        
                                                        </span>
                                                        <h4>
                                                            Closing    
                                                        </h4>
                                                        <p class="tz_event_time">
                                                            <i class="fa fa-clock-o"></i>
                                                            17:00PM -
                                                            17:30PM
                                                            <p>PANAROMA room (11th floor), EdenStar Saigon Hotel</p>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab3" class="wpb_tab ui-tabs-panel wpb_ui-tabs-hide vc_clearfix">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="tz_event_meetup">
                                    <div class="tz_box_event_meetup">
                                        <h3>
                                            day 3       
                                        </h3>
                                        <h3 class="tz_event_meetup_subtitle">
                                            <em>november 30, 2016</em>        
                                        </h3>
                                        <div class="tz_event_meettup_box_content">
                                            <div class="tz_event_meetup_content">
                                                <div class="tz_meetup_box_detail">
                                                    <div class="tz_meetup_box_detail_custom">
                                                        <span class="tz_meetup_start_time">
                                                            9:00AM        
                                                        </span>
                                                        <h4>
                                                            Trip to Mekong delta.      
                                                        </h4>
                                                        <p class="tz_event_time">
                                                            <i class="fa fa-clock-o"></i>
                                                            9:00AM -
                                                            17:00AM
                                                            <p>Trip to Mekong delta. (to be updated)</p>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="organizer" class="feature-section">
        <div class="container" style="padding:0;">
            <div class="row" style="margin:0 0 10px;">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h3 class="conference">
                        organizers
                    </h3>
                    <div class="line"></div>
                </div>
            </div>
            <div class="row" style="margin:0;">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="row textunderline" style="margin: 0 0 10px;">
                        <center>
                            <img src="<?php echo URL;?>public/img/about.png" class="img-responsive" />
                        </center>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12" style="font-weight: 300;font-size:16px;line-height:24px;color:#888;">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo.
                </div>
            </div>
            <div class="row" style="margin: 10px 0 0;">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a class="faq-toggle collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne"> I. Organising Committee </a>
                                </h4>
                            </div>

                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    1. Prof. Nguyen Ky Phung, Honorary Chair<br/>
                                    Department of Science and Technology, Ho Chi Minh City, Vietnam.<br/>
                                    <br/>
                                    2. Prof. Mai Suan Li, Chair.<br/>
                                    Institute of Physics, Polish Academy of Sciences, Warsaw, Poland<br/>
                                    <br/>
                                    3. Prof. Truong Nguyen Thanh.<br/>
                                    Department of Chemistry, University of Utah, USA.<br/>
                                    <br/>
                                    4. Prof. Nguyen Minh Tho<br/>
                                    Department of Chemistry, University of Leuven, Leuven, Belgium<br/>
                                    <br/>
                                    5. Prof. Nguyen Van Thinh<br/>
                                    Department of Civil and Environmental Engineering, Seoul National University, Seoul, Korea.<br/><br/>
                                    6. Prof. Nguyen Van Hien.<br/>
                                    Department of Mathematics, University of Namur, Namur, Belgium.<br/>
                                    <br/>
                                    7. Trinh Xuan Hoang<br/>
                                    Institute of Physics, VAST, Hanoi, Vietnam<br/>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="faq-toggle collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">II. Scientific Committee</a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    1. Prof. Mai Suan Li,  Chair of the Scientific Committee, also Co-chair of Computational Chemistry, Biophysics, and Medicine Track<br/>
                                    Institute of Physics, Polish Academy of Science, Warsaw, Poland.<br/>
                                    <br/>
                                    2. Prof. Nguyen Minh Tho, Co-Chair,  Computational Chemistry, Biophysics and Medicine Track<br/>
                                    Department of Chemistry, University of Leuven, Leuven, Belgium.<br/>
                                    <br/>
                                    3. Prof. Nguyen Van Thinh, Co-Chair, Applied Mathematics and Computational Environmental Science Track<br/>
                                    Department of Civil and Environmental Engineering, Seoul National University, Seoul, Korea<br/><br/>
                                    4. Prof. Nguyen Van Hien, Co-Chair, Applied Mathematics and Computational Environmental Science Track<br/>
                                    Department of Mathematics, University of Namur, Namur, Belgium<br/>
                                    <br/>
                                    5. Prof. Truong Nguyen Thanh<br/>
                                    Department of Chemistry, University of Utah, USA<br/>
                                    <br/>
                                    6. Prof. Tran Cong Thanh<br/>
                                    University of Southern Queensland, Toowoomba, QLD, Australia<br/>
                                    <br/>
                                    7.  Prof. Edward P. O’Brien<br/>
                                    Department of Chemistry, Pennsylvania State University, USA<br/>
                                </div>
                            </div>
                        </div>
  
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="faq-toggle collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> III. Local organising committee</a>
                                </h4>
                            </div>

                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    1. Dr. Doan Xuan Huy Minh<br/>
                                    Institute for Computational Science and Technology<br/>
                                    <br/>
                                    2. Prof. Huynh Kim Lam<br/>
                                    International University, VNU – HCMC<br/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="payment" class="latest-news-section" style="background:url('<?php echo URL;?>public/img/cart.png') no-repeat center;background-size:cover;min-height:480px;padding: 30px 0;" >
        <div class="container" style="padding:0;">
            <div class="row" style="margin:0;">
                <div class="col-md-12 col-sm-12 col-xs-12" >
                    <div class="col-sm-6 col-md-6 col-xs-12" id="packetleft">
                        <div class="row" style="margin:40px 0 20px;background: #212430;">
                            <div class="col-sm-8 col-md-8 col-xs-8" style="padding:20px;min-height:242px;">
                                <h3 style="color:#fff;">PREMIUM</h3>
                                <p style="color:#fff;font-size: 16px;line-height: 24px;">
                                    ● Access to conference<br/>
                                    ● 4 nights Premium accommodation<br/>
                                    ● Gala dinner<br/>
                                    ● Mekong Delta trip<br/>
                                    ● Airport pickup and drop-off
                                </p>
                            </div>
                            <div class="col-sm-4 col-md-4 col-xs-4" style="padding:0;min-height:242px;background:#2969b0;">
                                <div style="text-align:center;margin-top: 32px;">
                                    <br/><br/>
                                    <b style="color:#fff;font-size:60px;">120$</b><br/>
                                    <a href="#" class="addtocart">Buy Ticket</a>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin:0;background: #212430;">
                            <div class="col-sm-8 col-md-8 col-xs-8" style="padding:20px;min-height:242px;">
                                <h3 style="color:#fff;">STANDARD</h3>
                                <p style="color:#fff;font-size: 16px;line-height: 24px;">
                                    ● Access to conference<br/>
                                    ● 4 nights Premium accommodation<br/>
                                    ● Gala dinner<br/>
                                    ● Mekong Delta trip
                                </p>
                            </div>
                            <div class="col-sm-4 col-md-4 col-xs-4" style="padding:0;min-height:242px;background:#2969b0;">
                                <div style="text-align:center;margin-top: 32px;">
                                    <br/><br/>
                                    <b style="color:#fff;font-size:60px;">100$</b><br/>
                                    <a href="#" class="addtocart">Buy Ticket</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-xs-12" id="packetright">
                        <div class="row" style="margin:40px 0 20px;background: #212430;">
                            <div class="col-sm-8 col-md-8 col-xs-8" style="padding:20px;min-height:242p;">
                                <h3 style="color:#fff;">BASIC</h3>
                                <p style="color:#fff;font-size: 16px;line-height: 24px;">
                                    ● Access to conference<br/>
                                    ● (Other options) (Add Hyperlink to Ticketbox)
                                </p>
                            </div>
                            <div class="col-sm-4 col-md-4 col-xs-4" style="padding:0;min-height:242px;background:#2969b0;">
                                <div style="text-align:center;margin-top: 32px;">
                                    <br/><br/>
                                    <b style="color:#fff;font-size:60px;">40$+</b><br/>
                                    <a href="#" class="addtocart">Buy Ticket</a>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin:0;background: #212430;">
                            <div class="col-sm-8 col-md-8 col-xs-8" style="padding:20px;min-height:242px;">
                                <h3 style="color:#fff;">STUDENTS</h3>
                                <p style="color:#fff;font-size: 16px;line-height: 24px;">
                                    * Participants must be students at the time of booking this package. We have the right to refuse your participation and you will receive no refund if the requirement is not met.<br/>
                                    ● Access to conference<br/>
                                    ● (Other options) (Add Hyperlink to Ticketbox)
                                </p>
                            </div>
                            <div class="col-sm-4 col-md-4 col-xs-4" style="padding:0;min-height:242px;background:#2969b0;">
                                <div style="text-align:center;margin-top: 32px;">
                                    <br/><br/>
                                    <b style="color:#fff;font-size:60px;">20$+</b><br/>
                                    <a href="#" class="addtocart">Buy Ticket</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="sponsor" class="latest-news-section" style="background-size:cover;min-height:480px;padding: 50px 0;" >
        <div class="container">
            <div class="row" style="margin:20px 0;text-align:center;">
                <h3 class="conference">
                    Hosts
                </h3>
                <center>
                    <img src="<?php echo URL;?>public/img/line.png" class="img-responsive" />
                </center>
            </div>
            <div class="row" style="margin:20px 0;">
                <div class="latest-news1">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="frame">
                            <center>
                                <img src="http://gogodev.me/public/img/tomorrow.png" class="img-responsive" />
                            </center>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="frame">
                            <center>
                                <img src="http://gogodev.me/public/img/cocacola.png" class="img-responsive" />
                            </center>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="frame">
                            <center>
                                <img src="http://gogodev.me/public/img/sony.png" class="img-responsive" />
                            </center>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="frame">
                            <center>
                                <img src="http://gogodev.me/public/img/deadmau5.png" class="img-responsive" />
                            </center>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="frame">
                            <center>
                                <img src="http://gogodev.me/public/img/tiesto.png" class="img-responsive" />
                            </center>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="frame">
                            <center>
                                <img src="http://gogodev.me/public/img/redbull.png" class="img-responsive" />
                            </center>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin:20px 0;text-align:center;">
                <h3 class="conference">
                    DIAMOND SPONSOR
                </h3>
                <center>
                    <img src="<?php echo URL;?>public/img/line.png" class="img-responsive" />
                </center>
            </div>
            <div class="row" style="margin:20px 0;">
                <div class="latest-news1">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="frame">
                            <center>
                                <img src="http://gogodev.me/public/img/tomorrow.png" class="img-responsive" />
                            </center>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="frame">
                            <center>
                                <img src="http://gogodev.me/public/img/cocacola.png" class="img-responsive" />
                            </center>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="frame">
                            <center>
                                <img src="http://gogodev.me/public/img/sony.png" class="img-responsive" />
                            </center>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="frame">
                            <center>
                                <img src="http://gogodev.me/public/img/deadmau5.png" class="img-responsive" />
                            </center>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="frame">
                            <center>
                                <img src="http://gogodev.me/public/img/tiesto.png" class="img-responsive" />
                            </center>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="frame">
                            <center>
                                <img src="http://gogodev.me/public/img/redbull.png" class="img-responsive" />
                            </center>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin:20px 0;text-align:center;">
                <h3 class="conference">
                    GOLD SPONSOR
                </h3>
                <center>
                    <img src="<?php echo URL;?>public/img/line.png" class="img-responsive" />
                </center>
            </div>
            <div class="row" style="margin:20px 0;">
                <div class="latest-news2">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="frame">
                            <center>
                                <img src="http://gogodev.me/public/img/tomorrow.png" class="img-responsive" />
                            </center>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="frame">
                            <center>
                                <img src="http://gogodev.me/public/img/cocacola.png" class="img-responsive" />
                            </center>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="frame">
                            <center>
                                <img src="http://gogodev.me/public/img/sony.png" class="img-responsive" />
                            </center>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="frame">
                            <center>
                                <img src="http://gogodev.me/public/img/deadmau5.png" class="img-responsive" />
                            </center>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="frame">
                            <center>
                                <img src="http://gogodev.me/public/img/tiesto.png" class="img-responsive" />
                            </center>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="frame">
                            <center>
                                <img src="http://gogodev.me/public/img/redbull.png" class="img-responsive" />
                            </center>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin:20px 0;text-align:center;">
                <h3 class="conference">
                    SILVER SPONSOR
                </h3>
                <center>
                    <img src="<?php echo URL;?>public/img/line.png" class="img-responsive" />
                </center>
            </div>
            <div class="row" style="margin:20px 0;">
                <div class="latest-news3">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="frame">
                            <center>
                                <img src="http://gogodev.me/public/img/tomorrow.png" class="img-responsive" />
                            </center>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="frame">
                            <center>
                                <img src="http://gogodev.me/public/img/cocacola.png" class="img-responsive" />
                            </center>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="frame">
                            <center>
                                <img src="http://gogodev.me/public/img/sony.png" class="img-responsive" />
                            </center>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="frame">
                            <center>
                                <img src="http://gogodev.me/public/img/deadmau5.png" class="img-responsive" />
                            </center>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="frame">
                            <center>
                                <img src="http://gogodev.me/public/img/tiesto.png" class="img-responsive" />
                            </center>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="frame">
                            <center>
                                <img src="http://gogodev.me/public/img/redbull.png" class="img-responsive" />
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="feature" class="feature-section" style="background:#f1f4f4;">
        <div class="container" style="padding:0;">
            <div class="row" style="margin:0;">
                <div class="col-md-4 col-sm-4 col-xs-12" style="border-left:0;padding: 0;">
                    <div class="row" style="margin:0;background:#f1f4f4;">
                        <h3 class="conference">
                            Contact Us
                        </h3>
                        <div class="line"></div>
                    </div>
                    <div class="row" style="margin:0;padding:20px 0;background:#f1f4f4;min-height:322px;text-align:left;font-size: 18px;line-height: 34px;">
                        <div class="row" style="margin:0;">
                            <i class="fa fa-phone" aria-hidden="true"></i>&nbsp;Phone: +84 944507065 (Ms. Jenny)<br/>
                            <i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;Location: Institute for Computational Science and Technology (ICST), Quang Trung Software Park, District 12, HCM City, Viet <br/>
                            <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;Mail: iccse@icst.org.vn<br/>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12" style="padding:0;">
                    <div class="google-maps">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.514455853315!2d106.68797431517466!3d10.77185399232468!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752f3cedd9d98f%3A0xffc865aea6dcffdc!2sEdenStar+Saigon+Hotel!5e0!3m2!1svi!2s!4v1468761444344" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>