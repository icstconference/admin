<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $this->title;?></title>
    <meta name="description" content="<?php echo $this->description;?>" />
    <meta name="keywords" content="<?php echo $this->keyword;?>" />
    <meta name="robots" content="index, follow" />
    <meta name="generator" content="<?php echo $this->title;?>" />
    <meta name="author" content="<?php echo $this->title;?>" />
    <link rel="shortcut icon" href="<?php echo $this->favicon;?>" />
    <meta name="revisit-after" content="1 days" />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,800,600' rel='stylesheet' type='text/css'>    
    <?php
        $this->appendArrayStyleSheet($this->css);
    ?>
    <link href="<?php echo URL;?>public/cssmenu/styles.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo URL;?>public/lightbox/nlightbox.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo URL;?>public/lightbox/lightbox.css" rel="stylesheet" type="text/css"/>
	<script>
        var URL_G = '<?php echo URL;?>';
    </script>
    <style>
        body{
            font-family: 'Open Sans', sans-serif;
        }
        .buttonsocial{
            border-radius: 50%;
            width: 50px;
            height: 50px;
            font-size: 22px;
            margin-top: 20px;
        }
        .buttonblack{
            background:#333;
            color: #fff;;
        }
        .buttonblack:hover{
            background:#666;
            color: #fff;;
        }
        .buttonblueblur{
            background:#90c9dc;
            color: #fff;;
        }
        .buttonblueblur:hover{
            color: #fff;
        }
        .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9{
            position: initial !important;
        }
        /*
        menu*/
        #cssmenu{
            background:#65bcb4;
        }
        .menu_page{
            background:#65bcb4;
        }
        
        #cssmenu > ul > li::after{
            background:#f15a25;
        }
        #cssmenu > ul > li.logo_img::after{
            background:#65bcb4;
        }
        #cssmenu > ul > li:hover > a, #cssmenu > ul > li.active > a{
            color:#ffffff;
        }
        #cssmenu > ul > li.has-sub > a{
            padding-right:none;
        }
        #cssmenu > ul > li.has-sub > a::after{
            content: none;
        }
        #cssmenu > ul > li.has-sub > a::before{
            content: none;
        }
        #cssmenu ul li ul li{
            background: #fbb03b;
        }
        #cssmenu ul li ul li a{
            color:#fff;
        }
        #cssmenu > ul > li{
            padding-top:37px;
        }
        #cssmenu > ul > li > ul{
            width:222px;
        }
        #cssmenu > ul > li > ul > li{
            width:222px;
        }
        .logo_img_ipad{
            display: none;
        }
        #cssmenu ul li ul li a{
            width: 100%;
        }
        #cssmenu > ul > li.search::after{
            width: 0px;
        }
        @media(max-width:980px){
            #cssmenu > ul > li > a{
                padding: 15px 12px;
            }
            #cssmenu > ul > li.logo_img  > a{
                display: none;
            }
            .logo_img img{
                display: none;
            }
            .logo_img_ipad{
                display: block;
            }
            #cssmenu > ul > li{
                padding-top:0;
            }
        }
        @media(max-width:800px){
            #cssmenu > ul > li > ul{
                width: 100%;
            }
            #cssmenu > ul > li > ul > li{
            width:100%;
        }
        
        #cssmenu ul li ul li{
            background: #fbb03b;
        }
        #cssmenu ul li ul li {
            background: #65bcb4 none repeat scroll 0 0;
        }
        .open a:hover{
            background: #fbb03b;
        }
        .search{
            width: 20%;
            right: 220px;
            background-color: transparent;
            position: absolute;
        }
     }
     .navbar-default .navbar-nav>li>a:focus, .navbar-default .navbar-nav>li>a:hover{
        background: #336c57;
     }
     .navbar-default .navbar-nav>.active>a, .navbar-default .navbar-nav>.active>a:focus, .navbar-default .navbar-nav>.active>a:hover{
        background: #336c57;
     }
     .buttonsearch{
        border: 1px solid #fff;
        background: transparent;
        height: 30px;
        width: 30px;
        border-radius: 50%;
        color: #fff;
     }
    .h3footer{
        color: #fff;
        margin-top: 40px;
        font-size: 16px;
        font-weight: 600;
    }
    #featured{ 
        width:100%; 
        position:relative; 
        height:360px; 
        overflow:hidden;
        background:#fff;
    }
    #featured ul.ui-tabs-nav{ 
        position:absolute; 
        top:0; 
        right:0px; 
        list-style:none; 
        padding:0; margin:0; 
        width:34%; 
        height:360px;
        overflow:auto;
        overflow-x:hidden;
        overflow-y:hidden;
    }
    #featured ul.ui-tabs-nav li{ 
        padding-left:13px;  
        font-size:12px; 
        color:#666; 
    }
    #featured ul.ui-tabs-nav li img{ 
        float: left;
        margin: 6px 5px;
        background: #fff;
        padding: 2px;
        border: 1px solid #eee;
    }
    #featured ul.ui-tabs-nav li span{ 
        font-size:11px; 
        font-family: 'Open Sans', sans-serif;
        line-height:18px; 
        margin-top: 10px;
    }
    #featured li.ui-tabs-nav-item a{ 
        display:block; 
        height:72px; 
        text-decoration:none;
        color:#fff;  
        background:#8fb6a7; 
        line-height:20px; 
        outline:none;
        padding: 10px;
        font-family: 'Open Sans', sans-serif;
    }
    #featured li.ui-tabs-nav-item a:hover{ 
        background:#6a9e89; 
    }
    #featured li.ui-tabs-selected, #featured li.ui-tabs-active{ 
        background:url('images/selected-item.gif') top left no-repeat;  
    }
    #featured ul.ui-tabs-nav li.ui-tabs-selected a, #featured ul.ui-tabs-nav li.ui-tabs-active a{ 
        background:#6a9e89; 
    }
    #featured .ui-tabs-panel{ 
        width:68%; 
        height:360px; 
        background:#999; position:relative;
    }
    #featured .ui-tabs-panel .info{ 
        position:absolute; 
        bottom:0; left:0; 
        height:90px; 
        background: #333;
        opacity: 0.8;
    }
    #featured .ui-tabs-panel .info a.hideshow{
        position:absolute; font-size:11px; font-family:Verdana; color:#f0f0f0; right:10px; top:-20px; line-height:20px; margin:0; outline:none; background:#333;
    }
    #featured .info h2{ 
        font-size:1.2em; 
        font-family: 'Open Sans', sans-serif; 
        color:#fff; 
        padding:5px; 
        margin:0; 
        font-weight:normal;
        overflow:hidden; 
    }
    #featured .info p{ 
        margin:0 5px; 
        font-family: 'Open Sans', sans-serif;  
        font-size:12px; 
        line-height:15px; color:#f0f0f0;
    }
    #featured .info a{ 
        text-decoration:none; 
        color:#fff; 
    }
    #featured .info a:hover{ 
		color:#339966;
        text-decoration:none; 
    }
    #featured .ui-tabs-hide{ 
        display:none; 
    }
    .navbar-default .navbar-nav>.open>a, .navbar-default .navbar-nav>.open>a:focus, .navbar-default .navbar-nav>.open>a:hover{
        background: #336c57;
        color:#fff;
    }
    .dropdown-menu>li>a{
        padding: 8px 20px;
    }
    .dropdown-menu{
        padding: 0px;
    }
    .dropdown-menu>li>a{
        background: #45866c;
        color: #fff;
    }
    .dropdown-menu>li>a:focus, .dropdown-menu>li>a:hover{
        background: #336c57;
        color: #fff;
    }
	ul.nav li.dropdown:hover > ul.dropdown-menu {
		display: block;    
	}
	button, input, select, textarea{
		font-size:12px;
	}
	#mobile{
		display:none;
	}
	
	@media (min-width:768px) and (max-width:1024px){
		.navbar-nav a{
			font-size:10px !important;
		}
		.searcharea{
			font-size:10px !important;
		}
		.nav-tabs>li>a{
			font-size:10px !important;
		}
		.navbar {
			width: 100%;
		}
		#searcharea{
            width: 93%;
			margin-bottom: 10px;
        }
		#contentleft{
			width: 100%;
		}
		#sidebar{
            width: 100%;
			padding: 0 20px !important;
        }
		#searchboard{
			width: 100%;
			float: left;
		}
		#featured img{
			width: 100%;
		}
	}
	
    @media (max-width:760px){
        #searcharea{
            width: 90%;
            margin-bottom: 10px;
        }
        .navbar-nav{
            padding: 0 10px;
            margin: 0px;
        }

    }
    @media (max-width:510px){
        #searcharea{
            width: 89% !important;
            margin-bottom: 10px;
        }
        #sidebar{
            padding:0 20px !important;
        }

        #ads1 img{
            margin-bottom: 10px;
        }
        .navbar-nav{
            padding: 0 10px;
            margin: 0px;
        }
        .carousel-caption{
            left:0px;
            height: 150px !important;
        }
        .info{
            height: 150px !important;
        }
    }
    @media screen and (max-width: 860px){
        .flex-direction-nav .flex-prev {
            opacity: 1;
            right: 50px !important;
        }
        .info{
            height: 125px !important;
        }
        #sidebar{
            padding:0 20px !important;
        }
    }
    @media screen and (max-width: 360px){
		#slidedesktop{
			display:none !important;
		}
		#slidemobile{
			display:block !important;
		}
		#searcharea{
			width:81% !important;
		}
        .info{
            height: 95px !important;
        }
        #sidebar{
            padding:0 20px !important;
        }
		#ads1{
			padding-left:0px;
		}
		#addss1{
			padding-left:0px;
		}
		#desktop{
			padding:0px;
		}
		#desktop .container{
			background:#fff;
			width:100%;
		}
		.tableft>li {
			height: 100px !Important;
		}
		.btnemail{
			margin-top:10px;
		}
		.gototop{
			margin-top:10px;
		}
		.carousel-caption{
            left:0px;
            height: 170px !important;
        }
    }
    </style>
</head>
<body style="background:#6e9183;">
    <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5&appId=254227268096895";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="nlightbox">	
		<a href="#" class="nlightbox-close nlightbox-button"></a>	
		<div href="#" class="nlightbox-caption">
			<img class='resize_fit_center nlightbox-center' src=""		
			id="imagelightbox" />	
		</div>	
</div>
    <div class="col-sm-12 col-xs-12" style="background:#fff;min-height: 95px;">
        <div class="container" style="padding:0px;">
            <div class="col-sm-3 col-xs-12" style="margin-top: 10px;padding:0px;">
                <center>
                    <a href="<?php echo URL;?>" title="<?php echo $this->title;?>" alt="<?php echo $this->title;?>">
                        <img src="<?php echo $this->logo;?>" class="img-responsive" style="max-height: 80px;"/>
                    </a>
                </center>
            </div>
            <div class="col-sm-9 col-xs-12" id="ads1" style="margin-top: 10px;float:right;padding-right:0px;">
                <?php 
                    if($this->listadvertise1){
                        $i=0;
                        $now = date('Y-m-d');
                        foreach ($this->listadvertise1 as $value) {
                            if(strtotime($value['advertise_start']) <= strtotime($now) && strtotime($value['advertise_end']) >= strtotime($now)){
                                $i++;
                            if($i <= 2){
                ?>
                    <div  class="col-sm-6" id="addss1" style="padding-right:0px;">
                        <center>
                            <a href="<?php echo $value['advertise_url'];?>">
                                <img src="<?php echo $value['advertise_image'];?>" class="img-responsive" />
                            </a>
                        </center>
                    </div>
                <?php }}}} ?>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-xs-12" id="desktop"  style="margin-bottom:40px;">
        <div class="container" style="background:#fff;padding:0px;">
            <div class="col-sm-12 col-xs-12" style="padding:0px;background:#45866c;border-radius:0 0 5px 5px;">
                <nav class="navbar navbar-default col-sm-9 col-xs-12" role="navigation" style="border:none;background-color: transparent;margin-bottom: 0px;padding:0px;">
                        <div class="navbar-header" style="color:#fff;">
                            <button type="button" style="color:#fff;" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <?php 
                            if($this->allmenu){
                        ?>
                    <div id="navbar" class="collapse navbar-collapse" style="padding:0px;">
                        <ul class="nav navbar-nav" id="menu">
                            <li>
                                <a style="color: #fff;font-size: 14px;border-right: 1px solid #336c57;font-weight:600;text-transform:uppercase;" href="<?php echo URL;?>">Trang chủ</a>
                            </li>
                            <?php 
                                foreach ($this->allmenu as $value) {
                                    if($value['menu_child']){
                            ?>
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                                   aria-expanded="false" style="color: #fff;font-size: 14px;border-right: 1px solid #336c57;font-weight:600;text-transform:uppercase;" href="<?php echo $value['menu_url'];?>"><?php echo $value['menu_name'];?>&nbsp;<i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu">
                                    <?php 
                                        foreach ($value['menu_child'] as $value1) {
                                    ?>
                                    <li>
                                        <a style="color: #fff;font-size: 14px;border-right: 1px solid #336c57;font-weight:600;text-transform:uppercase;" href="<?php echo $value1['menu_url'];?>"><?php echo $value1['menu_name'];?></a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } else{?>
                            <li>
                                <a style="color: #fff;font-size: 14px;border-right: 1px solid #336c57;font-weight:600;text-transform:uppercase;" href="<?php echo $value['menu_url'];?>"><?php echo $value['menu_name'];?></a>
                            </li>
                        <?php }} ?>
                        </ul>
                    </div>
                    <?php } ?>
                </nav>
                <div class="col-sm-3 col-xs-12" id="searchboard" style="float:right;text-align:right;">
                    <form>
                        <input type="text" id="searcharea" value="Tìm kiếm" style="margin-right:10px;height: 30px;margin-top: 10px;border-radius: 0px;background: transparent;border: 1px solid #fff;color:#fff;padding-left:10px;">
                        <button type="submit" class="buttonsearch">
                            <i class="fa fa-search"></i>
                        </button>
                    </form>
                </div>
            </div>
            <div class="col-sm-12 col-xs-12" style="padding:20px 0;">
                <?php $this->content();?> 
            </div>
            <div class="col-sm-12 col-xs-12" style="background:#336c57;padding-bottom:30px;">
                <div class="col-sm-5 col-xs-12" style="color:#fff;font-size:12px;">
                    <h3 class="h3footer">VỀ HỘI NGOẠI NHI</h3>
                    Ngày 15 tháng 10 năm 2013, Hội Y học Thành phố Hồ Chí Minh đã phê duyệt và ban hành Quyết định số 97/2013-HYH-HCM/QĐ công nhận ban Chấp hành Hội Ngoại nhi Thành phố Hồ Chí Minh bao gồm 13 thành viên nhiệm kỳ IV (2013-2018).
                </div>
                <div class="col-sm-4 col-xs-12" style="color:#fff;">
                    <h3 class="h3footer">HÌNH ẢNH VỀ HOẠT ĐỘNG CỦA HỘI</h3>
					<div class="row" style="margin:0px;">
						<?php 
						if($this->listlibrary){
                            $i=0;
							foreach ($this->listlibrary as $value) {
						      $i++;
                              if($i<=6){
                        ?>
						<div class="col-sm-4 col-xs-6" style="margin-bottom:10px;padding-left:0px;">
							<a href="<?php echo $value['media_url'];?>" id="nlightbox-open">
                                <img src="<?php echo $value['media_url'];?>" class="img-responsive nlightbox-source" />
                            </a>
						</div>
						<?php }}} ?>
					</div>
                </div>
                <div class="col-sm-3 col-xs-12" style="color:#fff;">
                    <h3 class="h3footer">THỐNG KÊ</h3>
                    <!-- Histats.com  START  (standard)-->
					<script type="text/javascript">document.write(unescape("%3Cscript src=%27http://s10.histats.com/js15.js%27 type=%27text/javascript%27%3E%3C/script%3E"));</script>
					<a href="http://www.histats.com" target="_blank" title="website statistics" ><script  type="text/javascript" >
					try {Histats.start(1,3257096,4,1029,150,25,"00011111");
					Histats.track_hits();} catch(err){};
					</script></a>
					<noscript><a href="http://www.histats.com" target="_blank"><img  src="http://sstatic1.histats.com/0.gif?3257096&101" alt="website statistics" border="0"></a></noscript>
					<!-- Histats.com  END  -->
                </div>
            </div>
            <div class="col-sm-12 col-xs-12" style="background:#133f2f;font-size:12px;padding:20px;color:#fff;">
                © Copyright 2015  Hoingoainhi.com, All rights reserved - ® Hoingoainhi.com giữ bản quyền nội dung trên website này.
                <img src="<?php echo URL;?>public/img/ontop.png" class="img-responsive gototop" style="cursor:pointer;float:right;display:inline;"/>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<link href="http://katchup.vn/public/resources/css/nlightbox.css" rel="stylesheet">
	<script src="http://katchup.vn/public/resources/js/nlightbox.js" type="text/javascript"></script>

    <!-- Start lightbox -->
    <script src="http://katchup.vn/public/resources/js/lightbox/jquery.lightbox.js" type="text/javascript"></script>
    <script src="http://katchup.vn/public/resources/js/lightbox/lightbox.js" type="text/javascript"></script>
    <link href="http://katchup.vn/public/resources/css/lightbox.css" rel="stylesheet">
    <!-- End lightbox -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="https://jquery-ui-bootstrap.github.io/jquery-ui-bootstrap/assets/js/vendor/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="<?php echo URL;?>public/cssmenu/script.js"></script>
    <script type="text/javascript" src="<?php echo URL;?>public/js/jquery-ui-1.9.0.custom.min.js"></script>
    <script type="text/javascript" src="<?php echo URL;?>public/js/jquery-ui-tabs-rotate.js"></script>
	<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4d8754322af86c81" async="async"></script>    <?php
        $this->appendArrayScript($this->js);
    ?>
    <script>
        $(document).ready(function(){
            $("#featured").tabs({fx:{opacity: "toggle"}}).tabs("rotate", 5000, true);
        });
    </script>
	<script type="text/javascript" src="<?php echo URL.'public/js/index.js';?>"></script>
    <script>
        $(document).ready(function(){
            //menu
            var url = String(window.location);
            url = url.split("?");
            url = url[0];
            url = url.replace("#","");
            // Will only work if string in href matches with location
            var nodeParent = $('#menu li a[href="'+ url +'"]').parents();
            if(nodeParent[2].nodeName.toLowerCase() !== 'li')
            {
                $('#menu li a[href="'+ url +'"]').parent().addClass('active');
            }
            else {
                $('#menu li a[href="'+ url +'"]').closest('li.has-sub').addClass('active');
            }
            // Will also work for relative and absolute hrefs
        });

        $('.gototop').click(function(event) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $("body").offset().top
            }, 500);
        });
    </script>
</body>

</html>