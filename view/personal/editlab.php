<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<?php 
    $personalinfo = $this->personalinfo;
?>
<div class="row">
    <div id="breadcrumb" class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo URL;?>admin">Quản trị</a></li>
            <li><a href="<?php echo URL;?>personal/lab/">Danh sách nhân sự lab</a></li>
            <li><a href="<?php echo URL;?>personal/editpersonal1/<?php echo $personalinfo[0]['personal_lab_id'];?>"><?php echo $personalinfo[0]['personal_lab_name'];?></a></li>
        </ol>
    </div>
</div>
<?php 
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];
?>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <span>Form chỉnh sửa nhân sự</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding"><br/>
                <div id="error"></div>
                <form class="form-horizontal" role="form" enctype="multipart/form-data">
                    <input type="hidden" name="personal_id" value="<?php echo $personalinfo[0]['personal_lab_id'];?>">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tiêu đề nhân sự</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo $personalinfo[0]['personal_lab_name'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="personal_lab_name" name="personal_lab_name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tiêu đề nhân sự tiếng anh</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo $personalinfo[0]['personal_lab_name_english'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="personal_lab_name_english" name="personal_lab_name_english" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Chọn danh mục lab</label>
                        <div class="col-sm-3">
                            <select id="s2_with_tag" class="populate placeholder" name="personal_lab_categories">
                                <?php
                                $listpermission = $this->listpermission;
                                
                                if($_SESSION['user_id'] == 1){
                                    if($this->listcategories){
                                        foreach($this->listcategories as $lt){
                                            if($lt['news_categories_id'] == $personalinfo[0]['personal_lab_categories']){
                                ?>
                                    <option value="<?php echo $lt['news_categories_id'];?>" selected><?php echo $lt['news_categories_name'];?></option>
                                <?php }else{ ?>
                                    <option value="<?php echo $lt['news_categories_id'];?>"><?php echo $lt['news_categories_name'];?></option>
                                <?php }}}}else{
                                    if($this->listcategories){
                                        foreach($this->listcategories as $lt){
                                            if(strpos($listpermission,$lt['news_categories_id'])){
                                                if($lt['news_categories_id'] == $personalinfo[0]['personal_lab_categories']){
                                ?>
                                    <option value="<?php echo $lt['news_categories_id'];?>" selected><?php echo $lt['news_categories_name'];?></option>
                                <?php }else{ ?>
                                    <option value="<?php echo $lt['news_categories_id'];?>"><?php echo $lt['news_categories_name'];?></option>
                                <?php }}}}} ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hình ảnh đại diện (489 x 349)</label>
                        <div class="col-sm-3">
                            <?php
                            if(strlen($personalinfo[0]['personal_lab_image']) > 1){
                                ?>
                                <img src="<?php echo $personalinfo[0]['personal_lab_image'];?>" class="img-rounded imgupload" alt="avatar" id="personal_labimg">
                            <?php }else{ ?>
                                <img src="<?php echo URL;?>public/img/noneimage.jpg" class="img-rounded imgupload" alt="avatar" id="personal_labimg">
                            <?php } ?>
                            <input type="file" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="personal_lab_img" name="personal_lab_img" accept="image/jpg,image/png,image/jpeg,image/gif"  required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Vị trí nhân sự</label>
                        <div class="col-sm-8">
                            <textarea type="text" class="form-control" rows="5" title="Tooltip for name" id="wysiwig_full" name="personal_lab_position"><?php echo $personalinfo[0]['personal_lab_position'];?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Vị trí nhân sự tiếng anh</label>
                        <div class="col-sm-8">
                            <textarea type="text" class="form-control" rows="5" title="Tooltip for name" id="wysiwig_full1" name="personal_lab_position_english"><?php echo $personalinfo[0]['personal_lab_position_english'];?></textarea>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-sm-3 control-label">Thứ tự nhân sự</label>
                        <div class="col-sm-1">
                            <input type="number" class="form-control" value="<?php echo $personalinfo[0]['personal_lab_sort'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="personal_lab_sort" name="personal_lab_sort" required>
						</div>
					</div>
                    <div class="form-group" id="progressbar" style="display: none;">
                        <center>
                            <progress></progress>
                        </center>
                    </div>
					<?php                                         
						if(strpos($create[0]['permission_detail'],'personal') || $_SESSION['user_id'] == 1){                                    
					?>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-success btn-label-left" name="register" id="update1">
                                <span><i class="fa fa-clock-o"></i></span>
                                Submit
                            </button>
                        </div>
                    </div>
					<?php } ?>
                </form>
            </div>
        </div>
    </div>
</div>