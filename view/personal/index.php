<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<div class="row">
    <div id="breadcrumb" class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo URL;?>admin">Quản trị</a></li>
            <li><a href="<?php echo URL;?>personal">Danh sách nhân sự</a></li>
        </ol>
    </div>
</div>
<?php                            
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];
	if(strpos($create[0]['permission_detail'],'personal') || $_SESSION['user_id'] == 1){                                    
?>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <span>Form thêm nhân sự</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding"><br/>
                <div id="error"></div>
                <form class="form-horizontal" role="form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tiêu đề nhân sự</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Tiêu đề nhân sự" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="personal_name" name="personal_name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tiêu đề nhân sự tiếng anh</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Tiêu đề nhân sự" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="personal_name_english" name="personal_name_english" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Chọn loại nhân sự</label>
                        <div class="col-sm-2">
                            <select id="s2_with_tag" class="populate placeholder" name="personal_categories">
                                <?php
                                if($this->listcategories){
                                    foreach($this->listcategories as $lt){
                                        ?>
                                        <option value="<?php echo $lt['personal_categories_id'];?>"><?php echo $lt['personal_categories_name'];?></option>
                                    <?php }}?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hình ảnh đại diện (489 x 349)</label>
                        <div class="col-sm-3">
                            <img src="#" id="personalimg" class="img-responsive imgupload" style="border-radius:3px;border:1px solid #ddd;display:none;margin-bottom:5px;" /> 
                            <input type="file" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="personal_img" name="personal_img" accept="image/jpg,image/png,image/jpeg,image/gif"  required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Vị trí nhân sự</label>
                        <div class="col-sm-8">
                            <textarea type="text" class="form-control" rows="5" title="Tooltip for name" id="wysiwig_full" name="personal_position"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Vị trí nhân sự tiếng anh</label>
                        <div class="col-sm-8">
                            <textarea type="text" class="form-control" rows="5" title="Tooltip for name" id="wysiwig_full1" name="personal_position_english"></textarea>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-sm-3 control-label">Thứ tự nhân sự</label>
                        <div class="col-sm-1">
                            <input type="number" class="form-control" value="1" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="personal_sort" name="personal_sort" required>
						</div>
					</div>
                    <div class="form-group" id="progressbar" style="display: none;">
                        <center>
                            <progress></progress>
                        </center>
                    </div>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-success btn-label-left" name="register" id="submit">
                                <span><i class="fa fa-clock-o"></i></span>
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<?php
    $listpersonal = $this->listpersonal;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <i class="fa fa-gift"></i>
                    <span>Danh sách nhân sự</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding">
                <table class="table table-bordered table-striped table-hover table-heading table-datatable" id="datatable-1">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Nhân sự</th>
                        <th>Danh mục</th>
						<th>Thứ tự nhân sự</th>
                        <th>Ngày viết</th>
                        <th>Tùy chỉnh</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($this->listpersonal){
                        $i=0;
                        foreach($this->listpersonal as $lp){
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i;?></td>
                                <td>
                                    <?php
                                    if(strlen($lp['personal_image']) > 1){
                                        ?>
                                        <img src="<?php echo $lp['personal_image'];?>" />
                                    <?php } else {?>
                                        <img src="<?php echo URL;?>public/img/noneimage.jpg" />
                                    <?php } ?>
                                    <?php echo $lp['personal_name'];?>
                                </td>
                                <td>
                                    <?php
                                    if($this->listcategories){
                                        foreach($this->listcategories as $lt){
                                            if($lp['categories_id'] == $lt['personal_categories_id']){
                                                ?>
                                                <?php echo $lt['personal_categories_name'];?>
                                            <?php }}}?>
                                </td>
								<td>
                                    <?php echo $lp['personal_sort'];?>
                                </td>
                                <td>
                                    <?php echo $lp['personal_create_date'];?>
                                </td>
                                <td>
									<?php                                         
										if(strpos($delete[0]['permission_detail'],'personal') || $_SESSION['user_id'] == 1){                                    
									?>
                                    <a class="btn btn-primary" href="<?php echo URL;?>personal/editpersonal/<?php echo $lp['personal_id'];?>">Edit</a>
									<?php } ?>
									<?php                                         
										if(strpos($edit[0]['permission_detail'],'personal') || $_SESSION['user_id'] == 1){                                    
									?>
									<a class="btn btn-danger" href="<?php echo URL;?>personal/deletepersonal/<?php echo $lp['personal_id'];?>">Delete</a>
									<?php } ?>
                                </td>
                            </tr>
                        <?php }} ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>