<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<?php
    $categoriesinfo = $this->categoriesinfo;
?>
<div class="row">
    <div id="breadcrumb" class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo URL;?>admin">Quản trị</a></li>
            <li><a href="<?php echo URL;?>personal/categories">Danh sách loại nhân sự</a></li>
            <li><a href="<?php echo URL;?>personal/editcategories/<?php echo $categoriesinfo[0]['personal_categories_id'];?>"><?php echo $categoriesinfo[0]['personal_categories_name'];?></a></li>
        </ol>
    </div>
</div>
<?php 
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];
?>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <span>Form chỉnh sửa loại nhân sự</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding"><br/>
                <div id="error"></div>
                <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="nutrition_type" id="nutrition_type" value="">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tên loại nhân sự</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo $categoriesinfo[0]['personal_categories_name'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="personal_type" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tên loại nhân sự tiếng anh</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo $categoriesinfo[0]['personal_categories_name_english'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="personal_type_english" required>
                        </div>
                    </div>
                    <?php
                    if($this->listcategories){
                        ?>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Chọn loại nhân sự cha</label>
                            <div class="col-sm-8">
                                <select id="s2_with_tag" class="populate placeholder" name="personal_fathertype">
                                    <?php
                                    if($categoriesinfo[0]['personal_categories_father'] != 0){
                                        foreach($this->listcategories as $lt){
                                            if($categoriesinfo[0]['personal_categories_father'] == $lt['personal_categories_id']){
                                                ?>
                                                <option value="<?php echo $lt['personal_categories_id'];?>"><?php echo $lt['personal_categories_name'];?></option>
                                            <?php }} ?>
                                        <option value="0">Thư mục chính</option>
                                        <?php
                                        foreach($this->listcategories as $lt){
                                            if($categoriesinfo[0]['personal_categories_father'] != $lt['news_categories_id']){
                                                ?>
                                                <option value="<?php echo $lt['personal_categories_id'];?>"><?php echo $lt['personal_categories_name'];?></option>
                                            <?php }}}else{?>
                                        <option value="0">Thư mục chính</option>
                                        <?php
                                        foreach($this->listcategories as $lt){
                                            ?>
                                            <option value="<?php echo $lt['personal_categories_id'];?>"><?php echo $lt['personal_categories_name'];?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    <?php } ?>
					<?php                                         
						if(strpos($edit[0]['permission_detail'],'personal') || $_SESSION['user_id'] == 1){                                    
					?>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-success btn-label-left" name="register" id="submit">
                                <span><i class="fa fa-clock-o"></i></span>
                                Submit
                            </button>
                        </div>
                    </div>
					<?php } ?>
                </form>
            </div>
        </div>
    </div>
</div>