<header class="page-header">
    <h2>Tất cả bảng</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <span>Tất cả bảng</span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Form excel</h2>
    </header>
    <div class="panel-body">
        <div class="box-content">
            <div class="row" style="margin:0;">
                <div class="col-sm-6">
                    <center>
                        <a href="<?php echo URL;?>excel/import/" class="btn btn-primary btn-block">Import excel</a>
                    </center>
                </div>
                <div class="col-sm-6">
                    <center>
                        <a href="<?php echo URL;?>excel/export/" class="btn btn-primary btn-block">Export excel</a>
                    </center>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
    $listtable =$this->listtable;
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Tất cả bảng</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-default">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Table</th>
                    <th>Num row</th>
                    <th>Setting</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($listtable){
                        $i = 0;
                            foreach($listtable as $ls){
                                $i++;
                    ?>
                        <tr>
                            <td><?php echo $i;?></td>
                            <td><?php echo $ls['table_name'];?></td>
                            <td><?php echo $ls['table_num_row'];?></td>
                            <td>
                                <a class="btn btn-danger" href="<?php echo URL;?>excel/deletedata/<?php echo $ls['table_name'];?>">Delete</a>
                            </td>
                        </tr>
                <?php }} ?>
            </tbody>
        </table>
    </div>
</section>