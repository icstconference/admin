<header class="page-header">
    <h2>Xuất dữ liệu</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <span>Xuất dữ liệu</span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Xuất dữ liệu</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label class="col-sm-3 control-label">Chọn table cần xuất</label>
                <div class="col-sm-4">
                    <select id="s2_with_tag" data-plugin-selectTwo class="form-control populate" name="table_name">
                        <?php
                            if($this->listtable){
                                foreach($this->listtable as $lt){
                        ?>
                            <option value="<?php echo $lt['table_name'];?>"><?php echo $lt['table_name'];?></option>
                        <?php }}?>
                    </select>
                </div>
            </div>
            <div class="form-group" style="margin-top: 40px;">
                <div class="col-sm-offset-4 col-sm-2">
                    <button type="cancel" class="btn btn-danger btn-label-left" name="cancel">
                        <span><i class="fa fa-clock-o txt-danger"></i></span>
                        Cancel
                    </button>
                </div>
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-success btn-label-left" name="submit">
                        <span><i class="fa fa-clock-o"></i></span>
                        Submit
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>
<?php
    $listtable =$this->listtable;
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Danh sách table</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-default">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Table</th>
                    <th>Num row</th>
                    <th>Setting</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($listtable){
                        $i = 0;
                            foreach($listtable as $ls){
                                $i++;
                ?>
                <tr>
                    <td><?php echo $i;?></td>
                    <td><?php echo $ls['table_name'];?></td>
                    <td><?php echo $ls['table_num_row'];?></td>
                    <td>
                        <a class="btn btn-danger" href="<?php echo URL;?>excel/deletedata/<?php echo $ls['table_name'];?>">Delete</a>
                    </td>
                </tr>
                <?php }} ?>
             </tbody>
        </table>
    </div>
</section>