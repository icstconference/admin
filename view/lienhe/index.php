<style type="text/css">
    .indextitle1 {
        font-size: 50px;
        color: #fff;
        font-weight: 400;
        border-bottom: 3px solid #ff7733;
        padding-bottom: 0px;
        text-transform: uppercase;
    }
    .newstitle{
        font-size: 35px;
        color: #333;
        font-weight: 400;
        padding-bottom: 0px;
        text-transform: uppercase;
        text-align:center;
    }
    p{
        font-size: 16px;
        line-height: 24px;
        font-weight: 300;
    }
    .datetime{
        padding:15px 0;
        font-weight: 300;
        font-size:16px;
        color:#959595;
    }
    .view{
        padding:15px 0;
        font-weight: 300;
        font-size:16px;
        text-align:left;
        color:#959595;
    }
    .buttonsharefacebook{
        background: #ccc;
        padding: 14px 19px;
        border-radius: 50%;
        font-size: 16px;
        margin-right: 10px;
        color: #fff;
    }
    .buttonshare{
        background: #ccc;
        padding: 14px 17px;
        border-radius: 50%;
        font-size: 16px;
        margin-right: 10px;
        color: #fff;
    }
    .buttonshare:hover{
        color:#ff7733;
    }
    .buttonsharefacebook:hover{
        color:#ff7733;
    }
    .readmore{
        color: #fff;
        background: #ff7733;
        padding: 14px 30px;
        border-radius: 3px;
    }
    .readmore:hover{
        background: #333;
    }
    #sidebar h3{
        text-transform: none;
    }
    .linesidebar{
        width: 50px;
        border-bottom:2px solid #333;
        margin: 15px 0;
    }
    @media (min-width:1400px){
        #sidebar{
            padding:0;
        }
    }
    .google-maps {
        position: relative;
        height: 0;
    }
    .google-maps iframe {
        top: 0;
        left: 0;
        width: 100% !important;
        z-index:0;
    }
</style>
<section id="page-top" style="background:url('<?php echo URL;?>public/img/header.png') center no-repeat;background-size:cover;height:370px;width:100%;">
    <div class="row" style="text-align:center;margin:0px;background:rgba(149,149,149,0.37);height:370px;width:100%;">
        <div class="col-md-12" style="padding-top:150px;">
            <span class="indextitle1">Contact Us</span>
        </div>
    </div>
</section>
<section id="feature" class="feature-section" style="padding:50px 0 50px;">
    <div class="container" style="padding: 0px;">
        <div class="row" style="margin:0 0 20px 0;height:450px;">
            <div class="google-maps">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.921392358911!2d106.69882981441283!3d10.740541762792859!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752f9f466092ab%3A0x1e5e9741278e9f92!2zNjczIE5ndXnhu4VuIEjhu691IFRo4buNLCBUw6JuIEjGsG5nLCBRdeG6rW4gNywgSOG7kyBDaMOtIE1pbmgsIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1460011933985" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
        <div class="row" style="margin:20px 0;">
            <div class="col-md-6 col-xs-12" style="padding: 0px;">
                 <div style="margin-top: 15px;color: #63544d;font-weight:300;">
                    <?php 
                        if($this->listcontact){
                            foreach ($this->listcontact as $value) {
                    ?>
                    <?php 
                        if($this->lang == 'vi'){
                    ?>
                    <table>
                        <tr>
                            <th style="width:3%;border-bottom:none;padding-bottom:10px;"><i class="fa fa-building"></i></th>
                            <td style="padding-bottom:10px;">&nbsp;&nbsp;Branch:&nbsp;<?php echo $value['contact_name'];?></td>
                        </tr>
                        <tr>
                            <th style="width:3%;border-bottom:none;padding-bottom:10px;"><i class="fa fa-map-marker"></i></th>
                            <td style="padding-bottom:10px;">&nbsp;&nbsp;Address:&nbsp;<?php echo $value['contact_address'];?></td>
                        </tr>
                        <tr>
                            <th style="width:3%;border-bottom:none;padding-bottom:10px;"><i class="fa fa-phone"></i></th>
                            <td style="padding-bottom:10px;">&nbsp;&nbsp;Mobile:&nbsp;<?php echo $value['contact_phone'];?></td>
                        </tr>
                        <tr>
                            <th style="width:3%;border-bottom:none;padding-bottom:10px;"><i class="fa fa-fax"></i></th>
                            <td style="padding-bottom:10px;">&nbsp;&nbsp;Fax:&nbsp;<?php echo $value['contact_fax'];?></td>
                        </tr>
                        <tr>
                            <th style="width:3%;border-bottom:none;padding-bottom:10px;"><i class="fa fa-envelope"></i></th>
                            <td style="padding-bottom:10px;">&nbsp;&nbsp;Email:&nbsp;<?php echo $value['contact_email'];?></td>
                        </tr>
                    </table>
                    <?php }else{ ?>
                    <table>
                        <tr>
                            <th style="width:3%;border-bottom:none;"><i class="fa fa-building"></i></th>
                            <td>Branch:&nbsp;<?php echo $value['contact_name_english'];?></td>
                        </tr>
                        <tr>
                            <th style="width:3%;border-bottom:none;"><i class="fa fa-map-marker"></i></th>
                            <td>Address:&nbsp;<?php echo $value['contact_address_english'];?></td>
                        </tr>
                        <tr>
                            <th style="width:3%;border-bottom:none;"><i class="fa fa-phone"></i></th>
                            <td>Phone:&nbsp;<?php echo $value['contact_phone'];?></td>
                        </tr>
                        <tr>
                            <th style="width:3%;border-bottom:none;"><i class="fa fa-fax"></i></th>
                            <td>Fax:&nbsp;<?php echo $value['contact_fax'];?></td>
                        </tr>
                        <tr>
                            <th style="width:3%;border-bottom:none;"><i class="fa fa-envelope"></i></th>
                            <td>Email:&nbsp;<?php echo $value['contact_email'];?></td>
                        </tr>
                    </table>
                    <?php }}} ?>
                </div>
            </div>
            <div class="col-md-6 col-xs-12" style="padding:0;">
                <div class="row" style="margin:10px 0;">
                    <form action="<?php echo URL;?>contact/addcontact/" method="post">
                        <input type="text" name="first-name" style="width: 49%;border: 1px solid #333;height:35px;margin-right: 5px;padding: 5px;margin-bottom:10px;background: #fff;border-radius: 3px;" placeholder="FIRST NAME" required>
                        <input type="text" name="last-name" style="width: 49%;border: 1px solid #333;height:35px;/* margin-right:22px; */padding: 5px;margin-bottom:10px;background: #fff;border-radius: 3px;" placeholder="LAST NAME" required>
                        <input type="email" name="your-email" style="width: 49%;border: 1px solid #333;height:35px;margin-right: 5px;padding: 5px;margin-bottom:10px;background: #fff;border-radius: 3px;" placeholder="YOUR EMAIL" required>
                        <input type="text" name="your-phone" style="width: 49%;border: 1px solid #333;height:35px;/* margin-right:22px; */padding: 5px;margin-bottom:10px;background: #fff;border-radius: 3px;" placeholder="YOUR PHONE" required>
                        <textarea name="your-message" style="width:100%;border:1px solid #333;padding: 5px;height: 120px;resize: none;background: #fff;border-radius: 3px;" placeholder="YOUR MESSAGE" required></textarea>
                        <br/><br/>
                        <center>
                            <button type="submit" class="btn buttonsubcribe btn-min-block" style="text-align:center;border-radius:3px;">
                                Send message
                            </button>
                        </center>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>