<header class="page-header">
    <h2>Thêm hình mới</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="<?php echo URL;?>library">
                    <span>Hình ảnh hoạt động</span>
                </a>
            </li>
            <li><span>Thêm hình mới</span></li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Thêm hình mới</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="dropzone dz-square dz-clickable" id="dropzone-example">
            <input type="hidden" value="1" name="library"/>

            <div class="dz-default dz-message">
                
            </div>
            
            <div class="form-group" id="progressbar" style="display: none;">
                <center>
                    <progress></progress>
                </center>
            </div>
        </form>
        <div class="row dropzone dz-square dz-clickable" style="margin:10px 0;min-height:300px;background:rgba(0,0,0,0.03);" id="zoneimg">
            
        </div>

    </div>
</section>