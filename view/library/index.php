<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<header class="page-header">
    <h2>Hình ảnh hoạt động</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <span>Hình ảnh hoạt động</span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<?php     
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];
?>
<?php
    $listlibrary = $this->listlibrary;
?>
<div class="row" style="margin: 0 0 10px;text-align:right;">
    <a href="<?php echo URL;?>library/addimage/" class="mb-xs mt-xs mr-xs btn btn-primary">
        <i class="fa fa-file-image-o"></i>&nbsp;Thêm hình mới
    </a>
    <button href="#modalHeaderColorPrimary" class="mb-xs mt-xs mr-xs modal-basic btn btn-danger" disabled id="deletepost">
        <i class="fa fa-trash"></i>&nbsp;Xóa hình
    </button>
</div>
<span id="typepost" rel="library"></span>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Hình ảnh hoạt động</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
            <table class="table table-bordered table-striped mb-none" id="datatable-default">
                <thead>
                    <tr>
                        <th style="vertical-align:middle;text-align:center;"><input name="select_all" value="1" id="example-select-all" type="checkbox" /></th>
                        <th style="vertical-align:middle;">Hình ảnh</th>
                        <th style="vertical-align:middle;">Loại</th>
                        <th style="vertical-align:middle;">Đường dẫn</th>
                        <th style="vertical-align:middle;">Tùy chỉnh</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    if($this->listlibrary){
                        $i=0;
                        foreach($this->listlibrary as $lp){
                            $i++;
                ?>
                    <tr>
                        <td style="vertical-align:middle;text-align:center;"><?php echo $lp['media_id'];?></td>
                        <td style="vertical-align:middle;">
                            <img src="<?php echo $lp['media_url'];?>" style="width:100px;"/>
                        </td>
                        <td style="vertical-align:middle;">
                            <?php echo $lp['media_type'];?>
                        </td>
                        <td style="vertical-align:middle;">
                            <?php echo $lp['media_url'];?>
                        </td>
                        <td style="vertical-align:middle;">
    						<?php                                         
    							if(strpos($delete[0]['permission_detail'],'galleries') || $_SESSION['user_type'] == 1){                                    
    						?>
                                <a class="btn btn-danger" href="<?php echo URL;?>library/deletelibrary/<?php echo $lp['media_id'];?>"><i class="fa fa-trash"></i></a>
    						<?php } ?>
    					</td>
                    </tr>
                <?php }} ?>
                </tbody>
            </table>
        </div>
</section>
<div id="modalHeaderColorPrimary" class="modal-block modal-header-color modal-block-primary mfp-hide">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Thông báo</h2>
            </header>
            <div class="panel-body">
                <div class="modal-wrapper">
                    <div class="modal-icon">
                        <i class="fa fa-question-circle"></i>
                    </div>
                    <div class="modal-text">
                        <p>Xóa vĩnh viễn ?</p>
                    </div>
                </div>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button class="btn btn-primary modal-confirm">Đồng ý</button>
                        <button class="btn btn-default modal-dismiss">Hủy bỏ</button>
                    </div>
                </div>
            </footer>
        </section>
    </div>