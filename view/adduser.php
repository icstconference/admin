<header class="page-header">
    <h2>Thêm người mới</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="<?php echo URL;?>user">
                    <span>Danh sách người dùng</span>
                </a>
            </li>
            <li><span>Thêm người mới</span></li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<div class="row" style="margin: 0 0 10px;text-align:left;">
    <a href="<?php echo URL;?>user/" class="mb-xs mt-xs mr-xs btn btn-primary">
        <i class="fa fa-mail-reply"></i>&nbsp;Danh sách người dùng
    </a>
</div>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Thêm người mới</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal form-bordered" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label class="col-sm-2 control-label">Username</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Username" data-toggle="tooltip" data-placement="bottom" title="Nhập tên người dùng" name="username" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Loại người dùng</label>
                <div class="col-sm-10">
                    <select data-plugin-selectTwo class="form-control populate" name="user_type">
                        <option value="0">Người dùng</option>
                    <?php 
                        if($this->listgroup){
                            foreach ($this->listgroup as $value) {
                    ?>
                        <option value="<?php echo $value['user_group_id'];?>"><?php echo $value['user_group_name'];?></option>
                    <?php }} ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" placeholder="Password" data-toggle="tooltip" data-placement="bottom" title="Điền mật khẩu người dùng" name="password" required/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Nhập lại password</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" placeholder="Password" data-toggle="tooltip" data-placement="bottom" title="Điền lại mật khẩu người dùng" name="repassword" required/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Email" data-toggle="tooltip" data-placement="bottom" title="Điền email người dùng" name="email" required/>
                </div>
            </div>
            <div class="form-group" style="margin-top: 40px;">
                <div class="col-sm-offset-4 col-sm-2">
                    <button type="cancel" class="btn btn-danger btn-label-left" name="cancel">
                        <span><i class="fa fa-clock-o txt-danger"></i></span>
                        Reset
                    </button>
                </div>
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-success btn-label-left" name="register" value="">
                        <span><i class="fa fa-clock-o"></i></span>
                        Submit
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>