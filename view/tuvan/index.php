<style type="text/css">
    .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
        background: #215240;
        color:#fff !important;
        font-family: 'Open Sans', sans-serif;
    }
    .nav-tabs>li>a{
        border:1px solid #dfdfdf;
        border-radius: 8px 8px 0 0;
        margin-right: 0px;
        color: #333;
    }
    .tableft>li{
        list-style: none;
        min-height: 20px;
        border-bottom: 1px solid #dfdfdf;
        padding:10px;
    }
    .tableft a{
        text-decoration: none;
        color:#339966;
        font-family: 'Open Sans', sans-serif;
        font-size: 12px;
        font-weight: 600;
    }
    .tableft a:hover{
        text-decoration: none;
        color:#215240;
    }
    .panel{
        border-radius: 8px 8px 0 0;
    }
    .panel-default>.panel-heading{
        background: #45866c;
        color: #fff;
        border-radius: 8px 8px 0 0;
        text-transform: uppercase;
        font-weight: bold;
    }
    .panel-body a{
        text-decoration: none;
    }
    .panel-body a:hover{
        color: #339966;
    }
    .h3danhmuc{
        margin: 5px 0px;
        font-size: 15px;
        color: #747474;
        font-weight: 600;
        margin-left: 10px;
        line-height: 18px;
    }
    .h3danhmuc:hover{
        color: #339966;
    }
    .description{
        margin-left: 10px;
        margin-right: 10px;
        font-size: 12px;
        color: #333;
        margin-top: 5px;
        margin-bottom: 5px;
    }
    .datetime{
        margin-left: 10px;
        font-size: 12px;
        margin-top: 5px;
        margin-bottom: 5px;
        color: #747474;
    }
    .h3email{
        margin-top: 0px;
        font-size: 15px;
        font-weight: 800;
    }
    .inputemail{
        width: 75%;
        height: 36px;
        border-radius: 0px !important;
        padding: 8px !important;
    }
    .btnemail{
        text-align: center;
        margin-left: -5px;
        height: 36px;
        padding: 0 18px;
        background: #45866c;
        color: #fff;
        border:none;
    }
    .btnemail:hover{
        background: #215240;
    }
    .selectlink{
        height: 36px;
        width: 99%;
        padding: 5px;
    }
    .h3tuvan{
        font-size: 16px;
        font-weight: 600;
    }
    .fb-like-box, .fb-like-box span, .fb-like-box span iframe[style] { width: 100% !important; }
    .flex-direction-nav .flex-prev {
        position: absolute;
        right: 60px;
        top: -55px;
    }
    .flex-direction-nav .flex-next {
        right: 10px;
        top: -55px;
    }
</style>

<div class="col-sm-8 col-xs-12" style="padding:0 20px;">
    <div class="row" style="margin:0;">
        <div class="panel panel-default">
            <div class="panel-heading">
                <img src="<?php echo URL;?>public/img/iconcmt.png" />&nbsp;
                    TƯ VẤN
            </div>
            <div class="panel-body" style="padding:0px;">
                <div class="col-sm-6 col-xs-12" style="padding-left: 20px;">
                    <h3 class="h3tuvan">CÂU HỎI THƯỜNG GẶP</h3>
                    <?php 
                        if($this->listquestion){
                    ?>
                    <ul class="tableft" style="padding:0px;margin-bottom: 0px;">
                        <?php 
                            foreach ($this->listquestion as $value) {
                                if($value['questionaire_status'] == 'on'){
                        ?>
                        <li style="padding-left: 0px;padding-top: 5px;">
                            <a href="<?php echo URL.'question/detail/'.$value['questionaire_url'];?>"><?php echo ucfirst($value['questionaire_question']);?></a>
                        </li>
                        <?php }} ?>
                    </ul>
                    <?php } ?>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <h3 class="h3tuvan">GỬI CÂU HỎI CHO BÁC SĨ</h3>
                    <form action="<?php echo URL;?>question/createquestion/" method="post" style="padding-top: 5px;">
                        <input type="text" placeholder="Họ và tên" name="questionaire_name" style="width: 100%;border-radius: 0px;height: 34px;border: 1px solid #dfdfdf;padding: 5px;" required/>
                        <input type="text" placeholder="Email" name="questionaire_email" style="margin-top: 10px;margin-right: 3px;width: 49%;border-radius: 0px;height: 34px;border: 1px solid #dfdfdf;padding: 5px;" required/>
                        <input type="text" placeholder="Số điện thoại" name="questionaire_phone" style="width: 49%;border-radius: 0px;height: 34px;border: 1px solid #dfdfdf;padding: 5px;" required/>
                        <textarea name="questionaire_question" style="margin-top: 10px;border-radius: 0px;width:100%;resize:none;height: 170px;border: 1px solid #dfdfdf;padding: 5px;" placeholder="Nhập nội dung câu hỏi..." required></textarea>
                        <center>
                            <button type="submit" class="btnemail" style="text-align:center;margin: 10px 0;">
                                Gửi câu hỏi
                            </button>
                        </center>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-4 col-xs-12" style="padding:0 20px 0 0;">
    <div class="row" style="margin:0;">
        <ul class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" href="#view" style="text-transform:uppercase;font-size:13px;">Bài viết được xem nhiều</a>
            </li>
            <li>
                <a data-toggle="tab" href="#lastest" style="text-transform:uppercase;font-size:13px;width: 170px;">Bài viết mới nhất</a>
            </li>
        </ul>
        <div class="tab-content" style="margin-right: 1px;">
            <div id="view" class="tab-pane active" style="border:1px solid #dfdfdf;border-top:none;border-bottom:none;">
                <?php 
                    if($this->listnewsview){
                ?>
                <ul class="tableft" style="padding:0px;">
                    <?php 
                        foreach ($this->listnewsview as $value) {
                    ?>
                    <li>
                        <?php 
                            if(strlen($value['news_image_thumb']) > 0){
                        ?>
                        <div style="width:60px;float:left;margin-right:5px;">
                            <img src="<?php echo $value['news_image_thumb'];?>" style="width:60px;">
                        </div>
                        <?php }else{ ?>
                        <div style="width:60px;float:left;margin-right:5px;">
                            <img src="<?php echo URL;?>public/img/noneimage.jpg" style="width:60px;">
                        </div>
                        <?php } ?>
                        <div>
                            <a href="<?php echo URL.'news/detail/'.$value['news_url'];?>" title="<?php echo $value['news_name'];?>"><?php echo $value['news_name'];?></a>
                            <br/><span style="color:#ababab;font-size:12px;font-weight:500;"><i class="fa fa-clock-o"></i>&nbsp;<?php echo date('d/m/Y h:i',strtotime($value['news_create_date']));?></span>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
                <?php } ?>
            </div>
            <div id="lastest" class="tab-pane" style="border:1px solid #dfdfdf;border-top:none;border-bottom:none;">
                <?php 
                    if($this->listnewsrelease){
                ?>
                <ul class="tableft" style="padding:0px;">
                    <?php 
                        foreach ($this->listnewsrelease as $value) {
                    ?>
                    <li>
                        <?php 
                            if(strlen($value['news_image_thumb']) > 0){
                        ?>
                        <div style="width:60px;float:left;margin-right:5px;">
                            <img src="<?php echo $value['news_image_thumb'];?>" style="width:60px;">
                        </div>
                        <?php }else{ ?>
                        <div style="width:60px;float:left;margin-right:5px;">
                            <img src="<?php echo URL;?>public/img/noneimage.jpg" style="width:60px;">
                        </div>
                        <?php } ?>
                        <div>
                            <a href="<?php echo URL.'news/detail/'.$value['news_url'];?>" title="<?php echo $value['news_name'];?>"><?php echo $value['news_name'];?></a>
                            <br/><span style="color:#ababab;font-size:12px;font-weight:500;"><i class="fa fa-clock-o"></i>&nbsp;<?php echo date('d/m/Y h:i',strtotime($value['news_create_date']));?></span>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="row" style="margin:0;margin-top:10px;">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>

          <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <?php 
                    if($this->listadvertise3){
                        $i=0;
                        $now = date('Y-m-d');
                        foreach ($this->listadvertise3 as $value) {
                            if(strtotime($value['advertise_start']) <= strtotime($now) && strtotime($value['advertise_end']) >= strtotime($now)){
                                $i++;
                            if($i <= 3){
                                if($i == 1){
                ?>
                    <div class="item active">
                        <center>
                            <a href="<?php echo $value['advertise_url'];?>">
                                <img src="<?php echo $value['advertise_image'];?>" class="img-responsive" />
                            </a>
                        </center>
                    </div>
                <?php }else{?>
                    <div class="item">
                        <center>
                            <a href="<?php echo $value['advertise_url'];?>">
                                <img src="<?php echo $value['advertise_image'];?>" class="img-responsive" />
                            </a>
                        </center>
                    </div>
                <?php }}}}} ?>
            </div>
        </div>
    </div>
    <div class="row" style="margin:0px;margin-top:20px;">
        <center>
            <h3 class="h3email">ĐĂNG KÝ NHẬN TIN QUA EMAIL</h3>
            <form action="<?php echo URL;?>email/createemail/" method="post" enctype="multipart/form-data">
                <input type="email" name="email_register" placeholder="Nhập vào địa chỉ email của bạn..." class="inputemail" />
                <button type="submit" class="btnemail" style="text-align:center;">
                    Đăng ký
                </button>
            </form>
        </center>
    </div>
    <div class="row" style="margin:0px;margin-top:20px;">
        <center>
            <form>
                <select class="selectlink" id="linklienket">
                    <OPTION>WEBSITE LIÊN KẾT VỚI HỘI</OPTION>
                    <?php 
                        if($this->listlinker){
                            foreach ($this->listlinker as $value) {
                    ?>
                    <OPTION value="<?php echo $value['linker_url'];?>"><a href="<?php echo $value['linker_url'];?>"><?php echo $value['linker_name'];?></a></OPTION>
                    <?php } }?>
                </select>
            </form>
        </center>
    </div>
    <div class="row" style="margin:0px;margin-top:20px;">
        <center>
            <img src="<?php echo URL;?>public/img/clip.png" class="img-responsive" />
        </center>
    </div>
    <div class="row" style="margin:0px;margin-top:20px;">
            <?php 
                if($this->listpartner){
                    foreach ($this->listpartner as $value) {
            ?>
                <a href="<?php echo $value['partner_url'];?>" title="<?php echo $value['partner_name'];?>">
                    <img src="<?php echo $value['partner_image'];?>" class="img-responsive" style="height:62px;display:inline;margin:5px;"/>
                </a>
            <?php }} ?>
    </div>
    <div class="row" style="margin:0px;margin-top:20px;">
        <center>
            <div class="fb-page" data-href="https://www.facebook.com/benhviennhi.org.vn/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/benhviennhi.org.vn/"><a href="https://www.facebook.com/benhviennhi.org.vn/">Bệnh Viện Nhi Đồng 2, BV Nhi Dong 2, BVND2, BV NĐ2</a></blockquote></div></div>
        </center>
    </div>
</div>