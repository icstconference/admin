<style type="text/css">
    .indextitle1 {
        font-size: 50px;
        color: #fff;
        font-weight: 400;
        border-bottom: 3px solid #ff7733;
        padding-bottom: 0px;
        text-transform: uppercase;
    }
    .newstitle{
        font-size: 35px;
        color: #333;
        font-weight: 400;
        padding-bottom: 0px;
        text-transform: uppercase;
        text-align:center;
    }
    p{
        font-size: 16px;
        line-height: 24px;
        font-weight: 300;
    }
    .datetime{
        padding:15px 0;
        font-weight: 300;
        font-size:16px;
        color:#959595;
    }
    .view{
        padding:15px 0;
        font-weight: 300;
        font-size:16px;
        text-align:left;
        color:#959595;
    }
    .buttonsharefacebook{
        background: #ccc;
        padding: 14px 19px;
        border-radius: 50%;
        font-size: 16px;
        margin-right: 10px;
        color: #fff;
    }
    .buttonshare{
        background: #ccc;
        padding: 14px 17px;
        border-radius: 50%;
        font-size: 16px;
        margin-right: 10px;
        color: #fff;
    }
    .buttonshare:hover{
        color:#ff7733;
    }
    .buttonsharefacebook:hover{
        color:#ff7733;
    }
    .readmore{
        color: #fff;
        background: #ff7733;
        padding: 14px 30px;
        border-radius: 3px;
    }
    .readmore:hover{
        background: #333;
    }
    #sidebar h3{
        text-transform: none;
    }
    .linesidebar{
        width: 50px;
        border-bottom:2px solid #333;
        margin: 15px 0;
    }
    @media (min-width:1400px){
        #sidebar{
            padding:0;
        }
    }
    .google-maps {
        position: relative;
        height: 0;
    }
    .google-maps iframe {
        top: 0;
        left: 0;
        width: 100% !important;
        z-index:0;
    }
    .isotope {
        -webkit-transition-property: height, width;
        -moz-transition-property: height, width;
        -ms-transition-property: height, width;
        -o-transition-property: height, width;
        transition-property: height, width;
    }
    .isotope .isotope-item {
        -webkit-transition-property: -webkit-transform, opacity;
        -moz-transition-property: -moz-transform, opacity;
        -ms-transition-property: -ms-transform, opacity;
        -o-transition-property: -o-transform, opacity;
        transition-property: transform, opacity;
        padding-bottom:50px;
    }
    .isotope, .isotope .isotope-item {
        -webkit-transition-duration: 0.5s;
        -moz-transition-duration: 0.5s;
        -ms-transition-duration: 0.5s;
        -o-transition-duration: 0.5s;
        transition-duration: 0.5s;
    }
    article:hover figure{
        -ms-transform: rotateY(180deg);
        -o-transform: rotateY(180deg);
        -webkit-transform: rotateY(180deg);
        transform: rotateY(180deg);
    }
    article:hover figure figcaption .desc{
        opacity: 0;
        visibility: hidden;
    }
    figure{
        border: 5px solid rgba(0, 0, 0, 0.5);
        position: relative;
        -webkit-transition: all 0.2s ease-out;
        -moz-transition: all 0.2s ease-out;
        -o-transition: all 0.2s ease-out;
        -ms-transition: all 0.2s ease-out;
        transition: all 0.2s ease-out;
    }
    figure .back-face {
        background: rgba(242,101,34,0.8);
    }
    article:hover .back-face{
        opacity: 1;
        visibility: visible;
    }
    .back-face {
        position: absolute;
        width: 100%;
        height: 100%;
        padding: 10px;
        display: block;
        background: rgba(242,101,34,0.8);
        opacity: 0;
        visibility: hidden;
        -webkit-transition: all 0.2s ease-out;
        -moz-transition: all 0.2s ease-out;
        -o-transition: all 0.2s ease-out;
        -ms-transition: all 0.2s ease-out;
        transition: all 0.2s ease-out;
        -ms-transform: rotateY(-180deg);
        -o-transform: rotateY(-180deg);
        -webkit-transform: rotateY(-180deg);
        transform: rotateY(-180deg);
    }

    .back-face h5 {
        margin-top: 0;
        margin-bottom: 5px;
        color: #ededed;
        text-align: right;
    }
    .back-face > i {
        position: absolute;
        top: 50%;
        left: 50%;
        margin-top: -7px;
        margin-left: -7px;
        color: #fff;
    }
    .desc.gradient {
        background: rgba(255, 255, 255, 0);
    }
    .desc {
        -webkit-transition: all 0.2s ease-out;
        -moz-transition: all 0.2s ease-out;
        -o-transition: all 0.2s ease-out;
        -ms-transition: all 0.2s ease-out;
        transition: all 0.2s ease-out;
        position: absolute;
        width: 100%;
        bottom: 0;
        left: 0;
        padding: 10px;
        background: rgba(14, 14, 14, 0.85);
    }
    .desc h5 {
        margin-top: 0;
        margin-bottom: 5px;
        color: #ededed;
        text-align: left;
    }
    figcaption{
        display: block;
    }
    .texttop1{
        text-align:center;margin:0px;background:rgba(149,149,149,0.37);height:100%;width:100%;
    }
    .bgtop1{
        background:url('<?php echo URL;?>public/img/artist.png') fixed center no-repeat;background-size:cover;height:100%;
    }
</style>
<section id="page-top" class="bgtop1">
    <div class="row texttop1">
        <div class="col-md-12" id="indexartist" style="padding-top:150px;">
            <span class="indextitle1">Artists</span>
        </div>
        <div class="col-md-12" style="padding:50px 0;">
            <div class="container isotope">
                <?php 
                    if($this->listartist){
                        foreach($this->listartist as $lt){
                ?>
                <article class="col-sm-3 col-xs-6 electro progressive isotope-item">
                    <figure>
                        <a href="<?php echo URL.$lt['artist_url'];?>" class="back-face">
                            <h5>
                                <?php echo $lt['artist_name'];?>                                                    
                            </h5>
                            <i class="fa fa-link"></i>
                        </a>
                        <figcaption>
                            <div class="desc gradient">
                                <h5>
                                    <?php echo $lt['artist_name'];?>                                                      
                                </h5>
                            </div>
                            <img width="400" height="400" src="<?php echo $lt['artist_image'];?>" class="img-responsive lazyloaded" alt="">
                        </figcaption>
                    </figure>
                </article>
                <?php }} ?>
            </div>
        </div>
    </div>
</section>