<?php 
    $actual_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER[REQUEST_URI];
    function cut_string($str, $len, $more) {
        if ($str == "" || $str == NULL)
            return $str;
        if (is_array($str))
            return $str;
        $str = trim($str);
        if (strlen($str) <= $len)
            return $str;
        $str = substr($str, 0, $len);
        if ($str != "") {
            if (!substr_count($str, " ")) {
                if ($more)
                    $str .= " ...";
                return $str;
            }
            while (strlen($str) && ($str[strlen($str) - 1] != " ")) {
                $str = substr($str, 0, -1);
            }
            $str = substr($str, 0, -1);
            if ($more)
                $str .= " ...";
        }
        return $str;
    }

    $newsinfo = $this->newsinfo;
    $catinfo = $this->catinfo;
    $configurl = $this->configurl;
?>
<style type="text/css">
    .indextitle1 {
        font-size: 45px;
        color: #fff;
        font-weight: 400;
        border-bottom: 3px solid #ff7733;
        padding-bottom: 0px;
        text-transform: uppercase;
    }
    .newstitle{
        font-size: 35px;
        color: #333;
        font-weight: 400;
        padding-bottom: 0px;
        text-transform: uppercase;
        text-align:center;
    }
    p{
        font-size: 16px;
        line-height: 24px;
        font-weight: 300;
    }
    .datetime{
        padding:15px 0;
        font-weight: 300;
        font-size:16px;
        color:#959595;
    }
    .view{
        padding:15px 0;
        font-weight: 300;
        font-size:16px;
        text-align:left;
        color:#959595;
    }
    .buttonsharefacebook{
        background: #ccc;
        padding: 14px 19px;
        border-radius: 50%;
        font-size: 16px;
        margin-right: 10px;
        color: #fff;
    }
    .buttonshare{
        background: #ccc;
        padding: 14px 17px;
        border-radius: 50%;
        font-size: 16px;
        margin-right: 10px;
        color: #fff;
    }
    .buttonshare:hover{
        color:#ff7733;
    }
    .buttonsharefacebook:hover{
        color:#ff7733;
    }
    .readmore{
        color: #fff;
        background: #ff7733;
        padding: 14px 30px;
        border-radius: 3px;
    }
    .readmore:hover{
        background: #333;
    }
    #sidebar h3{
        text-transform: none;
    }
    .linesidebar{
        width: 50px;
        border-bottom:2px solid #333;
        margin: 15px 0;
    }
    @media (min-width:1400px){
        #sidebar{
            padding:0;
        }
    }
    .google-maps {
        position: relative;
        height: 0;
    }
    .google-maps iframe {
        top: 0;
        left: 0;
        width: 100% !important;
        z-index:0;
    }
    .eventname{
        padding: 20px 10px;
        text-align: center;
        background: #9c9c9c;
        font-size: 16px;
        font-weight: 500;
    }
    .bgtop{
        background:url('<?php echo URL;?>public/img/header.png') center no-repeat;background-size:cover;height:370px;width:100%;
    }
    .texttop{
        text-align:center;margin:0px;background:rgba(149,149,149,0.37);height:370px;width:100%;
    }
</style>
<?php 
    $artistinfo = $this->artistinfo;
?>
<section id="page-top" class="bgtop">
    <div class="row texttop">
        <div class="col-md-12" id="indexartist" style="padding-top:150px;">
            <span class="indextitle1" ><?php echo $artistinfo[0]['artist_name'];?></span>
        </div>
    </div>
</section>
<section id="feature" class="feature-section" style="padding:25px 0 50px;">
    <div class="container" style="padding: 0px;">
        <div class="col-md-4 col-lg-4 col-xs-12" style="padding-left:0;">
            <div class="row" style="margin:0 0 25px;">
                <img src="<?php echo $artistinfo[0]['artist_image'];?>" class="img-responsive" />
            </div>
            <div class="row" style="margin:0 0 25px;">
                <span class="indextitle1" style="color:#333;font-size:30px;">EVENTS</span>
                <div class="col-md-12 col-lg-12 col-xs-12" style="padding:0;">
                    <?php 
                        if($this->listevent){
                                    foreach($this->listevent as $lb){
                                            if(strlen($artistinfo[0]['artist_event']) >= 1){
                                                $part = explode(',', $artistinfo[0]['artist_event']);
                                                $dem = 0;
                                                if(count($part) > 0){
                                                    foreach($part as $vl){
                                                        if($vl == $lt['event_id']){
                                                            $dem++;
                                                        }
                                                    }
                                                }else{
                                                    if($artistinfo[0]['artist_event'] == $lt['event_id']){
                                                        $dem++;
                                                    }
                                                }

                                                if($dem > 0){
                    ?>
                    <div class="row" style="margin:20px 0;">
                        <div class="col-md-3 col-lg-3 col-xs-12" style="border: 2px solid #ff7733;text-align: center;padding: 10px 0;font-size: 14px;font-weight: 500;">
                            <?php echo date('d',strtotime($lb['event_date']));?><br/>
                            <?php echo date('F',strtotime($lb['event_date']));?><br/>
                            <?php echo date('Y',strtotime($lb['event_date']));?>
                        </div>
                        <div class="col-md-9 col-lg-9 col-xs-12 eventname">
                            <a href="<?php echo URL.$lb['event_url'];?>" title="<?php echo $lb['event_name'];?>">
                                <?php echo cut_string($lb['event_name'],20,30);?> <br/>
                                <i class="fa fa-map-marker"></i>&nbsp;<?php echo $lb['event_place'];?>
                            </a>
                        </div>
                    </div>
                    <?php }}}} ?>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-lg-8 col-xs-12" style="background: #ededed;padding: 15px;">
            <div class="row" style="margin:0;border-bottom: 3px solid #333;">
                <span class="indextitle1" style="color:#333;font-size:30px;border-bottom:0px !important;"><?php echo $artistinfo[0]['artist_name'];?></span>
            </div>
            <div class="row" style="margin: 10px 0;">
                <?php echo $artistinfo[0]['artist_description'];?>
            </div>
        </div>
    </div>
</section>