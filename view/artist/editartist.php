
<?php 
    $artistinfo = $this->artistinfo;
?>
<header class="page-header">
    <h2><?php echo $artistinfo[0]['artist_name'];?></h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="<?php echo URL;?>artist/artist/">
                    <span>Tất cả diễn giả</span>
                </a>
            </li>
            <li>
                <span><?php echo $artistinfo[0]['artist_name'];?></span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<?php                            
    $create = $_SESSION['user_permission_create'];
    $delete = $_SESSION['user_permission_delete'];
    $edit = $_SESSION['user_permission_edit'];
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Chỉnh sửa diễn giả</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" role="form" enctype="multipart/form-data">
                    <input type="hidden" name="artist_id" value="<?php echo $artistinfo[0]['artist_id'];?>" />
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tên diễn giả</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="artist_name" name="artist_name" value="<?php echo $artistinfo[0]['artist_name'];?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hình ảnh đại diện</label>
                        <div class="col-sm-3">
                            <?php
                            if(strlen($artistinfo[0]['artist_image']) > 1){
                                ?>
                                <img src="<?php echo $eventinfo[0]['artist_image'];?>" class="img-responsive" alt="avatar" id="eventimg">
                            <?php }else{ ?>
                                <img src="<?php echo URL;?>public/img/noneimage.jpg" class="img-responsive" alt="avatar" id="eventimg">
                            <?php } ?>
                            <input type="file" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="event_img" name="event_img" accept="image/jpg,image/png,image/jpeg,image/gif">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Sụ kiện đã tham gia</label>
                        <div class="col-sm-8">
                            <select multiple="" id="s2_with_tag" data-plugin-selectTwo class="form-control populate"  style="padding:0px;" name="artist_event[]">
                                <?php
                                if($this->listevent){
                                    foreach($this->listevent as $lb){
                                            if(strlen($artistinfo[0]['artist_event']) >= 1){
                                                $part = explode(',', $artistinfo[0]['artist_event']);
                                                $dem = 0;
                                                if(count($part) > 0){
                                                    foreach($part as $vl){
                                                        if($vl == $lt['event_id']){
                                                            $dem++;
                                                        }
                                                    }
                                                }else{
                                                    if($artistinfo[0]['artist_event'] == $lt['event_id']){
                                                        $dem++;
                                                    }
                                                }

                                                if($dem > 0){
                                            ?>
                                            <option value="<?php echo $lb['event_id'];?>" selected><?php echo $lb['event_name'];?></option>
                                            <?php }else{ ?>
                                            <option value="<?php echo $lb['event_id'];?>"><?php echo $lb['event_name'];?></option>
                                            <?php } ?>
                                        <?php }else{ ?>
                                        <option value="<?php echo $lb['event_id'];?>"><?php echo $lb['event_name'];?></option>
                                        <?php } ?>
                                <?php }}?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Mô tả diễn giả</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" placeholder="Mô tả sản phẩm" rows="5" title="Tooltip for name" id="wysiwig_full1" name="artist_description">
                                <?php echo $artistinfo[0]['artist_description'];?>
                            </textarea>
                        </div>
                    </div>
                    <div class="form-group" id="progressbar" style="display: none;">
                        <center>
                            <progress></progress>
                        </center>
                    </div>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-success btn-label-left" name="register" id="update">
                                <span><i class="fa fa-clock-o"></i></span>
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
</section>