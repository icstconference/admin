
<header class="page-header">
    <h2>Thêm 1 diễn giả mới</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="<?php echo URL;?>artist/artist/">
                    <span>Tất cả diễn giả</span>
                </a>
            </li>
            <li>
                <span>Tạo diễn giả</span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<?php      
    
    $create = $_SESSION['user_permission_create'];
    $delete = $_SESSION['user_permission_delete'];
    $edit = $_SESSION['user_permission_edit'];
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Tạo diễn giả</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" role="form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tên diễn giả</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Tên diễn giả" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="artist_name" name="artist_name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hình ảnh đại diện</label>
                        <div class="col-sm-3">
                            <img src="#" id="eventimg" class="img-responsive imgupload" style="border-radius:3px;border:1px solid #ddd;display:none;margin-bottom:5px;" /> 
                            <input type="file" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="event_img" name="event_img" accept="image/jpg,image/png,image/jpeg,image/gif"  required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Sụ kiện đã tham gia</label>
                        <div class="col-sm-8">
                            <select id="s2_with_tag" data-plugin-selectTwo class="form-control populate" multiple="" class="form-control" style="padding:0px;" name="artist_event[]">
                                <?php
                                if($this->listevent){
                                    foreach($this->listevent as $lb){
                                        ?>
                                        <option value="<?php echo $lb['event_id'];?>"><?php echo $lb['event_name'];?></option>
                                <?php }}?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Mô tả diễn giả</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" placeholder="Mô tả diễn giả" rows="5" title="Tooltip for name" id="wysiwig_full1" name="artist_description"></textarea>
                        </div>
                    </div>
                    <div class="form-group" id="progressbar" style="display: none;">
                        <center>
                            <progress></progress>
                        </center>
                    </div>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-success btn-label-left" name="register" id="submit">
                                <span><i class="fa fa-clock-o"></i></span>
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
</section>