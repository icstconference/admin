<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<header class="page-header">
    <h2>Tất cả sponsor</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <span>Danh sách sponsor</span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<?php                            
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Thêm sponsor mới</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
                <form class="form-horizontal" role="form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tên sponsor</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Tên đối tác" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="partner_name" name="partner_name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Hình ảnh sponsor</label>
                        <div class="col-sm-3">
                            <img src="#" id="partnerimg" class="img-responsive imgupload" style="border-radius:3px;border:1px solid #ddd;display:none;margin-bottom:5px;" /> 
                            <input type="file" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="partner_img" name="partner_img" accept="image/jpg,image/png,image/jpeg,image/gif"  required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Chọn sự kiện</label>
                        <div class="col-sm-8">
                            <select id="s2_with_tag" data-plugin-selectTwo class="form-control populate" name="partner_event">
                                <?php
                                
                                    if($this->listevent){
                                        foreach($this->listevent as $lt){
                                            ?>
                                            <option value="<?php echo $lt['event_id'];?>"><?php echo $lt['event_name'];?></option>
                                <?php }} ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Loại sponsor</label>
                        <div class="col-sm-3">
                            <select id="s2_with_tag" data-plugin-selectTwo class="form-control populate" name="partner_type">
                                <?php

                                    if($this->listcategories){
                                        foreach($this->listcategories as $lt){
                                ?>
                                    <option value="<?php echo $lt['categories_id'];?>"><?php echo $lt['categories_name'];?></option>
                                <?php }} ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Website sponsor</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Website đối tác" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="partner_url" name="partner_url" required>
                        </div>
                    </div>
                    <div class="form-group" id="progressbar" style="display: none;">
                        <center>
                            <progress></progress>
                        </center>
                    </div>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-success btn-label-left" name="register" id="submit">
                                <span><i class="fa fa-clock-o"></i></span>
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
</section>
<?php
    $listpartner = $this->listpartner;
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Tất cả sponsor</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-default">
            <thead>
                    <tr>
                        <th>STT</th>
                        <th>Đối tác</th>
                        <th>Thumbnail</th>
                        <th>Loại sponsor</th>
                        <th>Ngày viết</th>
                        <th>Tùy chỉnh</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($this->listpartner){
                        $i=0;
                        foreach($this->listpartner as $lp){
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i;?></td>
                                <td>
                                    <?php echo $lp['partner_name'];?>
                                </td>
                                <td>
                                    <?php
                                    if(strlen($lp['partner_image']) > 1){
                                        ?>
                                        <img src="<?php echo $lp['partner_image'];?>" />
                                    <?php } else {?>
                                        <img src="<?php echo URL;?>public/img/noneimage.jpg" />
                                    <?php } ?>
                                </td>
                                <td>
                                    <?php
                                        if($this->listcategories){
                                            foreach($this->listcategories as $lt){
                                                if($lt['categories_id'] == $lp['partner_type']){
                                                    echo $lt['categories_name'];
                                                }
                                            }
                                        }
                                    ?>
                                </td>
                                <td>
                                    <?php echo $lp['partner_create_date'];?>
                                </td>
                                <td>
									<?php                                         
										if(strpos($edit[0]['permission_detail'],'partner') || $_SESSION['user_type'] == 1){                                    
									?>
                                    <a class="btn btn-primary" href="<?php echo URL;?>partner/editpartner/<?php echo $lp['partner_id'];?>">Edit</a>
                                    <?php } ?>                                    
									<?php                                         
										if(strpos($delete[0]['permission_detail'],'partner') || $_SESSION['user_type'] == 1){                                    
									?>  
									<a class="btn btn-danger" href="<?php echo URL;?>partner/deletepartner/<?php echo $lp['partner_id'];?>">Delete</a>
									<?php } ?>
								</td>
                            </tr>
                        <?php }} ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>