<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<?php 
    $partnerinfo = $this->partnerinfo;
?>
<header class="page-header">
    <h2>Tất cả sponsor</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="<?php echo URL;?>partner">
                    <span>Danh sách sponsor</span>
                </a>
            </li>
            <li>
                <span><?php echo $partnerinfo[0]['partner_name'];?></span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<?php                            
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];
    $partnerinfo = $this->partnerinfo;
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Chỉnh sửa sponsor</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
                <form class="form-horizontal" role="form" enctype="multipart/form-data">
                    <input type="hidden" name="partner_id" value="<?php echo $partnerinfo[0]['partner_id'];?>">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tên đối tác</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo $partnerinfo[0]['partner_name'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="partner_name" name="partner_name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Hình ảnh đối tác</label>
                        <div class="col-sm-3">
                            <?php
                            if(strlen($partnerinfo[0]['partner_image']) > 1){
                                ?>
                                <img src="<?php echo $partnerinfo[0]['partner_image'];?>" class="img-rounded imgupload" alt="avatar" id="partnerimg">
                            <?php }else{ ?>
                                <img src="<?php echo URL;?>public/img/noneimage.jpg" class="img-rounded imgupload" alt="avatar" id="partnerimg">
                            <?php } ?>
                            <input type="file" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="partner_img" name="partner_img" accept="image/jpg,image/png,image/jpeg,image/gif" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Chọn sự kiện</label>
                        <div class="col-sm-8">
                            <select id="s2_with_tag" data-plugin-selectTwo class="form-control populate" name="partner_event">
                                <?php
                                
                                    if($this->listevent){
                                        foreach($this->listevent as $lt){
                                            if($lt['event_id'] == $partnerinfo[0]['event_id']){
                                ?>
                                    <option value="<?php echo $lt['event_id'];?>" selected><?php echo $lt['event_name'];?></option>
                                <?php }else{ ?>
                                    <option value="<?php echo $lt['event_id'];?>"><?php echo $lt['event_name'];?></option>
                                <?php }}} ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Loại sponsor</label>
                        <div class="col-sm-3">
                            <select id="s2_with_tag" data-plugin-selectTwo class="form-control populate" name="partner_type">
                                <?php

                                    if($this->listcategories){
                                        foreach($this->listcategories as $lt){
                                            if($lt['categories_id'] == $partnerinfo[0]['partner_type']){
                                ?>
                                    <option value="<?php echo $lt['categories_id'];?>" selected><?php echo $lt['categories_name'];?></option>
                                <?php }else{?>
                                    <option value="<?php echo $lt['categories_id'];?>"><?php echo $lt['categories_name'];?></option>
                                <?php }}} ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Website đối tác</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo $partnerinfo[0]['partner_url'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="partner_url" name="partner_url" required>
                        </div>
                    </div>
                    <div class="form-group" id="progressbar" style="display: none;">
                        <center>
                            <progress></progress>
                        </center>
                    </div>
					<?php                                         
						if(strpos($edit[0]['permission_detail'],'partner') || $_SESSION['user_id'] == 1){                                    
					?>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-success btn-label-left" name="register" id="update">
                                <span><i class="fa fa-clock-o"></i></span>
                                Update
                            </button>
                        </div>
                    </div>
					<?php } ?>
                </form>
    </div>
</section>