<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<?php
    $sliderinfo = $this->sliderinfo;
?>
<header class="page-header">
    <h2><?php echo $sliderinfo[0]['slider_id'];?></h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="<?php echo URL;?>slider/">
                    <span>Tất cả slider</span>
                </a>
            </li>
            <li>
                <span><?php echo $sliderinfo[0]['slider_id'];?></span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<?php                            
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Chỉnh sửa slider </h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" role="form" enctype="multipart/form-data">
            <input type="hidden" name="slider_id" value="<?php echo $sliderinfo[0]['slider_id'];?>">
            <div class="form-group">
                <label class="col-sm-2 col-sm-offset-1 control-label">Tiêu đề slide</label>
                <div class="col-sm-8">
                    <textarea class="form-control"  data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="slider_caption" name="slider_title"><?php echo $sliderinfo[0]['slider_title'];?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 col-sm-offset-1 control-label">Tiêu đề slide tiếng anh</label>
                <div class="col-sm-8">
                    <textarea class="form-control"  data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="slider_caption" name="slider_title_english"><?php echo $sliderinfo[0]['slider_title_english'];?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 col-sm-offset-1 control-label">Loại slider</label>
                <div class="col-sm-4">
                    <select data-plugin-selectTwo class="form-control populate" name="slider_type">
                        <?php 
                            if($sliderinfo[0]['slider_type'] == 1){
                        ?>
                        <option value="1" selected>Slider lớn</option>
                        <option value="2">Slider giới thiệu</option>
                        <option value="3">Slider sản phẩm</option>
                        <?php 
                            }elseif($sliderinfo[0]['slider_type'] == 2){
                        ?>
                        <option value="1" >Slider lớn</option>
                        <option value="2" selected>Slider giới thiệu</option>
                        <option value="3">Slider sản phẩm</option>
                        <?php 
                            }elseif($sliderinfo[0]['slider_type'] == 3){
                        ?>
                        <option value="1">Slider lớn</option>
                        <option value="2">Slider giới thiệu</option>
                        <option value="3" selected>Slider sản phẩm</option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 col-sm-offset-1 control-label">Mô tả slide</label>
                <div class="col-sm-8">
                    <textarea class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="slider_caption" name="slider_caption"><?php echo $sliderinfo[0]['slider_caption'];?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 col-sm-offset-1 control-label">Mô tả slide tiếng anh</label>
                <div class="col-sm-8">
                    <textarea class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="slider_caption" name="slider_caption_english"><?php echo $sliderinfo[0]['slider_caption_english'];?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 col-sm-offset-1 control-label">Link dẫn mà slider muốn trỏ tới</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" value="<?php echo $sliderinfo[0]['slider_url'];?> " data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="slider_caption" name="slider_url" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 col-sm-offset-1 control-label">Hình ảnh slider</label>
                <div class="col-sm-6">
                    <?php
                        if(strlen($sliderinfo[0]['slider_img']) > 1){
                    ?>
                        <img src="<?php echo $sliderinfo[0]['slider_img'];?>" class="img-rounded imgupload" alt="avatar" id="primg">
                    <?php }else{ ?>
                        <img src="<?php echo URL;?>public/img/noneimage.jpg" class="img-rounded imgupload" alt="avatar" id="primg">
                    <?php } ?>
                    <input type="file" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="slider_img" name="slider_img" accept="image/jpg,image/png,image/jpeg,image/gif">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 col-sm-offset-1 control-label">Trạng thái slider</label>
                <div class="col-sm-2">
                    <?php
                        if($sliderinfo[0]['slider_status'] == 'on'){
                    ?>
                    <div class="switch switch-success">
                        <input type="checkbox" name="slider_status" data-plugin-ios-switch checked="checked" />
                    </div>
                    <?php }else{?>
                    <div class="switch switch-success">
                        <input type="checkbox" name="slider_status" data-plugin-ios-switch/>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div class="form-group" id="progressbar" style="display: none;">
                <center>
                    <progress></progress>
                </center>
            </div>
            <div class="form-group" style="margin-top: 40px;">
                <div class="col-sm-offset-4 col-sm-2">
                    <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                        <span><i class="fa fa-clock-o txt-danger"></i></span>
                        Cancel
                    </button>
                </div>
                <div class="col-sm-2">
                    <button type="button" class="btn btn-success btn-label-left" name="register" id="update">
                        <span><i class="fa fa-clock-o"></i></span>
                        Submit
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>