<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<header class="page-header">
    <h2>Tất cả tài slider</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <span>Tất cả slider</span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<?php
    $listslider = $this->listslider;
?>
<?php                            
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];
?>
<div class="row" style="margin: 0 0 10px;text-align:right;">
    <a href="<?php echo URL;?>slider/addslider/" class="mb-xs mt-xs mr-xs btn btn-primary">
        <i class="fa fa-file-text-o"></i>&nbsp;Thêm slider mới
    </a>
</div>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Danh sách slider</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-default">
            <thead>
                <tr>
                    <th style="vertical-align:middle;text-align:center;">STT</th>
                    <th style="vertical-align:middle;">Hình ảnh slider</th>
                    <th style="vertical-align:middle;">Loại</th>
                    <th style="vertical-align:middle;">Trạng thái</th>
                    <th style="vertical-align:middle;">Tùy chỉnh</th>
                </tr>
            </thead>
            <tbody>
            <?php
                if($this->listslider){
                    $i=0;
                    foreach($this->listslider as $lp){
                        $i++;
            ?>
                <tr>
                    <td style="vertical-align:middle;text-align:center;"><?php echo $i;?></td>
                    <td style="vertical-align:middle;">
                        <img src="<?php echo $lp['slider_img'];?>" style="width:100px;"/>
                    </td>
                    <td style="vertical-align:middle;">
                        <?php 
                            if($lp['slider_type'] == 1){
                        ?>
                            Slider lớn
                        <?php 
                            }elseif($lp['slider_type'] == 2){
                        ?>
                            Slider sự kiện
                        <?php 
                            }elseif($lp['slider_type'] == 3){
                        ?>
                            Slider artist
                        <?php } ?>
                    </td>
                    <td style="vertical-align:middle;">
                        <?php echo $lp['slider_status'];?>
                    </td>
                    <td style="vertical-align:middle;">
                        <a class="btn btn-primary" href="<?php echo URL;?>slider/editslider/<?php echo $lp['slider_id'];?>"><i class="fa fa-edit"></i></a>
                        <a class="btn btn-danger" href="<?php echo URL;?>slider/deleteslider/<?php echo $lp['slider_id'];?>"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            <?php }} ?>
            </tbody>
        </table>
    </div>
</section>