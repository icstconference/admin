<header class="page-header">
    <h2>Thêm slider mới</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="<?php echo URL;?>slider/">
                    <span>Tất cả slider</span>
                </a>
            </li>
            <li>
                <span>Thêm slider mới</span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<?php                            
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Thêm slider mới</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" role="form" enctype="multipart/form-data">
            <div class="form-group" style="display:none;">
                <label class="col-sm-2 col-sm-offset-1 control-label">Tiêu đề slide</label>
                <div class="col-sm-8">
                    <textarea class="form-control" placeholder="Tiêu đề slide" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="slider_caption" name="slider_title"></textarea>
                </div>
            </div>
            <div class="form-group" style="display:none;">
                <label class="col-sm-2 col-sm-offset-1 control-label">Tiêu đề slide tiếng anh</label>
                <div class="col-sm-8">
                    <textarea class="form-control" placeholder="Tiêu đề slide tiếng anh" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="slider_caption" name="slider_title_english"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 col-sm-offset-1 control-label">Loại slider</label>
                <div class="col-sm-4">
                    <select data-plugin-selectTwo class="form-control populate" name="slider_type">
                        <option value="1">Slider lớn</option>
                        <option value="2">Slider sự kiện</option>
                        <option value="3">Slider artist</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 col-sm-offset-1 control-label">Mô tả slide</label>
                <div class="col-sm-8">
                    <textarea class="form-control" placeholder="Chữ hiển thị" id="wysiwig_full1" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="slider_caption" name="slider_caption"></textarea>
                </div>
            </div>
            <div class="form-group" style="display:none;">
                <label class="col-sm-2 col-sm-offset-1 control-label">Mô tả slide tiếng anh</label>
                <div class="col-sm-8">
                    <textarea class="form-control" placeholder="Chữ hiển thị" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="slider_caption" name="slider_caption_english"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 col-sm-offset-1 control-label">Link dẫn</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" placeholder="Link dẫn " data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="slider_caption" name="slider_url" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 col-sm-offset-1 control-label">Hình ảnh slider</label>
                <div class="col-sm-8">
                    <img src="#" id="sliderimg" class="img-responsive" style="border-radius:3px;border:1px solid #ddd;display:none;" /> 
                    <input type="file" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="slider_img" name="slider_img" accept="image/jpg,image/png,image/jpeg,image/gif">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 col-sm-offset-1 control-label">Trạng thái slider</label>
                <div class="col-sm-2">
                    <div class="switch switch-success">
                        <input type="checkbox" name="slider_status" data-plugin-ios-switch checked="checked" />
                    </div>
                </div>
            </div>
            <div class="form-group" id="progressbar" style="display: none;">
                <center>
                    <progress></progress>
                </center>
            </div>
            <div class="form-group" style="margin-top: 40px;">
                <div class="col-sm-offset-4 col-sm-2">
                    <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                        <span><i class="fa fa-clock-o txt-danger"></i></span>
                        Cancel
                    </button>
                </div>
                <div class="col-sm-2">
                    <button type="button" class="btn btn-success btn-label-left" name="register" id="submit">
                        <span><i class="fa fa-clock-o"></i></span>
                        Submit
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>