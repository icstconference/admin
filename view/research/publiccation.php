<?php 
    function cut_string($str, $len, $more) {
        if ($str == "" || $str == NULL)
            return $str;
        if (is_array($str))
            return $str;
        $str = trim($str);
        if (strlen($str) <= $len)
            return $str;
        $str = substr($str, 0, $len);
        if ($str != "") {
            if (!substr_count($str, " ")) {
                if ($more)
                    $str .= " ...";
                return $str;
            }
            while (strlen($str) && ($str[strlen($str) - 1] != " ")) {
                $str = substr($str, 0, -1);
            }
            $str = substr($str, 0, -1);
            if ($more)
                $str .= " ...";
        }
        return $str;
    }
?>
<style type="text/css">
    .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
        background: #215240;
        color:#fff !important;
        font-family: 'Open Sans', sans-serif;
    }
    .nav-tabs>li>a{
        border:1px solid #dfdfdf;
        border-radius: 8px 8px 0 0;
        margin-right: 0px;
        color: #333;
    }
    .tableft>li{
        list-style: none;
        min-height: 20px;
        border-bottom: 1px solid #dfdfdf;
        padding:10px;
    }
    .tableft a{
        text-decoration: none;
        color:#1c809b;
        font-family: 'Open Sans', sans-serif;
        font-size: 13px;
        font-weight: 600;
    }
    .tableft a:hover{
        text-decoration: none;
        color:#215240;
    }
    .panel{
        border-radius: 8px 8px 0 0;
    }
    .panel-default>.panel-heading{
        background: #1c809b;
        color: #fff;
        border-radius: 8px 8px 0 0;
        text-transform: uppercase;
        font-weight: bold;
    }
    .panel-body a{
        text-decoration: none;
    }
    .panel-body a:hover{
        color: #1c809b;
    }
    .h3danhmuc{
        margin: 5px 0px;
        font-size: 15px;
        color: #666666;
        font-weight: 600;
        margin-left: 10px;
        line-height: 18px;
    }
    .h3danhmuc:hover{
        color: #747474;
    }
    .description{
        margin-left: 10px;
        margin-right: 10px;
        font-size: 12px;
        color: #333;
        margin-top: 5px;
        margin-bottom: 5px;
    }
    .datetime{
        margin-left: 10px;
        font-size: 12px;
        margin-top: 5px;
        margin-bottom: 5px;
        color: #747474;
    }
    .h3email{
        margin-top: 0px;
        font-size: 15px;
        font-weight: 800;
    }
    .inputemail{
        width: 75%;
        height: 36px;
        border-radius: 0px !important;
        padding: 8px !important;
    }
    .btnemail{
        text-align: center;
        margin-left: -5px;
        height: 36px;
        padding: 0 18px;
        background: #1c809b;
        color: #fff;
        border:none;
    }
    .btnemail:hover{
        background: #42768f;
    }
    .selectlink{
        height: 36px;
        width: 99%;
        padding: 5px;
    }
    .h3tuvan{
        font-size: 16px;
        font-weight: 600;
    }
    .fb-like-box, .fb-like-box span, .fb-like-box span iframe[style] { width: 100% !important; }
    .flex-direction-nav .flex-prev {
        position: absolute;
        right: 60px;
        top: -55px;
    }
    .flex-direction-nav .flex-next {
        right: 10px;
        top: -55px;
    }

    #featured .ui-tabs-panel{ 
        width:100% !important; 
        height:360px; 
        background:#999; 
        position:relative;
    }
    #featured .ui-tabs-panel .info {
        position: absolute;
        bottom: 0;
        left: 0;
        height: 110px;
        background: #000;
        opacity: 0.8;
        width: 100%;
        padding: 15px 10px;
        font-family: 'Open Sans', sans-serif;
    }
    #featured .info p {
        margin: 5px 5px;
        font-family: 'Open Sans', sans-serif;
        font-size: 12px;
        line-height: 15px;
        color: #f0f0f0;
    }
    @media screen and (min-width: 768px){
        .carousel-caption {
            right: 0;
            left: 0;
        }
    }
    .carousel-caption {
        position: absolute;
        width: 100%;
        height: 100px;
        bottom: 0px;
        z-index: 10;
        padding: 10px;
        padding-bottom: 10px !Important;
        color: #fff;
        background: #000;
        opacity: 0.8;
        text-align: left;
    }
	.carousel-caption h3:hover{ 
		color:#339966;
	}
    .carousel-caption h3{ 
        font-size:1.2em; 
        font-family: 'Open Sans', sans-serif; 
        color:#fff; 
        padding:5px; 
        margin:0; 
        font-weight:normal;
        overflow:hidden; 
    }
    .carousel-caption p{ 
        margin:0 5px; 
        font-family: 'Open Sans', sans-serif;  
        font-size:12px; 
        line-height:15px; color:#f0f0f0;
    }
    #myindicators {
        top: 20px;            
        left: 25%;
        z-index: 15;
        width: 100%;
        padding-left: 0;
        text-align: right;
        list-style: none;
    }
    #myindicators .active {
        width: 15px;
        height: 15px;
        margin: 0;
        background-color: #fff;
        border: 3px solid #333;
        padding: 6px;
    }
    #myindicators li {
        display: inline-block;
        width: 15px;
        height: 15px;
        margin: 1px;
        text-indent: -999px;
        cursor: pointer;
        background-color: #000;
        background-color: rgba(0,0,0,0);
        border: 2px solid #333;
        border-radius: 10px;
    }
    .dropdown-menu>.active>a, .dropdown-menu>.active>a:focus, .dropdown-menu>.active>a:hover{
        background: #336c57;
        color: #fff;
    }
    .khungsuccess{
        margin: 0;
        border: 1px solid #45866c;
        padding: 20px;
        text-align: center;
        border-radius: 5px;
    }
	.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover{
		background-color: #1c809b;
		border-color: #1c809b;
	}
	.pagination>li>a, .pagination>li>span{
		color: #1c809b;
	}
    .nhansuli{
        padding: 10px 0 10px 20px;
        border-bottom: 1px solid #fff
    }
    .nhansua{
        color: #fff;
        text-transform: uppercase;
    }
    .nhansuli.active a{
        color: #1c809b;
    }
</style>
<?php 
    $categoriesinfo = $this->categoriesinfo;
    $newsinfo = $this->newsinfo;
?>
<div class="col-sm-4 col-xs-12" id="sidebar" style="padding:0 20px;">
    <div class="row" style="margin:0px;">
        <div class="panel panel-default" style="border:none;">
            <div class="panel-heading">
                <img src="<?php echo URL;?>public/img/icontt.png"/>&nbsp;
				<?php 
					if($this->lang == 'vi'){
				?>
                Nghiên cứu
				<?php }else{ ?>
				Research
				<?php } ?>
            </div>
            <div class="panel-body" style="padding: 0px;">
                <ul style="list-style:none;padding-left: 0px;font-size: 15px;background: #85b6cc;">
                    <?php
                        if($this->listcategories){
                            foreach($this->listcategories as $lt){
                                if($lt['news_categories_id'] == $categoriesinfo[0]['news_categories_id']){
									if($this->lang == 'vi'){
				   ?>
                        <li class="nhansuli active">
                            <a href="<?php echo URL;?>category/research/<?php echo $lt['news_categories_url'];?>" class="nhansua"><i class="fa fa-chevron-circle-right"></i>&nbsp;<?php echo $lt['news_categories_name'];?></a>
                        </li>
					<?php }else { ?>
						<li class="nhansuli active">
                            <a href="<?php echo URL;?>category/research/<?php echo $lt['news_categories_url'];?>" class="nhansua"><i class="fa fa-chevron-circle-right"></i>&nbsp;<?php echo $lt['news_categories_name_english'];?></a>
                        </li>
					<?php }}else{
								if($this->lang == 'vi'){
					?>
                        <li class="nhansuli">
                            <a href="<?php echo URL;?>category/research/<?php echo $lt['news_categories_url'];?>" class="nhansua"><i class="fa fa-chevron-circle-right"></i>&nbsp;<?php echo $lt['news_categories_name'];?></a>
                        </li>
                    <?php }else{ ?>
						<li class="nhansuli">
                            <a href="<?php echo URL;?>category/research/<?php echo $lt['news_categories_url'];?>" class="nhansua"><i class="fa fa-chevron-circle-right"></i>&nbsp;<?php echo $lt['news_categories_name_english'];?></a>
                        </li>
					<?php }}}}?>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-8 col-xs-12" style="padding:0 20px;">
    <div class="row" style="margin:0;">
        <div class="panel panel-default" style="border:none;">
            <div class="panel-heading">
                <img src="<?php echo URL;?>public/img/icontt.png"/>&nbsp;
					<?php 
						if($this->lang == 'vi'){
					?>
                    <a href="<?php echo URL;?>category/research/" style="color:#fff;text-decoration:none;">Nghiên cứu</a>
					&nbsp;>&nbsp;
					<a href="<?php echo URL;?>category/research/<?php echo $categoriesinfo[0]['news_categories_url'];?>/" style="color:#fff;text-decoration:none;"><?php echo $categoriesinfo[0]['news_categories_name'];?></a>
					<?php }else { ?>
					<a href="<?php echo URL;?>category/research/" style="color:#fff;text-decoration:none;">Research</a>
					&nbsp;>&nbsp;
					<a href="<?php echo URL;?>category/research/<?php echo $categoriesinfo[0]['news_categories_url'];?>/" style="color:#fff;text-decoration:none;"><?php echo $categoriesinfo[0]['news_categories_name_english'];?></a>
					<?php } ?>
            </div>
            <div class="panel-body" style="padding:0px;border:1px solid #ddd;border-top:none;padding-bottom:20px;">
                <?php
                    $listform1 = $this->listform1;
                ?>
                <div class="row" style="margin-top:10px;">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-content no-padding">
                                <table class="table table-bordered table-striped table-hover table-heading table-datatable" id="datatable-1">
                                    <thead>
                                    <?php 
                                        if($this->lang == 'vi'){
                                    ?>
                                    <tr>
                                        <th style="text-align:center;vertical-align:center;">STT</th>
                                        <th style="text-align:center;vertical-align:center;">Trích dẫn</th>
                                        <th style="text-align:center;vertical-align:center;">Năm&nbsp;<i class="fa fa-arrows-v"></i></th>
                                        <th style="text-align:center;vertical-align:center;">Danh mục</th>
                                    </tr>
                                    <?php }else{ ?>
                                    <tr>
                                        <th style="text-align:center;vertical-align:center;">No</th>
                                        <th style="text-align:center;vertical-align:center;">Citation</th>
                                        <th style="text-align:center;vertical-align:center;">Year&nbsp;<i class="fa fa-arrows-v"></i></th>
                                        <th style="text-align:center;vertical-align:center;">List</th>
                                    </tr>
                                    <?php } ?>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if($this->listform1){
                                        $i=0;
                                        foreach($this->listform1 as $lp){
                                            $i++;
                                            ?>
                                            <tr>
                                                <td><?php echo $i;?></td>
                                                <td><?php echo $lp['form1_author'];?> - <?php echo $lp['form1_title'];?> - <?php echo $lp['form1_journal'];?> - <a href="<?php echo $lp['form1_url'];?>" target="_blank">Link</a> - <a href="<?php echo URL.$lp['form1_file'];?>" download>PDF</a></td>
                                                <td><?php echo $lp['form1_year'];?></td>
                                                <td>
                                                    <?php
                                                    if($this->listformcategories){
                                                        foreach($this->listformcategories as $lt){
                                                            if($lt['form1_categories_id'] == $lp['form1_category_id']){
                                                                ?>
                                                                <?php echo $lt['form1_categories_name'];?>
                                                            <?php }}}?>
                                                </td>
                                            </tr>
                                        <?php }} ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>