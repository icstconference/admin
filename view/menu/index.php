<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
    h2.panel-title{
        font-size: 16px;
    }
</style>
<header class="page-header">
    <h2>Menu</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <span>menu</span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<?php     
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];
?>
<section class="panel">
    <div class="col-sm-4">
        <div class="row" style="margin:10px 0;">
            <header class="panel-heading">
                <h2 class="panel-title">
                    Trang &nbsp;
                    <a data-toggle="collapse" data-parent="#prod" href="#collapse3" class="" aria-expanded="true" style="float: right;">
                        <i class="fa fa-angle-down"></i>                                                   
                    </a>
                </h2>
            </header>
            <div id="collapse3" class="panel-collapse collapse in" aria-expanded="true">
                <form role="form" enctype="multipart/form-data">
                    <div class="panel-body" style="max-height:279px;overflow-y:auto;">
                        <ul style="list-style-type:none;padding-left:0px;">
                            <?php 
                                if($this->listpage){
                                    $i=0;
                                    foreach ($this->listpage as $value) {
                                        $i++;
                            ?>
                            <li style="padding: 3px 0;vertical-align:middle;">
                                <div class="checkbox-custom checkbox-default">
                                    <input type="checkbox" class="page" value="<?php echo $value['page_id'];?>" id="page<?php echo $i;?>" name="pageid[]">
                                    <label for="page<?php echo $i;?>"><?php echo $value['page_name'];?></label>
                                </div>
                            </li>
                            <?php }} ?>
                        </ul>
                    </div>
                    <div class="row" style="margin: 0;background: #fff;padding: 10px 0;">
                        <div class="pull-left" style="padding-left: 10px;">
                            <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" id="selectallpage">Chọn tất cả</button>
                        </div>
                        <div class="pull-right" style="padding-right: 10px;">
                            <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" id="addmenupage">Thêm vào menu</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row" style="margin:10px 0;">
            <header class="panel-heading">
                <h2 class="panel-title">
                    Bài viết &nbsp;
                    <a data-toggle="collapse" data-parent="#prod" href="#collapse1" class="" aria-expanded="true" style="float: right;">
                        <i class="fa fa-angle-down"></i>                                                   
                    </a>
                </h2>
            </header>
            <div id="collapse1" class="panel-collapse collapse" aria-expanded="true">
                <form role="form" enctype="multipart/form-data">
                    <div class="panel-body" style="max-height:279px;overflow-y:auto;">
                        <ul style="list-style-type:none;padding-left:0px;">
                            <?php 
                                if($this->listnews){
                                    $i=0;
                                    foreach ($this->listnews as $value) {
                                        $i++;
                            ?>
                            <li style="padding: 3px 0;vertical-align:middle;">
                                <div class="checkbox-custom checkbox-default">
                                    <input type="checkbox" class="news" id="post<?php echo $i;?>" value="<?php echo $value['news_id'];?>" rel="" name="newsid[]">
                                    <label for="post<?php echo $i;?>"><?php echo $value['news_name'];?></label>
                                </div>
                            </li>
                            <?php }} ?>
                        </ul>
                    </div>
                    <div class="row" style="margin: 0;background: #fff;padding: 10px 0;">
                        <div class="pull-left" style="padding-left: 10px;">
                            <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" id="selectallnew">Chọn tất cả</button>
                        </div>
                        <div class="pull-right" style="padding-right: 10px;">
                            <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" id="addmenunews">Thêm vào menu</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row" style="margin:10px 0;">
            <header class="panel-heading">
                <h2 class="panel-title">
                    Chuyên mục &nbsp;
                    <a data-toggle="collapse" data-parent="#prod" href="#collapse2" class="" aria-expanded="true" style="float: right;">
                        <i class="fa fa-angle-down"></i>                                                   
                    </a>
                </h2>
            </header>
            <div id="collapse2" class="panel-collapse collapse" aria-expanded="true">
                <form role="form" enctype="multipart/form-data">
                    <div class="panel-body" style="max-height:279px;overflow-y:auto;">
                        <ul style="list-style-type:none;padding-left:0px;">
                            <?php 
                                if($this->listcagoriesnews){
                                    $i=0;
                                    foreach ($this->listcagoriesnews as $value) {
                                        $i++;
                            ?>
                            <li style="padding: 2px 0;">
                                <div class="checkbox-custom checkbox-default">
                                    <input type="checkbox" class="categorynews" id="cat<?php echo $i;?>" value="<?php echo $value['news_categories_id'];?>" rel="" name="newscategoriesid[]">
                                    <label for="cat<?php echo $i;?>"><?php echo $value['news_categories_name'];?></label>
                                </div>
                            </li>
                            <?php }} ?>
                        </ul>
                    </div>
                    <div class="row" style="margin: 0;background: #fff;padding: 10px 0;">
                        <div class="pull-left" style="padding-left: 10px;">
                            <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" id="selectallcategory">Chọn tất cả</button>
                        </div>
                        <div class="pull-right" style="padding-right: 10px;">
                            <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" id="addmenucategory">Thêm vào menu</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row" style="margin:10px 0;">
            <header class="panel-heading">
                <h2 class="panel-title">
                    Liên kết tùy chỉnh &nbsp;
                    <a data-toggle="collapse" data-parent="#prod" href="#collapse4" class="" aria-expanded="true" style="float: right;">
                        <i class="fa fa-angle-down"></i>                                                   
                    </a>
                </h2>
            </header>
            <div id="collapse4" class="panel-collapse collapse" aria-expanded="true">
                <div class="panel-body">
                    <form role="form" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Tiêu đề menu</label>
                            <input type="text" placeholder="Tiêu đề menu" name="menu_name" id="menu_name" rel="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Link</label>
                            <input type="text" placeholder="Link menu" name="menu_link" id="menu_link" rel="" class="form-control">
                        </div>
                        <div class="pull-right" style="margin-top:10px;">
                            <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" id="addcustomenu">Thêm vào menu</button>
                        </div>
                    </form>
               </div>
            </div>
        </div>
    </div>
    <div class="col-sm-8" style="padding-right:0px;padding-bottom: 30px;">
        <div class="row" style="margin:10px 0;">
            <header class="panel-heading">
                <h2 class="panel-title">
                    Menu
                </h2>
            </header>
            <div class="panel-body">
                <div class="dd" id="nestable">
                    <ol class="dd-list" id="list">
                        <?php 
                            if($this->listmenu){
                                foreach ($this->listmenu as $value) {
                                    if($value['menuchild']){
                        ?>
                            <li class="dd-item" data-id="<?php echo $value['menu_id'];?>">
                                <div class="dd-handle">
                                    <h2 class="panel-title">
                                        <?php echo $value['menu_name'];?>
                                        <a data-toggle="collapse" data-parent="#prod" href="#menu<?php echo $value['menu_id'];?>" class="" onclick="" aria-expanded="true" style="float: right;text-decoration:none;">
                                                <?php 
                                                    if($value['menu_kind'] == 'news'){
                                                ?>
                                                    Bài viết
                                                <?php }elseif ($value['menu_kind'] == 'page') { ?>
                                                    Trang
                                                <?php }elseif ($value['menu_kind'] == 'news category') { ?>
                                                    Chuyên mục
                                                <?php }elseif ($value['menu_kind'] == 'custom'){?>
                                                    Tùy chỉnh
                                                <?php } ?>
                                            &nbsp;<i class="fa fa-angle-down"></i>                                                
                                        </a>
                                    </h2>
                                </div>
                                <?php 
                                    if($value['menu_kind'] != 'custom'){
                                ?>
                                <div id="menu<?php echo $value['menu_id'];?>" class="panel-collapse collapse" aria-expanded="true" style="margin-top: 5px;">
                                    <div class="panel-body">
                                        <form class="form-horizontal" role="form" enctype="multipart/form-data" id="form<?php echo $value['menu_id'];?>">
                                            <div class="form-group">
                                                <label>Tiêu đề menu</label>
                                                <input type="text" value="<?php echo $value['menu_name'];?>" name="menu_name" id="menu_name<?php echo $value['menu_id'];?>" rel="" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Tiêu đề menu tiếng anh</label>
                                                <input type="text" value="<?php echo $value['menu_name_english'];?>" name="menu_name_english" id="menu_name_english<?php echo $value['menu_id'];?>" rel="" class="form-control">
                                            </div>
                                            <input type="hidden" value="<?php echo $value['menu_id'];?>" name="menu_id">
                                            <div class="pull-right" style="margin-top:10px;">
                                                <?php 
                                                    if($value['menu_kind'] == 'news'){
                                                ?>
                                                   <a href="<?php echo URL;?>news/detail/<?php echo $value['menu_url'];?>" class="mb-xs mt-xs mr-xs btn btn-warning">Link gốc</a>
                                                <?php }elseif ($value['menu_kind'] == 'page') { ?>
                                                    <a href="<?php echo URL;?>page/detail/<?php echo $value['menu_url'];?>" class="mb-xs mt-xs mr-xs btn btn-warning">Link gốc</a>
                                                <?php }elseif ($value['menu_kind'] == 'news category') { ?>
                                                    <a href="<?php echo URL;?>category/news/<?php echo $value['menu_url'];?>" class="mb-xs mt-xs mr-xs btn btn-warning">Link gốc</a>
                                                <?php } ?>
                                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-danger" rel="<?php echo $value['menu_id'];?>" id="delete">Xóa</button>
                                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" rel="<?php echo $value['menu_id'];?>" id="update">Cập nhật</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <?php }else{ ?>
                                <div id="menu<?php echo $value['menu_id'];?>" class="panel-collapse collapse" aria-expanded="true" style="margin-top: 5px;">
                                    <div class="panel-body">
                                        <form class="form-horizontal" role="form" enctype="multipart/form-data" id="form<?php echo $value['menu_id'];?>">
                                            <div class="form-group">
                                                <label>Tiêu đề menu</label>
                                                <input type="text" value="<?php echo $value['menu_name'];?>" name="menu_name" id="menu_name<?php echo $value['menu_id'];?>" rel="" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Tiêu đề menu tiếng anh</label>
                                                <input type="text" value="<?php echo $value['menu_name_english'];?>" name="menu_name_english" id="menu_name_english<?php echo $value['menu_id'];?>" rel="" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Link</label>
                                                <input type="text" value="<?php echo $value['menu_url'];?>" name="menu_url" id="menu_url<?php echo $value['menu_id'];?>" rel="" class="form-control">
                                            </div>
                                            <input type="hidden" value="<?php echo $value['menu_id'];?>" name="menu_id">
                                            <div class="pull-right" style="margin-top:10px;">
                                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-danger" rel="<?php echo $value['menu_id'];?>" id="delete">Xóa</button>
                                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" rel="<?php echo $value['menu_id'];?>" id="updatecustom">Cập nhật</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <?php } ?>
                                <?php 
                                    if($value['menuchild']){
                                        $menuchild = $value['menuchild'];
                                ?>
                                <ol class="dd-list">
                                <?php 
                                        foreach ($menuchild as $key => $value) {
                                ?>
                                    <li class="dd-item" data-id="<?php echo $value['menu_id'];?>">
                                        <div class="dd-handle">
                                            <h2 class="panel-title">
                                                <?php echo $value['menu_name'];?>
                                                <a data-toggle="collapse" data-parent="#prod" href="#menu<?php echo $value['menu_id'];?>" class="" onclick="" aria-expanded="true" style="float: right;text-decoration:none;">
                                                    <?php 
                                                        if($value['menu_kind'] == 'news'){
                                                    ?>
                                                        Bài viết
                                                    <?php }elseif ($value['menu_kind'] == 'page') { ?>
                                                        Trang
                                                    <?php }elseif ($value['menu_kind'] == 'news category') { ?>
                                                        Chuyên mục
                                                    <?php }elseif ($value['menu_kind'] == 'custom'){?>
                                                        Tùy chỉnh
                                                    <?php } ?>
                                                    &nbsp;<i class="fa fa-angle-down"></i>                                                
                                                </a>
                                            </h2>
                                        </div>
                                        <?php 
                                    if($value['menu_kind'] != 'custom'){
                                ?>
                                <div id="menu<?php echo $value['menu_id'];?>" class="panel-collapse collapse" aria-expanded="true" style="margin-top: 5px;">
                                    <div class="panel-body">
                                        <form class="form-horizontal" role="form" enctype="multipart/form-data" id="form<?php echo $value['menu_id'];?>">
                                            <div class="form-group">
                                                <label>Tiêu đề menu</label>
                                                <input type="text" value="<?php echo $value['menu_name'];?>" name="menu_name" id="menu_name<?php echo $value['menu_id'];?>" rel="" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Tiêu đề menu tiếng anh</label>
                                                <input type="text" value="<?php echo $value['menu_name_english'];?>" name="menu_name_english" id="menu_name_english<?php echo $value['menu_id'];?>" rel="" class="form-control">
                                            </div>
                                            <input type="hidden" value="<?php echo $value['menu_id'];?>" name="menu_id">
                                            <div class="pull-right" style="margin-top:10px;">
                                                <?php 
                                                    if($value['menu_kind'] == 'news'){
                                                ?>
                                                   <a href="<?php echo URL;?>news/detail/<?php echo $value['menu_url'];?>" class="mb-xs mt-xs mr-xs btn btn-warning">Link gốc</a>
                                                <?php }elseif ($value['menu_kind'] == 'page') { ?>
                                                    <a href="<?php echo URL;?>page/detail/<?php echo $value['menu_url'];?>" class="mb-xs mt-xs mr-xs btn btn-warning">Link gốc</a>
                                                <?php }elseif ($value['menu_kind'] == 'news category') { ?>
                                                    <a href="<?php echo URL;?>category/news/<?php echo $value['menu_url'];?>" class="mb-xs mt-xs mr-xs btn btn-warning">Link gốc</a>
                                                <?php } ?>
                                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-danger" rel="<?php echo $value['menu_id'];?>" id="delete">Xóa</button>
                                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" rel="<?php echo $value['menu_id'];?>" id="update">Cập nhật</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <?php }else{ ?>
                                <div id="menu<?php echo $value['menu_id'];?>" class="panel-collapse collapse" aria-expanded="true" style="margin-top: 5px;">
                                    <div class="panel-body">
                                        <form class="form-horizontal" role="form" enctype="multipart/form-data" id="form<?php echo $value['menu_id'];?>">
                                            <div class="form-group">
                                                <label>Tiêu đề menu</label>
                                                <input type="text" value="<?php echo $value['menu_name'];?>" name="menu_name" id="menu_name<?php echo $value['menu_id'];?>" rel="" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Tiêu đề menu tiếng anh</label>
                                                <input type="text" value="<?php echo $value['menu_name_english'];?>" name="menu_name_english" id="menu_name_english<?php echo $value['menu_id'];?>" rel="" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Link</label>
                                                <input type="text" value="<?php echo $value['menu_url'];?>" name="menu_url" id="menu_url<?php echo $value['menu_id'];?>" rel="" class="form-control">
                                            </div>
                                            <input type="hidden" value="<?php echo $value['menu_id'];?>" name="menu_id">
                                            <div class="pull-right" style="margin-top:10px;">
                                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-danger" rel="<?php echo $value['menu_id'];?>" id="delete">Xóa</button>
                                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" rel="<?php echo $value['menu_id'];?>" id="updatecustom">Cập nhật</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <?php } ?>
                                    </li>
                                <?php } ?>
                                </ol>
                                <?php } ?>
                            </li>
                            <?php }else{ ?>
                            <li class="dd-item" data-id="<?php echo $value['menu_id'];?>">
                                <div class="dd-handle">
                                    <h2 class="panel-title">
                                        <?php echo $value['menu_name'];?>
                                        <a data-toggle="collapse" data-parent="#prod" href="#menu<?php echo $value['menu_id'];?>" class="" onclick="" aria-expanded="true" style="float: right;text-decoration:none;">
                                            <?php 
                                                    if($value['menu_kind'] == 'news'){
                                                ?>
                                                    Bài viết
                                                <?php }elseif ($value['menu_kind'] == 'page') { ?>
                                                    Trang
                                                <?php }elseif ($value['menu_kind'] == 'news category') { ?>
                                                    Chuyên mục
                                                <?php }elseif ($value['menu_kind'] == 'custom'){?>
                                                    Tùy chỉnh
                                                <?php } ?>
                                            &nbsp;<i class="fa fa-angle-down"></i>                                                
                                        </a>
                                    </h2>
                                </div>
                                <?php 
                                    if($value['menu_kind'] != 'custom'){
                                ?>
                                <div id="menu<?php echo $value['menu_id'];?>" class="panel-collapse collapse" aria-expanded="true" style="margin-top: 5px;">
                                    <div class="panel-body">
                                        <form class="form-horizontal" role="form" enctype="multipart/form-data" id="form<?php echo $value['menu_id'];?>">
                                            <div class="form-group">
                                                <label>Tiêu đề menu</label>
                                                <input type="text" value="<?php echo $value['menu_name'];?>" name="menu_name" id="menu_name<?php echo $value['menu_id'];?>" rel="" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Tiêu đề menu tiếng anh</label>
                                                <input type="text" value="<?php echo $value['menu_name_english'];?>" name="menu_name_english" id="menu_name_english<?php echo $value['menu_id'];?>" rel="" class="form-control">
                                            </div>
                                            <input type="hidden" value="<?php echo $value['menu_id'];?>" name="menu_id">
                                            <div class="pull-right" style="margin-top:10px;">
                                                <?php 
                                                    if($value['menu_kind'] == 'news'){
                                                ?>
                                                   <a href="<?php echo URL;?>news/detail/<?php echo $value['menu_url'];?>" class="mb-xs mt-xs mr-xs btn btn-warning">Link gốc</a>
                                                <?php }elseif ($value['menu_kind'] == 'page') { ?>
                                                    <a href="<?php echo URL;?>page/detail/<?php echo $value['menu_url'];?>" class="mb-xs mt-xs mr-xs btn btn-warning">Link gốc</a>
                                                <?php }elseif ($value['menu_kind'] == 'news category') { ?>
                                                    <a href="<?php echo URL;?>category/news/<?php echo $value['menu_url'];?>" class="mb-xs mt-xs mr-xs btn btn-warning">Link gốc</a>
                                                <?php } ?>
                                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-danger" rel="<?php echo $value['menu_id'];?>" id="delete">Xóa</button>
                                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" rel="<?php echo $value['menu_id'];?>" id="update">Cập nhật</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <?php }else{ ?>
                                <div id="menu<?php echo $value['menu_id'];?>" class="panel-collapse collapse" aria-expanded="true" style="margin-top: 5px;">
                                    <div class="panel-body">
                                        <form class="form-horizontal" role="form" enctype="multipart/form-data" id="form<?php echo $value['menu_id'];?>">
                                            <div class="form-group">
                                                <label>Tiêu đề menu</label>
                                                <input type="text" value="<?php echo $value['menu_name'];?>" name="menu_name" id="menu_name<?php echo $value['menu_id'];?>" rel="" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Tiêu đề menu tiếng anh</label>
                                                <input type="text" value="<?php echo $value['menu_name_english'];?>" name="menu_name_english" id="menu_name_english<?php echo $value['menu_id'];?>" rel="" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Link</label>
                                                <input type="text" value="<?php echo $value['menu_url'];?>" name="menu_url" id="menu_url<?php echo $value['menu_id'];?>" rel="" class="form-control">
                                            </div>
                                            <input type="hidden" value="<?php echo $value['menu_id'];?>" name="menu_id">
                                            <div class="pull-right" style="margin-top:10px;">
                                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-danger" rel="<?php echo $value['menu_id'];?>" id="delete">Xóa</button>
                                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" rel="<?php echo $value['menu_id'];?>" id="updatecustom">Cập nhật</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <?php } ?>
                            </li>
                        <?php }}} ?>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>