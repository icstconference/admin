<?php 
    $actual_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER[REQUEST_URI];
    function cut_string($str, $len, $more) {
        if ($str == "" || $str == NULL)
            return $str;
        if (is_array($str))
            return $str;
        $str = trim($str);
        if (strlen($str) <= $len)
            return $str;
        $str = substr($str, 0, $len);
        if ($str != "") {
            if (!substr_count($str, " ")) {
                if ($more)
                    $str .= " ...";
                return $str;
            }
            while (strlen($str) && ($str[strlen($str) - 1] != " ")) {
                $str = substr($str, 0, -1);
            }
            $str = substr($str, 0, -1);
            if ($more)
                $str .= " ...";
        }
        return $str;
    }

    $newsinfo = $this->newsinfo;
    $catinfo = $this->catinfo;
?>
<style type="text/css">
	.homepage-module img{
		width: 100% !important;
	}
	#main-contents p {
        font-size: 20px;
        line-height: 32px;
		font-family: 'helveticaneuelight' !important;
    }
    .celebrity_or_video .celebrity_or_video_text_area .rtf, .celebrity_or_video h1, .celebrity_or_video h2, .celebrity_or_video h3, .celebrity_or_video h4, .celebrity_or_video p {
        color:#63544d;
    }
	
	#main-contents span{
		font-size: 20px !important;
		line-height: 32px;
		font-family: 'helveticaneuelight' !important;
	}
</style>
<div id="main" role="main" class="single-col" style="padding-bottom:20px;min-height:0px;">		
	<div id="main-contents" style="padding: 10px 15px;">
		<ul class="breadcrumb" style="margin-left: 0;">
            <?php 
                if($this->lang == 'vi'){
            ?>
                <li style="display:inline;">
                    <a href="<?php echo URL;?>">
                        Trang chủ
                    </a> > 
                </li>
                <li style="display:inline;">
                    <a href="<?php echo URL;?>category/news/<?php echo $catinfo[0]['news_categories_url'];?>">
                        <?php echo $catinfo[0]['news_categories_name'];?>
                    </a>
                </li>
            <?php }else{ ?>
                <li style="display:inline;">
                    <a href="<?php echo URL;?>">
                        Home
                    </a> > 
                </li>
                <li style="display:inline;">
                    <a href="<?php echo URL;?>category/news/<?php echo $catinfo[0]['news_categories_url'];?>">
                        <?php echo $catinfo[0]['news_categories_name_english'];?>
                    </a>
                </li>
            <?php } ?>
        </ul>
        <div class="homepage-module" style="border-top:none;padding-top: 0px;">
            <div style="padding-top: 0px; margin-top: 0px; padding-bottom: 0px; margin-bottom: 0px;" class="celebrity_or_video">
                <div style="margin-top: 15px;color: #63544d;font-weight:300;line-height:24px;font-size:20px !important;">
                        <?php 
                            if($this->lang == 'vi'){
                        ?>
                        <h3 style="font-size: 20px;color: #63544d;font-weight: 500;">
                            <?php echo $newsinfo[0]['news_name'];?>
                        </h3>
                        <?php }else{ ?>
                        <h3 style="font-size: 20px;color: #63544d;font-weight: 500;">
                            <?php echo $newsinfo[0]['news_name_english'];?>
                        </h3>
                        <?php } ?>
                        <?php 
                            if($this->lang == 'vi'){
                        ?>
                            <?php echo $newsinfo[0]['news_description'];?>
                        <?php }else{ ?>
                            <?php echo $newsinfo[0]['news_description_english'];?>
                        <?php } ?>
                        <?php 
                            if($this->listrelatenews){
                            	$i=0;
                        ?>
                        <h3 style="font-size: 20px;color: #63544d;font-weight: 500;margin-bottom: 0px;margin-top: 15px;padding-bottom: 5px;">
							<?php 
								if($this->lang == 'vi'){
							?>
							Các tin khác
							<?php }else{ ?>
							Related news
							<?php } ?>
                        </h3> 
                        <ul style="list-style-type: disc;margin-left: 20px;border-top: 1px dotted #dfdfdf;margin-top: 0px;padding-top: 10px;">
                            <?php foreach ($this->listrelatenews as $value) { 
                            	$i++;
                                if($value['news_id'] != $newsinfo[0]['news_id']){
                                	if($i<=5){
                            ?>
                            <li>
                                <a href="<?php echo URL.'news/detail/'.$value['news_url'];?>" style="color:#c39122;font-size: 15px;">
                                    &nbsp;&nbsp;
                                    <?php 
                                        if($this->lang == 'vi'){
                                    ?>
                                        <?php echo $value['news_name'];?>
                                    <?php }else{ ?>
                                        <?php echo $value['news_name_english'];?>
                                    <?php } ?>
                                </a>
                            </li>
                            <?php }}} ?>
                        </ul>
                        <?php } ?>
                    </div>
            </div>
        </div>
	</div>
</div>