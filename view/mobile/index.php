<div id="main" role="main">
			<div id="home-carousel-wrap" style="">
				<section id="pager-wrap">
		            <div id="pager-wrap-pos"><!-- inline style applied to the .slide-caption (below) get translated to #pager-wrap-pos (here)-->
		                <div id="pager-caption"></div>
		                <span id='pager'></span>
		            </div>
		        </section>
	       
				<ul id="home-carousel">
	                <?php 
		                if($this->listslider1){
		                    foreach ($this->listslider1 as $value) {
		            ?>
		            <li>
		                <section>
		                     <img src="<?php echo $value['slider_img'];?>" alt="<?php echo $value['slider_title'];?>" />
		                        <div class="slide-caption slide-caption-1">
									<?php 
										if($this->lang == 'vi'){
									?>
		                            <h3 ><a href="<?php echo $value['slider_url'];?>" style="font-family:UVFDidot !important;"><br><br><br><br><?php echo $value['slider_title'];?><span>&nbsp;</span></a></h3>
		                            <h4 style="font-weight:300;font-family:helveticaneuelight !important;"><?php echo $value['slider_caption'];?></h4>
									<?php }else { ?>
									<h3 ><a href="<?php echo $value['slider_url'];?>" style="font-family:UVFDidot !important;"><br><br><br><br><?php echo $value['slider_title_english'];?><span>&nbsp;</span></a></h3>
		                            <h4 style="font-weight:300;font-family:helveticaneuelight !important;"><?php echo $value['slider_caption_english'];?></h4>
									<?php } ?>
								</div>
		                </section>
		            </li>
		            <?php }} ?>
	            </ul>
		    </div>
			
			<div class="home_menu">
		    	<h1 style="font-size: 26px;font-family: UVFDidot !important;">
		    		TỔNG QUAN PVGAS D - MỤC TIÊU - TẦM NHÌN
		    	</h1>
		    </div>


	        <div class="home_menu">
		        <a href="<?php echo URL;?>category/news/">
		            <span style="font-weight:300;font-family:'helveticaneuelight';">
						<?php 
							if($this->lang == 'vi'){
						?>
							TIN TỨC
						<?php }else{ ?>
							NEWS
						<?php } ?>
					</span>
		        </a>
		        <a href="<?php echo URL;?>page/detail/he-thong-quan-ly-14" class="right">
		            <span style="font-weight:300;font-family:'helveticaneuelight';">
						<?php 
							if($this->lang == 'vi'){
						?>
							AN TOÀN
						<?php }else{ ?>
							SAFETY
						<?php } ?>
					</span>
		        </a>
		        <a href="<?php echo URL;?>page/detail/san-pham-chinh-10">
		            <span style="font-weight:300;font-family:'helveticaneuelight';">
						<?php 
							if($this->lang == 'vi'){
						?>
							SẢN PHẨM
						<?php }else{ ?>
							PRODUCTS
						<?php } ?>
					</span>
		        </a>
		        <a href="<?php echo URL;?>category/file/" class="right">
		            <span style="font-weight:300;font-family:'helveticaneuelight';">
						<?php 
							if($this->lang == 'vi'){
						?>
							QUAN HỆ CỔ ĐÔNG
						<?php }else{ ?>
							SHAREHOLDER RELATION
						<?php } ?>
					</span>
		        </a>
		        <a href="<?php echo URL;?>category/image/">
		            <span style="font-weight:300;font-family:'helveticaneuelight';">
						<?php 
							if($this->lang == 'vi'){
						?>
							HÌNH ẢNH
						<?php }else{ ?>
							GALLERY
						<?php } ?>
					</span>
		        </a>
		        <a href="<?php echo URL;?>contact/" class="right">
		            <span style="font-weight:300;font-family:'helveticaneuelight';">
						<?php 
							if($this->lang == 'vi'){
						?>
							LIÊN HỆ
						<?php }else{ ?>
							CONTACT
						<?php } ?>
					</span>
		        </a>
	        </div>
	    </div>