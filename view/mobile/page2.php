<?php 
    $actual_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER[REQUEST_URI];
    function cut_string($str, $len, $more) {
        if ($str == "" || $str == NULL)
            return $str;
        if (is_array($str))
            return $str;
        $str = trim($str);
        if (strlen($str) <= $len)
            return $str;
        $str = substr($str, 0, $len);
        if ($str != "") {
            if (!substr_count($str, " ")) {
                if ($more)
                    $str .= " ...";
                return $str;
            }
            while (strlen($str) && ($str[strlen($str) - 1] != " ")) {
                $str = substr($str, 0, -1);
            }
            $str = substr($str, 0, -1);
            if ($more)
                $str .= " ...";
        }
        return $str;
    }

    $pageinfo = $this->pageinfo;
?>
<style type="text/css">
	.home_menu a{
		font-size: 20px;
	}
	.ui-tabs .ui-tabs-nav .ui-tabs-anchor {
		font-size: 14px;
		padding: 8px;
	}
	.ui-tabs .ui-tabs-panel {
	    padding: 10px 0;
	}
	#main-contents tr td:first-child{
		width: 65%;
		color: #65554e;
	}
	#main-contents td {
	    font-size: 15px;
	    font-weight: 300;
	    color: #333;
	}
	.filetitle{
		font-family: 'helveticaneuelight' !important;
	}
	.filedate{
		font-family: 'helveticaneuelight' !important;
	}
	.filedownload{
		font-family: 'helveticaneuelight' !important;
	}
	.homepage-module img{
		width: 100% !important;
	}
	#main-contents p {
        font-size: 20px;
        line-height: 32px;
		font-family: 'helveticaneuelight' !important;
    }
	#main-contents span{
		font-size: 20px !important;
		line-height: 32px;
		font-family: 'helveticaneuelight' !important;
	}
    .celebrity_or_video .celebrity_or_video_text_area .rtf, .celebrity_or_video h1, .celebrity_or_video h2, .celebrity_or_video h3, .celebrity_or_video h4, .celebrity_or_video p {
        color:#63544d;
    }
    .home_menu a.active{
    	background: #63544d;
    	color: #fff !important;
    }
</style>
<div id="main" role="main">
	<div id="main-contents" style="padding:20px 15px 0;">
		<?php 
            if($this->lang == 'vi'){
        ?>
            <h3 style="font-size: 20px;color: #63544d;font-family: 'helveticaneuelight' !important;font-weight: 400;text-transform:uppercase;margin-bottom: 5px;">
                SẢN PHẨM
            </h3>
        <?php }else{ ?>
            <h3 style="font-size: 20px;color: #63544d;font-family: 'helveticaneuelight' !important;font-weight: 400;text-transform:uppercase;margin-bottom: 5px;">
                PRODUCTS
            </h3>
        <?php } ?>
    </div>
	<div class="home_menu">
		<?php 
			if($pageinfo[0]['page_id'] == '10'){
		?>
			<a class="active" href="<?php echo URL;?>page/detail/san-pham-chinh-10/">
			    <span style="font-weight:300;font-family: 'helveticaneuelight' !important;">
					<?php 
						if($this->lang == 'vi'){
					?>
						SẢN PHẨM CHÍNH
					<?php }else{ ?>
						THE MAIN PRODUCTION
					<?php } ?>
				</span>
			</a>
			<a href="<?php echo URL;?>page/detail/he-thong-phan-phoi-11/" class="right">
			    <span style="font-weight:300;font-family: 'helveticaneuelight' !important;">
					<?php 
						if($this->lang == 'vi'){
					?>
						HỆ THỐNG PHÂN PHỐI
					<?php }else{ ?>
						HỆ THỐNG PHÂN PHỐI
					<?php } ?>
				</span>
			</a>
			<a href="<?php echo URL;?>page/detail/so-do-nguon-khi-12/">
				<span style="font-weight:300;font-family: 'helveticaneuelight' !important;">
					<?php 
						if($this->lang == 'vi'){
					?>
						SƠ ĐỒ NGUỒN KHÍ
					<?php }else{ ?>
						GAS SOURCE
					<?php } ?>
				</span>
			</a>
			<a href="<?php echo URL;?>page/detail/video-clip-13/" class="right">
			    <span style="font-weight:300;font-family: 'helveticaneuelight' !important;">
					<?php 
						if($this->lang == 'vi'){
					?>
						VIDEO CLIP
					<?php }else{ ?>
						VIDEO CLIP
					<?php } ?>
				</span>
			</a>
		<?php }elseif($pageinfo[0]['page_id'] == '11'){ ?>
			<a href="<?php echo URL;?>page/detail/san-pham-chinh-10/">
			    <span style="font-weight:300;font-family: 'helveticaneuelight' !important;">
					<?php 
						if($this->lang == 'vi'){
					?>
						SẢN PHẨM CHÍNH
					<?php }else{ ?>
						THE MAIN PRODUCTION
					<?php } ?>
				</span>
			</a>
			<a class="active" href="<?php echo URL;?>page/detail/he-thong-phan-phoi-11/" class="right">
			    <span style="font-weight:300;font-family: 'helveticaneuelight' !important;">
					<?php 
						if($this->lang == 'vi'){
					?>
						HỆ THỐNG PHÂN PHỐI
					<?php }else{ ?>
						HỆ THỐNG PHÂN PHỐI
					<?php } ?>
				</span>
			</a>
			<a href="<?php echo URL;?>page/detail/so-do-nguon-khi-12/">
				<span style="font-weight:300;font-family: 'helveticaneuelight' !important;">
					<?php 
						if($this->lang == 'vi'){
					?>
						SƠ ĐỒ NGUỒN KHÍ
					<?php }else{ ?>
						GAS SOURCE
					<?php } ?>
				</span>
			</a>
			<a href="<?php echo URL;?>page/detail/video-clip-13/" class="right">
			    <span style="font-weight:300;font-family: 'helveticaneuelight' !important;">
					<?php 
						if($this->lang == 'vi'){
					?>
						VIDEO CLIP
					<?php }else{ ?>
						VIDEO CLIP
					<?php } ?>
				</span>
			</a>
		<?php }elseif($pageinfo[0]['page_id'] == '12'){ ?>
			<a href="<?php echo URL;?>page/detail/san-pham-chinh-10/">
			    <span style="font-weight:300;font-family: 'helveticaneuelight' !important;">
					<?php 
						if($this->lang == 'vi'){
					?>
						SẢN PHẨM CHÍNH
					<?php }else{ ?>
						THE MAIN PRODUCTION
					<?php } ?>
				</span>
			</a>
			<a href="<?php echo URL;?>page/detail/he-thong-phan-phoi-11/" class="right">
			    <span style="font-weight:300;font-family: 'helveticaneuelight' !important;">
					<?php 
						if($this->lang == 'vi'){
					?>
						HỆ THỐNG PHÂN PHỐI
					<?php }else{ ?>
						HỆ THỐNG PHÂN PHỐI
					<?php } ?>
				</span>
			</a>
			<a class="active" href="<?php echo URL;?>page/detail/so-do-nguon-khi-12/">
				<span style="font-weight:300;font-family: 'helveticaneuelight' !important;">
					<?php 
						if($this->lang == 'vi'){
					?>
						SƠ ĐỒ NGUỒN KHÍ
					<?php }else{ ?>
						GAS SOURCE
					<?php } ?>
				</span>
			</a>
			<a href="<?php echo URL;?>page/detail/video-clip-13/" class="right">
			    <span style="font-weight:300;font-family: 'helveticaneuelight' !important;">
					<?php 
						if($this->lang == 'vi'){
					?>
						VIDEO CLIP
					<?php }else{ ?>
						VIDEO CLIP
					<?php } ?>
				</span>
			</a>
		<?php }elseif($pageinfo[0]['page_id'] == '13'){ ?>
			<a href="<?php echo URL;?>page/detail/san-pham-chinh-10/">
			    <span style="font-weight:300;font-family: 'helveticaneuelight' !important;">
					<?php 
						if($this->lang == 'vi'){
					?>
						SẢN PHẨM CHÍNH
					<?php }else{ ?>
						THE MAIN PRODUCTION
					<?php } ?>
				</span>
			</a>
			<a href="<?php echo URL;?>page/detail/he-thong-phan-phoi-11/" class="right">
			    <span style="font-weight:300;font-family: 'helveticaneuelight' !important;">
					<?php 
						if($this->lang == 'vi'){
					?>
						HỆ THỐNG PHÂN PHỐI
					<?php }else{ ?>
						HỆ THỐNG PHÂN PHỐI
					<?php } ?>
				</span>
			</a>
			<a href="<?php echo URL;?>page/detail/so-do-nguon-khi-12/">
				<span style="font-weight:300;font-family: 'helveticaneuelight' !important;">
					<?php 
						if($this->lang == 'vi'){
					?>
						SƠ ĐỒ NGUỒN KHÍ
					<?php }else{ ?>
						GAS SOURCE
					<?php } ?>
				</span>
			</a>
			<a class="active" href="<?php echo URL;?>page/detail/video-clip-13/" class="right">
			    <span style="font-weight:300;font-family: 'helveticaneuelight' !important;">
					<?php 
						if($this->lang == 'vi'){
					?>
						VIDEO CLIP
					<?php }else{ ?>
						VIDEO CLIP
					<?php } ?>
				</span>
			</a>
		<?php } ?>
	</div>
	<div id="main-contents" style="padding:0 15px 20px;">
		<div class="homepage-module" style="padding-top: 0px;">
            <div style="padding-top: 0px; margin-top: 0px; padding-bottom: 0px; margin-bottom: 0px;" class="celebrity_or_video">
            	
                        <?php 
                            if($this->lang == 'vi'){
                        ?>
                            <?php echo $pageinfo[0]['page_description'];?>
                        <?php }else{ ?>
                            <?php echo $pageinfo[0]['page_description_english'];?>
                        <?php } ?>
            </div>
        </div>
    </div>
</div>