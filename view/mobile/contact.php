<style type="text/css">
	.google-maps {
        position: relative;
        height: 0;
    }
    .google-maps iframe {
        top: 0;
        left: 0;
        width: 100% !important;
        z-index:0;
        border:1px solid #65554e;
    }
	.btnemail{
        text-align: center;
        margin-left: -5px;
        height: 36px;
        padding: 0 18px;
        background: #1c809b;
        color: #fff;
        border:none;
    }
    .btnemail:hover{
        background: #42768f;
    }
</style>
<div id="main" role="main" class="single-col" style="padding-bottom:20px;min-height:0px;">		
	<div id="main-contents" style="padding:20px 15px 0;">
		<?php 
            if($this->lang == 'vi'){
        ?>
            <h3 style="font-family: 'helveticaneuelight' !important;font-size: 20px;color: #63544d;font-weight: 500;text-transform:uppercase;margin-bottom: 10px;">
                Liên hệ
            </h3>
        <?php }else{ ?>
            <h3 style="font-family: 'helveticaneuelight' !important;font-size: 20px;color: #63544d;font-weight: 500;text-transform:uppercase;margin-bottom: 10px;">
                Contact
            </h3>
        <?php } ?>
		<?php 
            if($this->listcontact){
                foreach ($this->listcontact as $value) {
        ?>
        <?php 
            if($this->lang == 'vi'){
        ?>
        <table style="font-family: 'helveticaneuelight' !important;margin-bottom: 15px;">
            <tr>
                <th style="width:8%;border-bottom:none;vertical-align:middle;"><i class="fa fa-building"></i></th>
                <td style="font-family: 'helveticaneuelight' !important;">Chi nhánh:&nbsp;<?php echo $value['contact_name'];?></td>
            </tr>
            <tr>
                <th style="width:8%;border-bottom:none;vertical-align:middle;"><i class="fa fa-map-marker"></i></th>
                <td style="font-family: 'helveticaneuelight' !important;">Địa chỉ:&nbsp;<?php echo $value['contact_address'];?></td>
            </tr>
            <tr>
                <th style="width:8%;border-bottom:none;vertical-align:middle;"><i class="fa fa-phone"></i></th>
                <td style="font-family: 'helveticaneuelight' !important;">Số điện thoại:&nbsp;<?php echo $value['contact_phone'];?></td>
            </tr>
            <tr>
                <th style="width:8%;border-bottom:none;vertical-align:middle;"><i class="fa fa-fax"></i></th>
                <td style="font-family: 'helveticaneuelight' !important;">Fax:&nbsp;<?php echo $value['contact_fax'];?></td>
            </tr>
            <tr>
                <th style="width:8%;border-bottom:none;vertical-align:middle;"><i class="fa fa-envelope"></i></th>
                <td style="font-family: 'helveticaneuelight' !important;">Email:&nbsp;<?php echo $value['contact_email'];?></td>
            </tr>
        </table>
        <?php }else{ ?>
        <table>
            <tr>
                <th style="width:5%;border-bottom:none;vertical-align:middle;"><i class="fa fa-building"></i></th>
                <td style="font-family: 'helveticaneuelight' !important;">Branch:&nbsp;<?php echo $value['contact_name_english'];?></td>
            </tr>
            <tr>
                <th style="width:5%;border-bottom:none;vertical-align:middle;"><i class="fa fa-map-marker"></i></th>
                <td style="font-family: 'helveticaneuelight' !important;">Address:&nbsp;<?php echo $value['contact_address_english'];?></td>
            </tr>
            <tr>
                <th style="width:5%;border-bottom:none;vertical-align:middle;"><i class="fa fa-phone"></i></th>
                <td style="font-family: 'helveticaneuelight' !important;">Phone:&nbsp;<?php echo $value['contact_phone'];?></td>
            </tr>
            <tr>
                <th style="width:5%;border-bottom:none;vertical-align:middle;"><i class="fa fa-fax"></i></th>
                <td style="font-family: 'helveticaneuelight' !important;">Fax:&nbsp;<?php echo $value['contact_fax'];?></td>
            </tr>
           	<tr>
                <th style="width:5%;border-bottom:none;vertical-align:middle;"><i class="fa fa-envelope"></i></th>
                <td style="font-family: 'helveticaneuelight' !important;">Email:&nbsp;<?php echo $value['contact_email'];?></td>
            </tr>
        </table>
        <?php } ?>
        <?php }} ?>
		<div class="row" style="margin:0 0 20px 0;height:450px;">
            <div class="google-maps">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.921392358911!2d106.69882981441283!3d10.740541762792859!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752f9f466092ab%3A0x1e5e9741278e9f92!2zNjczIE5ndXnhu4VuIEjhu691IFRo4buNLCBUw6JuIEjGsG5nLCBRdeG6rW4gNywgSOG7kyBDaMOtIE1pbmgsIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1460011933985" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
		<div class="row" style="margin:10px 0;">
				<?php 
                        if($this->lang == 'vi'){
                    ?>
                    <h3 style="font-size: 20px;color: #63544d;font-weight: 500;margin-left: 0;text-transform:uppercase;">
                        Gởi yêu cầu hỗ trợ
                    </h3>
                    <?php }else{ ?>
                    <h3 style="font-size: 20px;color: #63544d;font-weight: 500;margin-left: 0;text-transform:uppercase;">
                        Send a reply
                    </h3>
                    <?php } ?>
                    <form action="<?php echo URL.$this->lang;?>/contact/addcontact/" method="post">
                        <?php 
                            if($this->lang == 'vi'){
                        ?>
                        <input type="text" name="your-name" style="width:100%;border:1px solid #dfdfdf;height:35px;margin-right:22px;padding: 5px;margin-bottom:10px;background: #fff;" placeholder="Họ và tên" required>
                        <input type="email" name="your-email" style="width:55%;border:1px solid #dfdfdf;height:35px;margin-right:22px;padding: 5px;margin-bottom:10px;background: #fff;" placeholder="Email" required>
                        <input type="text" name="your-phone" style="width:40.4%;border:1px solid #dfdfdf;height:35px;padding: 5px;margin-bottom:10px;background: #fff;" placeholder="Số điện thoại" required>
                        <textarea name="your-message" style="width:100%;border:1px solid #dfdfdf;padding: 5px;height: 120px;resize: none;background: #fff;" placeholder="Nội dung" required></textarea>
                        <br/><br/>
                        <div style="float:right;">
                            <button type="submit" class="btnemail" style="text-align:center;background:#63544d;">
                                Gửi tin nhắn
                            </button>
                        </div>
                        <?php }else { ?>
                        <input type="text" name="your-name" style="width:100%;border:1px solid #dfdfdf;height:35px;margin-right:22px;padding: 5px;margin-bottom:10px;background: #fff;" placeholder="Fullname" required>
                        <input type="email" name="your-email" style="width:55%;border:1px solid #dfdfdf;height:35px;margin-right:22px;padding: 5px;margin-bottom:10px;background: #fff;" placeholder="Email" required>
                        <input type="text" name="your-phone" style="width:40.4%;border:1px solid #dfdfdf;height:35px;padding: 5px;margin-bottom:10px;background: #fff;" placeholder="Phone" required>
                        <textarea name="your-message" style="width:100%;border:1px solid #dfdfdf;padding: 5px;height: 120px;resize: none;background: #fff;" placeholder="Description" required></textarea>
                        <br/><br/>
                        <div style="float:right;">
                            <button type="submit" class="btnemail" style="text-align:center;background:#63544d;">
                                Send message
                            </button>
                        </div>
                        <?php } ?>
                    </form>
                </div>
	</div>
</div>