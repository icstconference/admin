<style type="text/css">
	.home_menu a{
		font-size: 20px;
	}
	.ui-tabs .ui-tabs-nav .ui-tabs-anchor {
		font-size: 14px;
		padding: 8px;
	}
	.ui-tabs .ui-tabs-panel {
	    padding: 10px 0;
	}
	#main-contents tr td:first-child{
		width: 65%;
		color: #65554e;
	}
	#main-contents td {
	    font-size: 15px;
	    font-weight: 300;
	    color: #333;
	}
	.filetitle{
		font-family: 'helveticaneuelight' !important;
	}
	.filedate{
		font-family: 'helveticaneuelight' !important;
	}
	.filedownload{
		font-family: 'helveticaneuelight' !important;
	}
</style>
<?php 
    $filecategoriseinfo = $this->filecategoriseinfo;
?>
<div id="main" role="main">
	<div id="main-contents" style="padding:20px 15px 0;">
		<?php 
            if($this->lang == 'vi'){
        ?>
            <h3 style="font-size: 20px;color: #63544d;font-family: 'helveticaneuelight' !important;font-weight: 400;text-transform:uppercase;margin-bottom: 5px;">
                QUAN HỆ CỔ ĐÔNG
            </h3>
        <?php }else{ ?>
            <h3 style="font-size: 20px;color: #63544d;font-family: 'helveticaneuelight' !important;font-weight: 400;text-transform:uppercase;margin-bottom: 5px;">
                QUAN HỆ CỔ ĐÔNG
            </h3>
        <?php } ?>
    </div>
	<div class="home_menu">
		<a href="<?php echo URL;?>category/file/thong-tin-co-dong-1/">
		    <span style="font-weight:300;font-family: 'helveticaneuelight' !important;text-transform:uppercase;">
				<?php 
					if($this->lang == 'vi'){
				?>
					THÔNG TIN CỔ ĐÔNG
				<?php }else{ ?>
					Information of shareholder
				<?php } ?>
			</span>
		</a>
		<a href="<?php echo URL;?>category/file/tai-lieu-co-dong-2/" class="right">
		    <span style="font-weight:300;font-family: 'helveticaneuelight' !important;text-transform:uppercase;">
				<?php 
					if($this->lang == 'vi'){
				?>
					TÀI LIỆU CỔ ĐÔNG
				<?php }else{ ?>
					Documentation of shareholder
				<?php } ?>
			</span>
		</a>
		<a href="#">
			<span style="font-weight:300;font-family: 'helveticaneuelight' !important;text-transform:uppercase;">
				<?php 
					if($this->lang == 'vi'){
				?>
					THÔNG TIN CỔ PHIẾU
				<?php }else{ ?>
					Information of Sharing
				<?php } ?>
			</span>
		</a>
		<a href="<?php echo URL;?>contact" class="right">
		    <span style="font-weight:300;font-family: 'helveticaneuelight' !important;text-transform:uppercase;">
				<?php 
					if($this->lang == 'vi'){
				?>
					KẾT NỐI CỔ ĐÔNG
				<?php }else{ ?>
					Shareholder connection
				<?php } ?>
			</span>
		</a>
	</div>
	<div id="main-contents" style="padding:0 15px 20px;">
		<div class="homepage-module" style="padding-top: 0px;">
            <div style="padding-top: 0px; margin-top: 0px; padding-bottom: 0px; margin-bottom: 0px;" class="celebrity_or_video">
                    <?php 
                        if($filecategoriseinfo[0]['child']){
                            $child = $filecategoriseinfo[0]['child'];
                            $i=0;
                    ?>
                    <div id="tabs" style="background: transparent;border: none;">
                        <ul style="background: transparent;border: none;padding-left: 0px;margin-bottom: 5px;">
                            <?php 
                                foreach ($child as$value) {
                                    $i++;
                                    if($i <= 7){
                            ?>
                                <li>
                                    <a href="#tabs-<?php echo $i;?>">
                                        <?php 
                                            if($this->lang == 'vi'){
                                        ?>
                                            <?php echo $value['file_categories_name'];?>
                                        <?php }else{ ?>
                                            <?php echo $value['file_categories_name_english'];?>
                                        <?php } ?>
                                    </a>
                                </li>
                            <?php }} ?>
                        </ul>
                        <?php
                            foreach ($child as $value) {
                                $j++;
                                if($j <= 7){
                        ?>
                        <div id="tabs-<?php echo $j;?>" style="padding-left: 0px;">
                            <h3 style="color: #337BA7;font-size: 20px;font-family: 'helveticaneuelight' !important;text-transform:uppercase;font-weight:500;margin-bottom:5px;">
                                <?php 
                                    if($this->lang == 'vi'){
                                ?>
                                    Báo cáo tài chính
                                <?php }else{ ?>
                                    FINANCIAL REPORTS
                                <?php } ?>
                            </h3>
                            <table>
                                <thead></thead>
                                <tbody>
                                <?php 
                                    if($value['childfile']){
                                        $i=0;
                                        foreach ($value['childfile'] as $value1) {
                                            $i++;
                                            if($i % 2 == 0){
                                ?>
                                    <tr class="gray">
                                        <td style="vertical-align: middle;">
                                            <div class="filetitle">
                                                <i class="fa fa-file"></i>&nbsp;
                                                <?php 
                                                    if($this->lang == 'vi'){
                                                ?>
                                                    <?php echo $value1['file_name'];?>
                                                <?php }else{ ?>
                                                    <?php echo $value1['file_name_english'];?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                        <td style="vertical-align: middle;width:20%;">
                                            <div class="filedate">
                                                <i class="fa fa-calendar"></i>&nbsp;<?php echo date('d-m-Y',strtotime($value1['file_create_date']));?>
                                            </div>
                                        </td>
                                        <td style="vertical-align: middle;width:20%;">    
                                            <div class="filedownload">
                                                <i class="fa fa-download"></i>&nbsp;
                                                <a href="<?php echo URL.$value1['file_url'];?>" style="color:#337ab7;">
                                                    <?php 
                                                        if($this->lang == 'vi'){
                                                    ?>
                                                        Tải về
                                                    <?php }else{ ?>
                                                        Download
                                                    <?php } ?>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php }else{ ?> 
                                    <tr class="white">
                                        <td style="vertical-align: middle;">
                                            <div class="filetitle">
                                                <i class="fa fa-file"></i>&nbsp;
                                                <?php 
                                                    if($this->lang == 'vi'){
                                                ?>
                                                    <?php echo $value1['file_name'];?>
                                                <?php }else{ ?>
                                                    <?php echo $value1['file_name_english'];?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                        <td style="vertical-align: middle;width:20%;">
                                            <div class="filedate">
                                                <i class="fa fa-calendar"></i>&nbsp;<?php echo date('d-m-Y',strtotime($value1['file_create_date']));?>
                                            </div>
                                        </td>
                                        <td style="vertical-align: middle;width:20%;">    
                                            <div class="filedownload">
                                                <i class="fa fa-download"></i>&nbsp;
                                                <a href="<?php echo URL.$value1['file_url'];?>" style="color:#337ab7;">
                                                    <?php 
                                                        if($this->lang == 'vi'){
                                                    ?>
                                                        Tải về
                                                    <?php }else{ ?>
                                                        Download
                                                    <?php } ?>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php } }} ?>
                                </tbody>
                            </table>
                            <?php 
                                if($value['child']){ 
                                    $child1 = $value['child'];
                                    foreach ($child1 as $value1) {
                                        if($value1['child']){
                                            $child2 = $value1['child'];
                            ?>
                                <h3 style="color: #337BA7;font-size: 20px;font-family: 'helveticaneuelight' !important;text-transform:uppercase;font-weight:500;margin:5px 0;">
                                        <?php 
                                            if($this->lang == 'vi'){
                                        ?>
                                            <?php echo $value1['file_categories_name'];?>
                                        <?php }else{ ?>
                                            <?php echo $value1['file_categories_name_english'];?>
                                        <?php } ?>
                                </h3>
                                <?php 
                                    foreach ($child2 as $value2) {
                                ?>
                                    <h3 style="color: #63544d;margin:5px 0;font-family: 'helveticaneuelight' !important;font-size: 16px;text-transform:uppercase;"><i class="fa fa-hand-o-right"></i>&nbsp;
                                        <?php 
                                            if($this->lang == 'vi'){
                                        ?>
                                            <?php echo $value2['file_categories_name'];?>
                                        <?php }else{ ?>
                                            <?php echo $value2['file_categories_name_english'];?>
                                        <?php } ?>
                                    </h3>
                                        <table>
                                            <thead></thead>
                                            <tbody>
                                                <?php 
                                                    if($value2['childfile']){
                                                        $i=0;
                                                        foreach ($value2['childfile'] as $value3) {
                                                            $i++;
                                                            if($i % 2 == 0){
                                                ?>
                                                <tr class="gray">
                                                    <td style="vertical-align: middle;">
                                                        <div class="filetitle">
                                                            <i class="fa fa-file"></i>&nbsp;
                                                            <?php 
                                                                if($this->lang == 'vi'){
                                                            ?>
                                                                <?php echo $value3['file_name'];?>
                                                            <?php }else{ ?>
                                                                <?php echo $value3['file_name_english'];?>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                    <td style="vertical-align: middle;width:20%;">
                                                        <div class="filedate">
                                                            <i class="fa fa-calendar"></i>&nbsp;<?php echo date('d-m-Y',strtotime($value3['file_create_date']));?>
                                                        </div>
                                                    </td>
                                                    <td style="vertical-align: middle;width:20%;">    
                                                        <div class="filedownload">
                                                            <i class="fa fa-download"></i>&nbsp;
                                                            <a href="<?php echo URL.$value3['file_url'];?>" style="color:#337ab7;">
                                                                <?php 
                                                                    if($this->lang == 'vi'){
                                                                ?>
                                                                    Tải về
                                                                <?php }else{ ?>
                                                                    Download
                                                                <?php } ?>
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php }else{ ?> 
                                                <tr class="white">
                                                    <td style="vertical-align: middle;">
                                                        <div class="filetitle">
                                                            <i class="fa fa-file"></i>&nbsp;
                                                            <?php 
                                                                if($this->lang == 'vi'){
                                                            ?>
                                                                <?php echo $value3['file_name'];?>
                                                            <?php }else{ ?>
                                                                <?php echo $value3['file_name_english'];?>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                    <td style="vertical-align: middle;width:20%;">
                                                        <div class="filedate">
                                                            <i class="fa fa-calendar"></i>&nbsp;<?php echo date('d-m-Y',strtotime($value3['file_create_date']));?>
                                                        </div>
                                                    </td>
                                                    <td style="vertical-align: middle;width:20%;">    
                                                        <div class="filedownload">
                                                            <i class="fa fa-download"></i>&nbsp;
                                                            <a href="<?php echo URL.$value3['file_url'];?>" style="color:#337ab7;">
                                                                <?php 
                                                                    if($this->lang == 'vi'){
                                                                ?>
                                                                    Tải về
                                                                <?php }else{ ?>
                                                                    Download
                                                                <?php } ?>
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php } }} ?>
                                            </tbody>
                                        </table>
                                    <?php } ?>
                            <?php }else{
                            ?>
                                <h3 style="color: #337BA7;margin:5px 0;font-size: 20px;font-family: 'helveticaneuelight' !important;text-transform:uppercase;font-weight:500;"><?php echo $value1['file_categories_name'];?></h3>
                                <table>
                                    <thead></thead>
                                    <tbody>
                                        <?php 
                                            if($value1['childfile']){
                                                $i=0;
                                                foreach ($value1['childfile'] as $value2) {
                                                    $i++;
                                                    if($i % 2 == 0){
                                        ?>
                                        <tr class="gray">
                                                    <td style="vertical-align: middle;">
                                                        <div class="filetitle">
                                                            <i class="fa fa-file"></i>&nbsp;
                                                            <?php 
                                                                if($this->lang == 'vi'){
                                                            ?>
                                                                <?php echo $value2['file_name'];?>
                                                            <?php }else{ ?>
                                                                <?php echo $value2['file_name_english'];?>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                    <td style="vertical-align: middle;width:20%;">
                                                        <div class="filedate">
                                                            <i class="fa fa-calendar"></i>&nbsp;<?php echo date('d-m-Y',strtotime($value2['file_create_date']));?>
                                                        </div>
                                                    </td>
                                                    <td style="vertical-align: middle;width:20%;">    
                                                        <div class="filedownload">
                                                            <i class="fa fa-download"></i>&nbsp;
                                                            <a href="<?php echo URL.$value2['file_url'];?>" style="color:#337ab7;">
                                                                <?php 
                                                                    if($this->lang == 'vi'){
                                                                ?>
                                                                    Tải về
                                                                <?php }else{ ?>
                                                                    Download
                                                                <?php } ?>
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                        <?php }else{ ?> 
                                        <tr class="white">
                                            <td style="vertical-align: middle;">
                                                <div class="filetitle">
                                                    <i class="fa fa-file"></i>&nbsp;
                                                    <?php 
                                                        if($this->lang == 'vi'){
                                                    ?>
                                                        <?php echo $value2['file_name'];?>
                                                    <?php }else{ ?>
                                                        <?php echo $value2['file_name_english'];?>
                                                    <?php } ?>
                                                </div>
                                            </td>
                                            <td style="vertical-align: middle;width:20%;">
                                                <div class="filedate">
                                                    <i class="fa fa-calendar"></i>&nbsp;<?php echo date('d-m-Y',strtotime($value2['file_create_date']));?>
                                                </div>
                                            </td>
                                            <td style="vertical-align: middle;width:20%;">    
                                                <div class="filedownload">
                                                    <i class="fa fa-download"></i>&nbsp;
                                                    <a href="<?php echo URL.$value2['file_url'];?>" style="color:#337ab7;">
                                                        <?php 
                                                            if($this->lang == 'vi'){
                                                        ?>
                                                            Tải về
                                                        <?php }else{ ?>
                                                            Download
                                                        <?php } ?>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php } }} ?>
                                    </tbody>
                                </table>
                            <?php }}} ?>
                        </div>
                        <?php }} ?>
                    </div>
                    <?php }else{ ?>
                    <div style="margin-top: 15px;color: #63544d;font-weight: 400;">
                        <table>
                            <thead></thead>
                            <tbody>
                                <?php 
                                    if($this->listfile){
                                        $i=0;
                                        foreach ($this->listfile as $value) {
                                            $i++;
                                            if($i % 2 == 0){
                                ?>
                                <tr class="gray">
                                                    <td style="vertical-align: middle;">
                                                        <div class="filetitle">
                                                            <i class="fa fa-file"></i>&nbsp;
                                                            <?php 
                                                                if($this->lang == 'vi'){
                                                            ?>
                                                                <?php echo $value['file_name'];?>
                                                            <?php }else{ ?>
                                                                <?php echo $value['file_name_english'];?>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                    <td style="vertical-align: middle;width:20%;">
                                                        <div class="filedate">
                                                            <i class="fa fa-calendar"></i>&nbsp;<?php echo date('d-m-Y',strtotime($value['file_create_date']));?>
                                                        </div>
                                                    </td>
                                                    <td style="vertical-align: middle;width:20%;">    
                                                        <div class="filedownload">
                                                            <i class="fa fa-download"></i>&nbsp;
                                                            <a href="<?php echo URL.$value['file_url'];?>" style="color:#337ab7;">
                                                                <?php 
                                                                    if($this->lang == 'vi'){
                                                                ?>
                                                                    Tải về
                                                                <?php }else{ ?>
                                                                    Download
                                                                <?php } ?>
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                <?php }else{ ?> 
                                <tr class="white">
                                            <td style="vertical-align: middle;">
                                                <div class="filetitle">
                                                    <i class="fa fa-file"></i>&nbsp;
                                                    <?php 
                                                        if($this->lang == 'vi'){
                                                    ?>
                                                        <?php echo $value['file_name'];?>
                                                    <?php }else{ ?>
                                                        <?php echo $value['file_name_english'];?>
                                                    <?php } ?>
                                                </div>
                                            </td>
                                            <td style="vertical-align: middle;width:20%;">
                                                <div class="filedate">
                                                    <i class="fa fa-calendar"></i>&nbsp;<?php echo date('d-m-Y',strtotime($value['file_create_date']));?>
                                                </div>
                                            </td>
                                            <td style="vertical-align: middle;width:20%;">    
                                                <div class="filedownload">
                                                    <i class="fa fa-download"></i>&nbsp;
                                                    <a href="<?php echo URL.$value['file_url'];?>" style="color:#337ab7;">
                                                        <?php 
                                                            if($this->lang == 'vi'){
                                                        ?>
                                                            Tải về
                                                        <?php }else{ ?>
                                                            Download
                                                        <?php } ?>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                <?php } }} ?>
                            </tbody>
                        </table>
                    </div>
                    <?php } ?>
            </div>
        </div>
	</div>
</div>