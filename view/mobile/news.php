<?php 
    function cut_string($str, $len, $more) {
        if ($str == "" || $str == NULL)
            return $str;
        if (is_array($str))
            return $str;
        $str = trim($str);
        if (strlen($str) <= $len)
            return $str;
        $str = substr($str, 0, $len);
        if ($str != "") {
            if (!substr_count($str, " ")) {
                if ($more)
                    $str .= " ...";
                return $str;
            }
            while (strlen($str) && ($str[strlen($str) - 1] != " ")) {
                $str = substr($str, 0, -1);
            }
            $str = substr($str, 0, -1);
            if ($more)
                $str .= " ...";
        }
        return $str;
    }
?>
<style type="text/css">
    .module-header{
        height: 190px;
        width: 268px !important;
    }
    .module-body h2{
        padding-left: 290px;
        padding-top: 40px;
    }
    .module-body h2, .module.three-col .module-body h2{
        font-family: 'helveticaneuelight' !important;
        font-size: 20px !important;
        line-height: 30px;
    }
    @media(max-width: 480px) {
        .module-header{
            height: 190px;
            width: 180px !important;
        }
        .module-body h2{
            padding-left: 180px;
            padding-top: 10px;
        }
        .module.three-col .module-body h2{
            font-size: 16px !important;
            line-height: 22px;
        }
    }
</style>
<?php 
    $numpage = $this->numpage;
    $categoriesinfo = $this->categoriesinfo;
?>
<div id="main" role="main" class="single-col" style="padding-bottom:20px;min-height:0px;">		
	<div id="main-contents" style="padding-top:20px;">
		<?php 
            if($this->listnew){
                foreach ($this->listnew as $value) {
                    $category = $value['category'];
        ?>
        <section>
            <div id="cphMainContent_gridViewOffers635845015382573711_rptrOfferItems_divGridItem_2" class="module three-col clear-section" onclick="window.location.href='<?php echo URL.'news/detail/'.$value['news_url'];?>';">
	            <div class="module-header" style="background: url('<?php echo $value['news_image_thumb'];?>') no-repeat center;background-size: cover;">
	            </div>
	            <div class="module-body">
		            <a id="cphMainContent_gridViewOffers635845015382573711_rptrOfferItems_hLink_2" href="<?php echo URL.'news/detail/'.$value['news_url'];?>">
		            	<h2 style="">
		            		<?php 
                                if($this->lang == 'vi'){
                            ?>
                                <?php echo cut_string($value['news_name'],90,50);?>
                            <?php }else{ ?>
                                <?php echo cut_string($value['news_name_english'],90,50);?>
                           	<?php } ?>
		            	</h2>
		            </a>
                    <span></span>
	            </div>
            </div>
        </section>
        <?php }} ?>
	</div>
</div>