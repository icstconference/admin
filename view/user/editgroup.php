<?php 
    $groupinfo = $this->groupinfo;
?>
<header class="page-header">
    <h2><?php echo $groupinfo[0]['user_group_name'];?></h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="<?php echo URL;?>user">
                    <span>Danh sách người dùng</span>
                </a>
            </li>
            <li>
                <a href="<?php echo URL;?>user/group/">
                    <span>Danh sách nhóm người dùng</span>
                </a>
            </li>
            <li>
                <span><?php echo $groupinfo[0]['user_group_name'];?></span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Chỉnh sửa nhóm người dùng mới</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label class="col-sm-2 control-label">Tên nhóm</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" value="<?php echo $groupinfo[0]['user_group_name'];?>" data-toggle="tooltip" data-placement="bottom" title="Điền tên nhóm" name="group_name" required>
                </div>
            </div>
            <div class="form-group" style="margin-top: 40px;">
                <div class="col-sm-offset-4 col-sm-2">
                    <button type="cancel" class="btn btn-danger btn-label-left" name="cancel">
                        <span><i class="fa fa-clock-o txt-danger"></i></span>
                        Reset
                    </button>
                </div>
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-success btn-label-left" name="register" value="">
                        <span><i class="fa fa-clock-o"></i></span>
                        Submit
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>