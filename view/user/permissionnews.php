<style type="text/css">
    input[type="radio"], input[type="checkbox"]{
        width: 15px;
        height: 15px;
    }
</style>
<header class="page-header">
    <h2>Danh sách quyền</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <span>Danh sách quyền bài viết</span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Thêm quyền bài viết mới</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
			<div class="form-group">
                <label class="col-sm-3 control-label">Nhóm người dùng</label>
                <div class="col-sm-2">
                    <select id="s2_with_tag" data-plugin-selectTwo class="form-control populate" name="user_id">
                        <?php 
                            foreach ($this->listadmin1 as $value) {
                                if($value['user_group_id'] != 1){
                        ?>
                        <option value="<?php echo $value['user_group_id'];?>"><?php echo $value['user_group_name'];?></option>
                        <?php }} ?>
                    </select>
                </div>
            </div>
			<div class="form-group">
                <label class="col-sm-3 control-label">Chọn chuyên mục bài viết</label>
                <div class="col-sm-2">
                    <select id="s2_with_tag" data-plugin-selectTwo class="form-control populate" name="categories_id">
                        <?php
                            if($this->listcategories){
                                foreach($this->listcategories as $lt){
                        ?>
                            <option value="<?php echo $lt['news_categories_id'];?>"><?php echo $lt['news_categories_name'];?></option>
                        <?php }}?>
                    </select>
                </div>
            </div>
			<div class="form-group" style="margin-top: 40px;">
                <div class="col-sm-offset-4 col-sm-2">
                    <button type="cancel" class="btn btn-danger btn-label-left" name="cancel">
                        <span><i class="fa fa-clock-o txt-danger"></i></span>
                        Cancel
                    </button>
                </div>
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-success btn-label-left" name="register">
                        <span><i class="fa fa-clock-o"></i></span>
                        Submit
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>
<?php
    $listpermissionews =$this->listpermissionews;
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Danh sách quyền bài viết</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-default">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nhóm</th>
                    <th>Chuyên mục</th>
                    <th>Chỉnh sửa</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($listpermissionews){
                        $i = 0;
                        foreach($listpermissionews as $ls){
                            $i++;
							$categoryinfo = $ls['categoryinfo'];
                ?>
                    <tr>
                        <td><?php echo $i;?></td>
                        <td>
                            <?php 
                                foreach ($this->listadmin1 as $value) {
                                    if($ls['user_group_id'] == $value['user_group_id']){
                                        echo $value['user_group_name'];
                                    }
                                }
                            ?>
                        </td>
                        <td>
                            <?php echo $categoryinfo['news_categories_name'];?>
                        </td>
                        <td>
                            <a class="btn btn-primary" href="<?php echo URL;?>user/editpermissionnews/<?php echo $ls['permission_news_id'];?>"><i class="fa fa-edit"></i></a>
							<a class="btn btn-danger" href="<?php echo URL;?>user/deletepermissionnews/<?php echo $ls['permission_news_id'];?>"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                <?php }} ?>
            </tbody>
        </table>
    </div>
</section>