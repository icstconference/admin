<header class="page-header">
    <h2>Danh sách nhóm người dùng</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="<?php echo URL;?>user">
                    <span>Danh sách người dùng</span>
                </a>
            </li>
            <li><span>Danh sách nhóm người dùng</span></li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Thêm nhóm người dùng mới</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label class="col-sm-2 control-label">Tên nhóm</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Tên nhóm" data-toggle="tooltip" data-placement="bottom" title="Điền tên nhóm" name="group_name" required>
                </div>
            </div>
            <div class="form-group" style="margin-top: 40px;">
                <div class="col-sm-offset-4 col-sm-2">
                    <button type="cancel" class="btn btn-danger btn-label-left" name="cancel">
                        <span><i class="fa fa-clock-o txt-danger"></i></span>
                        Reset
                    </button>
                </div>
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-success btn-label-left" name="register" value="">
                        <span><i class="fa fa-clock-o"></i></span>
                        Submit
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>
<?php
    $listgroup =$this->listgroup;
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Danh sách nhóm người dùng</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-default">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Tên nhóm</th>
                    <th>Chỉnh sửa</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($listgroup){
                        $i = 0;
                        foreach($listgroup as $ls){
                            $i++;
                ?>
                    <tr>
                        <td><?php echo $i;?></td>
                        <td>
                            <?php echo $ls['user_group_name'];?>
                        </td>
                        <td>
                            <?php 
                                if($ls['user_group_id'] != 1){
                            ?>
                                <a class="btn btn-primary" href="<?php echo URL;?>user/editgroup/<?php echo $ls['user_group_id'];?>"><i class="fa fa-edit"></i></a>
                                <a class="btn btn-danger" href="<?php echo URL;?>user/deletegroup/<?php echo $ls['user_group_id'];?>"><i class="fa fa-trash"></i></a>
                            <?php }else{ ?>
                                Không được chỉnh sửa
                            <?php } ?>
                        </td>
                    </tr>
                <?php }} ?>
            </tbody>
        </table>
    </div>
</section>
