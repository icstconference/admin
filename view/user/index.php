<header class="page-header">
    <h2>Danh sách người dùng</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Danh sách người dùng</span></li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<?php
    $listuser =$this->listuser;
?>
<div class="row" style="margin: 0 0 10px;text-align:right;">
    <a href="<?php echo URL;?>user/adduser/" class="mb-xs mt-xs mr-xs btn btn-primary">
        <i class="fa fa-user-plus"></i>&nbsp;Thêm người dùng
    </a>
</div>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Danh sách người dùng</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-default">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Loại</th>
                    <th>Chỉnh sửa</th>
                </tr>
            </thead>
            <tbody>
            <?php
                if($listuser){
                    $i = 0;
                    foreach($listuser as $ls){
                        $i++;
            ?>
                <tr>
                    <td><?php echo $i;?></td>
                    <td>
                        <img class="img-responsive" src="<?php echo URL;?>public/img/defavatar.png" style="width: 60px;display: inline;">
                        <?php echo $ls['user_name'];?>
                    </td>
                    <td style="vertical-align:middle;"><?php echo $ls['user_email'];?></td>
                    <td style="vertical-align:middle;"><?php echo $ls['user_type'];?></td>
                    <td style="vertical-align:middle;text-align:left;">
						<?php 
							if($_SESSION['user_id'] != '1'){
                            if($ls['user_id'] != '1'){
                        ?>
							<a class="btn btn-primary" href="<?php echo URL;?>user/edituser/<?php echo $ls['user_id'];?>"><i class="fa fa-edit"></i>&nbsp;</a>
                            <a class="btn btn-danger" href="<?php echo URL;?>user/deleteuser/<?php echo $ls['user_id'];?>"><i class="fa fa-trash"></i>&nbsp;</a>
						<?php }}else{ ?>
							<a class="btn btn-primary" href="<?php echo URL;?>user/edituser/<?php echo $ls['user_id'];?>"><i class="fa fa-edit"></i>&nbsp;</a>
                            <a class="btn btn-danger" href="<?php echo URL;?>user/deleteuser/<?php echo $ls['user_id'];?>"><i class="fa fa-trash"></i>&nbsp;</a>
						<?php } ?>
                    </td>
                </tr>
            <?php }} ?>
            </tbody>
        </table>
    </div>
</section>
<div class="row" style="margin: 10px 0 0;text-align:right;">
    <a href="<?php echo URL;?>user/adduser/" class="mb-xs mt-xs mr-xs btn btn-primary">
        <i class="fa fa-user-plus"></i>&nbsp;Thêm người dùng
    </a>
</div>