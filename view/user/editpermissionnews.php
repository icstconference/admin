<style type="text/css">
    input[type="radio"], input[type="checkbox"]{
        width: 15px;
        height: 15px;
    }
</style>
<?php 
	$permissioninfo = $this->permissioninfo;
	$userinfo = $this->userinfo;	
?>
<header class="page-header">
    <h2>Danh sách quyền</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="<?php echo URL;?>user/permissionnews/">
                    <span>Danh sách quyền bài viết</span>
                </a>
            </li>
            <li>
                <span><?php echo $permissioninfo[0]['permission_news_id'];?></span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Thêm quyền bài viết mới</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
			<div class="form-group">
                <label class="col-sm-3 control-label">Nhóm người dùng</label>
                <div class="col-sm-2">
                    <?php 
                        foreach ($this->listadmin1 as $value) {
                            if($value['user_group_id'] == $permissioninfo[0]['user_group_id']){
                    ?>
                        <input type="hidden" name="user_id" value="<?php echo $value['user_group_id'];?>" />
                        <?php echo $value['user_group_name'];?>
                    <?php }} ?>
				</div>
            </div>
			<div class="form-group">
                <label class="col-sm-3 control-label">Chọn mục bài viết</label>
                <div class="col-sm-2">
                    <select id="s2_with_tag" class="populate placeholder" name="categories_id">
                        <?php
                            if($this->listcategories){
                                foreach($this->listcategories as $lt){
									if($permissioninfo[0]['categories_id'] == $lt['news_categories_id']) {
                        ?>
							<option value="<?php echo $lt['news_categories_id'];?>" selected="selected" ><?php echo $lt['news_categories_name'];?></option>
						<?php }else{ ?>
							<option value="<?php echo $lt['news_categories_id'];?>" ><?php echo $lt['news_categories_name'];?></option>
						<?php }}}?>
                    </select>
                </div>
            </div>
			<div class="form-group" style="margin-top: 40px;">
                <div class="col-sm-offset-4 col-sm-2">
                    <button type="cancel" class="btn btn-danger btn-label-left" name="cancel">
                        <span><i class="fa fa-clock-o txt-danger"></i></span>
                        Cancel
                    </button>
                </div>
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-success btn-label-left" name="register">
                        <span><i class="fa fa-clock-o"></i></span>
                        Submit
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>