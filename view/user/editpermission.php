<style type="text/css">
    input[type="radio"], input[type="checkbox"]{
        width: 15px;
        height: 15px;
    }
</style>
<?php 
    $permissioninfo = $this->permissioninfo;
    $create = $permissioninfo[0]['create'];
    $delete = $permissioninfo[0]['delete'];
    $edit   = $permissioninfo[0]['edit'];
?>
<header class="page-header">
    <h2><?php echo $permissioninfo[0]['user_group_name'];?></h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="<?php echo URL;?>user/permission/">
                    <span>Danh sách quyền</span>
                </a>
            </li>
            <li>
                <span><?php echo $permissioninfo[0]['user_group_name'];?></span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<div class="row" style="margin: 0 0 10px;text-align:left;">
    <a href="<?php echo URL;?>user/permission/" class="mb-xs mt-xs mr-xs btn btn-primary">
        <i class="fa fa-mail-reply"></i>&nbsp;Danh sách quyền
    </a>
</div>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Thêm chỉnh sửa quyền</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
            <input type="hidden" name="user_id" value="<?php echo $permissioninfo[0]['user_group_id'];?>">
            <div class="form-group">
                <label class="col-sm-2 control-label">Nhóm người dùng</label>
                    <div class="col-sm-10">
                        <?php echo $permissioninfo[0]['user_group_name'];?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Quyền tạo</label>
                    <div class="col-sm-10">
                        <?php 
                            if(strpos($create['permission_detail'],'new')){
                        ?>
                        <label for="creatnew">Tin tức</label>
                        <input type="checkbox" checked name="creatnew" /> | 
                        <?php }else{ ?>
                        <label for="creatnew">Tin tức</label>
                        <input type="checkbox" name="creatnew" /> |  
                        <?php } ?>
                        <?php 
                            if(strpos($create['permission_detail'],'file')){
                        ?>
                        <label for="creatnew">Tài nguyên</label>
                        <input type="checkbox" checked name="creatfile" /> | 
                        <?php }else{ ?>
                        <label for="creatnew">Tài nguyên</label>
                        <input type="checkbox" name="creatfile" /> |  
                        <?php } ?>
						<?php 
                            if(strpos($create['permission_detail'],'galleries')){
                        ?>
                        <label for="creatnew">Galleries</label>
                        <input type="checkbox" checked name="creatgalleries" /> | 
                        <?php }else{ ?>
                        <label for="creatnew">Galleries</label>
                        <input type="checkbox" name="creatgalleries" /> |  
                        <?php } ?>
						<?php 
                            if(strpos($create['permission_detail'],'menu')){
                        ?>
                        <label for="creatnew">Menu</label>
                        <input type="checkbox" checked name="creatmenu" /> | 
                        <?php }else{ ?>
                        <label for="creatnew">Menu</label>
                        <input type="checkbox" name="creatmenu" /> | 
                        <?php } ?>
                        <?php 
                            if(strpos($create['permission_detail'],'slider')){
                        ?>
                        <label for="creatnew">Slider</label>
                        <input type="checkbox" name="creatslider" checked/> | 
                        <?php }else{ ?>
                        <label for="creatnew">Slider</label>
                        <input type="checkbox" name="creatslider"/> | 
                        <?php } ?>
                        <?php 
                            if(strpos($create['permission_detail'],'page')){
                        ?>
                        <label for="creatnew">Trang</label>
                        <input type="checkbox" name="creatpage" checked/> | 
                        <?php }else{ ?>
                        <label for="creatnew">Trang</label>
                        <input type="checkbox" name="creatpage"/> | 
                        <?php } ?>
					    <?php 
                            if(strpos($create['permission_detail'],'contact')){
                        ?>
                        <label for="creatnew">Liên hệ</label>
                        <input type="checkbox" checked name="creatcontact" />
                        <?php }else{ ?>
                        <label for="creatnew">Liên hệ</label>
                        <input type="checkbox" name="creatcontact" />
                        <?php } ?>
                        <?php 
                            if(strpos($create['permission_detail'],'event')){
                        ?>
                        <label for="creatnew">Sự kiện</label>
                        <input type="checkbox" checked name="createvent" />
                        <?php }else{ ?>
                        <label for="creatnew">Sự kiện</label>
                        <input type="checkbox" name="createvent" />
                        <?php } ?>
                        <?php 
                            if(strpos($create['permission_detail'],'artist')){
                        ?>
                        <label for="creatnew">Artist</label>
                        <input type="checkbox" checked name="creatartist" />
                        <?php }else{ ?>
                        <label for="creatnew">Artist</label>
                        <input type="checkbox" name="creatartist" />
                        <?php } ?>
                        <?php 
                            if(strpos($create['permission_detail'],'partner')){
                        ?>
                        <label for="creatnew">Đối tác</label>
                        <input type="checkbox" checked name="creatpartner" />
                        <?php }else{ ?>
                        <label for="creatnew">Đối tác</label>
                        <input type="checkbox" name="creatpartner" />
                        <?php } ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Quyền edit</label>
                    <div class="col-sm-10">
                        <?php 
                            if(strpos($edit['permission_detail'],'new')){
                        ?>
                        <label for="creatnew">Tin tức</label>
                        <input type="checkbox" checked name="editnew" /> | 
                        <?php }else{ ?>
                        <label for="editnew">Tin tức</label>
                        <input type="checkbox" name="editnew" /> |  
                        <?php } ?>
						<?php 
                            if(strpos($edit['permission_detail'],'file')){
                        ?>
                        <label for="creatnew">Tài nguyên</label>
                        <input type="checkbox" checked name="editfile" /> | 
                        <?php }else{ ?>
                        <label for="creatnew">Tài nguyên</label>
                        <input type="checkbox" name="editfile" /> |  
                        <?php } ?>
						<?php 
                            if(strpos($edit['permission_detail'],'galleries')){
                        ?>
                        <label for="editnew">Galleries</label>
                        <input type="checkbox" checked name="editgalleries" /> | 
                        <?php }else{ ?>
                        <label for="editnew">Galleries</label>
                        <input type="checkbox" name="editgalleries" /> |  
                        <?php } ?>
						<?php 
                            if(strpos($edit['permission_detail'],'menu')){
                        ?>
                        <label for="editnew">Menu</label>
                        <input type="checkbox" checked name="editmenu" /> | 
                        <?php }else{ ?>
                        <label for="editnew">Menu</label>
                        <input type="checkbox" name="editmenu" /> | 
                        <?php } ?>
                        <?php 
                            if(strpos($edit['permission_detail'],'slider')){
                        ?>
                        <label for="creatnew">Slider</label>
                        <input type="checkbox" name="editslider" checked/> | 
                        <?php }else{ ?>
                        <label for="creatnew">Slider</label>
                        <input type="checkbox" name="editslider"/> | 
                        <?php } ?>
                        <?php 
                            if(strpos($edit['permission_detail'],'page')){
                        ?>
                        <label for="creatnew">Trang</label>
                        <input type="checkbox" name="editpage" checked/> | 
                        <?php }else{ ?>
                        <label for="creatnew">Trang</label>
                        <input type="checkbox" name="editpage"/> | 
                        <?php } ?>
                        <?php 
                            if(strpos($edit['permission_detail'],'contact')){
                        ?>
                        <label for="editnew">Liên hệ</label>
                        <input type="checkbox" checked name="editcontact" />
                        <?php }else{ ?>
                        <label for="editnew">Liên hệ</label>
                        <input type="checkbox" name="editcontact" />
                        <?php } ?>
                        <?php 
                            if(strpos($edit['permission_detail'],'event')){
                        ?>
                        <label for="creatnew">Sự kiện</label>
                        <input type="checkbox" checked name="editevent" />
                        <?php }else{ ?>
                        <label for="creatnew">Sự kiện</label>
                        <input type="checkbox" name="editevent" />
                        <?php } ?>
                        <?php 
                            if(strpos($edit['permission_detail'],'artist')){
                        ?>
                        <label for="creatnew">Artist</label>
                        <input type="checkbox" checked name="editartist" />
                        <?php }else{ ?>
                        <label for="creatnew">Artist</label>
                        <input type="checkbox" name="editartist" />
                        <?php } ?>
                        <?php 
                            if(strpos($edit['permission_detail'],'partner')){
                        ?>
                        <label for="creatnew">Đối tác</label>
                        <input type="checkbox" checked name="editpartner" />
                        <?php }else{ ?>
                        <label for="creatnew">Đối tác</label>
                        <input type="checkbox" name="editpartner" />
                        <?php } ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Quyền xóa</label>
                    <div class="col-sm-10">
                        <?php 
                            if(strpos($delete['permission_detail'],'new')){
                        ?>
                        <label for="creatnew">Tin tức</label>
                        <input type="checkbox" checked name="deletenew" /> | 
                        <?php }else{ ?>
                        <label for="deletenew">Tin tức</label>
                        <input type="checkbox" name="deletenew" /> |  
                        <?php } ?>
                        <?php 
                            if(strpos($delete['permission_detail'],'file')){
                        ?>
                        <label for="creatnew">Tài nguyên</label>
                        <input type="checkbox" checked name="deletefile" /> | 
                        <?php }else{ ?>
                        <label for="creatnew">Tài nguyên</label>
                        <input type="checkbox" name="deletefile" /> |  
                        <?php } ?>
						<?php 
                            if(strpos($delete['permission_detail'],'galleries')){
                        ?>
                        <label for="deletenew">Galleries</label>
                        <input type="checkbox" checked name="deletegalleries" /> | 
                        <?php }else{ ?>
                        <label for="deletenew">Galleries</label>
                        <input type="checkbox" name="deletegalleries" /> |  
                        <?php } ?>
						<?php 
                            if(strpos($delete['permission_detail'],'menu')){
                        ?>
                        <label for="deletenew">Menu</label>
                        <input type="checkbox" checked name="deletemenu" /> | 
                        <?php }else{ ?>
                        <label for="deletenew">Menu</label>
                        <input type="checkbox" name="deletemenu" /> | 
                        <?php } ?>
                        <?php 
                            if(strpos($delete['permission_detail'],'slider')){
                        ?>
                        <label for="creatnew">Slider</label>
                        <input type="checkbox" name="deleteslider" checked/> | 
                        <?php }else{ ?>
                        <label for="creatnew">Slider</label>
                        <input type="checkbox" name="deleteslider"/> | 
                        <?php } ?>
                        <?php 
                            if(strpos($delete['permission_detail'],'page')){
                        ?>
                        <label for="creatnew">Trang</label>
                        <input type="checkbox" name="deletepage" checked/> | 
                        <?php }else{ ?>
                        <label for="creatnew">Trang</label>
                        <input type="checkbox" name="deletepage"/> | 
                        <?php } ?>
                        <?php 
                            if(strpos($delete['permission_detail'],'contact')){
                        ?>
                        <label for="deletenew">Liên hệ</label>
                        <input type="checkbox" checked name="deletecontact" />
                        <?php }else{ ?>
                        <label for="deletenew">Liên hệ</label>
                        <input type="checkbox" name="deletecontact" />
                        <?php } ?>
                        <?php 
                            if(strpos($delete['permission_detail'],'event')){
                        ?>
                        <label for="creatnew">Sự kiện</label>
                        <input type="checkbox" checked name="deleteevent" />
                        <?php }else{ ?>
                        <label for="creatnew">Sự kiện</label>
                        <input type="checkbox" name="deleteevent" />
                        <?php } ?>
                        <?php 
                            if(strpos($delete['permission_detail'],'artist')){
                        ?>
                        <label for="creatnew">Artist</label>
                        <input type="checkbox" checked name="deleteartist" />
                        <?php }else{ ?>
                        <label for="creatnew">Artist</label>
                        <input type="checkbox" name="deleteartist" />
                        <?php } ?>
                        <?php 
                            if(strpos($delete['permission_detail'],'partner')){
                        ?>
                        <label for="creatnew">Đối tác</label>
                        <input type="checkbox" checked name="deletepartner" />
                        <?php }else{ ?>
                        <label for="creatnew">Đối tác</label>
                        <input type="checkbox" name="deletepartner" />
                        <?php } ?>
                    </div>
                </div>

                <div class="form-group" style="margin-top: 40px;">
                    <div class="col-sm-offset-4 col-sm-2">
                        <button type="cancel" class="btn btn-danger btn-label-left" name="cancel">
                            <span><i class="fa fa-clock-o txt-danger"></i></span>
                            Cancel
                        </button>
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-success btn-label-left" name="register">
                            <span><i class="fa fa-clock-o"></i></span>
                            Submit
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </section>