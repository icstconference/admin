<?php
    $userinfo = $this->userinfo;
?>
<header class="page-header">
    <h2><?php echo $userinfo[0]['user_name'];?></h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="<?php echo URL;?>user/">
                    <span>Danh sách người dùng</span>
                </a>
            </li>
            <li>
                <span><?php echo $userinfo[0]['user_name'];?></span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Chỉnh sửa người dùng</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label class="col-sm-2 control-label">Username</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" value="<?php echo $userinfo[0]['user_name'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="user_name">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" value="<?php echo $userinfo[0]['user_email'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="user_email">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Old password</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" value="" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="pass">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">New password</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" value="" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="new_pass">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Loại user</label>
                <div class="col-sm-4">
                    <select data-plugin-selectTwo class="form-control populate" name="user_type">
                        <option value="0">Người dùng</option>
                        <?php 
                            if($this->listgroup){
                                foreach ($this->listgroup as $value) {
                                    if($value['user_group_id'] == $userinfo[0]['user_group_id']){
                        ?>
                            <option value="<?php echo $value['user_group_id'];?>" selected><?php echo $value['user_group_name'];?></option>
                        <?php }else{?>
                            <option value="<?php echo $value['user_group_id'];?>"><?php echo $value['user_group_name'];?></option>
                        <?php }}} ?>
                    </select>
                </div>
            </div>
            <div class="form-group" style="margin-top: 40px;">
                <div class="col-sm-offset-4 col-sm-2">
                    <button type="cancel" class="btn btn-danger btn-label-left" name="cancel">
                        <span><i class="fa fa-clock-o txt-danger"></i></span>
                        Cancel
                    </button>
                </div>
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-success btn-label-left" name="update">
                        <span><i class="fa fa-clock-o"></i></span>
                            Submit
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>