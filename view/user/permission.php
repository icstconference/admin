<style type="text/css">
    input[type="radio"], input[type="checkbox"]{
        width: 15px;
        height: 15px;
    }
</style>
<header class="page-header">
    <h2>Danh sách quyền</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Danh sách quyền</span></li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<div class="row" style="margin: 0 0 10px;text-align:right;">
    <a href="<?php echo URL;?>user/group/" class="mb-xs mt-xs mr-xs btn btn-primary">
        <i class="fa fa-users"></i>&nbsp;Thêm nhóm người dùng
    </a>
</div>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Thêm quyền mới</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label class="col-sm-2 control-label">Nhóm người dùng</label>
                <div class="col-sm-10">
                    <select id="s2_with_tag" class="populate placeholder" name="user_id">
                    <?php 
                        foreach ($this->listadmin as $value) {
                            if($value['user_group_id'] != 1){
                    ?>
                        <option value="<?php echo $value['user_group_id'];?>"><?php echo $value['user_group_name'];?></option>
                    <?php }} ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Quyền tạo</label>
                <div class="col-sm-10">
                    <label for="creatnew">Tin tức</label>
                    <input type="checkbox" name="creatnew" /> | 
					<label for="creatnew">Tài nguyên</label>
                    <input type="checkbox" name="creatfile" /> | 
					<label for="creatnew">Galleries</label>
                    <input type="checkbox" name="creatgalleries" /> | 
                    <label for="creatnew">Menu</label>
                    <input type="checkbox" name="creatmenu" /> | 
                    <label for="creatnew">Slider</label>
                    <input type="checkbox" name="creatslider" /> | 
                    <label for="creatnew">Trang</label>
                    <input type="checkbox" name="creatpage" /> | 
                    <label for="creatnew">Liên hệ</label>
                    <input type="checkbox" name="creatcontact" /> | 
                    <label for="creatnew">Sự kiện</label>
                    <input type="checkbox" name="createvent" /> | 
                    <label for="creatnew">Artist</label>
                    <input type="checkbox" name="creatartist" />
                    <label for="creatnew">Đối tác</label>
                    <input type="checkbox" name="creatpartner" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Quyền edit</label>
                <div class="col-sm-10">
				    <label for="creatnew">Tin tức</label>
                    <input type="checkbox" name="editnew" /> | 
					<label for="creatnew">Tài nguyên</label>
                    <input type="checkbox" name="editfile" /> | 
					<label for="creatnew">Galleries</label>
                    <input type="checkbox" name="editgalleries" /> | 
                    <label for="editnew">Menu</label>
                    <input type="checkbox" name="editmenu" /> | 
                    <label for="creatnew">Slider</label>
                    <input type="checkbox" name="editslider" /> | 
                    <label for="creatnew">Trang</label>
                    <input type="checkbox" name="editpage" /> | 
                    <label for="editnew">Liên hệ</label>
                    <input type="checkbox" name="editcontact" /> | 
                    <label for="creatnew">Sự kiện</label>
                    <input type="checkbox" name="editevent" /> | 
                    <label for="creatnew">Artist</label>
                    <input type="checkbox" name="editartist" />
                    <label for="creatnew">Đối tác</label>
                    <input type="checkbox" name="editpartner" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Quyền xóa</label>
                <div class="col-sm-10">
                    <label for="creatnew">Tin tức</label>
                    <input type="checkbox" name="deletenew" /> | 
					<label for="creatnew">Tài nguyên</label>
                    <input type="checkbox" name="deletefile" /> | 
                    <label for="creatnew">Galleries</label>
                    <input type="checkbox" name="deletegalleries" /> | 
                    <label for="deletenew">Menu</label>
                    <input type="checkbox" name="deletemenu" /> | 
                    <label for="creatnew">Slider</label>
                    <input type="checkbox" name="deleteslider" /> | 
                    <label for="creatnew">Trang</label>
                    <input type="checkbox" name="deletepage" /> | 
                    <label for="deletenew">Liên hệ</label>
                    <input type="checkbox" name="deletecontact" /> | 
                    <label for="creatnew">Sự kiện</label>
                    <input type="checkbox" name="deleteevent" /> | 
                    <label for="creatnew">Artist</label>
                    <input type="checkbox" name="deleteartist" />
                    <label for="creatnew">Đối tác</label>
                    <input type="checkbox" name="deletepartner" />
                </div>
            </div>

            <div class="form-group" style="margin-top: 40px;">
                <div class="col-sm-offset-4 col-sm-2">
                    <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                        <span><i class="fa fa-clock-o txt-danger"></i></span>
                        Reset
                    </button>
                </div>
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-success btn-label-left" name="register">
                        <span><i class="fa fa-clock-o"></i></span>
                        Submit
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>
<?php
    $listadmin =$this->listadmin1;
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Danh sách quyền</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-default">
            <thead>
                <tr>
                    <th style="vertical-align:middle;text-align:center;">No</th>
                    <th>Nhóm người dùng</th>
                    <th>Quyền</th>
                    <th>Chỉnh sửa</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($listadmin){
                        $i = 0;
                        foreach($listadmin as $ls){
                            $i++;
                ?>
                    <tr>
                        <td style="vertical-align:middle;text-align:center;"><?php echo $i;?></td>
                        <td style="vertical-align:middle;">
                            <?php echo $ls['user_group_name'];?>
                        </td>
                        <td style="vertical-align:middle;">
                            <?php 
                                if($ls['user_group_id'] != 1){
                            ?>
                                Tạo: <?php echo $ls['create']['permission_detail'];?><br/>
                                Sửa: <?php echo $ls['edit']['permission_detail'];?><br/>
                                Xóa: <?php echo $ls['delete']['permission_detail'];?>
                            <?php }else{ ?>
                                Full quyền
                            <?php } ?>
                        </td>
                        <td style="vertical-align:middle;">
                            <?php 
                                if($ls['user_group_id'] != 1){
                            ?>
                            <a class="btn btn-primary" href="<?php echo URL;?>user/editpermission/<?php echo $ls['user_group_id'];?>"><i class="fa fa-edit"></i>&nbsp;</a>
                            <?php } ?>
                        </td>
                    </tr>
                <?php }} ?>
            </tbody>
        </table>
    </div>
</section>