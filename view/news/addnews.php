<header class="page-header">
    <h2>Viết bài mới</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="<?php echo URL;?>news/">
                    <span>Tất cả bài viết</span>
                </a>
            </li>
            <li>
                <span>Viết bài mới</span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<?php                            
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Thêm bài viết mới</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" role="form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tiêu đề bài viết</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Tên bài viết" data-toggle="tooltip" data-placement="bottom" title="Nhập tiêu đề bài viết" id="news_name" name="news_name" required>
                        </div>
                    </div>
                    <div class="form-group" style="display:none;">
                        <label class="col-sm-3 control-label">Tiêu đề bài viết tiếng anh</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Tên bài viết tiếng anh" data-toggle="tooltip" data-placement="bottom" title="Nhập tiêu đề bài viết tiếng anh" id="news_name_english" name="news_name_english" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Chọn sự kiện</label>
                        <div class="col-sm-8">
                            <select id="s2_with_tag" data-plugin-selectTwo class="form-control populate" name="news_type">
                                <?php
                                $listpermission = $this->listpermission;
                                
                                    if($this->listevent){
                                        foreach($this->listevent as $lt){
                                            ?>
                                            <option value="<?php echo $lt['event_id'];?>"><?php echo $lt['event_name'];?></option>
                                <?php }} ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="display:none;">
                        <label class="col-sm-3 control-label">Chọn loại hiển thị</label>
                        <div class="col-sm-3">
                            <select id="s2_with_tag" data-plugin-selectTwo class="form-control populate" name="news_display">
                                <option value="news">News</option>
                                <option value="company">Company snapshot</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="display:none;">
                        <label class="col-sm-3 control-label">Chọn nhãn bài viết</label>
                        <div class="col-sm-8">
                            <select multiple="" class="form-control" style="padding:0px;" name="news_label[]">
                                <option value="0">Không có</option>
                                <?php
                                if($this->listlabel){
                                    foreach($this->listlabel as $lb){
                                        ?>
                                        <option value="<?php echo $lb['label_id'];?>"><?php echo $lb['label_name'];?></option>
                                <?php }}?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hình ảnh đại diện (489 x 349)</label>
                        <div class="col-sm-3">
                            <img src="#" id="newsimg" class="img-responsive imgupload" style="border-radius:3px;border:1px solid #ddd;display:none;margin-bottom:5px;" /> 
                            <input type="file" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="news_img" name="news_img" accept="image/jpg,image/png,image/jpeg,image/gif"  required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nội dung bài viết</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" placeholder="Nội dung bài viết" rows="5" title="Tooltip for name" id="wysiwig_full1" name="news_description"></textarea>
                        </div>
                    </div>
                    <div class="form-group" style="display:none;">
                        <label class="col-sm-3 control-label" >Nội dung bài viết tiếng anh</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" placeholder="Nội dung bài viết tiếng anh" rows="5" title="Tooltip for name" id="wysiwig_full2" name="news_description_english"></textarea>
                        </div>
                    </div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Ngày viết</label>
						<div class="col-sm-3">
							<input type="datetime" class="form-control" value="<?php echo date('Y-m-d h:i:s');?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="news_create_date" name="news_create_date" required>
						</div>
					</div>
                    <div class="form-group" id="progressbar" style="display: none;">
                        <center>
                            <progress></progress>
                        </center>
                    </div>
					<?php                                         
						if($_SESSION['user_type'] == 1 || strpos($create[0]['permission_detail'],'new')){                                    
					?> 
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Nhập lại
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-success btn-label-left" name="register" id="submit">
                                <span><i class="fa fa-clock-o"></i></span>
                                Đồng ý
                            </button>
                        </div>
                    </div>
						<?php } ?>
                </form>

    </div>
</section>
