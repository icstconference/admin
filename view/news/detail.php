<?php 
    $actual_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER[REQUEST_URI];
    function cut_string($str, $len, $more) {
        if ($str == "" || $str == NULL)
            return $str;
        if (is_array($str))
            return $str;
        $str = trim($str);
        if (strlen($str) <= $len)
            return $str;
        $str = substr($str, 0, $len);
        if ($str != "") {
            if (!substr_count($str, " ")) {
                if ($more)
                    $str .= " ...";
                return $str;
            }
            while (strlen($str) && ($str[strlen($str) - 1] != " ")) {
                $str = substr($str, 0, -1);
            }
            $str = substr($str, 0, -1);
            if ($more)
                $str .= " ...";
        }
        return $str;
    }

    $newsinfo = $this->newsinfo;
    $catinfo = $this->catinfo;
    $configurl = $this->configurl;
?>
<style type="text/css">
    .indextitle1 {
        font-size: 50px;
        color: #fff;
        font-weight: 400;
        border-bottom: 3px solid #ff7733;
        padding-bottom: 0px;
        text-transform: uppercase;
    }
    .newstitle{
        font-size: 35px;
        color: #333;
        font-weight: 400;
        padding-bottom: 0px;
        text-transform: uppercase;
        text-align:center;
    }
    p{
        font-size: 16px;
        line-height: 24px;
        font-weight: 300;
    }
    .datetime{
        padding:15px 0;
        font-weight: 300;
        font-size:16px;
        color:#959595;
    }
    .view{
        padding:15px 0;
        font-weight: 300;
        font-size:16px;
        text-align:left;
        color:#959595;
    }
    .buttonsharefacebook{
        background: #ccc;
        padding: 14px 19px;
        border-radius: 50%;
        font-size: 16px;
        margin-right: 10px;
        color: #fff;
    }
    .buttonshare{
        background: #ccc;
        padding: 14px 17px;
        border-radius: 50%;
        font-size: 16px;
        margin-right: 10px;
        color: #fff;
    }
    .buttonshare:hover{
        color:#ff7733;
    }
    .buttonsharefacebook:hover{
        color:#ff7733;
    }
    .readmore{
        color: #fff;
        background: #ff7733;
        padding: 14px 30px;
        border-radius: 3px;
    }
    .readmore:hover{
        background: #333;
    }
    #sidebar h3{
        text-transform: none;
    }
    .linesidebar{
        width: 50px;
        border-bottom:2px solid #333;
        margin: 15px 0;
    }
    @media (min-width:1400px){
        #sidebar{
            padding:0;
        }
    }
    .google-maps {
        position: relative;
        height: 0;
    }
    .google-maps iframe {
        top: 0;
        left: 0;
        width: 100% !important;
        z-index:0;
    }
    .linkcategories{
        color: #333;
        font-size: 20px;
        padding: 5px 0;
        font-weight: 300;
    }
    .linkcategories:hover{
        color: #ff7733;
    }
    .linkcategories1{
        color: #fff;
        font-size: 22px;
        padding: 5px 0;
        font-weight: 300;
    }
    .linkcategories1:hover{
        color: #ff7733;
    }
    .bgtop{
        background:url('<?php echo URL;?>public/img/header.png') center no-repeat;background-size:cover;height:370px;width:100%;
    }
    .texttop{
        text-align:center;margin:0px;background:rgba(149,149,149,0.37);height:370px;width:100%;
    }
</style>
<section id="page-top" class="bgtop">
    <div class="row texttop">
        <div class="col-md-12" id="indexartist" style="padding-top:150px;">
            <div class="col-md-10 col-md-offset-1">
                <div style="margin-bottom:10px;">
                    <?php 
                        if($newsinfo[0]['news_display'] == 'news'){
                    ?>
                    <span class="indextitle1" >News</span>
                    <?php }else{ ?>
                    <span class="indextitle1" >Company Snapshot</span>
                    <?php } ?>
                </div>
                <div style="color:#fff;">
                    <a href="<?php echo URL;?>category/news/" class="linkcategories1">News</a> / 
                    <?php 
                        if($newsinfo[0]['news_display'] == 'news'){
                    ?>
                    <a href="<?php echo URL;?><?php echo $catinfo[0]['news_categories_url'];?>" class="linkcategories1"><?php echo $catinfo[0]['news_categories_name'];?></a> / 
                    <?php }else{ ?>
                    <a href="<?php echo URL.'company/';?><?php echo $catinfo[0]['news_categories_url'];?>" class="linkcategories1"><?php echo $catinfo[0]['news_categories_name'];?></a> / 
                    <?php } ?>
                    <a href="<?php echo URL;?><?php echo $newsinfo[0]['news_url'];?>" class="linkcategories1"><?php echo $newsinfo[0]['news_name'];?></a>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="feature" class="feature-section" style="padding:25px 0 50px;">
    <div class="container" style="padding: 0px;">
        <div class="col-md-9 col-lg-9 col-xs-12" id="main" style="padding: 0 20px 0 0;">
            <div class="row" style="margin:0;padding:40px 0;">
                <div class="col-md-12" style="padding: 0 0 25px;">
                    <div class="row" style="margin:0;">
                        <h3 style="margin-top:0;margin-bottom: 20px;">
                            <?php echo $newsinfo[0]['news_name'];?>
                        </h3>
                        <?php echo $newsinfo[0]['news_description'];?>
                        <div class="row" style="margin: 25px 0 10px;border-bottom: 1px solid #ddd;border-top: 1px solid #ddd;padding:10px 0;">
                            <div class="col-md-6">
                                <div class="view col-md-6">
                                    <i class="fa fa-clock-o"></i>&nbsp;<?php echo date('d F Y',strtotime($newsinfo[0]['news_create_date']));?>
                                </div>
                                <div class="datetime col-md-6">
                                    <i class="fa fa-comments" ></i>&nbsp; <?php 
                                        $comment = $this->comment;
                                        if($comment[0]['numcomment'] > 1){
                                            echo $comment[0]['numcomment'].' comments';
                                        }else{
                                            echo $comment[0]['numcomment'].' comment';
                                        }
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="datetime" style="text-align: right;">
                                    <a href="#" class="buttonsharefacebook"><i class="fa fa-facebook"></i></a>
                                    <a href="#" class="buttonshare"><i class="fa fa-twitter"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="padding: 0 0 25px;border-bottom:1px dotted #ddd;">
                    <div class="row" style="margin:0;">
                        <h3 style="margin-top:0;margin-bottom: 20px;">
                            <?php 
                                        $comment = $this->comment;
                                        if($comment[0]['numcomment'] > 1){
                                            echo $comment[0]['numcomment'].' comments';
                                        }else{
                                            echo $comment[0]['numcomment'].' comment';
                                        }
                                    ?>
                        </h3>
                    </div>
                    <div class="row" style="margin: 20px 0;">
                        <?php 
                            if($this->listcomment){
                                $i=0;
                                $numsilder = count($this->listcomment);
                                foreach($this->listcomment as $lm){
                                    $i++;
                                    if($i == $numsilder){
                        ?>
                        <div class="col-md-12" style="padding:0;">
                            <div class="col-md-2">
                                <div class="col-md-12" style="background:#ddd;">
                                    <center style="padding:35px 0;">
                                        <img src="<?php echo URL;?>public/img/user.png" class="img-responsive"/>
                                    </center>
                                </div>
                            </div>
                            <div class="col-md-10" style="padding:0;">
                                <div class="col-md-12" style="border:1px solid #ddd;border:1px solid #ddd;padding: 10px;font-size: 16px;line-height: 24px;color: #333;">
                                    <div class="row" style="margin:0;border-bottom:1px solid #ddd;padding: 10px 0;font-weight:300;">
                                        <?php echo $lm['comment_message'];?>
                                    </div>
                                    <div class="row" style="margin:0;">
                                        <div class="col-md-6" style="padding: 10px 0 0;">
                                            <?php echo $lm['comment_name'];?>
                                        </div>
                                        <div class="col-md-6" style="text-align:right;padding: 10px 1px 0 0;">
                                            <?php echo date('d-m-Y',strtotime($lm['comment_created_date']));?>
<!--                                             &nbsp;&nbsp;&nbsp;&nbsp;<a href="#" style="color:#ff7733;">REPLY</a>
 -->                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-md-12" style="margin: 20px 0 0;padding:0;">
                                    <div class="col-md-2">
                                        <div class="col-md-12" style="background:#ddd;">
                                            <center style="padding:35px 0;">
                                                <img src="<?php echo URL;?>public/img/user.png" class="img-responsive"/>
                                            </center>
                                        </div>
                                    </div>
                                    <div class="col-md-10" style="border:1px solid #ddd;padding: 10px;font-size: 16px;line-height: 24px;color: #333;">
                                        <div class="row" style="margin:0;border-bottom:1px solid #ddd;padding: 10px 0;font-weight:300;">
                                            Maecenas vestibulum ex at libero pulvinar, et iaculis diam condimentum. Proin nec est sit amet tortor egestas pulvinar. Etiam et lorem sagittis, suscipit turpis ac, viverra nulla. Praesent accumsan auctor commodo
                                        </div>
                                        <div class="row" style="margin:0;">
                                            <div class="col-md-6" style="padding: 10px 0 0;">
                                                USER 1
                                            </div>
                                            <div class="col-md-6" style="text-align:right;padding: 10px 1px 0 0;">
                                                2 days ago
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                            </div>
                            <?php }else{ ?>
                            <div class="col-md-12" style="padding:0;margin-bottom:20px;">
                                <div class="col-md-2">
                                    <div class="col-md-12" style="background:#ddd;">
                                        <center style="padding:35px 0;">
                                            <img src="<?php echo URL;?>public/img/user.png" class="img-responsive"/>
                                        </center>
                                    </div>
                                </div>
                                <div class="col-md-10" style="padding:0;">
                                    <div class="col-md-12" style="border:1px solid #ddd;border:1px solid #ddd;padding: 10px;font-size: 16px;line-height: 24px;color: #333;">
                                        <div class="row" style="margin:0;border-bottom:1px solid #ddd;padding: 10px 0;font-weight:300;">
                                            <?php echo $lm['comment_message'];?>
                                        </div>
                                        <div class="row" style="margin:0;">
                                            <div class="col-md-6" style="padding: 10px 0 0;">
                                                <?php echo $lm['comment_name'];?>
                                            </div>
                                            <div class="col-md-6" style="text-align:right;padding: 10px 1px 0 0;">
                                                <?php echo date('d-m-Y',strtotime($lm['comment_created_date']));?>
    <!--                                             &nbsp;&nbsp;&nbsp;&nbsp;<a href="#" style="color:#ff7733;">REPLY</a>
     -->                                        </div>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-12" style="margin: 20px 0 0;padding:0;">
                                        <div class="col-md-2">
                                            <div class="col-md-12" style="background:#ddd;">
                                                <center style="padding:35px 0;">
                                                    <img src="<?php echo URL;?>public/img/user.png" class="img-responsive"/>
                                                </center>
                                            </div>
                                        </div>
                                        <div class="col-md-10" style="border:1px solid #ddd;padding: 10px;font-size: 16px;line-height: 24px;color: #333;">
                                            <div class="row" style="margin:0;border-bottom:1px solid #ddd;padding: 10px 0;font-weight:300;">
                                                Maecenas vestibulum ex at libero pulvinar, et iaculis diam condimentum. Proin nec est sit amet tortor egestas pulvinar. Etiam et lorem sagittis, suscipit turpis ac, viverra nulla. Praesent accumsan auctor commodo
                                            </div>
                                            <div class="row" style="margin:0;">
                                                <div class="col-md-6" style="padding: 10px 0 0;">
                                                    USER 1
                                                </div>
                                                <div class="col-md-6" style="text-align:right;padding: 10px 1px 0 0;">
                                                    2 days ago
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                            <?php }}} ?>
                    </div>
                </div>
                <?php 
                    if($newsinfo[0]['comment'] > 0){
                ?>
                <div class="col-md-12" style="padding: 0;">
                <?php }else{ ?>
                <div class="col-md-12" style="padding: 45px 0 0;">
                <?php } ?>
                    <div class="col-md-12" style="padding:0;">
                            <div class="col-md-2">
                                <div class="col-md-12" style="background:#ddd;">
                                    <center style="padding:35px 0;">
                                        <img src="<?php echo URL;?>public/img/user.png" class="img-responsive"/>
                                    </center>
                                </div>
                            </div>
                            <div class="col-md-10" style="padding:0;">
                                <form action="<?php echo URL;?>comment/createcomment/" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="news_id" value="<?php echo $newsinfo[0]['news_id'];?>"/>
                                    <input type="hidden" name="comment_type" value="news"/>
                                    <textarea name="comment_message" class="form-control" style="border: 1px solid #ddd;padding: 10px;height: 80px;resize: none;background: #fff;border-radius: 3px;margin-bottom: 10px;box-shadow: none;" placeholder="YOUR MESSAGE" required></textarea>
                                    <input type="text" name="comment_name" class="form-control" style="border: 1px solid #ddd;height:35px;margin-right: 5px;padding: 10px;margin-bottom:10px;background: #fff;border-radius: 3px;" placeholder="YOUR NAME" required>
                                    <input type="text" name="comment_email" class="form-control" style="border: 1px solid #ddd;height:35px;/* margin-right:22px; */padding: 10px;margin-bottom:10px;background: #fff;border-radius: 3px;" placeholder="YOUR EMAIL" required>
                                    <div class="g-recaptcha" data-sitekey="6Le0IiATAAAAAGhvVC4bIM2n2jlkXAHtQLXbcA3g"></div>
                                    <div style="float:right;">
                                        <button type="submit" class="btn btn-sm buttonsubcribe btn-min-block" style="text-align:center;border-radius:3px;font-size:16px;">
                                            SUBMIT
                                        </button>
                                    </div>  
                                </form>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-lg-3 col-xs-12" id="sidebar" style="padding: 0 0 0 20px;margin-top: 5px;">
            <div class="row" style="margin:0;padding-top: 20px;padding-bottom: 10px;">
                <h3>
                    Categories
                </h3>
                <div class="linesidebar"></div>
                <div class="col-md-12" style="padding:0px;">
                    <?php 
                        if($this->listnewscategories){
                            foreach($this->listnewscategories as $lp){
                                if($newsinfo[0]['news_display'] == 'news'){
                    ?>
                    <div class="row" style="margin:0;border-bottom: 1px solid #eee;padding: 5px 0;">
                        <a href="<?php echo URL.$lp['news_categories_url'];?>" class="linkcategories">
                            <?php echo ucfirst($lp['news_categories_name']);?>
                        </a>
                    </div>
                    <?php }else{ ?>
                    <div class="row" style="margin:0;border-bottom: 1px solid #eee;padding: 5px 0;">
                        <a href="<?php echo URL.'compayny/'.$lp['news_categories_url'];?>" class="linkcategories">
                            <?php echo ucfirst($lp['news_categories_name']);?>
                        </a>
                    </div>
                    <?php }}} ?>
                </div>
            </div>
            <div class="row" style="margin:0;padding-top: 20px;padding-bottom: 10px;">
                <h3>
                    Popular posts
                </h3>
                <div class="linesidebar"></div>
                <div class="col-md-12" style="padding:0px;">
                    <?php 
                        if($this->topnews){
                            foreach($this->topnews as $lp){
                    ?>
                    <div class="row" style="margin:0;border-bottom: 1px solid #eee;padding: 5px 0;">
                        <a href="<?php echo URL.$lp['news_url'];?>" class="linkcategories" style="font-size:18px;">
                            <?php echo $lp['news_name'];?>
                        </a>
                    </div>
                    <?php }} ?>
                </div>
            </div>
        </div>
    </div>
</section>