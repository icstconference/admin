<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<?php                
	function cut_string($str, $len, $more) {
        if ($str == "" || $str == NULL)
            return $str;
        if (is_array($str))
            return $str;
        $str = trim($str);
        if (strlen($str) <= $len)
            return $str;
        $str = substr($str, 0, $len);
        if ($str != "") {
            if (!substr_count($str, " ")) {
                if ($more)
                    $str .= " ...";
                return $str;
            }
            while (strlen($str) && ($str[strlen($str) - 1] != " ")) {
                $str = substr($str, 0, -1);
            }
            $str = substr($str, 0, -1);
            if ($more)
                $str .= " ...";
        }
        return $str;
    }
            
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];
	$newsinfo = $this->newsinfo;
?>
<header class="page-header">
    <h2><?php echo cut_string($newsinfo[0]['news_name'],50,20);?></h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="<?php echo URL;?>news/">
                    <span>Tất cả bài viết</span>
                </a>
            </li>
            <li>
                <span><?php echo cut_string($newsinfo[0]['news_name'],50,20);?></span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Chỉnh sửa bài viết</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" role="form" enctype="multipart/form-data">
            <input type="hidden" name="news_id" value="<?php echo $newsinfo[0]['news_id'];?>" />
            <div class="form-group">
                <label class="col-sm-3 control-label">Tiêu đề tin tức</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" value="<?php echo $newsinfo[0]['news_name'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="news_name" name="news_name" required>
                </div>
            </div>
            <div class="form-group" style="display:none;">
                <label class="col-sm-3 control-label">Tiêu đề tin tức tiếng anh</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" value="<?php echo $newsinfo[0]['news_name_english'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="news_name_english" name="news_name_english" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Chọn sự kiện</label>
                <div class="col-sm-8">
                    <select id="s2_with_tag" data-plugin-selectTwo class="form-control populate" name="news_type">
                        <?php
							$listpermission = $this->listpermission;
							
                            if($this->listevent){
                                foreach($this->listevent as $lt){
                                    if($lt['event_id'] == $newsinfo[0]['event_id']){
                        ?>
                            <option value="<?php echo $lt['event_id'];?>" selected><?php echo $lt['event_name'];?></option>
                        <?php }else{ ?>
                            <option value="<?php echo $lt['event_id'];?>"><?php echo $lt['event_name'];?></option>
						<?php }}} ?>
                    </select>
                </div>
            </div>
            <div class="form-group" style="display:none;">
                <label class="col-sm-3 control-label">Chọn loại hiển thị</label>
                <div class="col-sm-3">
                    <select id="s2_with_tag" data-plugin-selectTwo class="form-control populate" name="news_display">
                        <?php 
                            if($newsinfo[0]['news_display'] == 'news'){
                        ?>
                            <option value="news" selected>News</option>
                            <option value="company">Company snapshot</option>
                        <?php }else{ ?>
                            <option value="news">News</option>
                            <option value="company" selected>Company snapshot</option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group" style="display:none;">
                <label class="col-sm-3 control-label">Chọn nhãn tin tức</label>
                <div class="col-sm-8">
                    <select multiple data-plugin-selectTwo class="form-control populate" style="padding:0px;" name="news_label[]">
                        <?php
                            if($this->listlabel){
                                foreach($this->listlabel as $lb){
                                    if(strpos($newsinfo[0]['label_id'],$lb['label_id'])){
                        ?>
                            <option value="<?php echo $lb['label_id'];?>" selected><?php echo $lb['label_name'];?></option>
                        <?php }else{ ?>
                            <option value="<?php echo $lb['label_id'];?>"><?php echo $lb['label_name'];?></option>
						<?php }}} ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Hình ảnh đại diện (489 x 349)</label>
                <div class="col-sm-3">
                    <?php
                        if(strlen($newsinfo[0]['news_image_thumb']) > 1){
                    ?>
                        <img src="<?php echo $newsinfo[0]['news_image_thumb'];?>" class="img-rounded imgupload" alt="avatar" id="primg">
                    <?php }else{ ?>
                        <img src="<?php echo URL;?>public/img/noneimage.jpg" class="img-rounded imgupload" alt="avatar" id="primg">
                    <?php } ?>
                    <input type="file" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="news_img" name="news_img" accept="image/jpg,image/png,image/jpeg,image/gif" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Nội dung tin tức</label>
                <div class="col-sm-8">
                    <textarea class="form-control" placeholder="Mô tả sản phẩm" rows="5" title="Tooltip for name" id="wysiwig_full1" name="news_description">
                        <?php echo $newsinfo[0]['news_description'];?>
                    </textarea>
                </div>
            </div>
            <div class="form-group" style="display:none;">
                <label class="col-sm-3 control-label">Nội dung tin tức tiếng anh</label>
                <div class="col-sm-8">
                    <textarea class="form-control" placeholder="Mô tả sản phẩm" rows="5" title="Tooltip for name" id="wysiwig_full2" name="news_description_english">
                        <?php echo $newsinfo[0]['news_description_english'];?>
                    </textarea>
                </div>
            </div>
			<div class="form-group">
                <label class="col-sm-3 control-label">Ngày viết</label>
                <div class="col-sm-3">
                    <input type="datetime" class="form-control" value="<?php echo date('Y-m-d h:i:s',strtotime($newsinfo[0]['news_create_date']));?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="news_create_date" name="news_create_date" required>
                </div>
            </div>
            <div class="form-group" id="progressbar" style="display: none;">
                <center>
                    <progress></progress>
                </center>
            </div>
            <div class="form-group" style="margin-top: 40px;">
                <div class="col-sm-offset-4 col-sm-2">
                    <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                        <span><i class="fa fa-clock-o txt-danger"></i></span>
                        Nhập lại
                    </button>
                </div>
                <div class="col-sm-2">
                    <button type="button"  class="btn btn-success btn-label-left" name="register" id="update">
                        <span><i class="fa fa-clock-o"></i></span>
                        Đồng ý
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>