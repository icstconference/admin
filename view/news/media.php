<style>
	html.fixed .content-with-menu .inner-toolbar, html.fixed.inner-menu-opened .content-with-menu .inner-toolbar{
		left:0px !important;
	}
	html.fixed .inner-body{
		margin-left:0px;
	}
</style>
<header class="page-header">
    <h2>Hình ảnh bài viết</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Hình ảnh bài viết</span></li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<section class="panel" style="margin-top:30px;">
    <header class="panel-heading">
        <h2 class="panel-title">Cách thêm hình mới : Nhấp vào icon hình ảnh khi viết bài mới > chọn Duyệt máy chủ > Chọn hình Tải lên</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <div class="content-with-menu-container">
            <iframe src="<?php echo URL;?>public/ckfinder/ckfinder.html" style="width:100%;height:650px;border:none;box-shadow:none;"></iframe>
        </div> 
    </div>
</section>