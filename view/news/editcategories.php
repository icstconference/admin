<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<?php
    $categoriesinfo = $this->categoriesinfo;
?>
<header class="page-header">
    <h2><?php echo $categoriesinfo[0]['news_categories_name'];?></h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="<?php echo URL;?>news/categories/">
                    <span>
                        Chuyên mục bài viết
                    </span>
                </a>
            </li>
            <li>
                <span>
                    <?php echo $categoriesinfo[0]['news_categories_name'];?>
                </span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Chỉnh sửa Chuyên mục</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
            <input type="hidden" name="nutrition_type" id="nutrition_type" value="">
            <div class="form-group">
                <label class="col-sm-3 control-label">Tên loại tin tức</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" value="<?php echo $categoriesinfo[0]['news_categories_name'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="news_type" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Tên loại tin tức tieng anh</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" value="<?php echo $categoriesinfo[0]['news_categories_name_english'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="news_type_english" required>
                </div>
            </div>
            <?php
                if($this->listcategoriesnews){
            ?>
            <div class="form-group">
                <label class="col-sm-3 control-label">Chọn loại tin tức cha</label>
                <div class="col-sm-8">
                    <select id="s2_with_tag" class="populate placeholder" name="news_fathertype">
                        <?php
                            if($categoriesinfo[0]['news_categories_father'] != 0){
                                foreach($this->listcategoriesnews as $lt){
                                    if($categoriesinfo[0]['news_categories_father'] == $lt['news_categories_id']){
                        ?>
                            <option value="<?php echo $lt['news_categories_id'];?>"><?php echo $lt['news_categories_name'];?></option>
                        <?php }} ?>
                            <option value="0">Thư mục chính</option>
                        <?php
                            foreach($this->listcategoriesnews as $lt){
                                if($categoriesinfo[0]['news_categories_father'] != $lt['news_categories_id']){
                        ?>
                            <option value="<?php echo $lt['news_categories_id'];?>"><?php echo $lt['news_categories_name'];?></option>
                        <?php }}}else{?>
                            <option value="0">Thư mục chính</option>
                        <?php
                            foreach($this->listcategoriesnews as $lt){
                        ?>
                            <option value="<?php echo $lt['news_categories_id'];?>"><?php echo $lt['news_categories_name'];?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <?php } ?>
			<?php                                         
				if($_SESSION['user_type'] == 1 || strpos($edit[0]['permission_detail'],'new')){                                    
			?>  
            <div class="form-group" style="margin-top: 40px;">
                <div class="col-sm-offset-4 col-sm-2">
                    <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                        <span><i class="fa fa-clock-o txt-danger"></i></span>
                        Nhập lại
                    </button>
                </div>
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-success btn-label-left" name="register" id="submit">
                        <span><i class="fa fa-clock-o"></i></span>
                        Đồng ý
                    </button>
                </div>
            </div>
			<?php } ?>
        </form>
    </div>
</section>