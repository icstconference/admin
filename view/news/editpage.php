<?php 
    $pageinfo = $this->pageinfo;
?>
<?php                            
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];
?>
<header class="page-header">
    <h2><?php echo $pageinfo[0]['page_name'];?></h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="<?php echo URL;?>page">
                    <span>Tất cả trang</span>
                </a>
            </li>
            <li>
                <span><?php echo $pageinfo[0]['page_name'];?></span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Chỉnh sửa trang</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" role="form" enctype="multipart/form-data">
            <input type="hidden" name="news_id" value="<?php echo $pageinfo[0]['page_id'];?>" />
            <div class="form-group">
                <label class="col-sm-3 control-label">Tiêu đề trang</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" value="<?php echo $pageinfo[0]['page_name'];?>" data-toggle="tooltip" data-placement="bottom" title="Nhập tiêu đề trang" id="news_name" name="news_name" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Tiêu đề trang tiếng anh</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" value="<?php echo $pageinfo[0]['page_name_english'];?>" data-toggle="tooltip" data-placement="bottom" title="Nhập tiêu đề trang tiếng anh" id="news_name_english" name="news_name_english" required>
                </div>
            </div>
            <div class="form-group" style="display:none;">
                <label class="col-sm-3 control-label">Chọn loại trang</label>
                <div class="col-sm-3">
                    <select id="s2_with_tag" data-plugin-selectTwo class="form-control populate" name="page_type">
                        <?php 
                            if($pageinfo[0]['page_type'] == 0){
                        ?>
                        <option value="0" selected>Không có</option> 
                        <option value="1">Giới thiệu</option> 
                        <option value="2">Sản phẩm</option> 
                        <option value="3">An toàn</option> 
                        <?php }elseif($pageinfo[0]['page_type'] == 1){ ?>
                        <option value="0" >Không có</option> 
                        <option value="1" selected>Giới thiệu</option> 
                        <option value="2">Sản phẩm</option> 
                        <option value="3">An toàn</option> 
                        <?php }elseif($pageinfo[0]['page_type'] == 2){ ?>
                        <option value="0">Không có</option> 
                        <option value="1">Giới thiệu</option> 
                        <option value="2" selected>Sản phẩm</option> 
                        <option value="3">An toàn</option> 
                        <?php }elseif($pageinfo[0]['page_type'] == 3){ ?>
                        <option value="0" >Không có</option> 
                        <option value="1">Giới thiệu</option> 
                        <option value="2">Sản phẩm</option> 
                        <option value="3" selected>An toàn</option> 
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group" style="display:none;">
                <label class="col-sm-3 control-label">Hình ảnh đại diện (489 x 349)</label>
                <div class="col-sm-3">
                    <?php
                        if(strlen($pageinfo[0]['page_thumbnail']) > 1){
                    ?>
                        <img src="<?php echo $pageinfo[0]['page_thumbnail'];?>" class="img-rounded imgupload" alt="avatar" id="primg">
                    <?php }else{ ?>
                        <img src="<?php echo URL;?>public/img/noneimage.jpg" class="img-rounded imgupload" alt="avatar" id="primg">
                    <?php } ?>
                    <input type="file" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="news_img" name="news_img" accept="image/jpg,image/png,image/jpeg,image/gif"  required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Nội dung trang</label>
                <div class="col-sm-8">
                    <textarea class="form-control" placeholder="Nội dung trang" rows="5" title="Tooltip for name" id="wysiwig_full1" name="news_description">
                        <?php echo $pageinfo[0]['page_description'];?>
                    </textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Nội dung trang tiếng anh</label>
                <div class="col-sm-8">
                    <textarea class="form-control" placeholder="Nội dung trang tiếng anh" rows="5" title="Tooltip for name" id="wysiwig_full2" name="news_description_english">
                        <?php echo $pageinfo[0]['page_description_english'];?>
                    </textarea>
                </div>
            </div>
            <div class="form-group" id="progressbar" style="display: none;">
                <center>
                    <progress></progress>
                </center>
            </div>
			<?php                                         
               if(strpos($edit[0]['permission_detail'],'page') || $_SESSION['user_type'] == 1){                                    
            ?> 
            <div class="form-group" style="margin-top: 40px;">
                <div class="col-sm-offset-4 col-sm-2">
                    <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                        <span><i class="fa fa-clock-o txt-danger"></i></span>
                        Nhập lại
                    </button>
                </div>
                <div class="col-sm-2">
                    <button type="button" class="btn btn-success btn-label-left" name="register" id="update">
                        <span><i class="fa fa-clock-o"></i></span>
                        Đồng ý
                    </button>
                </div>
            </div>
			<?php } ?>
        </form>
    </div>
</section>