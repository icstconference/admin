<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
	#s2id_autogen2{
		padding:0px;
	}
</style>
<header class="page-header">
    <h2>Tất cả bài viết</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Tất cả bài viết</span></li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<?php                            
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];
	$listpermission = $this->listpermission;
?>
<div class="row" style="margin: 0 0 10px;text-align:right;">
    <a href="<?php echo URL;?>news/addnews/" class="mb-xs mt-xs mr-xs btn btn-primary">
        <i class="fa fa-user-plus"></i>&nbsp;Thêm bài viết
    </a>
    <button href="#modalHeaderColorPrimary" class="mb-xs mt-xs mr-xs modal-basic btn btn-danger" disabled id="deletepost">
        <i class="fa fa-trash"></i>&nbsp;Xóa bài viết
    </button>
</div>
<span id="typepost" rel="news"></span>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Tất cả bài viết</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-default">
            <thead>
                <tr>
                    <th style="vertical-align:middle;"><input name="select_all" value="1" id="example-select-all" type="checkbox" /></th>
                    <th style="vertical-align:middle;">Bài viết</th>
                    <th style="vertical-align:middle;">Thumbnail</th>
                    <th style="vertical-align:middle;">Chuyên mục</th>
                    <th style="vertical-align:middle;">Ngày viết</th>
                    <th style="vertical-align:middle;">Tùy chỉnh</th>
                </tr>
            </thead>
            <tbody>
            <?php
                if($this->listnews){
                    $i=0;
					if($_SESSION['user_type'] == 1 || strpos($create[0]['permission_detail'],'new') || strpos($edit[0]['permission_detail'],'new') || strpos($delete[0]['permission_detail'],'new')){
                        foreach($this->listnews as $lp){
                            $i++;
            ?>
                <tr>
                    <td style="vertical-align:middle;"><?php echo $lp['news_id'];?></td>
                    <td style="vertical-align:middle;width:30%;">
                        <?php echo $lp['news_name'];?>
                    </td>
                    <td style="vertical-align:middle;">
                        <center>
                            <?php
                                if(strlen($lp['news_image_thumb']) > 1){
                            ?>
                                <img src="<?php echo $lp['news_image_thumb'];?>" style="width:60px;"/>
                            <?php } else {?>
                                <img src="<?php echo URL;?>public/img/noneimage.jpg" style="width:60px;"/>
                            <?php } ?>
                        </center>
                    </td>
                    <td style="vertical-align:middle;">
                        <?php
                           if($this->listevent){
                                foreach($this->listevent as $lt){
                                    if($lp['event_id'] == $lt['event_id']){
                        ?>
                        <?php echo $lt['event_name'];?>
                        <?php }}}?>
                    </td>
                    <td style="vertical-align:middle;">
                        <?php echo $lp['news_create_date'];?>
                    </td>
                    <td style="vertical-align:middle;text-align:center;width:22%;">
						<?php                                         
							if(strpos($edit[0]['permission_detail'],'new') || $_SESSION['user_type'] == 1){                                    
						?>
                        <a class="btn btn-info" href="<?php echo URL;?>news/editnews/<?php echo $lp['news_id'];?>" data-toggle="tooltip" data-placement="bottom" title="Sửa bài viết"><i class="fa fa-edit"></i></a>
						<?php } ?>                                    
						<?php                                         
							if(strpos($delete[0]['permission_detail'],'new') || $_SESSION['user_type'] == 1){                                    
						?> 
                        <a class="btn btn-danger"href="<?php echo URL;?>news/deletenews/<?php echo $lp['news_id'];?>" data-toggle="tooltip" data-placement="bottom" title="Xóa bài viết"><i class="fa fa-trash"></i></a>
                        <?php } ?>
                        <a class="btn btn-warning" href="<?php echo URL;?>news/copy/<?php echo $lp['news_id'];?>" data-toggle="tooltip" data-placement="bottom" title="Sao chép bài viết"><i class="fa fa-copy"></i></a>
                        <?php 
                            $allsetting = $this->allsetting;
                            if($allsetting[0]['config_url'] == 'dai'){
                        ?>
                        <a class="btn btn-success" href="<?php echo URL;?>news/detail/<?php echo $lp['news_url'];?>.html" data-toggle="tooltip" data-placement="bottom" title="Xem bài viết" target="_blank"><i class="fa fa-eye"></i></a>
                        <?php }else{ ?>
                        <a class="btn btn-success" href="<?php echo URL;?><?php echo $lp['news_url'];?>.html" data-toggle="tooltip" data-placement="bottom" title="Xem bài viết" target="_blank"><i class="fa fa-eye"></i></a>
                        <?php } ?>
                    </td>
                </tr>
					<?php }}else{ 
					foreach($this->listnews as $lp){
                        $i++;
						if(strpos($listpermission,$lp['news_categories_id'])){
                    ?>
                    <tr>
                        <td style="vertical-align:middle;">
							<?php echo $i;?>
						</td>
						<td style="vertical-align:middle;width:30%;">
							<?php echo $lp['news_name'];?>
						</td>
                        <td style="vertical-align:middle;">
                            <?php
                                if(strlen($lp['news_image_thumb']) > 1){
                            ?>
                            <img src="<?php echo $lp['news_image_thumb'];?>" style="width:60px;"/>
                            <?php } else {?>
                            <img src="<?php echo URL;?>public/img/noneimage.jpg" style="width:60px;"/>
                            <?php } ?>
                        </td>
                        <td style="vertical-align:middle;">
                            <?php
                                if($this->listcategories){
                                    foreach($this->listcategories as $lt){
                                        if($lp['news_categories_id'] == $lt['news_categories_id']){
                            ?>
                                <?php echo $lt['news_categories_name'];?>
                            <?php }}}?>
                        </td>
                        <td style="vertical-align:middle;">
                            <?php echo $lp['news_create_date'];?>
                        </td>
                        <td style="vertical-align:middle;text-align:center;">
							<?php                                         
								if(strpos($edit[0]['permission_detail'],'new') || $_SESSION['user_id'] == 1){                                    
							?>
                            <a class="btn btn-info" href="<?php echo URL;?>news/editnews/<?php echo $lp['news_id'];?>" data-toggle="tooltip" data-placement="bottom" title="Sửa bài viết"><i class="fa fa-edit"></i>&nbsp;</a>
							<?php } ?>                                    
							<?php                                         
								if(strpos($delete[0]['permission_detail'],'new') || $_SESSION['user_id'] == 1){                                    
							?> 
                            <a class="btn btn-danger" href="<?php echo URL;?>news/deletenews/<?php echo $lp['news_id'];?>" data-toggle="tooltip" data-placement="bottom" title="Xóa bài viết"><i class="fa fa-trash"></i>&nbsp;</a>
                            <?php } ?>
                            <a class="btn btn-warning" href="<?php echo URL;?>news/copy/<?php echo $lp['news_id'];?>" data-toggle="tooltip" data-placement="bottom" title="Sao chép bài viết"><i class="fa fa-copy"></i>&nbsp;</a>
                            <?php 
                                $allsetting = $this->allsetting;
                                if($allsetting[0]['config_url'] == 'dai'){
                            ?>
                            <a class="btn btn-success" href="<?php echo URL;?>news/detail/<?php echo $lp['news_url'];?>.html" data-toggle="tooltip" data-placement="bottom" title="Xem bài viết" target="_blank"><i class="fa fa-eye"></i></a>
                            <?php }else{ ?>
                            <a class="btn btn-success" href="<?php echo URL;?><?php echo $lp['news_url'];?>.html" data-toggle="tooltip" data-placement="bottom" title="Xem bài viết" target="_blank"><i class="fa fa-eye"></i></a>
                            <?php } ?>
                        </td>
                    </tr>
					<?php }}}} ?>
                </tbody>
            </table>
        </div>
    </section>
    <div id="modalHeaderColorPrimary" class="modal-block modal-header-color modal-block-primary mfp-hide">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Thông báo</h2>
            </header>
            <div class="panel-body">
                <div class="modal-wrapper">
                    <div class="modal-icon">
                        <i class="fa fa-question-circle"></i>
                    </div>
                    <div class="modal-text">
                        <p>Xóa vĩnh viễn ?</p>
                    </div>
                </div>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button class="btn btn-primary modal-confirm">Đồng ý</button>
                        <button class="btn btn-default modal-dismiss">Hủy bỏ</button>
                    </div>
                </div>
            </footer>
        </section>
    </div>