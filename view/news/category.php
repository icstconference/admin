<?php
class controller_category extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    public $arrcategory = array();

    public function GetTreeByUserId($catid) {
        $product = new model_product();

        if ($product->GetChildByParentCatid($catid)) {
            $listchild = $product->GetChildByParentCatid($catid);
            foreach ($listchild as $ld) {
                array_push($this->arrcategory,$ld['categories_id']);
                if ($product->GetChildByParentCatid($ld['categories_id'])) {
                    $this->GetTreeByUserId($ld['categories_id']);
                }
            }
        }
    }

    public function product($path = null,$page = 1){
        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
			$this->view->aboutus = $allsetting[0]['config_url_aboutus'];
            $this->view->laboratories = $allsetting[0]['config_url_laboratories'];
			$this->view->emaillink = $allsetting[0]['config_url_email'];
            $this->view->internal = $allsetting[0]['config_url_internal'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOnByPostion('all','ngang')){
            $allmenufather = $model1->SelectAllMenuFatherOnByPostion('all','ngang');

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        if($model1->SelectAllMenuFatherOnByPostion('product','doc')){
            $allmenufatherproduct = $model1->SelectAllMenuFatherOnByPostion('product','doc');

            foreach($allmenufatherproduct as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufatherproduct[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenuproduct = $allmenufatherproduct;
        }

        if($advertise->GetAdvertiseByPosition(1)){
            $this->view->listadvertise1 = $advertise->GetAdvertiseByPosition(1);
        }

        if($advertise->GetAdvertiseByPosition(2)){
            $this->view->listadvertise2 = $advertise->GetAdvertiseByPosition(2);
        }

        if($advertise->GetAdvertiseByPosition(3)){
            $this->view->listadvertise3 = $advertise->GetAdvertiseByPosition(3);
        }

        if($news->SelectTopNewsView(4)){
            $this->view->listnewsview = $news->SelectTopNewsView(4);
        }

        if($news->SelectTopNewsRelease(4)){
            $this->view->listnewsrelease = $news->SelectTopNewsRelease(4);
        }

        if($linker->SelectAlllinker()){
            $this->view->listlinker = $linker->SelectAlllinker();
        }

        if($news->SelectTopNewsRelease(5)){
            $this->view->listnewsnew = $news->SelectTopNewsRelease(5);
        }

        $model4 = new model_news();

        if($model4->SelectAllCategoriesNews()){
            $this->view->listcategoriesnews = $model4->SelectAllCategoriesNews();
        }

        if($model4->SelectTopNews(5)){
            $this->view->topnew = $model4->SelectTopNews(5);
        }

        if($path){
            $part = explode('-',$path);
            $id = $part[count($part)-1];
            $result = $model3->GetCategoriesProductById($id);

            if($result){
                $this->view->categoriesinfo = $result;

                $this->view->title = $result[0]['categories_name'].'-'.$allsetting[0]['config_title'];
                $this->view->description = $result[0]['categories_name'].'-'.$allsetting[0]['config_description'];
                $this->view->keyword = $result[0]['categories_name'].','.$allsetting[0]['config_keyword'];

                $this->GetTreeByUserId($id);

                $iteminpage = 9;

                $pagination = new lib_pagination();
                $pagination->SetCurrentPage($page);
                $pagination->SetRowInPage($iteminpage);
                $pagination->SetUrl(URL . 'category/product/'.$path.'/@page');
                $pagination->SetRange(4);
                $pagination->SetLanguage(array(
                    'first'     => '<<',
                    'previous'  => '<',
                    'next'      => '>',
                    'last'      => '>>'
                ));
                $paginator  = $pagination->BuildPagination($this->GetBlogDB($id));

                $this->view->paginator  = $paginator['paginator'];

                $this->view->listnewsproduct   = $model3->PaginationProductByCatId($page,$iteminpage,$id);
            }
        }

        $this->view->layout('layout');
        $this->view->Render('category/product');
        return true;
    }

    private function GetBlogDB($catid) {
        $db = new lib_db();
        return $db->query('select pr.*,cat.*
            from tbl_product as pr
            join tbl_categories as cat on cat.categories_id = pr.categories_id
            where pr.categories_id ='.$catid.' order by pr.product_id DESC');
    }

    public function news($path = null,$page = 1){

        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $partner = new model_partner();

        $library = new model_library();
		
		$video = new model_video();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
			$this->view->aboutus = $allsetting[0]['config_url_aboutus'];
            $this->view->laboratories = $allsetting[0]['config_url_laboratories'];
			$this->view->emaillink = $allsetting[0]['config_url_email'];
            $this->view->internal = $allsetting[0]['config_url_internal'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOn()){
            $allmenufather = $model1->SelectAllMenuFatherOn();

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        $model4 = new model_news();
		
        if($path){
            $part = explode('_',$path);
			if(count($part) > 1){
				$id = $part[count($part)-1];
				$result = $model4->GetCategoriesById($id);

				if($result){
					$this->view->categoriesinfo = $result;

					if($this->view->lang == 'vi'){
						$this->view->title = $result[0]['news_categories_name'].'-'.$allsetting[0]['config_title'];
						$this->view->description = $result[0]['news_categories_name'].'-'.$allsetting[0]['config_description'];
						$this->view->keyword = $result[0]['news_categories_name'].','.$allsetting[0]['config_keyword'];
					}else{
						$this->view->title = $result[0]['news_categories_name_english'].'-'.'Institute for Computational Science and Technology';
						$this->view->description = $result[0]['news_categories_name_english'].'-'.$allsetting[0]['config_description'];
						$this->view->keyword = $result[0]['news_categories_name_english'].','.$allsetting[0]['config_keyword'];
					}
					
					if($page == 1){
						$iteminpage = 9;
					}

					if($page > 1){
						$iteminpage = 9;
					}

					$pagination = new lib_pagination();
					$pagination->SetCurrentPage($page);
					$pagination->SetRowInPage($iteminpage);
					$pagination->SetUrl(URL . 'category/news/'.$path.'/@page');
					$pagination->SetRange(4);
					$pagination->SetLanguage(array(
						'first'     => '<<',
						'previous'  => '<',
						'next'      => '>',
						'last'      => '>>'
					));
					$paginator  = $pagination->BuildPagination($this->GetBlogDB1($id));

					$this->view->paginator  = $paginator['paginator'];

					$listnew   = $model4->PaginationNews($page,$iteminpage,$id);

					foreach ($listnew as $key => $value) {
						if($news->GetCategoriesNewsById($value['news_categories_id'])){
							$newsinfo = $news->GetCategoriesNewsById($value['news_categories_id']);
							$listnew[$key]['category'] = $newsinfo[0];
						}
					}

					$this->view->listnew = $listnew;
				}
			}else{
				
				$page = $path;
				
				$iteminpage = 9;
				
				$pagination = new lib_pagination();
				$pagination->SetCurrentPage($page);
				$pagination->SetRowInPage($iteminpage);
				$pagination->SetUrl(URL . 'category/news/@page');
				$pagination->SetRange(4);
				$pagination->SetLanguage(array(
					'first'     => '<<',
					'previous'  => '<',
					'next'      => '>',
					'last'      => '>>'
				));
				$paginator  = $pagination->BuildPagination($this->GetBlogDB1(0));

				$this->view->paginator  = $paginator['paginator'];

				$listnew   = $model4->PaginationNews($page,$iteminpage,0);
				
				foreach ($listnew as $key => $value) {
					if($news->GetCategoriesNewsById($value['news_categories_id'])){
						$newsinfo = $news->GetCategoriesNewsById($value['news_categories_id']);
						$listnew[$key]['category'] = $newsinfo[0];
					}
				}

				$this->view->listnew = $listnew;
			}
        }else{
            if($page == 1){
                $iteminpage = 9;
            }

            if($page > 1){
                $iteminpage = 9;
            }

            $pagination = new lib_pagination();
            $pagination->SetCurrentPage($page);
            $pagination->SetRowInPage($iteminpage);
            $pagination->SetUrl(URL . 'category/news/@page');
            $pagination->SetRange(4);
            $pagination->SetLanguage(array(
                'first'     => '<<',
                'previous'  => '<',
                'next'      => '>',
                'last'      => '>>'
            ));
            $paginator  = $pagination->BuildPagination($this->GetBlogDB1(0));

            $this->view->paginator  = $paginator['paginator'];

            $listnew   = $model4->PaginationNews($page,$iteminpage,0);
			
            foreach ($listnew as $key => $value) {
                if($news->GetCategoriesNewsById($value['news_categories_id'])){
                    $newsinfo = $news->GetCategoriesNewsById($value['news_categories_id']);
                    $listnew[$key]['category'] = $newsinfo[0];
                }
            }

            $this->view->listnew = $listnew;
        }

        $this->view->numpage = $page;

		$useragent=$_SERVER['HTTP_USER_AGENT'];
		
		if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
			$this->view->layout('layout-mobile');
			$this->view->Render('mobile/news');
			return true;
		}else{
			$this->view->layout('layout');
			$this->view->Render('category/news');
			return true;
		}
    }

    private function GetBlogDB1($catid) {
        $db = new lib_db();

        if($catid != 0){
            return $db->query('select *
                from tbl_news
                where news_categories_id ='.$catid.' order by news_create_date DESC');
        }else{
            return $db->query('select *
                from tbl_news order by news_create_date DESC');
        }
    }

    public function personal($path = null){
        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
			$this->view->aboutus = $allsetting[0]['config_url_aboutus'];
            $this->view->laboratories = $allsetting[0]['config_url_laboratories'];
			$this->view->emaillink = $allsetting[0]['config_url_email'];
            $this->view->internal = $allsetting[0]['config_url_internal'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOnByPostion('all','ngang')){
            $allmenufather = $model1->SelectAllMenuFatherOnByPostion('all','ngang');

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        if($model1->SelectAllMenuFatherOnByPostion('product','doc')){
            $allmenufatherproduct = $model1->SelectAllMenuFatherOnByPostion('product','doc');

            foreach($allmenufatherproduct as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufatherproduct[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenuproduct = $allmenufatherproduct;
        }

        $personal = new model_personal();
		$news = new model_news();

        if($personal->GetAllPersonalCategories()){
            $this->view->listcategories = $personal->GetAllPersonalCategories();
        }

        if($path){
            $part = explode('_',$path);
            $id = $part[count($part)-1];
            $result = $personal->GetPersonalCategoriesById($id);

            if($result){
                $this->view->categoriesinfo = $result;

				if($this->view->lang == 'vi'){
					$this->view->title = $result[0]['personal_categories_name'].'-'.$allsetting[0]['config_title'];
					$this->view->description = $result[0]['personal_categories_name'].'-'.$allsetting[0]['config_description'];
					$this->view->keyword = $result[0]['personal_categories_name'].','.$allsetting[0]['config_keyword'];
				}else{
					$this->view->title = $result[0]['personal_categories_name_english'].'-'.'Institute for Computational Science and Technology';
					$this->view->description = $result[0]['personal_categories_name_english'].'-'.$allsetting[0]['config_description'];
					$this->view->keyword = $result[0]['personal_categories_name_english'].','.$allsetting[0]['config_keyword'];
				}
				
				$this->view->listpersonal = $personal->GetAllPersonalByCatId($result[0]['personal_categories_id']);
            }
        }else{
            $this->view->title = 'Nhân sự'.'-'.$allsetting[0]['config_title'];
            $this->view->description = 'Nhân sự'.'-'.$allsetting[0]['config_description'];
            $this->view->keyword = 'Nhân sự'.','.$allsetting[0]['config_keyword'];

            $result = $personal->GetPersonalCategoriesById(1);

             $this->view->categoriesinfo = $result;

            $this->view->listpersonal = $personal->GetAllPersonalByCatId(1);
        }

        $this->view->layout('layout');
        $this->view->Render('category/personal');
        return true;
    }

    public function research($path = null){
        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
			$this->view->aboutus = $allsetting[0]['config_url_aboutus'];
            $this->view->laboratories = $allsetting[0]['config_url_laboratories'];
			$this->view->emaillink = $allsetting[0]['config_url_email'];
            $this->view->internal = $allsetting[0]['config_url_internal'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOnByPostion('all','ngang')){
            $allmenufather = $model1->SelectAllMenuFatherOnByPostion('all','ngang');

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        if($model1->SelectAllMenuFatherOnByPostion('product','doc')){
            $allmenufatherproduct = $model1->SelectAllMenuFatherOnByPostion('product','doc');

            foreach($allmenufatherproduct as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufatherproduct[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenuproduct = $allmenufatherproduct;
        }

        $news = new model_news();

        $personal = new model_personal();

        $form = new model_form();

        if($news->SelectAllNewsCategoriesChildByFatherId(10)){
            $this->view->listcategories = $news->SelectAllNewsCategoriesChildByFatherId(10);
        }

        if($path){
            $part = explode('_',$path);
            $id = $part[count($part)-1];
            $result = $news->GetCategoriesById($id);

            if($result){
                $this->view->categoriesinfo = $result;

                if($this->view->lang == 'vi'){
					$this->view->title = $result[0]['news_categories_name'].'-'.$allsetting[0]['config_title'];
					$this->view->description = $result[0]['news_categories_name'].'-'.$allsetting[0]['config_description'];
					$this->view->keyword = $result[0]['news_categories_name'].','.$allsetting[0]['config_keyword'];
				}else{
					$this->view->title = $result[0]['news_categories_name_english'].'-'.'Institute for Computational Science and Technology';
					$this->view->description = $result[0]['news_categories_name_english'].'-'.$allsetting[0]['config_description'];
					$this->view->keyword = $result[0]['news_categories_name_english'].','.$allsetting[0]['config_keyword'];
				}
				
                $this->view->listnews = $news->SelectAllNewsByCatId1($result[0]['news_categories_id']);

                if($personal->GetAllPersonalLabByCatId($result[0]['news_categories_id'])){
                    $this->view->personallab = $personal->GetAllPersonalLabByCatId($result[0]['news_categories_id']);
                }

                if($form->GetForm1ByCatId($result[0]['news_categories_id'])){
                    $this->view->form1 = $form->GetForm1ByCatId($result[0]['news_categories_id']);
                }

                if($form->GetForm2ByCatId($result[0]['news_categories_id'])){
                    $this->view->form2 = $form->GetForm2ByCatId($result[0]['news_categories_id']);
                }
            }
        }else{
            if($this->view->lang == 'vi'){
				$this->view->title = 'Nghiên cứu'.'-'.$allsetting[0]['config_title'];
				$this->view->description = 'Nghiên cứu'.'-'.$allsetting[0]['config_description'];
				$this->view->keyword = 'Nghiên cứu'.','.$allsetting[0]['config_keyword'];
			}else{
				$this->view->title = 'Research'.'-'.$allsetting[0]['config_title'];
				$this->view->description = 'Research'.'-'.$allsetting[0]['config_description'];
				$this->view->keyword = 'research'.','.$allsetting[0]['config_keyword'];
			}
			
            $result = $news->GetCategoriesById(11);

            $this->view->categoriesinfo = $result;

            $this->view->listnews = $news->SelectAllNewsByCatId1($result[0]['news_categories_id']);

            if($personal->GetAllPersonalLabByCatId(11)){
                $this->view->personallab = $personal->GetAllPersonalLabByCatId(11);
            }

            if($form->GetForm1ByCatId(11)){
                $this->view->form1 = $form->GetForm1ByCatId(11);
            }

            if($form->GetForm2ByCatId(11)){
                $this->view->form2 = $form->GetForm2ByCatId(11);
            }
        }

        $this->view->layout('layout');
        $this->view->Render('category/research');
        return true;
    }

    public function file($path = null){
        $file = new model_file();

        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
			$this->view->aboutus = $allsetting[0]['config_url_aboutus'];
            $this->view->laboratories = $allsetting[0]['config_url_laboratories'];
			$this->view->emaillink = $allsetting[0]['config_url_email'];
            $this->view->internal = $allsetting[0]['config_url_internal'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOn()){
            $allmenufather = $model1->SelectAllMenuFatherOn();

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        if($file->GetAllFileCategoriesFather()){
            $listcategoriesfather = $file->GetAllFileCategoriesFather();

            $this->view->listcategoriesfather = $listcategoriesfather;
        }

        if($path){
            $part = explode('_',$path);
            $id = $part[count($part)-1];
            $result = $file->GetFileCategoriesById($id);

            if($result){
                $listallcategories = $file->GetAllFileCategories1();

                foreach ($result as $key => $value) {
                    if($file->GetAllFileCategoriesChildByFatherId1($value['file_categories_id'])){
                        $child = $file->GetAllFileCategoriesChildByFatherId1($value['file_categories_id']);
                        $filechild = $file->GetAllFileByCategoriesId($value['file_categories_id']);

                        foreach ($child as $key1 => $value1) {
                            $child1 = array();
                            $filechild1 = $file->GetAllFileByCategoriesId($value1['file_categories_id']);

                            foreach ($listallcategories as $value2) {
                                if(strpos($value2['file_categories_father'], $value1['file_categories_id'])){
                                    $child1[] = $value2;
                                }

                                if($value2['file_categories_father'] == $value1['file_categories_id']){
                                    $child1[] = $value2;
                                }
                            }

                            foreach ($child1 as $key2 => $value2) {
                                $child2 = array();
                                $filechild2 = $file->GetAllFileByCategoriesId($value2['file_categories_id']);

                                foreach ($listallcategories as $value3) {
                                    if(strpos($value3['file_categories_father'], $value2['file_categories_id'])){
                                        $child2[] = $value3;
                                    }

                                    if($value3['file_categories_father'] == $value2['file_categories_id']){
                                        $child2[] = $value3;
                                    }
                                }

                                foreach ($child2 as $key3 => $value3) {
                                    $filechild3 = $file->GetAllFileByCategoriesId($value3['file_categories_id']);

                                    $child2[$key3]['childfile'] = $filechild3;
                                }


                                $child1[$key2]['child'] = $child2;
                                $child1[$key2]['childfile'] = $filechild2;
                            }

                            $child[$key1]['child'] = $child1;
                            $child[$key1]['childfile'] = $filechild1;
                        }
                    }

                    $result[$key]['child'] = $child;
                    $result[$key]['childfile'] = $filechild;
                }

                $this->view->filecategoriseinfo = $result;

				if($this->view->lang == 'vi'){
					$this->view->title = $result[0]['file_categories_name'].'-'.$allsetting[0]['config_title'];
					$this->view->description = $result[0]['file_categories_name'].'-'.$allsetting[0]['config_description'];
					$this->view->keyword = $result[0]['file_categories_name'].','.$allsetting[0]['config_keyword'];
				}else{
					$this->view->title = $result[0]['file_categories_name_english'].'-'.'Institute for Computational Science and Technology';
					$this->view->description = $result[0]['file_categories_name_english'].'-'.$allsetting[0]['config_description'];
					$this->view->keyword = $result[0]['file_categories_name_english'].','.$allsetting[0]['config_keyword'];
				}
				
				$listfile = array();
			
                if($file->GetAllFileByCategoriesId($result[0]['file_categories_id'])){
                    $files = $file->GetAllFileByCategoriesId($result[0]['file_categories_id']);
					foreach($files as $fl){
						$listfile[] = $fl;
					}
				}
				
				if($file->GetAllFileCategoriesChildByFatherId($result[0]['file_categories_id'])){
					$child = $file->GetAllFileCategoriesChildByFatherId($result[0]['file_categories_id']);
					foreach($child as $ch){
						if($file->GetAllFileByCategoriesId($ch['file_categories_id'])){
							$files1 = $file->GetAllFileByCategoriesId($ch['file_categories_id']);
							foreach($files1 as $fl1){
								$listfile[] = $fl1;
							}
						}
						if($file->GetAllFileCategoriesChildByFatherId($ch['file_categories_id'])){
							$child1 = $file->GetAllFileCategoriesChildByFatherId($ch['file_categories_id']);
							foreach($child1 as $ch1){
								if($file->GetAllFileByCategoriesId($ch1['file_categories_id'])){
									$files2 = $file->GetAllFileByCategoriesId($ch1['file_categories_id']);
									foreach($files2 as $fl2){
										$listfile[] = $fl2;
									}
								}
							}
						}
					}
				}
				
				$this->view->listfile = $listfile;
            }
        }else{
            $result = $file->GetFirstFileCategoriesFather();

            $this->view->title = $result[0]['file_categories_name'].'-'.$allsetting[0]['config_title'];
            $this->view->description = $result[0]['file_categories_name'].'-'.$allsetting[0]['config_description'];
            $this->view->keyword = $result[0]['file_categories_name'].','.$allsetting[0]['config_keyword'];

            $this->view->filecategoriseinfo = $result;

			$listfile = array();
			
                if($file->GetAllFileByCategoriesId($result[0]['file_categories_id'])){
                    $files = $file->GetAllFileByCategoriesId($result[0]['file_categories_id']);
					foreach($files as $fl){
						$listfile[] = $fl;
					}
				}
				
				if($file->GetAllFileCategoriesChildByFatherId($result[0]['file_categories_id'])){
					$child = $file->GetAllFileCategoriesChildByFatherId($result[0]['file_categories_id']);
					foreach($child as $ch){
						if($file->GetAllFileByCategoriesId($ch['file_categories_id'])){
							$files1 = $file->GetAllFileByCategoriesId($ch['file_categories_id']);
							foreach($files1 as $fl1){
								$listfile[] = $fl1;
							}
						}
						if($file->GetAllFileCategoriesChildByFatherId($ch['file_categories_id'])){
							$child1 = $file->GetAllFileCategoriesChildByFatherId($ch['file_categories_id']);
							foreach($child1 as $ch1){
								if($file->GetAllFileByCategoriesId($ch1['file_categories_id'])){
									$files2 = $file->GetAllFileByCategoriesId($ch1['file_categories_id']);
									foreach($files2 as $fl2){
										$listfile[] = $fl2;
									}
								}
							}
						}
					}
				}
				
				$this->view->listfile = $listfile;
        }

		$useragent=$_SERVER['HTTP_USER_AGENT'];
		
		if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
			$this->view->layout('layout-mobile');
			$this->view->Render('mobile/file');
			return true;
		}else{
			$this->view->layout('layout');
			$this->view->Render('category/file');
			return true;
		}
    }

	public function galery(){
		$file = new model_file();

        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

		$library = new model_library();
		
		$albumn = new model_albumn();
		
        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
			$this->view->aboutus = $allsetting[0]['config_url_aboutus'];
            $this->view->laboratories = $allsetting[0]['config_url_laboratories'];
			$this->view->emaillink = $allsetting[0]['config_url_email'];
            $this->view->internal = $allsetting[0]['config_url_internal'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOnByPostion('all','ngang')){
            $allmenufather = $model1->SelectAllMenuFatherOnByPostion('all','ngang');

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        if($model1->SelectAllMenuFatherOnByPostion('product','doc')){
            $allmenufatherproduct = $model1->SelectAllMenuFatherOnByPostion('product','doc');

            foreach($allmenufatherproduct as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufatherproduct[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenuproduct = $allmenufatherproduct;
        }

        if($file->GetAllFileCategoriesFather()){
            $listcategoriesfather = $file->GetAllFileCategoriesFather();

            foreach ($listcategoriesfather as $key => $value) {
                if($file->GetAllFileCategoriesChildByFatherId($value['file_categories_id'])){
                    $child = $file->GetAllFileCategoriesChildByFatherId($value['file_categories_id']);

                    foreach ($child as $key1 => $value1) {
                        $child1 = $file->GetAllFileCategoriesChildByFatherId($value1['file_categories_id']);

                        $child[$key1]['child'] = $child1;
                    }
                }
                $listcategoriesfather[$key]['child'] = $child;
            }

            $this->view->listcategoriesfather = $listcategoriesfather;
        }
		
		if($this->view->lang == 'vi'){
			$this->view->title = 'Thư viện -'.$allsetting[0]['config_title'];
            $this->view->description = 'Thư viện -'.$allsetting[0]['config_description'];
            $this->view->keyword = 'thư viện,'.$allsetting[0]['config_keyword'];
		}else{
			$this->view->title = 'Galery - Institute for Computational Science and Technology';
            $this->view->description = 'Galery -'.$allsetting[0]['config_description'];
            $this->view->keyword = 'galery,'.$allsetting[0]['config_keyword'];
		}
		
		if($albumn->GetAllAlbumn()){
        	$listalbumn = $albumn->GetAllAlbumn();
			
			foreach($listalbumn as $key=>$value){
				if($albumn->GetImageDefaultById($value['albumn_id'])){
					$image_default = $albumn->GetImageDefaultById($value['albumn_id']);
					$listalbumn[$key]['image_default'] = $image_default[0]['albumn_image_url'];
				}
			}
			
			$this->view->listalbumn = $listalbumn;
        }
		
		$this->view->layout('layout');
        $this->view->Render('category/galery');
        return true;
	}
	
	public function image(){
    	$model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $partner = new model_partner();

        $library = new model_library();
		
		$video = new model_video();

        $slider = new model_slider();

        if($allsetting){
			if($this->view->lang == 'vi'){
				$this->view->title = $allsetting[0]['config_title'];
				$this->view->description = $allsetting[0]['config_description'];
				$this->view->keyword = $allsetting[0]['config_keyword'];
			}else{
				$this->view->title = 'PV Gas D';
				$this->view->description = $allsetting[0]['config_description'];
				$this->view->keyword = $allsetting[0]['config_keyword'];
			}
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
			$this->view->aboutus = $allsetting[0]['config_url_aboutus'];
            $this->view->laboratories = $allsetting[0]['config_url_laboratories'];
			$this->view->emaillink = $allsetting[0]['config_url_email'];
            $this->view->internal = $allsetting[0]['config_url_internal'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOn()){
            $allmenufather = $model1->SelectAllMenuFatherOn();

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        if($news->SelectTopNewsRelease(12)){
            $listnewsnew = $news->SelectTopNewsRelease(12);

            foreach ($listnewsnew as $key => $value) {
                if($news->GetCategoriesNewsById($value['news_categories_id'])){
                    $newsinfo = $news->GetCategoriesNewsById($value['news_categories_id']);
                    $listnewsnew[$key]['category'] = $newsinfo[0];
                }
            }

            $this->view->listnewsnew = $listnewsnew;
        }

        if($library->SelectAllLibrary()){
            $this->view->listlibrary = $library->SelectAllLibrary();
        }

        $this->view->layout('layout-mobile');
        $this->view->Render('mobile/image');
    }
}
