<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<?php 
    function cut_string($str, $len, $more) {
        if ($str == "" || $str == NULL)
            return $str;
        if (is_array($str))
            return $str;
        $str = trim($str);
        if (strlen($str) <= $len)
            return $str;
        $str = substr($str, 0, $len);
        if ($str != "") {
            if (!substr_count($str, " ")) {
                if ($more)
                    $str .= " ...";
                return $str;
            }
            while (strlen($str) && ($str[strlen($str) - 1] != " ")) {
                $str = substr($str, 0, -1);
            }
            $str = substr($str, 0, -1);
            if ($more)
                $str .= " ...";
        }
        return $str;
    }

	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];  
    $categoriesinfo = $this->categoriesinfo;

?>
<header class="page-header">
    <h2><?php echo cut_string($categoriesinfo[0]['file_categories_name'],100,15);?></h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="<?php echo URL;?>file/categories/">
                    <span>Chuyên mục tài nguyên</span>
                </a>
            </li>
            <li>
                <span><?php echo cut_string($categoriesinfo[0]['file_categories_name'],100,15);?></span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Chỉnh sửa chuyên mục tài nguyên</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label class="col-sm-3 control-label">Tên chuyên mục</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" value="<?php echo $categoriesinfo[0]['file_categories_name'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="file_type" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Tên chuyên mục tiếng anh</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" value="<?php echo $categoriesinfo[0]['file_categories_name_english'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="file_type_english" required>
                </div>
            </div>
            <?php
                if($this->listcategories){
            ?>
            <div class="form-group">
                <label class="col-sm-3 control-label">Chọn chuyên mục cha</label>
                <div class="col-sm-8">
                    <select id="s2_with_tag" multiple data-plugin-selectTwo class="form-control populate" name="file_fathertype[]">
                        <?php 
                            foreach($this->listcategories as $lt){
                                if($lt['file_categories_id'] == $categoriesinfo[0]['file_categories_father']){
                        ?>
                            <option value="<?php echo $lt['file_categories_id'];?>" selected><?php echo $lt['file_categories_name'];?></option>
                        <?php }else{?>
                            <option value="<?php echo $lt['file_categories_id'];?>"><?php echo $lt['file_categories_name'];?></option>
                        <?php 
                            }
                            if(strpos($categoriesinfo[0]['file_categories_father'],$lt['file_categories_id'])){
                        ?>
                            <option value="<?php echo $lt['file_categories_id'];?>" selected><?php echo $lt['file_categories_name'];?></option>
                        <?php }else{?>
                            <option value="<?php echo $lt['file_categories_id'];?>"><?php echo $lt['file_categories_name'];?></option>
                        <?php }}?>
                    </select>
                </div>
            </div>
            <?php } ?>
			<?php         
				if(strpos($edit[0]['permission_detail'],'new') || $_SESSION['user_type'] == 1){    
			?>
            <div class="form-group" style="margin-top: 40px;">
                <div class="col-sm-offset-4 col-sm-2">
                    <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                        <span><i class="fa fa-clock-o txt-danger"></i></span>
                        Nhập lại
                    </button>
                </div>
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-success btn-label-left" name="register" id="submit">
                        <span><i class="fa fa-clock-o"></i></span>
                        Đồng ý
                    </button>
                </div>
            </div>
			<?php } ?>
        </form>
    </div>
</section>