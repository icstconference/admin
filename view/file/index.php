<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<header class="page-header">
    <h2>Tất cả tài nguyên</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <span>Tất cả tài nguyên</span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<?php     
    $create = $_SESSION['user_permission_create'];
    $delete = $_SESSION['user_permission_delete'];
    $edit = $_SESSION['user_permission_edit']; 
?>
<?php
    $listfile = $this->listfile;
?>
<div class="row" style="margin: 0 0 10px;text-align:right;">
    <a href="<?php echo URL;?>file/addfile/" class="mb-xs mt-xs mr-xs btn btn-primary">
        <i class="fa fa-file-text-o"></i>&nbsp;Thêm tài nguyên mới
    </a>
    <button href="#modalHeaderColorPrimary" class="mb-xs mt-xs mr-xs modal-basic btn btn-danger" disabled id="deletepost">
        <i class="fa fa-trash"></i>&nbsp;Xóa tài nguyên
    </button>
</div>
<span id="typepost" rel="file"></span>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Danh sách tài nguyên</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-default">
            <thead>
                <tr>
                    <th style="vertical-align:middle;text-align:center;"><input name="select_all" value="1" id="example-select-all" type="checkbox" /></th>
                    <th style="vertical-align:middle;">Tên tài nguyên</th>
                    <th style="vertical-align:middle;">Danh mục</th>
                    <th style="vertical-align:middle;width: 15%;">Dung lượng</th>
                    <th style="vertical-align:middle;width: 15%;">Chỉnh sửa</th>
                </tr>
            </thead>
            <tbody>
            <?php
                if($this->listfile){
                    $i=0;
                        foreach($this->listfile as $lp){
                            $i++;
            ?>
                <tr>
                    <td style="vertical-align:middle;text-align:center;"><?php echo $lp['file_id'];?></td>
                    <td style="vertical-align:middle;"><?php echo $lp['file_name'];?></td>
                    <td style="vertical-align:middle;">
                        <?php
                            if($lp['file_categories_id'] != 0){
                                foreach($this->listcategories as $ls){
                                    if($ls['file_categories_id'] == $lp['file_categories_id']){
                        ?>
                            <?php echo $ls['file_categories_name'];?>
                        <?php }}} ?>
                    </td>
                    <td style="vertical-align:middle;"><?php echo ceil(($lp['file_volume'])/1024);?> KB</td>
                    <td style="vertical-align:middle;width: 15%;">    
						<?php                                         
                            if(strpos($edit[0]['permission_detail'],'file') || $_SESSION['user_type'] == 1){                                    
                        ?>  
                        <a class="btn btn-info" href="<?php echo URL;?>file/editfiles/<?php echo $lp['file_id'];?>" data-toggle="tooltip" data-placement="bottom" title="Sửa tài nguyên"><i class="fa fa-edit"></i></a>
                        <?php } ?>
                        <?php                                         
                            if(strpos($delete[0]['permission_detail'],'file') || $_SESSION['user_type'] == 1){                                    
                        ?>  
                        <a class="btn btn-danger" href="<?php echo URL;?>file/deletefiles/<?php echo $lp['file_id'];?>" data-toggle="tooltip" data-placement="bottom" title="Xóa tài nguyên" ><i class="fa fa-trash"></i></a>
                        <?php } ?>
                        <a class="btn btn-success" href="<?php echo $lp['file_url'];?>" data-toggle="tooltip" data-placement="bottom" title="Tải xuống tài nguyên" target="_blank" download><i class="fa fa-download"></i></a>
                    </td>
                </tr>
            <?php }} ?>
            </tbody>
        </table>
    </div>
</section>
<div id="modalHeaderColorPrimary" class="modal-block modal-header-color modal-block-primary mfp-hide">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Thông báo</h2>
            </header>
            <div class="panel-body">
                <div class="modal-wrapper">
                    <div class="modal-icon">
                        <i class="fa fa-question-circle"></i>
                    </div>
                    <div class="modal-text">
                        <p>Xóa vĩnh viễn ?</p>
                    </div>
                </div>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button class="btn btn-primary modal-confirm">Đồng ý</button>
                        <button class="btn btn-default modal-dismiss">Hủy bỏ</button>
                    </div>
                </div>
            </footer>
        </section>
    </div>