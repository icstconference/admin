<?php                            
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];
?>
<header class="page-header">
    <h2>Thêm tài nguyên</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="<?php echo URL;?>file/">
                    <span>Tất cả tài nguyên</span>
                </a>
            </li>
            <li>
                <span>Thêm tài nguyên</span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Thêm tài nguyên</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" role="form" enctype="multipart/form-data">
            <div class="form-group">
                <label class="col-sm-3 control-label">Tên tài nguyên</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="file_name" name="file_name" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Tên tài nguyên tiếng anh</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="file_name_english" name="file_name_english" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Tập tin tài nguyên</label>
                <div class="col-sm-3">
                    <input type="file" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="file_type" name="file_type">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Link xem tài nguyên</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="file_link" name="file_link">
                </div>
            </div>
            <?php
                if($this->listcategories){
            ?>
            <div class="form-group">
                <label class="col-sm-3 control-label">Chọn loại tài nguyên</label>
                <div class="col-sm-6">
                    <select id="s2_with_tag" data-plugin-selectTwo class="form-control populate" name="file_categories_id">
                        <?php 
                            foreach($this->listcategories as $lt){
                                $catfather = '';
                                foreach ($this->listcategories as $lt1) {
                                    if($lt['file_categories_father'] == $lt1['file_categories_id']){
                                        $catfather = $lt1['file_categories_name'];
                                    }
                                }
                        ?>
                            <option value="<?php echo $lt['file_categories_id'];?>"><?php echo $lt['file_categories_name'].'-'.$catfather;?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <?php } ?>
            <div class="form-group">
                <label class="col-sm-3 control-label">Ngày tạo</label>
                        <div class="col-sm-3">
                            <input type="datetime" class="form-control" value="<?php echo date('Y-m-d h:i:s');?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="file_create_date" name="file_create_date" required>
                        </div>
            </div>
            <div class="form-group" id="progressbar" style="display: none;">
                <center>
                    <progress></progress>
                </center>
            </div>
			<?php                                         
				if($_SESSION['user_type'] == 1 || strpos($create[0]['permission_detail'],'file')){                                    
			?> 
            <div class="form-group" style="margin-top: 40px;">
                <div class="col-sm-offset-4 col-sm-2">
                    <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                        <span><i class="fa fa-clock-o txt-danger"></i></span>
                        Nhập lại
                    </button>
                </div>
                <div class="col-sm-2">
                    <button type="button" class="btn btn-success btn-label-left" name="register" id="submit">
                        <span><i class="fa fa-clock-o"></i></span>
                        Đồng ý
                    </button>
                </div>
            </div>
			<?php } ?>
        </form>
    </div>
</section>