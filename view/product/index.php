<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<header class="page-header">
    <h2>Tất cả sản phẩm</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <span>Tất cả sản phẩm</span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<?php
    $listproduction = $this->listproduction;
?>
<?php                            
    $create = $_SESSION['user_permission_create'];
    $delete = $_SESSION['user_permission_delete'];
    $edit = $_SESSION['user_permission_edit'];
    $listpermission = $this->listpermission;
?>
<div class="row" style="margin: 0 0 10px;text-align:right;">
    <a href="<?php echo URL;?>product/adddproduct/" class="mb-xs mt-xs mr-xs btn btn-primary">
        <i class="fa fa-plus"></i>&nbsp;Thêm sản phẩm
    </a>
    <button href="#modalHeaderColorPrimary" class="mb-xs mt-xs mr-xs modal-basic btn btn-danger" disabled id="deletepost">
        <i class="fa fa-trash"></i>&nbsp;Xóa sản phẩm
    </button>
</div>
<span id="typepost" rel="product"></span>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Tất cả sản phẩm</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-default">
            <thead>
                <tr>
                    <th style="vertical-align:middle;">STT</th>
                    <th style="vertical-align:middle;">Tên sản phẩm</th>
                    <th style="vertical-align:middle;">Hình ảnh</th>
                    <th style="vertical-align:middle;">Số lượng</th>
                    <th style="vertical-align:middle;">Loại</th>
                    <th style="vertical-align:middle;">Giá</th>
                    <th style="vertical-align:middle;">Tùy chỉnh</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($this->listproduction){
                        $i=0;
                            foreach($this->listproduction as $lp){
                                $i++;
                ?>
                <tr>
                    <td style="vertical-align:middle;text-align:center;"><?php echo $i;?></td>
                    <td style="vertical-align:middle;"><?php echo $lp['product_name'];?></td>
                    <td style="vertical-align:middle;">
                        <?php
                            if(strlen($lp['product_image_thumb']) > 1){
                        ?>
                            <img src="<?php echo $lp['product_image_thumb'];?>" class="img-responsive" style="width:60px;"/>
                        <?php } else {?>
                            <img src="<?php echo URL;?>public/img/noneimage.jpg" class="img-responsive" style="width:60px;"/>
                        <?php } ?>
                    </td>
                    <td style="vertical-align:middle;"><?php echo $lp['product_quantity'];?></td>
                    <td style="vertical-align:middle;">
                        <?php
                            if($this->listcategories){
                                foreach($this->listcategories as $lt){
                                    if($lp['categories_id'] == $lt['categories_id']){
                        ?>
                            <?php echo $lt['categories_name'];?>
                        <?php }}}?>
                    </td>
                    <td style="vertical-align:middle;">
                        <font color="red">Mới:</font> <?php echo $lp['product_price_new'];?><br/>
                        <font color="gray">Cũ:</font> <?php echo $lp['product_price_old'];?>
                    </td>
                    <td style="vertical-align:middle;">
                        <a class="btn btn-primary" href="<?php echo URL;?>product/editproduct/<?php echo $lp['product_id'];?>" data-toggle="tooltip" data-placement="bottom" title="Sửa sản phẩm"><i class="fa fa-edit"></i></a>
                        <a class="btn btn-danger" href="<?php echo URL;?>product/deleteproduct/<?php echo $lp['product_id'];?>" data-toggle="tooltip" data-placement="bottom" title="Xóa sản phẩm"><i class="fa fa-trash"></i></a>
                        <?php 
                            $allsetting = $this->allsetting;
                            if($allsetting[0]['config_url'] == 'dai'){
                        ?>
                            <a class="btn btn-success" href="<?php echo URL;?>product/detail/<?php echo $lp['product_url'];?>.html" data-toggle="tooltip" data-placement="bottom" title="Xem sản phẩm" target="_blank"><i class="fa fa-eye"></i></a>
                        <?php }else{ ?>
                            <a class="btn btn-success" href="<?php echo URL;?><?php echo $lp['product_url'];?>.html" data-toggle="tooltip" data-placement="bottom" title="Xem sản phẩm" target="_blank"><i class="fa fa-eye"></i></a>
                        <?php } ?>
                    </td>
                </tr>
                <?php }} ?>
            </tbody>
        </table>
    </div>
</section>