<?php 
    $actual_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER[REQUEST_URI];
    $productinfo = $this->productinfo;
    $labelinfo = $this->labelinfo;
?>
<style type="text/css">
    .main{
        padding: 40px 0 0px;
    }
    .product-related {
        padding-top: 40px;
    }
</style>
<section class="product">
    <!-- Product info -->
    <section class="product-info">
        <div class="container">
            <div class="post-370 product type-product status-publish has-post-thumbnail product_cat-mens-jackets product_cat-mens-jumpers product_cat-mens product_cat-mens-shirts product_cat-mens-t-shirts shipping-taxable purchasable product-type-simple product-cat-mens-jackets product-cat-mens-jumpers product-cat-mens product-cat-mens-shirts product-cat-mens-t-shirts instock">
                <div class="row">
                    <div class="span4">
                        <div class="product-images">
                            <div class="box">
                                <div class="images">
                                    <?php 
                                        if($this->listimgproduct){
                                            $i=0;
                                            $listimgproduct = $this->listimgproduct;
                                    ?>
                                    <div class="primary">
                                        <img src="<?php echo $listimgproduct[0]['images_url'];?>" data-zoom-image="<?php echo $listimgproduct[0]['images_url'];?>" alt="50f9691c448f99.58934949">                
                                    </div>
                                    <div class="thumbs" id="gallery"> 
                                        <ul class="thumbs-list">
                                            <?php 
                                                foreach ($listimgproduct as $value) {
                                            ?>
                                            <li>
                                                <a href="#" data-image="<?php echo $value['images_url'];?>" data-zoom-image="<?php echo $value['images_url'];?>" title="<?php echo $value['images_url'];?>">
                                                    <img src="<?php echo $value['images_url'];?>" alt="50f9691c448f99.58934949">
                                                </a>
                                            </li>
                                            <?php } ?>                 
                                        </ul>
                                    </div>
                                    <?php }else{?>
                                    <div class="primary">
                                        <img src="<?php echo $productinfo[0]['product_image'];?>" data-zoom-image="<?php echo $productinfo[0]['product_image'];?>" alt="<?php echo $productinfo[0]['product_image'];?>">                
                                    </div>
                                    <?php } ?>
                                    <div class="social">
                                        <div id="sharrre">
                                            <div class="facebook sharrre">
                                                <button class="btn btn-mini btn-facebook">
                                                    <i class="fa fa-facebook"></i> &nbsp; 0
                                                </button>
                                            </div>
                                            <div class="twitter sharrre"> </div>
                                            <div class="googleplus sharrre"><button class="btn btn-mini btn-googleplus"><i class="icon-google-plus"></i> &nbsp; 0</button></div>                                                   
                                            <div class="pinterest sharrre"><button class="btn btn-mini btn-pinterest"><i class="icon-pinterest"></i> &nbsp; 0</button></div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>    
                </div>
                <div class="span8"> 
                    <div class="product-content">
                        <div class="box">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#product" data-toggle="tab">
                                        <center>
                                            <i class="fa fa-reorder fa-3x"></i>
                                        </center>
                                        <span class="hidden-phone">Sản phẩm</span>
                                    </a>
                                </li>
                                <!-- <li class="">
                                    <a href="http://theme.laboutiquetheme.com/product/ls-button-down/#shipping" data-toggle="tab">
                                        <i class="icon-truck icon-large"></i>
                                        <span class="hidden-phone">Shipping</span>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="http://theme.laboutiquetheme.com/product/ls-button-down/#returns" data-toggle="tab">
                                        <i class="icon-undo icon-large"></i>
                                        <span class="hidden-phone">Returns</span>
                                    </a>
                                </li> -->
                                <!-- <li class="description_tab">
                                    <a href="#tab-description" data-toggle="tab">
                                        <i class="icon-align-left icon-large"></i>                            
                                        <span class="hidden-phone">
                                            Mô tả                           
                                        </span>
                                    </a>
                                </li> -->
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="product">
                                    <div class="details">
                                        <h1 itemprop="name" class="product_title entry-title">
                                            <?php echo $productinfo[0]['product_name'];?>&nbsp;
                                            <span class="label label-sale" style="background:<?php echo $labelinfo[0]['label_color'];?>;"><?php echo $labelinfo[0]['label_name'];?></span>
                                        </h1>
                                            <div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
                                                <div class="prices">
                                                    <span itemprop="price" class="price">
                                                        <ins><span class="amount"><?php echo number_format($productinfo[0]['product_price_new']).' VNĐ';?></span></ins>
                                                        <?php 
                                                            if($productinfo[0]['product_price_old'] > 0){
                                                        ?>
                                                            <del><span class="amount"><?php echo number_format($productinfo[0]['product_price_old']).' VNĐ';?></span></del> 
                                                        <?php } ?>
                                                    </span> 
                                                </div>
                                            </div>
                                            <div class="product_meta meta">
                                                <div class="posted_in categories">
                                                    <!-- <span>
                                                        <i class="icon-bookmark"></i>&nbsp;
                                                        <a href="http://theme.laboutiquetheme.com/product-category/mens/mens-jackets/" rel="tag">Jackets</a>
                                                        <a href="http://theme.laboutiquetheme.com/product-category/mens/mens-jumpers/" rel="tag">Jumpers</a>
                                                        <a href="http://theme.laboutiquetheme.com/product-category/mens/" rel="tag">Mens</a>
                                                        <a href="http://theme.laboutiquetheme.com/product-category/mens/mens-shirts/" rel="tag">Shirts</a>
                                                        <a href="http://theme.laboutiquetheme.com/product-category/mens/mens-t-shirts/" rel="tag">T-Shirts</a>
                                                    </span> -->
                                                </div>
                                            </div>                   
                                    </div>
                                    <div itemprop="description" class="short-description">
                                        <?php echo $productinfo[0]['product_description'];?>
                                    </div>                
                                    <div class="add-to-cart">
                                        <form action="<?php echo URL;?>product/addproduct/" method="post" role="form" enctype="multipart/form-data">
                                            <input type="hidden" name="typeclothes" value="<?php echo $productinfo[0]['product_type'];?>" />
                                            <table class="variations" cellspacing="0">
                                                <tbody>
                                                    <tr>
                                                        <?php 
                                                            if($productinfo[0]['product_type'] != 'nhom'){
                                                        ?>
                                                        <td class="label"><label for="sleeve-length" style="font-size:14px;font-weight: bold;">Nam</label></td>
                                                        <td id="man">  
                                                            <input type="radio" name="manclothes" id="manclothes" value="S" style="margin:0px;"> S -
                                                            <input type="radio" name="manclothes" id="manclothes" value="M" style="margin:0px;"> M -
                                                            <input type="radio" name="manclothes" id="manclothes" value="L" style="margin:0px;"> L -
                                                            <input type="radio" name="manclothes" id="manclothes" value="XL" style="margin:0px;"> XL -
                                                            <input type="radio" name="manclothes" id="manclothes" value="XXL" style="margin:0px;"> XXL
                                                        </td>
                                                        <td class="label"><label for="sleeve-length" style="font-size:14px;font-weight: bold;">Nữ</label></td>
                                                        <td id="woman" >  
                                                            <input type="radio" name="womanclothes" id="womanclothes" value="S" style="margin:0px;"> S -
                                                            <input type="radio" name="womanclothes" id="womanclothes" value="M" style="margin:0px;"> M -
                                                            <input type="radio" name="womanclothes" id="womanclothes" value="L" style="margin:0px;"> L 
                                                        </td>
                                                        <?php 
                                                            if($productinfo[0]['product_type'] == 'giadinh'){
                                                        ?>
                                                        <td class="label"><label for="sleeve-length" style="font-size:14px;font-weight: bold;">Em bé</label></td>
                                                        <td id="baby">  
                                                            <input type="radio" name="babyclothes" id="babyclothes" value="1" style="margin:0px;"> 1 -
                                                            <input type="radio" name="babyclothes" id="babyclothes" value="2" style="margin:0px;"> 2 -
                                                            <input type="radio" name="babyclothes" id="babyclothes" value="4" style="margin:0px;"> 4 -
                                                            <input type="radio" name="babyclothes" id="babyclothes" value="6" style="margin:0px;"> 6 -
                                                            <input type="radio" name="babyclothes" id="babyclothes" value="8" style="margin:0px;"> 8 -
                                                            <input type="radio" name="babyclothes" id="babyclothes" value="10" style="margin:0px;"> 10 
                                                        </td>
                                                        <?php } ?>
                                                        <?php } ?>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="cart">
                                                <div class="quantity">
                                                    <input type="number" step="1" min="1" name="quantity" value="1" title="Qty" class="input-text qty text" size="4">
                                                </div>
                                                <input type="hidden" name="idproduct" value="<?php echo $productinfo[0]['product_id']; ?>"> 
                                                <input type="hidden" name="add-to-cart" value="370">
                                                <button type="buton" class="single_add_to_cart_button button alt" name="addcart" id="addcart">Thêm vào giỏ hàng</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!-- Shipping tab -->
                                <div class="tab-pane" id="shipping">                                    
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
                                    <div style="margin-bottom: 10px; margin-top: 10px;"><img class="img-polaroid" src="./L_S Button Down _ La Boutique Theme_files/royal-mail.png" alt=""><img class="img-polaroid" src="./L_S Button Down _ La Boutique Theme_files/ups-logo.png" alt=""></div>
                                    Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.            
                                </div>
                                <!-- End id="shipping" -->
                                <!-- Returns tab -->
                                <div class="tab-pane" id="returns">
                                    <p class="lead">For any unwanted goods La Boutique offers a <strong>21-day return policy</strong>.</p><br>
                                    If you receive items from us that differ from what you have ordered, then you must notify us as soon as possible using our <a href="http://theme.laboutiquetheme.com/product/ls-button-down/#">online contact form</a>.<br>
                                    <br>
                                    If you find that your items are faulty or damaged on arrival, then you are entitled to a repair, replacement or a refund. Please note that for some goods it may be disproportionately costly to repair, and so where this is the case, then we will give you a replacement or a refund.<br>
                                    <br>
                                    Please visit our <a href="http://theme.laboutiquetheme.com/product/ls-button-down/#">Warranty section</a> for more details.            
                                </div>
                                <!-- End id="returns" -->
                                <div class="tab-pane entry-content" id="tab-description">
                                </div>
                            </div>
                        </div>                
                    </div>
                </div>
            </div>
        </div><!-- #product-370 -->              
    </div>
    </section>
    <section class="product-reviews">
        <div class="container">
            <div class="span8 offset2">
                <h5>Bình luận của bạn <span class="script">love❤</span> cho sản phẩm này</h5>
                <div id="fb-root"></div>
                <script>(function(d, s, id) {
                  var js, fjs = d.getElementsByTagName(s)[0];
                  if (d.getElementById(id)) return;
                  js = d.createElement(s); js.id = id;
                  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5&appId=254227268096895";
                  fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
                <div class="fb-comments" data-href="<?php echo $actual_link;?>" data-numposts="5"></div>
            </div>
        </div>      
    </section>
    <!-- End class="product-reviews" -->
    
    <!-- Related products -->
    <section class="product-related">
        <div class="container">
            <div class="span12">
                <?php 
                    if($this->listrelatedproduct){
                ?>
                <h2>Các sản phẩm liên quan</h2>
                <ul class="product-list" id="isotope_content" style="display: block; position: relative;">
                    <?php 
                        foreach ($this->listrelatedproduct as $value) {
                    ?>
                    <li class="last standard item product type-product status-publish has-post-thumbnail">
                        <a href="<?php echo URL;?>product/detail/<?php echo $value['product_url'];?>" title="<?php echo $value['product_name'];?>">
                            <div class="image">
                                <img class="primary" src="<?php echo $value['product_image'];?>" alt="<?php echo $value['product_name'];?>">
                                <img class="secondary" src="<?php echo $value['product_image_behind'];?>" alt="<?php echo $value['product_name'];?>">
                                <?php 
                                    if($value['label_id'] == 3){
                                ?>
                                    <span class="badge badge-sale"><?php echo $value['labelinfo'];?></span>
                                <?php } ?>
                            </div>
                            <div class="title">
                                <div class="prices">
                                        <span class="price">
                                            <span class="amount"><?php echo number_format($value['product_price_new']).' VNĐ';?></span>                       
                                        </span>
                                </div>
                                <h3><?php echo $value['product_name'];?></h3>
                            </div>
                        </a>
                    </li>
                    <?php } ?>
                </ul>
                <?php } ?>
            </div>
        </div>  
    </section>
</section>