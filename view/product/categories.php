<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<header class="page-header">
    <h2>Tất cả loại sản phẩm</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <span>Tất cả loại sản phẩm</span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Thêm loại sản phẩm</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label class="col-sm-3 control-label">Tên loại sản phẩm</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" placeholder="Loại sản phẩm" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="product_type" required>
                </div>
            </div>
            <?php
                if($this->listcategories){
            ?>
            <div class="form-group">
                <label class="col-sm-3 control-label">Chọn loại sản phẩm cha ( nếu có )</label>
                <div class="col-sm-8">
                    <select id="s2_with_tag" data-plugin-selectTwo class="form-control populate" name="product_fathertype">
                        <option value="0">Thư mục chính</option>
                        <?php foreach($this->listcategories as $lt){?>
                            <option value="<?php echo $lt['categories_id'];?>"><?php echo $lt['categories_name'];?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <?php } ?>
            <div class="form-group" style="margin-top: 40px;">
                <div class="col-sm-offset-4 col-sm-2">
                    <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                        <span><i class="fa fa-clock-o txt-danger"></i></span>
                        Nhập lại
                    </button>
                </div>
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-success btn-label-left" name="register" id="submit">
                        <span><i class="fa fa-clock-o"></i></span>
                        Đồng ý
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>
<?php
    $listcategories = $this->listcategories;
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Tất cả loại sản phẩm</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-default">
            <thead>
                <tr>
                    <th style="vertical-align:middle;">STT</th>
                    <th style="vertical-align:middle;">Tên loại sản phẩm</th>
                    <th style="vertical-align:middle;">Danh mục cha</th>
                    <th style="vertical-align:middle;">Chỉnh sửa</th>
                </tr>
            </thead>
            <tbody>
                    <?php
                    if($this->listcategories){
                        $i=0;
                        foreach($this->listcategories as $lp){
                            $i++;
                            ?>
                            <tr>
                                <td style="vertical-align:middle;"><?php echo $i;?></td>
                                <td style="vertical-align:middle;"><?php echo $lp['categories_name'];?></td>
                                <td style="vertical-align:middle;">
                                    <?php
                                        foreach($this->listcategories as $ls){
                                            if($ls['categories_id'] == $lp['categories_father']){
                                    ?>
                                        <?php echo $ls['categories_name'];?>
                                    <?php }} ?>
                                </td>
                                <td style="vertical-align:middle;">
                                    <a class="btn btn-primary" href="<?php echo URL;?>product/editcategories/<?php echo $lp['categories_id'];?>" data-toggle="tooltip" data-placement="bottom" title="Sửa loại sản phẩm"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-danger" href="<?php echo URL;?>product/deletecategories/<?php echo $lp['categories_id'];?>" data-toggle="tooltip" data-placement="bottom" title="Xóa loại sản phẩm"><i class="fa fa-trash"></i></a>
                                    <?php 
                                        $allsetting = $this->allsetting;
                                        if($allsetting[0]['config_url'] == 'dai'){
                                    ?>
                                    <a class="btn btn-success" href="<?php echo URL;?>category/product/<?php echo $lp['categories_url'];?>.html" target="_blank"><i class="fa fa-eye"></i></a>
                                    <?php }else{ ?>
                                    <a class="btn btn-success" href="<?php echo URL;?><?php echo $lp['categories_url'];?>.html" target="_blank"><i class="fa fa-eye"></i></a>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php }} ?>
            </tbody>
        </table>
    </div>
</section>