<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<?php
    $categoriesinfo = $this->categoriesinfo;
?>
<header class="page-header">
    <h2><?php echo $categoriesinfo[0]['categories_name'];?></h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="<?php echo URL;?>product/categories/">
                    <span>Tất cả loại sản phẩm</span>
                </a>
            </li>
            <li>
                <span><?php echo $categoriesinfo[0]['categories_name'];?></span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Chỉnh sửa sản phẩm</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label class="col-sm-3 control-label">Tên loại sản phẩm</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" value="<?php echo $categoriesinfo[0]['categories_name'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="product_type" required>
                </div>
            </div>
            <?php
                    if($this->listcategories){
                        ?>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Chọn loại sản phẩm cha ( nếu có )</label>
                            <div class="col-sm-8">
                                <select id="s2_with_tag" data-plugin-selectTwo class="form-control populate" name="product_fathertype">
                                    <?php
                                        if($categoriesinfo[0]['categories_father'] != 0){
                                        foreach($this->listcategories as $lt){
                                            if($categoriesinfo[0]['categories_father'] == $lt['categories_id']){
                                    ?>
                                        <option value="<?php echo $lt['categories_id'];?>"><?php echo $lt['categories_name'];?></option>
                                    <?php }} ?>
                                    <option value="0">Thư mục chính</option>
                                    <?php
                                    foreach($this->listcategories as $lt){
                                        if($categoriesinfo[0]['categories_father'] != $lt['categories_id']){
                                            ?>
                                            <option value="<?php echo $lt['categories_id'];?>"><?php echo $lt['categories_name'];?></option>
                                        <?php }}}else{?>
                                            <option value="0">Thư mục chính</option>
                                            <?php
                                            foreach($this->listcategories as $lt){
                                            ?>
                                            <option value="<?php echo $lt['categories_id'];?>"><?php echo $lt['categories_name'];?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Nhập lại
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-success btn-label-left" name="register" id="submit">
                                <span><i class="fa fa-clock-o"></i></span>
                                Đồng ý
                            </button>
                        </div>
                    </div>
        </form>
    </div>
</section>