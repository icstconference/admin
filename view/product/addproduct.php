<header class="page-header">
    <h2>Tất cả sản phẩm</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="<?php echo URL;?>product">
                    <span>Tất cả sản phẩm</span>
                </a>
            </li>
            <li>
                <span>Thêm sản phẩm</span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<?php                            
    $create = $_SESSION['user_permission_create'];
    $delete = $_SESSION['user_permission_delete'];
    $edit = $_SESSION['user_permission_edit'];
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Thêm sản phẩm mới</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" role="form" enctype="multipart/form-data">
            <div class="form-group">
                <label class="col-sm-2 control-label">Tên sản phẩm</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" placeholder="Tên sản phẩm" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="product_name" name="product_name" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Chọn loại sản phẩm</label>
                <div class="col-sm-5">
                    <select id="s2_with_tag" data-plugin-selectTwo class="form-control populate" name="product_type">
                        <?php
                            if($this->listcategories){
                                foreach($this->listcategories as $lt){
                        ?>
                            <option value="<?php echo $lt['categories_id'];?>"><?php echo $lt['categories_name'];?></option>
                        <?php }}?>
                    </select>
                </div>
            </div>
            <div class="form-group" style="display:none;">
                <label class="col-sm-2 control-label">Chọn kiểu sản phẩm</label>
                <div class="col-sm-2">
                    <select id="s2_with_tag" class="populate placeholder" name="product_kind">
                        <option value="don">Áo đơn</option>
                        <option value="doi">Áo đôi</option>
                        <option value="giadinh">Áo gia đình</option>
                        <option value="nhom">Áo nhóm</option>
                    </select>
                </div>
            </div>
            <div class="form-group" style="display:none;">
                <label class="col-sm-2 control-label">Chọn nhãn sản phẩm</label>
                <div class="col-sm-2">
                    <select id="s2_with_tag" class="populate placeholder" name="product_label">
                        <option value="0">Không có</option>
                        <?php
                            if($this->listlabel){
                                foreach($this->listlabel as $lb){
                        ?>
                            <option value="<?php echo $lb['label_id'];?>"><?php echo $lb['label_name'];?></option>
                        <?php }}?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Số lượng</label>
                <div class="col-sm-2">
                    <input type="number" class="form-control" placeholder="Số lượng" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name"  name="product_quantity">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Giá mới</label>
                <div class="col-sm-3">
                    <div class="input-group">
                        <input type="number" class="form-control" placeholder="Giá mới" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name"  name="product_newprice" >
                        <span class="input-group-addon">VNĐ</span>
                    </div>
                </div>
                <label class="col-sm-1 control-label">Giá cũ</label>
                <div class="col-sm-3">
                    <div class="input-group">
                        <input type="number" class="form-control" placeholder="Giá cũ" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name"  name="product_oldprice" >
                        <span class="input-group-addon">VNĐ</span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Hình ảnh đại diện</label>
                <div class="col-sm-10">
                    <div class="col-sm-5" style="padding:0px;">
                        <label class="col-sm-3 control-label" style="padding:0px;">Mặt trước</label>
                        <div class="col-sm-9" >
                            <img src="#" id="productimgfront" class="img-responsive imgupload" style="border-radius:3px;border:1px solid #ddd;display:none;margin-bottom:5px;" /> 
                            <input type="file" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="product_img" name="product_img" accept="image/jpg,image/png,image/jpeg,image/gif"  required>
                        </div>
                    </div>
                    <div class="col-sm-5" style="padding:0px;">
                        <label class="col-sm-2 control-label" style="padding:0px;">Mặt sau</label>
                        <div class="col-sm-9">
                            <img src="#" id="productimgbehind" class="img-responsive imgupload" style="border-radius:3px;border:1px solid #ddd;display:none;margin-bottom:5px;" /> 
                            <input type="file" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="product_img_behind" name="product_img_behind" accept="image/jpg,image/png,image/jpeg,image/gif"  required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="row col-sm-12 control-lable" style="margin:0 0 10px 0;text-align:center;">
                    Các hình khác của sản phẩm
                </label>
                <div class="row" style="margin:0px;">
                    <div class="col-sm-12">
                        <div class="col-sm-3">
                            <center>
                                <img src="<?php echo URL;?>public/img/noneimage.jpg" id="productimg1" class="img-responsive" style="border-radius:3px;border:1px solid #ddd;margin-bottom:5px;max-width:200px;" /> 
                                <input type="file" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="product_img1" name="product_img1" accept="image/jpg,image/png,image/jpeg,image/gif"  required style="display:none;">
                                <label>Hình 1</label>
                            </center>                            
                        </div>
                        <div class="col-sm-3">
                            <center>
                                <img src="<?php echo URL;?>public/img/noneimage.jpg" id="productimg2" class="img-responsive" style="border-radius:3px;border:1px solid #ddd;margin-bottom:5px;max-width:200px;" /> 
                                <input type="file" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="product_img2" name="product_img2" accept="image/jpg,image/png,image/jpeg,image/gif"  required style="display:none;">
                                <label>Hình 2</label>
                            </center> 
                        </div>
                        <div class="col-sm-3">
                            <center>
                                <img src="<?php echo URL;?>public/img/noneimage.jpg" id="productimg3" class="img-responsive" style="border-radius:3px;border:1px solid #ddd;margin-bottom:5px;max-width:200px;" /> 
                                <input type="file" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="product_img3" name="product_img3" accept="image/jpg,image/png,image/jpeg,image/gif"  required style="display:none;">
                                <label>Hình 3</label>
                            </center> 
                        </div>
                        <div class="col-sm-3">
                            <center>
                                <img src="<?php echo URL;?>public/img/noneimage.jpg" id="productimg4" class="img-responsive" style="border-radius:3px;border:1px solid #ddd;margin-bottom:5px;max-width:200px;" /> 
                                <input type="file" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="product_img4" name="product_img4" accept="image/jpg,image/png,image/jpeg,image/gif"  required style="display:none;">
                                <label>Hình 4</label>
                            </center> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Mô tả sản phẩm</label>
                <div class="col-sm-8">
                    <textarea class="form-control" placeholder="Mô tả sản phẩm" data-toggle="tooltip" data-placement="bottom" rows="5" title="Tooltip for name" id="wysiwig_full" name="product_description"></textarea>
                </div>
            </div>
            <div class="form-group" id="progressbar" style="display: none;">
                <center>
                    <progress></progress>
                </center>
            </div>
            <div class="form-group" style="margin-top: 40px;">
                <div class="col-sm-offset-4 col-sm-2">
                    <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                        <span><i class="fa fa-clock-o txt-danger"></i></span>
                        Nhập lại
                    </button>
                </div>
                <div class="col-sm-2">
                    <button type="button" class="btn btn-success btn-label-left" name="register" id="submit">
                        <span><i class="fa fa-clock-o"></i></span>
                        Đồng ý
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>