<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<div class="row">
    <div id="breadcrumb" class="col-md-12">
        <ol class="breadcrumb" style="text-align: left;font-size: 14px;">
            <li><a href="<?php echo URL;?>admin">Quản trị</a></li>
            <li><a href="<?php echo URL;?>product/label/">Danh sách nhãn sản phẩm</a></li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <span>Form thêm nhãn sản phẩm</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding"><br/>
                <div id="error"></div>
                <form class="form-horizontal" role="form" action="<?php echo URL;?>product/label/" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="nutrition_type" id="nutrition_type" value="">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tên nhãn sản phẩm</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Nhãn sản phẩm" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="product_label" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Chọn màu sắc của nhãn</label>
                        <div class="col-sm-8">
                            <p id="colorpickerHolder"></p>
                            <input type="text" class="form-control" maxlength="6" size="6" id="colorpickerField2" value="#ff0000" name="product_labelcolor" required/>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-success btn-label-left" name="register" id="submit">
                                <span><i class="fa fa-clock-o"></i></span>
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php
    $listlabel = $this->listlabel;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <i class="fa fa-gift"></i>
                    <span>Danh sách nhãn sản phẩm</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding">
                <table class="table table-bordered table-striped table-hover table-heading table-datatable" id="datatable-1">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Tên nhãn sản phẩm</th>
                        <th>Màu sắc</th>
                        <th>Chỉnh sửa</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($this->listlabel){
                        $i=0;
                        foreach($this->listlabel as $lp){
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i;?></td>
                                <td><?php echo $lp['label_name'];?></td>
                                <td>
                                    <div style="background-color: <?php echo $lp['label_color'];?>;height: 30px;width: 30px;border:5px solid #ddd;border-radius: 5px;">
                                </td>
                                <td>
                                    <a class="btn btn-primary" href="<?php echo URL;?>product/editlabel/<?php echo $lp['label_id'];?>">Edit</a>
                                    <a class="btn btn-danger" href="<?php echo URL;?>product/deletelabel/<?php echo $lp['label_id'];?>">Delete</a>
                                </td>
                            </tr>
                        <?php }} ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>