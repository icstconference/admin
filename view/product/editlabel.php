<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<?php
    $labelinfo = $this->labelinfo;
?>
<div class="row">
    <div id="breadcrumb" class="col-md-12">
        <ol class="breadcrumb" style="text-align: left;font-size: 14px;">
            <li><a href="<?php echo URL;?>admin">Quản trị</a></li>
            <li><a href="<?php echo URL;?>product/label/">Danh sách nhãn sản phẩm</a></li>
            <li><a href="<?php echo URL;?>product/editlabel/<?php echo $labelinfo[0]['label_id'];?>"><?php echo $labelinfo[0]['label_name'];?></a></li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <span>Form thêm nhãn sản phẩm</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding"><br/>
                <div id="error"></div>
                <form class="form-horizontal" role="form" action="<?php echo URL;?>product/editlabel/<?php echo $labelinfo[0]['label_id'];?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tên nhãn sản phẩm</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo $labelinfo[0]['label_name'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="product_label" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Chọn màu sắc cho nhãn</label>
                        <div class="col-sm-8">
                            <p id="colorpickerHolder1"></p>
                            <input type="text" class="form-control" maxlength="6" size="6" id="colorpickerField2" value="<?php echo $labelinfo[0]['label_color'];?>" name="product_labelcolor" required/>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-success btn-label-left" name="register" id="submit">
                                <span><i class="fa fa-clock-o"></i></span>
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>