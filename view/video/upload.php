<header class="page-header">
    <h2>Upload video</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="<?php echo URL;?>videoupload">
                    <span>Video clip</span>
                </a>
            </li>
            <li>
                <span>Upload video</span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Thêm video</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form method="post" class="form-horizontal" role="form" enctype="multipart/form-data">
            <div class="form-group">
                <label class="col-sm-3 control-label">Tên video</label>
                <div class="col-sm-8">
                    <input type="text" placeholder="Tên video" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="video_name" name="video_name" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Tập tin video( .flv, .mp4)</label>
                <div class="col-sm-3">
                    <input type="file" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="video_upload" name="video_upload" accept="video/*" required>
                </div>
            </div>
            <div class="form-group" id="progressbar" style="display: none;">
                <center>
                    <progress></progress>
                </center>
            </div>
            <div class="form-group" id="videopreview" style="display: none;">
                <center>
                    <video src="" id="preview" width="480" height="320"></video>
                </center>
            </div>
            <div class="form-group" style="margin-top: 40px;">
                <div class="col-sm-offset-4 col-sm-2">
                    <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                        <span><i class="fa fa-clock-o txt-danger"></i></span>
                        Nhập lại
                    </button>
                </div>
                <div class="col-sm-2">
                    <button type="button" class="btn btn-success btn-label-left" name="register" id="submit">
                        <span><i class="fa fa-clock-o"></i></span>
                        Đồng ý
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>