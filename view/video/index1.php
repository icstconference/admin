<header class="page-header">
    <h2>Video clip</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Video clip</span></li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<?php
    $listvideo = $this->listvideo;
?>
<?php     
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];
?>
<div class="row" style="margin: 0 0 10px;text-align:right;">
    <a href="<?php echo URL;?>videoupload/upload/" class="mb-xs mt-xs mr-xs btn btn-primary">
        <i class="fa fa-file-video-o"></i>&nbsp;Upload video
    </a>
    <button href="#modalHeaderColorPrimary" class="mb-xs mt-xs mr-xs modal-basic btn btn-danger" disabled id="deletepost">
        <i class="fa fa-trash"></i>&nbsp;Xóa video
    </button>
</div>
<span id="typepost" rel="video"></span>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Danh sách video</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-default">
            <thead>
                <tr>
                    <th><input name="select_all" value="1" id="example-select-all" type="checkbox" /></th>
                    <th>Video</th>
                    <th>Link video</th>
                    <th>Ngày tạo</th>
                    <th>Chỉnh sửa</th>
                </tr>
            </thead>
            <tbody>
            <?php
                if($this->listvideo){
                    $i=0;
                    foreach($this->listvideo as $lp){
                        $i++;
            ?>
                <tr>
                    <td><?php echo $lp['video_upload_id'];?></td>
                    <td>
                        <?php echo $lp['video_upload_name'];?>
                    </td>
                    <td>
                        <?php echo $lp['video_upload_url'];?>
                    </td>
                    <td>
                        <?php echo date('d-m-Y h:i:s',strtotime($lp['video_upload_create_date']));?>
                    </td>
                    <td>
                        <?php                                         
                            if(strpos($edit[0]['permission_detail'],'galleries') || $_SESSION['user_id'] == 1){                                    
                        ?> 
                        <a href="<?php echo URL.$this->lang.'/videoupload/edit/'.$lp['video_upload_id'];?>" class="btn btn-info">
                            <i class="fa fa-edit"></i>
                        </a>
                        <?php } ?>
                        <?php                                         
                            if(strpos($delete[0]['permission_detail'],'galleries') || $_SESSION['user_id'] == 1){                                    
                        ?> 
                        <a href="<?php echo URL.$this->lang.'/videoupload/delete/'.$lp['video_upload_id'];?>" class="btn btn-danger">
                            <i class="fa fa-trash"></i>
                        </a>
                        <?php } ?>
                    </td>
                </tr>
            <?php }} ?>
            </tbody>
        </table>
    </div>
</section>
<div id="modalHeaderColorPrimary" class="modal-block modal-header-color modal-block-primary mfp-hide">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Thông báo</h2>
            </header>
            <div class="panel-body">
                <div class="modal-wrapper">
                    <div class="modal-icon">
                        <i class="fa fa-question-circle"></i>
                    </div>
                    <div class="modal-text">
                        <p>Xóa vĩnh viễn ?</p>
                    </div>
                </div>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button class="btn btn-primary modal-confirm">Đồng ý</button>
                        <button class="btn btn-default modal-dismiss">Hủy bỏ</button>
                    </div>
                </div>
            </footer>
        </section>
    </div>