<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<div class="row">
    <div id="breadcrumb" class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo URL;?>admin">Quản trị</a></li>
            <li><a href="<?php echo URL;?>video">Danh sách video</a></li>
        </ol>
    </div>
</div>
<?php     
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];
	if(strpos($create[0]['permission_detail'],'video') || $_SESSION['user_id'] == 1){
?>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <span>Form thêm video</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding"><br/>
                <div id="error"></div>
                <form method="post" class="form-horizontal" role="form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Link youtube</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Link youtube" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="youtube_url" required>
                            <input type="hidden" class="form-control" placeholder="Link youtube" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="youtube_name" name="youtube_name">
                            <input type="hidden" class="form-control" placeholder="Link youtube" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="youtube_image" name="youtube_image">
                            <input type="hidden" class="form-control" placeholder="Link youtube" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="youtube_link" name="youtube_url">
                        </div>
                    </div>
                    <div class="row" style="margin:0px;display:none;" id="videobox">
                        <div class="text-center">
                            <h4>Video đã chọn</h4>
                        </div>
                        <div class="col-sm-8 col-sm-offset-2" id="videoembed">

                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-sm-2 control-label">Thứ tự video</label>
                        <div class="col-sm-1">
                            <input type="number" class="form-control" value="1" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="youtube_sort" name="youtube_sort" required>
						</div>
					</div>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-success btn-label-left" name="register" id="submit">
                                <span><i class="fa fa-clock-o"></i></span>
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<?php
    $listvideo = $this->listvideo;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <i class="fa fa-gift"></i>
                    <span>Danh sách video</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding">
                <table class="table table-bordered table-striped table-hover table-heading table-datatable" id="datatable-1">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Video</th>
                        <th>Ngày tạo</th>
						<th>Thứ tự</th>
                        <th>Chi tiết</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($this->listvideo){
                        $i=0;
                        foreach($this->listvideo as $lp){
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i;?></td>
                                <td>
                                    <?php 
                                        if(strlen($lp['video_image']) > 1){
                                    ?>
                                        <img src="<?php echo $lp['video_image'];?>" class="img-responsive" style="display:inline;">
                                    <?php }else{ ?>
                                        <img src="<?php echo URL;?>public/img/noneimage.jpg" class="img-responsive" style="display:inline;">
                                    <?php } ?>
                                    <?php echo $lp['video_name'];?>
                                </td>
                                <td>
                                    <?php echo date('d-m-Y h:i:s',strtotime($lp['video_create_date']));?>
                                </td>
								<td>
                                    <?php echo $lp['video_sort'];?>
                                </td>
                                <td>
									<?php                                         
										if(strpos($edit[0]['permission_detail'],'video') || $_SESSION['user_id'] == 1){                                    
									?> 
                                    <a href="<?php echo URL.$this->lang.'/video/editvideo/'.$lp['video_id'];?>" class="btn btn-info">
                                        Edit
                                    </a>
									<?php } ?>
									<?php                                         
										if(strpos($delete[0]['permission_detail'],'video') || $_SESSION['user_id'] == 1){                                    
									?> 
                                    <a href="<?php echo URL.$this->lang.'/video/deletevideo/'.$lp['video_id'];?>" class="btn btn-danger">
                                        Xóa
                                    </a>
									<?php } ?>
                                </td>
                            </tr>
                        <?php }} ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>