<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<?php 
	$videoinfo = $this->videoinfo;
?>
<div class="row">
    <div id="breadcrumb" class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo URL;?>admin">Quản trị</a></li>
            <li><a href="<?php echo URL;?>video">Danh sách video</a></li>
			<li><a href="<?php echo URL;?>video/editvideo/<?php echo $videoinfo[0]['video_id'];?>/"><?php echo $videoinfo[0]['video_name'];?></a></li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <span>Form chỉnh sửa video</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding"><br/>
                <div id="error"></div>
                <form method="post" class="form-horizontal" role="form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Link youtube</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo $videoinfo[0]['video_url'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="youtube_url" name="youtube_url" disabled required>
                        </div>
                    </div>
                    <div class="row" style="margin:0px;" id="videobox">
                        <div class="text-center">
                            <h4>Video đã chọn</h4>
                        </div>
                        <div class="col-sm-8 col-sm-offset-2" id="videoembed">
							<center><iframe width="640" height="360" src="<?php echo $videoinfo[0]['video_url'];?>" frameborder="0" allowfullscreen></iframe></center>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-sm-2 control-label">Thứ tự video</label>
                        <div class="col-sm-1">
                            <input type="number" class="form-control" value="<?php echo $videoinfo[0]['video_sort'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="youtube_sort" name="youtube_sort" required>
						</div>
					</div>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-success btn-label-left" name="register" id="submit">
                                <span><i class="fa fa-clock-o"></i></span>
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
