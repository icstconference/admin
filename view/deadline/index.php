<header class="page-header">
    <h2>Tất cả deadline</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <span>Tất cả deadline</span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<?php                            
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Thêm deadline mới</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label class="col-sm-3 control-label">Tiêu đề deadline</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" placeholder="Tiêu đề deadline" data-toggle="tooltip" data-placement="bottom" title="Nhập tiêu đề bài viết" id="deadline_name" name="deadline_name" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Chọn sự kiện</label>
                <div class="col-sm-8">
                    <select id="s2_with_tag" data-plugin-selectTwo class="form-control populate" name="event_id">
                        <?php
                                
                        if($this->listevent){
                            foreach($this->listevent as $lt){
                        ?>
                            <option value="<?php echo $lt['event_id'];?>"><?php echo $lt['event_name'];?></option>
                        <?php }} ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="row" style="margin:0;">
                    <label class="col-sm-3 control-label">Ngày kết thúc</label>
                    <div class="col-sm-3">
                        <input type="date" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="deadline_date" name="deadline_date" value="<?php echo date('Y-m-d');?>" required>
                    </div>
                </div>
            </div>
			<?php                                         
			     if($_SESSION['user_type'] == 1 || strpos($create[0]['permission_detail'],'event')){                                    
			?> 
            <div class="form-group" style="margin-top: 40px;">
                <div class="col-sm-offset-4 col-sm-2">
                    <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                        <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Nhập lại
                    </button>
                </div>
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-success btn-label-left" name="register" id="submit">
                        <span><i class="fa fa-clock-o"></i></span>
                            Đồng ý
                    </button>
                </div>
            </div>
			<?php } ?>
        </form>
    </div>
</section>
<?php
    $listdeadline = $this->listdeadline;
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Tất cả deadline</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-default">
            <thead>
                    <tr>
                        <th>STT</th>
                        <th>Tên deadline</th>
                        <th>Sự kiện</th>
                        <th>Ngày kết thúc</th>
                        <th>Tùy chỉnh</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($this->listdeadline){
                        $i=0;
                        foreach($this->listdeadline as $lp){
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i;?></td>
                                <td>
                                    <?php echo $lp['deadline_name'];?>
                                </td>
                                <td>
                                    <?php
                                    if($this->listevent){
                                        foreach($this->listevent as $lt){
                                            if($lp['event_id'] == $lt['event_id']){
                                    ?>
                                        <?php echo $lt['event_name'];?>
                                    <?php }}} ?>
                                </td>
                                <td>
                                    <?php echo $lp['deadline_date'];?>
                                </td>
                                <td>
                                    <?php                                         
                                        if(strpos($edit[0]['permission_detail'],'event') || $_SESSION['user_type'] == 1){                                    
                                    ?>
                                    <a class="btn btn-primary" href="<?php echo URL;?>deadline/editdeadline/<?php echo $lp['deadline_id'];?>">Edit</a>
                                    <?php } ?>                                    
                                    <?php                                         
                                        if(strpos($delete[0]['permission_detail'],'event') || $_SESSION['user_type'] == 1){                                    
                                    ?>  
                                    <a class="btn btn-danger" href="<?php echo URL;?>deadline/deletedeadline/<?php echo $lp['deadline_id'];?>">Delete</a>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php }} ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>