<?php 
    $actual_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER[REQUEST_URI];
    function cut_string($str, $len, $more) {
        if ($str == "" || $str == NULL)
            return $str;
        if (is_array($str))
            return $str;
        $str = trim($str);
        if (strlen($str) <= $len)
            return $str;
        $str = substr($str, 0, $len);
        if ($str != "") {
            if (!substr_count($str, " ")) {
                if ($more)
                    $str .= " ...";
                return $str;
            }
            while (strlen($str) && ($str[strlen($str) - 1] != " ")) {
                $str = substr($str, 0, -1);
            }
            $str = substr($str, 0, -1);
            if ($more)
                $str .= " ...";
        }
        return $str;
    }

    $pageinfo = $this->pageinfo;
?>
<section id="page-top">
        <div id="main-slide1" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="item active">
                    <img class="img-responsive" src="<?php echo $pageinfo[0]['page_thumbnail'];?>" alt="slider">
                    <div class="slider-content">
                        <div class="col-md-12 text-center">
                            <h1  style="margin-bottom:25px;font-size:50px;color:#2969b0;" id="sliderlon">
                                <div style="line-height: 80px;font-weight: 300;font-size: 48px;margin-top: 70px;">
                                    <div style="font-weight:400;">
                                    <?php echo $pageinfo[0]['page_name'];?>
                                    </div>
                                </div>
                            </h1>
                            <?php 
                                if(strpos(strtolower($pageinfo[0]['page_name']),'submission') !== false){
                            ?>
                            <a class="slider btn buttonblue btn-min-block" href="#">REGISTER NOW</a>
                             <a class="slider btn buttonblue btn-min-block" href="#">SIGN IN</a>
                            <?php
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<section id="feature" class="feature-section" style="padding:50px 0;">
    <div class="container" style="padding:0;">
        <div class="row" style="margin:0;">
            <?php echo $pageinfo[0]['page_description'];?>
        </div>
    </div>
</section>

