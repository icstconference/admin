<header class="page-header">
    <h2>Tất cả trang</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <span>Tất cả trang</span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<?php                            
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];
?>
<div class="row" style="margin: 0 0 10px;text-align:right;">
    <a href="<?php echo URL;?>page/addpage/" class="mb-xs mt-xs mr-xs btn btn-primary">
        <i class="fa fa-file-o"></i>&nbsp;Thêm trang
    </a>
    <button href="#modalHeaderColorPrimary" class="mb-xs mt-xs mr-xs modal-basic btn btn-danger" disabled id="deletepost">
        <i class="fa fa-trash"></i>&nbsp;Xóa trang
    </button>
</div>
<span id="typepost" rel="page"></span>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Tất cả trang</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-default">
            <thead>
                <tr>
                    <th style="vertical-align:middle;text-align:center;"><input name="select_all" value="1" id="example-select-all" type="checkbox" /></th>
                    <th style="vertical-align:middle;">Trang</th>
                    <th style="vertical-align:middle;">Thumbnail</th>
                    <th style="vertical-align:middle;">Ngày viết</th>
                    <th style="vertical-align:middle;">Tùy chỉnh</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($this->listpage){
                        $i=0;
                        foreach($this->listpage as $lp){
                            $i++;
                ?>
                <tr>
                    <td style="vertical-align:middle;text-align:center;"><?php echo $lp['page_id'];?></td>
                    <td style="vertical-align:middle;">
                        <?php echo $lp['page_name'];?>
                    </td>
                    <td style="vertical-align:middle;">
                        <center>
                            <?php
                                if(strlen($lp['page_thumbnail']) > 1){
                            ?>
                                <img src="<?php echo $lp['page_thumbnail'];?>" style="width:60px;"/>
                            <?php } else {?>
                                <img src="<?php echo URL;?>public/img/noneimage.jpg" style="width:60px;"/>
                            <?php } ?>
                        </center>
                    </td>
                    <td style="vertical-align:middle;">
                        <?php echo $lp['page_create_date'];?>
                    </td>
                    <td style="vertical-align:middle;width:21%;">
                        <?php                                         
                            if(strpos($edit[0]['permission_detail'],'page') || $_SESSION['user_type'] == 1){                                    
                        ?>
                        <a class="btn btn-primary" href="<?php echo URL;?>page/edit/<?php echo $lp['page_id'];?>" data-toggle="tooltip" data-placement="bottom" title="Sửa trang"><i class="fa fa-edit"></i></a>
                        <?php } ?>                                    
                        <?php                                         
                            if(strpos($delete[0]['permission_detail'],'page') || $_SESSION['user_type'] == 1){                                    
                        ?> 
                        <a class="btn btn-danger"href="<?php echo URL;?>page/delete/<?php echo $lp['page_id'];?>" data-toggle="tooltip" data-placement="bottom" title="Xóa trang"><i class="fa fa-trash"></i></a>
                        <?php } ?>
                        <a class="btn btn-warning" href="<?php echo URL;?>page/copy/<?php echo $lp['page_id'];?>" data-toggle="tooltip" data-placement="bottom" title="Sao chép trang"><i class="fa fa-copy"></i>&nbsp;</a>
                        <?php 
                            $allsetting = $this->allsetting;
                            if($allsetting[0]['config_url'] == 'dai'){
                        ?>
                            <a class="btn btn-success" href="<?php echo URL;?>page/detail/<?php echo $lp['page_url'];?>.html" data-toggle="tooltip" data-placement="bottom" title="Xem trang" target="_blank"><i class="fa fa-eye"></i></a>
                        <?php }else{ ?>
                            <a class="btn btn-success" href="<?php echo URL;?><?php echo $lp['page_url'];?>.html" data-toggle="tooltip" data-placement="bottom" title="Xem trang" target="_blank"><i class="fa fa-eye"></i></a>
                        <?php } ?>
                    </td>
                </tr>
                <?php }} ?>
            </tbody>
        </table>
    </div>
</section>
<div id="modalHeaderColorPrimary" class="modal-block modal-header-color modal-block-primary mfp-hide">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Thông báo</h2>
            </header>
            <div class="panel-body">
                <div class="modal-wrapper">
                    <div class="modal-icon">
                        <i class="fa fa-question-circle"></i>
                    </div>
                    <div class="modal-text">
                        <p>Xóa vĩnh viễn ?</p>
                    </div>
                </div>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button class="btn btn-primary modal-confirm">Đồng ý</button>
                        <button class="btn btn-default modal-dismiss">Hủy bỏ</button>
                    </div>
                </div>
            </footer>
        </section>
    </div>