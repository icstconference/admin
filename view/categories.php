<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<header class="page-header">
    <h2>Chuyên mục bài viết</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Chuyên mục bài viết</span></li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<?php     
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit']; 
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Thêm chuyên mục</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label class="col-sm-2 control-label">Tên chuyên mục</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" placeholder="Loại chuyên mục" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="news_type" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Tên chuyên mục tiếng anh</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" placeholder="Loại chuyên mục tiếng anh" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="news_type_english" required>
                </div>
            </div>
            <?php
                if($this->listcategoriesnews){
            ?>
            <div class="form-group">
                <label class="col-sm-2 control-label">Chọn chuyên mục cha</label>
                <div class="col-sm-8">
                    <select id="s2_with_tag" data-plugin-selectTwo class="form-control populate" name="news_fathertype">
                        <option value="0">Thư mục chính</option>
                        <?php foreach($this->listcategoriesnews as $lt){?>
                            <option value="<?php echo $lt['news_categories_id'];?>"><?php echo $lt['news_categories_name'];?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <?php } ?>
            <div class="form-group" style="margin-top: 40px;">
                <div class="col-sm-offset-4 col-sm-2">
                    <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                        <span><i class="fa fa-clock-o txt-danger"></i></span>
                        Nhập lại
                    </button>
                </div>
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-success btn-label-left" name="register" id="submit">
                        <span><i class="fa fa-clock-o"></i></span>
                        Đồng ý
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>
<?php
    $listcategoriesnews = $this->listcategoriesnews;
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Danh sách chuyên mục</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-default">
            <thead>
                <tr>
                    <th style="vertical-align:middle;text-align:center;">STT</th>
                    <th style="vertical-align:middle;">Tên chuyên mục</th>
                    <th style="vertical-align:middle;">Chuyên mục cha</th>
                    <th style="vertical-align:middle;">Chỉnh sửa</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($this->listcategoriesnews){
                        $i=0;
                        foreach($this->listcategoriesnews as $lp){
                            $i++;
                ?>
                    <tr>
                        <td style="vertical-align:middle;text-align:center;"><?php echo $i;?></td>
                        <td style="vertical-align:middle;"><?php echo $lp['news_categories_name'];?></td>
                        <td style="vertical-align:middle;">
                            <?php
                                if($lp['news_categories_father'] != 0){
                                    foreach($this->listcategoriesnews as $ls){
                                        if($ls['news_categories_id'] == $lp['news_categories_father']){
                            ?>
                            <?php echo $ls['news_categories_name'];?>
                            <?php }}}else{?>
                                Thư mục chính
                            <?php } ?>
                        </td>
                        <td style="vertical-align:middle;text-align:left;">
							<?php                                         
								if($_SESSION['user_type'] == 1 || strpos($edit[0]['permission_detail'],'new')){                                    
							?>
                            <a class="btn btn-primary" href="<?php echo URL;?>news/editcategories/<?php echo $lp['news_categories_id'];?>"><i class="fa fa-edit"></i></a>
                            <?php } ?>                                    
							<?php                                         
								if($_SESSION['user_type'] == 1 || strpos($delete[0]['permission_detail'],'new')){                                    
							?>  
							<a class="btn btn-danger" href="<?php echo URL;?>news/deletecategories/<?php echo $lp['news_categories_id'];?>"><i class="fa fa-trash"></i></a>
							<?php } ?>
                            <?php 
                                $allsetting = $this->allsetting;
                                if($allsetting[0]['config_url'] == 'dai'){
                            ?>
                            <a class="btn btn-success" href="<?php echo URL;?>category/news/<?php echo $lp['news_categories_url'];?>.html" target="_blank"><i class="fa fa-eye"></i></a>
                            <?php }else{ ?>
                            <a class="btn btn-success" href="<?php echo URL;?><?php echo $lp['news_categories_url'];?>.html" target="_blank"><i class="fa fa-eye"></i></a>
                            <?php } ?>
						</td>
                    </tr>
                <?php }} ?>
            </tbody>
        </table>
    </div>
</section>