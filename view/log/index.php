<header class="page-header">
    <h2>Tất cả log</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <span>Tất cả log</span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Tất cả log</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-default">
            <thead>
                <tr>
                    <th style="vertical-align:middle;text-align:center;">STT</th>
                    <th style="vertical-align:middle;">Username</th>
                    <th style="vertical-align:middle;">Hành động</th>
                    <th style="vertical-align:middle;">Ngày thực hiện</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($this->listlog){
                        $i=0;
                        foreach($this->listlog as $lp){
                            $i++;
                ?>
                <tr>
                    <td style="vertical-align:middle;text-align:center;"><?php echo $i;?></td>
                    <td style="vertical-align:middle;">
                        <?php echo $lp['user_name'];?>
                    </td>
                    <td style="vertical-align:middle;">
                        <?php 
                            if($lp['log_action'] == 'create'){
                                echo 'Khởi tạo <b>'.$lp['log_object_name'].'</b> trong '.$lp['log_object'];
                            }elseif($lp['log_action'] == 'update'){
                                echo 'Cập nhật <b>'.$lp['log_object_name'].'</b> trong '.$lp['log_object'];
                            }elseif($lp['log_action'] == 'delete'){
                                echo 'Xóa <b>'.$lp['log_object_name'].'</b> trong '.$lp['log_object'];
                            }
                        ?>
                    </td>
                    <td style="vertical-align:middle;">
                        <?php echo $lp['log_create_date'];?>
                    </td>
                </tr>
                <?php }} ?>
            </tbody>
        </table>
    </div>
</section>