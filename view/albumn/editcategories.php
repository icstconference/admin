<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<?php 
    $categoriesinfo = $this->categoriesinfo;
?>
<div class="row">
    <div id="breadcrumb" class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo URL;?>admin">Quản trị</a></li>
            <li><a href="<?php echo URL;?>albumn/categories/">Danh sách loại albumn</a></li>
            <li><a href="<?php echo URL;?>albumn/editcagories/<?php echo $categoriesinfo[0]['albumn_categories_id'];?>"><?php echo $categoriesinfo[0]['albumn_categories_name'];?></a></li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <span>Form chỉnh sửa loại albumn</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding"><br/>
                <div id="error"></div>
                <form class="form-horizontal" method="post" role="form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tên loại albumn</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo $categoriesinfo[0]['albumn_categories_name'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="albumn_categories_name" name="albumn_categories_name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tên loại albumn tiếng anh</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo $categoriesinfo[0]['albumn_categories_name_english'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="albumn_categories_name_english" name="albumn_categories_name_english" required>
                        </div>
                    </div>
                    <?php
                        if($this->listcategories){
                    ?>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Chọn loại albumn cha ( nếu có )</label>
                        <div class="col-sm-8">
                            <select id="s2_with_tag" class="populate placeholder" name="albumn_categories_father">
                                <?php 
                                    if($categoriesinfo[0]['albumn_categories_father'] != 0){
                                        foreach($this->listcategories as $lt){
                                            if($lt['albumn_categories_id'] == $categoriesinfo[0]['albumn_categories_father']){
                                ?>
                                    <option value="<?php echo $lt['albumn_categories_id'];?>" selected><?php echo $lt['albumn_categories_name'];?></option>
                                <?php }else{ ?>
                                    <option value="<?php echo $lt['albumn_categories_id'];?>"><?php echo $lt['albumn_categories_name'];?></option>
                                <?php }}}else{ ?>
                                <option value="0">Thư mục chính</option>
                                <?php foreach($this->listcategories as $lt){?>
                                    <option value="<?php echo $lt['albumn_categories_id'];?>"><?php echo $lt['albumn_categories_name'];?></option>
                                <?php }} ?>
                            </select>
                        </div>
                    </div>
                    <?php } ?>
                    <?php 
                        if(strpos($_SESSION['user_permission_edit'],'albumn') || $_SESSION['user_id'] == 1){
                    ?>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-success btn-label-left" name="register" id="submit">
                                <span><i class="fa fa-clock-o"></i></span>
                                Submit
                            </button>
                        </div>
                    </div>
                    <?php } ?>
                </form>
            </div>
        </div>
    </div>
</div>