<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<div class="row">
    <div id="breadcrumb" class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo URL;?>admin">Quản trị</a></li>
            <li><a href="<?php echo URL;?>albumn">Danh sách albumn</a></li>
        </ol>
    </div>
</div>
<?php 
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit']; 
    if(strpos($create[0]['permission_detail'],'albumn') || $_SESSION['user_id'] == 1){
?>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <span>Form thêm albumn</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding"><br/>
                <div id="error"></div>
                <form class="form-horizontal" method="post" role="form" enctype="multipart/form-data">
                    <input type="hidden" name="nutrition_type" id="nutrition_type" value="">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tên albumn</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Tên albumn" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="albumn_name" name="albumn_name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tên albumn tiếng anh</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Tên albumn tiếng anh" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="albumn_name_english" name="albumn_name_english" required>
                        </div>
                    </div>
                    <?php
                    if($this->listcategories){
                        ?>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Chọn loại albumn</label>
                            <div class="col-sm-8">
                                <select id="s2_with_tag" class="populate placeholder" name="albumn_categories">
                                    <option value="0">Thư mục chính</option>
                                    <?php foreach($this->listcategories as $lt){?>
                                        <option value="<?php echo $lt['albumn_categories_id'];?>"><?php echo $lt['albumn_categories_name'];?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nội dung albumn</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" placeholder="Nội dung albumn" rows="5" title="Tooltip for name" id="wysiwig_full1" name="albumn_description"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nội dung albumn tiếng anh</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" placeholder="Nội dung albumn tiếng anh" rows="5" title="Tooltip for name" id="wysiwig_full2" name="albumn_description_english"></textarea>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-success btn-label-left" name="register" id="submit">
                                <span><i class="fa fa-clock-o"></i></span>
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<?php
    $listalbumn = $this->listalbumn;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <i class="fa fa-gift"></i>
                    <span>Danh sách albumn</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding">
                <table class="table table-bordered table-striped table-hover table-heading table-datatable" id="datatable-1">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Albumn</th>
                        <th>Ngày tạo</th>
                        <th>Tùy chỉnh</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($this->listalbumn){
                        $i=0;
                        foreach($this->listalbumn as $lp){
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i;?></td>
                                <td>
                                    <?php echo $lp['albumn_name'];?>
                                </td>
                                <td>
                                    <?php echo $lp['albumn_create_date'];?>
                                </td>
                                <td>
                                    <?php 
                                        if(strpos($edit[0]['permission_detail'],'albumn') || $_SESSION['user_id'] == 1){
                                    ?>
                                    <a class="btn btn-primary" href="<?php echo URL;?>albumn/editalbumn/<?php echo $lp['albumn_id'];?>">Edit</a>
                                    <?php } ?>
                                    <?php 
                                        if(strpos($delete[0]['permission_detail'],'albumn') || $_SESSION['user_id'] == 1){
                                    ?>
                                    <a class="btn btn-danger" href="<?php echo URL;?>albumn/deletealbumn/<?php echo $lp['albumn_id'];?>">Delete</a>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php }} ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>