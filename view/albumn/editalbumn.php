<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
    fieldset {
        background: #eee;
        border: 2px groove #ccc;
        padding: 0.35em 0.625em 0.75em;
    }
    legend {
        font-size: 18px;
        margin-left: 5px;
        width: 120px;
    }
    #dropArea {
        float: left;
        font-size: 20px;
        font-weight: bold;
        height: 130px;
        line-height: 130px;
        position: relative;
        text-align: center;
        width: 100%;
    }
    #dropArea.hover {
        background-color: #CCCCCC;
    }
</style>
<?php 
    $albumninfo = $this->albumninfo;
?>
<div class="row">
    <div id="breadcrumb" class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo URL;?>admin">Quản trị</a></li>
            <li><a href="<?php echo URL;?>albumn">Danh sách albumn</a></li>
            <li><a href="<?php echo URL;?>albumn/editalbumn/<?php echo $albumninfo[0]['albumn_id'];?>"><?php echo $albumninfo[0]['albumn_name'];?></a></li>
        </ol>
    </div>
</div>
<?php 
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit']; 
    if(strpos($edit[0]['permission_detail'],'albumn') || $_SESSION['user_id'] == 1){
?>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <span>Form thêm hình ảnh albumn</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content">
                <div class="row" style="margin:0;">
					<div class="col-sm-12">
						<form class="form-horizontal" role="form" enctype="multipart/form-data">
							<input type="hidden" name="albumn_id" value="<?php echo $albumninfo[0]['albumn_id'];?>" />
							<div class="form-group">
								<label class="col-sm-3 control-label">Hình ảnh</label>
								<div class="col-sm-8">
									<input type="file" class="form-control" id="albumn_image" name="albumn_image[]" multiple required>
								</div>
							</div>
							<div class="form-group" id="progressbar" style="display: none;">
								<center>
									<progress></progress>
								</center>
							</div>
							<div class="form-group" style="margin-top: 20px;">
								<center>
									<button type="button" class="btn btn-success btn-label-left" id="uploadmulti">
										<span><i class="fa fa-clock-o"></i></span>
										Submit
									</button>
								</center>
							</div>
						</form>
					</div>
					<center>
						<b>HOẶC</b>
					</center>
                    <div class="col-sm-12" style="background:#ccc;border-radius:5px;margin-top:10px;">
                        <div class="col-sm-12" id="dropArea" style="padding: 0px;">
                            Kéo thả hình ảnh vào đây
                        </div>
                        <div style="display: none;">Files left: <span id="count">0</span></div>
                        <input type="hidden" id="url" value="<?php echo URL.$this->lang;?>/albumn/uploaddrag/<?php echo $albumninfo[0]['albumn_id'];?>"/>
                        <input type="hidden" id="postid" value="<?php echo $albumninfo[0]['albumn_id'];?>"/>
                        <center><canvas id="progess" width="500" height="20" style="margin-bottom:10px;"></canvas></center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<?php 
    $listimage = $this->listimage;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <i class="fa fa-gift"></i>
                    <span>Danh sách hình ảnh trong albumn</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding">
                <table class="table table-bordered table-striped table-hover table-heading table-datatable" id="datatable-1">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Hình ảnh</th>
                        <th>Link</th>
                        <th>Ngày tạo</th>
                        <th>Tùy chỉnh</th>
                    </tr>
                    </thead>
                    <tbody id="result">
                    <?php
                    if($this->listimage){
                        $i=0;
                        foreach($this->listimage as $lp){
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i;?></td>
                                <td>
                                    <img src="<?php echo $lp['albumn_image_thumb_url'];?>" class="img-responsive" />
                                </td>
                                <td>
                                    <?php echo $lp['albumn_image_url'];?>
                                </td>
                                <td>
                                    <?php echo $lp['albumn_image_create_date'];?>
                                </td>
                                <td>
                                    <?php 
                                        if(strpos($_SESSION['user_permission_edit'],'albumn') || $_SESSION['user_id'] == 1){
											if($lp['albumn_image_default']  != '1'){
                                    ?>
									<a class="btn btn-success" href="<?php echo URL;?>albumn/defaultimage/<?php echo $lp['albumn_image_id'];?>/<?php echo $lp['albumn_id'];?>/">Set image album</a>
									<?php } ?>
									<a class="btn btn-primary" href="<?php echo URL;?>albumn/deleteimage/<?php echo $lp['albumn_image_id'];?>">Delete</a>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php }} ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <span>Form chỉnh sửa albumn</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding"><br/>
                <div id="error"></div>
                <form class="form-horizontal" method="post" role="form" enctype="multipart/form-data">
                    <input type="hidden" name="nutrition_type" id="nutrition_type" value="">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tên albumn</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo $albumninfo[0]['albumn_name'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="albumn_name" name="albumn_name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tên albumn tiếng anh</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo $albumninfo[0]['albumn_name_english'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="albumn_name_english" name="albumn_name_english" required>
                        </div>
                    </div>
                    <?php
                    if($this->listcategories){
                        ?>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Chọn loại albumn</label>
                            <div class="col-sm-8">
                                <select id="s2_with_tag" class="populate placeholder" name="albumn_categories">
                                    <option value="0">Thư mục chính</option>
                                    <?php 
                                        if($albumninfo[0]['albumn_categories'] != 0){
                                            foreach ($this->listcategories as $lt) {
                                                if($albumninfo[0]['albumn_categories'] == $lt['albumn_categories_id']){
                                    ?>
                                        <option value="<?php echo $lt['albumn_categories_id'];?>" selected><?php echo $lt['albumn_categories_name'];?></option>
                                    <?php }else{ ?>
                                        <option value="<?php echo $lt['albumn_categories_id'];?>"><?php echo $lt['albumn_categories_name'];?></option>
                                    <?php }}}else{ ?>
                                    <?php foreach($this->listcategories as $lt){?>
                                        <option value="<?php echo $lt['albumn_categories_id'];?>"><?php echo $lt['albumn_categories_name'];?></option>
                                    <?php }} ?>
                                </select>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nội dung albumn</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" rows="5" title="Tooltip for name" id="wysiwig_full1" name="albumn_description"><?php echo $albumninfo[0]['albumn_description'];?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nội dung albumn tiếng anh</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" rows="5" title="Tooltip for name" id="wysiwig_full2" name="albumn_description_english"><?php echo $albumninfo[0]['albumn_description_english'];?></textarea>
                        </div>
                    </div>
                    <?php 
                        if(strpos($edit[0]['permission_detail'],'albumn') || $_SESSION['user_id'] == 1){
                    ?>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-success btn-label-left" name="register" id="submit">
                                <span><i class="fa fa-clock-o"></i></span>
                                Submit
                            </button>
                        </div>
                    </div>
                    <?php } ?>
                </form>
            </div>
        </div>
    </div>
</div>