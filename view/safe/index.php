<style type="text/css">
    .rtf ol li, .rtf ul li {
        margin-left: 5px;
    }
</style>
<div style="margin-top: 0px;background: #f7f7f7;" id="main" role="main">
    <div id="main-contents" class="single-col homepage-wrap" style="margin-top: 160px;">
        <div class="homepage-module">
            <div style="padding-top: 0px; margin-top: 0px; padding-bottom: 0px; margin-bottom: 0px;" class="celebrity_or_video">
                <div class="celebrity_or_video_content_area" style="width: 75%;margin-left:0px;">
                    <center>
                        <img src="<?php echo URL;?>public/img/safe.png" alt="" style="border-width:0px;">
                    </center>
                    <div style="margin-top: 15px;margin-left: 5px;color: #63544d;font-weight: 400;">
                        <b>HỆ THỐNG QUẢN LÝ AT-CL-MT</b><br />
                        <br />
                        PV GAS D xây dựng Hệ thống quản lý AT–CL–MT tích hợp theo các yêu cầu tiêu chuẩn Quốc tế ISO 9001:2008, ISO 14001:2004 & OHSAS 18001:2007, trong đó chú trọng hoàn thiện và cải tiến hệ thống quản lý các hoạt động sản xuất kinh doanh của Công ty với mục đích thỏa mãn tối đa các yêu cầu khách hàng bằng chất lượng sản phẩm, dịch vụ, tuân thủ các yêu cầu về an toàn, sức khỏe và môi trường và các yêu cầu khác. PV GAS D xây dựng hệ thống quản lý tích hợp An toàn – Chất lượng - Môi trường theo mô hình Plan – Do - Check – Act
                        <br/><br/>
                        PV GAS D xây dựng hệ thống quản lý tích hợp An toàn – Chất lượng - Môi trường theo mô hình Plan – Do - Check – Act
                        <br/><br/>
                        PV GAS D đã được TÜV Rheinland cấp chứng chỉ chứng nhận Hệ thống Quản lý AT-CL-MT theo các yêu cầu tiêu chuẩn Quốc tế OHSAS 18001, ISO 9001 và ISO 14001 từ năm 2008 và chứng chỉ đang được áp dụng, duy trì liên tục đến nay.
                    </div>
                </div>

                <div class="celebrity_or_video_text_area" style="width: 24%;">
                    <div class="rtf">
                        <ul style="list-style-type: none;line-height: 25px;">
                            <li>
                                <a href="#" style="color: #63544d;font-size: 16px;text-decoration:none;font-weight: 500;border-bottom: 2px solid #63544d;">HỆ THỐNG - QUẢN LÝ</a>
                            </li>
                            <li>
                                <a href="#" style="color: #63544d;font-size: 16px;text-decoration:none;font-weight: 400;">CHÍNH SÁCH - MỤC TIÊU</a>
                            </li>
                            <li>
                                <a href="#" style="color: #63544d;font-size: 16px;text-decoration:none;font-weight: 400;">CHỨNG NHẬN - CHỨNG CHỈ</a>
                            </li>
                            <li>
                                <a href="#" style="color: #63544d;font-size: 16px;text-decoration:none;font-weight: 400;">HUẤN LUYỆN - ĐÀO TẠO</a>
                            </li>
                            <li>
                                <a href="#" style="color: #63544d;font-size: 16px;text-decoration:none;font-weight: 400;">VIDEO - HÌNH ẢNH</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>