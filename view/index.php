<style type="text/css">'
    .google-maps {
        position: relative;
        height: 0;
    }
    .google-maps iframe {
        top: 0;
        left: 0;
        width: 100% !important;
        z-index:0;
    }
</style>
<div class="content kk-woo-shop clearfix">
    <div class="content-inner-wrapper clearfix">
        <h1 itemprop="name" class="product_title entry-title">
            <?php 
                if($this->lang == 'vi'){
                    echo 'Liên hệ';
                }else{
                    echo 'Contact';
                }
            ?>
        </h1>
        <div class="divider clear"></div>
        <div class="inner-content">
            <div class="row" style="margin:0 0 20px 0;height:450px;">
                <div class="google-maps">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.1303656889168!2d106.67708731528677!3d10.801325992304655!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317528d9d52fc97d%3A0xe6816afd883edeba!2zNDM3IE5ndXnhu4VuIEtp4buHbSwgcGjGsOG7nW5nIDksIFBow7ogTmh14bqtbiwgSOG7kyBDaMOtIE1pbmgsIFZpZXRuYW0!5e0!3m2!1sen!2s!4v1462706072068" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="row" style="margin:10px 0;">
                <form action="<?php echo URL.$this->lang;?>/contact/addcontact/" method="post">
                    <?php 
                        if($this->lang == 'vi'){
                    ?>
                    <input type="text" name="your-name" style="width:100%;border:1px solid #dfdfdf;height:35px;margin-right:22px;padding: 5px;margin-bottom:10px;" placeholder="Họ và tên" required>
                    <input type="email" name="your-email" style="width:100%;border:1px solid #dfdfdf;height:35px;margin-right:22px;padding: 5px;margin-bottom:10px;" placeholder="Email">
                    <input type="text" name="your-phone" style="width:100%;border:1px solid #dfdfdf;height:35px;padding: 5px;margin-bottom:10px;" placeholder="Số điện thoại" required>
                    <label for="your-services">Chọn dịch vụ:</label>&nbsp;
                    <select name="your-services" style="height: 34px;width: 180px;">
                        <option value="Dịch vụ khác">Dịch vụ khác</option>
                        <option value="Dịch vụ">Dịch vụ</option>
                        <option value="Cho thuê trang phục">Cho thuê trang phục</option>
                        <option value="Cho thuê studio">Cho thuê studio</option>
                    </select>
                    <textarea name="your-message" style="width:100%;border:1px solid #dfdfdf;padding: 5px;height: 120px;resize: none;" placeholder="Nội dung" required></textarea>
                    <br/><br/>
                    <div style="float:right;">
                        <button type="submit" class="btnemail" style="text-align:center;">
                            Gửi tin nhắn
                        </button>
                    </div>
                    <?php }else { ?>
                    <input type="text" name="your-name" style="width:100%;border:1px solid #dfdfdf;height:35px;margin-right:22px;padding: 5px;margin-bottom:10px;" placeholder="Fullname" required>
                    <input type="email" name="your-email" style="width:100%;border:1px solid #dfdfdf;height:35px;margin-right:22px;padding: 5px;margin-bottom:10px;" placeholder="Email">
                    <input type="text" name="your-phone" style="width:100%;border:1px solid #dfdfdf;height:35px;padding: 5px;margin-bottom:10px;" placeholder="Phone" required>
                    <label for="your-services">Select service:</label>&nbsp;
                    <select name="your-services" style="height: 34px;width: 180px;">
                        <option value="Other services">Other services</option>
                        <option value="Services">Services</option>
                        <option value="Rental Clothes">Rental Clothes</option>
                        <option value="Rental studio">Rental studio</option>
                    </select>
                    <textarea name="your-message" style="width:100%;border:1px solid #dfdfdf;padding: 5px;height: 120px;resize: none;" placeholder="Description" required></textarea>
                    <br/><br/>
                    <div style="float:right;">
                        <button type="submit" class="btnemail" style="text-align:center;">
                            Send message
                        </button>
                    </div>
                    <?php } ?>
                </form>
            </div>   
        </div>
    </div>
    <div class="kk-content-footer" style="padding:10px 10px 20px 10px;">
            <div class="kk-inner-footer">
                <ul class="social">
                    <li>
                        <a class="kk-icon-facebook" id="kk-fb" title="Facebook" target="_blank" href="<?php echo $this->facebook;?>"></a>
                    </li>
                    <li>
                        <a class="kk-icon-twitter" id="kk-twitter" title="Twitter" target="_blank" href="<?php echo $this->twitter;?>"></a>
                    </li>
                    <li>
                        <a class="kk-icon-gplus" id="kk-google" title="Google Plus" target="_blank" href="<?php echo $this->google;?>"></a>
                    </li>
                    <li>
                        <a class="kk-icon-pinterest" id="kk-pinterest" title="Pinterest" target="_blank" href="#"></a>
                    </li>
                    <li>
                        <a class="kk-icon-instagramm" id="kk-instagram" title="Instagram" target="_blank" href="<?php echo $this->pinterest;?>"></a>
                    </li>
                    <li>
                        <a class="kk-icon-youtube" id="kk-youtube" title="Youtube" target="_blank" href="#"></a>
                    </li>
                    <li>
                        <a class="kk-icon-linkedin" id="kk-linkedin" title="Linkedin" target="_blank" href="#"></a>
                    </li>     
                </ul>
            </div>
        </div>
</div>