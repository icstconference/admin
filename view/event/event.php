<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<header class="page-header">
    <h2>Tất cả sự kiện</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Tất cả sự kiện</span></li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<div class="row" style="margin: 0 0 10px;text-align:right;">
    <a href="<?php echo URL;?>event/addevent/" class="mb-xs mt-xs mr-xs btn btn-primary">
        <i class="fa fa-user-plus"></i>&nbsp;Thêm sự kiện
    </a>
</div>
<?php
    $listevent = $this->listevent;
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Tất cả sự kiện</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-default">
                    <thead>
                    <tr>
                        <th style="vertical-align:middle;">STT</th>
                        <th style="vertical-align:middle;">Sự kiện</th>
                        <th style="vertical-align:middle;">Thumbnail</th>
                        <th style="vertical-align:middle;">Băt đầu</th>
                        <th style="vertical-align:middle;">Kết thúc</th>
                        <th style="vertical-align:middle;">Ngày tạo</th>
                        <th style="vertical-align:middle;width:13%;">Tùy chỉnh</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($this->listevent){
                        $i=0;
                        foreach($this->listevent as $lp){
                            $i++;
                            ?>
                            <tr>
                                <td style="vertical-align:middle;text-align:center;"><?php echo $i;?></td>
                                <td style="vertical-align:middle;">
                                    <?php echo $lp['event_name'];?>
                                </td>
                                <td style="vertical-align:middle;">
                                    <?php
                                    if(strlen($lp['event_image']) > 1){
                                        ?>
                                        <img src="<?php echo $lp['event_image'];?>" class="img-responsive" style="width:80px;display:inline;"/>
                                    <?php } else {?>
                                        <img src="<?php echo URL;?>public/img/noneimage.jpg" class="img-responsive" style="width:80px;display:inline;"/>
                                    <?php } ?>
                                </td>
                                <td style="vertical-align:middle;">
                                    <?php echo date('d/m/Y',strtotime($lp['event_start']));?>
                                </td>
                                <td style="vertical-align:middle;">
                                    <?php echo date('d/m/Y',strtotime($lp['event_end']));?>
                                </td>
                                <td style="vertical-align:middle;">
                                    <?php echo $lp['event_create_date'];?>
                                </td>
                                <td style="vertical-align:middle;">
                                    <a class="btn btn-primary" href="<?php echo URL;?>event/editevent/<?php echo $lp['event_id'];?>"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-danger" href="<?php echo URL;?>event/deletevent/<?php echo $lp['event_id'];?>"><i class="fa fa-trash"></i></a>
                                    <a class="btn btn-warning" href="<?php echo URL;?><?php echo $lp['event_url'];?>"><i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                        <?php }} ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>