<?php 
    $actual_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER[REQUEST_URI];
    function cut_string($str, $len, $more) {
        if ($str == "" || $str == NULL)
            return $str;
        if (is_array($str))
            return $str;
        $str = trim($str);
        if (strlen($str) <= $len)
            return $str;
        $str = substr($str, 0, $len);
        if ($str != "") {
            if (!substr_count($str, " ")) {
                if ($more)
                    $str .= " ...";
                return $str;
            }
            while (strlen($str) && ($str[strlen($str) - 1] != " ")) {
                $str = substr($str, 0, -1);
            }
            $str = substr($str, 0, -1);
            if ($more)
                $str .= " ...";
        }
        return $str;
    }
?>
<?php 
    $eventinfo = $this->eventinfo;
?>
<div class="page_content_wrap" >
    <div class="content">
        <article class="itemscope post_item post_item_single_courses post_featured_center post-664 courses type-courses status-publish has-post-thumbnail hentry courses_group-medicine courses_tag-medicine" itemscope itemtype="http://schema.org/Review">            
            <section class="post_featured bg_tint_dark">
                <div class="post_thumb" data-image="<?php echo $eventinfo[0]['event_image'];?>" data-title="<?php echo $eventinfo[0]['event_name'];?>">
                    <img class="wp-post-image" width="1150" height="647" alt="<?php echo $eventinfo[0]['event_name'];?>" src="<?php echo $eventinfo[0]['event_image'];?>" itemprop="image">              
                </div>
                <div class="post_thumb_hover">
                    <div class="post_icon icon-book-2"></div>
                    <div class="post_categories">
                        <a class="courses_group_link" href="<?php echo URL;?>su-kien.html">
                            Sự kiện
                        </a>
                    </div>
                    <h1 itemprop="name" class="post_title entry-title"><?php echo $eventinfo[0]['event_name'];?></h1>
                    <div class="post_button" style="padding:0 50px;">
                        <?php echo $eventinfo[0]['event_short_description'];?>
                    </div>                  
                </div>
            </section>
            <div class="content_wrap">
                <div class="post_info" style="font-size: 1.3em;">
                    <span class="post_info_item post_info_posted">Ngày tổ chức: <?php echo date('d-m-Y',strtotime($eventinfo[0]['event_date']));?></span>
                    <span class="post_info_item post_info_time">Từ: <?php echo $eventinfo[0]['event_start'];?> - <?php echo $eventinfo[0]['event_end'];?></span>    
                     <span class="post_info_item post_info_time">Tại: <?php echo $eventinfo[0]['event_place'];?></span>               
                </div>
                <section class="post_content" itemprop="reviewBody" style="padding:0 10px 2.5em;">
                    <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper">
                            <h2>Nội dung sự kiện</h2>
                            <?php echo $eventinfo[0]['event_description'];?>
                            <center>
                                <a href="<?php echo $eventinfo[0]['event_link'];?>" class="sc_button sc_button_square sc_button_style_filled sc_button_bg_link sc_button_size_small" target="_blank">Đăng kí tham gia</a>
                            </center>
                        </div>
                    </div>
                </section>
            </div>
        </article>
        <article class="itemscope post_item post_item_single post_featured_default post_format_standard post-92 post type-post status-publish format-standard has-post-thumbnail hentry category-masonry-3-columns category-portfolio-3-columns tag-medical" itemscope itemtype="http://schema.org/Review"> 
            <section class="related_wrap scroll_wrap" style="background:#f4f7f9;">
                <h2 class="section_title">Các sự kiện khác</h2>
                <div class="sc_scroll_container sc_scroll_controls sc_scroll_controls_horizontal sc_scroll_controls_type_top">
                    <div class="sc_scroll sc_scroll_horizontal swiper-slider-container scroll-container" id="related_scroll">
                        <div class="sc_scroll_wrapper swiper-wrapper">
                            <?php 
                                if($this->listevent){
                            ?>
                            <div class="sc_scroll_slide swiper-slide">
                                <?php 
                                    foreach ($this->listevent as $value) {
                                        if($value['event_id'] != $eventinfo[0]['event_id']){
                                ?>
                                <article class="post_item post_item_related post_item_2">
                                    <div class="post_content">
                                        <div class="post_featured">
                                            <div class="post_thumb" data-image="<?php echo $value['question_catalog_img'];?>" data-title="<?php echo $value['question_catalog_name'];?>">
                                                <a class="hover_icon hover_icon_link" href="<?php echo URL.$value['event_url'];?>.html">
                                                    <img class="wp-post-image" width="400" height="225" alt="<?php echo $value['event_name'];?>" src="<?php echo $value['event_image'];?>">
                                                </a>                     
                                            </div>
                                        </div>
                
                                        <div class="post_content_wrap">
                                            <h4 class="post_title">
                                                <a class="hover_icon hover_icon_link" href="<?php echo URL.$value['event_url'];?>.html" title="<?php echo $value['event_name'];?>">
                                                   <?php echo $value['event_name'];?>            
                                                </a>
                                            </h4>
                                        </div>
                                    </div> 
                                </article>
                                <?php }} ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="sc_scroll_controls_wrap">
                            <a class="sc_scroll_prev" href="#"></a>
                            <a class="sc_scroll_next" href="#"></a>
                        </div>
                </div>
            </section>
        </article>
    </div>
</div>