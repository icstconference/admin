<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<?php 
    $eventinfo = $this->eventinfo;
    $eventdetailinfo = $this->eventdetailinfo;
?>
<div class="row">
    <div id="breadcrumb" class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo URL;?>admin">Quản trị</a></li>
            <li><a href="<?php echo URL;?>event/event/">Danh sách sự kiện</a></li>
            <li><a href="<?php echo URL;?>event/editevent/<?php echo $eventinfo[0]['event_id'];?>"><?php echo $eventinfo[0]['event_name'];?></a></li>
            <li><a href="<?php echo URL;?>event/editeventdetail/<?php echo $eventdetailinfo[0]['event_detail_id'];?>"><?php echo $eventdetailinfo[0]['event_detail_name'];?></a></li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <span>Form chỉnh sửa chi tiết sự kiện</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding"><br/>
                <div id="error"></div>
                <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tiêu đề chi tiết sự kiện</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo $eventdetailinfo[0]['event_detail_name'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="event_detail_name" name="event_detail_name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Thời gian tổ chức sự kiện</label>
                        <label class="col-sm-1 control-label" style="text-align:center;">Từ</label>
                        <div class="col-sm-2">
                            <input type="time" value="<?php echo date('h:i',strtotime($eventdetailinfo[0]['event_detail_start']));?>" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="event_detail_start" name="event_detail_start" required>
                        </div>
                        <label class="col-sm-1 control-label" style="text-align:center;">Đến</label>
                        <div class="col-sm-2">
                            <input type="time" value="<?php echo date('h:i',strtotime($eventdetailinfo[0]['event_detail_end']));?>" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="event_detail_end" name="event_detail_end" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nội dung chi tiết sự kiện</label>
                        <div class="col-sm-8">
                            <textarea placeholder="Nội dung chi tiết sự kiện" class="form-control"  rows="5" title="Tooltip for name" id="wysiwig_full1" name="event_detail_description">
                                <?php echo $eventdetailinfo[0]['event_detail_description'];?>
                            </textarea>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-success btn-label-left" name="register" id="submit1">
                                <span><i class="fa fa-clock-o"></i></span>
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>