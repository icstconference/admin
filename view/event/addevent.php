
<header class="page-header">
    <h2>Tạo 1 sự kiện mới</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="<?php echo URL;?>event/event/">
                    <span>Tất cả sự kiện</span>
                </a>
            </li>
            <li>
                <span>Tạo sự kiện</span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<?php                            
    $create = $_SESSION['user_permission_create'];
    $delete = $_SESSION['user_permission_delete'];
    $edit = $_SESSION['user_permission_edit'];
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Tạo sự kiện</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" role="form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tiêu đề sự kiện</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" placeholder="Tên sự kiện" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="event_name" id="wysiwig_full3"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hình ảnh đại diện</label>
                        <div class="col-sm-3">
                            <img src="#" id="eventimg" class="img-responsive imgupload" style="border-radius:3px;border:1px solid #ddd;display:none;margin-bottom:5px;" /> 
                            <input type="file" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="event_img" name="event_img" accept="image/jpg,image/png,image/jpeg,image/gif"  required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row" style="margin:0;">
                            <label class="col-sm-3 control-label">Bắt đầu</label>
                            <div class="col-sm-3">
                                <input type="date" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="event_date_start" name="event_date_start" value="<?php echo date('Y-m-d');?>" required>
                            </div>
                            <label class="col-sm-1 control-label">Kết thúc</label>
                            <div class="col-sm-3">
                                <input type="date" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="event_date_end" name="event_date_end" value="<?php echo date('Y-m-d');?>" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" style="display:none;">
                        <label class="col-sm-3 control-label">Link đăng kí</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="event_link" name="event_link" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Mô tả sự kiện</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" placeholder="Mô tả sự kiện" rows="5" title="Tooltip for name" name="event_short_description" id="wysiwig_full2" style="resize:none;height:100px;"></textarea>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label">Hình ảnh mô tả sự kiện</label>
                        <div class="col-sm-3">
                            <img src="#" id="eventimg1" class="img-responsive imgupload" style="border-radius:3px;border:1px solid #ddd;display:none;margin-bottom:5px;" /> 
                            <input type="file" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="event_img1" name="event_img1" accept="image/jpg,image/png,image/jpeg,image/gif"  required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Highlight sự kiện</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" placeholder="Mô tả sản phẩm" rows="5" title="Tooltip for name" id="wysiwig_full1" name="event_description"></textarea>
                        </div>
                    </div>
                    <div class="form-group" id="progressbar" style="display: none;">
                        <center>
                            <progress></progress>
                        </center>
                    </div>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-success btn-label-left" name="register" id="submit">
                                <span><i class="fa fa-clock-o"></i></span>
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
</section>