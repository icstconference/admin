<header class="page-header">
    <h2>Thêm organizer mới</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="<?php echo URL;?>organize/">
                    <span>Tất cả organizer</span>
                </a>
            </li>
            <li>
                <span>Thêm organizer mới</span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<?php                            
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Thêm organizer mới</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" method="post" role="form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tên nhóm organizer</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Tên bài viết" data-toggle="tooltip" data-placement="bottom" title="Nhập tiêu đề bài viết" id="organize_name" name="organize_name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Chọn sự kiện</label>
                        <div class="col-sm-8">
                            <select id="s2_with_tag" data-plugin-selectTwo class="form-control populate" name="event_id">
                                <?php
                                $listpermission = $this->listpermission;
                                
                                    if($this->listevent){
                                        foreach($this->listevent as $lt){
                                            ?>
                                            <option value="<?php echo $lt['event_id'];?>"><?php echo $lt['event_name'];?></option>
                                <?php }} ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" >Thành viên nhóm organizer</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" placeholder="Nội dung bài viết tiếng anh" rows="5" title="Tooltip for name" id="wysiwig_full1" name="organize_member"></textarea>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Nhập lại
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-success btn-label-left" name="register" id="submit">
                                <span><i class="fa fa-clock-o"></i></span>
                                Đồng ý
                            </button>
                        </div>
                    </div>
                </form>

    </div>
</section>
