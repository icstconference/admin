<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<header class="page-header">
    <h2>Tất cả organizer</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Tất cả organizer</span></li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<div class="row" style="margin: 0 0 10px;text-align:right;">
    <a href="<?php echo URL;?>organize/addorganize/" class="mb-xs mt-xs mr-xs btn btn-primary">
        <i class="fa fa-user-plus"></i>&nbsp;Thêm organizer
    </a>
</div>
<?php
    $listorganize = $this->listorganize;
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Tất cả organizer</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-default">
                    <thead>
                    <tr>
                        <th style="vertical-align:middle;">STT</th>
                        <th style="vertical-align:middle;">Organizer</th>
                        <th style="vertical-align:middle;">Sự kiện</th>
                        <th style="vertical-align:middle;">Ngày tạo</th>
                        <th style="vertical-align:middle;width:13%;">Tùy chỉnh</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($this->listorganize){
                        $i=0;
                        foreach($this->listorganize as $lp){
                            $i++;
                            ?>
                            <tr>
                                <td style="vertical-align:middle;text-align:center;"><?php echo $i;?></td>
                                <td style="vertical-align:middle;">
                                    <?php echo $lp['organize_name'];?>
                                </td>
                                <td style="vertical-align:middle;">
                                    <?php
                                       if($this->listevent){
                                            foreach($this->listevent as $lt){
                                                if($lp['event_id'] == $lt['event_id']){
                                    ?>
                                    <?php echo $lt['event_name'];?>
                                    <?php }}}?>
                                </td>
                                <td style="vertical-align:middle;">
                                    <?php echo $lp['organize_create_date'];?>
                                </td>
                                <td style="vertical-align:middle;">
                                    <a class="btn btn-primary" href="<?php echo URL;?>organize/editorganize/<?php echo $lp['organize_id'];?>"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-danger" href="<?php echo URL;?>organize/deletorganize/<?php echo $lp['organize_id'];?>"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        <?php }} ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>