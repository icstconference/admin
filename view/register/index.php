<div class="container-fluid">
    <div id="page-login" class="row">
        <div class="col-xs-12 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
            <div class="box">
                <div class="box-content">
                    <div class="text-center">
                        <img src="<?php echo $this->logo;?>" class="img-responsive" style="padding: 10px;border-bottom: 1px dashed #B6B6B6;"/>
                        <h3 class="page-header" style="padding: 10px;border-bottom:none;">Đăng kí ICST</h3>
                        <?php 
                            print_r($this->error);
                        ?>
                    </div>
                    <form method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="control-label">Tên đăng nhập</label>
                            <input type="text" class="form-control" name="username" placeholder="Tên đăng nhập ..." required/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Email ..." required/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Nhập lại email</label>
                            <input type="email" class="form-control" name="reemail" placeholder="Nhập lại email ..." required/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Password ..." required/>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Nhập lại Password</label>
                            <input type="password" class="form-control" name="repassword" placeholder="Nhập lại Password ..." required/>
                        </div>
                        <div class="row" style="margin:0px;">
                            <a href="<?php echo URL;?>login/" class="btn btn-primary" style="float:right;margin-left:5px;">Đăng nhập</a>
                            <input type="submit" class="btn btn-primary" name="register" value="Đăng kí" style="float:right;"/>
                            <a href="<?php echo URL;?>" class="btn btn-primary" style="float:left;"><i class="fa fa-chevron-circle-left"></i>&nbsp;Quay lại trang chủ</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>