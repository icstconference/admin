<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<div class="row">
    <div id="breadcrumb" class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo URL;?>admin">Quản trị</a></li>
            <li><a href="<?php echo URL;?>order">Danh sách đơn hàng</a></li>
        </ol>
    </div>
</div>
<?php
    $listorder = $this->listorder;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <i class="fa fa-gift"></i>
                    <span>Danh sách đơn hàng</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding">
                <table class="table table-bordered table-striped table-hover table-heading table-datatable" id="datatable-1">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Họ tên</th>
                        <th>Email</th>
                        <th>Ngày nhận đơn</th>
                        <th>Tình trạng</th>
                        <th>Chi tiết</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($this->listorder){
                        $i=0;
                        foreach($this->listorder as $lp){
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i;?></td>
                                <td>
                                    <?php echo $lp['order_name'];?>
                                </td>
                                <td>
                                    <?php echo $lp['order_email'];?>
                                </td>
                                <td>
                                    <?php echo date('d-m-Y h:i:s',strtotime($lp['order_create_date']));?>
                                </td>
                                <td>
                                    <?php if($lp['order_status'] == 1){?>
                                        <a href="<?php echo URL;?>order/check/<?php echo $lp['order_id'];?>" class="btn btn-danger">Chưa giao</a>
                                    <?php }else{?>
                                        <a href="<?php echo URL;?>order/check/<?php echo $lp['order_id'];?>" class="btn btn-success">Đã giao</a>
                                    <?php }?>
                                </td>
                                <td>
                                    <button id="detail" rel="<?php echo $lp['order_id'];?>" class="btn btn-warning" data-toggle="modal" data-target="#myModal">
                                        Chi tiết
                                    </button>
                                    <a href="<?php echo URL.'order/delete/'.$lp['order_id'];?>" class="btn btn-danger">
                                        Xóa
                                    </a>
                                </td>
                            </tr>
                        <?php }} ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="border:none;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel" style="text-align:center;">Chi tiết giỏ hàng</h4>
      </div>
      <div class="modal-body" id="cartdetail">
      </div>
      <div class="modal-footer" style="border:none;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>