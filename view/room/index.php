<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<header class="page-header">
    <h2>Quản lý phòng sự kiện</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <span>Quản lý phòng sự kiện</span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<?php     
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Thêm phòng mới</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
                <form class="form-horizontal" role="form" enctype="multipart/form-data" method="post">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tên phòng</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Tên phòng" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="room_name" name="room_name" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Chọn sự kiện</label>
                        <div class="col-sm-8">
                            <select id="s2_with_tag" data-plugin-selectTwo class="form-control populate" name="event_id">
                                <?php
                                $listpermission = $this->listpermission;
                                
                                    if($this->listevent){
                                        foreach($this->listevent as $lt){
                                            ?>
                                            <option value="<?php echo $lt['event_id'];?>"><?php echo $lt['event_name'];?></option>
                                <?php }} ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-success btn-label-left" name="register" id="submit">
                                <span><i class="fa fa-clock-o"></i></span>
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
</section>
<?php
    $listroom = $this->listroom;
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Danh sách phòng</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-default">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Tên phòng</th>
                        <th>Sự kiện</th>
                        <th>Tùy chỉnh</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($this->listroom){
                        $i=0;
                        foreach($this->listroom as $lp){
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i;?></td>
                                <td>
                                    <?php echo $lp['room_name'];?>
                                </td>
                                <td>
                                    <?php
                                       if($this->listevent){
                                            foreach($this->listevent as $lt){
                                                if($lp['event_id'] == $lt['event_id']){
                                    ?>
                                    <?php echo $lt['event_name'];?>
                                    <?php }}}?>
                                </td>
                                <td>
									<?php                                         
										if(strpos($edit[0]['permission_detail'],'shedule') || $_SESSION['user_type'] == 1){                                    
									?>
                                    <a class="btn btn-primary" href="<?php echo URL;?>calendar/editroom/<?php echo $lp['room_id'];?>">Edit</a>
                                    <?php } ?>                                    
									<?php                                         
										if(strpos($delete[0]['permission_detail'],'shedule') || $_SESSION['user_type'] == 1){                                    
									?>  
									<a class="btn btn-danger" href="<?php echo URL;?>calendar/deleteroom/<?php echo $lp['room_id'];?>">Delete</a>
									<?php } ?>
								</td>
                            </tr>
                        <?php }} ?>
                    </tbody>
                </table>
            </div>
</section>
