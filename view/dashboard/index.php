<?php
    if($this->listnutrition){
        $listnutrition = $this->listnutrition;
        $listnutritionname = '';
        $listnutritionvalue = '';
        $listnutritionid = '';
        foreach($listnutrition as $ls){
            $listnutritionname .= $ls['product_nutrition_name'].',';
            $listnutritionid .= $ls['product_nutrition_id'].',';
            $listnutritionvalue .= $ls['user_nutrition_quantity'].',';
        }

        $listnutritionname = rtrim($listnutritionname,',');
        $listnutritionvalue = rtrim($listnutritionvalue,',');
        $listnutritionid = rtrim($listnutritionid,',');
        $numnutrition = count($listnutrition);
    }

    if($this->listnutrition1){
        $listnutrition1 = $this->listnutrition1;
        $listnutritionname1 = '';
        $listnutritionvalue1 = '';
        $listnutritionid1 = '';
        foreach($listnutrition1 as $ls1){
            $listnutritionname1 .= $ls1['product_nutrition_name'].',';
            $listnutritionid1 .= $ls1['product_nutrition_id'].',';
            $listnutritionvalue1 .= $ls1['user_nutrition_quantity'].',';
        }

        $listnutritionname1 = rtrim($listnutritionname1,',');
        $listnutritionvalue1 = rtrim($listnutritionvalue1,',');
        $listnutritionid1 = rtrim($listnutritionid1,',');
        $numnutrition1 = count($listnutrition1);
    }

    if($this->listnutrition2){
        $listnutrition2 = $this->listnutrition2;
        $listnutritionvalue2 = '';
        $listdaynutrition = '';
        foreach($listnutrition2 as $ls){
            $listnutritionvalue2 .= $ls['user_nutrition_quantity'].',';
            $listdaynutrition .= date('d-m-Y',strtotime($ls['user_nutrition_detail_create_date'])).',';
        }

        $listnutritionvalue2 = rtrim($listnutritionvalue2,',');
        $listdaynutrition = rtrim($listdaynutrition,',');
        $numnutrition2 = count($listnutrition2);
    }

    if($this->listnutrition3){
        $listnutrition3 = $this->listnutrition3;
        $listnutritionvalue3 = '';
        $listdaynutrition1 = '';
        foreach($listnutrition3 as $ls){
            $listnutritionvalue3 .= $ls['user_nutrition_quantity'].',';
            $listdaynutrition1 .= date('d-m-Y',strtotime($ls['user_nutrition_detail_create_date'])).',';
        }

        $listnutritionvalue3 = rtrim($listnutritionvalue3,',');
        $listdaynutrition1 = rtrim($listdaynutrition1,',');
        $numnutrition3 = count($listnutrition3);
    }
?>
<span id="listnutritionname" rel="<?php echo $listnutritionname;?>" style="display: none;"></span>
<span id="listnutritionvalue" rel="<?php echo $listnutritionvalue;?>" style="display: none;"></span>
<span id="listnutritionid" rel="<?php echo $listnutritionid;?>" style="display: none;"></span>
<span id="numnutrition" rel="<?php echo $numnutrition;?>" style="display: none;"></span>
<span id="listnutritionname1" rel="<?php echo $listnutritionname1;?>" style="display: none;"></span>
<span id="listnutritionvalue1" rel="<?php echo $listnutritionvalue1;?>" style="display: none;"></span>
<span id="listnutritionid1" rel="<?php echo $listnutritionid1;?>" style="display: none;"></span>
<span id="numnutrition1" rel="<?php echo $numnutrition1;?>" style="display: none;"></span>
<span id="listnutritionvalue2" rel="<?php echo $listnutritionvalue2;?>" style="display: none;"></span>
<span id="numnutrition2" rel="<?php echo $numnutrition2;?>" style="display: none;"></span>
<span id="listdaynutrition" rel="<?php echo $listdaynutrition;?>" style="display: none;"></span>
<span id="listnutritionvalue3" rel="<?php echo $listnutritionvalue3;?>" style="display: none;"></span>
<span id="numnutrition3" rel="<?php echo $numnutrition3;?>" style="display: none;"></span>
<span id="listdaynutrition1" rel="<?php echo $listdaynutrition1;?>" style="display: none;"></span>
<div class="col-sm-12 col-xs-12 khungslide" style="padding: 0;">
    <div class="container">
        <div class="col-sm-6 col-xs-12" style="border: 2px solid #ddd;border-radius: 5px;max-width: 560px;margin-right: 10px;margin-bottom: 10px;">
            <div id="container1" style="height: 400px;"></div>
        </div>
        <div class="col-sm-6 col-xs-12" style="border: 2px solid #ddd;border-radius: 5px;max-width: 560px;margin-bottom: 10px;">
            <div id="container2" style="height: 400px;"></div>
        </div>
        <div class="col-sm-6 col-xs-12" style="border: 2px solid #ddd;border-radius: 5px;max-width: 560px;margin-right: 10px;">
            <div id="container3" style="height: 400px;"></div>
        </div>
        <div class="col-sm-6 col-xs-12" style="border: 2px solid #ddd;border-radius: 5px;max-width: 560px;">
            <div id="container4" style="height: 400px;"></div>
        </div>
    </div>
</div>