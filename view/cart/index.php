<div class="woocommerce">
    <?php
        if(count($_SESSION['cart']) > 0){
            $quantity = 0;
            $summoney = 0;
            $shipfee = 0;
            if(isset($_SESSION['cart']['don'])){
                foreach ($_SESSION['cart']['don'] as $value) {
                    $quantity += intval($value[0]['quantity']);
                }
            }

            if(isset($_SESSION['cart']['doi'])){
                foreach ($_SESSION['cart']['doi'] as $value) {
                    $quantity += intval($value[0]['quantity']);
                }
            }
                                
            if(isset($_SESSION['cart']['giadinh'])){
                foreach ($_SESSION['cart']['giadinh'] as $value) {
                    $quantity += intval($value[0]['quantity']);
                }
            }
         }
        if(!isset($_SESSION['cart']) || $quantity < 1){
    ?>
    <section class="cart-empty">
        <div class="container">       
            <div class="row">
                <div class="span6 offset3">
                        <div class="box">
                            <div class="hgroup title">
                                <h3>Giỏ hàng</h3>    
                            </div>
                            <div class="box-content">
                                <p class="cart-empty">Giỏ hàng của bạn trống. Bạn vui lòng quay lại để mua sắm thêm nhé.</p>
                            </div>
                            <div class="buttons">
                                <a class="button btn  btn-small" href="<?php echo URL;?>">
                                    Quay lại shop
                                </a>                              
                            </div>
                        </div>
                </div>
            </div>
        </div>	
    </section>
    <?php }else{ ?>
    <section class="cart">
        <div class="container">
            <div class="row">
                <div class="span9">
                    <div class="box">
                        <?php 
                            $i=0;
                            if(count($_SESSION['cart']) > 0){
                                $quantity = 0;
                                $summoney = 0;
                                $shipfee = 0;
                                if(isset($_SESSION['cart']['don'])){
                                    foreach ($_SESSION['cart']['don'] as $value) {
                                         $quantity += intval($value[0]['quantity']);
                                    }
                                }
                                if(isset($_SESSION['cart']['doi'])){
                                    foreach ($_SESSION['cart']['doi'] as $value) {
                                         $quantity += intval($value[0]['quantity']);
                                    }
                                }
                                if(isset($_SESSION['cart']['giadinh'])){
                                    foreach ($_SESSION['cart']['giadinh'] as $value) {
                                         $quantity += intval($value[0]['quantity']);
                                    }
                                }
                        ?>
                            <div class="box-header">
                                <h3>Giỏ hàng</h3>
                                <h5>Bạn hiện đang có <strong><?php echo $quantity;?></strong> sản phẩm trong giỏ</h5>
                            </div>
                            <div class="box-content">
                                <div class="cart-items">
                                    <table class="shop_table cart styled-table" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th class="product-thumbnail text-left">Sản phẩm</th>
                                                <th class="product-name text-left"></th>
                                                <th class="product-remove text-right">&nbsp;</th>
                                                <th class="product-price text-right">Loại</th>
                                                <th class="product-price text-right">Gía</th>
                                                <th class="product-quantity text-right">Số lượng</th>
                                                <th class="product-subtotal text-right">Tổng tiền</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                if(isset($_SESSION['cart']['don'])){
                                                foreach ($_SESSION['cart']['don'] as $value) {
                                                    $i++;
                                                    $quantity += intval($value[0]['quantity']);
                                                    $summoney += intval($value[0]['totalsum']);
                                                    $shipfee += intval($value[0]['shipfee']);
                                            ?>
                                            <tr class="cart_item">
                                                <td data-title="Thumbnail" class="text-left product-thumbnail">
                                                    <div class="image ">
                                                        <a href="<?php echo URL;?>product/detail/<?php echo $value[0]['product_url'];?>">
                                                            <img width="160" height="205" src="<?php echo $value[0]['product_image_thumb'];?>" class="attachment-shop_thumbnail wp-post-image" alt="">
                                                        </a>                                                                                                            
                                                    </div>
                                                </td>
                                                <td data-title="Product" class="product-name text-left">
                                                    <a href="<?php echo URL;?>product/detail/<?php echo $value[0]['product_url'];?>"><?php echo $value[0]['product_name'];?></a>
                                                    <dl class="variation">
                                                        <dt class="variation-Size">Giới tính: </dt>
                                                        <dd class="variation-Size"><p><?php echo $value[0]['sex'];?></p></dd>
                                                        <dt class="variation-Size">Kích thước: </dt>
                                                        <dd class="variation-Size"><p><?php echo $value[0]['size'];?></p></dd>
                                                    </dl>
                                                </td>
                                                <td data-title="Remove" class="product-remove text-right">                                                                                                                
                                                    <a class="remove" title="Remove this item" id="removecard" rel="<?php echo $value[0]['product_id'];?>" rel1="<?php echo $value[0]['key'];?>" style="cursor:pointer;">
                                                        <i class="fa fa-trash fa-lg"></i>
                                                    </a>                                                                                                        
                                                </td>
                                                <td data-title="Price" class="product-price text-right">
                                                    <span class="amount">Áo đơn</span>                                                                                                        
                                                </td>
                                                <td data-title="Price" class="product-price text-right">
                                                    <span class="amount"><?php echo number_format($value[0]['product_price_new']).' VNĐ';?></span>                                                                                                        
                                                </td>
                                                <td data-title="Quantity" class="product-quantity text-right">
                                                    <div class="quantity">
                                                        <input type="number" onchange="addquantitybyvalue('<?php echo $value[0]['product_id'];?>',this.value,'<?php echo $value[0]['key'];?>');" value="<?php echo $value[0]['quantity'];?>" title="Qty" class="input-text qty text" size="4">
                                                    </div>
                                                </td>
                                                <td data-title="Total" class="product-subtotal text-right">
                                                    <span class="amount"><?php echo number_format($value[0]['totalsum']).' VNĐ';?></span>                                                                                                        
                                                </td>
                                            </tr>
                                            <?php }} ?> 
                                            <?php 
                                                if(isset($_SESSION['cart']['doi'])){
                                                foreach ($_SESSION['cart']['doi'] as $value) {
                                                    $i++;
                                                    $quantity += intval($value[0]['quantity']);
                                                    $summoney += intval($value[0]['totalsum']);
                                                    $shipfee += intval($value[0]['shipfee']);
                                            ?>
                                            <tr class="cart_item">
                                                <td data-title="Thumbnail" class="text-left product-thumbnail">
                                                    <div class="image ">
                                                        <a href="<?php echo URL;?>product/detail/<?php echo $value[0]['product_url'];?>">
                                                            <img width="160" height="205" src="<?php echo $value[0]['product_image_thumb'];?>" class="attachment-shop_thumbnail wp-post-image" alt="">
                                                        </a>                                                                                                            
                                                    </div>
                                                </td>
                                                <td data-title="Product" class="product-name text-left">
                                                    <a href="<?php echo URL;?>product/detail/<?php echo $value[0]['product_url'];?>"><?php echo $value[0]['product_name'];?></a>
                                                    <dl class="variation">
                                                        <dt class="variation-Size">Giới tính: </dt>
                                                        <dd class="variation-Size"><p><?php echo $value[0]['sex'];?></p></dd>
                                                        <dt class="variation-Size">Kích thước: </dt>
                                                        <dd class="variation-Size"><p><?php echo $value[0]['size'];?></p></dd>
                                                    </dl>
                                                </td>
                                                <td data-title="Remove" class="product-remove text-right">                                                                                                                
                                                    <a class="remove" title="Remove this item" id="removecard" rel="<?php echo $value[0]['product_id'];?>" rel1="<?php echo $value[0]['key'];?>" style="cursor:pointer;">
                                                        <i class="fa fa-trash fa-lg"></i>
                                                    </a>                                                                                                        
                                                </td>
                                                <td data-title="Price" class="product-price text-right">
                                                    <span class="amount">Áo đôi</span>                                                                                                        
                                                </td>
                                                <td data-title="Price" class="product-price text-right">
                                                    <span class="amount"><?php echo number_format($value[0]['product_price_new']).' VNĐ';?></span>                                                                                                        
                                                </td>
                                                <td data-title="Quantity" class="product-quantity text-right">
                                                    <div class="quantity">
                                                        <input type="number" onchange="addquantitybyvalue('<?php echo $value[0]['product_id'];?>',this.value,'<?php echo $value[0]['key'];?>');" value="<?php echo $value[0]['quantity'];?>" title="Qty" class="input-text qty text" size="4">
                                                    </div>
                                                </td>
                                                <td data-title="Total" class="product-subtotal text-right">
                                                    <span class="amount"><?php echo number_format($value[0]['totalsum']).' VNĐ';?></span>                                                                                                        
                                                </td>
                                            </tr>
                                            <?php }} ?> 
                                            <?php 
                                                if(isset($_SESSION['cart']['giadinh'])){
                                                foreach ($_SESSION['cart']['giadinh'] as $value) {
                                                    $i++;
                                                    $quantity += intval($value[0]['quantity']);
                                                    $summoney += intval($value[0]['totalsum']);
                                                    $shipfee += intval($value[0]['shipfee']);
                                            ?>
                                            <tr class="cart_item">
                                                <td data-title="Thumbnail" class="text-left product-thumbnail">
                                                    <div class="image ">
                                                        <a href="<?php echo URL;?>product/detail/<?php echo $value[0]['product_url'];?>">
                                                            <img width="160" height="205" src="<?php echo $value[0]['product_image_thumb'];?>" class="attachment-shop_thumbnail wp-post-image" alt="">
                                                        </a>                                                                                                            
                                                    </div>
                                                </td>
                                                <td data-title="Product" class="product-name text-left">
                                                    <a href="<?php echo URL;?>product/detail/<?php echo $value[0]['product_url'];?>"><?php echo $value[0]['product_name'];?></a>
                                                    <dl class="variation">
                                                        <dt class="variation-Size">Giới tính: </dt>
                                                        <dd class="variation-Size"><p><?php echo $value[0]['sex'];?></p></dd>
                                                        <dt class="variation-Size">Kích thước: </dt>
                                                        <dd class="variation-Size"><p><?php echo $value[0]['size'];?></p></dd>
                                                    </dl>
                                                </td>
                                                <td data-title="Remove" class="product-remove text-right">                                                                                                                
                                                    <a class="remove" title="Remove this item" id="removecard" rel="<?php echo $value[0]['product_id'];?>" rel1="<?php echo $value[0]['key'];?>" style="cursor:pointer;">
                                                        <i class="fa fa-trash fa-lg"></i>
                                                    </a>                                                                                                        
                                                </td>
                                                <td data-title="Price" class="product-price text-right">
                                                    <span class="amount">Gia đình</span>                                                                                                        
                                                </td>
                                                <td data-title="Price" class="product-price text-right">
                                                    <span class="amount"><?php echo number_format($value[0]['product_price_new']).' VNĐ';?></span>                                                                                                        
                                                </td>
                                                <td data-title="Quantity" class="product-quantity text-right">
                                                    <div class="quantity">
                                                        <input type="number" onchange="addquantitybyvalue('<?php echo $value[0]['product_id'];?>',this.value,'<?php echo $value[0]['key'];?>');" value="<?php echo $value[0]['quantity'];?>" title="Qty" class="input-text qty text" size="4">
                                                    </div>
                                                </td>
                                                <td data-title="Total" class="product-subtotal text-right">
                                                    <span class="amount"><?php echo number_format($value[0]['totalsum']).' VNĐ';?></span>                                                                                                        
                                                </td>
                                            </tr>
                                            <?php }} ?>                                                   
                                        </tbody>
                                    </table>  
                                </div>
                            </div>
                            <div class="box-footer actions">
                                <div class="pull-left">
                                    <a href="<?php echo URL;?>" class="btn">Tiếp tục mua sắm</a>
                                </div>
                                <div class="pull-right wc-proceed-to-checkout">                          
                                    <a href="<?php echo URL;?>cart/checkout/" class="checkout-button button alt wc-forward btn btn-success">Tiếp tục thanh toán</a>
                                </div>
                            </div>
                        <?php } ?>     
                    </div>
                </div>
                <div class="span3">
                    <div class="cart-collaterals">
                        <div class="cart_totals ">
                            <div class="box">
                                <div class="hgroup title">
                                    <h3>Tổng thanh toán</h3>
                                </div>
                                <table cellspacing="0">
                                    <tbody>
                                        <tr class="cart-subtotal">
                                            <th>Tổng tiền giỏ hàng</th>
                                            <td><span class="amount"><?php echo number_format($summoney).' VNĐ';?></span></td>
                                        </tr>
                                        <tr class="shipping">
                                            <th>Phí ship</th>
                                            <td>
                                            <?php 
                                                if($summoney >= 600000){
                                            ?>   
                                                <span class="amount">Miễn phí</span>
                                            <?php } else {?>
                                                <span class="amount"><?php echo number_format($shipfee).' VNĐ';?></span>
                                            <?php } ?>
                                            </td>
                                        </tr>
                                        <tr class="order-total">
                                            <th>Tổng cộng</th>
                                            <td><strong><span class="amount"><?php echo number_format($shipfee+$summoney).' VNĐ';?></span></strong> </td>
                                        </tr>
                                    </tbody>
                                </table>   
                                <div class="wc-proceed-to-checkout">
                                    <a href="<?php echo URL;?>cart/checkout/" class="checkout-button button alt wc-forward">Tiếp tục thanh toán</a>
                                </div>
                            </div>
                        </div>                                                                        
                    </div>
                </div>
            </div>  
        </section>
    <?php } ?>
</div>