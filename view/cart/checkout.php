<?php 
    if(isset($_SESSION['cart'])){
?>
<section class="checkout">
    <div class="container">
        <form name="checkout" method="post" class="checkout woocommerce-checkout" enctype="multipart/form-data">
        	<div class="col2-set row" id="customer_details">
                <div class="col-1 span6">
                    <div class="box">
                        <div class="box-header">       
                            <h3>Thông tin thanh toán</h3>
                        </div>
                        <div class="box-content">
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="control-group">           
                                        <p class="form-row form-row-first validate-required" id="billing_first_name_field">
                                            <label for="billing_first_name" class="">Họ tên của bạn 
                                            <abbr class="required" title="required">*</abbr>
                                            </label>
                                            <input type="text" class="input-text " name="fullname" id="billing_first_name" value="">
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="control-group">            
                                        <p class="form-row form-row-wide address-field validate-required" id="billing_address_1_field">
                                            <label for="billing_address_1" class="">
                                                Địa chỉ <abbr class="required" title="required">*</abbr>
                                            </label>
                                            <input type="text" class="input-text " name="address" id="billing_address_1"  value="">
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="control-group">            
                                        <p class="form-row form-row-first validate-required validate-email" id="billing_email_field">
                                            <label for="billing_email" class="">Email
                                                <abbr class="required" title="required">*</abbr>
                                            </label>
                                            <input type="text" class="input-text " name="email" id="billing_email" placeholder="" value="">
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="control-group">            
                                        <p class="form-row form-row-last validate-required validate-phone" id="billing_phone_field">
                                            <label for="billing_phone" class="">Số điện thoại 
                                                <abbr class="required" title="required">*</abbr>
                                            </label>
                                            <input type="text" class="input-text " name="phone" id="billing_phone" placeholder="" value="">
                                        </p>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        		</div>

        		<div class="col-2 span6">
                    <div class="box">
                        <div class="box-header">
                            <h3>Thông tin thêm</h3>
                        </div>
                        <div class="box-content">
                            <div class="row-fluid">
                                <div class="control-group">                            
                                    <p class="form-row notes" id="order_comments_field">
                                        <textarea name="information" class="input-text " id="order_comments"  rows="2" cols="5"></textarea>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
        		</div>
        	</div>
            <div class="row">
                <div class="span12">
                    <div class="box">
                        <div class="box-header">
                            <h3>Giỏ hàng của bạn</h3>
                        </div>
                        <div class="box-content">
                            <div id="order_review" class="woocommerce-checkout-review-order">
                                <table class="shop_table woocommerce-checkout-review-order-table" style="position: relative; zoom: 1;">
                                	<thead>
                                		<tr>
                                			<th class="product-name">Sản phẩm</th>
                                            <th class="product-name">Loại áo</th>
                                			<th class="product-total">Tổng tiền</th>
                                		</tr>
                                	</thead>
        	                        <tbody>
                                        <?php 
                                                $quantity = 0;
                                                $summoney = 0;
                                                $shipfee = 0;
                                                if(isset($_SESSION['cart']['don'])){
                                                    foreach ($_SESSION['cart']['don'] as $value) {
                                                        $quantity += intval($value[0]['quantity']);
                                                        $summoney += intval($value[0]['totalsum']);
                                                        $shipfee += intval($value[0]['shipfee']);
                                                    }
                                                }
                                                if(isset($_SESSION['cart']['doi'])){
                                                    foreach ($_SESSION['cart']['doi'] as $value) {
                                                        $quantity += intval($value[0]['quantity']);
                                                        $summoney += intval($value[0]['totalsum']);
                                                        $shipfee += intval($value[0]['shipfee']);
                                                    }
                                                }
                                                if(isset($_SESSION['cart']['giadinh'])){
                                                    foreach ($_SESSION['cart']['giadinh'] as $value) {
                                                        $quantity += intval($value[0]['quantity']);
                                                        $summoney += intval($value[0]['totalsum']);
                                                        $shipfee += intval($value[0]['shipfee']);
                                                    }
                                                }
                                                if(isset($_SESSION['cart']['don'])){
                                                foreach ($_SESSION['cart']['don'] as $value) {
                                        ?>
        							    <tr class="cart_item">
        						            <td class="product-name"><?php echo $value[0]['product_name'];?>&nbsp;							 
                                                <strong class="product-quantity">× <?php echo $value[0]['quantity'];?></strong>							
                                                <dl class="variation">
        			                                <dt class="variation-Size">Giới tính: </dt>
                                                    <dd class="variation-Size"><p><?php echo $value[0]['sex'];?></p></dd>
                                                    <dt class="variation-Size">Kích thước: </dt>
                                                    <dd class="variation-Size"><p><?php echo $value[0]['size'];?></p></dd>
        	                                   </dl>
        						            </td>
                                            <td> Áo đơn </td>
        						            <td class="product-total">
        							            <span class="amount"><?php echo number_format($value[0]['product_price_new']).' VNĐ';?></span>						
                                            </td>
        					           </tr>
                                       <?php }} ?>
                                       <?php 
                                                if(isset($_SESSION['cart']['doi'])){
                                                foreach ($_SESSION['cart']['doi'] as $value) {
                                        ?>
                                        <tr class="cart_item">
                                            <td class="product-name"><?php echo $value[0]['product_name'];?>&nbsp;                           
                                                <strong class="product-quantity">× <?php echo $value[0]['quantity'];?></strong>                         
                                                <dl class="variation">
                                                    <dt class="variation-Size">Giới tính: </dt>
                                                    <dd class="variation-Size"><p><?php echo $value[0]['sex'];?></p></dd>
                                                    <dt class="variation-Size">Kích thước: </dt>
                                                    <dd class="variation-Size"><p><?php echo $value[0]['size'];?></p></dd>
                                               </dl>
                                            </td>
                                            <td> Áo đôi </td>
                                            <td class="product-total">
                                                <span class="amount"><?php echo number_format($value[0]['product_price_new']).' VNĐ';?></span>                      
                                            </td>
                                       </tr>
                                       <?php }} ?>
                                       <?php 
                                                if(isset($_SESSION['cart']['giadinh'])){
                                                foreach ($_SESSION['cart']['giadinh'] as $value) {
                                        ?>
                                        <tr class="cart_item">
                                            <td class="product-name"><?php echo $value[0]['product_name'];?>&nbsp;                           
                                                <strong class="product-quantity">× <?php echo $value[0]['quantity'];?></strong>                         
                                                <dl class="variation">
                                                    <dt class="variation-Size">Giới tính: </dt>
                                                    <dd class="variation-Size"><p><?php echo $value[0]['sex'];?></p></dd>
                                                    <dt class="variation-Size">Kích thước: </dt>
                                                    <dd class="variation-Size"><p><?php echo $value[0]['size'];?></p></dd>
                                               </dl>
                                            </td>
                                            <td> Set gia đình </td>
                                            <td class="product-total">
                                                <span class="amount"><?php echo number_format($value[0]['product_price_new']).' VNĐ';?></span>                      
                                            </td>
                                       </tr>
                                       <?php }} ?>
        						    </tbody>
        	                        <tfoot>
                                		<tr class="cart-subtotal">
                                			<th>Tổng tiền</th>
                                			<td><span class="amount"><?php echo number_format($summoney).' VNĐ';?></span></td>
                                		</tr>
        			                    <tr class="shipping">
        	                               <th>Phí ship</th>
        	                               <td>
        			                          <?php 
                                                if($summoney >= 600000){
                                              ?>   
                                                <span class="amount">Miễn phí</span>
                                              <?php } else {?>
                                                <span class="amount"><?php echo number_format($shipfee).' VNĐ';?></span>
                                              <?php } ?>
                                           </td>
                                        </tr>
                                		<tr class="order-total">
                                			<th>Tổng cộng</th>
                                			<td><strong>
                                                <?php 
                                                if($summoney >= 600000){
                                                ?>   
                                                    <span class="amount"><?php echo number_format($summoney).' VNĐ';?></span>
                                                    <input type="hidden" name="summoney" value="<?php echo $summoney;?>">
                                                <?php } else {?>
                                                    <span class="amount"><?php echo number_format($shipfee+$summoney).' VNĐ';?></span>
                                                    <input type="hidden" name="summoney" value="<?php echo $summoney+$shipfee;?>">
                                                <?php } ?>
                                                </strong> 
                                            </td>
                                		</tr>
        	                        </tfoot>
                                    <div class="blockUI" style="display:none"></div>
                                </table>
        	
                                <div id="payment" class="woocommerce-checkout-payment" style="position: relative; zoom: 1;">
        	                       <div class="form-row place-order">
        		                       <input type="hidden" id="_wpnonce" name="_wpnonce" value="c134aae319">
        		                       <input type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="Đăng ký thanh toán">
        	                       </div>
        	                       <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </form>
    </div>
</section>
<?php }?>