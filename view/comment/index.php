<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<header class="page-header">
    <h2>Danh sách bình luận</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <span>Danh sách bình luận</span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<?php
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];
    $listcomment = $this->listcomment;
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Danh sách bình luận</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-default">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Họ tên</th>
                        <th>Bài viết</th>
                        <th>Tình trạng</th>
                        <th>Ngày nhận</th>
                        <th>Chi tiết</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($this->listcomment){
                        $i=0;
                        foreach($this->listcomment as $lp){
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i;?></td>
                                <td>
                                    <?php echo $lp['comment_name'];?>
                                </td>
                                <td>
                                    <?php 
                                        if($lp['comment_type'] == 'news'){
                                            echo 'News: '.$lp['news_info']['news_name'];
                                        }else{
                                            echo 'Event: '.$lp['news_info']['event_name'];
                                        }
                                    ?>
                                </td>
                                <td>
                                    <?php if($lp['comment_read'] == 1){?>
                                        <a href="<?php echo URL.$this->lang;?>/comment/check/<?php echo $lp['comment_id'];?>" class="btn btn-danger">Đã duyệt</a>
                                    <?php }else{?>
                                        <a href="<?php echo URL.$this->lang;?>/comment/check/<?php echo $lp['comment_id'];?>" class="btn btn-success">Duyệt</a>
                                    <?php }?>
                                </td>
                                <td>
                                    <?php echo date('d-m-Y h:i:s',strtotime($lp['comment_created_date']));?>
                                </td>
                                <td>
                                    <button id="detail" rel="<?php echo $lp['comment_id'];?>" class="btn btn-warning" data-toggle="modal" data-target="#myModal">
                                        Chi tiết
                                    </button>
									<?php                                         
										if(strpos($delete[0]['permission_detail'],'comment') || $_SESSION['user_id'] == 1){                                    
									?>
                                    <a href="<?php echo URL.'comment/deletecomment/'.$lp['comment_id'];?>" class="btn btn-danger">
                                        Xóa
                                    </a>
									<?php } ?>
                                </td>
                            </tr>
                        <?php }} ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="border:none;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel" style="text-align:center;">Chi tiết liên hệ</h4>
      </div>
      <div class="modal-body" id="cartdetail">
      </div>
      <div class="modal-footer" style="border:none;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>