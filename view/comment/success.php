<?php 
    function cut_string($str, $len, $more) {
        if ($str == "" || $str == NULL)
            return $str;
        if (is_array($str))
            return $str;
        $str = trim($str);
        if (strlen($str) <= $len)
            return $str;
        $str = substr($str, 0, $len);
        if ($str != "") {
            if (!substr_count($str, " ")) {
                if ($more)
                    $str .= " ...";
                return $str;
            }
            while (strlen($str) && ($str[strlen($str) - 1] != " ")) {
                $str = substr($str, 0, -1);
            }
            $str = substr($str, 0, -1);
            if ($more)
                $str .= " ...";
        }
        return $str;
    }
?>
<style type="text/css">
    .indextitle1 {
        font-size: 50px;
        color: #fff;
        font-weight: 400;
        border-bottom: 3px solid #ff7733;
        padding-bottom: 0px;
        text-transform: uppercase;
    }
    .newstitle{
        font-size: 35px;
        color: #333;
        font-weight: 400;
        padding-bottom: 0px;
        text-transform: uppercase;
        text-align:center;
    }
    p{
        font-size: 16px;
        line-height: 24px;
        font-weight: 300;
    }
    .datetime{
        padding:15px 0;
        font-weight: 300;
        font-size:16px;
        color:#959595;
    }
    .view{
        padding:15px 0;
        font-weight: 300;
        font-size:16px;
        text-align:left;
        color:#959595;
    }
    .buttonsharefacebook{
        background: #ccc;
        padding: 14px 19px;
        border-radius: 50%;
        font-size: 16px;
        margin-right: 10px;
        color: #fff;
    }
    .buttonshare{
        background: #ccc;
        padding: 14px 17px;
        border-radius: 50%;
        font-size: 16px;
        margin-right: 10px;
        color: #fff;
    }
    .buttonshare:hover{
        color:#ff7733;
    }
    .buttonsharefacebook:hover{
        color:#ff7733;
    }
    .readmore{
        color: #fff;
        background: #ff7733;
        padding: 14px 30px;
        border-radius: 3px;
    }
    .readmore:hover{
        background: #333;
    }
    #sidebar h3{
        text-transform: none;
    }
    .linesidebar{
        width: 50px;
        border-bottom:2px solid #333;
        margin: 15px 0;
    }
    @media (min-width:1400px){
        #sidebar{
            padding:0;
        }
    }
    .google-maps {
        position: relative;
        height: 0;
    }
    .google-maps iframe {
        top: 0;
        left: 0;
        width: 100% !important;
        z-index:0;
    }
    .linkcategories{
        color: #333;
        font-size: 20px;
        padding: 5px 0;
        font-weight: 300;
    }
    .linkcategories:hover{
        color: #ff7733;
    }
    .linkcategories1{
        color: #fff;
        font-size: 22px;
        padding: 5px 0;
        font-weight: 300;
    }
    .linkcategories1:hover{
        color: #ff7733;
    }
</style>
<section id="page-top" style="background:url('<?php echo URL;?>public/img/header.png') center no-repeat;background-size:cover;height:370px;width:100%;">
    <div class="row" style="text-align:center;margin:0px;background:rgba(149,149,149,0.37);height:370px;width:100%;">
        <div class="col-md-12" style="padding-top:150px;">
            <div class="col-md-10 col-md-offset-1">
                <span class="indextitle1" >Comment</span>
            </div>
        </div>
    </div>
</section>
<section id="feature" class="feature-section" style="padding:25px 0 50px;">
    <div class="container" style="padding: 0px;">
        <div class="col-md-9 col-lg-9 col-xs-12" id="main" style="padding: 0 20px 0 0;">
            <div class="row" style="margin:0;padding:40px 0;text-align:center;font-size:16px;line-height:24px;">
                Bình luận của bạn đã gửi thành công, chờ admin duyệt.<br/>Vui lòng nhấn link bên dưới để quay lại trang bài viết<br/>
                <a href="<?php echo URL;?>news/detail/<?php echo $this->newsurl;?>"><i class="fa fa-home"></i>&nbsp;Trang bài viết</a>
            </div>
        </div>
        <div class="col-md-3 col-lg-3 col-xs-12" id="sidebar" style="padding: 0 0 0 20px;margin-top: 5px;">
            <div class="row" style="margin:0;padding-top: 20px;padding-bottom: 10px;">
                <h3>
                    Categories
                </h3>
                <div class="linesidebar"></div>
                <div class="col-md-12" style="padding:0px;">
                    <?php 
                        if($this->listnewscategories){
                            foreach($this->listnewscategories as $lp){
                    ?>
                    <div class="row" style="margin:0;border-bottom: 1px solid #eee;padding: 5px 0;">
                        <a href="<?php echo URL.$lp['news_categories_url'];?>" class="linkcategories">
                            <?php echo ucfirst($lp['news_categories_name']);?>
                        </a>
                    </div>
                    <?php }} ?>
                </div>
            </div>
            <div class="row" style="margin:0;padding-top: 20px;padding-bottom: 10px;">
                <h3>
                    Popular posts
                </h3>
                <div class="linesidebar"></div>
                <div class="col-md-12" style="padding:0px;">
                    <?php 
                        if($this->topnews){
                            foreach($this->topnews as $lp){
                    ?>
                    <div class="row" style="margin:0;border-bottom: 1px solid #eee;padding: 5px 0;">
                        <a href="<?php echo URL.$lp['news_url'];?>" class="linkcategories" style="font-size:18px;">
                            <?php echo $lp['news_name'];?>
                        </a>
                    </div>
                    <?php }} ?>
                </div>
            </div>
        </div>
    </div>
</section>
