<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<header class="page-header">
    <h2>Hình ảnh bài viết</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Danh sách liên hệ</span></li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<?php
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];
    $listcontact = $this->listcontact;
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Danh sách liên hệ</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-default">
            <thead>
                    <tr>
                        <th>STT</th>
                        <th>Họ tên</th>
                        <th>Email</th>
                        <th>Ngày nhận</th>
                        <th>Chi tiết</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($this->listcontact){
                        $i=0;
                        foreach($this->listcontact as $lp){
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i;?></td>
                                <td>
                                    <?php echo $lp['contact_user_name'];?>
                                </td>
                                <td>
                                    <?php echo $lp['contact_user_email'];?>
                                </td>
                                <td>
                                    <?php echo date('d-m-Y h:i:s',strtotime($lp['contact_user_createdate']));?>
                                </td>
                                <td>
                                    <button id="detail" rel="<?php echo $lp['contact_user_id'];?>" class="btn btn-warning" data-toggle="modal" data-target="#myModal">
                                        Chi tiết
                                    </button>
                                    <a href="<?php echo URL.'contact/delete/'.$lp['contact_user_id'];?>" class="btn btn-danger">
                                        Xóa
                                    </a>
                                </td>
                            </tr>
                        <?php }} ?>
                    </tbody>
                </table>
            </div>
</section>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="border:none;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel" style="text-align:center;">Chi tiết liên hệ</h4>
      </div>
      <div class="modal-body" id="cartdetail">
      </div>
      <div class="modal-footer" style="border:none;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>