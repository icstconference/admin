<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<div class="row">
    <div id="breadcrumb" class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo URL;?>admin">Quản trị</a></li>
            <li><a href="<?php echo URL;?>question">Danh sách câu hỏi</a></li>
        </ol>
    </div>
</div>
<?php
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];
    $listquestion = $this->listquestion;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <i class="fa fa-gift"></i>
                    <span>Danh sách câu hỏi</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding">
                <table class="table table-bordered table-striped table-hover table-heading table-datatable" id="datatable-1">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Họ tên</th>
                        <th>Câu hỏi</th>
                        <th>Tình trạng</th>
                        <th>Ngày nhận</th>
                        <th>Chi tiết</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($this->listquestion){
                        $i=0;
                        foreach($this->listquestion as $lp){
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i;?></td>
                                <td>
                                    <?php echo $lp['questionaire_name'];?>
                                </td>
                                <td>
                                    <?php echo $lp['questionaire_question'];?>
                                </td>
                                <td>
                                    <?php if($lp['questionaire_status'] == 'on'){?>
                                        <a href="<?php echo URL;?>question/check/<?php echo $lp['questionaire_id'];?>" class="btn btn-success">Đã trả lời</a>
                                    <?php }else{?>
                                        <a href="<?php echo URL;?>question/check/<?php echo $lp['questionaire_id'];?>" class="btn btn-danger">Chưa trả lời</a>
                                    <?php }?>
                                </td>
                                <td>
                                    <?php echo date('d-m-Y h:i:s',strtotime($lp['questionaire_create_date']));?>
                                </td>
                                <td>
                                    <a href="<?php echo URL.'question/editquestion/'.$lp['questionaire_id'];?>" class="btn btn-warning">
                                        Chi tiết
                                    </a>
									<?php                                         
										if(strpos($delete[0]['permission_detail'],'question') || $_SESSION['user_id'] == 1){                                    
									?> 
                                    <a href="<?php echo URL.'question/deletequestion/'.$lp['questionaire_id'];?>" class="btn btn-danger">
                                        Xóa
                                    </a>
									<?php } ?>
                                </td>
                            </tr>
                        <?php }} ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>