<?php 
    function cut_string($str, $len, $more) {
        if ($str == "" || $str == NULL)
            return $str;
        if (is_array($str))
            return $str;
        $str = trim($str);
        if (strlen($str) <= $len)
            return $str;
        $str = substr($str, 0, $len);
        if ($str != "") {
            if (!substr_count($str, " ")) {
                if ($more)
                    $str .= " ...";
                return $str;
            }
            while (strlen($str) && ($str[strlen($str) - 1] != " ")) {
                $str = substr($str, 0, -1);
            }
            $str = substr($str, 0, -1);
            if ($more)
                $str .= " ...";
        }
        return $str;
    }
?>
<style type="text/css">
    .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
        background: #215240;
        color:#fff !important;
        font-family: 'Open Sans', sans-serif;
    }
    .nav-tabs>li>a{
        border:1px solid #dfdfdf;
        border-radius: 8px 8px 0 0;
        margin-right: 0px;
        color: #333;
    }
    .tableft>li{
        list-style: none;
        height: 80px;
        border-bottom: 1px solid #dfdfdf;
        padding:10px;
    }
    .tableft a{
        text-decoration: none;
        color:#339966;
        font-family: 'Open Sans', sans-serif;
        font-size: 12px;
        font-weight: 600;
    }
    .tableft a:hover{
        text-decoration: none;
        color:#215240;
    }
    .panel{
        border-radius: 8px 8px 0 0;
    }
    .panel-default>.panel-heading{
        background: #45866c;
        color: #fff;
        border-radius: 8px 8px 0 0;
        text-transform: uppercase;
        font-weight: bold;
    }
    .panel-body a{
        text-decoration: none;
    }
    .panel-body a:hover{
        color: #339966;
    }
    .h3danhmuc{
        margin: 5px 0px;
        font-size: 15px;
        color: #747474;
        font-weight: 600;
        margin-left: 10px;
        line-height: 18px;
    }
    .h3danhmuc:hover{
        color: #339966;
    }
    .description{
        margin-left: 10px;
        margin-right: 10px;
        font-size: 12px;
        color: #333;
        margin-top: 5px;
        margin-bottom: 5px;
    }
    .datetime{
        margin-left: 10px;
        font-size: 12px;
        margin-top: 5px;
        margin-bottom: 5px;
        color: #747474;
    }
    .h3email{
        margin-top: 0px;
        font-size: 15px;
        font-weight: 800;
    }
    .inputemail{
        width: 75%;
        height: 36px;
        border-radius: 0px !important;
        padding: 8px !important;
    }
    .btnemail{
        text-align: center;
        margin-left: -5px;
        height: 36px;
        padding: 0 18px;
        background: #45866c;
        color: #fff;
        border:none;
    }
    .btnemail:hover{
        background: #215240;
    }
    .selectlink{
        height: 36px;
        width: 99%;
        padding: 5px;
    }
    .h3tuvan{
        font-size: 16px;
        font-weight: 600;
    }
    .fb-like-box, .fb-like-box span, .fb-like-box span iframe[style] { width: 100% !important; }
    .flex-direction-nav .flex-prev {
        position: absolute;
        right: 60px;
        top: -55px;
    }
    .flex-direction-nav .flex-next {
        right: 10px;
        top: -55px;
    }
    .khungsuccess{
        margin: 0;
        border: 1px solid #45866c;
        padding: 20px;
        text-align: center;
        border-radius: 5px;
    }
</style>

<div class="col-sm-8 col-xs-12" style="padding:0 20px;">
    <div class="row khungsuccess">
        Câu hỏi của bạn đã gửi thành công.<br/>Vui lòng nhấn link bên dưới để quay lại trang chủ<br/>
        <a href="<?php echo URL;?>"><i class="fa fa-home"></i>&nbsp;Trang chủ</a>
    </div>
</div>
<div class="col-sm-4 col-xs-12" id="sidebar" style="padding:0 20px 0 0;">
	<?php 
		if($this->listvideo){
			$i=0;
			$j=0;
			$k=0;
	?>
    <div class="row" style="margin:0px;">
        <div class="col-sm-12 col-xs-12" style="padding:0px;">
			<?php 
				foreach($this->listvideo as $vid){
					$i++;
					if($i == 1){
			?>
			<div class="videoWrapper">
				<iframe width="100%" height="220" src="<?php echo $vid['video_url']?>" frameborder="0" allowfullscreen></iframe>
			</div>
			<?php }} ?>
        </div>
        <div class="col-sm-12 col-xs-12" style="margin-top:10px;padding:0px;">
            <div class="col-sm-6" style="padding-left:0px;padding-right:5px;">
				<?php 
					foreach($this->listvideo as $vid1){
						$j++;
						if($j == 2){
				?>
                <div class="videoWrapper">
					<iframe width="100%" height="220" src="<?php echo $vid1['video_url']?>" frameborder="0" allowfullscreen></iframe>
				</div>
				<?php } }?>
            </div>
            <div class="col-sm-6" style="padding-right:0px;padding-left:5px;">
				<?php 
					foreach($this->listvideo as $vid2){
						$k++;
						if($k == 3){
				?>
                <div class="videoWrapper">
					<iframe width="100%" height="220" src="<?php echo $vid2['video_url']?>" frameborder="0" allowfullscreen></iframe>
				</div>
				<?php } } ?>
            </div>
        </div>
    </div>
	<?php } ?>
    <div class="row" style="margin:0px;margin-top:20px;">
        <div class="col-sm-12 col-xs-12" style="margin-top:5px;padding:0px;">
			<?php if($this->lang == 'vi'){?>
            <div class="col-sm-6" style="padding-left:0px;padding-right:5px;">
                <center>
					<a href="http://binhtun.xyz/vi/news/detail/gioi-thieu-ve-vien-38" title="Giới thiệu viện" target="_blank">
						<img src="<?php echo URL;?>public/img/gioithieu.png" class="img-responsive" />
					</a>
				</center>
            </div>
            <div class="col-sm-6" style="padding-right:0px;padding-left:5px;">
                <center>
                    <a href="http://binhtun.xyz/vi/news/detail/danh-sach-lab-39" title="Danh sách lab" target="_blank">
						<img src="<?php echo URL;?>public/img/danhsach.png" class="img-responsive" />
					</a>
				</center>
            </div>
			<?php } else { ?>
			<div class="col-sm-6" style="padding-left:0px;padding-right:5px;">
                <center>
					<a href="http://binhtun.xyz/en/news/detail/gioi-thieu-ve-vien-38" title="Giới thiệu viện" target="_blank">
						<img src="<?php echo URL;?>public/img/aboutus.png" class="img-responsive" />
					</a>
				</center>
            </div>
            <div class="col-sm-6" style="padding-right:0px;padding-left:5px;">
                <center>
                    <a href="http://binhtun.xyz/en/news/detail/danh-sach-lab-39" title="Danh sách lab" target="_blank">
						<img src="<?php echo URL;?>public/img/laboraties.png" class="img-responsive" />
					</a>
				</center>
            </div>
			<?php } ?>
        </div>
    </div>
    <div class="row" style="margin:0px;margin-top:20px;">
        <center>
            <form>
                <select class="selectlink" id="linklienket">
					<?php if($this->lang == 'vi'){?>
                    <OPTION>WEBSITE LIÊN KẾT VỚI VIỆN</OPTION>
					<?php }else{ ?>
					<OPTION>WEBSITE LINKED TO INSTITUTE</OPTION>
					<?php } ?>
                    <?php 
                        if($this->listlinker){
                            foreach ($this->listlinker as $value) {
                    ?>
                    <OPTION value="<?php echo $value['linker_url'];?>"><a href="<?php echo $value['linker_url'];?>"><?php echo $value['linker_name'];?></a></OPTION>
                    <?php } }?>
                </select>
            </form>
        </center>
    </div>
    <div class="row" style="margin:0px;margin-top:20px;">
            <?php 
                if($this->listpartner){
                    foreach ($this->listpartner as $value) {
            ?>
                <a href="<?php echo $value['partner_url'];?>" title="<?php echo $value['partner_name'];?>">
                    <img src="<?php echo $value['partner_image'];?>" class="img-responsive" style="height:62px;display:inline;margin:5px;"/>
                </a>
            <?php }} ?>
    </div>
    <div class="row" style="margin:0px;margin-top:20px;">
        <center>
            <div class="fb-page" data-href="https://www.facebook.com/Viện-Khoa-Học-và-Công-Nghệ-Tính-Toán-1379253225632992/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/Viện-Khoa-Học-và-Công-Nghệ-Tính-Toán-1379253225632992/">VIỆN KHOA HỌC VÀ CÔNG NGHỆ TÍNH TOÁN</a></blockquote></div></div>
        </center>
    </div>
</div>