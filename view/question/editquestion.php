<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<?php
    $questioninfo = $this->questioninfo;
?>
<span id="questionpage" rel="1"></span>
<div class="row">
    <div id="breadcrumb" class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo URL;?>admin">Quản trị</a></li>
            <li><a href="<?php echo URL;?>question">Danh sách câu hỏi</a></li>
            <li><a href="<?php echo URL;?>question/editquestion/<?php echo $questioninfo[0]['questionaire_id'];?>"><?php echo $questioninfo[0]['questionaire_question'];?></a></li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <span>Form chỉnh sửa câu hỏi</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding"><br/>
                <div id="error"></div>
                <form class="form-horizontal" method="post" role="form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tên người hỏi</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo $questioninfo[0]['questionaire_name'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="event_name" name="questionaire_name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Email người hỏi</label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" value="<?php echo $questioninfo[0]['questionaire_email'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="event_name" name="questionaire_email" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Số điện thoại người hỏi</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo $questioninfo[0]['questionaire_phone'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="event_name" name="questionaire_phone" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nội dung câu hỏi</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" rows="3" title="Tooltip for name" name="questionaire_question"><?php echo $questioninfo[0]['questionaire_question'];?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nội dung câu trả lời</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" rows="3" title="Tooltip for name" id="wysiwig_full" name="questionaire_answer"><?php echo $questioninfo[0]['questionaire_answer'];?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-offset-1 control-label">Trạng thái câu hỏi</label>
                        <div class="col-sm-2">
                            <div class="col-sm-2">
                                <?php
                                if($questioninfo[0]['questionaire_status'] == 'on'){
                                    ?>
                                    <div class="toggle-switch toggle-switch-success">
                                        <label>
                                            <input type="checkbox" name="questionaire_status" checked="">
                                            <div class="toggle-switch-inner"></div>
                                            <div class="toggle-switch-switch"><i class="fa fa-check"></i></div>
                                        </label>
                                    </div>
                                <?php }else{?>
                                    <div class="toggle-switch toggle-switch-success">
                                        <label>
                                            <input type="checkbox" name="questionaire_status" >
                                            <div class="toggle-switch-inner"></div>
                                            <div class="toggle-switch-switch"><i class="fa fa-check"></i></div>
                                        </label>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
					<?php                                         
						if(strpos($_SESSION['user_permission_edit'],'question') || $_SESSION['user_id'] == 1){                                    
					?>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-success btn-label-left" name="register" id="update">
                                <span><i class="fa fa-clock-o"></i></span>
                                Update
                            </button>
                        </div>
                    </div>
					<?php } ?>
                </form>
            </div>
        </div>
    </div>
</div>