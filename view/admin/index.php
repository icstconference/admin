<?php
    $numuser = $this->numuser;

    $numnews = $this->numnews;

    $numimage = $this->numimage;

    $numfile = $this->numfile;

    $numevent = $this->numevent;

    $numartist = $this->numartist;
 ?>
    <header class="page-header">
        <h2>Bảng điều khiển</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Bảng điều khiển</span></li>
            </ol>
                    
            <a class="sidebar-right-toggle" data-open="sidebar-right">
                <i class="fa fa-chevron-left"></i>
            </a>
        </div>
    </header>

    <div class="row">
        <div class="col-md-12 col-lg-12 col-xl-12">
            <header class="panel-heading panel-heading-transparent">
                <h4 class="title" style="font-size: 20px;font-weight: 600;">Chào mừng bạn đến với trang quản trị</h4>
            </header>
            <section class="panel">
                <div class="panel-body">
                    <div class="widget-summary">
                        <div class="widget-summary-col">
                            <div class="summary">
                                <h4 class="title">Dưới đây là các chức năng chính của website bạn có thể click vào để sử dụng:</h4>
                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                        <ul style="list-style-type: none;padding-left: 0px;">
                                            <li style="margin-top: 10px;">
                                                <a href="#" style="font-size: 16px;text-decoration: none;">
                                                    <i class="fa fa-user-plus"></i>&nbsp;Thêm người dùng
                                                </a>
                                            </li>
                                            <li style="margin-top: 10px;">
                                                <a href="#" style="font-size: 16px;text-decoration: none;">
                                                    <i class="fa fa-users"></i>&nbsp;Phân quyền người dùng
                                                </a>
                                            </li>
                                            <li style="margin-top: 10px;">
                                                <a href="#" style="font-size: 16px;text-decoration: none;">
                                                    <i class="fa fa-user"></i>&nbsp;&nbsp;Danh sách người dùng
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                        <ul style="list-style-type: none;padding-left: 0px;">
                                            <li style="margin-top: 10px;">
                                                <a href="#" style="font-size: 16px;text-decoration: none;">
                                                    <i class="fa fa-plus-square"></i>&nbsp;&nbsp;Viết bài mới
                                                </a>
                                            </li>
                                            <li style="margin-top: 10px;">
                                                <a href="#" style="font-size: 16px;text-decoration: none;">
                                                    <i class="fa fa-file"></i>&nbsp;&nbsp;Tất cả bài viết
                                                </a>
                                            </li>
                                            <li style="margin-top: 10px;">
                                                <a href="#" style="font-size: 16px;text-decoration: none;">
                                                    <i class="fa fa-folder"></i>&nbsp;Chuyên mục bài viết
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6 col-lg-4 col-xl-3">
                                        <ul style="list-style-type: none;padding-left: 0px;">
                                            <li style="margin-top: 10px;">
                                                <a href="#" style="font-size: 16px;text-decoration: none;">
                                                    <i class="fa fa-picture-o"></i>&nbsp;&nbsp;Galleries
                                                </a>
                                            </li>
                                            <li style="margin-top: 10px;">
                                                <a href="#" style="font-size: 16px;text-decoration: none;">
                                                    <i class="fa fa-file-text"></i>&nbsp;&nbsp;Tất cả tài nguyên
                                                </a>
                                            </li>
                                            <li style="margin-top: 10px;">
                                                <a href="#" style="font-size: 16px;text-decoration: none;">
                                                    <i class="fa fa-mail-forward"></i>&nbsp;&nbsp;Liên hệ
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12">
            <section class="panel">
                <header class="panel-heading panel-heading-transparent">
                    <h2 class="panel-title">
                        <i class="fa fa-tachometer"></i>&nbsp;Thống kê
                    </h2>
                </header>
            </section>
        </div>
        <div class="col-md-6 col-lg-12 col-xl-12">
            <div class="row">
                <div class="col-md-12 col-lg-6 col-xl-6">
                    <section class="panel panel-featured-left panel-featured-quartenary">
                        <div class="panel-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-quartenary">
                                        <i class="fa fa-user"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title">Thống kê người dùng</h4>
                                        <div class="info">
                                            <strong class="amount">
                                                <?php
                                                    echo $numuser[0]['numuser'];
                                                ?>
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="summary-footer">
                                        <a href="<?php echo URL;?>user" class="text-muted text-uppercase">Xem thêm</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-md-12 col-lg-6 col-xl-6">
                    <section class="panel panel-featured-left panel-featured-primary">
                        <div class="panel-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-primary">
                                        <i class="fa fa-life-ring"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title">Thống kê bài viết</h4>
                                        <div class="info">
                                            <strong class="amount">
                                                <?php
                                                    echo $numnews[0]['numnews'];
                                                ?>
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="summary-footer">
                                        <a href="<?php echo URL;?>news" class="text-muted text-uppercase">Xem thêm</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-md-12 col-lg-6 col-xl-6">
                    <section class="panel panel-featured-left panel-featured-warning">
                        <div class="panel-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-warning">
                                        <i class="fa fa-recycle"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title">Thống kê sự kiện</h4>
                                        <div class="info">
                                            <strong class="amount">
                                                <?php
                                                    echo $numevent[0]['numevent'];
                                                ?>
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="summary-footer">
                                        <a href="<?php echo URL;?>event/event/" class="text-muted text-uppercase">Xem thêm</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-md-12 col-lg-6 col-xl-6">
                    <section class="panel panel-featured-left panel-featured-gray">
                        <div class="panel-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-default">
                                        <i class="fa fa-user-secret"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title">Thống kê diễn giả</h4>
                                        <div class="info">
                                            <strong class="amount">
                                                <?php
                                                    echo $numartist[0]['numartist'];
                                                ?>
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="summary-footer">
                                        <a href="<?php echo URL;?>artist/artist/" class="text-muted text-uppercase">Xem thêm</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-12">
            <header class="panel-heading panel-heading-transparent">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                    <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                </div>

                <h2 class="panel-title"><i class="fa fa-bookmark"></i>&nbsp;Các bài viết mới nhất</h2>
            </header>           
            <section class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <?php 
                            if($this->listnews){
                                $i=0;
                        ?>
                        <table class="table table-striped mb-none">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tên bài viết</th>
                                    <th>Ngày viết</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    foreach ($this->listnews as $value) {
                                        $i++;
                                        if($i<=5){
                                ?>
                                <tr>
                                    <td><?php echo $i;?></td>
                                    <td>
                                        <a href="#">
                                            <?php echo $value['news_name'];?>
                                        </a>
                                    </td>
                                    <td><?php echo date('d/m/Y',strtotime($value['news_create_date']));?></td>
                                </tr>
                                <?php }} ?>
                            </tbody>
                        </table>
                        <?php } ?>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-lg-6 col-md-12">
            <header class="panel-heading panel-heading-transparent">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                    <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                </div>

                <h2 class="panel-title"><i class="fa fa-file"></i>&nbsp;Các tài nguyên mới nhất</h2>
            </header>
            <section class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <?php 
                            if($this->listfiles){
                                $i=0;
                        ?>
                        <table class="table table-striped mb-none">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tên file</th>
                                    <th>Ngày up</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    foreach ($this->listfiles as $value) {
                                        $i++;
                                        if($i<=5){
                                ?>
                                <tr>
                                    <td><?php echo $i;?></td>
                                    <td>
                                        <a href="#">
                                            <?php echo $value['file_name'];?>
                                        </a>
                                    </td>
                                    <td><?php echo date('d/m/Y',strtotime($value['file_create_date']));?></td>
                                </tr>
                                <?php }} ?>
                            </tbody>
                        </table>
                        <?php } ?>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-12">
            <header class="panel-heading panel-heading-transparent">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                    <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                </div>

                <h2 class="panel-title"><i class="fa fa-recycle"></i>&nbsp;Các sự kiện mới nhất</h2>
            </header>           
            <section class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <?php 
                            if($this->listevent){
                                $i=0;
                        ?>
                        <table class="table table-striped mb-none">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Sự kiện</th>
                                    <th>Ngày tạo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    foreach ($this->listevent as $value) {
                                        $i++;
                                        if($i<=5){
                                ?>
                                <tr>
                                    <td><?php echo $i;?></td>
                                    <td>
                                        <a href="#">
                                            <?php echo $value['event_name'];?>
                                        </a>
                                    </td>
                                    <td><?php echo date('d/m/Y',strtotime($value['event_create_date']));?></td>
                                </tr>
                                <?php }} ?>
                            </tbody>
                        </table>
                        <?php } ?>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-lg-6 col-md-12">
            <header class="panel-heading panel-heading-transparent">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                    <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                </div>

                <h2 class="panel-title"><i class="fa fa-user-secret"></i>&nbsp;Các artist mới nhất</h2>
            </header>           
            <section class="panel">
                <div class="panel-body">
                    <div class="table-responsive">
                        <?php 
                            if($this->listartist){
                                $i=0;
                        ?>
                        <table class="table table-striped mb-none">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Artist</th>
                                    <th>Ngày tạo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    foreach ($this->listartist as $value) {
                                        $i++;
                                        if($i<=5){
                                ?>
                                <tr>
                                    <td><?php echo $i;?></td>
                                    <td>
                                        <a href="#">
                                            <?php echo $value['artist_name'];?>
                                        </a>
                                    </td>
                                    <td><?php echo date('d/m/Y',strtotime($value['artist_create_date']));?></td>
                                </tr>
                                <?php }} ?>
                            </tbody>
                        </table>
                        <?php } ?>
                    </div>
                </div>
            </section>
        </div>
    </div>
</section>