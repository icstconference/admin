
<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<?php 
    $allsetting = $this->allsetting;
?>
<header class="page-header">
    <h2>Cấu hình website</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <span>Cấu hình website</span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Cấu hình website</h2>
    </header>
    <div class="panel-body">
        <div class="box-content" style="margin:10px 0;border:1px dashed #ddd;padding:10px;">
            Với email gửi về là gmail cần vào cấu hình các yếu tố sau:<br/>
            * <a href="https://www.google.com/settings/u/0/security/lesssecureapps">https://www.google.com/settings/u/0/security/lesssecureapps</a> - "Bật" chế độ ứng dụng kém bảo mật hơn<br/>
            * <a href="https://accounts.google.com/b/0/DisplayUnlockCaptcha">https://accounts.google.com/b/0/DisplayUnlockCaptcha</a> - Click "tiếp tục"<br/>
            * <a href="https://security.google.com/settings/security/activity?hl=en&pli=1">https://security.google.com/settings/security/activity?hl=en&pli=1</a> - Xem và cho phép truy cập trình duyệt
        </div>
        <div class="box-content">
            <form class="form-horizontal" role="form" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Tên website</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" value="<?php echo $allsetting[0]['config_title'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="website_title" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Mô tả website</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" value="<?php echo $allsetting[0]['config_description'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="website_description" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Keyword website</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" value="<?php echo $allsetting[0]['config_keyword'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="website_keyword" >
                     </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">URL website</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" value="<?php echo $allsetting[0]['config_siteurl'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="website_url" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Email website</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" value="<?php echo $allsetting[0]['config_email'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="website_email" >
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-sm-3 control-label">Chọn kiểu url website</label>
                    <div class="col-sm-5">
                        <select id="s2_with_tag" data-plugin-selectTwo class="form-control populate" name="config_url">
                            <?php 
                                if($allsetting[0]['config_url'] == 'dai'){
                            ?>
                                <option value="dai" selected>Url thân thiện dài</option>
                                <option value="ngan">Url thân thiện ngắn</option>
                            <?php }else{ ?>
                                <option value="ngan" selected>Url thân thiện ngắn</option>
                                <option value="dai">Url thân thiện dài</option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Logo website</label>
                    <div class="col-sm-8">
                        <?php
                            if(strlen($allsetting[0]['config_logo']) > 0){
                        ?>
                            <img src="<?php echo $allsetting[0]['config_logo'];?>" class="img-responsive" alt="avatar" id="bqimg">
                        <?php }else{ ?>
                            <img src="<?php echo URL;?>public/img/noneimage.jpg" class="img-responsive" alt="avatar" id="bqimg">
                        <?php } ?>
                        <input type="file" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="logo-img" accept="image/jpg,image/png,image/jpeg,image/gif"  >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Icon website</label>
                    <div class="col-sm-4">
                        <?php
                        if(strlen($allsetting[0]['config_logo']) > 0){
                            ?>
                            <img src="<?php echo $allsetting[0]['config_icon'];?>" class="img-rounded imgupload" alt="avatar" id="bqimg">
                        <?php }else{ ?>
                            <img src="<?php echo URL;?>public/img/noneimage.jpg" class="img-rounded imgupload" alt="avatar" id="bqimg">
                        <?php } ?>
                        <input type="file" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="icon-img" accept="image/jpg,image/png,image/jpeg,image/gif"  >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Link social</label>
                    <div class="col-sm-2">
                        <div class="input-group">
                            <input type="text" class="form-control" value="<?php echo $allsetting[0]['config_facebook'];?>" name="website_facebook">
                            <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="input-group">
                            <input type="text" class="form-control" value="<?php echo $allsetting[0]['config_google'];?>" name="website_google">
                            <span class="input-group-addon"><i class="fa fa-google-plus"></i></span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="input-group">
                            <input type="text" class="form-control" value="<?php echo $allsetting[0]['config_linkedin'];?>" name="website_linkedin">
                            <span class="input-group-addon"><i class="fa fa-linkedin"></i></span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="input-group">
                            <input type="text" class="form-control" value="<?php echo $allsetting[0]['config_twitter'];?>" name="website_twitter">
                            <span class="input-group-addon"><i class="fa fa-twitter"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-top: 40px;">
                    <div class="col-sm-offset-4 col-sm-2">
                        <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                            <span><i class="fa fa-clock-o txt-danger"></i></span>
                            Hủy bỏ
                        </button>
                    </div>
                    <div class="col-sm-2">
                        <button type="button" class="btn btn-success btn-label-left" name="submit" id="submit">
                            <span><i class="fa fa-clock-o"></i></span>
                            Cập nhật
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
