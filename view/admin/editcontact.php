<?php
    $create = $_SESSION['user_permission_create'];
    $delete = $_SESSION['user_permission_delete'];
    $edit = $_SESSION['user_permission_edit'];

    $contactinfo = $this->contactinfo;
?>
<header class="page-header">
    <h2>Liên hệ</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="<?php echo URL;?>admin/contact/">
                    <span>Liên hệ</span>
                </a>
            </li>
            <li>
                <?php echo $contactinfo[0]['contact_name'];?>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Chỉnh sửa thông tin liên hệ</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tên chi nhánh</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" value="<?php echo $contactinfo[0]['contact_name'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="brand_name">
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-sm-2 control-label">Tên chi nhánh tiếng anh</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" value="<?php echo $contactinfo[0]['contact_name_english'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="brand_name_english">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" value="<?php echo $contactinfo[0]['contact_email'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Số điện thoại</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" value="<?php echo $contactinfo[0]['contact_phone'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="phone">
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-sm-2 control-label">Số fax</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" value="<?php echo $contactinfo[0]['contact_fax'];?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="fax">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Địa chỉ</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="address"><?php echo $contactinfo[0]['contact_address'];?></textarea>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-sm-2 control-label">Địa chỉ tiếng anh</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="address_english"><?php echo $contactinfo[0]['contact_address_english'];?></textarea>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="cancel" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-success btn-label-left" name="submit">
                                <span><i class="fa fa-clock-o"></i></span>
                                Submit
                            </button>
                        </div>
                    </div>
        </form>
    </div>
</section>