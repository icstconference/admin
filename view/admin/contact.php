<header class="page-header">
    <h2>Liên hệ</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <span>Liên hệ</span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<?php     
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Thêm thông tin liên hệ</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label class="col-sm-2 control-label">Tên chi nhánh</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Tên chi nhánh" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="brand_name">
                </div>
            </div>
			<div class="form-group">
                <label class="col-sm-2 control-label">Tên chi nhánh tiếng anh</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Tên chi nhánh tiếng anh" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="brand_name_english">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Email" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="email">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Số điện thoại</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Số điện thoại" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="phone">
                </div>
            </div>
			<div class="form-group">
                <label class="col-sm-2 control-label">Số fax</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Số fax" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="fax">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Địa chỉ</label>
                <div class="col-sm-10">
                    <textarea class="form-control" placeholder="Địa chỉ" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="address"></textarea>
                </div>
            </div>
			<div class="form-group">
                <label class="col-sm-2 control-label">Địa chỉ tiếng anh</label>
                <div class="col-sm-10">
                    <textarea class="form-control" placeholder="Địa chỉ" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="address_english"></textarea>
                </div>
            </div>
            <div class="form-group" style="margin-top: 40px;">
                <div class="col-sm-offset-4 col-sm-2">
                <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                    <span><i class="fa fa-clock-o txt-danger"></i></span>
                        Nhập lại
                    </button>
                </div>
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-success btn-label-left" name="submit">
                        <span><i class="fa fa-clock-o"></i></span>
                        Đồng ý
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>
<?php
    $listcontact = $this->listcontact;
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Danh sách liên hệ</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none" id="datatable-default">
            <thead>
                <tr>
                    <th style="vertical-align:middle;text-align:center;">STT</th>
                    <th style="vertical-align:middle;">Chi nhánh</th>
                    <th style="vertical-align:middle;">Email</th>
                    <th style="vertical-align:middle;">Số điện thoại</th>
                    <th style="vertical-align:middle;">Địa chỉ</th>
                    <th style="vertical-align:middle;">Chỉnh sửa</th>
                </tr>
            </thead>
            <tbody>
            <?php
                if($this->listcontact){
                    $i=0;
                    foreach($this->listcontact as $lc){
                        $i++;
            ?>
                <tr>
                    <td style="vertical-align:middle;text-align:center;"><?php echo $i;?></td>
                    <td style="vertical-align:middle;"><?php echo $lc['contact_name'];?></td>
                    <td style="vertical-align:middle;"><?php echo $lc['contact_email'];?></td>
                    <td style="vertical-align:middle;width:13%;"><?php echo $lc['contact_phone'];?></td>
                    <td style="vertical-align:middle;"><?php echo $lc['contact_address'];?></td>
                    <td style="vertical-align:middle;width:11%;">
						<?php                                         
							if(strpos($edit[0]['permission_detail'],'contact') || $_SESSION['user_id'] == 1){                                    
						?>
                        <a class="btn btn-primary" href="<?php echo URL;?>admin/editcontact/<?php echo $lc['contact_id'];?>"><i class="fa fa-edit"></i></a>
                        <?php } ?>                                    
						<?php                                         
							if(strpos($delete[0]['permission_detail'],'contact') || $_SESSION['user_id'] == 1){                                    
						?>  
						<a class="btn btn-danger" href="<?php echo URL;?>admin/deletecontact/<?php echo $lc['contact_id'];?>"><i class="fa fa-trash"></i></a>
						<?php } ?> 
					</td>
                </tr>
            <?php }} ?>
            </tbody>
        </table>
    </div>
</section>
