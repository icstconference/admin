<style type="text/css">
    .indextitle1 {
        font-size: 50px;
        color: #fff;
        font-weight: 400;
        border-bottom: 3px solid #ff7733;
        padding-bottom: 0px;
        text-transform: uppercase;
    }
    .newstitle{
        font-size: 35px;
        color: #333;
        font-weight: 400;
        padding-bottom: 0px;
        text-transform: uppercase;
        text-align:center;
    }
    p{
        font-size: 16px;
        line-height: 24px;
        font-weight: 300;
    }
    .datetime{
        padding:15px 0;
        font-weight: 300;
        font-size:16px;
        color:#959595;
    }
    .view{
        padding:15px 0;
        font-weight: 300;
        font-size:16px;
        text-align:left;
        color:#959595;
    }
    .buttonsharefacebook{
        background: #ccc;
        padding: 14px 19px;
        border-radius: 50%;
        font-size: 16px;
        margin-right: 10px;
        color: #fff;
    }
    .buttonshare{
        background: #ccc;
        padding: 14px 17px;
        border-radius: 50%;
        font-size: 16px;
        margin-right: 10px;
        color: #fff;
    }
    .buttonshare:hover{
        color:#ff7733;
    }
    .buttonsharefacebook:hover{
        color:#ff7733;
    }
    .readmore{
        color: #fff;
        background: #ff7733;
        padding: 14px 30px;
        border-radius: 3px;
    }
    .readmore:hover{
        background: #333;
    }
    #sidebar h3{
        text-transform: none;
    }
    .linesidebar{
        width: 50px;
        border-bottom:2px solid #333;
        margin: 15px 0;
    }
    @media (min-width:1400px){
        #sidebar{
            padding:0;
        }
    }
    .linkcategories{
        color: #333;
        font-size: 20px;
        padding: 5px 0;
        font-weight: 300;
    }
    .linkcategories:hover{
        color: #ff7733;
    }
</style>
<?php 
    function cut_string($str, $len, $more) {
        if ($str == "" || $str == NULL)
            return $str;
        if (is_array($str))
            return $str;
        $str = trim($str);
        if (strlen($str) <= $len)
            return $str;
        $str = substr($str, 0, $len);
        if ($str != "") {
            if (!substr_count($str, " ")) {
                if ($more)
                    $str .= " ...";
                return $str;
            }
            while (strlen($str) && ($str[strlen($str) - 1] != " ")) {
                $str = substr($str, 0, -1);
            }
            $str = substr($str, 0, -1);
            if ($more)
                $str .= " ...";
        }
        return $str;
    }

    $companyinfo = $this->companyinfo;
?>
<section id="page-top" style="background:url('<?php echo URL;?>public/img/header.png') center no-repeat;background-size:cover;height:370px;width:100%;">
    <div class="row" style="text-align:center;margin:0px;background:rgba(149,149,149,0.37);height:370px;width:100%;">
        <div class="col-md-12" id="indexartist" style="padding-top:150px;">
            <?php 
                if($companyinfo){
            ?>
                <span class="indextitle1"><?php echo $companyinfo[0]['news_categories_name'];?></span>
            <?php }else{ ?>
                <span class="indextitle1">Company Snapshot</span>
            <?php } ?>
        </div>
    </div>
</section>
<section id="feature" class="feature-section" style="padding:25px 0 50px;">
    <div class="container" style="padding: 0px;">
        <div class="col-md-8 col-lg-8 col-xs-12" id="main" style="padding: 0px;">
            <?php 
                if($this->listnew){
                    foreach($this->listnew as $ln){
            ?>
            <div class="row" style="margin:0;padding:25px 0 20px;border-bottom:1px solid #ddd;">
                <div class="col-md-12 col-xs-12" style="padding:0;">
                    <center>
                        <a href="<?php echo URL.$ln['news_url'];?>" title="<?php echo $ln['news_name'];?>" alt="<?php echo $ln['news_name'];?>">
                            <img src="<?php echo URL;?>public/img/company.png" class="img-responsive" tilte="<?php echo $ln['news_name'];?>" />
                        </a>
                    </center>
                    <a href="<?php echo URL.$ln['news_url'];?>" title="<?php echo $ln['news_name'];?>" alt="<?php echo $ln['news_name'];?>">
                        <h3>
                            <?php echo $ln['news_name'];?>
                        </h3>
                    </a>
                    <div id="companydescription" style="height:73px;font-size:16px;line-height:24px;">
                        <?php echo cut_string(strip_tags($ln['news_description']),230,30);?>
                    </div>
                </div>
                <div class="col-md-12 col-xs-12" style="padding: 10px 0 5px;">
                    <div class="view col-md-3 col-xs-6">
                        <i class="fa fa-clock-o"></i>&nbsp;<?php echo date('d F Y',strtotime($ln['news_create_date']));?>
                    </div>
                    <div class="datetime col-xs-6" id="commentmobile" style="text-align:right;display:none;">
                            <i class="fa fa-comments" ></i>&nbsp;
                            <?php 
                                    if($ln['comment'] <= 1){
                                        echo $ln['comment'].' comment';
                                    }else{
                                        echo $ln['comment'].' comments';
                                    }
                                ?>
                        </div>
                    <div class="datetime col-md-9 col-xs-12">
                        <div class="col-md-4 col-xs-6" id="commentdesktop" style="text-align:right;">
                            <i class="fa fa-comments" ></i>&nbsp;
                            <?php 
                                    if($ln['comment'] <= 1){
                                        echo $ln['comment'].' comment';
                                    }else{
                                        echo $ln['comment'].' comments';
                                    }
                                ?>
                        </div>
                        <div class="col-md-4 col-xs-6" style="text-align:right;">
                            <a href="#" class="buttonsharefacebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="buttonshare"><i class="fa fa-twitter"></i></a>
                        </div>
                        <div class="col-md-4 col-xs-6" style="text-align:right;">
                              <a href="<?php echo URL.$ln['news_url'];?>" title="<?php echo $ln['news_name'];?>" alt="<?php echo $ln['news_name'];?>" class="readmore">Read more</a>
                        </div>
                    </div>
                </div>
            </div>
            <?php }}else{ ?>
            <div class="row" style="margin:0;padding:25px 0 20px;">
                There is no post in this category.
            </div>
            <?php } ?>
        </div>
        <div class="col-md-3 col-md-offset-1 col-lg-3 col-lg-offset-1 col-xs-12" id="sidebar">
            <div class="row" style="margin:0;padding-top: 20px;padding-bottom: 10px;">
                <h3>
                    Categories
                </h3>
                <div class="linesidebar"></div>
                <div class="col-md-12" style="padding:0px;">
                    <?php 
                        if($this->listnewscategories){
                            foreach($this->listnewscategories as $lp){
                    ?>
                    <div class="row" style="margin:0;border-bottom: 1px solid #eee;padding: 5px 0;">
                        <a href="<?php echo URL.'company/'.$lp['news_categories_url'];?>" class="linkcategories">
                            <?php echo ucfirst($lp['news_categories_name']);?>
                        </a>
                    </div>
                    <?php }} ?>
                </div>
            </div>
            <div class="row" style="margin:0;padding-top: 20px;padding-bottom: 10px;">
                <h3>
                    Popular posts
                </h3>
                <div class="linesidebar"></div>
                <div class="col-md-12" style="padding:0px;">
                    <?php 
                        if($this->topnews){
                            foreach($this->topnews as $lp){
                    ?>
                    <div class="row" style="margin:0;border-bottom: 1px solid #eee;padding: 5px 0;">
                        <a href="<?php echo URL.$lp['news_url'];?>" class="linkcategories" style="font-size:18px;">
                            <?php echo $lp['news_name'];?>
                        </a>
                    </div>
                    <?php }} ?>
                </div>
            </div>
        </div>
    </div>
</section>