<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<div class="row">
    <div id="breadcrumb" class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo URL;?>admin">Quản trị</a></li>
            <li><a href="<?php echo URL;?>linker">Danh sách link liên kết</a></li>
        </ol>
    </div>
</div>
<?php     
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];
	if(strpos($create[0]['permission_detail'],'linker') || $_SESSION['user_id'] == 1){
?>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <span>Form thêm liên kết</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding"><br/>
                <div id="error"></div>
                <form class="form-horizontal" role="form" enctype="multipart/form-data" method="post">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tên liên kết</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Tên liên kết" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="linker_name" name="linker_name" />
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-sm-3 control-label">Tên liên kết tiếng anh</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Tên liên kết tiếng anh" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="linker_name_english" name="linker_name_english" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Link liên kết</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Link liên kết" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="linker_link" name="linker_link" />
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-sm-3 control-label">Thứ tự liên kết</label>
                        <div class="col-sm-1">
                            <input type="number" class="form-control" value="1" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="linker_sort" name="linker_sort" required>
						</div>
					</div>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-success btn-label-left" name="register" id="submit">
                                <span><i class="fa fa-clock-o"></i></span>
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<?php
    $listlinker = $this->listlinker;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <i class="fa fa-gift"></i>
                    <span>Danh sách link liên kết</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding">
                <table class="table table-bordered table-striped table-hover table-heading table-datatable" id="datatable-1">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Tên liên kết</th>
                        <th>Link liên kết</th>
						<th>Thứ tự liên kết</th>
                        <th>Tùy chỉnh</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($this->listlinker){
                        $i=0;
                        foreach($this->listlinker as $lp){
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i;?></td>
                                <td>
                                    <?php echo $lp['linker_name'];?>
                                </td>
                                <td>
                                    <?php echo $lp['linker_url'];?>
                                </td>
								<td>
                                    <?php echo $lp['linker_sort'];?>
                                </td>
                                <td>
									<?php                                         
										if(strpos($edit[0]['permission_detail'],'linker') || $_SESSION['user_id'] == 1){                                    
									?>
                                    <a class="btn btn-primary" href="<?php echo URL;?>linker/editlinker/<?php echo $lp['linker_id'];?>">Edit</a>
                                    <?php } ?>                                    
									<?php                                         
										if(strpos($delete[0]['permission_detail'],'linker') || $_SESSION['user_id'] == 1){                                    
									?>  
									<a class="btn btn-danger" href="<?php echo URL;?>linker/deletelinker/<?php echo $lp['linker_id'];?>">Delete</a>
									<?php } ?>
								</td>
                            </tr>
                        <?php }} ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
