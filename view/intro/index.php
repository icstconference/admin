<style type="text/css">
    .rtf ol li, .rtf ul li {
        margin-left: 5px;
    }
     #main-contents p {
        font-size: 16px;
        line-height: 23px;
        margin: 0 0 0 5px;
    }
    .celebrity_or_video .celebrity_or_video_text_area .rtf, .celebrity_or_video h1, .celebrity_or_video h2, .celebrity_or_video h3, .celebrity_or_video h4, .celebrity_or_video p {
        color:#63544d;
    }
</style>
<?php 
    $listpage = $this->listpage;
    $pageinfo = $this->pageinfo;
?>
<div style="margin-top: 0px;background: #f7f7f7;" id="main" role="main">
    <div id="main-contents" class="single-col homepage-wrap" style="margin-top: 160px;">
        <ul class="breadcrumb" style="margin-left: 0;">
            <li style="display:inline;">
                <a href="<?php echo URL;?>">
                    Trang chủ
                </a> > 
            </li>
            <li style="display:inline;">
                <span><?php echo $pageinfo[0]['page_name'];?></span>
            </li>
        </ul>
        <div class="homepage-module" style="border-top:none;padding-top: 0px;">
            <div style="padding-top: 0px; margin-top: 0px; padding-bottom: 0px; margin-bottom: 0px;" class="celebrity_or_video">
                <div class="celebrity_or_video_content_area" style="width: 75%;margin-left:0px;">
                    <div style="margin-top: 15px;color: #63544d;font-weight: 300;">
                        <?php 
                            if($this->lang == 'vi'){
                        ?>
                        <h3 style="font-size: 20px;color: #63544d;font-weight: 500;">
                            <?php echo $pageinfo[0]['page_name'];?>
                        </h3>
                        <?php }else{ ?>
                        <h3 style="font-size: 20px;color: #63544d;font-weight: 500;">
                            <?php echo $pageinfo[0]['page_name_english'];?>
                        </h3>
                        <?php } ?>
                        <?php 
                            if($this->lang == 'vi'){
                        ?>
                            <?php echo $pageinfo[0]['page_description'];?>
                        <?php }else{ ?>
                            <?php echo $pageinfo[0]['page_description_english'];?>
                        <?php } ?>
                    </div>
                </div>

                <div class="celebrity_or_video_text_area" style="width: 24%;">
                    <div class="rtf">
                        <ul style="list-style-type: none;line-height: 25px;">
                            <?php 
                                if($this->listpage){
                                    foreach ($this->listpage as $value) {
                                        if($pageinfo[0]['page_id'] == $value['page_id']){
                            ?>
                            <li>
                                <a href="<?php echo URL;?>page/detail/<?php echo $value['page_url'];?>/" style="color: #63544d;font-size: 16px;text-decoration:none;font-weight: 500;border-bottom: 2px solid #63544d;"><?php echo $value['page_name'];?></a>
                            </li>
                            <?php }else{ ?>
                            <li>
                                <a href="<?php echo URL;?>page/detail/<?php echo $value['page_url'];?>/" style="color: #63544d;font-size: 16px;text-decoration:none;"><?php echo $value['page_name'];?></a>
                            </li>
                            <?php }}} ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>