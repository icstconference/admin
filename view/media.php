<style>
	html.fixed .content-with-menu .inner-toolbar, html.fixed.inner-menu-opened .content-with-menu .inner-toolbar{
		left:0px !important;
	}
	html.fixed .inner-body{
		margin-left:0px;
	}
</style>
<header class="page-header">
    <h2>Hình ảnh bài viết</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Hình ảnh bài viết</span></li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<section class="panel" style="margin-top:30px;">
    <header class="panel-heading">
        <h2 class="panel-title">Thêm hình mới</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="dropzone dz-square dz-clickable" id="dropzone-example1">
            <input type="hidden" value="1" name="library"/>

            <div class="dz-default dz-message">
                
            </div>
            
            <div class="form-group" id="progressbar" style="display: none;">
                <center>
                    <progress></progress>
                </center>
            </div>
        </form>
        <div class="row dropzone dz-square dz-clickable" style="margin:10px 0;min-height:300px;background:rgba(0,0,0,0.03);" id="zoneimg">
            
        </div>
        <div class="content-with-menu-container">
                <div class="inner-body mg-main">
                    <div class="inner-toolbar clearfix" style="text-align: right;">
                        <ul>
                            <li>
                                <a href="#" id="mgSelectAll">
                                    <i class="fa fa-check-square"></i> 
                                    <span data-all-text="Select All" data-none-text="Select None">Chọn tất cả </span>
                                </a>
                            </li>
                            <li>
                                <a id="deletemedia" style="cursor: pointer;">
                                    <i class="fa fa-trash-o"></i> Xóa
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="row mg-files" data-sort-destination data-sort-id="media-gallery">
                        <form role="form" enctype="multipart/form-data">
                        <?php 
                            if($this->listimagejpg){
                                $i=0;
                                foreach ($this->listimagejpg as $value) {
                                    $i++;
                        ?>
                        <div class="isotope-item document col-sm-6 col-md-4 col-lg-3">
                            <div class="thumbnail">
                                <div class="thumb-preview">
                                    <a class="thumb-image" href="<?php echo URL.$value;?>">
                                        <img src="<?php echo URL.$value;?>" class="img-responsive" style="height: 150px;width:auto;">
                                    </a>
                                    <div class="mg-thumb-options">
                                        <div class="mg-zoom"></div>
                                        <div class="mg-toolbar">
                                            <div class="mg-option checkbox-custom checkbox-inline">
                                                <input type="checkbox" name="file1[]" id="file_<?php echo $i;?>" value="<?php echo URL.$value;?>">
                                                <label for="file_<?php echo $i;?>">Chọn</label>
                                            </div>
                                            <div class="mg-group pull-right">
                                                <ul class="dropdown-menu mg-menu" role="menu">
                                                    <li><a href="<?php echo URL.$value;?>" download><i class="fa fa-download"></i> Tải xuống</a></li>
                                                    <li><a id="deletemedia" style="cursor: pointer;"><i class="fa fa-trash-o"></i> Xóa</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } } ?>
                        <?php 
                            if($this->listimagepng){
                                $i=0;
                                foreach ($this->listimagepng as $value) {
                                    $i++;
                        ?>
                        <div class="isotope-item document col-sm-6 col-md-4 col-lg-3">
                            <div class="thumbnail">
                                <div class="thumb-preview">
                                    <a class="thumb-image" href="<?php echo URL.$value;?>">
                                        <img src="<?php echo URL.$value;?>" class="img-responsive" style="height: 150px;width:auto;">
                                    </a>
                                    <div class="mg-thumb-options">
                                        <div class="mg-zoom"></div>
                                        <div class="mg-toolbar">
                                            <div class="mg-option checkbox-custom checkbox-inline">
                                                    <input type="checkbox" name="file1[]" id="file1_<?php echo $i;?>" value="<?php echo URL.$value;?>">
                                                    <label for="file1_<?php echo $i;?>">Chọn</label>
                                                
                                            </div>
                                            <div class="mg-group pull-right">
                                                <ul class="dropdown-menu mg-menu" role="menu">
                                                    <li><a href="<?php echo URL.$value;?>" download><i class="fa fa-download"></i> Tải xuống</a></li>
                                                    <li><a href="#"><i class="fa fa-trash-o"></i> Xóa</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } } ?>
                        </form>
                        <div class="form-group" id="progressbar" style="display: none;">
                            <center>
                                <progress></progress>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</section>