<style type="text/css">
    .indextitle1 {
        font-size: 50px;
        color: #fff;
        font-weight: 400;
        border-bottom: 3px solid #ff7733;
        padding-bottom: 0px;
        text-transform: uppercase;
    }
    .newstitle{
        font-size: 35px;
        color: #333;
        font-weight: 400;
        padding-bottom: 0px;
        text-transform: uppercase;
        text-align:center;
    }
    p{
        font-size: 16px;
        line-height: 24px;
        font-weight: 300;
    }
</style>
<section id="page-top" style="background:url('<?php echo URL;?>public/img/header.png') center no-repeat;background-size:cover;height:370px;width:100%;">
    <div class="row" style="text-align:center;margin:0px;background:rgba(149,149,149,0.37);height:370px;width:100%;">
        <div class="col-md-12" style="padding-top:150px;">
            <span class="indextitle1">ABOUT US</span>
        </div>
    </div>
</section>
<section id="feature" class="feature-section" style="padding:70px 0;">
    <div class="container" style="padding:0;">
        <div class="row" style="margin:0;">
            <div class="newstitle">Welcome TO CONNECTED</div>
            <br/><br/>
            <center>
                <img src="<?php echo URL;?>public/img/dj1.png" class="img-responsive" />
            </center>
            <br/><br/>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer adipiscing erat eget risus sollicitudin pellentesque et non erat. Maecenas nibh dolor, malesuada et bibendum a, sagittis accumsan ipsum. Pellentesque ultrices ultrices sapien, nec tincidunt nunc posuere ut. Cras dui nulla, tincidunt et hendrerit vitae, aliquet non urna.
                <br/><br/>
                Vestibulum et erat metus, in rhoncus ante. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In sed turpis egestas risus rutrum aliquet at eget nisi. Quisque mauris mi, venenatis eget rutrum sit amet, pulvinar vel est. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur tortor urna, tempus ac convallis ac, lobortis eget ligula.
                <br/><br/>
                Vestibulum et erat metus, in rhoncus ante. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In sed turpis egestas risus rutrum aliquet at eget nisi. Quisque mauris mi, venenatis eget rutrum sit amet, pulvinar vel est. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur tortor urna, tempus ac convallis ac, lobortis eget ligula.
            </p>
            <br/><br/>
            <div class="newstitle">WE DO</div>
            <br/><br/>
            <center>
                <img src="<?php echo URL;?>public/img/dj2.png" class="img-responsive" />
            </center>
            <br/><br/>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer adipiscing erat eget risus sollicitudin pellentesque et non erat. Maecenas nibh dolor, malesuada et bibendum a, sagittis accumsan ipsum. Pellentesque ultrices ultrices sapien, nec tincidunt nunc posuere ut. Cras dui nulla, tincidunt et hendrerit vitae, aliquet non urna.
                <br/><br/>
                Vestibulum et erat metus, in rhoncus ante. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In sed turpis egestas risus rutrum aliquet at eget nisi. Quisque mauris mi, venenatis eget rutrum sit amet, pulvinar vel est. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur tortor urna, tempus ac convallis ac, lobortis eget ligula.
                <br/><br/>
                Vestibulum et erat metus, in rhoncus ante. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In sed turpis egestas risus rutrum aliquet at eget nisi. Quisque mauris mi, venenatis eget rutrum sit amet, pulvinar vel est. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur tortor urna, tempus ac convallis ac, lobortis eget ligula.
            </p>
        </div>
    </div><!-- /.container -->
</section>    