<?php 
	$actual_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER[REQUEST_URI];
	
    function cut_string($str, $len, $more) {
        if ($str == "" || $str == NULL)
            return $str;
        if (is_array($str))
            return $str;
        $str = trim($str);
        if (strlen($str) <= $len)
            return $str;
        $str = substr($str, 0, $len);
        if ($str != "") {
            if (!substr_count($str, " ")) {
                if ($more)
                    $str .= " ...";
                return $str;
            }
            while (strlen($str) && ($str[strlen($str) - 1] != " ")) {
                $str = substr($str, 0, -1);
            }
            $str = substr($str, 0, -1);
            if ($more)
                $str .= " ...";
        }
        return $str;
    }
?>
<?php 
    $filecategoriseinfo = $this->filecategoriseinfo;
    $configurl = $this->configurl;
?>
<style type="text/css">
    .rtf ol li, .rtf ul li {
        margin-left: 5px;
    }
    .filetitle{
        float:left;color: #63544d;
        font-weight:400;
        clear:both;
        font-size: 14px;
    }
    .filedate{
        float: left;
        margin-left: 10px;
        font-size: 14px;
    }
    .filedownload{
        float: right;
        font-size: 14px;
		width:90px;
    }
    .white{
        background: #fff;
        vertical-align: middle;
        height: 45px;
    }
    .gray{
        vertical-align: middle;
        height: 45px;
    }
</style>
<div style="margin-top: 0px;background: #f7f7f7;" id="main" role="main">
    <div id="main-contents" class="single-col homepage-wrap" style="margin-top: 160px;">
        <div class="homepage-module">
            <div style="padding-top: 0px; margin-top: 0px; padding-bottom: 0px; margin-bottom: 0px;" class="celebrity_or_video">
                <div class="celebrity_or_video_content_area" style="width: 70%;margin-left:0px;">
                    <?php 
                        if($filecategoriseinfo[0]['child']){
                            $child = $filecategoriseinfo[0]['child'];
                            $i=0;
                    ?>
                    <div id="tabs" style="background: transparent;border: none;">
                        <ul style="background: transparent;border: none;padding-left: 0px;margin-bottom: 5px;">
                            <?php 
                                foreach ($child as$value) {
                                    $i++;
                                    if($i <= 7){
                            ?>
                                <li>
                                    <a href="#tabs-<?php echo $i;?>">
                                        <?php 
                                            if($this->lang == 'vi'){
                                        ?>
                                            <?php echo $value['file_categories_name'];?>
                                        <?php }else{ ?>
                                            <?php echo $value['file_categories_name_english'];?>
                                        <?php } ?>
                                    </a>
                                </li>
                            <?php }} ?>
                        </ul>
                        <?php
                            foreach ($child as $value) {
                                $j++;
                                if($j <= 7){
                        ?>
                        <div id="tabs-<?php echo $j;?>" style="padding-left: 0px;">
                            <h3 style="color: #337BA7;font-size: 24px;text-transform:uppercase;font-weight:500;">
                                <?php 
                                    if($this->lang == 'vi'){
                                ?>
                                    Báo cáo tài chính
                                <?php }else{ ?>
                                    FINANCIAL REPORTS
                                <?php } ?>
                            </h3>
                            <table>
                                <thead></thead>
                                <tbody>
                                <?php 
                                    if($value['childfile']){
                                        $i=0;
                                        foreach ($value['childfile'] as $value1) {
                                            $i++;
                                            if($i % 2 == 0){
                                ?>
                                    <tr class="gray">
                                        <td style="vertical-align: middle;padding: 10px;">
                                            <div class="filetitle">
                                                <i class="fa fa-file"></i>&nbsp;
                                                <?php 
                                                    if($this->lang == 'vi'){
                                                ?>
                                                    <?php echo $value1['file_name'];?>
                                                <?php }else{ ?>
                                                    <?php echo $value1['file_name_english'];?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                        <td style="vertical-align: middle;padding: 10px;width:20%;">
                                            <div class="filedate">
                                                <i class="fa fa-calendar"></i>&nbsp;<?php echo date('d-m-Y',strtotime($value1['file_create_date']));?>
                                            </div>
                                        </td>
                                        <td style="vertical-align: middle;padding: 10px;width:20%;">    
                                            <div class="filedownload">
                                                <?php 
                                                    if(strlen($value1['file_url']) >1){
                                                ?>
                                                <i class="fa fa-download"></i>&nbsp;
                                                <a href="<?php echo URL.$value1['file_url'];?>" style="color:#337ab7;" download>
                                                    <?php 
                                                        if($this->lang == 'vi'){
                                                    ?>
                                                        Tải về
                                                    <?php }else{ ?>
                                                        Download
                                                    <?php } ?>
                                                </a>
                                                <?php } ?>
                                                <?php 
                                                    if(strlen($value1['file_link']) >1){
                                                ?>
                                                <i class="fa fa-eye"></i>&nbsp;
                                                <a href="<?php echo $value1['file_link'];?>" style="color:#337ab7;">
                                                    <?php 
                                                        if($this->lang == 'vi'){
                                                    ?>
                                                        Xem
                                                    <?php }else{ ?>
                                                        View
                                                    <?php } ?>
                                                </a>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                <?php }else{ ?> 
                                    <tr class="white">
                                        <td style="vertical-align: middle;padding: 10px;">
                                            <div class="filetitle">
                                                <i class="fa fa-file"></i>&nbsp;
                                                <?php 
                                                    if($this->lang == 'vi'){
                                                ?>
                                                    <?php echo $value1['file_name'];?>
                                                <?php }else{ ?>
                                                    <?php echo $value1['file_name_english'];?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                        <td style="vertical-align: middle;padding: 10px;width:20%;">
                                            <div class="filedate">
                                                <i class="fa fa-calendar"></i>&nbsp;<?php echo date('d-m-Y',strtotime($value1['file_create_date']));?>
                                            </div>
                                        </td>
                                        <td style="vertical-align: middle;padding: 10px;width:20%;">    
                                            <div class="filedownload">
                                                <?php 
                                                    if(strlen($value1['file_url']) >1){
                                                ?>
                                                <i class="fa fa-download"></i>&nbsp;
                                                <a href="<?php echo URL.$value1['file_url'];?>" style="color:#337ab7;" download>
                                                    <?php 
                                                        if($this->lang == 'vi'){
                                                    ?>
                                                        Tải về
                                                    <?php }else{ ?>
                                                        Download
                                                    <?php } ?>
                                                </a>
                                                <?php } ?>
                                                <?php 
                                                    if(strlen($value1['file_link']) >1){
                                                ?>
                                                <i class="fa fa-eye"></i>&nbsp;
                                                <a href="<?php echo $value1['file_link'];?>" style="color:#337ab7;">
                                                    <?php 
                                                        if($this->lang == 'vi'){
                                                    ?>
                                                        Xem
                                                    <?php }else{ ?>
                                                        View
                                                    <?php } ?>
                                                </a>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                <?php } }} ?>
                                </tbody>
                            </table>
                            <?php 
                                if($value['child']){ 
                                    $child1 = $value['child'];
                                    foreach ($child1 as $value1) {
                                        if($value1['child']){
                                            $child2 = $value1['child'];
                            ?>
                                <h3 style="color: #337BA7;font-size: 24px;text-transform:uppercase;font-weight:500;">
                                        <?php 
                                            if($this->lang == 'vi'){
                                        ?>
                                            <?php echo $value1['file_categories_name'];?>
                                        <?php }else{ ?>
                                            <?php echo $value1['file_categories_name_english'];?>
                                        <?php } ?>
                                </h3>
                                <?php 
                                    foreach ($child2 as $value2) {
                                ?>
                                    <h3 style="color: #63544d;font-size: 16px;text-transform:uppercase;"><i class="fa fa-hand-o-right"></i>&nbsp;
                                        <?php 
                                            if($this->lang == 'vi'){
                                        ?>
                                            <?php echo $value2['file_categories_name'];?>
                                        <?php }else{ ?>
                                            <?php echo $value2['file_categories_name_english'];?>
                                        <?php } ?>
                                    </h3>
                                        <table>
                                            <thead></thead>
                                            <tbody>
                                                <?php 
                                                    if($value2['childfile']){
                                                        $i=0;
                                                        foreach ($value2['childfile'] as $value3) {
                                                            $i++;
                                                            if($i % 2 == 0){
                                                ?>
                                                <tr class="gray">
                                                    <td style="vertical-align: middle;padding: 10px;">
                                                        <div class="filetitle">
                                                            <i class="fa fa-file"></i>&nbsp;
                                                            <?php 
                                                                if($this->lang == 'vi'){
                                                            ?>
                                                                <?php echo $value3['file_name'];?>
                                                            <?php }else{ ?>
                                                                <?php echo $value3['file_name_english'];?>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                    <td style="vertical-align: middle;padding: 10px;width:20%;">
                                                        <div class="filedate">
                                                            <i class="fa fa-calendar"></i>&nbsp;<?php echo date('d-m-Y',strtotime($value3['file_create_date']));?>
                                                        </div>
                                                    </td>
                                                    <td style="vertical-align: middle;padding: 10px;width:20%;">    
                                                        <div class="filedownload">
                                                            <?php 
                                                                if(strlen($value3['file_url']) >1){
                                                            ?>
                                                            <i class="fa fa-download"></i>&nbsp;
                                                            <a href="<?php echo URL.$value3['file_url'];?>" style="color:#337ab7;" download>
                                                                <?php 
                                                                    if($this->lang == 'vi'){
                                                                ?>
                                                                    Tải về
                                                                <?php }else{ ?>
                                                                    Download
                                                                <?php } ?>
                                                            </a>
                                                            <?php } ?>
                                                            <?php 
                                                                if(strlen($value3['file_link']) >1){
                                                            ?>
                                                            <i class="fa fa-eye"></i>&nbsp;
                                                            <a href="<?php echo $value3['file_link'];?>" style="color:#337ab7;">
                                                                <?php 
                                                                    if($this->lang == 'vi'){
                                                                ?>
                                                                    Xem
                                                                <?php }else{ ?>
                                                                    View
                                                                <?php } ?>
                                                            </a>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php }else{ ?> 
                                                <tr class="white">
                                                    <td style="vertical-align: middle;padding: 10px;">
                                                        <div class="filetitle">
                                                            <i class="fa fa-file"></i>&nbsp;
                                                            <?php 
                                                                if($this->lang == 'vi'){
                                                            ?>
                                                                <?php echo $value3['file_name'];?>
                                                            <?php }else{ ?>
                                                                <?php echo $value3['file_name_english'];?>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                    <td style="vertical-align: middle;padding: 10px;width:20%;">
                                                        <div class="filedate">
                                                            <i class="fa fa-calendar"></i>&nbsp;<?php echo date('d-m-Y',strtotime($value3['file_create_date']));?>
                                                        </div>
                                                    </td>
                                                    <td style="vertical-align: middle;padding: 10px;width:20%;">    
                                                        <div class="filedownload">
                                                            <?php 
                                                                if(strlen($value3['file_url']) >1){
                                                            ?>
                                                            <i class="fa fa-download"></i>&nbsp;
                                                            <a href="<?php echo URL.$value3['file_url'];?>" style="color:#337ab7;" download>
                                                                <?php 
                                                                    if($this->lang == 'vi'){
                                                                ?>
                                                                    Tải về
                                                                <?php }else{ ?>
                                                                    Download
                                                                <?php } ?>
                                                            </a>
                                                            <?php } ?>
                                                            <?php 
                                                                if(strlen($value3['file_link']) >1){
                                                            ?>
                                                            <i class="fa fa-eye"></i>&nbsp;
                                                            <a href="<?php echo $value3['file_link'];?>" style="color:#337ab7;">
                                                                <?php 
                                                                    if($this->lang == 'vi'){
                                                                ?>
                                                                    Xem
                                                                <?php }else{ ?>
                                                                    View
                                                                <?php } ?>
                                                            </a>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php } }} ?>
                                            </tbody>
                                        </table>
                                    <?php } ?>
                            <?php }else{
                            ?>
                                <h3 style="color: #337BA7;font-size: 24px;text-transform:uppercase;font-weight:500;"><?php echo $value1['file_categories_name'];?></h3>
                                <table>
                                    <thead></thead>
                                    <tbody>
                                        <?php 
                                            if($value1['childfile']){
                                                $i=0;
                                                foreach ($value1['childfile'] as $value2) {
                                                    $i++;
                                                    if($i % 2 == 0){
                                        ?>
                                        <tr class="gray">
                                                    <td style="vertical-align: middle;padding: 10px;">
                                                        <div class="filetitle">
                                                            <i class="fa fa-file"></i>&nbsp;
                                                            <?php 
                                                                if($this->lang == 'vi'){
                                                            ?>
                                                                <?php echo $value2['file_name'];?>
                                                            <?php }else{ ?>
                                                                <?php echo $value2['file_name_english'];?>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                    <td style="vertical-align: middle;padding: 10px;width:20%;">
                                                        <div class="filedate">
                                                            <i class="fa fa-calendar"></i>&nbsp;<?php echo date('d-m-Y',strtotime($value2['file_create_date']));?>
                                                        </div>
                                                    </td>
                                                    <td style="vertical-align: middle;padding: 10px;width:20%;">    
                                                        <div class="filedownload">
                                                            <?php 
                                                                if(strlen($value2['file_url']) >1){
                                                            ?>
                                                            <i class="fa fa-download"></i>&nbsp;
                                                            <a href="<?php echo URL.$value2['file_url'];?>" style="color:#337ab7;" download>
                                                                <?php 
                                                                    if($this->lang == 'vi'){
                                                                ?>
                                                                    Tải về
                                                                <?php }else{ ?>
                                                                    Download
                                                                <?php } ?>
                                                            </a>
                                                            <?php } ?>
                                                            <?php 
                                                                if(strlen($value2['file_link']) >1){
                                                            ?>
                                                            <i class="fa fa-eye"></i>&nbsp;
                                                            <a href="<?php echo $value2['file_link'];?>" style="color:#337ab7;">
                                                                <?php 
                                                                    if($this->lang == 'vi'){
                                                                ?>
                                                                    Xem
                                                                <?php }else{ ?>
                                                                    View
                                                                <?php } ?>
                                                            </a>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                        <?php }else{ ?> 
                                        <tr class="white">
                                            <td style="vertical-align: middle;padding: 10px;">
                                                <div class="filetitle">
                                                    <i class="fa fa-file"></i>&nbsp;
                                                    <?php 
                                                        if($this->lang == 'vi'){
                                                    ?>
                                                        <?php echo $value2['file_name'];?>
                                                    <?php }else{ ?>
                                                        <?php echo $value2['file_name_english'];?>
                                                    <?php } ?>
                                                </div>
                                            </td>
                                            <td style="vertical-align: middle;padding: 10px;width:20%;">
                                                <div class="filedate">
                                                    <i class="fa fa-calendar"></i>&nbsp;<?php echo date('d-m-Y',strtotime($value2['file_create_date']));?>
                                                </div>
                                            </td>
                                            <td style="vertical-align: middle;padding: 10px;width:20%;">    
                                                <div class="filedownload">
                                                            <?php 
                                                                if(strlen($value2['file_url']) >1){
                                                            ?>
                                                            <i class="fa fa-download"></i>&nbsp;
                                                            <a href="<?php echo URL.$value2['file_url'];?>" style="color:#337ab7;" download>
                                                                <?php 
                                                                    if($this->lang == 'vi'){
                                                                ?>
                                                                    Tải về
                                                                <?php }else{ ?>
                                                                    Download
                                                                <?php } ?>
                                                            </a>
                                                            <?php } ?>
                                                            <?php 
                                                                if(strlen($value2['file_link']) >1){
                                                            ?>
                                                            <i class="fa fa-eye"></i>&nbsp;
                                                            <a href="<?php echo $value2['file_link'];?>" style="color:#337ab7;">
                                                                <?php 
                                                                    if($this->lang == 'vi'){
                                                                ?>
                                                                    Xem
                                                                <?php }else{ ?>
                                                                    View
                                                                <?php } ?>
                                                            </a>
                                                            <?php } ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php } }} ?>
                                    </tbody>
                                </table>
                            <?php }}} ?>
                        </div>
                        <?php }} ?>
                    </div>
                    <?php }else{ ?>
                    <div style="margin-top: 15px;margin-left: 5px;color: #63544d;font-weight: 400;">
                        <table>
                            <thead></thead>
                            <tbody>
                                <?php 
                                    if($this->listfile){
                                        $i=0;
                                        foreach ($this->listfile as $value) {
                                            $i++;
                                            if($i % 2 == 0){
                                ?>
                                <tr class="gray">
                                                    <td style="vertical-align: middle;padding: 10px;width:73%;">
                                                        <div class="filetitle">
                                                            <i class="fa fa-file"></i>&nbsp;
                                                            <?php 
                                                                if($this->lang == 'vi'){
                                                            ?>
                                                                <?php echo $value['file_name'];?>
                                                            <?php }else{ ?>
                                                                <?php echo $value['file_name_english'];?>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                    <td style="vertical-align: middle;padding: 10px;width:20%;">
                                                        <div class="filedate">
                                                            <i class="fa fa-calendar"></i>&nbsp;<?php echo date('d-m-Y',strtotime($value['file_create_date']));?>
                                                        </div>
                                                    </td>
                                                    <td style="vertical-align: middle;padding: 10px;width:20%;">    
                                                        <div class="filedownload">
                                                            <?php 
                                                                if(strlen($value['file_url']) >1){
                                                            ?>
                                                            <i class="fa fa-download"></i>&nbsp;
                                                            <a href="<?php echo URL.$value['file_url'];?>" style="color:#337ab7;" download>
                                                                <?php 
                                                                    if($this->lang == 'vi'){
                                                                ?>
                                                                    Tải về
                                                                <?php }else{ ?>
                                                                    Download
                                                                <?php } ?>
                                                            </a>
                                                            <?php } ?>
                                                            <?php 
                                                                if(strlen($value['file_link']) >1){
                                                            ?>
                                                            <i class="fa fa-eye"></i>&nbsp;
                                                            <a href="<?php echo $value['file_link'];?>" style="color:#337ab7;">
                                                                <?php 
                                                                    if($this->lang == 'vi'){
                                                                ?>
                                                                    Xem
                                                                <?php }else{ ?>
                                                                    View
                                                                <?php } ?>
                                                            </a>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                <?php }else{ ?> 
                                <tr class="white">
                                            <td style="vertical-align: middle;padding: 10px;width:73%;">
                                                <div class="filetitle">
                                                    <i class="fa fa-file"></i>&nbsp;
                                                    <?php 
                                                        if($this->lang == 'vi'){
                                                    ?>
                                                        <?php echo $value['file_name'];?>
                                                    <?php }else{ ?>
                                                        <?php echo $value['file_name_english'];?>
                                                    <?php } ?>
                                                </div>
                                            </td>
                                            <td style="vertical-align: middle;padding: 10px;width:20%;">
                                                <div class="filedate">
                                                    <i class="fa fa-calendar"></i>&nbsp;<?php echo date('d-m-Y',strtotime($value['file_create_date']));?>
                                                </div>
                                            </td>
                                            <td style="vertical-align: middle;padding: 10px;width:10%;">    
                                                <div class="filedownload">
                                                    <?php 
                                                                if(strlen($value['file_url']) >1){
                                                            ?>
                                                            <i class="fa fa-download"></i>&nbsp;
                                                            <a href="<?php echo URL.$value['file_url'];?>" style="color:#337ab7;" download>
                                                                <?php 
                                                                    if($this->lang == 'vi'){
                                                                ?>
                                                                    Tải về
                                                                <?php }else{ ?>
                                                                    Download
                                                                <?php } ?>
                                                            </a>
                                                            <?php } ?>
                                                            <?php 
                                                                if(strlen($value['file_link']) >1){
                                                            ?>
                                                            <i class="fa fa-eyes"></i>&nbsp;
                                                            <a href="<?php echo $value['file_link'];?>" style="color:#337ab7;">
                                                                <?php 
                                                                    if($this->lang == 'vi'){
                                                                ?>
                                                                    Xem
                                                                <?php }else{ ?>
                                                                    View
                                                                <?php } ?>
                                                            </a>
                                                            <?php } ?>
                                                </div>
                                            </td>
                                        </tr>
                                <?php } }} ?>
                            </tbody>
                        </table>
                    </div>
                    <?php } ?>
                </div>

                <div class="celebrity_or_video_text_area" style="width: 30%;">
                    <div class="rtf">
                        <ul style="list-style-type: none;line-height: 25px;">
                            <?php 
                                if($this->listcategoriesfather){
                                    foreach ($this->listcategoriesfather as $value) {
                                        if($filecategoriseinfo[0]['file_categories_id'] == $value['file_categories_id']){
                            ?>
                            <li>
                                <?php 
                                    if($configurl == 'dai'){
                                ?>
                                <a href="<?php echo URL.$this->lang;?>/category/file/<?php echo $value['file_categories_url'];?>.html" style="color: #63544d;font-size: 16px;text-decoration:none;text-transform:uppercase;font-weight:500;border-bottom: 2px solid #63544d;">
                                    <?php 
                                            if($this->lang == 'vi'){
                                        ?>
                                            <?php echo $value['file_categories_name'];?>
                                        <?php }else{ ?>
                                            <?php echo $value['file_categories_name_english'];?>
                                        <?php } ?>
                                </a>
                                <?php }else{ ?>
                                <a href="<?php echo URL.$this->lang;?>/<?php echo $value['file_categories_url'];?>.html" style="color: #63544d;font-size: 16px;text-decoration:none;text-transform:uppercase;font-weight:500;border-bottom: 2px solid #63544d;">
                                    <?php 
                                            if($this->lang == 'vi'){
                                        ?>
                                            <?php echo $value['file_categories_name'];?>
                                        <?php }else{ ?>
                                            <?php echo $value['file_categories_name_english'];?>
                                        <?php } ?>
                                </a>
                                <?php } ?>
                            </li>
                            <?php }else{ ?>
                            <li>
                                <?php 
                                    if($configurl == 'dai'){
                                ?>
                                <a href="<?php echo URL.$this->lang;?>/category/file/<?php echo $value['file_categories_url'];?>.html" style="color: #63544d;font-size: 16px;text-decoration:none;text-transform:uppercase;">
                                    <?php 
                                            if($this->lang == 'vi'){
                                        ?>
                                            <?php echo $value['file_categories_name'];?>
                                        <?php }else{ ?>
                                            <?php echo $value['file_categories_name_english'];?>
                                        <?php } ?>
                                </a>
                                <?php }else{ ?>
                                <a href="<?php echo URL.$this->lang;?>/<?php echo $value['file_categories_url'];?>.html" style="color: #63544d;font-size: 16px;text-decoration:none;text-transform:uppercase;">
                                    <?php 
                                            if($this->lang == 'vi'){
                                        ?>
                                            <?php echo $value['file_categories_name'];?>
                                        <?php }else{ ?>
                                            <?php echo $value['file_categories_name_english'];?>
                                        <?php } ?>
                                </a>
                                <?php } ?>
                            </li>
                            <?php }}} ?>
                            <li>
                                <a href="#" style="color: #63544d;font-size: 16px;text-decoration:none;font-weight: 400;text-transform:uppercase;">
									<?php 
										if($this->lang == 'vi'){
									?>
										THÔNG TIN CỔ PHIẾU
									<?php }else{ ?>
										Information of Sharing
									<?php } ?>
								</a>
                            </li>
                            <li>
                                <a href="<?php echo URL;?>contact" style="color: #63544d;font-size: 16px;text-decoration:none;font-weight: 400;text-transform:uppercase;">
									<?php 
										if($this->lang == 'vi'){
									?>
										KẾT NỐI CỔ ĐÔNG
									<?php }else{ ?>
										Shareholder connection
									<?php } ?>
								</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>