<?php 
    function cut_string($str, $len, $more) {
        if ($str == "" || $str == NULL)
            return $str;
        if (is_array($str))
            return $str;
        $str = trim($str);
        if (strlen($str) <= $len)
            return $str;
        $str = substr($str, 0, $len);
        if ($str != "") {
            if (!substr_count($str, " ")) {
                if ($more)
                    $str .= " ...";
                return $str;
            }
            while (strlen($str) && ($str[strlen($str) - 1] != " ")) {
                $str = substr($str, 0, -1);
            }
            $str = substr($str, 0, -1);
            if ($more)
                $str .= " ...";
        }
        return $str;
    }
?>
<style type="text/css">
    .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
        background: #215240;
        color:#fff !important;
        font-family: 'Open Sans', sans-serif;
    }
    .nav-tabs>li>a{
        border:1px solid #dfdfdf;
        border-radius: 8px 8px 0 0;
        margin-right: 0px;
        color: #333;
    }
    .tableft>li{
        list-style: none;
        min-height: 20px;
        border-bottom: 1px solid #dfdfdf;
        padding:10px;
    }
    .tableft a{
        text-decoration: none;
        color:#1c809b;
        font-family: 'Open Sans', sans-serif;
        font-size: 13px;
        font-weight: 600;
    }
    .tableft a:hover{
        text-decoration: none;
        color:#215240;
    }
    .panel{
        border-radius: 8px 8px 0 0;
    }
    .panel-default>.panel-heading{
        background: #1c809b;
        color: #fff;
        border-radius: 8px 8px 0 0;
        text-transform: uppercase;
        font-weight: bold;
    }
    .panel-body a{
        text-decoration: none;
    }
    .panel-body a:hover{
        color: #339966;
    }
    .h3danhmuc{
        margin: 5px 0px;
        font-size: 15px;
        color: #666666;
        font-weight: 600;
        margin-left: 10px;
        line-height: 18px;
    }
    .h3danhmuc:hover{
        color: #747474;
    }
    .description{
        margin-left: 10px;
        margin-right: 10px;
        font-size: 12px;
        color: #333;
        margin-top: 5px;
        margin-bottom: 5px;
    }
    .datetime{
        margin-left: 10px;
        font-size: 12px;
        margin-top: 5px;
        margin-bottom: 5px;
        color: #747474;
    }
    .h3email{
        margin-top: 0px;
        font-size: 15px;
        font-weight: 800;
    }
    .inputemail{
        width: 75%;
        height: 36px;
        border-radius: 0px !important;
        padding: 8px !important;
    }
    .btnemail{
        text-align: center;
        margin-left: -5px;
        height: 36px;
        padding: 0 18px;
        background: #1c809b;
        color: #fff;
        border:none;
    }
    .btnemail:hover{
        background: #42768f;
    }
    .selectlink{
        height: 36px;
        width: 99%;
        padding: 5px;
    }
    .h3tuvan{
        font-size: 16px;
        font-weight: 600;
    }
    .fb-like-box, .fb-like-box span, .fb-like-box span iframe[style] { width: 100% !important; }
    .flex-direction-nav .flex-prev {
        position: absolute;
        right: 60px;
        top: -55px;
    }
    .flex-direction-nav .flex-next {
        right: 10px;
        top: -55px;
    }

    #featured .ui-tabs-panel{ 
        width:100% !important; 
        height:360px; 
        background:#999; 
        position:relative;
    }
    #featured .ui-tabs-panel .info {
        position: absolute;
        bottom: 0;
        left: 0;
        height: 110px;
        background: #000;
        opacity: 0.8;
        width: 100%;
        padding: 15px 10px;
        font-family: 'Open Sans', sans-serif;
    }
    #featured .info p {
        margin: 5px 5px;
        font-family: 'Open Sans', sans-serif;
        font-size: 12px;
        line-height: 15px;
        color: #f0f0f0;
    }
    @media screen and (min-width: 768px){
        .carousel-caption {
            right: 0;
            left: 0;
        }
    }
    .carousel-caption {
        position: absolute;
        width: 100%;
        height: 100px;
        bottom: 0px;
        z-index: 10;
        padding: 10px;
        padding-bottom: 10px !Important;
        color: #fff;
        background: #000;
        opacity: 0.8;
        text-align: left;
    }
	.carousel-caption h3:hover{ 
		color:#339966;
	}
    .carousel-caption h3{ 
        font-size:1.2em; 
        font-family: 'Open Sans', sans-serif; 
        color:#fff; 
        padding:5px; 
        margin:0; 
        font-weight:normal;
        overflow:hidden; 
    }
    .carousel-caption p{ 
        margin:0 5px; 
        font-family: 'Open Sans', sans-serif;  
        font-size:12px; 
        line-height:15px; color:#f0f0f0;
    }
    #myindicators {
        top: 20px;            
        left: 25%;
        z-index: 15;
        width: 100%;
        padding-left: 0;
        text-align: right;
        list-style: none;
    }
    #myindicators .active {
        width: 15px;
        height: 15px;
        margin: 0;
        background-color: #fff;
        border: 3px solid #333;
        padding: 6px;
    }
    #myindicators li {
        display: inline-block;
        width: 15px;
        height: 15px;
        margin: 1px;
        text-indent: -999px;
        cursor: pointer;
        background-color: #000;
        background-color: rgba(0,0,0,0);
        border: 2px solid #333;
        border-radius: 10px;
    }
    .dropdown-menu>.active>a, .dropdown-menu>.active>a:focus, .dropdown-menu>.active>a:hover{
        background: #336c57;
        color: #fff;
    }
    .khungsuccess{
        margin: 0;
        border: 1px solid #45866c;
        padding: 20px;
        text-align: center;
        border-radius: 5px;
    }
	.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover{
		background-color: #1c809b;
		border-color: #1c809b;
	}
	.pagination>li>a, .pagination>li>span{
		color: #1c809b;
	}
</style>
<?php 
    $listalbumn = $this->listalbumn;
?>
<div class="col-sm-12 col-xs-12" style="padding:0 20px;">
    <div class="row" style="margin:0;">
        <?php 
             if($this->listalbumn){
                    $i=0;
                    foreach ($this->listalbumn as $value) {
                        $i++;
                            if($i % 3 == 0){
        ?>
        <div class="col-sm-4 col-xs-6" style="padding-left:0px;padding-right:0px;margin-bottom: 15px;">
            <div class="panel panel-default" style="border-radius:0px;height:289px;border:none;margin-bottom: 5px;">
                <div class="panel-body" style="padding:0px;border:1px solid #ddd;height:289px;">
                    <div class="row" style="margin:0;">
                            <?php
                                if(strlen($value['image_default']) > 1){
                            ?>
                            <div style="background-image:url('<?php echo $value['image_default'];?>');background-size:cover;height:213px;width:100%;cursor:pointer;" onclick="window.location.href='<?php echo URL;?>albumn/detail/<?php echo $value['albumn_url'];?>';"></div>
                            <?php }else{?>
                            <div style="background-image:url('<?php echo URL;?>public/img/noneimage.jpg');background-size:cover;height:213px;width:100%;cursor:pointer;" onclick="window.location.href='<?php echo URL;?>albumn/detail/<?php echo $value['albumn_url'];?>';"></div>
                            <?php } ?>
							<div style="background:#fff;">
								<?php 
									if($this->lang == 'vi'){
								?>
								<a href="<?php echo URL;?>albumn/detail/<?php echo $value['albumn_url'];?>">
									<h3 class="h3danhmuc"><?php echo $value['albumn_name'];?></h3>
								</a>
								<?php }else{ ?>
								<a href="<?php echo URL;?>albumn/detail/<?php echo $value['albumn_url'];?>">
									<h3 class="h3danhmuc"><?php echo $value['albumn_name_english'];?></h3>
								</a>
								<?php } ?>
								<div class="description">
									<?php 
										if($this->lang == 'vi'){
											echo $value['albumn_description'];
										}else{ 
											echo $value['albumn_description_english'];
										} 
									?>
								</div>
							</div>
                    </div>
                </div>
            </div>
        </div>
        <?php }else{?>
        <div class="col-sm-4 col-xs-6" style="padding-left:0px;padding-right:20px;margin-bottom: 15px;">
            <div class="panel panel-default" style="border-radius:0px;border:none;height:289px;margin-bottom: 5px;">
                <div class="panel-body" style="padding:0px;border:1px solid #ddd;height:289px;">
                    <div class="row" style="margin:0;">
                            <?php
                                if(strlen($value['image_default']) > 1){
                            ?>
                            <div style="background-image:url('<?php echo $value['image_default'];?>');background-size:cover;height:213px;width:100%;cursor:pointer;" onclick="window.location.href='<?php echo URL;?>albumn/detail/<?php echo $value['albumn_url'];?>';"></div>
                            <?php }else{?>
                            <div style="background-image:url('<?php echo URL;?>public/img/noneimage.jpg');background-size:cover;height:213px;width:100%;cursor:pointer;" onclick="window.location.href='<?php echo URL;?>albumn/detail/<?php echo $value['albumn_url'];?>';"></div>
                            <?php } ?>
							<div style="background:#fff;">
								<?php 
									if($this->lang == 'vi'){
								?>
								<a href="<?php echo URL;?>albumn/detail/<?php echo $value['albumn_url'];?>" >
									<h3 class="h3danhmuc"><?php echo $value['albumn_name'];?></h3>
								</a>
								<?php }else{ ?>
								<a href="<?php echo URL;?>albumn/detail/<?php echo $value['albumn_url'];?>">
									<h3 class="h3danhmuc"><?php echo $value['albumn_name_english'];?></h3>
								</a>
								<?php } ?>
								<div class="description">
									<?php 
										if($this->lang == 'vi'){
											echo $value['albumn_description'];
										}else{ 
											echo $value['albumn_description_english'];
										} 
									?>
								</div>
							</div>
                    </div>
                </div>
            </div>
        </div>
		<?php } }} ?>
    </div>
</div>