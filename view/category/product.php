<style type="text/css">
    .rtf ol li, .rtf ul li {
        margin-left: 5px;
    }
</style>
<div style="margin-top: 0px;background: #f7f7f7;" id="main" role="main">
    <div id="main-contents" class="single-col homepage-wrap" style="margin-top: 160px;">
        <div class="homepage-module">
            <div style="padding-top: 0px; margin-top: 0px; padding-bottom: 0px; margin-bottom: 0px;" class="celebrity_or_video">
                <div class="celebrity_or_video_content_area" style="width: 75%;margin-left:0px;">
                    <center>
                        <img src="<?php echo URL;?>public/img/sanpham.png" alt="" style="border-width:0px;">
                    </center>
                    <div style="margin-top: 15px;margin-left: 5px;color: #63544d;font-weight: 400;">
                        <b>Khí thấp áp (Natural Gas)</b><br />
                        <br />
                        Kh&iacute; thấp &aacute;p (Natural Gas) l&agrave; kh&iacute; thi&ecirc;n nhi&ecirc;n ở &aacute;p suất thấp c&oacute; th&agrave;nh phần chủ yếu l&agrave; c&aacute;c hydrocacbon ở thể kh&iacute; trong đ&oacute; Metan chiếm tỷ lệ lớn nhất c&oacute; thể đến 85%, Etan 10% v&agrave; một lượng nhỏ hơn propan, butan v&agrave; c&aacute;c loại kh&iacute; kh&aacute;c. Kh&iacute; thi&ecirc;n nhi&ecirc;n được d&ugrave;ng phổ biến rộng r&atilde;i tr&ecirc;n thế giới cung cấp khoảng 25% năng lượng trong mọi lĩnh vực. Ở Việt Nam, kh&iacute; thấp &aacute;p l&agrave; dạng nhi&ecirc;n liệu mới cung cấp tới nơi ti&ecirc;u thụ bằng đường ống c&oacute; nhiều lợi thế vượt trội về phương diện c&ocirc;ng nghệ, m&ocirc;i trường v&agrave; kinh tế so với c&aacute;c nhi&ecirc;n liệu truyền thống kh&aacute;c.<br />
                        <br />
                        Về phương diện c&ocirc;ng nghệ, kh&iacute; thấp &aacute;p c&oacute; nhiệt trị lớn, hiệu suất đốt ch&aacute;y cao, dễ d&agrave;ng điều chỉnh nhiệt độ buồng đốt, c&ocirc;ng t&aacute;c vận h&agrave;nh/bảo dưỡng hệ thống dễ d&agrave;ng. Nhiệt trị của kh&iacute; thấp &aacute;p được cung cấp bởi PV GAS D trong dải từ 38-42 MJ/SM3.<br />
                        <br />
                        Về phương diện m&ocirc;i trường, sử dụng kh&iacute; thấp &aacute;p sẽ giảm thiểu được t&aacute;c động &ocirc; nhiễm m&ocirc;i trường, h&agrave;m lượng kh&iacute; thải COx, SOx v&agrave; NOx ở mức thấp hơn nhiều so với đốt c&ugrave;ng một lượng nhi&ecirc;n liệu kh&aacute;c. Trong c&aacute;c loại nhi&ecirc;n liệu h&oacute;a thạch, kh&iacute; thấp &aacute;p th&acirc;n thiện nhất với m&ocirc;i trường.<br />
                        <br />
                        Về phương diện kinh tế, gi&aacute; kh&iacute; thấp &aacute;p ở Việt Nam được cung cấp bởi PV GAS D cạnh tranh hơn so với c&aacute;c nhi&ecirc;u liệu truyền thống kh&aacute;c như DO, FO, LPG (t&iacute;nh theo đơn vị nhiệt lượng). Mặt kh&aacute;c, sử dụng kh&iacute; thấp &aacute;p l&agrave;m nhi&ecirc;n liệu sẽ giảm đ&aacute;ng kể chi ph&iacute; đầu tư cho kho b&atilde;i, bể chứa v&agrave; chi ph&iacute; bảo tr&igrave;, bảo dưỡng cũng như tăng tuổi thọ của thiết bị.<br />
                        <br />
                        Trước những ưu việt n&ecirc;u tr&ecirc;n cũng như khả năng khai th&aacute;c/nguồn cung kh&iacute; tại Việt Nam, PV GAS D đ&atilde; v&agrave; đang từng ng&agrave;y cung cấp kh&iacute; tới c&aacute;c hộ ti&ecirc;u thụ c&ocirc;ng nghiệp trong c&aacute;c KCN Ph&uacute; Mỹ - Mỹ Xu&acirc;n &ndash; G&ograve; Dầu tại tỉnh B&agrave; Rịa Vũng T&agrave;u v&agrave; Đồng Nai, c&aacute;c KCN Huyện Nhơn Trạch (Đồng Nai), Hiệp Phước (Th&agrave;nh phố Hồ Ch&iacute; Minh) v&agrave; c&aacute;c KCN huyện Tiền Hải (Th&aacute;i B&igrave;nh).<br />
                        <br />
                        CNG (Compressed Natural Gas)<br />
                        CNG (Compressed Natural Gas) l&agrave; kh&iacute; thi&ecirc;n nhi&ecirc;n (Natural Gas) được n&eacute;n dưới &aacute;p suất cao (khoảng 250 barg). CNG được n&eacute;n v&agrave;o c&aacute;c bồn chuy&ecirc;n dụng v&agrave; vận chuyển tới nơi ti&ecirc;u thụ bằng c&aacute;c đầu k&eacute;o (CNG trailer).<br />
                        <br />
                        CNG c&oacute; đầy đủ c&aacute;c đặc t&iacute;nh của kh&iacute; thi&ecirc;n nhi&ecirc;n đ&oacute; l&agrave;:<br />
                        <br />
                        CNG c&oacute; nhiệt trị lớn trong dải từ 38-42 MJ/SM3, hiệu suất đốt ch&aacute;y cao, dễ d&agrave;ng điều chỉnh nhiệt độ buồng đốt, c&ocirc;ng t&aacute;c vận h&agrave;nh/bảo dưỡng hệ thống dễ d&agrave;ng.<br />
                        <br />
                        Về m&ocirc;i trường, sử dụng CNG sẽ giảm thiểu được t&aacute;c động &ocirc; nhiễm m&ocirc;i trường, h&agrave;m lượng kh&iacute; thải COx, SOx v&agrave; NOx ở mức thấp hơn nhiều so với đốt c&ugrave;ng một lượng nhi&ecirc;n liệu kh&aacute;c. Trong c&aacute;c loại nhi&ecirc;n liệu, CNG th&acirc;n thiện nhất với m&ocirc;i trường.<br />
                        <br />
                        Về kinh tế, gi&aacute; kh&iacute; CNG được cung cấp bởi PV GAS D cạnh tranh hơn so với c&aacute;c nhi&ecirc;u liệu truyền thống kh&aacute;c như DO, FO, LPG (t&iacute;nh theo đơn vị nhiệt lượng). PV GAS D l&agrave; đơn vị đầu nguồn thực hiện việc sản xuất CNG để ph&acirc;n phối cho c&aacute;c kh&aacute;ch h&agrave;ng ở c&aacute;c Khu c&ocirc;ng nghiệp ti&ecirc;u thụ. Mặt kh&aacute;c, sử dụng kh&iacute; CNG sẽ giảm đ&aacute;ng kể chi ph&iacute; đầu tư cho kho b&atilde;i, bể chứa v&agrave; chi ph&iacute; bảo tr&igrave;, bảo dưỡng cũng như tăng tuổi thọ của thiết bị.
                    </div>
                </div>

                <div class="celebrity_or_video_text_area" style="width: 24%;">
                    <div class="rtf">
                        <ul style="list-style-type: none;line-height: 25px;">
                            <li>
                                <a href="#" style="color: #63544d;font-size: 16px;text-decoration:none;font-weight: 500;border-bottom: 2px solid #63544d;">SẢN PHẨM CHÍNH</a>
                            </li>
                            <li>
                                <a href="#" style="color: #63544d;font-size: 16px;text-decoration:none;font-weight: 400;">HỆ THỐNG PHÂN PHỐI</a>
                            </li>
                            <li>
                                <a href="#" style="color: #63544d;font-size: 16px;text-decoration:none;font-weight: 400;">DỊCH VỤ</a>
                            </li>
                            <li>
                                <a href="#" style="color: #63544d;font-size: 16px;text-decoration:none;font-weight: 400;">SƠ ĐỒ NGUỒN KHÍ</a>
                            </li>
                            <li>
                                <a href="#" style="color: #63544d;font-size: 16px;text-decoration:none;font-weight: 400;">CLIP GIỚI THIỆU</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>