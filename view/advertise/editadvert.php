<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<?php 
    $advertiseinfo = $this->advertiseinfo;
?>
<div class="row">
    <div id="breadcrumb" class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo URL;?>admin">Quản trị</a></li>
            <li><a href="<?php echo URL;?>advertise">Danh sách quảng cáo</a></li>
            <li><a href="<?php echo URL;?>advertise/editadvertise/<?php echo $advertiseinfo[0]['advertise_id'];?>"><?php echo $advertiseinfo[0]['advertise_url'];?></a></li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <span>Form chỉnh sửa quảng cáo</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding"><br/>
                <div id="error"></div>
                <form class="form-horizontal" role="form" enctype="multipart/form-data">
                    <input type="hidden" name="advertise_id" value="<?php echo $advertiseinfo[0]['advertise_id'];?>">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hình ảnh quảng cáo</label>
                        <div class="col-sm-8">
                            <?php
                            if(strlen($advertiseinfo[0]['advertise_image']) > 1){
                                ?>
                                <img src="<?php echo $advertiseinfo[0]['advertise_image'];?>" class="img-rounded imgupload" alt="avatar" id="hinhimg" style="width:100%;">
                            <?php }else{ ?>
                                <img src="<?php echo URL;?>public/img/noneimage.jpg" class="img-rounded imgupload" alt="avatar" id="hinhimg" style="width:100%;">
                            <?php } ?>
                            <input type="file" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="hinh_img" name="hinh_img" accept="image/jpg,image/png,image/jpeg,image/gif"  required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Thời gian quảng cáo</label>
                        <label class="col-sm-1 control-label" style="text-align:center;">Từ</label>
                        <div class="col-sm-2">
                            <input type="date" value="<?php echo date('Y-m-d',strtotime($advertiseinfo[0]['advertise_start']));?>" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="advertise_start" name="advertise_start" required>
                        </div>
                        <label class="col-sm-1 control-label" style="text-align:center;">Đến</label>
                        <div class="col-sm-2">
                            <input type="date" value="<?php echo date('Y-m-d',strtotime($advertiseinfo[0]['advertise_end']));?>" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="advertise_end" name="advertise_end" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Đường dẫn quảng cáo</label>
                        <div class="col-sm-8">
                            <input type="text" value="<?php echo $advertiseinfo[0]['advertise_url'];?>" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="advertise_url" name="advertise_url" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Chọn loại quảng cáo</label>
                        <div class="col-sm-3">
                            <select id="s2_with_tag" class="populate placeholder" name="advertise_position">
                                <?php 
                                    if($advertiseinfo[0]['advertise_position'] == 1){
                                ?>
                                <option value="1" selected>Vị trí top (416 x 75)</option>
                                <option value="2">Vị trí medium (740 x 96)</option>
                                <option value="3">Vị trí sidebar (367 x 265)</option>
                                <?php }elseif($advertiseinfo[0]['advertise_position'] == 2){?>
                                <option value="1">Vị trí top (416 x 75)</option>
                                <option value="2" selected>Vị trí medium (740 x 96)</option>
                                <option value="3">Vị trí sidebar (367 x 265)</option>
                                <?php }elseif ($advertiseinfo[0]['advertise_position'] == 3) { ?>
                                <option value="1">Vị trí top (416 x 75)</option>
                                <option value="2">Vị trí medium (740 x 96)</option>
                                <option value="3" selected>Vị trí sidebar (367 x 265)</option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-offset-1 control-label">Trạng thái quảng cáo</label>
                        <div class="col-sm-2">
                            <?php
                                if($advertiseinfo[0]['advertise_status'] == 'on'){
                                    ?>
                                    <div class="toggle-switch toggle-switch-success">
                                        <label>
                                            <input type="checkbox" name="advertise_status" checked="">
                                            <div class="toggle-switch-inner"></div>
                                            <div class="toggle-switch-switch"><i class="fa fa-check"></i></div>
                                        </label>
                                    </div>
                                <?php }else{?>
                                    <div class="toggle-switch toggle-switch-success">
                                        <label>
                                            <input type="checkbox" name="advertise_status">
                                            <div class="toggle-switch-inner"></div>
                                            <div class="toggle-switch-switch"><i class="fa fa-check"></i></div>
                                        </label>
                                    </div>
                                <?php } ?>
                        </div>
                    </div>
                    <div class="form-group" id="progressbar" style="display: none;">
                        <center>
                            <progress></progress>
                        </center>
                    </div>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-success btn-label-left" name="register" id="update">
                                <span><i class="fa fa-clock-o"></i></span>
                                Update
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>