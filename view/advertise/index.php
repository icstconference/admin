<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<div class="row">
    <div id="breadcrumb" class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo URL;?>admin">Quản trị</a></li>
            <li><a href="<?php echo URL;?>advertise">Danh sách quảng cáo</a></li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <span>Form thêm quảng cáo</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding"><br/>
                <div id="error"></div>
                <form class="form-horizontal" role="form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hình ảnh quảng cáo</label>
                        <div class="col-sm-3">
                            <img src="#" id="hinhimg" class="img-responsive imgupload" style="border-radius:3px;border:1px solid #ddd;display:none;margin-bottom:5px;" /> 
                            <input type="file" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="hinh_img" name="hinh_img" accept="image/jpg,image/png,image/jpeg,image/gif"  required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Thời gian quảng cáo</label>
                        <label class="col-sm-1 control-label" style="text-align:center;">Từ</label>
                        <div class="col-sm-2">
                            <input type="date" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="advertise_start" name="advertise_start" required>
                        </div>
                        <label class="col-sm-1 control-label" style="text-align:center;">Đến</label>
                        <div class="col-sm-2">
                            <input type="date" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="advertise_end" name="advertise_end" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Đường dẫn quảng cáo</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="advertise_url" name="advertise_url" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Chọn loại quảng cáo</label>
                        <div class="col-sm-3">
                            <select id="s2_with_tag" class="populate placeholder" name="advertise_position">
                                <option value="1">Vị trí top (416 x 75)</option>
                                <option value="2">Vị trí medium (740 x 96)</option>
                                <option value="3">Vị trí sidebar (367 x 265)</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-offset-1 control-label">Trạng thái quảng cáo</label>
                        <div class="col-sm-2">
                            <div class="toggle-switch toggle-switch-success">
                                <label>
                                    <input type="checkbox" name="advertise_status" checked="">
                                    <div class="toggle-switch-inner"></div>
                                    <div class="toggle-switch-switch"><i class="fa fa-check"></i></div>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="progressbar" style="display: none;">
                        <center>
                            <progress></progress>
                        </center>
                    </div>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-success btn-label-left" name="register" id="submit">
                                <span><i class="fa fa-clock-o"></i></span>
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php
    $listadvertise = $this->listadvertise;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <i class="fa fa-gift"></i>
                    <span>Danh sách quảng cáo</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding">
                <table class="table table-bordered table-striped table-hover table-heading table-datatable" id="datatable-1">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Quảng cáo</th>
                        <th>Vị trí</th>
                        <th>Thời gian</th>
                        <th>Ngày tạo</th>
                        <th>Trạng thái</th>
                        <th>Tùy chỉnh</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($this->listadvertise){
                        $i=0;
                        foreach($this->listadvertise as $lp){
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i;?></td>
                                <td>
                                    <?php
                                    if(strlen($lp['advertise_image']) > 1){
                                        ?>
                                        <img src="<?php echo $lp['advertise_image'];?>" />
                                    <?php } else {?>
                                        <img src="<?php echo URL;?>public/img/noneimage.jpg" />
                                    <?php } ?>
                                </td>
                                <td>
                                    <?php 
                                        if($lp['advertise_position'] == 1){
                                            echo 'Vị trí top';
                                        }elseif($lp['advertise_position'] == 2){
                                            echo 'Vị trí medium';
                                        }elseif ($lp['advertise_position'] == 3) {
                                            echo 'Vị trí sidebar';
                                        }
                                    ?>
                                </td>
                                <td>
                                    <?php echo date('d/m/Y',strtotime($lp['advertise_start']));?> - 
                                    <?php echo date('d/m/Y',strtotime($lp['advertise_end']));?>
                                </td>
                                <td>
                                    <?php echo $lp['advertise_status'];?>
                                </td>
                                <td>
                                    <?php echo $lp['advertise_create_date'];?>
                                </td>
                                <td>
                                    <a class="btn btn-primary" href="<?php echo URL;?>advertise/editadvertise/<?php echo $lp['advertise_id'];?>">Edit</a>
                                    <a class="btn btn-danger" href="<?php echo URL;?>advertise/deleteadvertise/<?php echo $lp['advertise_id'];?>">Delete</a>
                                </td>
                            </tr>
                        <?php }} ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>