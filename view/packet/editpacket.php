<?php 
    $packetinfo = $this->packetinfo;
?>
<header class="page-header">
    <h2><?php echo $packetinfo[0]['packet_name'];?></h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <a href="<?php echo URL;?>packet/">
                    <span>Tất cả gói thanh toán</span>
                </a>
            </li>
            <li>
                <span><?php echo $packetinfo[0]['packet_name'];?></span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<?php                            
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Chỉnh sửa gói thanh toán mới</h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin:5px 0;text-align:center;color:red;">
            <?php
                if($this->error){
                    echo $this->error;
                }
            ?>
        </div>
        <form class="form-horizontal" method="post" role="form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tên gói thanh toán</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo $packetinfo[0]['packet_name'];?>" data-toggle="tooltip" data-placement="bottom" title="Nhập tiêu đề bài viết" id="packet_name" name="packet_name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Chọn sự kiện</label>
                        <div class="col-sm-8">
                            <select id="s2_with_tag" data-plugin-selectTwo class="form-control populate" name="event_id">
                                <?php
                                    if($this->listevent){
                                        foreach($this->listevent as $lt){
                                            if($lt['event_id'] == $packetinfo[0]['event_id']){
                                ?>
                                    <option value="<?php echo $lt['event_id'];?>" selected><?php echo $lt['event_name'];?></option>
                                <?php }else{ ?>
                                    <option value="<?php echo $lt['event_id'];?>"><?php echo $lt['event_name'];?></option>
                                <?php }}} ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Mô tả gói thanh toán</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" placeholder="Mô tả gói thanh toán" rows="5" title="Tooltip for name" id="wysiwig_full1" name="packet_description">
                                <?php echo $packetinfo[0]['packet_description'];?>
                            </textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Giá</label>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <input type="number" class="form-control" id="packet_price" name="packet_price" value="<?php echo $packetinfo[0]['packet_price'];?>" required>
                                <div class="input-group-addon">$</div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Số lượng đăng kí</label>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <input type="number" class="form-control" id="packet_people" name="packet_people" value="<?php echo $packetinfo[0]['packet_people'];?>" required>
                                <div class="input-group-addon">người</div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Nhập lại
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-success btn-label-left" name="register" id="submit">
                                <span><i class="fa fa-clock-o"></i></span>
                                Đồng ý
                            </button>
                        </div>
                    </div>
                </form>

    </div>
</section>
