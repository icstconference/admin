<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<?php 
    $forminfo = $this->forminfo;
?>
<div class="row">
    <div id="breadcrumb" class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo URL;?>admin">Quản trị</a></li>
            <li><a href="<?php echo URL;?>form/">Danh sách form</a></li>
            <li><a href="<?php echo URL;?>form/editform/<?php echo $forminfo[0]['form_id']; ?>"></a><?php echo $forminfo[0]['form_id']; ?></li>
           <li><a href="<?php echo URL;?>form/editform/<?php echo $forminfo[0]['form1_id']; ?>"></a><?php echo $forminfo[0]['form1_id']; ?></li>
        </ol>
    </div>
</div><div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <span>Form thêm nội dung form</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding"><br/>
                <div id="error"></div>
                <form class="form-horizontal" method="post" role="form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tác giả</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo $forminfo[0]['form1_author']; ?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="form1_author" name="form1_author" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tên bài báo</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo $forminfo[0]['form1_title']; ?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="form1_title" name="form1_title" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tên ấn phẩm</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo $forminfo[0]['form1_journal']; ?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="form1_journal" name="form1_journal" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Năm xuất bản</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo $forminfo[0]['form1_year']; ?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="form1_year" name="form1_year" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Danh mục bài báo khóa học</label>
                        <div class="col-sm-3">
                            <?php
                            if($this->listformcategories){
                                ?>
                                <select id="s2_with_tag" class="populate placeholder" name="form1_category_id">
                                    <?php foreach($this->listformcategories as $lt){
                                                if($lt['form1_categories_id'] == $forminfo[0]['form1_category_id']){
                                    ?>
                                        <option value="<?php echo $lt['form1_categories_id'];?>" selected><?php echo $lt['form1_categories_name'];?></option>
                                    <?php }else{ ?>
                                        <option value="<?php echo $lt['form1_categories_id'];?>"><?php echo $lt['form1_categories_name'];?></option>
                                    <?php }} ?>
                                </select>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Link bài báo</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo $forminfo[0]['form1_url']; ?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="form1_url" name="form1_url" required>
                        </div>
                    </div>
                    <div class="form-group" id="progressbar" style="display: none;">
                        <center>
                            <progress></progress>
                        </center>
                    </div>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-success btn-label-left" name="register" id="submit1">
                                <span><i class="fa fa-clock-o"></i></span>
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>