<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<div class="row">
    <div id="breadcrumb" class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo URL;?>admin">Quản trị</a></li>
            <li><a href="<?php echo URL;?>form/">Danh sách form</a></li>
        </ol>
    </div>
</div>
<?php                            
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];  
	if(strpos($create[0]['permission_detail'],'new') || $_SESSION['user_id'] == 1){
?>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <span>Form thêm form</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding"><br/>
                <div id="error"></div>
                <form class="form-horizontal" method="post" role="form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tên form</label>
                        <div class="col-sm-6">
                            <input type="text" placeholder="Tên form " class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="form_name" name="form_name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tên form tiếng anh</label>
                        <div class="col-sm-6">
                            <input type="text" placeholder="Tên form tiếng anh ..." class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="form_name_english" name="form_name_english" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Chọn loại form</label>
                        <div class="col-sm-3">
                            <select id="s2_with_tag" class="populate placeholder" name="form_type">
                                <option value="1">Bài báo khoa học</option>
                                <option value="2">Hoạt động sự kiện</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Chọn danh mục</label>
                        <div class="col-sm-3">
                            <select id="s2_with_tag" class="populate placeholder" name="form_categories_id">
                                <?php
                                $listpermission = $this->listpermission;
                                
                                if($_SESSION['user_id'] == 1){
                                    if($this->listcategories){
                                        foreach($this->listcategories as $lt){
                                ?>
                                    <option value="<?php echo $lt['news_categories_id'];?>"><?php echo $lt['news_categories_name'];?></option>
                                <?php }}}else{
                                    if($this->listcategories){
                                        foreach($this->listcategories as $lt){
                                            if(strpos($listpermission,$lt['news_categories_id'])){
                                ?>
                                    <option value="<?php echo $lt['news_categories_id'];?>"><?php echo $lt['news_categories_name'];?></option>
                                <?php }}}} ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Mô tả form</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" placeholder="Mô tả form ..." rows="5" title="Tooltip for name" id="wysiwig_full1" name="form_description"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Mô tả form tiếng anh</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" placeholder="Mô tả form tiếng anh ..." rows="5" title="Tooltip for name" id="wysiwig_full2" name="form_description_english"></textarea>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-success btn-label-left" name="register" id="submit">
                                <span><i class="fa fa-clock-o"></i></span>
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<?php
    $listform = $this->listform;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <i class="fa fa-gift"></i>
                    <span>Danh sách form</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding">
                <table class="table table-bordered table-striped table-hover table-heading table-datatable" id="datatable-1">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Tên form</th>
                        <th>Danh mục form</th>
                        <th>Loại form</th>
                        <th>Chỉnh sửa</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($this->listform){
                        $i=0;
                        foreach($this->listform as $lp){
                            $i++;
							if($_SESSION['user_id'] == 1){
                            ?>
                            <tr>
                                <td><?php echo $i;?></td>
                                <td><?php echo $lp['form_name'];?></td>
                                <td>
                                    <?php 
                                        if($lp['form_type'] == '1'){
                                            echo 'Bài báo khoa học';
                                        }else{
                                            echo 'Online seminar';
                                        }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if($this->listcategories){
                                        foreach($this->listcategories as $lt){
                                            if($lt['news_categories_id'] == $lp['form_categories_id']){
                                                ?>
                                                <?php echo $lt['news_categories_name'];?>
                                            <?php }}}?>
                                </td>
                                <td>                               
                                    <?php                                         
										if(strpos($edit[0]['permission_detail'],'new') || $_SESSION['user_id'] == 1){                                    
									?> 
                                    <a class="btn btn-info" href="<?php echo URL;?>form/editform/<?php echo $lp['form_id'];?>" >Edit</a>
                                    <?php } ?>
									<?php                                         
										if(strpos($delete[0]['permission_detail'],'new') || $_SESSION['user_id'] == 1){                                    
									?> 
									<a class="btn btn-danger" href="<?php echo URL;?>form/deleteform/<?php echo $lp['form_id'];?>" >Delete</a>
                                    <?php } ?>
                                </td>
                            </tr>
						<?php }else{ 
							if(strpos($listpermission,$lp['form_categories_id'])){
						?>	
						<tr>
                                <td><?php echo $i;?></td>
                                <td><?php echo $lp['form_name'];?></td>
                                <td>
                                    <?php 
                                        if($lp['form_type'] == '1'){
                                            echo 'Bài báo khoa học';
                                        }else{
                                            echo 'Online seminar';
                                        }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if($this->listcategories){
                                        foreach($this->listcategories as $lt){
                                            if($lt['news_categories_id'] == $lp['form_categories_id']){
                                                ?>
                                                <?php echo $lt['news_categories_name'];?>
                                            <?php }}}?>
                                </td>
                                <td>                               
                                    <?php                                         
										if(strpos($edit[0]['permission_detail'],'new') || $_SESSION['user_id'] == 1){                                    
									?>
                                    <a class="btn btn-info" href="<?php echo URL;?>form/editform/<?php echo $lp['form_id'];?>" >Edit</a>
									<?php } ?>
									<?php                                         
										if(strpos($delete[0]['permission_detail'],'new') || $_SESSION['user_id'] == 1){                                    
									?> 
									<a class="btn btn-danger" href="<?php echo URL;?>form/deleteform/<?php echo $lp['form_id'];?>" >Delete</a>
                                    <?php } ?>
                                </td>
                            </tr>
					<?php }}}} ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>