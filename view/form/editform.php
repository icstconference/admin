<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<?php 
    $forminfo = $this->forminfo;
?>
<div class="row">
    <div id="breadcrumb" class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo URL;?>admin">Quản trị</a></li>
            <li><a href="<?php echo URL;?>form/">Danh sách form</a></li>
            <li><a href="<?php echo URL;?>form/editform/<?php echo $forminfo[0]['form_id']; ?>"></a><?php echo $forminfo[0]['form_name']; ?></li>
        </ol>
    </div>
</div>
<?php 
	$create = $_SESSION['user_permission_create'];
	$delete = $_SESSION['user_permission_delete'];
	$edit = $_SESSION['user_permission_edit'];  
    if($forminfo[0]['form_type'] == 1){
?>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <span>Form thêm nội dung form</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding"><br/>
                <div id="error"></div>
                <form class="form-horizontal" role="form" enctype="multipart/form-data">
                    <input type="hidden" name="form_id" value="<?php echo $forminfo[0]['form_id']; ?>">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tác giả</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Tác giả" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="form1_author" name="form1_author" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tên bài báo</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Tên bài báo" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="form1_title" name="form1_title" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tên ấn phẩm</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Tên ấn phẩm" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="form1_journal" name="form1_journal" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Năm xuất bản</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Năm xuất bản" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="form1_year" name="form1_year" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Danh mục bài báo khóa học</label>
                        <div class="col-sm-3">
                            <?php
                            if($this->listformcategories){
                                ?>
                                <select id="s2_with_tag" class="populate placeholder" name="form1_category_id">
                                    <?php foreach($this->listformcategories as $lt){?>
                                        <option value="<?php echo $lt['form1_categories_id'];?>"><?php echo $lt['form1_categories_name'];?></option>
                                    <?php } ?>
                                </select>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tập tin bài báo</label>
                        <div class="col-sm-3">
                            <input type="file" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="form1_file" name="form1_file" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Link bài báo</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Năm xuất bản" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="form1_url" name="form1_url" required>
                        </div>
                    </div>
                    <div class="form-group" id="progressbar" style="display: none;">
                        <center>
                            <progress></progress>
                        </center>
                    </div>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-success btn-label-left" name="register" id="submit1">
                                <span><i class="fa fa-clock-o"></i></span>
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php
    $listform1 = $this->listform1;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <i class="fa fa-gift"></i>
                    <span>Danh sách nội dung form</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding">
                <table class="table table-bordered table-striped table-hover table-heading table-datatable" id="datatable-1">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Tác giả</th>
                        <th>Tên bài báo</th>
                        <th>Danh mục</th>
                        <th>Chỉnh sửa</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($this->listform1){
                        $i=0;
                        foreach($this->listform1 as $lp){
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i;?></td>
                                <td><?php echo $lp['form1_author'];?></td>
                                <td><?php echo $lp['form1_title'];?></td>
                                <td>
                                    <?php
                                    if($this->listformcategories){
                                        foreach($this->listformcategories as $lt){
                                            if($lt['form1_categories_id'] == $lp['form1_category_id']){
                                                ?>
                                                <?php echo $lt['form1_categories_name'];?>
                                            <?php }}}?>
                                </td>
                                <td>                               
                                    <?php                                         
										if(strpos($edit[0]['permission_detail'],'new') || $_SESSION['user_id'] == 1){                                    
									?>  
                                    <a class="btn btn-info" href="<?php echo URL;?>form/editform1/<?php echo $lp['form1_id'];?>" >Edit</a>
                                    <?php } ?>
									<?php                                         
										if(strpos($delete[0]['permission_detail'],'new') || $_SESSION['user_id'] == 1){                                    
									?> 
									<a class="btn btn-danger" href="<?php echo URL;?>form/deleteform1/<?php echo $lp['form1_id'];?>" >Delete</a>
                                    <?php } ?>
                                    <a class="btn btn-success" href="<?php echo URL;?><?php echo $lp['form1_file'];?>" download>Download</a>
                                </td>
                            </tr>
                        <?php }} ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<?php 
    if($forminfo[0]['form_type'] == 2){
?>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <span>Form thêm nội dung form</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding"><br/>
                <div id="error"></div>
                <form class="form-horizontal" role="form" enctype="multipart/form-data">
                    <input type="hidden" name="form_id" value="<?php echo $forminfo[0]['form_id']; ?>">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Ngày</label>
                        <div class="col-sm-8">
                            <input type="date" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="form2_date" name="form2_date" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Thời gian</label>
                        <div class="col-sm-8">
                            <input type="time" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="form2_hours" name="form2_hours" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Chủ đề</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Chủ đề " data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="form2_topic" name="form2_topic" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Chủ đề tiếng anh</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Chủ đề tiếng anh " data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="form2_topic_english" name="form2_topic_english" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Người phụ trách</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Người phụ trách " data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="form2_people" name="form2_people" required>
                        </div>
                    </div>
                    <div class="form-group" id="progressbar" style="display: none;">
                        <center>
                            <progress></progress>
                        </center>
                    </div>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-success btn-label-left" name="register" id="submit2">
                                <span><i class="fa fa-clock-o"></i></span>
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php
    $listform2 = $this->listform2;
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <i class="fa fa-gift"></i>
                    <span>Danh sách nội dung form</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding">
                <table class="table table-bordered table-striped table-hover table-heading table-datatable" id="datatable-1">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Chủ đề</th>
                        <th>Người phụ trách</th>
                        <th>Ngày</th>
                        <th>Chỉnh sửa</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($this->listform2){
                        $i=0;
                        foreach($this->listform2 as $lp){
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i;?></td>
                                <td><?php echo $lp['form2_topic'];?></td>
                                <td><?php echo $lp['form2_people'];?></td>
                                <td><?php echo $lp['form2_date'];?></td>
                                <td>                               
                                    <?php                                         
										if(strpos($edit[0]['permission_detail'],'new') || $_SESSION['user_id'] == 1){                                    
									?>  
                                    <a class="btn btn-info" href="<?php echo URL;?>form/editform2/<?php echo $lp['form2_id'];?>" >Edit</a>
                                    <?php } ?>
									<?php                                         
										if(strpos($delete[0]['permission_detail'],'new') || $_SESSION['user_id'] == 1){                                    
									?> 
									<a class="btn btn-danger" href="<?php echo URL;?>form/deleteform2/<?php echo $lp['form2_id'];?>" >Delete</a>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php }} ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <span>Form chỉnh form</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding"><br/>
                <div id="error"></div>
                <form class="form-horizontal" method="post" role="form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tên form</label>
                        <div class="col-sm-6">
                            <input type="text" value="<?php echo $forminfo[0]['form_name'];?>" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="form_name" name="form_name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tên form tiếng anh</label>
                        <div class="col-sm-6">
                            <input type="text" value="<?php echo $forminfo[0]['form_name_english'];?>" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="form_name_english" name="form_name_english" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Chọn loại form</label>
                        <div class="col-sm-3">
                            <select id="s2_with_tag" class="populate placeholder" name="form_type">
                                <?php 
                                    if($forminfo[0]['form_type'] == 1){
                                ?>
                                    <option value="1" selected>Bài báo khoa học</option>
                                    <option value="2">Online seminar</option>
                                <?php }else{ ?>
                                    <option value="1" >Bài báo khoa học</option>
                                    <option value="2" selected>Online seminar</option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Chọn danh mục</label>
                        <div class="col-sm-2">
                            <select id="s2_with_tag" class="populate placeholder" name="form_categories_id">
                                <?php
                                $listpermission = $this->listpermission;
                                
                                if($_SESSION['user_id'] == 1){
                                if($this->listcategories){
                                    foreach($this->listcategories as $lt){
                                        if($lt['news_categories_id'] == $forminfo[0]['form_categories_id']){
                                            ?>
                                            <option value="<?php echo $lt['news_categories_id'];?>"><?php echo $lt['news_categories_name'];?></option>
                                        <?php }}
                                    foreach($this->listcategories as $lt){
                                        if($lt['news_categories_id'] != $forminfo[0]['form_categories_id']){ ?>
                                            <option value="<?php echo $lt['news_categories_id'];?>"><?php echo $lt['news_categories_name'];?></option>
                                <?php }}}}else{
                                    if($this->listcategories){
                                        foreach($this->listcategories as $lt){
                                            if(strpos($listpermission,$lt['news_categories_id'])){
                                                if($lt['news_categories_id'] == $forminfo[0]['form_categories_id']){
                                ?>
                                        <option value="<?php echo $lt['news_categories_id'];?>" selected><?php echo $lt['news_categories_name'];?></option>
                                <?php }else{ ?>
                                        <option value="<?php echo $lt['news_categories_id'];?>"><?php echo $lt['news_categories_name'];?></option>
                                <?php }}}}} ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Mô tả form</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" placeholder="Mô tả form ..." rows="5" title="Tooltip for name" id="wysiwig_full1" name="form_description">
                                <?php echo $forminfo[0]['form_description'];?>
                            </textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Mô tả form tiếng anh</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" placeholder="Mô tả form tiếng anh ..." rows="5" title="Tooltip for name" id="wysiwig_full2" name="form_description_english">
                                <?php echo $forminfo[0]['form_description_english'];?>
                            </textarea>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-success btn-label-left" name="register" id="submit">
                                <span><i class="fa fa-clock-o"></i></span>
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>