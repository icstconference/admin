<style>
    .imgupload{
        margin-bottom: 10px;
        border: 1px solid #ddd;
        height: 200px;
    }
</style>
<?php 
    $forminfo = $this->forminfo;
?>
<div class="row">
    <div id="breadcrumb" class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo URL;?>admin">Quản trị</a></li>
            <li><a href="<?php echo URL;?>form/">Danh sách form</a></li>
            <li><a href="<?php echo URL;?>form/editform/<?php echo $forminfo[0]['form_id']; ?>"></a><?php echo $forminfo[0]['form_id']; ?></li>
            <li><a href="<?php echo URL;?>form/editform2/<?php echo $forminfo[0]['form2_id']; ?>"></a><?php echo $forminfo[0]['form2_id']; ?></li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box">
            <div class="box-header">
                <div class="box-name">
                    <span>Form thêm nội dung form</span>
                </div>
                <div class="box-icons">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="expand-link">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="no-move"></div>
            </div>
            <div class="box-content no-padding"><br/>
                <div id="error"></div>
                <form class="form-horizontal" method="post" role="form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Ngày</label>
                        <div class="col-sm-8">
                            <input type="date" value="<?php echo date('Y-m-d',strtotime($forminfo[0]['form2_date']));?>" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="form2_date" name="form2_date" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Thời gian</label>
                        <div class="col-sm-8">
                            <input type="time" value="<?php echo date('h:i',strtotime($forminfo[0]['form2_hours']));?>" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="form2_hours" name="form2_hours" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Chủ đề</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo $forminfo[0]['form2_topic']; ?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="form2_topic" name="form2_topic" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Chủ đề tiếng anh</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo $forminfo[0]['form2_topic_english']; ?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="form2_topic_english" name="form2_topic_english" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Người phụ trách</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo $forminfo[0]['form2_people']; ?>" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" id="form2_people" name="form2_people" required>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: 40px;">
                        <div class="col-sm-offset-4 col-sm-2">
                            <button type="reset" class="btn btn-danger btn-label-left" name="cancel">
                                <span><i class="fa fa-clock-o txt-danger"></i></span>
                                Cancel
                            </button>
                        </div>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-success btn-label-left" name="register" id="submit2">
                                <span><i class="fa fa-clock-o"></i></span>
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>