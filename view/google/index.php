<style type="text/css">
    fieldset {
        border: 1px solid #dfdfdf;
        margin: 0 2px;
        padding: 0.35em 0.625em 0.75em;
    }
</style>
<header class="page-header">
    <h2>Google XML</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo URL;?>admin">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li>
                <span>Google XML</span>
            </li>
        </ol>
                    
        <a class="sidebar-right-toggle" data-open="sidebar-right">
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>
</header>
<section class="panel">
    <div class="col-sm-6">
        <header class="panel-heading">
            <h2 class="panel-title">Tạo sitemap và ping Google</h2>
        </header>
        <div class="panel-body">
            <div class="row" style="margin:5px 0;text-align:center;display:none;" id="sitemap">
                <?php
                    if($this->error){
                        echo $this->error;
                    }
                ?>
            </div>
            <form method="post" role="form" enctype="multipart/form-data">
                <fieldset>
                    <label>Độ ưu tiên</label>
                    <div class="col-sm-12">
                        <label>Bài viết</label>
                        <input type="number" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Bài viết" name="priority_news" value="0.6">
                        <label>Chuyên mục</label>
                        <input type="number" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Chuyên mục" name="priority_category" value="0.8">
                        <label>Trang</label>
                        <input type="number" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Trang" name="priority_page" value="0.8">
                    </div>
                    <div class="form-group" id="progressbar" style="display: none;">
                        <center>
                            <progress></progress>
                        </center>
                    </div>
                    <div class="pull-right" style="padding-right: 10px;margin-top:10px;">
                        <button type="button" class="mb-xs mt-xs mr-xs btn btn-warning" id="createsitemap">Tạo sitemap</button>
                        <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" id="pinggoogle">Ping sitemap</button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</section>