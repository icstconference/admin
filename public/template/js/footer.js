function adjustHomeCarousel(pw) {
    var hc = $("ul#home-carousel"),
        margin_left = 0,
        page_width = pw.width();
    switch (!0) {
        case 1400 >= page_width:
            margin_left = (page_width - hc.find("img:first").width()) / 2 + "px";
            break;
        case page_width > 1400:
            margin_left = "auto"
    }
    hc.find("li img").each(function() {
        $(this).css("margin-left", margin_left)
    })
}

function galleryGoTo(gallery, detailedView, currI, nextI, numPerRow) {
    var activeImageWrap = detailedView.find("ul > li:visible"),
        detailViewClasses = detailedView[0].className,
        detailViewClassesArray = detailViewClasses.split(" "),
        rowNum = 0;
    $.each(detailViewClassesArray, function(index, value) {
        3 == value.split("-").length && (rowNum = parseInt(value.split("-")[2]), rowNum = isNaN(rowNum) ? 1 : rowNum)
    });
    var nextDetailedView, nextRowNum = Math.ceil(nextI / numPerRow);
    if (rowNum > nextRowNum & currI > nextI ? nextDetailedView = $(".detail-view-" + (rowNum - 1)) : nextRowNum > rowNum & nextI > currI && (nextDetailedView = $(".detail-view-" + (rowNum + 1))), addRemoveActive(gallery.find('> li a[rel="' + currI + '"]'), !0), addRemoveActive(gallery.find('> li a[rel="' + nextI + '"]')), !gallery.hasClass("celebrity-gallery")) {
        var imageLink = gallery.find("> li a[rel='" + nextI + "']"),
            fullImage = imageLink.attr("href");
        detailedView.find("li[rel='" + nextI + "'] .img-view-wrap img").attr("src", fullImage)
    }
    detailedView.is(":visible") && rowNum == nextRowNum ? activeImageWrap.fadeOut("fast", function() {
        detailedView.find('ul > li[rel="' + nextI + '"]').fadeIn()
    }) : nextRowNum != rowNum && hideShowDetailed(gallery, detailedView, currI, [nextDetailedView, nextI], numPerRow)
}

function hideShowDetailed(gallery, detailedView, i, nextItems, numPerRow) {
    if (detailedView.is(":visible")) {
        var selectedIndex = detailedView.find("ul > li:visible").attr("rel"),
            selectedRowNum = Math.ceil(selectedIndex / numPerRow),
            curRowNum = Math.ceil(i / numPerRow);
        if (!gallery.hasClass("celebrity-gallery")) {
            var imageLink = gallery.find("> li a[rel='" + i + "']"),
                fullImage = imageLink.attr("href");
            detailedView.find("li[rel='" + i + "'] .img-view-wrap img").attr("src", fullImage)
        }
        selectedIndex == i || 0 == i || selectedRowNum != curRowNum ? (addRemoveActive(detailedView, !0), 0 == aniRun && (aniRun = !0, detailedView.slideUp(400, function() {
            addRemoveActive(gallery.find('> li a[class="active"]'), !0), gallery.removeClass("opaque"), detailedView.find("ul > li").hide(), void 0 != nextItems && hideShowDetailed(gallery, nextItems[0], nextItems[1]), aniRun = !1
        }))) : detailedView.find("ul > li").hide(0, function() {
            addRemoveActive(gallery.find('> li a[class="active"]'), !0), detailedView.find('ul > li[rel="' + i + '"]').show(0, function() {
                addRemoveActive(gallery.find('> li a[rel="' + i + '"]'))
            })
        })
    } else {
        if (gallery.find(".detail-view:visible").length > 0) return void hideShowDetailed(gallery, gallery.find(".detail-view:visible"), i, [detailedView, i], numPerRow);
        if (detailedView.find('ul > li[rel="' + i + '"]').show(0, function() {
                addRemoveActive(gallery.find('> li a[rel="' + i + '"]')), gallery.addClass("opaque")
            }), addRemoveActive(detailedView), !gallery.hasClass("celebrity-gallery")) {
            var imageLink = gallery.find("> li a[rel='" + i + "']"),
                fullImage = imageLink.attr("href");
            detailedView.find("li[rel='" + i + "'] .img-view-wrap img").attr("src", fullImage)
        }
        galleryInstantAnimate ? (detailedView.show(), $("html, body").scrollTop($(this).offset().top - 30)) : detailedView.slideDown(400, function() {
            $("html, body").animate({
                scrollTop: $(this).offset().top - 30
            }, 800)
        }), galleryInstantAnimate = !1
    }
}

function addRemoveActive(item, remove) {
    return void 0 != remove ? void item.removeClass("active") : void item.addClass("active")
}

function adjustRoomCards(cardList, forceHeight) {
    var rowList = new Array,
        cardNum = 0,
        largestHeight = 0;
    if (forceHeight) $(cardList).each(function() {
        $(this).find("h2").height(forceHeight)
    });
    else {
        var cardListItems = cardList.children("li");
        cardListItems.each(function() {
            var last = $(this).index() == cardListItems.length - 1;
            if (rowList.push($(this)), cardNum++, 3 >= cardNum && !last) {
                var titleHeight = $(this).find("h2").height();
                titleHeight > largestHeight && (largestHeight = titleHeight)
            } else(parseInt(cardNum) > 3 || last) && (adjustRoomCards(rowList, largestHeight), cardNum = 0)
        })
    }
}

function filterGalleryItems(node, val) {
    document.location = val
}

function showCaption(element, numCaptions) {
    var $Element = null;
    $("#detail-cycle img").length > 1 ? ($Element = $(element), $Element.length > 0 && "FIGURE" == $Element[0].tagName ? ($Element = $Element.find("figcaption"), $Element.length > 0 && ($("#pagerCaption").html($Element.html()), "" != $.trim($Element.html()) && $("#pagerBar").fadeIn("fast"))) : "undefined" != typeof $Element.attr("alt") && "" !== $Element.attr("alt") ? ($("#pagerCaption").html($Element.attr("alt")), $("#pagerBar").fadeIn("fast")) : $("#pagerCaption").html("")) : 1 == $("#detail-cycle img").length && (1 == $("#detail-cycle figure").length ? ($Element = $("#detail-cycle figure figcaption"), $Element.length > 0 && ($("#pagerCaption").html($Element.html()), "" != $.trim($Element.html()) && $("#pagerBar").fadeIn("fast"))) : ($Element = $("#detail-cycle img"), "undefined" != typeof $Element.attr("alt") && "" !== $Element.attr("alt") && ($("#pagerCaption").html($Element.attr("alt")), "" != $.trim($Element.attr("alt")) && $("#pagerBar").fadeIn("fast"))))
}

function showCelebrityCaption(detailCycle) {
    var pagerBar = detailCycle.siblings(".pagerBar"),
        pagerCaption = pagerBar.find(".pagerCaption"),
        image = detailCycle.find("img");
    image.length > 1 ? (image = $(this), "undefined" != typeof image.attr("alt") && "" !== image.attr("alt") ? (pagerCaption.html(image.attr("alt")), pagerBar.fadeIn("fast")) : pagerCaption.html("")) : 1 == image.length && "undefined" != typeof image.attr("alt") && "" !== image.attr("alt") && (pagerCaption.html(image.attr("alt")), pagerBar.fadeIn("fast"))
}

function showHomeCaption($next) {
    if (!($(".byline_no_border").length > 0)) {
        var fadeTime = 1e3,
            $slideCaption = $next.find(".slide-caption"),
            $pagerCaption = $("#pager-caption");
        $("#pager-wrap-pos"), $("#pager-wrap");
        if ($("body").hasClass("mobile")) var fadeInTime = 0,
            fadeOutTime = 0;
        else var fadeInTime = fadeTime;
        if ($pagerCaption.is(":empty")) return void $pagerCaption.append($slideCaption.clone().removeAttr("style"));
        null == $slideCaption.attr("style"), $pagerCaption.fadeOut(fadeOutTime, function() {
            $("#pager-caption").empty()
        }), $pagerCaption.queue(function() {
            $(this).append($slideCaption.clone().removeAttr("style")), $(this).dequeue()
        }), $pagerCaption.queue(function() {
            $(this).fadeIn(fadeInTime), $(this).dequeue()
        })
    }
}

function ie6ImgSwap(picArg) {
    for (var a = 0; a < picArg.length; a++) $(picArg[a][0]).attr("src", picArg[a][1])
}

function gotoGalleryImg(goto, $gallery) {
    $gallery.find(".active").removeClass("active"), $gallery.find(goto).addClass("active")
}

function hideBlock($block) {
    return $block.find(".wrap").slideUp(800), $block.removeClass("active"), $block.find(".toggle").text("Show Details"), !1
}

function showBlock($block, f) {
    return "function" != typeof f ? $block.find(".wrap").slideDown(800) : $block.find(".wrap").slideDown(800, f), $block.addClass("active"), $block.find(".toggle").text("Hide Details"), !1
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return null == results ? "" : decodeURIComponent(results[1].replace(/\+/g, " "))
}

function setRequestorLocation() {
    var codeFromQuery = getParameterByName("usecountrycode");
    "" != codeFromQuery ? requestor_cc = codeFromQuery.toUpperCase() : $.ajax({
        url: "/country-code.aspx",
        data: {
            nocache: ""
        },
        success: function(data) {
            requestor_cc = data
        }
    })
}

function checkRequestorLocation(countryCode, callback) {
    $.isArray(countryCode) || (countryCode = [countryCode]), $.ajax({
        url: "/country-code.aspx",
        data: {
            nocache: ""
        },
        success: function(data) {
            requestor_cc = data, $.inArray(data, countryCode) > -1 && callback()
        }
    })
}

function generateCurrencyDropdown() {
    var fromCurrencyDropdownHtml = "",
        toCurrencyDropdownHtml = "",
        currencyUrl = window.location.protocol + "//" + window.location.host + "/currency.xml";
    $.ajax({
        type: "GET",
        url: currencyUrl,
        dataType: "xml",
        success: function(xml) {
            $(xml).find("currency").each(function() {
                var currencyValue = $(this).find("crate").text(),
                    currencySymbol = $(this).find("csymbol").text(),
                    startOptionString = "<option value='" + currencyValue,
                    endOptionString = "'>" + currencySymbol + "</option>";
                fromCurrencyDropdownHtml += startOptionString, toCurrencyDropdownHtml += startOptionString, "USD" == currencySymbol ? fromCurrencyDropdownHtml += "' selected='selected" : "EUR" == currencySymbol && (toCurrencyDropdownHtml += "' selected='selected"), fromCurrencyDropdownHtml += endOptionString, toCurrencyDropdownHtml += endOptionString
            }), $('[name="fromCurrency"]').html(fromCurrencyDropdownHtml), $('[name="toCurrency"]').html(toCurrencyDropdownHtml), $.uniform.update(".cconverter select")
        }
    })
}

function calcNewConversion(direction) {
    var originField, destinationField, originDropdown, destinationDropdown;
    "rtol" == direction ? (originField = $('[name="toCurrencyTextField"]'), destinationField = $('[name="fromCurrencyTextField"]'), originDropdown = "toCurrency", destinationDropdown = "fromCurrency") : (originField = $('[name="fromCurrencyTextField"]'), destinationField = $('[name="toCurrencyTextField"]'), originDropdown = "fromCurrency", destinationDropdown = "toCurrency");
    var calculatedEquivalent = 0,
        currentFieldValue = originField.prop("value"),
        fromValue = $('[name="' + originDropdown + '"] option:selected').val(),
        toValue = $('[name="' + destinationDropdown + '"] option:selected').val();
    calculatedEquivalent = (currentFieldValue * toValue / fromValue).toFixed(2), isNaN(parseFloat(calculatedEquivalent)) ? destinationField.attr("value", "Please Enter a Proper Number") : destinationField.attr("value", calculatedEquivalent)
}

function LookupError(key, defaultError) {
    var msg = "",
        xml = "";
    try {
        "undefined" != typeof MOPROErrorMessages && "undefined" != typeof MOPROErrorMessages.responseText && (xml = MOPROErrorMessages.responseText), msg = $(xml).find('systemcomponent > systemcomponentkeyvalues > sckey:contains("' + key + '")').siblings().text(), "" == msg && (msg = defaultError)
    } catch (m) {
        msg = defaultError
    }
    return msg
}

function LookupHotelInformation(queryFilters) {
    var results = [],
        xml = "";
    try {
        "undefined" != typeof HotelInformation && "undefined" != typeof HotelInformation.responseText && (xml = HotelInformation.responseText);
        var attrFilters = {
            equals: {},
            contains: {},
            startsWith: {},
            endsWith: {}
        };
        $.extend(attrFilters, queryFilters);
        var x = $(xml).find("hotel_information");
        $.each(attrFilters.equals, function(key, val) {
            x = x.filter(function() {
                return new RegExp("^" + val + "$", "i").test($(this).attr(key))
            })
        }), $.each(attrFilters.contains, function(key, val) {
            x = x.filter(function() {
                return new RegExp(val, "i").test($(this).attr(key))
            })
        }), $.each(attrFilters.startsWith, function(key, val) {
            x = x.filter(function() {
                return new RegExp("^" + val, "i").test($(this).attr(key))
            })
        }), $.each(attrFilters.endsWith, function(key, val) {
            x = x.filter(function() {
                return new RegExp(val + "$", "i").test($(this).attr(key))
            })
        }), x.each(function() {
            var attrs = {};
            $.each(this.attributes, function(i, attr) {
                attrs[attr.name] = attr.value
            }), results.push(attrs)
        })
    } catch (m) {
        results = []
    }
    return $(results)
}

function clearText(arg) {
    var a = arg.target;
    a.defaultValue == a.value ? a.value = "" : "" == a.value && (a.value = a.defaultValue)
}

function openImageZoom(imgSrc) {
    if (imgSrc.indexOf("?") > -1) {
        var endOfBaseImg = imgSrc.indexOf("?");
        imgSrc = imgSrc.substring(0, endOfBaseImg)
    }!isMobile.any();
    var winHeight = $(window).height();
    $("#image-zoom").show(), $("#image-zoom").append('<img src="' + imgSrc + "?fmt=jpg&hei=" + winHeight + '" />'), imageZoomOnImageLoad(imgSrc)
}

function equalHeight(group) {
    tallest = 0, group.each(function() {
        thisHeight = $(this).height(), thisHeight > tallest && (tallest = thisHeight)
    }), group.height(tallest)
}

function imageZoomOnImageLoad(imgSrc) {
    $("#image-zoom img").load(function() {
        var imgWidth = $(this).width();
        imgWidth > $(window).width() && ($(this).remove(), $("#image-zoom").append('<img src="' + imgSrc + "?wid=" + $(window).width() + '" />'), $("#image-zoom img").load(function() {
            var winHeight = $(window).height(),
                imgHeight = $(this).height(),
                marginTop = winHeight / 2 - imgHeight / 2;
            $("#image-zoom img").css("margin-top", marginTop + "px"), $("#image-zoom img").css("visibility", "visible")
        })), $("#image-zoom img").css("visibility", "visible")
    })
}

function openLogoutNotification(logoutMessage, loginText) {
    logoutMessage || (logoutMessage = "You've been logged out due to inactivity."), loginText || (loginText = "Login"), $("#image-zoom .close").hide(), $("#image-zoom").find("img").remove(), denyImageZoomClosing = !0, $("#image-zoom").show(), $("#image-zoom").append('<div class="logout-notify"><p>' + logoutMessage + '</p><p><a class="button-light">' + loginText + "</a></p></div>")
}

function displayWelcomeMessage(selector, msg) {
    $(selector).length > 0 && ($(selector).after('<div class="welcome-message">' + msg + "</div>"), $(".welcome-message").delay(5e3).fadeOut(1e3))
}

function timerIncrement() {
    idleTime += 1, idleTime > 29 && LoggedInFlag && (openLogoutNotification(), LoggedInFlag = !1, $(".button-light").click(function() {
        $("#image-zoom").hide(), __doPostBack("ctl00$cphTopContent$top1$lbMyProfileSignOut$signoutLink", "")
    }))
}

function tabLogic() {
    $(".tab-content").hide(), $(".tab-content:first").show(), $("ul.tabs li").click(function() {
        $(".tab-content").hide();
        var activeTab = $(this).attr("data-rel");
        $("#" + activeTab).fadeIn(), $("ul.tabs li").removeClass("active"), $(this).addClass("active")
    })
}

function closeModalNavHeadReset() {
    $("body.mobile").length > 0 ? ($("header").css("z-index", "3"), $("nav").css("z-index", "4")) : ($("header").css("z-index", "initial"), $("nav").css("z-index", "initial"))
}

function getCookie(cname) {
    for (var name = cname + "=", ca = document.cookie.split(";"), i = 0; i < ca.length; i++) {
        for (var c = ca[i];
            " " == c.charAt(0);) c = c.substring(1);
        if (0 == c.indexOf(name)) return c.substring(name.length, c.length)
    }
    return ""
}

function createCookie(name, value, days) {
    if (days) {
        var date = new Date;
        date.setTime(date.getTime() + 24 * days * 60 * 60 * 1e3);
        var expires = "; expires=" + date.toGMTString()
    } else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/"
}

function readCookie(name) {
    for (var nameEQ = name + "=", ca = document.cookie.split(";"), i = 0; i < ca.length; i++) {
        for (var c = ca[i];
            " " == c.charAt(0);) c = c.substring(1, c.length);
        if (0 == c.indexOf(nameEQ)) return c.substring(nameEQ.length, c.length)
    }
    return null
}

function eraseCookie(name) {
    createCookie(name, "", -1)
}

function saveBookCookie() {
    for (var MAXITEMSSAVED = 3, keyValueOBJ = {}, i = 0; i < arguments.length; i++) {
        var kv = arguments[i].split(":");
        "" != kv[i] && "undefined" != kv[i] && (keyValueOBJ[kv[0]] = kv[1])
    }
    var cookieVal = getCookie("recentSearches"),
        searchesJSON = {
            items: []
        };
    "" != cookieVal && (searchesJSON = JSON.parse(getCookie("recentSearches"))), -1 == cookieVal.indexOf(JSON.stringify(keyValueOBJ)) && searchesJSON.items.unshift(keyValueOBJ), searchesJSON.items.length > MAXITEMSSAVED && searchesJSON.items.pop(), createCookie("recentSearches", JSON.stringify(searchesJSON))
}

function normalizeHeightRooms() {
    "rtl" != $("html").css("direction") ? $(".gridviewWrapper section.roomType").css("float", "left") : $(".gridviewWrapper section.roomType").css("float", "right"), $(".gridviewWrapper section.roomType .three-col").removeClass("right"), $(".gridviewWrapper section.roomType:visible").each(function(index) {
        index + 1 != 1 && (index + 1) % 3 == 0 && $(this).find(".three-col").addClass("right")
    }), $(".gridviewWrapper section.roomType").find(".three-col.module").removeClass(function(index, css) {
        return (css.match(/(^|\s)js-colgroup-\S+/g) || []).join(" ")
    }), $(".gridviewWrapper section.roomType").normalizeHeight(".three-col.module")
}

function normalizeHeightGridView() {
    "rtl" != $("html").css("direction") ? $(".gridviewWrapper section").css("float", "left") : $(".gridviewWrapper section").css("float", "right"), $(".gridviewWrapper section .three-col").removeClass("right"), $(".gridviewWrapper section:visible").each(function(index) {
        index + 1 != 1 && (index + 1) % 3 == 0 && $(this).find(".three-col").addClass("right")
    }), $(".gridviewWrapper section.roomType").find(".three-col.module").removeClass(function(index, css) {
        return (css.match(/(^|\s)js-colgroup-\S+/g) || []).join(" ")
    }), $("#main-contents section").normalizeHeight(".three-col.module")
}

function removeFromCookie(cookieValToRemove) {
    var cookieVal = getCookie("recentSearches"),
        searchesJSON = {
            items: []
        };
    "" != cookieVal && (searchesJSON = JSON.parse(getCookie("recentSearches")));
    var itemIndexToRemove = null;
    $.each(searchesJSON.items, function(index, value) {
        JSON.stringify(value) == JSON.stringify(cookieValToRemove) && (itemIndexToRemove = index)
    }), null != itemIndexToRemove && (searchesJSON.items.splice(itemIndexToRemove, 1), createCookie("recentSearches", JSON.stringify(searchesJSON)))
}
window.log = function() {
        log.history = log.history || [], log.history.push(arguments), this.console && console.log(Array.prototype.slice.call(arguments))
    },
    function(b) {
        function c() {}
        for (var a, d = "assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,timeStamp,profile,profileEnd,time,timeEnd,trace,warn".split(","); a = d.pop();) b[a] = b[a] || c
    }(function() {
        try {
            return console.log(), window.console
        } catch (err) {
            return window.console = {}
        }
    }()),
    function(e) {
        "use strict";

        function t(e) {
            return (e || "").toLowerCase()
        }
        var i = "2.1.2";
        e.fn.cycle = function(i) {
            var n;
            return 0 !== this.length || e.isReady ? this.each(function() {
                var n, s, o, c, l = e(this),
                    r = e.fn.cycle.log;
                if (!l.data("cycle.opts")) {
                    (l.data("cycle-log") === !1 || i && i.log === !1 || s && s.log === !1) && (r = e.noop), r("--c2 init--"), n = l.data();
                    for (var a in n) n.hasOwnProperty(a) && /^cycle[A-Z]+/.test(a) && (c = n[a], o = a.match(/^cycle(.*)/)[1].replace(/^[A-Z]/, t), r(o + ":", c, "(" + typeof c + ")"), n[o] = c);
                    s = e.extend({}, e.fn.cycle.defaults, n, i || {}), s.timeoutId = 0, s.paused = s.paused || !1, s.container = l, s._maxZ = s.maxZ, s.API = e.extend({
                        _container: l
                    }, e.fn.cycle.API), s.API.log = r, s.API.trigger = function(e, t) {
                        return s.container.trigger(e, t), s.API
                    }, l.data("cycle.opts", s), l.data("cycle.API", s.API), s.API.trigger("cycle-bootstrap", [s, s.API]), s.API.addInitialSlides(), s.API.preInitSlideshow(), s.slides.length && s.API.initSlideshow()
                }
            }) : (n = {
                s: this.selector,
                c: this.context
            }, e.fn.cycle.log("requeuing slideshow (dom not ready)"), e(function() {
                e(n.s, n.c).cycle(i)
            }), this)
        }, e.fn.cycle.API = {
            opts: function() {
                return this._container.data("cycle.opts")
            },
            addInitialSlides: function() {
                var t = this.opts(),
                    i = t.slides;
                t.slideCount = 0, t.slides = e(), i = i.jquery ? i : t.container.find(i), t.random && i.sort(function() {
                    return Math.random() - .5
                }), t.API.add(i)
            },
            preInitSlideshow: function() {
                var t = this.opts();
                t.API.trigger("cycle-pre-initialize", [t]);
                var i = e.fn.cycle.transitions[t.fx];
                i && e.isFunction(i.preInit) && i.preInit(t), t._preInitialized = !0
            },
            postInitSlideshow: function() {
                var t = this.opts();
                t.API.trigger("cycle-post-initialize", [t]);
                var i = e.fn.cycle.transitions[t.fx];
                i && e.isFunction(i.postInit) && i.postInit(t)
            },
            initSlideshow: function() {
                var t, i = this.opts(),
                    n = i.container;
                i.API.calcFirstSlide(), "static" == i.container.css("position") && i.container.css("position", "relative"), e(i.slides[i.currSlide]).css({
                    opacity: 1,
                    display: "block",
                    visibility: "visible"
                }), i.API.stackSlides(i.slides[i.currSlide], i.slides[i.nextSlide], !i.reverse), i.pauseOnHover && (i.pauseOnHover !== !0 && (n = e(i.pauseOnHover)), n.hover(function() {
                    i.API.pause(!0)
                }, function() {
                    i.API.resume(!0)
                })), i.timeout && (t = i.API.getSlideOpts(i.currSlide), i.API.queueTransition(t, t.timeout + i.delay)), i._initialized = !0, i.API.updateView(!0), i.API.trigger("cycle-initialized", [i]), i.API.postInitSlideshow()
            },
            pause: function(t) {
                var i = this.opts(),
                    n = i.API.getSlideOpts(),
                    s = i.hoverPaused || i.paused;
                t ? i.hoverPaused = !0 : i.paused = !0, s || (i.container.addClass("cycle-paused"), i.API.trigger("cycle-paused", [i]).log("cycle-paused"), n.timeout && (clearTimeout(i.timeoutId), i.timeoutId = 0, i._remainingTimeout -= e.now() - i._lastQueue, (0 > i._remainingTimeout || isNaN(i._remainingTimeout)) && (i._remainingTimeout = void 0)))
            },
            resume: function(e) {
                var t = this.opts(),
                    i = !t.hoverPaused && !t.paused;
                e ? t.hoverPaused = !1 : t.paused = !1, i || (t.container.removeClass("cycle-paused"), 0 === t.slides.filter(":animated").length && t.API.queueTransition(t.API.getSlideOpts(), t._remainingTimeout), t.API.trigger("cycle-resumed", [t, t._remainingTimeout]).log("cycle-resumed"))
            },
            add: function(t, i) {
                var n, s = this.opts(),
                    o = s.slideCount,
                    c = !1;
                "string" == e.type(t) && (t = e.trim(t)), e(t).each(function() {
                    var t, n = e(this);
                    i ? s.container.prepend(n) : s.container.append(n), s.slideCount++, t = s.API.buildSlideOpts(n), s.slides = i ? e(n).add(s.slides) : s.slides.add(n), s.API.initSlide(t, n, --s._maxZ), n.data("cycle.opts", t), s.API.trigger("cycle-slide-added", [s, t, n])
                }), s.API.updateView(!0), c = s._preInitialized && 2 > o && s.slideCount >= 1, c && (s._initialized ? s.timeout && (n = s.slides.length, s.nextSlide = s.reverse ? n - 1 : 1, s.timeoutId || s.API.queueTransition(s)) : s.API.initSlideshow())
            },
            calcFirstSlide: function() {
                var e, t = this.opts();
                e = parseInt(t.startingSlide || 0, 10), (e >= t.slides.length || 0 > e) && (e = 0), t.currSlide = e, t.reverse ? (t.nextSlide = e - 1, 0 > t.nextSlide && (t.nextSlide = t.slides.length - 1)) : (t.nextSlide = e + 1, t.nextSlide == t.slides.length && (t.nextSlide = 0))
            },
            calcNextSlide: function() {
                var e, t = this.opts();
                t.reverse ? (e = 0 > t.nextSlide - 1, t.nextSlide = e ? t.slideCount - 1 : t.nextSlide - 1, t.currSlide = e ? 0 : t.nextSlide + 1) : (e = t.nextSlide + 1 == t.slides.length, t.nextSlide = e ? 0 : t.nextSlide + 1, t.currSlide = e ? t.slides.length - 1 : t.nextSlide - 1)
            },
            calcTx: function(t, i) {
                var n, s = t;
                return i && s.manualFx && (n = e.fn.cycle.transitions[s.manualFx]), n || (n = e.fn.cycle.transitions[s.fx]), n || (n = e.fn.cycle.transitions.fade, s.API.log('Transition "' + s.fx + '" not found.  Using fade.')), n
            },
            prepareTx: function(e, t) {
                var i, n, s, o, c, l = this.opts();
                return 2 > l.slideCount ? void(l.timeoutId = 0) : (!e || l.busy && !l.manualTrump || (l.API.stopTransition(), l.busy = !1, clearTimeout(l.timeoutId), l.timeoutId = 0), void(l.busy || (0 !== l.timeoutId || e) && (n = l.slides[l.currSlide], s = l.slides[l.nextSlide], o = l.API.getSlideOpts(l.nextSlide), c = l.API.calcTx(o, e), l._tx = c, e && void 0 !== o.manualSpeed && (o.speed = o.manualSpeed), l.nextSlide != l.currSlide && (e || !l.paused && !l.hoverPaused && l.timeout) ? (l.API.trigger("cycle-before", [o, n, s, t]), c.before && c.before(o, n, s, t), i = function() {
                    l.busy = !1, l.container.data("cycle.opts") && (c.after && c.after(o, n, s, t), l.API.trigger("cycle-after", [o, n, s, t]), l.API.queueTransition(o), l.API.updateView(!0))
                }, l.busy = !0, c.transition ? c.transition(o, n, s, t, i) : l.API.doTransition(o, n, s, t, i), l.API.calcNextSlide(), l.API.updateView()) : l.API.queueTransition(o))))
            },
            doTransition: function(t, i, n, s, o) {
                var c = t,
                    l = e(i),
                    r = e(n),
                    a = function() {
                        r.animate(c.animIn || {
                            opacity: 1
                        }, c.speed, c.easeIn || c.easing, o)
                    };
                r.css(c.cssBefore || {}), l.animate(c.animOut || {}, c.speed, c.easeOut || c.easing, function() {
                    l.css(c.cssAfter || {}), c.sync || a()
                }), c.sync && a()
            },
            queueTransition: function(t, i) {
                var n = this.opts(),
                    s = void 0 !== i ? i : t.timeout;
                return 0 === n.nextSlide && 0 === --n.loop ? (n.API.log("terminating; loop=0"), n.timeout = 0, s ? setTimeout(function() {
                    n.API.trigger("cycle-finished", [n])
                }, s) : n.API.trigger("cycle-finished", [n]), void(n.nextSlide = n.currSlide)) : void 0 !== n.continueAuto && (n.continueAuto === !1 || e.isFunction(n.continueAuto) && n.continueAuto() === !1) ? (n.API.log("terminating automatic transitions"), n.timeout = 0, void(n.timeoutId && clearTimeout(n.timeoutId))) : void(s && (n._lastQueue = e.now(), void 0 === i && (n._remainingTimeout = t.timeout), n.paused || n.hoverPaused || (n.timeoutId = setTimeout(function() {
                    n.API.prepareTx(!1, !n.reverse)
                }, s))))
            },
            stopTransition: function() {
                var e = this.opts();
                e.slides.filter(":animated").length && (e.slides.stop(!1, !0), e.API.trigger("cycle-transition-stopped", [e])), e._tx && e._tx.stopTransition && e._tx.stopTransition(e)
            },
            advanceSlide: function(e) {
                var t = this.opts();
                return clearTimeout(t.timeoutId), t.timeoutId = 0, t.nextSlide = t.currSlide + e, 0 > t.nextSlide ? t.nextSlide = t.slides.length - 1 : t.nextSlide >= t.slides.length && (t.nextSlide = 0), t.API.prepareTx(!0, e >= 0), !1
            },
            buildSlideOpts: function(i) {
                var n, s, o = this.opts(),
                    c = i.data() || {};
                for (var l in c) c.hasOwnProperty(l) && /^cycle[A-Z]+/.test(l) && (n = c[l], s = l.match(/^cycle(.*)/)[1].replace(/^[A-Z]/, t), o.API.log("[" + (o.slideCount - 1) + "]", s + ":", n, "(" + typeof n + ")"), c[s] = n);
                c = e.extend({}, e.fn.cycle.defaults, o, c), c.slideNum = o.slideCount;
                try {
                    delete c.API, delete c.slideCount, delete c.currSlide, delete c.nextSlide, delete c.slides
                } catch (r) {}
                return c
            },
            getSlideOpts: function(t) {
                var i = this.opts();
                void 0 === t && (t = i.currSlide);
                var n = i.slides[t],
                    s = e(n).data("cycle.opts");
                return e.extend({}, i, s)
            },
            initSlide: function(t, i, n) {
                var s = this.opts();
                i.css(t.slideCss || {}), n > 0 && i.css("zIndex", n), isNaN(t.speed) && (t.speed = e.fx.speeds[t.speed] || e.fx.speeds._default), t.sync || (t.speed = t.speed / 2), i.addClass(s.slideClass)
            },
            updateView: function(e, t) {
                var i = this.opts();
                if (i._initialized) {
                    var n = i.API.getSlideOpts(),
                        s = i.slides[i.currSlide];
                    !e && t !== !0 && (i.API.trigger("cycle-update-view-before", [i, n, s]), 0 > i.updateView) || (i.slideActiveClass && i.slides.removeClass(i.slideActiveClass).eq(i.currSlide).addClass(i.slideActiveClass), e && i.hideNonActive && i.slides.filter(":not(." + i.slideActiveClass + ")").css("visibility", "hidden"), 0 === i.updateView && setTimeout(function() {
                        i.API.trigger("cycle-update-view", [i, n, s, e])
                    }, n.speed / (i.sync ? 2 : 1)), 0 !== i.updateView && i.API.trigger("cycle-update-view", [i, n, s, e]), e && i.API.trigger("cycle-update-view-after", [i, n, s]))
                }
            },
            getComponent: function(t) {
                var i = this.opts(),
                    n = i[t];
                return "string" == typeof n ? /^\s*[\>|\+|~]/.test(n) ? i.container.find(n) : e(n) : n.jquery ? n : e(n)
            },
            stackSlides: function(t, i, n) {
                var s = this.opts();
                t || (t = s.slides[s.currSlide], i = s.slides[s.nextSlide], n = !s.reverse), e(t).css("zIndex", s.maxZ);
                var o, c = s.maxZ - 2,
                    l = s.slideCount;
                if (n) {
                    for (o = s.currSlide + 1; l > o; o++) e(s.slides[o]).css("zIndex", c--);
                    for (o = 0; s.currSlide > o; o++) e(s.slides[o]).css("zIndex", c--)
                } else {
                    for (o = s.currSlide - 1; o >= 0; o--) e(s.slides[o]).css("zIndex", c--);
                    for (o = l - 1; o > s.currSlide; o--) e(s.slides[o]).css("zIndex", c--)
                }
                e(i).css("zIndex", s.maxZ - 1)
            },
            getSlideIndex: function(e) {
                return this.opts().slides.index(e)
            }
        }, e.fn.cycle.log = function() {
            window.console && console.log && console.log("[cycle2] " + Array.prototype.join.call(arguments, " "))
        }, e.fn.cycle.version = function() {
            return "Cycle2: " + i
        }, e.fn.cycle.transitions = {
            custom: {},
            none: {
                before: function(e, t, i, n) {
                    e.API.stackSlides(i, t, n), e.cssBefore = {
                        opacity: 1,
                        visibility: "visible",
                        display: "block"
                    }
                }
            },
            fade: {
                before: function(t, i, n, s) {
                    var o = t.API.getSlideOpts(t.nextSlide).slideCss || {};
                    t.API.stackSlides(i, n, s), t.cssBefore = e.extend(o, {
                        opacity: 0,
                        visibility: "visible",
                        display: "block"
                    }), t.animIn = {
                        opacity: 1
                    }, t.animOut = {
                        opacity: 0
                    }
                }
            },
            fadeout: {
                before: function(t, i, n, s) {
                    var o = t.API.getSlideOpts(t.nextSlide).slideCss || {};
                    t.API.stackSlides(i, n, s), t.cssBefore = e.extend(o, {
                        opacity: 1,
                        visibility: "visible",
                        display: "block"
                    }), t.animOut = {
                        opacity: 0
                    }
                }
            },
            scrollHorz: {
                before: function(e, t, i, n) {
                    e.API.stackSlides(t, i, n);
                    var s = e.container.css("overflow", "hidden").width();
                    e.cssBefore = {
                        left: n ? s : -s,
                        top: 0,
                        opacity: 1,
                        visibility: "visible",
                        display: "block"
                    }, e.cssAfter = {
                        zIndex: e._maxZ - 2,
                        left: 0
                    }, e.animIn = {
                        left: 0
                    }, e.animOut = {
                        left: n ? -s : s
                    }
                }
            }
        }, e.fn.cycle.defaults = {
            allowWrap: !0,
            autoSelector: ".cycle-slideshow[data-cycle-auto-init!=false]",
            delay: 0,
            easing: null,
            fx: "fade",
            hideNonActive: !0,
            loop: 0,
            manualFx: void 0,
            manualSpeed: void 0,
            manualTrump: !0,
            maxZ: 100,
            pauseOnHover: !1,
            reverse: !1,
            slideActiveClass: "cycle-slide-active",
            slideClass: "cycle-slide",
            slideCss: {
                position: "absolute",
                top: 0,
                left: 0
            },
            slides: "> img",
            speed: 500,
            startingSlide: 0,
            sync: !0,
            timeout: 4e3,
            updateView: 0
        }, e(document).ready(function() {
            e(e.fn.cycle.defaults.autoSelector).cycle()
        })
    }(jQuery),
    function(e) {
        "use strict";

        function t(t, n) {
            var s, o, c, l = n.autoHeight;
            if ("container" == l) o = e(n.slides[n.currSlide]).outerHeight(), n.container.height(o);
            else if (n._autoHeightRatio) n.container.height(n.container.width() / n._autoHeightRatio);
            else if ("calc" === l || "number" == e.type(l) && l >= 0) {
                if (c = "calc" === l ? i(t, n) : l >= n.slides.length ? 0 : l, c == n._sentinelIndex) return;
                n._sentinelIndex = c, n._sentinel && n._sentinel.remove(), s = e(n.slides[c].cloneNode(!0)), s.removeAttr("id name rel").find("[id],[name],[rel]").removeAttr("id name rel"), s.css({
                    position: "static",
                    visibility: "hidden",
                    display: "block"
                }).prependTo(n.container).addClass("cycle-sentinel cycle-slide").removeClass("cycle-slide-active"), s.find("*").css("visibility", "hidden"), n._sentinel = s
            }
        }

        function i(t, i) {
            var n = 0,
                s = -1;
            return i.slides.each(function(t) {
                var i = e(this).height();
                i > s && (s = i, n = t)
            }), n
        }

        function n(t, i, n, s) {
            var o = e(s).outerHeight();
            i.container.animate({
                height: o
            }, i.autoHeightSpeed, i.autoHeightEasing)
        }

        function s(i, o) {
            o._autoHeightOnResize && (e(window).off("resize orientationchange", o._autoHeightOnResize), o._autoHeightOnResize = null), o.container.off("cycle-slide-added cycle-slide-removed", t), o.container.off("cycle-destroyed", s), o.container.off("cycle-before", n), o._sentinel && (o._sentinel.remove(), o._sentinel = null)
        }
        e.extend(e.fn.cycle.defaults, {
            autoHeight: 0,
            autoHeightSpeed: 250,
            autoHeightEasing: null
        }), e(document).on("cycle-initialized", function(i, o) {
            function c() {
                t(i, o)
            }
            var l, r = o.autoHeight,
                a = e.type(r),
                d = null;
            ("string" === a || "number" === a) && (o.container.on("cycle-slide-added cycle-slide-removed", t), o.container.on("cycle-destroyed", s), "container" == r ? o.container.on("cycle-before", n) : "string" === a && /\d+\:\d+/.test(r) && (l = r.match(/(\d+)\:(\d+)/), l = l[1] / l[2], o._autoHeightRatio = l), "number" !== a && (o._autoHeightOnResize = function() {
                clearTimeout(d), d = setTimeout(c, 50)
            }, e(window).on("resize orientationchange", o._autoHeightOnResize)), setTimeout(c, 30))
        })
    }(jQuery),
    function(e) {
        "use strict";
        e.extend(e.fn.cycle.defaults, {
            caption: "> .cycle-caption",
            captionTemplate: "{{slideNum}} / {{slideCount}}",
            overlay: "> .cycle-overlay",
            overlayTemplate: "<div>{{title}}</div><div>{{desc}}</div>",
            captionModule: "caption"
        }), e(document).on("cycle-update-view", function(t, i, n, s) {
            "caption" === i.captionModule && e.each(["caption", "overlay"], function() {
                var e = this,
                    t = n[e + "Template"],
                    o = i.API.getComponent(e);
                o.length && t ? (o.html(i.API.tmpl(t, n, i, s)), o.show()) : o.hide()
            })
        }), e(document).on("cycle-destroyed", function(t, i) {
            var n;
            e.each(["caption", "overlay"], function() {
                var e = this,
                    t = i[e + "Template"];
                i[e] && t && (n = i.API.getComponent("caption"), n.empty())
            })
        })
    }(jQuery),
    function(e) {
        "use strict";
        var t = e.fn.cycle;
        e.fn.cycle = function(i) {
            var n, s, o, c = e.makeArray(arguments);
            return "number" == e.type(i) ? this.cycle("goto", i) : "string" == e.type(i) ? this.each(function() {
                var l;
                return n = i, o = e(this).data("cycle.opts"), void 0 === o ? void t.log('slideshow must be initialized before sending commands; "' + n + '" ignored') : (n = "goto" == n ? "jump" : n, s = o.API[n], e.isFunction(s) ? (l = e.makeArray(c), l.shift(), s.apply(o.API, l)) : void t.log("unknown command: ", n))
            }) : t.apply(this, arguments)
        }, e.extend(e.fn.cycle, t), e.extend(t.API, {
            next: function() {
                var e = this.opts();
                if (!e.busy || e.manualTrump) {
                    var t = e.reverse ? -1 : 1;
                    e.allowWrap === !1 && e.currSlide + t >= e.slideCount || (e.API.advanceSlide(t), e.API.trigger("cycle-next", [e]).log("cycle-next"))
                }
            },
            prev: function() {
                var e = this.opts();
                if (!e.busy || e.manualTrump) {
                    var t = e.reverse ? 1 : -1;
                    e.allowWrap === !1 && 0 > e.currSlide + t || (e.API.advanceSlide(t), e.API.trigger("cycle-prev", [e]).log("cycle-prev"))
                }
            },
            destroy: function() {
                this.stop();
                var t = this.opts(),
                    i = e.isFunction(e._data) ? e._data : e.noop;
                clearTimeout(t.timeoutId), t.timeoutId = 0, t.API.stop(), t.API.trigger("cycle-destroyed", [t]).log("cycle-destroyed"), t.container.removeData(), i(t.container[0], "parsedAttrs", !1), t.retainStylesOnDestroy || (t.container.removeAttr("style"), t.slides.removeAttr("style"), t.slides.removeClass(t.slideActiveClass)), t.slides.each(function() {
                    e(this).removeData(), i(this, "parsedAttrs", !1)
                })
            },
            jump: function(e) {
                var t, i = this.opts();
                if (!i.busy || i.manualTrump) {
                    var n = parseInt(e, 10);
                    if (isNaN(n) || 0 > n || n >= i.slides.length) return void i.API.log("goto: invalid slide index: " + n);
                    if (n == i.currSlide) return void i.API.log("goto: skipping, already on slide", n);
                    i.nextSlide = n, clearTimeout(i.timeoutId), i.timeoutId = 0, i.API.log("goto: ", n, " (zero-index)"), t = i.currSlide < i.nextSlide, i.API.prepareTx(!0, t)
                }
            },
            stop: function() {
                var t = this.opts(),
                    i = t.container;
                clearTimeout(t.timeoutId), t.timeoutId = 0, t.API.stopTransition(), t.pauseOnHover && (t.pauseOnHover !== !0 && (i = e(t.pauseOnHover)), i.off("mouseenter mouseleave")), t.API.trigger("cycle-stopped", [t]).log("cycle-stopped")
            },
            reinit: function() {
                var e = this.opts();
                e.API.destroy(), e.container.cycle()
            },
            remove: function(t) {
                for (var i, n, s = this.opts(), o = [], c = 1, l = 0; s.slides.length > l; l++) i = s.slides[l], l == t ? n = i : (o.push(i), e(i).data("cycle.opts").slideNum = c, c++);
                n && (s.slides = e(o), s.slideCount--, e(n).remove(), t == s.currSlide ? s.API.advanceSlide(1) : s.currSlide > t ? s.currSlide-- : s.currSlide++, s.API.trigger("cycle-slide-removed", [s, t, n]).log("cycle-slide-removed"), s.API.updateView())
            }
        }), e(document).on("click.cycle", "[data-cycle-cmd]", function(t) {
            t.preventDefault();
            var i = e(this),
                n = i.data("cycle-cmd"),
                s = i.data("cycle-context") || ".cycle-slideshow";
            e(s).cycle(n, i.data("cycle-arg"))
        })
    }(jQuery),
    function(e) {
        "use strict";

        function t(t, i) {
            var n;
            return t._hashFence ? void(t._hashFence = !1) : (n = window.location.hash.substring(1), void t.slides.each(function(s) {
                if (e(this).data("cycle-hash") == n) {
                    if (i === !0) t.startingSlide = s;
                    else {
                        var o = s > t.currSlide;
                        t.nextSlide = s, t.API.prepareTx(!0, o)
                    }
                    return !1
                }
            }))
        }
        e(document).on("cycle-pre-initialize", function(i, n) {
            t(n, !0), n._onHashChange = function() {
                t(n, !1)
            }, e(window).on("hashchange", n._onHashChange)
        }), e(document).on("cycle-update-view", function(e, t, i) {
            i.hash && "#" + i.hash != window.location.hash && (t._hashFence = !0, window.location.hash = i.hash)
        }), e(document).on("cycle-destroyed", function(t, i) {
            i._onHashChange && e(window).off("hashchange", i._onHashChange)
        })
    }(jQuery),
    function(e) {
        "use strict";
        e.extend(e.fn.cycle.defaults, {
            loader: !1
        }), e(document).on("cycle-bootstrap", function(t, i) {
            function n(t, n) {
                function o(t) {
                    var o;
                    "wait" == i.loader ? (l.push(t), 0 === a && (l.sort(c), s.apply(i.API, [l, n]), i.container.removeClass("cycle-loading"))) : (o = e(i.slides[i.currSlide]), s.apply(i.API, [t, n]), o.show(), i.container.removeClass("cycle-loading"))
                }

                function c(e, t) {
                    return e.data("index") - t.data("index")
                }
                var l = [];
                if ("string" == e.type(t)) t = e.trim(t);
                else if ("array" === e.type(t))
                    for (var r = 0; t.length > r; r++) t[r] = e(t[r])[0];
                t = e(t);
                var a = t.length;
                a && (t.css("visibility", "hidden").appendTo("body").each(function(t) {
                    function c() {
                        0 === --r && (--a, o(d))
                    }
                    var r = 0,
                        d = e(this),
                        u = d.is("img") ? d : d.find("img");
                    return d.data("index", t), u = u.filter(":not(.cycle-loader-ignore)").filter(':not([src=""])'), u.length ? (r = u.length, void u.each(function() {
                        this.complete ? c() : e(this).load(function() {
                            c()
                        }).on("error", function() {
                            0 === --r && (i.API.log("slide skipped; img not loaded:", this.src), 0 === --a && "wait" == i.loader && s.apply(i.API, [l, n]))
                        })
                    })) : (--a, void l.push(d))
                }), a && i.container.addClass("cycle-loading"))
            }
            var s;
            i.loader && (s = i.API.add, i.API.add = n)
        })
    }(jQuery),
    function(e) {
        "use strict";

        function t(t, i, n) {
            var s, o = t.API.getComponent("pager");
            o.each(function() {
                var o = e(this);
                if (i.pagerTemplate) {
                    var c = t.API.tmpl(i.pagerTemplate, i, t, n[0]);
                    s = e(c).appendTo(o)
                } else s = o.children().eq(t.slideCount - 1);
                s.on(t.pagerEvent, function(e) {
                    t.pagerEventBubble || e.preventDefault(), t.API.page(o, e.currentTarget)
                })
            })
        }

        function i(e, t) {
            var i = this.opts();
            if (!i.busy || i.manualTrump) {
                var n = e.children().index(t),
                    s = n,
                    o = s > i.currSlide;
                i.currSlide != s && (i.nextSlide = s, i.API.prepareTx(!0, o), i.API.trigger("cycle-pager-activated", [i, e, t]))
            }
        }
        e.extend(e.fn.cycle.defaults, {
            pager: "> .cycle-pager",
            pagerActiveClass: "cycle-pager-active",
            pagerEvent: "click.cycle",
            pagerEventBubble: void 0,
            pagerTemplate: "<span>&bull;</span>"
        }), e(document).on("cycle-bootstrap", function(e, i, n) {
            n.buildPagerLink = t
        }), e(document).on("cycle-slide-added", function(e, t, n, s) {
            t.pager && (t.API.buildPagerLink(t, n, s), t.API.page = i)
        }), e(document).on("cycle-slide-removed", function(t, i, n) {
            if (i.pager) {
                var s = i.API.getComponent("pager");
                s.each(function() {
                    var t = e(this);
                    e(t.children()[n]).remove()
                })
            }
        }), e(document).on("cycle-update-view", function(t, i) {
            var n;
            i.pager && (n = i.API.getComponent("pager"), n.each(function() {
                e(this).children().removeClass(i.pagerActiveClass).eq(i.currSlide).addClass(i.pagerActiveClass)
            }))
        }), e(document).on("cycle-destroyed", function(e, t) {
            var i = t.API.getComponent("pager");
            i && (i.children().off(t.pagerEvent), t.pagerTemplate && i.empty())
        })
    }(jQuery),
    function(e) {
        "use strict";
        e.extend(e.fn.cycle.defaults, {
            next: "> .cycle-next",
            nextEvent: "click.cycle",
            disabledClass: "disabled",
            prev: "> .cycle-prev",
            prevEvent: "click.cycle",
            swipe: !1
        }), e(document).on("cycle-initialized", function(e, t) {
            if (t.API.getComponent("next").on(t.nextEvent, function(e) {
                    e.preventDefault(), t.API.next()
                }), t.API.getComponent("prev").on(t.prevEvent, function(e) {
                    e.preventDefault(), t.API.prev()
                }), t.swipe) {
                var i = t.swipeVert ? "swipeUp.cycle" : "swipeLeft.cycle swipeleft.cycle",
                    n = t.swipeVert ? "swipeDown.cycle" : "swipeRight.cycle swiperight.cycle";
                t.container.on(i, function() {
                    t.API.next()
                }), t.container.on(n, function() {
                    t.API.prev()
                })
            }
        }), e(document).on("cycle-update-view", function(e, t) {
            if (!t.allowWrap) {
                var i = t.disabledClass,
                    n = t.API.getComponent("next"),
                    s = t.API.getComponent("prev"),
                    o = t._prevBoundry || 0,
                    c = void 0 !== t._nextBoundry ? t._nextBoundry : t.slideCount - 1;
                t.currSlide == c ? n.addClass(i).prop("disabled", !0) : n.removeClass(i).prop("disabled", !1), t.currSlide === o ? s.addClass(i).prop("disabled", !0) : s.removeClass(i).prop("disabled", !1)
            }
        }), e(document).on("cycle-destroyed", function(e, t) {
            t.API.getComponent("prev").off(t.nextEvent), t.API.getComponent("next").off(t.prevEvent), t.container.off("swipeleft.cycle swiperight.cycle swipeLeft.cycle swipeRight.cycle swipeUp.cycle swipeDown.cycle")
        })
    }(jQuery),
    function(e) {
        "use strict";
        e.extend(e.fn.cycle.defaults, {
            progressive: !1
        }), e(document).on("cycle-pre-initialize", function(t, i) {
            if (i.progressive) {
                var n, s, o = i.API,
                    c = o.next,
                    l = o.prev,
                    r = o.prepareTx,
                    a = e.type(i.progressive);
                if ("array" == a) n = i.progressive;
                else if (e.isFunction(i.progressive)) n = i.progressive(i);
                else if ("string" == a) {
                    if (s = e(i.progressive), n = e.trim(s.html()), !n) return;
                    if (/^(\[)/.test(n)) try {
                        n = e.parseJSON(n)
                    } catch (d) {
                        return void o.log("error parsing progressive slides", d)
                    } else n = n.split(RegExp(s.data("cycle-split") || "\n")), n[n.length - 1] || n.pop()
                }
                r && (o.prepareTx = function(e, t) {
                    var s, o;
                    return e || 0 === n.length ? void r.apply(i.API, [e, t]) : void(t && i.currSlide == i.slideCount - 1 ? (o = n[0], n = n.slice(1), i.container.one("cycle-slide-added", function(e, t) {
                        setTimeout(function() {
                            t.API.advanceSlide(1)
                        }, 50)
                    }), i.API.add(o)) : t || 0 !== i.currSlide ? r.apply(i.API, [e, t]) : (s = n.length - 1, o = n[s], n = n.slice(0, s), i.container.one("cycle-slide-added", function(e, t) {
                        setTimeout(function() {
                            t.currSlide = 1, t.API.advanceSlide(-1)
                        }, 50)
                    }), i.API.add(o, !0)))
                }), c && (o.next = function() {
                    var e = this.opts();
                    if (n.length && e.currSlide == e.slideCount - 1) {
                        var t = n[0];
                        n = n.slice(1), e.container.one("cycle-slide-added", function(e, t) {
                            c.apply(t.API), t.container.removeClass("cycle-loading")
                        }), e.container.addClass("cycle-loading"), e.API.add(t)
                    } else c.apply(e.API)
                }), l && (o.prev = function() {
                    var e = this.opts();
                    if (n.length && 0 === e.currSlide) {
                        var t = n.length - 1,
                            i = n[t];
                        n = n.slice(0, t), e.container.one("cycle-slide-added", function(e, t) {
                            t.currSlide = 1, t.API.advanceSlide(-1), t.container.removeClass("cycle-loading")
                        }), e.container.addClass("cycle-loading"), e.API.add(i, !0)
                    } else l.apply(e.API)
                })
            }
        })
    }(jQuery),
    function(e) {
        "use strict";
        e.extend(e.fn.cycle.defaults, {
            tmplRegex: "{{((.)?.*?)}}"
        }), e.extend(e.fn.cycle.API, {
            tmpl: function(t, i) {
                var n = RegExp(i.tmplRegex || e.fn.cycle.defaults.tmplRegex, "g"),
                    s = e.makeArray(arguments);
                return s.shift(), t.replace(n, function(t, i) {
                    var n, o, c, l, r = i.split(".");
                    for (n = 0; s.length > n; n++)
                        if (c = s[n]) {
                            if (r.length > 1)
                                for (l = c, o = 0; r.length > o; o++) c = l, l = l[r[o]] || i;
                            else l = c[i];
                            if (e.isFunction(l)) return l.apply(c, s);
                            if (void 0 !== l && null !== l && l != i) return l
                        }
                    return i
                })
            }
        })
    }(jQuery),
    function(e) {
        "use strict";
        e.event.special.swipe = e.event.special.swipe || {
            scrollSupressionThreshold: 10,
            durationThreshold: 1e3,
            horizontalDistanceThreshold: 30,
            verticalDistanceThreshold: 75,
            setup: function() {
                var i = e(this);
                i.bind("touchstart", function(t) {
                    function n(i) {
                        if (r) {
                            var t = i.originalEvent.touches ? i.originalEvent.touches[0] : i;
                            s = {
                                time: (new Date).getTime(),
                                coords: [t.pageX, t.pageY]
                            }, Math.abs(r.coords[0] - s.coords[0]) > e.event.special.swipe.scrollSupressionThreshold && i.preventDefault()
                        }
                    }
                    var s, o = t.originalEvent.touches ? t.originalEvent.touches[0] : t,
                        r = {
                            time: (new Date).getTime(),
                            coords: [o.pageX, o.pageY],
                            origin: e(t.target)
                        };
                    i.bind("touchmove", n).one("touchend", function() {
                        i.unbind("touchmove", n), r && s && s.time - r.time < e.event.special.swipe.durationThreshold && Math.abs(r.coords[0] - s.coords[0]) > e.event.special.swipe.horizontalDistanceThreshold && Math.abs(r.coords[1] - s.coords[1]) < e.event.special.swipe.verticalDistanceThreshold && r.origin.trigger("swipe").trigger(r.coords[0] > s.coords[0] ? "swipeleft" : "swiperight"), r = s = void 0
                    })
                })
            }
        }, e.event.special.swipeleft = e.event.special.swipeleft || {
            setup: function() {
                e(this).bind("swipe", e.noop)
            }
        }, e.event.special.swiperight = e.event.special.swiperight || e.event.special.swipeleft
    }(jQuery), navigator.userAgent.match(/(iPhone|iPod|iPad)/i) && !navigator.userAgent.match(/.*(OS.7)*/i) && ! function(c) {
        function n(b, a, l) {
            var g, f = l[0],
                e = b === m;
            return l[0] = function() {
                f && (f.apply(c, arguments), e || (delete a[g], f = null))
            }, g = b.apply(c, l), a[g] = {
                args: l,
                created: Date.now(),
                cb: f,
                id: g
            }, g
        }

        function p(b, a, e, g, f) {
            function k() {
                d.cb && (d.cb.apply(c, arguments), h || (delete e[g], d.cb = null))
            }
            var d = e[g];
            if (d) {
                var h = b === m;
                a(d.id), h || (a = d.args[1], f = Date.now() - d.created, 0 > f && (f = 0), a -= f, 0 > a && (a = 0), d.args[1] = a), d.args[0] = k, d.created = Date.now(), d.id = b.apply(c, d.args)
            }
        }
        var e = {},
            k = {},
            q = c.setTimeout,
            m = c.setInterval,
            r = c.clearTimeout,
            s = c.clearInterval;
        if (!c.addEventListener) return !1;
        c.setTimeout = function() {
            return n(q, e, arguments)
        }, c.setInterval = function() {
            return n(m, k, arguments)
        }, c.clearTimeout = function(b) {
            var a = e[b];
            a && (delete e[b], r(a.id))
        }, c.clearInterval = function(b) {
            var a = k[b];
            a && (delete k[b], s(a.id))
        };
        for (var h = c; h.location != h.parent.location;) h = h.parent;
        h.addEventListener("scroll", function() {
            for (var b in e) p(q, r, e, b);
            for (b in k) p(m, s, k, b)
        })
    }(window), ! function(e, t, n) {
        "use strict";

        function s(e) {
            var t = Array.prototype.slice.call(arguments, 1);
            return e.prop ? e.prop.apply(e, t) : e.attr.apply(e, t)
        }

        function a(e, t, n) {
            var s, a;
            for (s in n) n.hasOwnProperty(s) && (a = s.replace(/ |$/g, t.eventNamespace), e.bind(a, n[s]))
        }

        function r(e, t, n) {
            a(e, n, {
                focus: function() {
                    t.addClass(n.focusClass)
                },
                blur: function() {
                    t.removeClass(n.focusClass), t.removeClass(n.activeClass)
                },
                mouseenter: function() {
                    t.addClass(n.hoverClass)
                },
                mouseleave: function() {
                    t.removeClass(n.hoverClass), t.removeClass(n.activeClass)
                },
                "mousedown touchbegin": function() {
                    e.is(":disabled") || t.addClass(n.activeClass)
                },
                "mouseup touchend": function() {
                    t.removeClass(n.activeClass)
                }
            })
        }

        function i(e, t) {
            e.removeClass(t.hoverClass + " " + t.focusClass + " " + t.activeClass)
        }

        function l(e, t, n) {
            n ? e.addClass(t) : e.removeClass(t)
        }

        function u(e, t, n) {
            var s = "checked",
                a = t.is(":" + s);
            t.prop ? t.prop(s, a) : a ? t.attr(s, s) : t.removeAttr(s), l(e, n.checkedClass, a)
        }

        function o(e, t, n) {
            l(e, n.disabledClass, t.is(":disabled"))
        }

        function c(e, t, n) {
            switch (n) {
                case "after":
                    return e.after(t), e.next();
                case "before":
                    return e.before(t), e.prev();
                case "wrap":
                    return e.wrap(t), e.parent()
            }
            return null
        }

        function d(e, n, a) {
            var r, i, l;
            return a || (a = {}), a = t.extend({
                bind: {},
                divClass: null,
                divWrap: "wrap",
                spanClass: null,
                spanHtml: null,
                spanWrap: "wrap"
            }, a), r = t("<div />"), i = t("<span />"), n.autoHide && e.is(":hidden") && "none" === e.css("display") && r.hide(), a.divClass && r.addClass(a.divClass), n.wrapperClass && r.addClass(n.wrapperClass), a.spanClass && i.addClass(a.spanClass), l = s(e, "id"), n.useID && l && s(r, "id", n.idPrefix + "-" + l), a.spanHtml && i.html(a.spanHtml), r = c(e, r, a.divWrap), i = c(e, i, a.spanWrap), o(r, e, n), {
                div: r,
                span: i
            }
        }

        function f(e, n) {
            var s;
            return n.wrapperClass ? (s = t("<span />").addClass(n.wrapperClass), s = c(e, s, "wrap")) : null
        }

        function p() {
            var n, s, a, r;
            return r = "rgb(120,2,153)", s = t('<div style="width:0;height:0;color:' + r + '">'), t("body").append(s), a = s.get(0), n = e.getComputedStyle ? e.getComputedStyle(a, "").color : (a.currentStyle || a.style || {}).color, s.remove(), n.replace(/ /g, "") !== r
        }

        function m(e) {
            return e ? t("<span />").text(e).html() : ""
        }

        function v() {
            return navigator.cpuClass && !navigator.product
        }

        function h() {
            return void 0 !== e.XMLHttpRequest ? !0 : !1
        }

        function C(e) {
            var t;
            return e[0].multiple ? !0 : (t = s(e, "size"), !t || 1 >= t ? !1 : !0)
        }

        function b() {
            return !1
        }

        function y(e, t) {
            var n = "none";
            a(e, t, {
                "selectstart dragstart mousedown": b
            }), e.css({
                MozUserSelect: n,
                msUserSelect: n,
                webkitUserSelect: n,
                userSelect: n
            })
        }

        function w(e, t, n) {
            var s = e.val();
            "" === s ? s = n.fileDefaultHtml : (s = s.split(/[\/\\]+/), s = s[s.length - 1]), t.text(s)
        }

        function g(e, t, n) {
            var s, a;
            for (s = [], e.each(function() {
                    var e;
                    for (e in t) Object.prototype.hasOwnProperty.call(t, e) && (s.push({
                        el: this,
                        name: e,
                        old: this.style[e]
                    }), this.style[e] = t[e])
                }), n(); s.length;) a = s.pop(), a.el.style[a.name] = a.old
        }

        function k(e, t) {
            var n;
            n = e.parents(), n.push(e[0]), n = n.not(":visible"), g(n, {
                visibility: "hidden",
                display: "block",
                position: "absolute"
            }, t)
        }

        function H(e, t) {
            return function() {
                e.unwrap().unwrap().unbind(t.eventNamespace)
            }
        }
        var x = !0,
            A = !1,
            W = [{
                match: function(e) {
                    return e.is("a, button, :submit, :reset, input[type='button']")
                },
                apply: function(t, n) {
                    var l, u, c, f, p;
                    return u = n.submitDefaultHtml, t.is(":reset") && (u = n.resetDefaultHtml), f = t.is("a, button") ? function() {
                        return t.html() || u
                    } : function() {
                        return m(s(t, "value")) || u
                    }, c = d(t, n, {
                        divClass: n.buttonClass,
                        spanHtml: f()
                    }), l = c.div, r(t, l, n), p = !1, a(l, n, {
                        "click touchend": function() {
                            var n, a, r, i;
                            p || t.is(":disabled") || (p = !0, t[0].dispatchEvent ? (n = document.createEvent("MouseEvents"), n.initEvent("click", !0, !0), a = t[0].dispatchEvent(n), t.is("a") && a && (r = s(t, "target"), i = s(t, "href"), r && "_self" !== r ? e.open(i, r) : document.location.href = i)) : t.click(), p = !1)
                        }
                    }), y(l, n), {
                        remove: function() {
                            return l.after(t), l.remove(), t.unbind(n.eventNamespace), t
                        },
                        update: function() {
                            i(l, n), o(l, t, n), t.detach(), c.span.html(f()).append(t)
                        }
                    }
                }
            }, {
                match: function(e) {
                    return e.is(":checkbox")
                },
                apply: function(e, t) {
                    var n, s, l;
                    return n = d(e, t, {
                        divClass: t.checkboxClass
                    }), s = n.div, l = n.span, r(e, s, t), a(e, t, {
                        "click touchend": function() {
                            u(l, e, t)
                        }
                    }), u(l, e, t), {
                        remove: H(e, t),
                        update: function() {
                            i(s, t), l.removeClass(t.checkedClass), u(l, e, t), o(s, e, t)
                        }
                    }
                }
            }, {
                match: function(e) {
                    return e.is(":file")
                },
                apply: function(e, n) {
                    function l() {
                        w(e, p, n)
                    }
                    var u, f, p, m;
                    return u = d(e, n, {
                        divClass: n.fileClass,
                        spanClass: n.fileButtonClass,
                        spanHtml: n.fileButtonHtml,
                        spanWrap: "after"
                    }), f = u.div, m = u.span, p = t("<span />").html(n.fileDefaultHtml), p.addClass(n.filenameClass), p = c(e, p, "after"), s(e, "size") || s(e, "size", f.width() / 10), r(e, f, n), l(), v() ? a(e, n, {
                        click: function() {
                            e.trigger("change"), setTimeout(l, 0)
                        }
                    }) : a(e, n, {
                        change: l
                    }), y(p, n), y(m, n), {
                        remove: function() {
                            return p.remove(), m.remove(), e.unwrap().unbind(n.eventNamespace)
                        },
                        update: function() {
                            i(f, n), w(e, p, n), o(f, e, n)
                        }
                    }
                }
            }, {
                match: function(e) {
                    if (e.is("input")) {
                        var t = (" " + s(e, "type") + " ").toLowerCase(),
                            n = " color date datetime datetime-local email month number password search tel text time url week ";
                        return n.indexOf(t) >= 0
                    }
                    return !1
                },
                apply: function(e, t) {
                    var n, a;
                    return n = s(e, "type"), e.addClass(t.inputClass), a = f(e, t), r(e, e, t), t.inputAddTypeAsClass && e.addClass(n), {
                        remove: function() {
                            e.removeClass(t.inputClass), t.inputAddTypeAsClass && e.removeClass(n), a && e.unwrap()
                        },
                        update: b
                    }
                }
            }, {
                match: function(e) {
                    return e.is(":radio")
                },
                apply: function(e, n) {
                    var l, c, f;
                    return l = d(e, n, {
                        divClass: n.radioClass
                    }), c = l.div, f = l.span, r(e, c, n), a(e, n, {
                        "click touchend": function() {
                            t.uniform.update(t(':radio[name="' + s(e, "name") + '"]'))
                        }
                    }), u(f, e, n), {
                        remove: H(e, n),
                        update: function() {
                            i(c, n), u(f, e, n), o(c, e, n)
                        }
                    }
                }
            }, {
                match: function(e) {
                    return e.is("select") && !C(e) ? !0 : !1
                },
                apply: function(e, n) {
                    var s, l, u, c;
                    return n.selectAutoWidth && k(e, function() {
                        c = e.width()
                    }), s = d(e, n, {
                        divClass: n.selectClass,
                        spanHtml: (e.find(":selected:first") || e.find("option:first")).html(),
                        spanWrap: "before"
                    }), l = s.div, u = s.span, n.selectAutoWidth ? k(e, function() {
                        g(t([u[0], l[0]]), {
                            display: "block"
                        }, function() {
                            var e;
                            e = u.outerWidth() - u.width(), l.width(c + e), u.width(c)
                        })
                    }) : l.addClass("fixedWidth"), r(e, l, n), a(e, n, {
                        change: function() {
                            u.html(e.find(":selected").html()), l.removeClass(n.activeClass)
                        },
                        "click touchend": function() {
                            var t = e.find(":selected").html();
                            u.html() !== t && e.trigger("change")
                        },
                        keyup: function() {
                            u.html(e.find(":selected").html())
                        }
                    }), y(u, n), {
                        remove: function() {
                            return u.remove(), e.unwrap().unbind(n.eventNamespace), e
                        },
                        update: function() {
                            n.selectAutoWidth ? (t.uniform.restore(e), e.uniform(n)) : (i(l, n), u.html(e.find(":selected").html()), o(l, e, n))
                        }
                    }
                }
            }, {
                match: function(e) {
                    return e.is("select") && C(e) ? !0 : !1
                },
                apply: function(e, t) {
                    var n;
                    return e.addClass(t.selectMultiClass), n = f(e, t), r(e, e, t), {
                        remove: function() {
                            e.removeClass(t.selectMultiClass), n && e.unwrap()
                        },
                        update: b
                    }
                }
            }, {
                match: function(e) {
                    return e.is("textarea")
                },
                apply: function(e, t) {
                    var n;
                    return e.addClass(t.textareaClass), n = f(e, t), r(e, e, t), {
                        remove: function() {
                            e.removeClass(t.textareaClass), n && e.unwrap()
                        },
                        update: b
                    }
                }
            }];
        v() && !h() && (x = !1), t.uniform = {
            defaults: {
                activeClass: "active",
                autoHide: !0,
                buttonClass: "button",
                checkboxClass: "checker",
                checkedClass: "checked",
                disabledClass: "disabled",
                eventNamespace: ".uniform",
                fileButtonClass: "action",
                fileButtonHtml: "Choose File",
                fileClass: "uploader",
                fileDefaultHtml: "No file selected",
                filenameClass: "filename",
                focusClass: "focus",
                hoverClass: "hover",
                idPrefix: "uniform",
                inputAddTypeAsClass: !0,
                inputClass: "uniform-input",
                radioClass: "radio",
                resetDefaultHtml: "Reset",
                resetSelector: !1,
                selectAutoWidth: !0,
                selectClass: "selector",
                selectMultiClass: "uniform-multiselect",
                submitDefaultHtml: "Submit",
                textareaClass: "uniform",
                useID: !0,
                wrapperClass: null
            },
            elements: []
        }, t.fn.uniform = function(n) {
            var s = this;
            return n = t.extend({}, t.uniform.defaults, n), A || (A = !0, p() && (x = !1)), x ? (n.resetSelector && t(n.resetSelector).mouseup(function() {
                e.setTimeout(function() {
                    t.uniform.update(s)
                }, 10)
            }), this.each(function() {
                var e, s, a, r = t(this);
                if (r.data("uniformed")) return void t.uniform.update(r);
                for (e = 0; e < W.length; e += 1)
                    if (s = W[e], s.match(r, n)) return a = s.apply(r, n), r.data("uniformed", a), void t.uniform.elements.push(r.get(0))
            })) : this
        }, t.uniform.restore = t.fn.uniform.restore = function(e) {
            e === n && (e = t.uniform.elements), t(e).each(function() {
                var e, n, s = t(this);
                n = s.data("uniformed"), n && (n.remove(), e = t.inArray(this, t.uniform.elements), e >= 0 && t.uniform.elements.splice(e, 1), s.removeData("uniformed"))
            })
        }, t.uniform.update = t.fn.uniform.update = function(e) {
            e === n && (e = t.uniform.elements), t(e).each(function() {
                var e, n = t(this);
                e = n.data("uniformed"), e && e.update(n, e.options)
            })
        }
    }(this, jQuery),
    function(e, t) {
        function i(t, i) {
            var s, a, o, r = t.nodeName.toLowerCase();
            return "area" === r ? (s = t.parentNode, a = s.name, t.href && a && "map" === s.nodeName.toLowerCase() ? (o = e("img[usemap=#" + a + "]")[0], !!o && n(o)) : !1) : (/input|select|textarea|button|object/.test(r) ? !t.disabled : "a" === r ? t.href || i : i) && n(t)
        }

        function n(t) {
            return e.expr.filters.visible(t) && !e(t).parents().addBack().filter(function() {
                return "hidden" === e.css(this, "visibility")
            }).length
        }
        var s = 0,
            a = /^ui-id-\d+$/;
        e.ui = e.ui || {}, e.extend(e.ui, {
            version: "1.10.4",
            keyCode: {
                BACKSPACE: 8,
                COMMA: 188,
                DELETE: 46,
                DOWN: 40,
                END: 35,
                ENTER: 13,
                ESCAPE: 27,
                HOME: 36,
                LEFT: 37,
                NUMPAD_ADD: 107,
                NUMPAD_DECIMAL: 110,
                NUMPAD_DIVIDE: 111,
                NUMPAD_ENTER: 108,
                NUMPAD_MULTIPLY: 106,
                NUMPAD_SUBTRACT: 109,
                PAGE_DOWN: 34,
                PAGE_UP: 33,
                PERIOD: 190,
                RIGHT: 39,
                SPACE: 32,
                TAB: 9,
                UP: 38
            }
        }), e.fn.extend({
            focus: function(t) {
                return function(i, n) {
                    return "number" == typeof i ? this.each(function() {
                        var t = this;
                        setTimeout(function() {
                            e(t).focus(), n && n.call(t)
                        }, i)
                    }) : t.apply(this, arguments)
                }
            }(e.fn.focus),
            scrollParent: function() {
                var t;
                return t = e.ui.ie && /(static|relative)/.test(this.css("position")) || /absolute/.test(this.css("position")) ? this.parents().filter(function() {
                    return /(relative|absolute|fixed)/.test(e.css(this, "position")) && /(auto|scroll)/.test(e.css(this, "overflow") + e.css(this, "overflow-y") + e.css(this, "overflow-x"))
                }).eq(0) : this.parents().filter(function() {
                    return /(auto|scroll)/.test(e.css(this, "overflow") + e.css(this, "overflow-y") + e.css(this, "overflow-x"))
                }).eq(0), /fixed/.test(this.css("position")) || !t.length ? e(document) : t
            },
            zIndex: function(i) {
                if (i !== t) return this.css("zIndex", i);
                if (this.length)
                    for (var n, s, a = e(this[0]); a.length && a[0] !== document;) {
                        if (n = a.css("position"), ("absolute" === n || "relative" === n || "fixed" === n) && (s = parseInt(a.css("zIndex"), 10), !isNaN(s) && 0 !== s)) return s;
                        a = a.parent()
                    }
                return 0
            },
            uniqueId: function() {
                return this.each(function() {
                    this.id || (this.id = "ui-id-" + ++s)
                })
            },
            removeUniqueId: function() {
                return this.each(function() {
                    a.test(this.id) && e(this).removeAttr("id")
                })
            }
        }), e.extend(e.expr[":"], {
            data: e.expr.createPseudo ? e.expr.createPseudo(function(t) {
                return function(i) {
                    return !!e.data(i, t)
                }
            }) : function(t, i, n) {
                return !!e.data(t, n[3])
            },
            focusable: function(t) {
                return i(t, !isNaN(e.attr(t, "tabindex")))
            },
            tabbable: function(t) {
                var n = e.attr(t, "tabindex"),
                    s = isNaN(n);
                return (s || n >= 0) && i(t, !s)
            }
        }), e("<a>").outerWidth(1).jquery || e.each(["Width", "Height"], function(i, n) {
            function s(t, i, n, s) {
                return e.each(a, function() {
                    i -= parseFloat(e.css(t, "padding" + this)) || 0, n && (i -= parseFloat(e.css(t, "border" + this + "Width")) || 0), s && (i -= parseFloat(e.css(t, "margin" + this)) || 0)
                }), i
            }
            var a = "Width" === n ? ["Left", "Right"] : ["Top", "Bottom"],
                o = n.toLowerCase(),
                r = {
                    innerWidth: e.fn.innerWidth,
                    innerHeight: e.fn.innerHeight,
                    outerWidth: e.fn.outerWidth,
                    outerHeight: e.fn.outerHeight
                };
            e.fn["inner" + n] = function(i) {
                return i === t ? r["inner" + n].call(this) : this.each(function() {
                    e(this).css(o, s(this, i) + "px")
                })
            }, e.fn["outer" + n] = function(t, i) {
                return "number" != typeof t ? r["outer" + n].call(this, t) : this.each(function() {
                    e(this).css(o, s(this, t, !0, i) + "px")
                })
            }
        }), e.fn.addBack || (e.fn.addBack = function(e) {
            return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
        }), e("<a>").data("a-b", "a").removeData("a-b").data("a-b") && (e.fn.removeData = function(t) {
            return function(i) {
                return arguments.length ? t.call(this, e.camelCase(i)) : t.call(this)
            }
        }(e.fn.removeData)), e.ui.ie = !!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase()), e.support.selectstart = "onselectstart" in document.createElement("div"), e.fn.extend({
            disableSelection: function() {
                return this.bind((e.support.selectstart ? "selectstart" : "mousedown") + ".ui-disableSelection", function(e) {
                    e.preventDefault()
                })
            },
            enableSelection: function() {
                return this.unbind(".ui-disableSelection")
            }
        }), e.extend(e.ui, {
            plugin: {
                add: function(t, i, n) {
                    var s, a = e.ui[t].prototype;
                    for (s in n) a.plugins[s] = a.plugins[s] || [], a.plugins[s].push([i, n[s]])
                },
                call: function(e, t, i) {
                    var n, s = e.plugins[t];
                    if (s && e.element[0].parentNode && 11 !== e.element[0].parentNode.nodeType)
                        for (n = 0; s.length > n; n++) e.options[s[n][0]] && s[n][1].apply(e.element, i)
                }
            },
            hasScroll: function(t, i) {
                if ("hidden" === e(t).css("overflow")) return !1;
                var n = i && "left" === i ? "scrollLeft" : "scrollTop",
                    s = !1;
                return t[n] > 0 ? !0 : (t[n] = 1, s = t[n] > 0, t[n] = 0, s)
            }
        })
    }(jQuery),
    function(t, e) {
        var i = 0,
            s = Array.prototype.slice,
            n = t.cleanData;
        t.cleanData = function(e) {
            for (var i, s = 0; null != (i = e[s]); s++) try {
                t(i).triggerHandler("remove")
            } catch (o) {}
            n(e)
        }, t.widget = function(i, s, n) {
            var o, a, r, h, l = {},
                c = i.split(".")[0];
            i = i.split(".")[1], o = c + "-" + i, n || (n = s, s = t.Widget), t.expr[":"][o.toLowerCase()] = function(e) {
                return !!t.data(e, o)
            }, t[c] = t[c] || {}, a = t[c][i], r = t[c][i] = function(t, i) {
                return this._createWidget ? (arguments.length && this._createWidget(t, i), e) : new r(t, i)
            }, t.extend(r, a, {
                version: n.version,
                _proto: t.extend({}, n),
                _childConstructors: []
            }), h = new s, h.options = t.widget.extend({}, h.options), t.each(n, function(i, n) {
                return t.isFunction(n) ? (l[i] = function() {
                    var t = function() {
                            return s.prototype[i].apply(this, arguments)
                        },
                        e = function(t) {
                            return s.prototype[i].apply(this, t)
                        };
                    return function() {
                        var i, s = this._super,
                            o = this._superApply;
                        return this._super = t, this._superApply = e, i = n.apply(this, arguments), this._super = s, this._superApply = o, i
                    }
                }(), e) : (l[i] = n, e)
            }), r.prototype = t.widget.extend(h, {
                widgetEventPrefix: a ? h.widgetEventPrefix || i : i
            }, l, {
                constructor: r,
                namespace: c,
                widgetName: i,
                widgetFullName: o
            }), a ? (t.each(a._childConstructors, function(e, i) {
                var s = i.prototype;
                t.widget(s.namespace + "." + s.widgetName, r, i._proto)
            }), delete a._childConstructors) : s._childConstructors.push(r), t.widget.bridge(i, r)
        }, t.widget.extend = function(i) {
            for (var n, o, a = s.call(arguments, 1), r = 0, h = a.length; h > r; r++)
                for (n in a[r]) o = a[r][n], a[r].hasOwnProperty(n) && o !== e && (i[n] = t.isPlainObject(o) ? t.isPlainObject(i[n]) ? t.widget.extend({}, i[n], o) : t.widget.extend({}, o) : o);
            return i
        }, t.widget.bridge = function(i, n) {
            var o = n.prototype.widgetFullName || i;
            t.fn[i] = function(a) {
                var r = "string" == typeof a,
                    h = s.call(arguments, 1),
                    l = this;
                return a = !r && h.length ? t.widget.extend.apply(null, [a].concat(h)) : a, r ? this.each(function() {
                    var s, n = t.data(this, o);
                    return n ? t.isFunction(n[a]) && "_" !== a.charAt(0) ? (s = n[a].apply(n, h), s !== n && s !== e ? (l = s && s.jquery ? l.pushStack(s.get()) : s, !1) : e) : t.error("no such method '" + a + "' for " + i + " widget instance") : t.error("cannot call methods on " + i + " prior to initialization; attempted to call method '" + a + "'")
                }) : this.each(function() {
                    var e = t.data(this, o);
                    e ? e.option(a || {})._init() : t.data(this, o, new n(a, this))
                }), l
            }
        }, t.Widget = function() {}, t.Widget._childConstructors = [], t.Widget.prototype = {
            widgetName: "widget",
            widgetEventPrefix: "",
            defaultElement: "<div>",
            options: {
                disabled: !1,
                create: null
            },
            _createWidget: function(e, s) {
                s = t(s || this.defaultElement || this)[0], this.element = t(s), this.uuid = i++, this.eventNamespace = "." + this.widgetName + this.uuid, this.options = t.widget.extend({}, this.options, this._getCreateOptions(), e), this.bindings = t(), this.hoverable = t(), this.focusable = t(), s !== this && (t.data(s, this.widgetFullName, this), this._on(!0, this.element, {
                    remove: function(t) {
                        t.target === s && this.destroy()
                    }
                }), this.document = t(s.style ? s.ownerDocument : s.document || s), this.window = t(this.document[0].defaultView || this.document[0].parentWindow)), this._create(), this._trigger("create", null, this._getCreateEventData()), this._init()
            },
            _getCreateOptions: t.noop,
            _getCreateEventData: t.noop,
            _create: t.noop,
            _init: t.noop,
            destroy: function() {
                this._destroy(), this.element.unbind(this.eventNamespace).removeData(this.widgetName).removeData(this.widgetFullName).removeData(t.camelCase(this.widgetFullName)), this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName + "-disabled ui-state-disabled"), this.bindings.unbind(this.eventNamespace), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus")
            },
            _destroy: t.noop,
            widget: function() {
                return this.element
            },
            option: function(i, s) {
                var n, o, a, r = i;
                if (0 === arguments.length) return t.widget.extend({}, this.options);
                if ("string" == typeof i)
                    if (r = {}, n = i.split("."), i = n.shift(), n.length) {
                        for (o = r[i] = t.widget.extend({}, this.options[i]), a = 0; n.length - 1 > a; a++) o[n[a]] = o[n[a]] || {}, o = o[n[a]];
                        if (i = n.pop(), 1 === arguments.length) return o[i] === e ? null : o[i];
                        o[i] = s
                    } else {
                        if (1 === arguments.length) return this.options[i] === e ? null : this.options[i];
                        r[i] = s
                    }
                return this._setOptions(r), this
            },
            _setOptions: function(t) {
                var e;
                for (e in t) this._setOption(e, t[e]);
                return this
            },
            _setOption: function(t, e) {
                return this.options[t] = e, "disabled" === t && (this.widget().toggleClass(this.widgetFullName + "-disabled ui-state-disabled", !!e).attr("aria-disabled", e), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus")), this
            },
            enable: function() {
                return this._setOption("disabled", !1)
            },
            disable: function() {
                return this._setOption("disabled", !0)
            },
            _on: function(i, s, n) {
                var o, a = this;
                "boolean" != typeof i && (n = s, s = i, i = !1), n ? (s = o = t(s), this.bindings = this.bindings.add(s)) : (n = s, s = this.element, o = this.widget()), t.each(n, function(n, r) {
                    function h() {
                        return i || a.options.disabled !== !0 && !t(this).hasClass("ui-state-disabled") ? ("string" == typeof r ? a[r] : r).apply(a, arguments) : e
                    }
                    "string" != typeof r && (h.guid = r.guid = r.guid || h.guid || t.guid++);
                    var l = n.match(/^(\w+)\s*(.*)$/),
                        c = l[1] + a.eventNamespace,
                        u = l[2];
                    u ? o.delegate(u, c, h) : s.bind(c, h)
                })
            },
            _off: function(t, e) {
                e = (e || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace, t.unbind(e).undelegate(e)
            },
            _delay: function(t, e) {
                function i() {
                    return ("string" == typeof t ? s[t] : t).apply(s, arguments)
                }
                var s = this;
                return setTimeout(i, e || 0)
            },
            _hoverable: function(e) {
                this.hoverable = this.hoverable.add(e), this._on(e, {
                    mouseenter: function(e) {
                        t(e.currentTarget).addClass("ui-state-hover")
                    },
                    mouseleave: function(e) {
                        t(e.currentTarget).removeClass("ui-state-hover")
                    }
                })
            },
            _focusable: function(e) {
                this.focusable = this.focusable.add(e), this._on(e, {
                    focusin: function(e) {
                        t(e.currentTarget).addClass("ui-state-focus")
                    },
                    focusout: function(e) {
                        t(e.currentTarget).removeClass("ui-state-focus")
                    }
                })
            },
            _trigger: function(e, i, s) {
                var n, o, a = this.options[e];
                if (s = s || {}, i = t.Event(i), i.type = (e === this.widgetEventPrefix ? e : this.widgetEventPrefix + e).toLowerCase(), i.target = this.element[0], o = i.originalEvent)
                    for (n in o) n in i || (i[n] = o[n]);
                return this.element.trigger(i, s), !(t.isFunction(a) && a.apply(this.element[0], [i].concat(s)) === !1 || i.isDefaultPrevented())
            }
        }, t.each({
            show: "fadeIn",
            hide: "fadeOut"
        }, function(e, i) {
            t.Widget.prototype["_" + e] = function(s, n, o) {
                "string" == typeof n && (n = {
                    effect: n
                });
                var a, r = n ? n === !0 || "number" == typeof n ? i : n.effect || i : e;
                n = n || {}, "number" == typeof n && (n = {
                    duration: n
                }), a = !t.isEmptyObject(n), n.complete = o, n.delay && s.delay(n.delay), a && t.effects && t.effects.effect[r] ? s[e](n) : r !== e && s[r] ? s[r](n.duration, n.easing, o) : s.queue(function(i) {
                    t(this)[e](), o && o.call(s[0]), i()
                })
            }
        })
    }(jQuery),
    function(t) {
        var e = !1;
        t(document).mouseup(function() {
            e = !1
        }), t.widget("ui.mouse", {
            version: "1.10.4",
            options: {
                cancel: "input,textarea,button,select,option",
                distance: 1,
                delay: 0
            },
            _mouseInit: function() {
                var e = this;
                this.element.bind("mousedown." + this.widgetName, function(t) {
                    return e._mouseDown(t)
                }).bind("click." + this.widgetName, function(i) {
                    return !0 === t.data(i.target, e.widgetName + ".preventClickEvent") ? (t.removeData(i.target, e.widgetName + ".preventClickEvent"), i.stopImmediatePropagation(), !1) : void 0
                }), this.started = !1
            },
            _mouseDestroy: function() {
                this.element.unbind("." + this.widgetName), this._mouseMoveDelegate && t(document).unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate)
            },
            _mouseDown: function(i) {
                if (!e) {
                    this._mouseStarted && this._mouseUp(i), this._mouseDownEvent = i;
                    var s = this,
                        n = 1 === i.which,
                        a = "string" == typeof this.options.cancel && i.target.nodeName ? t(i.target).closest(this.options.cancel).length : !1;
                    return n && !a && this._mouseCapture(i) ? (this.mouseDelayMet = !this.options.delay, this.mouseDelayMet || (this._mouseDelayTimer = setTimeout(function() {
                        s.mouseDelayMet = !0
                    }, this.options.delay)), this._mouseDistanceMet(i) && this._mouseDelayMet(i) && (this._mouseStarted = this._mouseStart(i) !== !1, !this._mouseStarted) ? (i.preventDefault(), !0) : (!0 === t.data(i.target, this.widgetName + ".preventClickEvent") && t.removeData(i.target, this.widgetName + ".preventClickEvent"), this._mouseMoveDelegate = function(t) {
                        return s._mouseMove(t)
                    }, this._mouseUpDelegate = function(t) {
                        return s._mouseUp(t)
                    }, t(document).bind("mousemove." + this.widgetName, this._mouseMoveDelegate).bind("mouseup." + this.widgetName, this._mouseUpDelegate), i.preventDefault(), e = !0, !0)) : !0
                }
            },
            _mouseMove: function(e) {
                return t.ui.ie && (!document.documentMode || 9 > document.documentMode) && !e.button ? this._mouseUp(e) : this._mouseStarted ? (this._mouseDrag(e), e.preventDefault()) : (this._mouseDistanceMet(e) && this._mouseDelayMet(e) && (this._mouseStarted = this._mouseStart(this._mouseDownEvent, e) !== !1, this._mouseStarted ? this._mouseDrag(e) : this._mouseUp(e)), !this._mouseStarted)
            },
            _mouseUp: function(e) {
                return t(document).unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate), this._mouseStarted && (this._mouseStarted = !1, e.target === this._mouseDownEvent.target && t.data(e.target, this.widgetName + ".preventClickEvent", !0), this._mouseStop(e)), !1
            },
            _mouseDistanceMet: function(t) {
                return Math.max(Math.abs(this._mouseDownEvent.pageX - t.pageX), Math.abs(this._mouseDownEvent.pageY - t.pageY)) >= this.options.distance
            },
            _mouseDelayMet: function() {
                return this.mouseDelayMet
            },
            _mouseStart: function() {},
            _mouseDrag: function() {},
            _mouseStop: function() {},
            _mouseCapture: function() {
                return !0
            }
        })
    }(jQuery),
    function(t, e) {
        function i(t, e, i) {
            return [parseFloat(t[0]) * (p.test(t[0]) ? e / 100 : 1), parseFloat(t[1]) * (p.test(t[1]) ? i / 100 : 1)]
        }

        function s(e, i) {
            return parseInt(t.css(e, i), 10) || 0
        }

        function n(e) {
            var i = e[0];
            return 9 === i.nodeType ? {
                width: e.width(),
                height: e.height(),
                offset: {
                    top: 0,
                    left: 0
                }
            } : t.isWindow(i) ? {
                width: e.width(),
                height: e.height(),
                offset: {
                    top: e.scrollTop(),
                    left: e.scrollLeft()
                }
            } : i.preventDefault ? {
                width: 0,
                height: 0,
                offset: {
                    top: i.pageY,
                    left: i.pageX
                }
            } : {
                width: e.outerWidth(),
                height: e.outerHeight(),
                offset: e.offset()
            }
        }
        t.ui = t.ui || {};
        var a, o = Math.max,
            r = Math.abs,
            l = Math.round,
            h = /left|center|right/,
            c = /top|center|bottom/,
            u = /[\+\-]\d+(\.[\d]+)?%?/,
            d = /^\w+/,
            p = /%$/,
            f = t.fn.position;
        t.position = {
                scrollbarWidth: function() {
                    if (a !== e) return a;
                    var i, s, n = t("<div style='display:block;position:absolute;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"),
                        o = n.children()[0];
                    return t("body").append(n), i = o.offsetWidth, n.css("overflow", "scroll"), s = o.offsetWidth, i === s && (s = n[0].clientWidth), n.remove(), a = i - s
                },
                getScrollInfo: function(e) {
                    var i = e.isWindow || e.isDocument ? "" : e.element.css("overflow-x"),
                        s = e.isWindow || e.isDocument ? "" : e.element.css("overflow-y"),
                        n = "scroll" === i || "auto" === i && e.width < e.element[0].scrollWidth,
                        a = "scroll" === s || "auto" === s && e.height < e.element[0].scrollHeight;
                    return {
                        width: a ? t.position.scrollbarWidth() : 0,
                        height: n ? t.position.scrollbarWidth() : 0
                    }
                },
                getWithinInfo: function(e) {
                    var i = t(e || window),
                        s = t.isWindow(i[0]),
                        n = !!i[0] && 9 === i[0].nodeType;
                    return {
                        element: i,
                        isWindow: s,
                        isDocument: n,
                        offset: i.offset() || {
                            left: 0,
                            top: 0
                        },
                        scrollLeft: i.scrollLeft(),
                        scrollTop: i.scrollTop(),
                        width: s ? i.width() : i.outerWidth(),
                        height: s ? i.height() : i.outerHeight()
                    }
                }
            }, t.fn.position = function(e) {
                if (!e || !e.of) return f.apply(this, arguments);
                e = t.extend({}, e);
                var a, p, g, m, v, _, b = t(e.of),
                    y = t.position.getWithinInfo(e.within),
                    k = t.position.getScrollInfo(y),
                    w = (e.collision || "flip").split(" "),
                    D = {};
                return _ = n(b), b[0].preventDefault && (e.at = "left top"), p = _.width, g = _.height, m = _.offset, v = t.extend({}, m), t.each(["my", "at"], function() {
                    var t, i, s = (e[this] || "").split(" ");
                    1 === s.length && (s = h.test(s[0]) ? s.concat(["center"]) : c.test(s[0]) ? ["center"].concat(s) : ["center", "center"]), s[0] = h.test(s[0]) ? s[0] : "center", s[1] = c.test(s[1]) ? s[1] : "center", t = u.exec(s[0]), i = u.exec(s[1]), D[this] = [t ? t[0] : 0, i ? i[0] : 0], e[this] = [d.exec(s[0])[0], d.exec(s[1])[0]]
                }), 1 === w.length && (w[1] = w[0]), "right" === e.at[0] ? v.left += p : "center" === e.at[0] && (v.left += p / 2), "bottom" === e.at[1] ? v.top += g : "center" === e.at[1] && (v.top += g / 2), a = i(D.at, p, g), v.left += a[0], v.top += a[1], this.each(function() {
                    var n, h, c = t(this),
                        u = c.outerWidth(),
                        d = c.outerHeight(),
                        f = s(this, "marginLeft"),
                        _ = s(this, "marginTop"),
                        x = u + f + s(this, "marginRight") + k.width,
                        C = d + _ + s(this, "marginBottom") + k.height,
                        M = t.extend({}, v),
                        T = i(D.my, c.outerWidth(), c.outerHeight());
                    "right" === e.my[0] ? M.left -= u : "center" === e.my[0] && (M.left -= u / 2), "bottom" === e.my[1] ? M.top -= d : "center" === e.my[1] && (M.top -= d / 2), M.left += T[0], M.top += T[1], t.support.offsetFractions || (M.left = l(M.left), M.top = l(M.top)), n = {
                        marginLeft: f,
                        marginTop: _
                    }, t.each(["left", "top"], function(i, s) {
                        t.ui.position[w[i]] && t.ui.position[w[i]][s](M, {
                            targetWidth: p,
                            targetHeight: g,
                            elemWidth: u,
                            elemHeight: d,
                            collisionPosition: n,
                            collisionWidth: x,
                            collisionHeight: C,
                            offset: [a[0] + T[0], a[1] + T[1]],
                            my: e.my,
                            at: e.at,
                            within: y,
                            elem: c
                        })
                    }), e.using && (h = function(t) {
                        var i = m.left - M.left,
                            s = i + p - u,
                            n = m.top - M.top,
                            a = n + g - d,
                            l = {
                                target: {
                                    element: b,
                                    left: m.left,
                                    top: m.top,
                                    width: p,
                                    height: g
                                },
                                element: {
                                    element: c,
                                    left: M.left,
                                    top: M.top,
                                    width: u,
                                    height: d
                                },
                                horizontal: 0 > s ? "left" : i > 0 ? "right" : "center",
                                vertical: 0 > a ? "top" : n > 0 ? "bottom" : "middle"
                            };
                        u > p && p > r(i + s) && (l.horizontal = "center"), d > g && g > r(n + a) && (l.vertical = "middle"), l.important = o(r(i), r(s)) > o(r(n), r(a)) ? "horizontal" : "vertical", e.using.call(this, t, l)
                    }), c.offset(t.extend(M, {
                        using: h
                    }))
                })
            }, t.ui.position = {
                fit: {
                    left: function(t, e) {
                        var i, s = e.within,
                            n = s.isWindow ? s.scrollLeft : s.offset.left,
                            a = s.width,
                            r = t.left - e.collisionPosition.marginLeft,
                            l = n - r,
                            h = r + e.collisionWidth - a - n;
                        e.collisionWidth > a ? l > 0 && 0 >= h ? (i = t.left + l + e.collisionWidth - a - n, t.left += l - i) : t.left = h > 0 && 0 >= l ? n : l > h ? n + a - e.collisionWidth : n : l > 0 ? t.left += l : h > 0 ? t.left -= h : t.left = o(t.left - r, t.left)
                    },
                    top: function(t, e) {
                        var i, s = e.within,
                            n = s.isWindow ? s.scrollTop : s.offset.top,
                            a = e.within.height,
                            r = t.top - e.collisionPosition.marginTop,
                            l = n - r,
                            h = r + e.collisionHeight - a - n;
                        e.collisionHeight > a ? l > 0 && 0 >= h ? (i = t.top + l + e.collisionHeight - a - n, t.top += l - i) : t.top = h > 0 && 0 >= l ? n : l > h ? n + a - e.collisionHeight : n : l > 0 ? t.top += l : h > 0 ? t.top -= h : t.top = o(t.top - r, t.top)
                    }
                },
                flip: {
                    left: function(t, e) {
                        var i, s, n = e.within,
                            a = n.offset.left + n.scrollLeft,
                            o = n.width,
                            l = n.isWindow ? n.scrollLeft : n.offset.left,
                            h = t.left - e.collisionPosition.marginLeft,
                            c = h - l,
                            u = h + e.collisionWidth - o - l,
                            d = "left" === e.my[0] ? -e.elemWidth : "right" === e.my[0] ? e.elemWidth : 0,
                            p = "left" === e.at[0] ? e.targetWidth : "right" === e.at[0] ? -e.targetWidth : 0,
                            f = -2 * e.offset[0];
                        0 > c ? (i = t.left + d + p + f + e.collisionWidth - o - a, (0 > i || r(c) > i) && (t.left += d + p + f)) : u > 0 && (s = t.left - e.collisionPosition.marginLeft + d + p + f - l, (s > 0 || u > r(s)) && (t.left += d + p + f))
                    },
                    top: function(t, e) {
                        var i, s, n = e.within,
                            a = n.offset.top + n.scrollTop,
                            o = n.height,
                            l = n.isWindow ? n.scrollTop : n.offset.top,
                            h = t.top - e.collisionPosition.marginTop,
                            c = h - l,
                            u = h + e.collisionHeight - o - l,
                            d = "top" === e.my[1],
                            p = d ? -e.elemHeight : "bottom" === e.my[1] ? e.elemHeight : 0,
                            f = "top" === e.at[1] ? e.targetHeight : "bottom" === e.at[1] ? -e.targetHeight : 0,
                            g = -2 * e.offset[1];
                        0 > c ? (s = t.top + p + f + g + e.collisionHeight - o - a, t.top + p + f + g > c && (0 > s || r(c) > s) && (t.top += p + f + g)) : u > 0 && (i = t.top - e.collisionPosition.marginTop + p + f + g - l, t.top + p + f + g > u && (i > 0 || u > r(i)) && (t.top += p + f + g))
                    }
                },
                flipfit: {
                    left: function() {
                        t.ui.position.flip.left.apply(this, arguments), t.ui.position.fit.left.apply(this, arguments)
                    },
                    top: function() {
                        t.ui.position.flip.top.apply(this, arguments), t.ui.position.fit.top.apply(this, arguments)
                    }
                }
            },
            function() {
                var e, i, s, n, a, o = document.getElementsByTagName("body")[0],
                    r = document.createElement("div");
                e = document.createElement(o ? "div" : "body"), s = {
                    visibility: "hidden",
                    width: 0,
                    height: 0,
                    border: 0,
                    margin: 0,
                    background: "none"
                }, o && t.extend(s, {
                    position: "absolute",
                    left: "-1000px",
                    top: "-1000px"
                });
                for (a in s) e.style[a] = s[a];
                e.appendChild(r), i = o || document.documentElement, i.insertBefore(e, i.firstChild), r.style.cssText = "position: absolute; left: 10.7432222px;", n = t(r).offset().left, t.support.offsetFractions = n > 10 && 11 > n, e.innerHTML = "", i.removeChild(e)
            }()
    }(jQuery),
    function(e, t) {
        function i() {
            this._curInst = null, this._keyEvent = !1, this._disabledInputs = [], this._datepickerShowing = !1, this._inDialog = !1, this._mainDivId = "ui-datepicker-div", this._inlineClass = "ui-datepicker-inline", this._appendClass = "ui-datepicker-append", this._triggerClass = "ui-datepicker-trigger", this._dialogClass = "ui-datepicker-dialog", this._disableClass = "ui-datepicker-disabled", this._unselectableClass = "ui-datepicker-unselectable", this._currentClass = "ui-datepicker-current-day", this._dayOverClass = "ui-datepicker-days-cell-over", this.regional = [], this.regional[""] = {
                closeText: "Done",
                prevText: "Prev",
                nextText: "Next",
                currentText: "Today",
                monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                dayNamesMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
                weekHeader: "Wk",
                dateFormat: "mm/dd/yy",
                firstDay: 0,
                isRTL: !1,
                showMonthAfterYear: !1,
                yearSuffix: ""
            }, this._defaults = {
                showOn: "focus",
                showAnim: "fadeIn",
                showOptions: {},
                defaultDate: null,
                appendText: "",
                buttonText: "...",
                buttonImage: "",
                buttonImageOnly: !1,
                hideIfNoPrevNext: !1,
                navigationAsDateFormat: !1,
                gotoCurrent: !1,
                changeMonth: !1,
                changeYear: !1,
                yearRange: "c-10:c+10",
                showOtherMonths: !1,
                selectOtherMonths: !1,
                showWeek: !1,
                calculateWeek: this.iso8601Week,
                shortYearCutoff: "+10",
                minDate: null,
                maxDate: null,
                duration: "fast",
                beforeShowDay: null,
                beforeShow: null,
                onSelect: null,
                onChangeMonthYear: null,
                onClose: null,
                numberOfMonths: 1,
                showCurrentAtPos: 0,
                stepMonths: 1,
                stepBigMonths: 12,
                altField: "",
                altFormat: "",
                constrainInput: !0,
                showButtonPanel: !1,
                autoSize: !1,
                disabled: !1
            }, e.extend(this._defaults, this.regional[""]), this.dpDiv = a(e("<div id='" + this._mainDivId + "' class='ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>"))
        }

        function a(t) {
            var i = "button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a";
            return t.delegate(i, "mouseout", function() {
                e(this).removeClass("ui-state-hover"), -1 !== this.className.indexOf("ui-datepicker-prev") && e(this).removeClass("ui-datepicker-prev-hover"), -1 !== this.className.indexOf("ui-datepicker-next") && e(this).removeClass("ui-datepicker-next-hover")
            }).delegate(i, "mouseover", function() {
                e.datepicker._isDisabledDatepicker(n.inline ? t.parent()[0] : n.input[0]) || (e(this).parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover"), e(this).addClass("ui-state-hover"), -1 !== this.className.indexOf("ui-datepicker-prev") && e(this).addClass("ui-datepicker-prev-hover"), -1 !== this.className.indexOf("ui-datepicker-next") && e(this).addClass("ui-datepicker-next-hover"))
            })
        }

        function s(t, i) {
            e.extend(t, i);
            for (var a in i) null == i[a] && (t[a] = i[a]);
            return t
        }
        e.extend(e.ui, {
            datepicker: {
                version: "1.10.4"
            }
        });
        var n, r = "datepicker";
        e.extend(i.prototype, {
            markerClassName: "hasDatepicker",
            maxRows: 4,
            _widgetDatepicker: function() {
                return this.dpDiv
            },
            setDefaults: function(e) {
                return s(this._defaults, e || {}), this
            },
            _attachDatepicker: function(t, i) {
                var a, s, n;
                a = t.nodeName.toLowerCase(), s = "div" === a || "span" === a, t.id || (this.uuid += 1, t.id = "dp" + this.uuid), n = this._newInst(e(t), s), n.settings = e.extend({}, i || {}), "input" === a ? this._connectDatepicker(t, n) : s && this._inlineDatepicker(t, n)
            },
            _newInst: function(t, i) {
                var s = t[0].id.replace(/([^A-Za-z0-9_\-])/g, "\\\\$1");
                return {
                    id: s,
                    input: t,
                    selectedDay: 0,
                    selectedMonth: 0,
                    selectedYear: 0,
                    drawMonth: 0,
                    drawYear: 0,
                    inline: i,
                    dpDiv: i ? a(e("<div class='" + this._inlineClass + " ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>")) : this.dpDiv
                }
            },
            _connectDatepicker: function(t, i) {
                var a = e(t);
                i.append = e([]), i.trigger = e([]), a.hasClass(this.markerClassName) || (this._attachments(a, i), a.addClass(this.markerClassName).keydown(this._doKeyDown).keypress(this._doKeyPress).keyup(this._doKeyUp), this._autoSize(i), e.data(t, r, i), i.settings.disabled && this._disableDatepicker(t))
            },
            _attachments: function(t, i) {
                var a, s, n, r = this._get(i, "appendText"),
                    o = this._get(i, "isRTL");
                i.append && i.append.remove(), r && (i.append = e("<span class='" + this._appendClass + "'>" + r + "</span>"), t[o ? "before" : "after"](i.append)), t.unbind("focus", this._showDatepicker), i.trigger && i.trigger.remove(), a = this._get(i, "showOn"), ("focus" === a || "both" === a) && t.focus(this._showDatepicker), ("button" === a || "both" === a) && (s = this._get(i, "buttonText"), n = this._get(i, "buttonImage"), i.trigger = e(this._get(i, "buttonImageOnly") ? e("<img/>").addClass(this._triggerClass).attr({
                    src: n,
                    alt: s,
                    title: s
                }) : e("<button type='button'></button>").addClass(this._triggerClass).html(n ? e("<img/>").attr({
                    src: n,
                    alt: s,
                    title: s
                }) : s)), t[o ? "before" : "after"](i.trigger), i.trigger.click(function() {
                    return e.datepicker._datepickerShowing && e.datepicker._lastInput === t[0] ? e.datepicker._hideDatepicker() : e.datepicker._datepickerShowing && e.datepicker._lastInput !== t[0] ? (e.datepicker._hideDatepicker(), e.datepicker._showDatepicker(t[0])) : e.datepicker._showDatepicker(t[0]), !1
                }))
            },
            _autoSize: function(e) {
                if (this._get(e, "autoSize") && !e.inline) {
                    var t, i, a, s, n = new Date(2009, 11, 20),
                        r = this._get(e, "dateFormat");
                    r.match(/[DM]/) && (t = function(e) {
                        for (i = 0, a = 0, s = 0; e.length > s; s++) e[s].length > i && (i = e[s].length, a = s);
                        return a
                    }, n.setMonth(t(this._get(e, r.match(/MM/) ? "monthNames" : "monthNamesShort"))), n.setDate(t(this._get(e, r.match(/DD/) ? "dayNames" : "dayNamesShort")) + 20 - n.getDay())), e.input.attr("size", this._formatDate(e, n).length)
                }
            },
            _inlineDatepicker: function(t, i) {
                var a = e(t);
                a.hasClass(this.markerClassName) || (a.addClass(this.markerClassName).append(i.dpDiv), e.data(t, r, i), this._setDate(i, this._getDefaultDate(i), !0), this._updateDatepicker(i), this._updateAlternate(i), i.settings.disabled && this._disableDatepicker(t), i.dpDiv.css("display", "block"))
            },
            _dialogDatepicker: function(t, i, a, n, o) {
                var u, c, h, l, d, p = this._dialogInst;
                return p || (this.uuid += 1, u = "dp" + this.uuid, this._dialogInput = e("<input type='text' id='" + u + "' style='position: absolute; top: -100px; width: 0px;'/>"), this._dialogInput.keydown(this._doKeyDown), e("body").append(this._dialogInput), p = this._dialogInst = this._newInst(this._dialogInput, !1), p.settings = {}, e.data(this._dialogInput[0], r, p)), s(p.settings, n || {}), i = i && i.constructor === Date ? this._formatDate(p, i) : i, this._dialogInput.val(i), this._pos = o ? o.length ? o : [o.pageX, o.pageY] : null, this._pos || (c = document.documentElement.clientWidth, h = document.documentElement.clientHeight, l = document.documentElement.scrollLeft || document.body.scrollLeft, d = document.documentElement.scrollTop || document.body.scrollTop, this._pos = [c / 2 - 100 + l, h / 2 - 150 + d]), this._dialogInput.css("left", this._pos[0] + 20 + "px").css("top", this._pos[1] + "px"), p.settings.onSelect = a, this._inDialog = !0, this.dpDiv.addClass(this._dialogClass), this._showDatepicker(this._dialogInput[0]), e.blockUI && e.blockUI(this.dpDiv), e.data(this._dialogInput[0], r, p), this
            },
            _destroyDatepicker: function(t) {
                var i, a = e(t),
                    s = e.data(t, r);
                a.hasClass(this.markerClassName) && (i = t.nodeName.toLowerCase(), e.removeData(t, r), "input" === i ? (s.append.remove(), s.trigger.remove(), a.removeClass(this.markerClassName).unbind("focus", this._showDatepicker).unbind("keydown", this._doKeyDown).unbind("keypress", this._doKeyPress).unbind("keyup", this._doKeyUp)) : ("div" === i || "span" === i) && a.removeClass(this.markerClassName).empty())
            },
            _enableDatepicker: function(t) {
                var i, a, s = e(t),
                    n = e.data(t, r);
                s.hasClass(this.markerClassName) && (i = t.nodeName.toLowerCase(), "input" === i ? (t.disabled = !1, n.trigger.filter("button").each(function() {
                    this.disabled = !1
                }).end().filter("img").css({
                    opacity: "1.0",
                    cursor: ""
                })) : ("div" === i || "span" === i) && (a = s.children("." + this._inlineClass), a.children().removeClass("ui-state-disabled"), a.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", !1)), this._disabledInputs = e.map(this._disabledInputs, function(e) {
                    return e === t ? null : e
                }))
            },
            _disableDatepicker: function(t) {
                var i, a, s = e(t),
                    n = e.data(t, r);
                s.hasClass(this.markerClassName) && (i = t.nodeName.toLowerCase(), "input" === i ? (t.disabled = !0, n.trigger.filter("button").each(function() {
                    this.disabled = !0
                }).end().filter("img").css({
                    opacity: "0.5",
                    cursor: "default"
                })) : ("div" === i || "span" === i) && (a = s.children("." + this._inlineClass), a.children().addClass("ui-state-disabled"), a.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", !0)), this._disabledInputs = e.map(this._disabledInputs, function(e) {
                    return e === t ? null : e
                }), this._disabledInputs[this._disabledInputs.length] = t)
            },
            _isDisabledDatepicker: function(e) {
                if (!e) return !1;
                for (var t = 0; this._disabledInputs.length > t; t++)
                    if (this._disabledInputs[t] === e) return !0;
                return !1
            },
            _getInst: function(t) {
                try {
                    return e.data(t, r)
                } catch (i) {
                    throw "Missing instance data for this datepicker"
                }
            },
            _optionDatepicker: function(i, a, n) {
                var r, o, u, c, h = this._getInst(i);
                return 2 === arguments.length && "string" == typeof a ? "defaults" === a ? e.extend({}, e.datepicker._defaults) : h ? "all" === a ? e.extend({}, h.settings) : this._get(h, a) : null : (r = a || {}, "string" == typeof a && (r = {}, r[a] = n), h && (this._curInst === h && this._hideDatepicker(), o = this._getDateDatepicker(i, !0), u = this._getMinMaxDate(h, "min"), c = this._getMinMaxDate(h, "max"), s(h.settings, r), null !== u && r.dateFormat !== t && r.minDate === t && (h.settings.minDate = this._formatDate(h, u)), null !== c && r.dateFormat !== t && r.maxDate === t && (h.settings.maxDate = this._formatDate(h, c)), "disabled" in r && (r.disabled ? this._disableDatepicker(i) : this._enableDatepicker(i)), this._attachments(e(i), h), this._autoSize(h), this._setDate(h, o), this._updateAlternate(h), this._updateDatepicker(h)), t)
            },
            _changeDatepicker: function(e, t, i) {
                this._optionDatepicker(e, t, i)
            },
            _refreshDatepicker: function(e) {
                var t = this._getInst(e);
                t && this._updateDatepicker(t)
            },
            _setDateDatepicker: function(e, t) {
                var i = this._getInst(e);
                i && (this._setDate(i, t), this._updateDatepicker(i), this._updateAlternate(i))
            },
            _getDateDatepicker: function(e, t) {
                var i = this._getInst(e);
                return i && !i.inline && this._setDateFromField(i, t), i ? this._getDate(i) : null
            },
            _doKeyDown: function(t) {
                var i, a, s, n = e.datepicker._getInst(t.target),
                    r = !0,
                    o = n.dpDiv.is(".ui-datepicker-rtl");
                if (n._keyEvent = !0, e.datepicker._datepickerShowing) switch (t.keyCode) {
                    case 9:
                        e.datepicker._hideDatepicker(), r = !1;
                        break;
                    case 13:
                        return s = e("td." + e.datepicker._dayOverClass + ":not(." + e.datepicker._currentClass + ")", n.dpDiv), s[0] && e.datepicker._selectDay(t.target, n.selectedMonth, n.selectedYear, s[0]), i = e.datepicker._get(n, "onSelect"), i ? (a = e.datepicker._formatDate(n), i.apply(n.input ? n.input[0] : null, [a, n])) : e.datepicker._hideDatepicker(), !1;
                    case 27:
                        e.datepicker._hideDatepicker();
                        break;
                    case 33:
                        e.datepicker._adjustDate(t.target, t.ctrlKey ? -e.datepicker._get(n, "stepBigMonths") : -e.datepicker._get(n, "stepMonths"), "M");
                        break;
                    case 34:
                        e.datepicker._adjustDate(t.target, t.ctrlKey ? +e.datepicker._get(n, "stepBigMonths") : +e.datepicker._get(n, "stepMonths"), "M");
                        break;
                    case 35:
                        (t.ctrlKey || t.metaKey) && e.datepicker._clearDate(t.target), r = t.ctrlKey || t.metaKey;
                        break;
                    case 36:
                        (t.ctrlKey || t.metaKey) && e.datepicker._gotoToday(t.target), r = t.ctrlKey || t.metaKey;
                        break;
                    case 37:
                        (t.ctrlKey || t.metaKey) && e.datepicker._adjustDate(t.target, o ? 1 : -1, "D"), r = t.ctrlKey || t.metaKey, t.originalEvent.altKey && e.datepicker._adjustDate(t.target, t.ctrlKey ? -e.datepicker._get(n, "stepBigMonths") : -e.datepicker._get(n, "stepMonths"), "M");
                        break;
                    case 38:
                        (t.ctrlKey || t.metaKey) && e.datepicker._adjustDate(t.target, -7, "D"), r = t.ctrlKey || t.metaKey;
                        break;
                    case 39:
                        (t.ctrlKey || t.metaKey) && e.datepicker._adjustDate(t.target, o ? -1 : 1, "D"), r = t.ctrlKey || t.metaKey, t.originalEvent.altKey && e.datepicker._adjustDate(t.target, t.ctrlKey ? +e.datepicker._get(n, "stepBigMonths") : +e.datepicker._get(n, "stepMonths"), "M");
                        break;
                    case 40:
                        (t.ctrlKey || t.metaKey) && e.datepicker._adjustDate(t.target, 7, "D"), r = t.ctrlKey || t.metaKey;
                        break;
                    default:
                        r = !1
                } else 36 === t.keyCode && t.ctrlKey ? e.datepicker._showDatepicker(this) : r = !1;
                r && (t.preventDefault(), t.stopPropagation())
            },
            _doKeyPress: function(i) {
                var a, s, n = e.datepicker._getInst(i.target);
                return e.datepicker._get(n, "constrainInput") ? (a = e.datepicker._possibleChars(e.datepicker._get(n, "dateFormat")), s = String.fromCharCode(null == i.charCode ? i.keyCode : i.charCode), i.ctrlKey || i.metaKey || " " > s || !a || a.indexOf(s) > -1) : t
            },
            _doKeyUp: function(t) {
                var i, a = e.datepicker._getInst(t.target);
                if (a.input.val() !== a.lastVal) try {
                    i = e.datepicker.parseDate(e.datepicker._get(a, "dateFormat"), a.input ? a.input.val() : null, e.datepicker._getFormatConfig(a)), i && (e.datepicker._setDateFromField(a), e.datepicker._updateAlternate(a), e.datepicker._updateDatepicker(a))
                } catch (s) {}
                return !0
            },
            _showDatepicker: function(t) {
                if (t = t.target || t, "input" !== t.nodeName.toLowerCase() && (t = e("input", t.parentNode)[0]), !e.datepicker._isDisabledDatepicker(t) && e.datepicker._lastInput !== t) {
                    var i, a, n, r, o, u, c;
                    i = e.datepicker._getInst(t), e.datepicker._curInst && e.datepicker._curInst !== i && (e.datepicker._curInst.dpDiv.stop(!0, !0), i && e.datepicker._datepickerShowing && e.datepicker._hideDatepicker(e.datepicker._curInst.input[0])), a = e.datepicker._get(i, "beforeShow"), n = a ? a.apply(t, [t, i]) : {}, n !== !1 && (s(i.settings, n), i.lastVal = null, e.datepicker._lastInput = t, e.datepicker._setDateFromField(i), e.datepicker._inDialog && (t.value = ""), e.datepicker._pos || (e.datepicker._pos = e.datepicker._findPos(t), e.datepicker._pos[1] += t.offsetHeight), r = !1, e(t).parents().each(function() {
                        return r |= "fixed" === e(this).css("position"), !r
                    }), o = {
                        left: e.datepicker._pos[0],
                        top: e.datepicker._pos[1]
                    }, e.datepicker._pos = null, i.dpDiv.empty(), i.dpDiv.css({
                        position: "absolute",
                        display: "block",
                        top: "-1000px"
                    }), e.datepicker._updateDatepicker(i), o = e.datepicker._checkOffset(i, o, r), i.dpDiv.css({
                        position: e.datepicker._inDialog && e.blockUI ? "static" : r ? "fixed" : "absolute",
                        display: "none",
                        left: o.left + "px",
                        top: o.top + "px"
                    }), i.inline || (u = e.datepicker._get(i, "showAnim"), c = e.datepicker._get(i, "duration"), i.dpDiv.zIndex(e(t).zIndex() + 1), e.datepicker._datepickerShowing = !0, e.effects && e.effects.effect[u] ? i.dpDiv.show(u, e.datepicker._get(i, "showOptions"), c) : i.dpDiv[u || "show"](u ? c : null), e.datepicker._shouldFocusInput(i) && i.input.focus(), e.datepicker._curInst = i))
                }
            },
            _updateDatepicker: function(t) {
                this.maxRows = 4, n = t, t.dpDiv.empty().append(this._generateHTML(t)), this._attachHandlers(t), t.dpDiv.find("." + this._dayOverClass + " a").mouseover();
                var i, a = this._getNumberOfMonths(t),
                    s = a[1],
                    r = 17;
                t.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width(""), s > 1 && t.dpDiv.addClass("ui-datepicker-multi-" + s).css("width", r * s + "em"), t.dpDiv[(1 !== a[0] || 1 !== a[1] ? "add" : "remove") + "Class"]("ui-datepicker-multi"), t.dpDiv[(this._get(t, "isRTL") ? "add" : "remove") + "Class"]("ui-datepicker-rtl"), t === e.datepicker._curInst && e.datepicker._datepickerShowing && e.datepicker._shouldFocusInput(t) && t.input.focus(), t.yearshtml && (i = t.yearshtml, setTimeout(function() {
                    i === t.yearshtml && t.yearshtml && t.dpDiv.find("select.ui-datepicker-year:first").replaceWith(t.yearshtml), i = t.yearshtml = null
                }, 0))
            },
            _shouldFocusInput: function(e) {
                return e.input && e.input.is(":visible") && !e.input.is(":disabled") && !e.input.is(":focus")
            },
            _checkOffset: function(t, i, a) {
                var s = t.dpDiv.outerWidth(),
                    n = t.dpDiv.outerHeight(),
                    r = t.input ? t.input.outerWidth() : 0,
                    o = t.input ? t.input.outerHeight() : 0,
                    u = document.documentElement.clientWidth + (a ? 0 : e(document).scrollLeft()),
                    c = document.documentElement.clientHeight + (a ? 0 : e(document).scrollTop());
                return i.left -= this._get(t, "isRTL") ? s - r : 0, i.left -= a && i.left === t.input.offset().left ? e(document).scrollLeft() : 0, i.top -= a && i.top === t.input.offset().top + o ? e(document).scrollTop() : 0, i.left -= Math.min(i.left, i.left + s > u && u > s ? Math.abs(i.left + s - u) : 0), i.top -= Math.min(i.top, i.top + n > c && c > n ? Math.abs(n + o) : 0), i
            },
            _findPos: function(t) {
                for (var i, a = this._getInst(t), s = this._get(a, "isRTL"); t && ("hidden" === t.type || 1 !== t.nodeType || e.expr.filters.hidden(t));) t = t[s ? "previousSibling" : "nextSibling"];
                return i = e(t).offset(), [i.left, i.top]
            },
            _hideDatepicker: function(t) {
                var i, a, s, n, o = this._curInst;
                !o || t && o !== e.data(t, r) || this._datepickerShowing && (i = this._get(o, "showAnim"), a = this._get(o, "duration"), s = function() {
                    e.datepicker._tidyDialog(o)
                }, e.effects && (e.effects.effect[i] || e.effects[i]) ? o.dpDiv.hide(i, e.datepicker._get(o, "showOptions"), a, s) : o.dpDiv["slideDown" === i ? "slideUp" : "fadeIn" === i ? "fadeOut" : "hide"](i ? a : null, s), i || s(), this._datepickerShowing = !1, n = this._get(o, "onClose"), n && n.apply(o.input ? o.input[0] : null, [o.input ? o.input.val() : "", o]), this._lastInput = null, this._inDialog && (this._dialogInput.css({
                    position: "absolute",
                    left: "0",
                    top: "-100px"
                }), e.blockUI && (e.unblockUI(), e("body").append(this.dpDiv))), this._inDialog = !1)
            },
            _tidyDialog: function(e) {
                e.dpDiv.removeClass(this._dialogClass).unbind(".ui-datepicker-calendar")
            },
            _checkExternalClick: function(t) {
                if (e.datepicker._curInst) {
                    var i = e(t.target),
                        a = e.datepicker._getInst(i[0]);
                    (i[0].id !== e.datepicker._mainDivId && 0 === i.parents("#" + e.datepicker._mainDivId).length && !i.hasClass(e.datepicker.markerClassName) && !i.closest("." + e.datepicker._triggerClass).length && e.datepicker._datepickerShowing && (!e.datepicker._inDialog || !e.blockUI) || i.hasClass(e.datepicker.markerClassName) && e.datepicker._curInst !== a) && e.datepicker._hideDatepicker()
                }
            },
            _adjustDate: function(t, i, a) {
                var s = e(t),
                    n = this._getInst(s[0]);
                this._isDisabledDatepicker(s[0]) || (this._adjustInstDate(n, i + ("M" === a ? this._get(n, "showCurrentAtPos") : 0), a), this._updateDatepicker(n))
            },
            _gotoToday: function(t) {
                var i, a = e(t),
                    s = this._getInst(a[0]);
                this._get(s, "gotoCurrent") && s.currentDay ? (s.selectedDay = s.currentDay, s.drawMonth = s.selectedMonth = s.currentMonth, s.drawYear = s.selectedYear = s.currentYear) : (i = new Date, s.selectedDay = i.getDate(), s.drawMonth = s.selectedMonth = i.getMonth(), s.drawYear = s.selectedYear = i.getFullYear()), this._notifyChange(s), this._adjustDate(a)
            },
            _selectMonthYear: function(t, i, a) {
                var s = e(t),
                    n = this._getInst(s[0]);
                n["selected" + ("M" === a ? "Month" : "Year")] = n["draw" + ("M" === a ? "Month" : "Year")] = parseInt(i.options[i.selectedIndex].value, 10), this._notifyChange(n), this._adjustDate(s)
            },
            _selectDay: function(t, i, a, s) {
                var n, r = e(t);
                e(s).hasClass(this._unselectableClass) || this._isDisabledDatepicker(r[0]) || (n = this._getInst(r[0]), n.selectedDay = n.currentDay = e("a", s).html(), n.selectedMonth = n.currentMonth = i, n.selectedYear = n.currentYear = a, this._selectDate(t, this._formatDate(n, n.currentDay, n.currentMonth, n.currentYear)))
            },
            _clearDate: function(t) {
                var i = e(t);
                this._selectDate(i, "")
            },
            _selectDate: function(t, i) {
                var a, s = e(t),
                    n = this._getInst(s[0]);
                i = null != i ? i : this._formatDate(n), n.input && n.input.val(i), this._updateAlternate(n), a = this._get(n, "onSelect"), a ? a.apply(n.input ? n.input[0] : null, [i, n]) : n.input && n.input.trigger("change"), n.inline ? this._updateDatepicker(n) : (this._hideDatepicker(), this._lastInput = n.input[0], "object" != typeof n.input[0] && n.input.focus(), this._lastInput = null)
            },
            _updateAlternate: function(t) {
                var i, a, s, n = this._get(t, "altField");
                n && (i = this._get(t, "altFormat") || this._get(t, "dateFormat"), a = this._getDate(t), s = this.formatDate(i, a, this._getFormatConfig(t)), e(n).each(function() {
                    e(this).val(s)
                }))
            },
            noWeekends: function(e) {
                var t = e.getDay();
                return [t > 0 && 6 > t, ""]
            },
            iso8601Week: function(e) {
                var t, i = new Date(e.getTime());
                return i.setDate(i.getDate() + 4 - (i.getDay() || 7)), t = i.getTime(), i.setMonth(0), i.setDate(1), Math.floor(Math.round((t - i) / 864e5) / 7) + 1
            },
            parseDate: function(i, a, s) {
                if (null == i || null == a) throw "Invalid arguments";
                if (a = "object" == typeof a ? "" + a : a + "", "" === a) return null;
                var n, r, o, u, c = 0,
                    h = (s ? s.shortYearCutoff : null) || this._defaults.shortYearCutoff,
                    l = "string" != typeof h ? h : (new Date).getFullYear() % 100 + parseInt(h, 10),
                    d = (s ? s.dayNamesShort : null) || this._defaults.dayNamesShort,
                    p = (s ? s.dayNames : null) || this._defaults.dayNames,
                    g = (s ? s.monthNamesShort : null) || this._defaults.monthNamesShort,
                    m = (s ? s.monthNames : null) || this._defaults.monthNames,
                    f = -1,
                    _ = -1,
                    v = -1,
                    k = -1,
                    y = !1,
                    b = function(e) {
                        var t = i.length > n + 1 && i.charAt(n + 1) === e;
                        return t && n++, t
                    },
                    D = function(e) {
                        var t = b(e),
                            i = "@" === e ? 14 : "!" === e ? 20 : "y" === e && t ? 4 : "o" === e ? 3 : 2,
                            s = RegExp("^\\d{1," + i + "}"),
                            n = a.substring(c).match(s);
                        if (!n) throw "Missing number at position " + c;
                        return c += n[0].length, parseInt(n[0], 10)
                    },
                    w = function(i, s, n) {
                        var r = -1,
                            o = e.map(b(i) ? n : s, function(e, t) {
                                return [
                                    [t, e]
                                ]
                            }).sort(function(e, t) {
                                return -(e[1].length - t[1].length)
                            });
                        if (e.each(o, function(e, i) {
                                var s = i[1];
                                return a.substr(c, s.length).toLowerCase() === s.toLowerCase() ? (r = i[0], c += s.length, !1) : t
                            }), -1 !== r) return r + 1;
                        throw "Unknown name at position " + c
                    },
                    M = function() {
                        if (a.charAt(c) !== i.charAt(n)) throw "Unexpected literal at position " + c;
                        c++
                    };
                for (n = 0; i.length > n; n++)
                    if (y) "'" !== i.charAt(n) || b("'") ? M() : y = !1;
                    else switch (i.charAt(n)) {
                        case "d":
                            v = D("d");
                            break;
                        case "D":
                            w("D", d, p);
                            break;
                        case "o":
                            k = D("o");
                            break;
                        case "m":
                            _ = D("m");
                            break;
                        case "M":
                            _ = w("M", g, m);
                            break;
                        case "y":
                            f = D("y");
                            break;
                        case "@":
                            u = new Date(D("@")), f = u.getFullYear(), _ = u.getMonth() + 1, v = u.getDate();
                            break;
                        case "!":
                            u = new Date((D("!") - this._ticksTo1970) / 1e4), f = u.getFullYear(), _ = u.getMonth() + 1, v = u.getDate();
                            break;
                        case "'":
                            b("'") ? M() : y = !0;
                            break;
                        default:
                            M()
                    }
                    if (a.length > c && (o = a.substr(c), !/^\s+/.test(o))) throw "Extra/unparsed characters found in date: " + o;
                if (-1 === f ? f = (new Date).getFullYear() : 100 > f && (f += (new Date).getFullYear() - (new Date).getFullYear() % 100 + (l >= f ? 0 : -100)), k > -1)
                    for (_ = 1, v = k; r = this._getDaysInMonth(f, _ - 1), !(r >= v);) _++, v -= r;
                if (u = this._daylightSavingAdjust(new Date(f, _ - 1, v)), u.getFullYear() !== f || u.getMonth() + 1 !== _ || u.getDate() !== v) throw "Invalid date";
                return u
            },
            ATOM: "yy-mm-dd",
            COOKIE: "D, dd M yy",
            ISO_8601: "yy-mm-dd",
            RFC_822: "D, d M y",
            RFC_850: "DD, dd-M-y",
            RFC_1036: "D, d M y",
            RFC_1123: "D, d M yy",
            RFC_2822: "D, d M yy",
            RSS: "D, d M y",
            TICKS: "!",
            TIMESTAMP: "@",
            W3C: "yy-mm-dd",
            _ticksTo1970: 864e9 * (718685 + Math.floor(492.5) - Math.floor(19.7) + Math.floor(4.925)),
            formatDate: function(e, t, i) {
                if (!t) return "";
                var a, s = (i ? i.dayNamesShort : null) || this._defaults.dayNamesShort,
                    n = (i ? i.dayNames : null) || this._defaults.dayNames,
                    r = (i ? i.monthNamesShort : null) || this._defaults.monthNamesShort,
                    o = (i ? i.monthNames : null) || this._defaults.monthNames,
                    u = function(t) {
                        var i = e.length > a + 1 && e.charAt(a + 1) === t;
                        return i && a++, i
                    },
                    c = function(e, t, i) {
                        var a = "" + t;
                        if (u(e))
                            for (; i > a.length;) a = "0" + a;
                        return a
                    },
                    h = function(e, t, i, a) {
                        return u(e) ? a[t] : i[t]
                    },
                    l = "",
                    d = !1;
                if (t)
                    for (a = 0; e.length > a; a++)
                        if (d) "'" !== e.charAt(a) || u("'") ? l += e.charAt(a) : d = !1;
                        else switch (e.charAt(a)) {
                            case "d":
                                l += c("d", t.getDate(), 2);
                                break;
                            case "D":
                                l += h("D", t.getDay(), s, n);
                                break;
                            case "o":
                                l += c("o", Math.round((new Date(t.getFullYear(), t.getMonth(), t.getDate()).getTime() - new Date(t.getFullYear(), 0, 0).getTime()) / 864e5), 3);
                                break;
                            case "m":
                                l += c("m", t.getMonth() + 1, 2);
                                break;
                            case "M":
                                l += h("M", t.getMonth(), r, o);
                                break;
                            case "y":
                                l += u("y") ? t.getFullYear() : (10 > t.getYear() % 100 ? "0" : "") + t.getYear() % 100;
                                break;
                            case "@":
                                l += t.getTime();
                                break;
                            case "!":
                                l += 1e4 * t.getTime() + this._ticksTo1970;
                                break;
                            case "'":
                                u("'") ? l += "'" : d = !0;
                                break;
                            default:
                                l += e.charAt(a)
                        }
                        return l
            },
            _possibleChars: function(e) {
                var t, i = "",
                    a = !1,
                    s = function(i) {
                        var a = e.length > t + 1 && e.charAt(t + 1) === i;
                        return a && t++, a
                    };
                for (t = 0; e.length > t; t++)
                    if (a) "'" !== e.charAt(t) || s("'") ? i += e.charAt(t) : a = !1;
                    else switch (e.charAt(t)) {
                        case "d":
                        case "m":
                        case "y":
                        case "@":
                            i += "0123456789";
                            break;
                        case "D":
                        case "M":
                            return null;
                        case "'":
                            s("'") ? i += "'" : a = !0;
                            break;
                        default:
                            i += e.charAt(t)
                    }
                    return i
            },
            _get: function(e, i) {
                return e.settings[i] !== t ? e.settings[i] : this._defaults[i]
            },
            _setDateFromField: function(e, t) {
                if (e.input.val() !== e.lastVal) {
                    var i = this._get(e, "dateFormat"),
                        a = e.lastVal = e.input ? e.input.val() : null,
                        s = this._getDefaultDate(e),
                        n = s,
                        r = this._getFormatConfig(e);
                    try {
                        n = this.parseDate(i, a, r) || s
                    } catch (o) {
                        a = t ? "" : a
                    }
                    e.selectedDay = n.getDate(), e.drawMonth = e.selectedMonth = n.getMonth(), e.drawYear = e.selectedYear = n.getFullYear(), e.currentDay = a ? n.getDate() : 0, e.currentMonth = a ? n.getMonth() : 0, e.currentYear = a ? n.getFullYear() : 0, this._adjustInstDate(e)
                }
            },
            _getDefaultDate: function(e) {
                return this._restrictMinMax(e, this._determineDate(e, this._get(e, "defaultDate"), new Date))
            },
            _determineDate: function(t, i, a) {
                var s = function(e) {
                        var t = new Date;
                        return t.setDate(t.getDate() + e), t
                    },
                    n = function(i) {
                        try {
                            return e.datepicker.parseDate(e.datepicker._get(t, "dateFormat"), i, e.datepicker._getFormatConfig(t))
                        } catch (a) {}
                        for (var s = (i.toLowerCase().match(/^c/) ? e.datepicker._getDate(t) : null) || new Date, n = s.getFullYear(), r = s.getMonth(), o = s.getDate(), u = /([+\-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g, c = u.exec(i); c;) {
                            switch (c[2] || "d") {
                                case "d":
                                case "D":
                                    o += parseInt(c[1], 10);
                                    break;
                                case "w":
                                case "W":
                                    o += 7 * parseInt(c[1], 10);
                                    break;
                                case "m":
                                case "M":
                                    r += parseInt(c[1], 10), o = Math.min(o, e.datepicker._getDaysInMonth(n, r));
                                    break;
                                case "y":
                                case "Y":
                                    n += parseInt(c[1], 10), o = Math.min(o, e.datepicker._getDaysInMonth(n, r))
                            }
                            c = u.exec(i)
                        }
                        return new Date(n, r, o)
                    },
                    r = null == i || "" === i ? a : "string" == typeof i ? n(i) : "number" == typeof i ? isNaN(i) ? a : s(i) : new Date(i.getTime());
                return r = r && "Invalid Date" == "" + r ? a : r, r && (r.setHours(0), r.setMinutes(0), r.setSeconds(0), r.setMilliseconds(0)), this._daylightSavingAdjust(r)
            },
            _daylightSavingAdjust: function(e) {
                return e ? (e.setHours(e.getHours() > 12 ? e.getHours() + 2 : 0), e) : null
            },
            _setDate: function(e, t, i) {
                var a = !t,
                    s = e.selectedMonth,
                    n = e.selectedYear,
                    r = this._restrictMinMax(e, this._determineDate(e, t, new Date));
                e.selectedDay = e.currentDay = r.getDate(), e.drawMonth = e.selectedMonth = e.currentMonth = r.getMonth(), e.drawYear = e.selectedYear = e.currentYear = r.getFullYear(), s === e.selectedMonth && n === e.selectedYear || i || this._notifyChange(e), this._adjustInstDate(e), e.input && e.input.val(a ? "" : this._formatDate(e))
            },
            _getDate: function(e) {
                var t = !e.currentYear || e.input && "" === e.input.val() ? null : this._daylightSavingAdjust(new Date(e.currentYear, e.currentMonth, e.currentDay));
                return t
            },
            _attachHandlers: function(t) {
                var i = this._get(t, "stepMonths"),
                    a = "#" + t.id.replace(/\\\\/g, "\\");
                t.dpDiv.find("[data-handler]").map(function() {
                    var t = {
                        prev: function() {
                            e.datepicker._adjustDate(a, -i, "M")
                        },
                        next: function() {
                            e.datepicker._adjustDate(a, +i, "M")
                        },
                        hide: function() {
                            e.datepicker._hideDatepicker()
                        },
                        today: function() {
                            e.datepicker._gotoToday(a)
                        },
                        selectDay: function() {
                            return e.datepicker._selectDay(a, +this.getAttribute("data-month"), +this.getAttribute("data-year"), this), !1
                        },
                        selectMonth: function() {
                            return e.datepicker._selectMonthYear(a, this, "M"), !1
                        },
                        selectYear: function() {
                            return e.datepicker._selectMonthYear(a, this, "Y"), !1
                        }
                    };
                    e(this).bind(this.getAttribute("data-event"), t[this.getAttribute("data-handler")])
                })
            },
            _generateHTML: function(e) {
                var t, i, a, s, n, r, o, u, c, h, l, d, p, g, m, f, _, v, k, y, b, D, w, M, C, x, I, N, T, A, E, S, Y, F, P, O, j, K, R, H = new Date,
                    W = this._daylightSavingAdjust(new Date(H.getFullYear(), H.getMonth(), H.getDate())),
                    L = this._get(e, "isRTL"),
                    U = this._get(e, "showButtonPanel"),
                    B = this._get(e, "hideIfNoPrevNext"),
                    z = this._get(e, "navigationAsDateFormat"),
                    q = this._getNumberOfMonths(e),
                    G = this._get(e, "showCurrentAtPos"),
                    J = this._get(e, "stepMonths"),
                    Q = 1 !== q[0] || 1 !== q[1],
                    V = this._daylightSavingAdjust(e.currentDay ? new Date(e.currentYear, e.currentMonth, e.currentDay) : new Date(9999, 9, 9)),
                    $ = this._getMinMaxDate(e, "min"),
                    X = this._getMinMaxDate(e, "max"),
                    Z = e.drawMonth - G,
                    et = e.drawYear;
                if (0 > Z && (Z += 12, et--), X)
                    for (t = this._daylightSavingAdjust(new Date(X.getFullYear(), X.getMonth() - q[0] * q[1] + 1, X.getDate())), t = $ && $ > t ? $ : t; this._daylightSavingAdjust(new Date(et, Z, 1)) > t;) Z--, 0 > Z && (Z = 11, et--);
                for (e.drawMonth = Z, e.drawYear = et, i = this._get(e, "prevText"), i = z ? this.formatDate(i, this._daylightSavingAdjust(new Date(et, Z - J, 1)), this._getFormatConfig(e)) : i, a = this._canAdjustMonth(e, -1, et, Z) ? "<a class='ui-datepicker-prev ui-corner-all' data-handler='prev' data-event='click' title='" + i + "'><span class='ui-icon ui-icon-circle-triangle-" + (L ? "e" : "w") + "'>" + i + "</span></a>" : B ? "" : "<a class='ui-datepicker-prev ui-corner-all ui-state-disabled' title='" + i + "'><span class='ui-icon ui-icon-circle-triangle-" + (L ? "e" : "w") + "'>" + i + "</span></a>", s = this._get(e, "nextText"), s = z ? this.formatDate(s, this._daylightSavingAdjust(new Date(et, Z + J, 1)), this._getFormatConfig(e)) : s, n = this._canAdjustMonth(e, 1, et, Z) ? "<a class='ui-datepicker-next ui-corner-all' data-handler='next' data-event='click' title='" + s + "'><span class='ui-icon ui-icon-circle-triangle-" + (L ? "w" : "e") + "'>" + s + "</span></a>" : B ? "" : "<a class='ui-datepicker-next ui-corner-all ui-state-disabled' title='" + s + "'><span class='ui-icon ui-icon-circle-triangle-" + (L ? "w" : "e") + "'>" + s + "</span></a>",
                    r = this._get(e, "currentText"), o = this._get(e, "gotoCurrent") && e.currentDay ? V : W, r = z ? this.formatDate(r, o, this._getFormatConfig(e)) : r, u = e.inline ? "" : "<button type='button' class='ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all' data-handler='hide' data-event='click'>" + this._get(e, "closeText") + "</button>", c = U ? "<div class='ui-datepicker-buttonpane ui-widget-content'>" + (L ? u : "") + (this._isInRange(e, o) ? "<button type='button' class='ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all' data-handler='today' data-event='click'>" + r + "</button>" : "") + (L ? "" : u) + "</div>" : "", h = parseInt(this._get(e, "firstDay"), 10), h = isNaN(h) ? 0 : h, l = this._get(e, "showWeek"), d = this._get(e, "dayNames"), p = this._get(e, "dayNamesMin"), g = this._get(e, "monthNames"), m = this._get(e, "monthNamesShort"), f = this._get(e, "beforeShowDay"), _ = this._get(e, "showOtherMonths"), v = this._get(e, "selectOtherMonths"), k = this._getDefaultDate(e), y = "", D = 0; q[0] > D; D++) {
                    for (w = "", this.maxRows = 4, M = 0; q[1] > M; M++) {
                        if (C = this._daylightSavingAdjust(new Date(et, Z, e.selectedDay)), x = " ui-corner-all", I = "", Q) {
                            if (I += "<div class='ui-datepicker-group", q[1] > 1) switch (M) {
                                case 0:
                                    I += " ui-datepicker-group-first", x = " ui-corner-" + (L ? "right" : "left");
                                    break;
                                case q[1] - 1:
                                    I += " ui-datepicker-group-last", x = " ui-corner-" + (L ? "left" : "right");
                                    break;
                                default:
                                    I += " ui-datepicker-group-middle", x = ""
                            }
                            I += "'>"
                        }
                        for (I += "<div class='ui-datepicker-header ui-widget-header ui-helper-clearfix" + x + "'>" + (/all|left/.test(x) && 0 === D ? L ? n : a : "") + (/all|right/.test(x) && 0 === D ? L ? a : n : "") + this._generateMonthYearHeader(e, Z, et, $, X, D > 0 || M > 0, g, m) + "</div><table class='ui-datepicker-calendar'><thead><tr>", N = l ? "<th class='ui-datepicker-week-col'>" + this._get(e, "weekHeader") + "</th>" : "", b = 0; 7 > b; b++) T = (b + h) % 7, N += "<th" + ((b + h + 6) % 7 >= 5 ? " class='ui-datepicker-week-end'" : "") + "><span title='" + d[T] + "'>" + p[T] + "</span></th>";
                        for (I += N + "</tr></thead><tbody>", A = this._getDaysInMonth(et, Z), et === e.selectedYear && Z === e.selectedMonth && (e.selectedDay = Math.min(e.selectedDay, A)), E = (this._getFirstDayOfMonth(et, Z) - h + 7) % 7, S = Math.ceil((E + A) / 7), Y = Q && this.maxRows > S ? this.maxRows : S, this.maxRows = Y, F = this._daylightSavingAdjust(new Date(et, Z, 1 - E)), P = 0; Y > P; P++) {
                            for (I += "<tr>", O = l ? "<td class='ui-datepicker-week-col'>" + this._get(e, "calculateWeek")(F) + "</td>" : "", b = 0; 7 > b; b++) j = f ? f.apply(e.input ? e.input[0] : null, [F]) : [!0, ""], K = F.getMonth() !== Z, R = K && !v || !j[0] || $ && $ > F || X && F > X, O += "<td class='" + ((b + h + 6) % 7 >= 5 ? " ui-datepicker-week-end" : "") + (K ? " ui-datepicker-other-month" : "") + (F.getTime() === C.getTime() && Z === e.selectedMonth && e._keyEvent || k.getTime() === F.getTime() && k.getTime() === C.getTime() ? " " + this._dayOverClass : "") + (R ? " " + this._unselectableClass + " ui-state-disabled" : "") + (K && !_ ? "" : " " + j[1] + (F.getTime() === V.getTime() ? " " + this._currentClass : "") + (F.getTime() === W.getTime() ? " ui-datepicker-today" : "")) + "'" + (K && !_ || !j[2] ? "" : " title='" + j[2].replace(/'/g, "&#39;") + "'") + (R ? "" : " data-handler='selectDay' data-event='click' data-month='" + F.getMonth() + "' data-year='" + F.getFullYear() + "'") + ">" + (K && !_ ? "&#xa0;" : R ? "<span class='ui-state-default'>" + F.getDate() + "</span>" : "<a class='ui-state-default" + (F.getTime() === W.getTime() ? " ui-state-highlight" : "") + (F.getTime() === V.getTime() ? " ui-state-active" : "") + (K ? " ui-priority-secondary" : "") + "' href='#'>" + F.getDate() + "</a>") + "</td>", F.setDate(F.getDate() + 1), F = this._daylightSavingAdjust(F);
                            I += O + "</tr>"
                        }
                        Z++, Z > 11 && (Z = 0, et++), I += "</tbody></table>" + (Q ? "</div>" + (q[0] > 0 && M === q[1] - 1 ? "<div class='ui-datepicker-row-break'></div>" : "") : ""), w += I
                    }
                    y += w
                }
                return y += c, e._keyEvent = !1, y
            },
            _generateMonthYearHeader: function(e, t, i, a, s, n, r, o) {
                var u, c, h, l, d, p, g, m, f = this._get(e, "changeMonth"),
                    _ = this._get(e, "changeYear"),
                    v = this._get(e, "showMonthAfterYear"),
                    k = "<div class='ui-datepicker-title'>",
                    y = "";
                if (n || !f) y += "<span class='ui-datepicker-month'>" + r[t] + "</span>";
                else {
                    for (u = a && a.getFullYear() === i, c = s && s.getFullYear() === i, y += "<select class='ui-datepicker-month' data-handler='selectMonth' data-event='change'>", h = 0; 12 > h; h++)(!u || h >= a.getMonth()) && (!c || s.getMonth() >= h) && (y += "<option value='" + h + "'" + (h === t ? " selected='selected'" : "") + ">" + o[h] + "</option>");
                    y += "</select>"
                }
                if (v || (k += y + (!n && f && _ ? "" : "&#xa0;")), !e.yearshtml)
                    if (e.yearshtml = "", n || !_) k += "<span class='ui-datepicker-year'>" + i + "</span>";
                    else {
                        for (l = this._get(e, "yearRange").split(":"), d = (new Date).getFullYear(), p = function(e) {
                                var t = e.match(/c[+\-].*/) ? i + parseInt(e.substring(1), 10) : e.match(/[+\-].*/) ? d + parseInt(e, 10) : parseInt(e, 10);
                                return isNaN(t) ? d : t
                            }, g = p(l[0]), m = Math.max(g, p(l[1] || "")), g = a ? Math.max(g, a.getFullYear()) : g, m = s ? Math.min(m, s.getFullYear()) : m, e.yearshtml += "<select class='ui-datepicker-year' data-handler='selectYear' data-event='change'>"; m >= g; g++) e.yearshtml += "<option value='" + g + "'" + (g === i ? " selected='selected'" : "") + ">" + g + "</option>";
                        e.yearshtml += "</select>", k += e.yearshtml, e.yearshtml = null
                    }
                return k += this._get(e, "yearSuffix"), v && (k += (!n && f && _ ? "" : "&#xa0;") + y), k += "</div>"
            },
            _adjustInstDate: function(e, t, i) {
                var a = e.drawYear + ("Y" === i ? t : 0),
                    s = e.drawMonth + ("M" === i ? t : 0),
                    n = Math.min(e.selectedDay, this._getDaysInMonth(a, s)) + ("D" === i ? t : 0),
                    r = this._restrictMinMax(e, this._daylightSavingAdjust(new Date(a, s, n)));
                e.selectedDay = r.getDate(), e.drawMonth = e.selectedMonth = r.getMonth(), e.drawYear = e.selectedYear = r.getFullYear(), ("M" === i || "Y" === i) && this._notifyChange(e)
            },
            _restrictMinMax: function(e, t) {
                var i = this._getMinMaxDate(e, "min"),
                    a = this._getMinMaxDate(e, "max"),
                    s = i && i > t ? i : t;
                return a && s > a ? a : s
            },
            _notifyChange: function(e) {
                var t = this._get(e, "onChangeMonthYear");
                t && t.apply(e.input ? e.input[0] : null, [e.selectedYear, e.selectedMonth + 1, e])
            },
            _getNumberOfMonths: function(e) {
                var t = this._get(e, "numberOfMonths");
                return null == t ? [1, 1] : "number" == typeof t ? [1, t] : t
            },
            _getMinMaxDate: function(e, t) {
                return this._determineDate(e, this._get(e, t + "Date"), null)
            },
            _getDaysInMonth: function(e, t) {
                return 32 - this._daylightSavingAdjust(new Date(e, t, 32)).getDate()
            },
            _getFirstDayOfMonth: function(e, t) {
                return new Date(e, t, 1).getDay()
            },
            _canAdjustMonth: function(e, t, i, a) {
                var s = this._getNumberOfMonths(e),
                    n = this._daylightSavingAdjust(new Date(i, a + (0 > t ? t : s[0] * s[1]), 1));
                return 0 > t && n.setDate(this._getDaysInMonth(n.getFullYear(), n.getMonth())), this._isInRange(e, n)
            },
            _isInRange: function(e, t) {
                var i, a, s = this._getMinMaxDate(e, "min"),
                    n = this._getMinMaxDate(e, "max"),
                    r = null,
                    o = null,
                    u = this._get(e, "yearRange");
                return u && (i = u.split(":"), a = (new Date).getFullYear(), r = parseInt(i[0], 10), o = parseInt(i[1], 10), i[0].match(/[+\-].*/) && (r += a), i[1].match(/[+\-].*/) && (o += a)), (!s || t.getTime() >= s.getTime()) && (!n || t.getTime() <= n.getTime()) && (!r || t.getFullYear() >= r) && (!o || o >= t.getFullYear())
            },
            _getFormatConfig: function(e) {
                var t = this._get(e, "shortYearCutoff");
                return t = "string" != typeof t ? t : (new Date).getFullYear() % 100 + parseInt(t, 10), {
                    shortYearCutoff: t,
                    dayNamesShort: this._get(e, "dayNamesShort"),
                    dayNames: this._get(e, "dayNames"),
                    monthNamesShort: this._get(e, "monthNamesShort"),
                    monthNames: this._get(e, "monthNames")
                }
            },
            _formatDate: function(e, t, i, a) {
                t || (e.currentDay = e.selectedDay, e.currentMonth = e.selectedMonth, e.currentYear = e.selectedYear);
                var s = t ? "object" == typeof t ? t : this._daylightSavingAdjust(new Date(a, i, t)) : this._daylightSavingAdjust(new Date(e.currentYear, e.currentMonth, e.currentDay));
                return this.formatDate(this._get(e, "dateFormat"), s, this._getFormatConfig(e))
            }
        }), e.fn.datepicker = function(t) {
            if (!this.length) return this;
            e.datepicker.initialized || (e(document).mousedown(e.datepicker._checkExternalClick), e.datepicker.initialized = !0), 0 === e("#" + e.datepicker._mainDivId).length && e("body").append(e.datepicker.dpDiv);
            var i = Array.prototype.slice.call(arguments, 1);
            return "string" != typeof t || "isDisabled" !== t && "getDate" !== t && "widget" !== t ? "option" === t && 2 === arguments.length && "string" == typeof arguments[1] ? e.datepicker["_" + t + "Datepicker"].apply(e.datepicker, [this[0]].concat(i)) : this.each(function() {
                "string" == typeof t ? e.datepicker["_" + t + "Datepicker"].apply(e.datepicker, [this].concat(i)) : e.datepicker._attachDatepicker(this, t)
            }) : e.datepicker["_" + t + "Datepicker"].apply(e.datepicker, [this[0]].concat(i))
        }, e.datepicker = new i, e.datepicker.initialized = !1, e.datepicker.uuid = (new Date).getTime(), e.datepicker.version = "1.10.4"
    }(jQuery),
    function(t, e) {
        var i = "ui-effects-";
        t.effects = {
                effect: {}
            },
            function(t, e) {
                function i(t, e, i) {
                    var s = u[e.type] || {};
                    return null == t ? i || !e.def ? null : e.def : (t = s.floor ? ~~t : parseFloat(t), isNaN(t) ? e.def : s.mod ? (t + s.mod) % s.mod : 0 > t ? 0 : t > s.max ? s.max : t)
                }

                function s(i) {
                    var s = h(),
                        n = s._rgba = [];
                    return i = i.toLowerCase(), f(l, function(t, a) {
                        var o, r = a.re.exec(i),
                            l = r && a.parse(r),
                            h = a.space || "rgba";
                        return l ? (o = s[h](l), s[c[h].cache] = o[c[h].cache], n = s._rgba = o._rgba, !1) : e
                    }), n.length ? ("0,0,0,0" === n.join() && t.extend(n, a.transparent), s) : a[i]
                }

                function n(t, e, i) {
                    return i = (i + 1) % 1, 1 > 6 * i ? t + 6 * (e - t) * i : 1 > 2 * i ? e : 2 > 3 * i ? t + 6 * (e - t) * (2 / 3 - i) : t
                }
                var a, o = "backgroundColor borderBottomColor borderLeftColor borderRightColor borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor",
                    r = /^([\-+])=\s*(\d+\.?\d*)/,
                    l = [{
                        re: /rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
                        parse: function(t) {
                            return [t[1], t[2], t[3], t[4]]
                        }
                    }, {
                        re: /rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
                        parse: function(t) {
                            return [2.55 * t[1], 2.55 * t[2], 2.55 * t[3], t[4]]
                        }
                    }, {
                        re: /#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/,
                        parse: function(t) {
                            return [parseInt(t[1], 16), parseInt(t[2], 16), parseInt(t[3], 16)]
                        }
                    }, {
                        re: /#([a-f0-9])([a-f0-9])([a-f0-9])/,
                        parse: function(t) {
                            return [parseInt(t[1] + t[1], 16), parseInt(t[2] + t[2], 16), parseInt(t[3] + t[3], 16)]
                        }
                    }, {
                        re: /hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
                        space: "hsla",
                        parse: function(t) {
                            return [t[1], t[2] / 100, t[3] / 100, t[4]]
                        }
                    }],
                    h = t.Color = function(e, i, s, n) {
                        return new t.Color.fn.parse(e, i, s, n)
                    },
                    c = {
                        rgba: {
                            props: {
                                red: {
                                    idx: 0,
                                    type: "byte"
                                },
                                green: {
                                    idx: 1,
                                    type: "byte"
                                },
                                blue: {
                                    idx: 2,
                                    type: "byte"
                                }
                            }
                        },
                        hsla: {
                            props: {
                                hue: {
                                    idx: 0,
                                    type: "degrees"
                                },
                                saturation: {
                                    idx: 1,
                                    type: "percent"
                                },
                                lightness: {
                                    idx: 2,
                                    type: "percent"
                                }
                            }
                        }
                    },
                    u = {
                        "byte": {
                            floor: !0,
                            max: 255
                        },
                        percent: {
                            max: 1
                        },
                        degrees: {
                            mod: 360,
                            floor: !0
                        }
                    },
                    d = h.support = {},
                    p = t("<p>")[0],
                    f = t.each;
                p.style.cssText = "background-color:rgba(1,1,1,.5)", d.rgba = p.style.backgroundColor.indexOf("rgba") > -1, f(c, function(t, e) {
                    e.cache = "_" + t, e.props.alpha = {
                        idx: 3,
                        type: "percent",
                        def: 1
                    }
                }), h.fn = t.extend(h.prototype, {
                    parse: function(n, o, r, l) {
                        if (n === e) return this._rgba = [null, null, null, null], this;
                        (n.jquery || n.nodeType) && (n = t(n).css(o), o = e);
                        var u = this,
                            d = t.type(n),
                            p = this._rgba = [];
                        return o !== e && (n = [n, o, r, l], d = "array"), "string" === d ? this.parse(s(n) || a._default) : "array" === d ? (f(c.rgba.props, function(t, e) {
                            p[e.idx] = i(n[e.idx], e)
                        }), this) : "object" === d ? (n instanceof h ? f(c, function(t, e) {
                            n[e.cache] && (u[e.cache] = n[e.cache].slice())
                        }) : f(c, function(e, s) {
                            var a = s.cache;
                            f(s.props, function(t, e) {
                                if (!u[a] && s.to) {
                                    if ("alpha" === t || null == n[t]) return;
                                    u[a] = s.to(u._rgba)
                                }
                                u[a][e.idx] = i(n[t], e, !0)
                            }), u[a] && 0 > t.inArray(null, u[a].slice(0, 3)) && (u[a][3] = 1, s.from && (u._rgba = s.from(u[a])))
                        }), this) : e
                    },
                    is: function(t) {
                        var i = h(t),
                            s = !0,
                            n = this;
                        return f(c, function(t, a) {
                            var o, r = i[a.cache];
                            return r && (o = n[a.cache] || a.to && a.to(n._rgba) || [], f(a.props, function(t, i) {
                                return null != r[i.idx] ? s = r[i.idx] === o[i.idx] : e
                            })), s
                        }), s
                    },
                    _space: function() {
                        var t = [],
                            e = this;
                        return f(c, function(i, s) {
                            e[s.cache] && t.push(i)
                        }), t.pop()
                    },
                    transition: function(t, e) {
                        var s = h(t),
                            n = s._space(),
                            a = c[n],
                            o = 0 === this.alpha() ? h("transparent") : this,
                            r = o[a.cache] || a.to(o._rgba),
                            l = r.slice();
                        return s = s[a.cache], f(a.props, function(t, n) {
                            var a = n.idx,
                                o = r[a],
                                h = s[a],
                                c = u[n.type] || {};
                            null !== h && (null === o ? l[a] = h : (c.mod && (h - o > c.mod / 2 ? o += c.mod : o - h > c.mod / 2 && (o -= c.mod)), l[a] = i((h - o) * e + o, n)))
                        }), this[n](l)
                    },
                    blend: function(e) {
                        if (1 === this._rgba[3]) return this;
                        var i = this._rgba.slice(),
                            s = i.pop(),
                            n = h(e)._rgba;
                        return h(t.map(i, function(t, e) {
                            return (1 - s) * n[e] + s * t
                        }))
                    },
                    toRgbaString: function() {
                        var e = "rgba(",
                            i = t.map(this._rgba, function(t, e) {
                                return null == t ? e > 2 ? 1 : 0 : t
                            });
                        return 1 === i[3] && (i.pop(), e = "rgb("), e + i.join() + ")"
                    },
                    toHslaString: function() {
                        var e = "hsla(",
                            i = t.map(this.hsla(), function(t, e) {
                                return null == t && (t = e > 2 ? 1 : 0), e && 3 > e && (t = Math.round(100 * t) + "%"), t
                            });
                        return 1 === i[3] && (i.pop(), e = "hsl("), e + i.join() + ")"
                    },
                    toHexString: function(e) {
                        var i = this._rgba.slice(),
                            s = i.pop();
                        return e && i.push(~~(255 * s)), "#" + t.map(i, function(t) {
                            return t = (t || 0).toString(16), 1 === t.length ? "0" + t : t
                        }).join("")
                    },
                    toString: function() {
                        return 0 === this._rgba[3] ? "transparent" : this.toRgbaString()
                    }
                }), h.fn.parse.prototype = h.fn, c.hsla.to = function(t) {
                    if (null == t[0] || null == t[1] || null == t[2]) return [null, null, null, t[3]];
                    var e, i, s = t[0] / 255,
                        n = t[1] / 255,
                        a = t[2] / 255,
                        o = t[3],
                        r = Math.max(s, n, a),
                        l = Math.min(s, n, a),
                        h = r - l,
                        c = r + l,
                        u = .5 * c;
                    return e = l === r ? 0 : s === r ? 60 * (n - a) / h + 360 : n === r ? 60 * (a - s) / h + 120 : 60 * (s - n) / h + 240, i = 0 === h ? 0 : .5 >= u ? h / c : h / (2 - c), [Math.round(e) % 360, i, u, null == o ? 1 : o]
                }, c.hsla.from = function(t) {
                    if (null == t[0] || null == t[1] || null == t[2]) return [null, null, null, t[3]];
                    var e = t[0] / 360,
                        i = t[1],
                        s = t[2],
                        a = t[3],
                        o = .5 >= s ? s * (1 + i) : s + i - s * i,
                        r = 2 * s - o;
                    return [Math.round(255 * n(r, o, e + 1 / 3)), Math.round(255 * n(r, o, e)), Math.round(255 * n(r, o, e - 1 / 3)), a]
                }, f(c, function(s, n) {
                    var a = n.props,
                        o = n.cache,
                        l = n.to,
                        c = n.from;
                    h.fn[s] = function(s) {
                        if (l && !this[o] && (this[o] = l(this._rgba)), s === e) return this[o].slice();
                        var n, r = t.type(s),
                            u = "array" === r || "object" === r ? s : arguments,
                            d = this[o].slice();
                        return f(a, function(t, e) {
                            var s = u["object" === r ? t : e.idx];
                            null == s && (s = d[e.idx]), d[e.idx] = i(s, e)
                        }), c ? (n = h(c(d)), n[o] = d, n) : h(d)
                    }, f(a, function(e, i) {
                        h.fn[e] || (h.fn[e] = function(n) {
                            var a, o = t.type(n),
                                l = "alpha" === e ? this._hsla ? "hsla" : "rgba" : s,
                                h = this[l](),
                                c = h[i.idx];
                            return "undefined" === o ? c : ("function" === o && (n = n.call(this, c), o = t.type(n)), null == n && i.empty ? this : ("string" === o && (a = r.exec(n), a && (n = c + parseFloat(a[2]) * ("+" === a[1] ? 1 : -1))), h[i.idx] = n, this[l](h)))
                        })
                    })
                }), h.hook = function(e) {
                    var i = e.split(" ");
                    f(i, function(e, i) {
                        t.cssHooks[i] = {
                            set: function(e, n) {
                                var a, o, r = "";
                                if ("transparent" !== n && ("string" !== t.type(n) || (a = s(n)))) {
                                    if (n = h(a || n), !d.rgba && 1 !== n._rgba[3]) {
                                        for (o = "backgroundColor" === i ? e.parentNode : e;
                                            ("" === r || "transparent" === r) && o && o.style;) try {
                                            r = t.css(o, "backgroundColor"), o = o.parentNode
                                        } catch (l) {}
                                        n = n.blend(r && "transparent" !== r ? r : "_default")
                                    }
                                    n = n.toRgbaString()
                                }
                                try {
                                    e.style[i] = n
                                } catch (l) {}
                            }
                        }, t.fx.step[i] = function(e) {
                            e.colorInit || (e.start = h(e.elem, i), e.end = h(e.end), e.colorInit = !0), t.cssHooks[i].set(e.elem, e.start.transition(e.end, e.pos))
                        }
                    })
                }, h.hook(o), t.cssHooks.borderColor = {
                    expand: function(t) {
                        var e = {};
                        return f(["Top", "Right", "Bottom", "Left"], function(i, s) {
                            e["border" + s + "Color"] = t
                        }), e
                    }
                }, a = t.Color.names = {
                    aqua: "#00ffff",
                    black: "#000000",
                    blue: "#0000ff",
                    fuchsia: "#ff00ff",
                    gray: "#808080",
                    green: "#008000",
                    lime: "#00ff00",
                    maroon: "#800000",
                    navy: "#000080",
                    olive: "#808000",
                    purple: "#800080",
                    red: "#ff0000",
                    silver: "#c0c0c0",
                    teal: "#008080",
                    white: "#ffffff",
                    yellow: "#ffff00",
                    transparent: [null, null, null, 0],
                    _default: "#ffffff"
                }
            }(jQuery),
            function() {
                function i(e) {
                    var i, s, n = e.ownerDocument.defaultView ? e.ownerDocument.defaultView.getComputedStyle(e, null) : e.currentStyle,
                        a = {};
                    if (n && n.length && n[0] && n[n[0]])
                        for (s = n.length; s--;) i = n[s], "string" == typeof n[i] && (a[t.camelCase(i)] = n[i]);
                    else
                        for (i in n) "string" == typeof n[i] && (a[i] = n[i]);
                    return a
                }

                function s(e, i) {
                    var s, n, o = {};
                    for (s in i) n = i[s], e[s] !== n && (a[s] || (t.fx.step[s] || !isNaN(parseFloat(n))) && (o[s] = n));
                    return o
                }
                var n = ["add", "remove", "toggle"],
                    a = {
                        border: 1,
                        borderBottom: 1,
                        borderColor: 1,
                        borderLeft: 1,
                        borderRight: 1,
                        borderTop: 1,
                        borderWidth: 1,
                        margin: 1,
                        padding: 1
                    };
                t.each(["borderLeftStyle", "borderRightStyle", "borderBottomStyle", "borderTopStyle"], function(e, i) {
                    t.fx.step[i] = function(t) {
                        ("none" !== t.end && !t.setAttr || 1 === t.pos && !t.setAttr) && (jQuery.style(t.elem, i, t.end), t.setAttr = !0)
                    }
                }), t.fn.addBack || (t.fn.addBack = function(t) {
                    return this.add(null == t ? this.prevObject : this.prevObject.filter(t))
                }), t.effects.animateClass = function(e, a, o, r) {
                    var l = t.speed(a, o, r);
                    return this.queue(function() {
                        var a, o = t(this),
                            r = o.attr("class") || "",
                            h = l.children ? o.find("*").addBack() : o;
                        h = h.map(function() {
                            var e = t(this);
                            return {
                                el: e,
                                start: i(this)
                            }
                        }), a = function() {
                            t.each(n, function(t, i) {
                                e[i] && o[i + "Class"](e[i])
                            })
                        }, a(), h = h.map(function() {
                            return this.end = i(this.el[0]), this.diff = s(this.start, this.end), this
                        }), o.attr("class", r), h = h.map(function() {
                            var e = this,
                                i = t.Deferred(),
                                s = t.extend({}, l, {
                                    queue: !1,
                                    complete: function() {
                                        i.resolve(e)
                                    }
                                });
                            return this.el.animate(this.diff, s), i.promise()
                        }), t.when.apply(t, h.get()).done(function() {
                            a(), t.each(arguments, function() {
                                var e = this.el;
                                t.each(this.diff, function(t) {
                                    e.css(t, "")
                                })
                            }), l.complete.call(o[0])
                        })
                    })
                }, t.fn.extend({
                    addClass: function(e) {
                        return function(i, s, n, a) {
                            return s ? t.effects.animateClass.call(this, {
                                add: i
                            }, s, n, a) : e.apply(this, arguments)
                        }
                    }(t.fn.addClass),
                    removeClass: function(e) {
                        return function(i, s, n, a) {
                            return arguments.length > 1 ? t.effects.animateClass.call(this, {
                                remove: i
                            }, s, n, a) : e.apply(this, arguments)
                        }
                    }(t.fn.removeClass),
                    toggleClass: function(i) {
                        return function(s, n, a, o, r) {
                            return "boolean" == typeof n || n === e ? a ? t.effects.animateClass.call(this, n ? {
                                add: s
                            } : {
                                remove: s
                            }, a, o, r) : i.apply(this, arguments) : t.effects.animateClass.call(this, {
                                toggle: s
                            }, n, a, o)
                        }
                    }(t.fn.toggleClass),
                    switchClass: function(e, i, s, n, a) {
                        return t.effects.animateClass.call(this, {
                            add: i,
                            remove: e
                        }, s, n, a)
                    }
                })
            }(),
            function() {
                function s(e, i, s, n) {
                    return t.isPlainObject(e) && (i = e, e = e.effect), e = {
                        effect: e
                    }, null == i && (i = {}), t.isFunction(i) && (n = i, s = null, i = {}), ("number" == typeof i || t.fx.speeds[i]) && (n = s, s = i, i = {}), t.isFunction(s) && (n = s, s = null), i && t.extend(e, i), s = s || i.duration, e.duration = t.fx.off ? 0 : "number" == typeof s ? s : s in t.fx.speeds ? t.fx.speeds[s] : t.fx.speeds._default, e.complete = n || i.complete, e
                }

                function n(e) {
                    return !e || "number" == typeof e || t.fx.speeds[e] ? !0 : "string" != typeof e || t.effects.effect[e] ? t.isFunction(e) ? !0 : "object" != typeof e || e.effect ? !1 : !0 : !0
                }
                t.extend(t.effects, {
                    version: "1.10.4",
                    save: function(t, e) {
                        for (var s = 0; e.length > s; s++) null !== e[s] && t.data(i + e[s], t[0].style[e[s]])
                    },
                    restore: function(t, s) {
                        var n, a;
                        for (a = 0; s.length > a; a++) null !== s[a] && (n = t.data(i + s[a]), n === e && (n = ""), t.css(s[a], n))
                    },
                    setMode: function(t, e) {
                        return "toggle" === e && (e = t.is(":hidden") ? "show" : "hide"), e
                    },
                    getBaseline: function(t, e) {
                        var i, s;
                        switch (t[0]) {
                            case "top":
                                i = 0;
                                break;
                            case "middle":
                                i = .5;
                                break;
                            case "bottom":
                                i = 1;
                                break;
                            default:
                                i = t[0] / e.height
                        }
                        switch (t[1]) {
                            case "left":
                                s = 0;
                                break;
                            case "center":
                                s = .5;
                                break;
                            case "right":
                                s = 1;
                                break;
                            default:
                                s = t[1] / e.width
                        }
                        return {
                            x: s,
                            y: i
                        }
                    },
                    createWrapper: function(e) {
                        if (e.parent().is(".ui-effects-wrapper")) return e.parent();
                        var i = {
                                width: e.outerWidth(!0),
                                height: e.outerHeight(!0),
                                "float": e.css("float")
                            },
                            s = t("<div></div>").addClass("ui-effects-wrapper").css({
                                fontSize: "100%",
                                background: "transparent",
                                border: "none",
                                margin: 0,
                                padding: 0
                            }),
                            n = {
                                width: e.width(),
                                height: e.height()
                            },
                            a = document.activeElement;
                        try {
                            a.id
                        } catch (o) {
                            a = document.body
                        }
                        return e.wrap(s), (e[0] === a || t.contains(e[0], a)) && t(a).focus(), s = e.parent(), "static" === e.css("position") ? (s.css({
                            position: "relative"
                        }), e.css({
                            position: "relative"
                        })) : (t.extend(i, {
                            position: e.css("position"),
                            zIndex: e.css("z-index")
                        }), t.each(["top", "left", "bottom", "right"], function(t, s) {
                            i[s] = e.css(s), isNaN(parseInt(i[s], 10)) && (i[s] = "auto")
                        }), e.css({
                            position: "relative",
                            top: 0,
                            left: 0,
                            right: "auto",
                            bottom: "auto"
                        })), e.css(n), s.css(i).show()
                    },
                    removeWrapper: function(e) {
                        var i = document.activeElement;
                        return e.parent().is(".ui-effects-wrapper") && (e.parent().replaceWith(e), (e[0] === i || t.contains(e[0], i)) && t(i).focus()), e
                    },
                    setTransition: function(e, i, s, n) {
                        return n = n || {}, t.each(i, function(t, i) {
                            var a = e.cssUnit(i);
                            a[0] > 0 && (n[i] = a[0] * s + a[1])
                        }), n
                    }
                }), t.fn.extend({
                    effect: function() {
                        function e(e) {
                            function s() {
                                t.isFunction(a) && a.call(n[0]), t.isFunction(e) && e()
                            }
                            var n = t(this),
                                a = i.complete,
                                r = i.mode;
                            (n.is(":hidden") ? "hide" === r : "show" === r) ? (n[r](), s()) : o.call(n[0], i, s)
                        }
                        var i = s.apply(this, arguments),
                            n = i.mode,
                            a = i.queue,
                            o = t.effects.effect[i.effect];
                        return t.fx.off || !o ? n ? this[n](i.duration, i.complete) : this.each(function() {
                            i.complete && i.complete.call(this)
                        }) : a === !1 ? this.each(e) : this.queue(a || "fx", e)
                    },
                    show: function(t) {
                        return function(e) {
                            if (n(e)) return t.apply(this, arguments);
                            var i = s.apply(this, arguments);
                            return i.mode = "show", this.effect.call(this, i)
                        }
                    }(t.fn.show),
                    hide: function(t) {
                        return function(e) {
                            if (n(e)) return t.apply(this, arguments);
                            var i = s.apply(this, arguments);
                            return i.mode = "hide", this.effect.call(this, i)
                        }
                    }(t.fn.hide),
                    toggle: function(t) {
                        return function(e) {
                            if (n(e) || "boolean" == typeof e) return t.apply(this, arguments);
                            var i = s.apply(this, arguments);
                            return i.mode = "toggle", this.effect.call(this, i)
                        }
                    }(t.fn.toggle),
                    cssUnit: function(e) {
                        var i = this.css(e),
                            s = [];
                        return t.each(["em", "px", "%", "pt"], function(t, e) {
                            i.indexOf(e) > 0 && (s = [parseFloat(i), e])
                        }), s
                    }
                })
            }(),
            function() {
                var e = {};
                t.each(["Quad", "Cubic", "Quart", "Quint", "Expo"], function(t, i) {
                    e[i] = function(e) {
                        return Math.pow(e, t + 2)
                    }
                }), t.extend(e, {
                    Sine: function(t) {
                        return 1 - Math.cos(t * Math.PI / 2)
                    },
                    Circ: function(t) {
                        return 1 - Math.sqrt(1 - t * t)
                    },
                    Elastic: function(t) {
                        return 0 === t || 1 === t ? t : -Math.pow(2, 8 * (t - 1)) * Math.sin((80 * (t - 1) - 7.5) * Math.PI / 15)
                    },
                    Back: function(t) {
                        return t * t * (3 * t - 2)
                    },
                    Bounce: function(t) {
                        for (var e, i = 4;
                            ((e = Math.pow(2, --i)) - 1) / 11 > t;);
                        return 1 / Math.pow(4, 3 - i) - 7.5625 * Math.pow((3 * e - 2) / 22 - t, 2)
                    }
                }), t.each(e, function(e, i) {
                    t.easing["easeIn" + e] = i, t.easing["easeOut" + e] = function(t) {
                        return 1 - i(1 - t)
                    }, t.easing["easeInOut" + e] = function(t) {
                        return .5 > t ? i(2 * t) / 2 : 1 - i(-2 * t + 2) / 2
                    }
                })
            }()
    }(jQuery),
    function(t) {
        t.effects.effect.fade = function(e, i) {
            var s = t(this),
                n = t.effects.setMode(s, e.mode || "toggle");
            s.animate({
                opacity: n
            }, {
                queue: !1,
                duration: e.duration,
                easing: e.easing,
                complete: i
            })
        }
    }(jQuery),
    function(t) {
        t.effects.effect.slide = function(e, i) {
            var s, n = t(this),
                a = ["position", "top", "bottom", "left", "right", "width", "height"],
                o = t.effects.setMode(n, e.mode || "show"),
                r = "show" === o,
                l = e.direction || "left",
                h = "up" === l || "down" === l ? "top" : "left",
                c = "up" === l || "left" === l,
                u = {};
            t.effects.save(n, a), n.show(), s = e.distance || n["top" === h ? "outerHeight" : "outerWidth"](!0), t.effects.createWrapper(n).css({
                overflow: "hidden"
            }), r && n.css(h, c ? isNaN(s) ? "-" + s : -s : s), u[h] = (r ? c ? "+=" : "-=" : c ? "-=" : "+=") + s, n.animate(u, {
                queue: !1,
                duration: e.duration,
                easing: e.easing,
                complete: function() {
                    "hide" === o && n.hide(), t.effects.restore(n, a), t.effects.removeWrapper(n), i()
                }
            })
        }
    }(jQuery), jQuery(function($) {
        if ($.datepicker.regional.ar = {
                closeText: "إغلاق",
                prevText: "&#x3c;السابق",
                nextText: "التالي&#x3e;",
                currentText: "اليوم",
                monthNames: ["كانون الثاني", "شباط", "آذار", "نيسان", "مايو", "حزيران", "تموز", "آب", "أيلول", "تشرين الأول", "تشرين الثاني", "كانون الأول"],
                monthNamesShort: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
                dayNames: ["الأحد", "الاثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة", "السبت"],
                dayNamesShort: ["الأحد", "الاثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة", "السبت"],
                dayNamesMin: ["ح", "ن", "ث", "ر", "خ", "ج", "س"],
                weekHeader: "أسبوع",
                dateFormat: "dd/mm/yy",
                firstDay: 6,
                isRTL: !0,
                showMonthAfterYear: !1,
                yearSuffix: ""
            }, $.datepicker.regional.de = {
                closeText: "schließen",
                prevText: "&#x3c;zurück",
                nextText: "Vor&#x3e;",
                currentText: "heute",
                monthNames: ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
                monthNamesShort: ["Jan", "Feb", "Mär", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"],
                dayNames: ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"],
                dayNamesShort: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
                dayNamesMin: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
                weekHeader: "KW",
                dateFormat: "dd.mm.yy",
                firstDay: 1,
                isRTL: !1,
                showMonthAfterYear: !1,
                yearSuffix: ""
            }, $.datepicker.regional.es = {
                closeText: "Cerrar",
                prevText: "<Ant",
                nextText: "Sig>",
                currentText: "Hoy",
                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                dayNamesShort: ["Dom", "Lun", "Mar", "Mié", "Juv", "Vie", "Sáb"],
                dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
                weekHeader: "Sm",
                dateFormat: "dd/mm/yy",
                firstDay: 1,
                isRTL: !1,
                showMonthAfterYear: !1,
                yearSuffix: ""
            }, $.datepicker.regional.fr = {
                closeText: "Fermer",
                prevText: "Précédent",
                nextText: "Suivant",
                currentText: "Aujourd'hui",
                monthNames: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
                monthNamesShort: ["Janv.", "Févr.", "Mars", "Avril", "Mai", "Juin", "Juil.", "Août", "Sept.", "Oct.", "Nov.", "Déc."],
                dayNames: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"],
                dayNamesShort: ["Dim.", "Lun.", "Mar.", "Mer.", "Jeu.", "Ven.", "Sam."],
                dayNamesMin: ["D", "L", "M", "M", "J", "V", "S"],
                weekHeader: "Sem.",
                dateFormat: "dd/mm/yy",
                firstDay: 1,
                isRTL: !1,
                showMonthAfterYear: !1,
                yearSuffix: ""
            }, $.datepicker.regional.ja = {
                closeText: "閉じる",
                prevText: "&#x3c;前",
                nextText: "次&#x3e;",
                currentText: "今日",
                monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
                monthNamesShort: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
                dayNames: ["日曜日", "月曜日", "火曜日", "水曜日", "木曜日", "金曜日", "土曜日"],
                dayNamesShort: ["日", "日", "日", "日", "日", "日", "日"],
                dayNamesMin: ["日", "日", "日", "日", "日", "日", "日"],
                weekHeader: "週",
                dateFormat: "yy/mm/dd",
                firstDay: 0,
                isRTL: !1,
                showMonthAfterYear: !0,
                yearSuffix: "年"
            }, $.datepicker.regional.ru = {
                closeText: "Закрыть",
                prevText: "&#x3c;Пред",
                nextText: "След&#x3e;",
                currentText: "Сегодня",
                monthNames: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                monthNamesShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
                dayNames: ["воскресенье", "понедельник", "вторник", "среда", "четверг", "пятница", "суббота"],
                dayNamesShort: ["вск", "пнд", "втр", "срд", "чтв", "птн", "сбт"],
                dayNamesMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                weekHeader: "Нед",
                dateFormat: "dd.mm.yy",
                firstDay: 1,
                isRTL: !1,
                showMonthAfterYear: !1,
                yearSuffix: ""
            }, $.datepicker.regional["zh-CN"] = {
                closeText: "关闭",
                prevText: "&#x3c;上月",
                nextText: "下月&#x3e;",
                currentText: "今天",
                monthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
                monthNamesShort: ["一", "二", "三", "四", "五", "六", "七", "八", "九", "十", "十一", "十二"],
                dayNames: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"],
                dayNamesShort: ["周日", "周一", "周二", "周三", "周四", "周五", "周六"],
                dayNamesMin: ["日", "一", "二", "三", "四", "五", "六"],
                weekHeader: "周",
                dateFormat: "yy-mm-dd",
                firstDay: 1,
                isRTL: !1,
                showMonthAfterYear: !0,
                yearSuffix: "年"
            }, $.datepicker.regional["zh-HK"] = {
                closeText: "關閉",
                prevText: "&#x3c;上月",
                nextText: "下月&#x3e;",
                currentText: "今天",
                monthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
                monthNamesShort: ["一", "二", "三", "四", "五", "六", "七", "八", "九", "十", "十一", "十二"],
                dayNames: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"],
                dayNamesShort: ["周日", "周一", "周二", "周三", "周四", "周五", "周六"],
                dayNamesMin: ["日", "一", "二", "三", "四", "五", "六"],
                weekHeader: "周",
                dateFormat: "dd-mm-yy",
                firstDay: 0,
                isRTL: !1,
                showMonthAfterYear: !0,
                yearSuffix: "年"
            }, $.datepicker.regional.it = {
                closeText: "Chiudi",
                prevText: "&#x3c;Prec",
                nextText: "Succ&#x3e;",
                currentText: "Oggi",
                monthNames: ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"],
                monthNamesShort: ["Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic"],
                dayNames: ["Domenica", "Luned&#236", "Marted&#236", "Mercoled&#236", "Gioved&#236", "Venerd&#236", "Sabato"],
                dayNamesShort: ["Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab"],
                dayNamesMin: ["Do", "Lu", "Ma", "Me", "Gi", "Ve", "Sa"],
                weekHeader: "Sm",
                dateFormat: "dd/mm/yy",
                firstDay: 1,
                isRTL: !1,
                showMonthAfterYear: !1,
                yearSuffix: ""
            }, $.datepicker.regional["tr-TR"] = {
                closeText: "kapat",
                prevText: "&#x3c;geri",
                nextText: "ileri&#x3e",
                currentText: "bugün",
                monthNames: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
                monthNamesShort: ["Oca", "Şub", "Mar", "Nis", "May", "Haz", "Tem", "Ağu", "Eyl", "Eki", "Kas", "Ara"],
                dayNames: ["Pazar", "Pazartesi", "Salı", "Çarşamba", "Perşembe", "Cuma", "Cumartesi"],
                dayNamesShort: ["Pz", "Pt", "Sa", "Ça", "Pe", "Cu", "Ct"],
                dayNamesMin: ["Pz", "Pt", "Sa", "Ça", "Pe", "Cu", "Ct"],
                weekHeader: "Hf",
                dateFormat: "dd.mm.yy",
                firstDay: 1,
                isRTL: !1,
                showMonthAfterYear: !1,
                yearSuffix: ""
            }, $.datepicker.regional["pt-PT"] = {
                closeText: "Fechar",
                prevText: "&#x3c;Anterior",
                nextText: "Seguinte",
                currentText: "Hoje",
                monthNames: ["Janeiro", "Fevereiro", "Mar&ccedil;o", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
                monthNamesShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
                dayNames: ["Domingo", "Segunda-feira", "Ter&ccedil;a-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "S&aacute;bado"],
                dayNamesShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "S&aacute;b"],
                dayNamesMin: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "S&aacute;b"],
                weekHeader: "Sem",
                dateFormat: "dd/mm/yy",
                firstDay: 0,
                isRTL: !1,
                showMonthAfterYear: !1,
                yearSuffix: ""
            }, $.datepicker.regional.ca = {
                closeText: "Tancar",
                prevText: "&#x3c;Ant",
                nextText: "Seg&#x3e;",
                currentText: "Avui",
                monthNames: ["Gener", "Febrer", "Mar&ccedil;", "Abril", "Maig", "Juny", "Juliol", "Agost", "Setembre", "Octubre", "Novembre", "Desembre"],
                monthNamesShort: ["Gen", "Feb", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Des"],
                dayNames: ["Diumenge", "Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte"],
                dayNamesShort: ["Dug", "Dln", "Dmt", "Dmc", "Djs", "Dvn", "Dsb"],
                dayNamesMin: ["Dg", "Dl", "Dt", "Dc", "Dj", "Dv", "Ds"],
                weekHeader: "Sm",
                dateFormat: "dd/mm/yy",
                firstDay: 1,
                isRTL: !1,
                showMonthAfterYear: !1,
                yearSuffix: ""
            }, $.datepicker.regional.cz = {
                closeText: "Zavřít",
                prevText: "&#x3c;Dříve",
                nextText: "Později&#x3e;",
                currentText: "Nyní",
                monthNames: ["leden", "únor", "březen", "duben", "květen", "červen", "červenec", "srpen", "září", "říjen", "listopad", "prosinec"],
                monthNamesShort: ["led", "úno", "bře", "dub", "kvě", "čer", "čvc", "srp", "zář", "říj", "lis", "pro"],
                dayNames: ["neděle", "pondělí", "úterý", "středa", "čtvrtek", "pátek", "sobota"],
                dayNamesShort: ["ne", "po", "út", "st", "čt", "pá", "so"],
                dayNamesMin: ["ne", "po", "út", "st", "čt", "pá", "so"],
                weekHeader: "Týd",
                dateFormat: "dd.mm.yy",
                firstDay: 1,
                isRTL: !1,
                showMonthAfterYear: !1,
                yearSuffix: ""
            }, "undefined" != typeof globalLang && globalLang || $("html").attr("lang") && $("html").attr("lang")) {
            var lang;
            "undefined" != typeof globalLang && globalLang ? lang = globalLang : (lang = $("html").attr("lang"), "zh" == lang && (lang += document.location.href.match(/mandarin\.cn\//i) || document.location.href.match(/mohgweb12\w{2,3}(-m)?-cn\./i) ? "-CN" : "-HK")), $.datepicker.setDefaults($.datepicker.regional[lang])
        }
    }),
    function($) {
        $.fn.hasClassParents = function(strClass, iDepth) {
            return void 0 == iDepth && (iDepth = 3), iDepth > $(this).parents().length && (iDepth = $(this).parents().length), iDepth--, $(this).hasClass(strClass) ? !0 : iDepth >= 0 ? $(this).parent().hasClassParents(strClass, iDepth) : !1
        }, $.fn.normalizeHeight = function(strColumnSelector, exclude) {
            var $this = $(this),
                strGroupPrefix = "js-colgroup",
                aColumnGroup = new Array,
                aExclude = new Array;
            void 0 !== exclude && (aExclude = exclude.split(" ")), $this.find(strColumnSelector).each(function(index, value) {
                for (var ndx = 0; ndx < aExclude.length; ndx++) {
                    var isExcluded = $(this).hasClassParents(aExclude[ndx].replace(".", ""), 2);
                    if (isExcluded) return
                }
                if (!$(this).attr("class").match(strGroupPrefix)) {
                    var iOffsetTop = Math.round($(this).offset().top),
                        strColumnGroup = strGroupPrefix + "-" + iOffsetTop;
                    $(this).addClass(strColumnGroup), -1 == $.inArray(strColumnGroup, aColumnGroup) && aColumnGroup.push(strColumnGroup)
                }
            });
            for (var ndx = 0; ndx < aColumnGroup.length; ndx++) {
                var iMaxHeight = 0,
                    $ColumnGroup = $this.find("." + aColumnGroup[ndx] + strColumnSelector);
                $ColumnGroup.length > 1 ? ($ColumnGroup.each(function(index, value) {
                    iMaxHeight < $(this).height() && (iMaxHeight = $(this).height())
                }), $ColumnGroup.each(function(index, value) {
                    var $expando = $(this).find(".expando").first();
                    if ("undefined" != typeof $expando && $expando.length > 0) {
                        var iOffsetHeight = iMaxHeight - $(this).height() + $expando.height();
                        $expando.css({
                            "min-height": iOffsetHeight,
                            overflow: "hidden"
                        })
                    }
                    $(this).css({
                        "min-height": iMaxHeight
                    })
                })) : $ColumnGroup.each(function() {
                    $(this).removeClass(aColumnGroup[ndx])
                })
            }
        }
    }(jQuery),
    function($) {
        $.fn.supersleight = function(settings) {
            return settings = $.extend({
                imgs: !0,
                backgrounds: !0,
                shim: "x.gif",
                apply_positioning: !0
            }, settings), this.each(function() {
                $("html").hasClass("ie6") && $(this).find("*").andSelf().each(function(i, obj) {
                    var self = $(obj);
                    if (settings.backgrounds && null !== self.css("background-image").match(/\.png/i)) {
                        var bg = self.css("background-image"),
                            src = bg.substring(5, bg.length - 2),
                            mode = "no-repeat" == self.css("background-repeat") ? "crop" : "scale",
                            styles = {
                                filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + src + "', sizingMethod='" + mode + "')",
                                "background-image": "url(" + settings.shim + ")"
                            };
                        self.css(styles)
                    }
                    if (settings.imgs && self.is("img[src$=png]")) {
                        var styles = {
                            width: self.width() + "px",
                            height: self.height() + "px",
                            filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + self.attr("src") + "', sizingMethod='scale')"
                        };
                        self.css(styles).attr("src", settings.shim)
                    }
                    settings.apply_positioning && self.is("a, input") && ("" === self.css("position") || "static" == self.css("position")) && self.css("position", "relative")
                })
            })
        }
    }(jQuery),
    function($) {
        $.fn.numeric = function(config, callback) {
            "boolean" == typeof config && (config = {
                decimal: config
            }), config = config || {}, "undefined" == typeof config.negative && (config.negative = !0);
            var decimal = config.decimal === !1 ? "" : config.decimal || ".",
                negative = config.negative === !0 ? !0 : !1;
            return callback = "function" == typeof callback ? callback : function() {}, this.data("numeric.decimal", decimal).data("numeric.negative", negative).data("numeric.callback", callback).keypress($.fn.numeric.keypress).keyup($.fn.numeric.keyup).blur($.fn.numeric.blur)
        }, $.fn.numeric.keypress = function(e) {
            var decimal = $.data(this, "numeric.decimal"),
                negative = $.data(this, "numeric.negative"),
                key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
            if (13 == key && "input" == this.nodeName.toLowerCase()) return !0;
            if (13 == key) return !1;
            var allow = !1;
            if (e.ctrlKey && 97 == key || e.ctrlKey && 65 == key) return !0;
            if (e.ctrlKey && 120 == key || e.ctrlKey && 88 == key) return !0;
            if (e.ctrlKey && 99 == key || e.ctrlKey && 67 == key) return !0;
            if (e.ctrlKey && 122 == key || e.ctrlKey && 90 == key) return !0;
            if (e.ctrlKey && 118 == key || e.ctrlKey && 86 == key || e.shiftKey && 45 == key) return !0;
            if (48 > key || key > 57) {
                var value = $(this).val();
                if (0 !== value.indexOf("-") && negative && 45 == key && (0 === value.length || 0 === parseInt($.fn.getSelectionStart(this), 10))) return !0;
                decimal && key == decimal.charCodeAt(0) && -1 != value.indexOf(decimal) && (allow = !1), 8 != key && 9 != key && 13 != key && 35 != key && 36 != key && 37 != key && 39 != key && 46 != key ? allow = !1 : "undefined" != typeof e.charCode && (e.keyCode == e.which && 0 !== e.which ? (allow = !0, 46 == e.which && (allow = !1)) : 0 !== e.keyCode && 0 === e.charCode && 0 === e.which && (allow = !0)), decimal && key == decimal.charCodeAt(0) && (allow = -1 == value.indexOf(decimal) ? !0 : !1)
            } else allow = !0;
            return allow
        }, $.fn.numeric.keyup = function(e) {
            var val = $(this).val();
            if (val && val.length > 0) {
                var carat = $.fn.getSelectionStart(this),
                    selectionEnd = $.fn.getSelectionEnd(this),
                    decimal = $.data(this, "numeric.decimal"),
                    negative = $.data(this, "numeric.negative");
                if ("" !== decimal && null !== decimal) {
                    var dot = val.indexOf(decimal);
                    0 === dot && (this.value = "0" + val), 1 == dot && "-" == val.charAt(0) && (this.value = "-0" + val.substring(1)), val = this.value
                }
                for (var validChars = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "-", decimal], length = val.length, i = length - 1; i >= 0; i--) {
                    var ch = val.charAt(i);
                    0 !== i && "-" == ch ? val = val.substring(0, i) + val.substring(i + 1) : 0 !== i || negative || "-" != ch || (val = val.substring(1));
                    for (var validChar = !1, j = 0; j < validChars.length; j++)
                        if (ch == validChars[j]) {
                            validChar = !0;
                            break
                        }
                    validChar && " " != ch || (val = val.substring(0, i) + val.substring(i + 1))
                }
                var firstDecimal = val.indexOf(decimal);
                if (firstDecimal > 0)
                    for (var k = length - 1; k > firstDecimal; k--) {
                        var chch = val.charAt(k);
                        chch == decimal && (val = val.substring(0, k) + val.substring(k + 1))
                    }
                this.value = val, $.fn.setSelection(this, [carat, selectionEnd])
            }
        }, $.fn.numeric.blur = function() {
            var decimal = $.data(this, "numeric.decimal"),
                callback = $.data(this, "numeric.callback"),
                val = this.value;
            if ("" !== val) {
                var re = new RegExp("^\\d+$|^\\d*" + decimal + "\\d+$");
                re.exec(val) || callback.apply(this)
            }
        }, $.fn.removeNumeric = function() {
            return this.data("numeric.decimal", null).data("numeric.negative", null).data("numeric.callback", null).unbind("keypress", $.fn.numeric.keypress).unbind("blur", $.fn.numeric.blur)
        }, $.fn.getSelectionStart = function(o) {
            if (o.createTextRange) {
                var r = document.selection.createRange().duplicate();
                return r.moveEnd("character", o.value.length), "" === r.text ? o.value.length : o.value.lastIndexOf(r.text)
            }
            return o.selectionStart
        }, $.fn.getSelectionEnd = function(o) {
            if (o.createTextRange) {
                var r = document.selection.createRange().duplicate();
                return r.moveStart("character", -o.value.length), r.text.length
            }
            return o.selectionEnd
        }, $.fn.setSelection = function(o, p) {
            if ("number" == typeof p && (p = [p, p]), p && p.constructor == Array && 2 == p.length)
                if (o.createTextRange) {
                    var r = o.createTextRange();
                    r.collapse(!0), r.moveStart("character", p[0]), r.moveEnd("character", p[1]), r.select()
                } else o.setSelectionRange && (o.focus(), o.setSelectionRange(p[0], p[1]))
        }
    }(jQuery),
    function(a) {
        a.hrzAccordion = {
            resetAccordion: function(b, c) {
                var d = a("#" + b + " > li[rel=" + c.listItemSelected + "]");
                return d.attr("rel", ""), d.removeClass(c.listItemSelected), a("#" + b + " > li").animate({
                    width: Math.floor(c.containerWidth / c.numPanels)
                }, {
                    queue: !1,
                    duration: c.closeSpeed,
                    easing: c.closeEaseAction,
                    complete: function() {
                        a("." + c.contentContainerClass).css("width", "0px")
                    }
                }), 0
            },
            selectCurrent: function(b, c, d, e) {
                var f = a("#" + c + " > li:nth-child(" + (b + 1) + ")");
                a("#" + c + " > li[rel=" + e.listItemSelected + "]");
                return f.addClass(e.listItemSelected), f.siblings().animate({
                    width: Math.floor((e.containerWidth - e.fixedWidth) / (e.numPanels - 1))
                }, {
                    queue: !1,
                    duration: e.closeSpeed,
                    easing: e.closeEaseAction
                }), f.animate({
                    width: e.fixedWidth + "px",
                    opacity: "1"
                }, {
                    queue: !1,
                    duration: e.closeSpeed,
                    easing: e.closeEaseAction
                }), width = f.find("." + e.handleClass).width(), new_width = d - width - 2, a("#" + c + "Content" + b).css("width", new_width), 0
            },
            setOnEvent: function(b, c, d, e) {
                a("#" + c + " > li:nth-child(" + (b + 1) + ")").bind(e.eventTrigger, function() {
                    var f = a("[rel=" + e.listItemSelected + "]").data("status"),
                        g = a("#" + c + " > li:nth-child(" + (b + 1) + ")"),
                        h = a("#" + c + " > li[rel=" + e.listItemSelected + "]");
                    if (1 == f && e.eventWaitForAnim === !0) return !1;
                    if (g.attr("rel") != e.listItemSelected) {
                        switch (e.eventAction, e.closeOpenAnimation) {
                            case 1:
                                var j = a("#" + c + " > li." + e.listItemSelected).get(0);
                                j && (h.data("status", 1), h.data("status", a.hrzAccordion.resetAccordion(c, e)));
                                var k = setInterval(function() {
                                    j && a(j).is(":animated") || (clearInterval(k), h.data("status", 1), h.data("status", a.hrzAccordion.selectCurrent(b, c, d, e)))
                                }, 50);
                                break;
                            case 2:
                                a("[id*=" + c + "Content]").css({
                                    width: "0px"
                                }), a("#" + c + "Content" + b).animate({
                                    width: d + "px",
                                    opacity: "1"
                                }, {
                                    queue: !1,
                                    duration: e.openSpeed,
                                    easing: e.openEaseAction,
                                    complete: e.completeAction
                                })
                        }
                        g.attr("rel", e.listItemSelected)
                    } else h.data("status", 1), h.data("status", a.hrzAccordion.resetAccordion(c, e))
                })
            }
        }, a.fn.extend({
            hrzAccordionLoop: function(b) {
                return this.each(function(b) {
                    var c = a(this).attr("id") || a(this).attr("class"),
                        d = a("#" + c + " > li, ." + c + " > li").size(),
                        e = a(this).data("settings");
                    variable_holder = "interval" + c;
                    var f = 0,
                        g = "start";
                    variable_holder = window.setInterval(function() {
                        a("#" + c + "Handle" + f).trigger(e.eventTrigger), "start" == g ? f += 1 : f -= 1, f == d && "start" == g && (g = "end", f = d - 1), 0 == f && "end" == g && (g = "start", f = 0)
                    }, e.cycleInterval)
                })
            },
            hrzAccordion: function(b) {
                this.settings = {
                    eventTrigger: "click",
                    containerClass: "container",
                    listItemClass: "listItem",
                    listItemSelected: "listItemSelected",
                    contentContainerClass: "contentContainer",
                    contentWrapper: "contentWrapper",
                    contentInnerWrapper: "contentInnerWrapper",
                    handleClass: "handle",
                    handleClassOver: "handleOver",
                    handleClassSelected: "handleSelected",
                    handlePosition: "right",
                    handlePositionArray: "",
                    closeEaseAction: "swing",
                    closeSpeed: 500,
                    openEaseAction: "swing",
                    openSpeed: 500,
                    openOnLoad: 2,
                    hashPrefix: "tab",
                    eventAction: function() {},
                    completeAction: function() {},
                    closeOpenAnimation: 1,
                    cycle: !1,
                    cycleInterval: 1e4,
                    fixedWidth: "",
                    eventWaitForAnim: !0,
                    numPanels: 0,
                    containerWidth: 0
                }, b && a.extend(this.settings, b);
                var c = this.settings;
                return this.each(function(b) {
                    var d = a(this).attr("id") || a(this).attr("class");
                    a(this).data("settings", c), a(this).wrap("<div class='" + c.containerClass + "'></div>");
                    var e = a("#" + d + " > li, ." + d + " > li").size(),
                        f = a("." + c.containerClass).width(),
                        g = a("." + c.handleClass).css("width");
                    g = g.replace(/px/, "");
                    var h, i;
                    h = c.fixedWidth ? c.fixedWidth : f - e * g - g, a("#" + d + " > li, ." + d + " > li").each(function(b) {
                        if (a(this).attr("id", d + "ListItem" + b), a(this).addClass(c.listItemClass), a(this).html("<div class='" + c.contentContainerClass + "' id='" + d + "Content" + b + "'><div class=\"" + c.contentWrapper + '"><div class="' + c.contentInnerWrapper + '">' + a(this).html() + "</div></div></div>"), a("div", this).hasClass(c.handleClass)) {
                            var f = a("div." + c.handleClass, this).attr("id", "" + d + "Handle" + b).html();
                            a("div." + c.handleClass, this).remove(), i = '<div class="' + c.handleClass + "\" id='" + d + "Handle" + b + "'>" + f + "</div>"
                        } else i = '<div class="' + c.handleClass + "\" id='" + d + "Handle" + b + "'></div>";
                        switch (c.handlePositionArray && (splitthis = c.handlePositionArray.split(","), c.handlePosition = splitthis[b]), c.handlePosition) {
                            case "left":
                                a(this).prepend(i);
                                break;
                            case "right":
                                a(this).append(i);
                                break;
                            case "top":
                                a("." + d + "Top").append(i);
                                break;
                            case "bottom":
                                a("." + d + "Bottom").append(i)
                        }
                        if (a("#" + d + "Handle" + b).bind("mouseover", function() {
                                a("#" + d + "Handle" + b).addClass(c.handleClassOver)
                            }), a("#" + d + "Handle" + b).bind("mouseout", function() {
                                "selected" != a("#" + d + "Handle" + b).attr("rel") && a("#" + d + "Handle" + b).removeClass(c.handleClassOver)
                            }), a(this).html('<div class="card-wrap">' + a(this).html() + "</div>"), a.hrzAccordion.setOnEvent(b, d, h, c), b == e - 1 && a("#" + d + ",." + d).show(), c.openOnLoad !== !1 && b == e - 1) {
                            var g = location.hash;
                            if (g = g.replace("#", ""), "-1" != g.search(c.hashPrefix)) {
                                var j = 1;
                                g = g.replace(c.hashPrefix, "")
                            }
                            g && 1 == j ? (a("#" + d + "Handle" + g).attr("rel", d + "HandleSelected"), a("#" + d + "Content" + g).attr("rel", d + "ContainerSelected"), a("#" + d + "Handle" + (g - 1)).trigger(c.eventTrigger)) : (a("#" + d + "Handle" + c.openOnLoad).attr("rel", d + "HandleSelected"), a("#" + d + "Content" + c.openOnLoad).attr("rel", d + "ContainerSelected"), a("#" + d + "Handle" + (c.openOnLoad - 1)).trigger(c.eventTrigger))
                        }
                    }), c.cycle === !0 && a(this).hrzAccordionLoop()
                })
            }
        })
    }(jQuery);
var swfobject = function() {
    function f() {
        if (!L && document.getElementsByTagName("body")[0]) {
            try {
                var ac, ad = C("span");
                ad.style.display = "none", ac = h.getElementsByTagName("body")[0].appendChild(ad), ac.parentNode.removeChild(ac), ac = null, ad = null
            } catch (ae) {
                return
            }
            L = !0;
            for (var aa = X.length, ab = 0; aa > ab; ab++) X[ab]()
        }
    }

    function M(aa) {
        L ? aa() : X[X.length] = aa
    }

    function s(ab) {
        if (typeof Q.addEventListener != D) Q.addEventListener("load", ab, !1);
        else if (typeof h.addEventListener != D) h.addEventListener("load", ab, !1);
        else if (typeof Q.attachEvent != D) g(Q, "onload", ab);
        else if ("function" == typeof Q.onload) {
            var aa = Q.onload;
            Q.onload = function() {
                aa(), ab()
            }
        } else Q.onload = ab
    }

    function Y() {
        var aa = h.getElementsByTagName("body")[0],
            ae = C(r);
        ae.setAttribute("style", "visibility: hidden;"), ae.setAttribute("type", q);
        var ad = aa.appendChild(ae);
        if (ad) {
            var ac = 0;
            ! function ab() {
                if (typeof ad.GetVariable != D) try {
                    var ag = ad.GetVariable("$version");
                    ag && (ag = ag.split(" ")[1].split(","), O.pv = [n(ag[0]), n(ag[1]), n(ag[2])])
                } catch (af) {
                    O.pv = [8, 0, 0]
                } else if (10 > ac) return ac++, void setTimeout(ab, 10);
                aa.removeChild(ae), ad = null, H()
            }()
        } else H()
    }

    function H() {
        var aj = o.length;
        if (aj > 0)
            for (var ai = 0; aj > ai; ai++) {
                var ab = o[ai].id,
                    ae = o[ai].callbackFn,
                    ad = {
                        success: !1,
                        id: ab
                    };
                if (O.pv[0] > 0) {
                    var ah = c(ab);
                    if (ah)
                        if (!F(o[ai].swfVersion) || O.wk && O.wk < 312)
                            if (o[ai].expressInstall && A()) {
                                var al = {};
                                al.data = o[ai].expressInstall, al.width = ah.getAttribute("width") || "0", al.height = ah.getAttribute("height") || "0", ah.getAttribute("class") && (al.styleclass = ah.getAttribute("class")), ah.getAttribute("align") && (al.align = ah.getAttribute("align"));
                                for (var ak = {}, aa = ah.getElementsByTagName("param"), af = aa.length, ag = 0; af > ag; ag++) "movie" != aa[ag].getAttribute("name").toLowerCase() && (ak[aa[ag].getAttribute("name")] = aa[ag].getAttribute("value"));
                                R(al, ak, ab, ae)
                            } else b(ah), ae && ae(ad);
                    else w(ab, !0), ae && (ad.success = !0, ad.ref = z(ab), ad.id = ab, ae(ad))
                } else if (w(ab, !0), ae) {
                    var ac = z(ab);
                    ac && typeof ac.SetVariable != D && (ad.success = !0, ad.ref = ac, ad.id = ac.id), ae(ad)
                }
            }
    }

    function z(ac) {
        var aa = null,
            ab = c(ac);
        return ab && "OBJECT" === ab.nodeName.toUpperCase() && (aa = typeof ab.SetVariable !== D ? ab : ab.getElementsByTagName(r)[0] || ab), aa
    }

    function A() {
        return !a && F("6.0.65") && (O.win || O.mac) && !(O.wk && O.wk < 312)
    }

    function R(ad, ae, aa, ac) {
        var ah = c(aa);
        if (aa = W(aa), a = !0, E = ac || null, B = {
                success: !1,
                id: aa
            }, ah) {
            "OBJECT" == ah.nodeName.toUpperCase() ? (I = J(ah), p = null) : (I = ah, p = aa), ad.id = S, (typeof ad.width == D || !/%$/.test(ad.width) && n(ad.width) < 310) && (ad.width = "310"), (typeof ad.height == D || !/%$/.test(ad.height) && n(ad.height) < 137) && (ad.height = "137");
            var ag = O.ie ? "ActiveX" : "PlugIn",
                af = "MMredirectURL=" + encodeURIComponent(Q.location.toString().replace(/&/g, "%26")) + "&MMplayerType=" + ag + "&MMdoctitle=" + encodeURIComponent(h.title.slice(0, 47) + " - Flash Player Installation");
            if (typeof ae.flashvars != D ? ae.flashvars += "&" + af : ae.flashvars = af, O.ie && 4 != ah.readyState) {
                var ab = C("div");
                aa += "SWFObjectNew", ab.setAttribute("id", aa), ah.parentNode.insertBefore(ab, ah), ah.style.display = "none", y(ah)
            }
            u(ad, ae, aa)
        }
    }

    function b(ab) {
        if (O.ie && 4 != ab.readyState) {
            ab.style.display = "none";
            var aa = C("div");
            ab.parentNode.insertBefore(aa, ab), aa.parentNode.replaceChild(J(ab), aa), y(ab)
        } else ab.parentNode.replaceChild(J(ab), ab)
    }

    function J(af) {
        var ae = C("div");
        if (O.win && O.ie) ae.innerHTML = af.innerHTML;
        else {
            var ab = af.getElementsByTagName(r)[0];
            if (ab) {
                var ag = ab.childNodes;
                if (ag)
                    for (var aa = ag.length, ad = 0; aa > ad; ad++) 1 == ag[ad].nodeType && "PARAM" == ag[ad].nodeName || 8 == ag[ad].nodeType || ae.appendChild(ag[ad].cloneNode(!0))
            }
        }
        return ae
    }

    function k(aa, ab) {
        var ac = C("div");
        return ac.innerHTML = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'><param name='movie' value='" + aa + "'>" + ab + "</object>", ac.firstChild
    }

    function u(ai, ag, ab) {
        var aa, ad = c(ab);
        if (ab = W(ab), O.wk && O.wk < 312) return aa;
        if (ad) {
            var af, ah, ae, ac = C(O.ie ? "div" : r);
            typeof ai.id == D && (ai.id = ab);
            for (ae in ag) ag.hasOwnProperty(ae) && "movie" !== ae.toLowerCase() && e(ac, ae, ag[ae]);
            O.ie && (ac = k(ai.data, ac.innerHTML));
            for (af in ai) ai.hasOwnProperty(af) && (ah = af.toLowerCase(), "styleclass" === ah ? ac.setAttribute("class", ai[af]) : "classid" !== ah && "data" !== ah && ac.setAttribute(af, ai[af]));
            O.ie ? P[P.length] = ai.id : (ac.setAttribute("type", q), ac.setAttribute("data", ai.data)), ad.parentNode.replaceChild(ac, ad), aa = ac
        }
        return aa
    }

    function e(ac, aa, ab) {
        var ad = C("param");
        ad.setAttribute("name", aa), ad.setAttribute("value", ab), ac.appendChild(ad)
    }

    function y(ac) {
        var ab = c(ac);
        ab && "OBJECT" == ab.nodeName.toUpperCase() && (O.ie ? (ab.style.display = "none", function aa() {
            if (4 == ab.readyState) {
                for (var ad in ab) "function" == typeof ab[ad] && (ab[ad] = null);
                ab.parentNode.removeChild(ab)
            } else setTimeout(aa, 10)
        }()) : ab.parentNode.removeChild(ab))
    }

    function U(aa) {
        return aa && aa.nodeType && 1 === aa.nodeType
    }

    function W(aa) {
        return U(aa) ? aa.id : aa
    }

    function c(ac) {
        if (U(ac)) return ac;
        var aa = null;
        try {
            aa = h.getElementById(ac)
        } catch (ab) {}
        return aa
    }

    function C(aa) {
        return h.createElement(aa)
    }

    function n(aa) {
        return parseInt(aa, 10)
    }

    function g(ac, aa, ab) {
        ac.attachEvent(aa, ab), K[K.length] = [ac, aa, ab]
    }

    function F(ac) {
        ac += "";
        var ab = O.pv,
            aa = ac.split(".");
        return aa[0] = n(aa[0]), aa[1] = n(aa[1]) || 0, aa[2] = n(aa[2]) || 0, ab[0] > aa[0] || ab[0] == aa[0] && ab[1] > aa[1] || ab[0] == aa[0] && ab[1] == aa[1] && ab[2] >= aa[2] ? !0 : !1
    }

    function v(af, ab, ag, ae) {
        var ad = h.getElementsByTagName("head")[0];
        if (ad) {
            var aa = "string" == typeof ag ? ag : "screen";
            if (ae && (m = null, G = null), !m || G != aa) {
                var ac = C("style");
                ac.setAttribute("type", "text/css"), ac.setAttribute("media", aa), m = ad.appendChild(ac), O.ie && typeof h.styleSheets != D && h.styleSheets.length > 0 && (m = h.styleSheets[h.styleSheets.length - 1]), G = aa
            }
            m && (typeof m.addRule != D ? m.addRule(af, ab) : typeof h.createTextNode != D && m.appendChild(h.createTextNode(af + " {" + ab + "}")))
        }
    }

    function w(ad, aa) {
        if (j) {
            var ab = aa ? "visible" : "hidden",
                ac = c(ad);
            L && ac ? ac.style.visibility = ab : "string" == typeof ad && v("#" + ad, "visibility:" + ab)
        }
    }

    function N(ab) {
        var ac = /[\\\"<>\.;]/,
            aa = null != ac.exec(ab);
        return aa && typeof encodeURIComponent != D ? encodeURIComponent(ab) : ab
    }
    var I, p, E, B, m, G, D = "undefined",
        r = "object",
        T = "Shockwave Flash",
        Z = "ShockwaveFlash.ShockwaveFlash",
        q = "application/x-shockwave-flash",
        S = "SWFObjectExprInst",
        x = "onreadystatechange",
        Q = window,
        h = document,
        t = navigator,
        V = !1,
        X = [],
        o = [],
        P = [],
        K = [],
        L = !1,
        a = !1,
        j = !0,
        l = !1,
        O = function() {
            var ad = typeof h.getElementById != D && typeof h.getElementsByTagName != D && typeof h.createElement != D,
                ak = t.userAgent.toLowerCase(),
                ab = t.platform.toLowerCase(),
                ah = ab ? /win/.test(ab) : /win/.test(ak),
                af = ab ? /mac/.test(ab) : /mac/.test(ak),
                ai = /webkit/.test(ak) ? parseFloat(ak.replace(/^.*webkit\/(\d+(\.\d+)?).*$/, "$1")) : !1,
                aa = "Microsoft Internet Explorer" === t.appName,
                aj = [0, 0, 0],
                ae = null;
            if (typeof t.plugins != D && typeof t.plugins[T] == r) ae = t.plugins[T].description, ae && typeof t.mimeTypes != D && t.mimeTypes[q] && t.mimeTypes[q].enabledPlugin && (V = !0, aa = !1, ae = ae.replace(/^.*\s+(\S+\s+\S+$)/, "$1"), aj[0] = n(ae.replace(/^(.*)\..*$/, "$1")), aj[1] = n(ae.replace(/^.*\.(.*)\s.*$/, "$1")), aj[2] = /[a-zA-Z]/.test(ae) ? n(ae.replace(/^.*[a-zA-Z]+(.*)$/, "$1")) : 0);
            else if (typeof Q.ActiveXObject != D) try {
                var ag = new ActiveXObject(Z);
                ag && (ae = ag.GetVariable("$version"), ae && (aa = !0, ae = ae.split(" ")[1].split(","), aj = [n(ae[0]), n(ae[1]), n(ae[2])]))
            } catch (ac) {}
            return {
                w3: ad,
                pv: aj,
                wk: ai,
                ie: aa,
                win: ah,
                mac: af
            }
        }();
    (function() {
        O.w3 && ((typeof h.readyState != D && ("complete" === h.readyState || "interactive" === h.readyState) || typeof h.readyState == D && (h.getElementsByTagName("body")[0] || h.body)) && f(), L || (typeof h.addEventListener != D && h.addEventListener("DOMContentLoaded", f, !1), O.ie && (h.attachEvent(x, function aa() {
            "complete" == h.readyState && (h.detachEvent(x, aa), f())
        }), Q == top && ! function ac() {
            if (!L) {
                try {
                    h.documentElement.doScroll("left")
                } catch (ad) {
                    return void setTimeout(ac, 0)
                }
                f()
            }
        }()), O.wk && ! function ab() {
            return L ? void 0 : /loaded|complete/.test(h.readyState) ? void f() : void setTimeout(ab, 0)
        }()))
    })();
    X[0] = function() {
        V ? Y() : H()
    };
    (function() {
        O.ie && window.attachEvent("onunload", function() {
            for (var af = K.length, ae = 0; af > ae; ae++) K[ae][0].detachEvent(K[ae][1], K[ae][2]);
            for (var ac = P.length, ad = 0; ac > ad; ad++) y(P[ad]);
            for (var ab in O) O[ab] = null;
            O = null;
            for (var aa in swfobject) swfobject[aa] = null;
            swfobject = null
        })
    })();
    return {
        registerObject: function(ae, aa, ad, ac) {
            if (O.w3 && ae && aa) {
                var ab = {};
                ab.id = ae, ab.swfVersion = aa, ab.expressInstall = ad, ab.callbackFn = ac, o[o.length] = ab, w(ae, !1)
            } else ac && ac({
                success: !1,
                id: ae
            })
        },
        getObjectById: function(aa) {
            return O.w3 ? z(aa) : void 0
        },
        embedSWF: function(af, al, ai, ak, ab, ae, ad, ah, aj, ag) {
            var ac = W(al),
                aa = {
                    success: !1,
                    id: ac
                };
            O.w3 && !(O.wk && O.wk < 312) && af && al && ai && ak && ab ? (w(ac, !1), M(function() {
                ai += "", ak += "";
                var an = {};
                if (aj && typeof aj === r)
                    for (var aq in aj) an[aq] = aj[aq];
                an.data = af, an.width = ai, an.height = ak;
                var ar = {};
                if (ah && typeof ah === r)
                    for (var ao in ah) ar[ao] = ah[ao];
                if (ad && typeof ad === r)
                    for (var am in ad)
                        if (ad.hasOwnProperty(am)) {
                            var ap = l ? encodeURIComponent(am) : am,
                                at = l ? encodeURIComponent(ad[am]) : ad[am];
                            typeof ar.flashvars != D ? ar.flashvars += "&" + ap + "=" + at : ar.flashvars = ap + "=" + at
                        }
                if (F(ab)) {
                    var au = u(an, ar, al);
                    an.id == ac && w(ac, !0), aa.success = !0, aa.ref = au, aa.id = au.id
                } else {
                    if (ae && A()) return an.data = ae, void R(an, ar, al, ag);
                    w(ac, !0)
                }
                ag && ag(aa)
            })) : ag && ag(aa)
        },
        switchOffAutoHideShow: function() {
            j = !1
        },
        enableUriEncoding: function(aa) {
            l = typeof aa === D ? !0 : aa
        },
        ua: O,
        getFlashPlayerVersion: function() {
            return {
                major: O.pv[0],
                minor: O.pv[1],
                release: O.pv[2]
            }
        },
        hasFlashPlayerVersion: F,
        createSWF: function(ac, ab, aa) {
            return O.w3 ? u(ac, ab, aa) : void 0
        },
        showExpressInstall: function(ac, ad, aa, ab) {
            O.w3 && A() && R(ac, ad, aa, ab)
        },
        removeSWF: function(aa) {
            O.w3 && y(aa)
        },
        createCSS: function(ad, ac, ab, aa) {
            O.w3 && v(ad, ac, ab, aa)
        },
        addDomLoadEvent: M,
        addLoadEvent: s,
        getQueryParamValue: function(ad) {
            var ac = h.location.search || h.location.hash;
            if (ac) {
                if (/\?/.test(ac) && (ac = ac.split("?")[1]), null == ad) return N(ac);
                for (var ab = ac.split("&"), aa = 0; aa < ab.length; aa++)
                    if (ab[aa].substring(0, ab[aa].indexOf("=")) == ad) return N(ab[aa].substring(ab[aa].indexOf("=") + 1))
            }
            return ""
        },
        expressInstallCallback: function() {
            if (a) {
                var aa = c(S);
                aa && I && (aa.parentNode.replaceChild(I, aa), p && (w(p, !0), O.ie && (I.style.display = "block")), E && E(B)), a = !1
            }
        },
        version: "2.3"
    }
}();
! function(a, b) {
    var c = function(c) {
        return a(c, b)
    };
    "function" == typeof define && define.amd ? define(["jquery"], c) : "object" == typeof exports ? module.exports = c : c(jQuery)
}(function(a, b, c) {
    a.fn.jScrollPane = function(d) {
        function e(d, e) {
            function f(b) {
                var e, h, j, l, m, n, q = !1,
                    r = !1;
                if (P = b, Q === c) m = d.scrollTop(), n = d.scrollLeft(), d.css({
                    overflow: "hidden",
                    padding: 0
                }), R = d.innerWidth() + tb, S = d.innerHeight(), d.width(R), Q = a('<div class="jspPane" />').css("padding", sb).append(d.children()), T = a('<div class="jspContainer" />').css({
                    width: R + "px",
                    height: S + "px"
                }).append(Q).appendTo(d);
                else {
                    if (d.css("width", ""), q = P.stickToBottom && C(), r = P.stickToRight && D(), l = d.innerWidth() + tb != R || d.outerHeight() != S, l && (R = d.innerWidth() + tb, S = d.innerHeight(), T.css({
                            width: R + "px",
                            height: S + "px"
                        })), !l && ub == U && Q.outerHeight() == V) return void d.width(R);
                    ub = U, Q.css("width", ""), d.width(R), T.find(">.jspVerticalBar,>.jspHorizontalBar").remove().end()
                }
                Q.css("overflow", "auto"), U = b.contentWidth ? b.contentWidth : Q[0].scrollWidth, V = Q[0].scrollHeight, Q.css("overflow", ""), W = U / R, X = V / S, Y = X > 1, Z = W > 1, Z || Y ? (d.addClass("jspScrollable"), e = P.maintainPosition && (ab || db), e && (h = A(), j = B()), g(), i(), k(), e && (y(r ? U - R : h, !1), x(q ? V - S : j, !1)), H(), E(), N(), P.enableKeyboardNavigation && J(), P.clickOnTrack && o(), L(), P.hijackInternalLinks && M()) : (d.removeClass("jspScrollable"), Q.css({
                    top: 0,
                    left: 0,
                    width: T.width() - tb
                }), F(), I(), K(), p()), P.autoReinitialise && !rb ? rb = setInterval(function() {
                    f(P)
                }, P.autoReinitialiseDelay) : !P.autoReinitialise && rb && clearInterval(rb), m && d.scrollTop(0) && x(m, !1), n && d.scrollLeft(0) && y(n, !1), d.trigger("jsp-initialised", [Z || Y])
            }

            function g() {
                Y && (T.append(a('<div class="jspVerticalBar" />').append(a('<div class="jspCap jspCapTop" />'), a('<div class="jspTrack" />').append(a('<div class="jspDrag" />').append(a('<div class="jspDragTop" />'), a('<div class="jspDragBottom" />'))), a('<div class="jspCap jspCapBottom" />'))), eb = T.find(">.jspVerticalBar"), fb = eb.find(">.jspTrack"), $ = fb.find(">.jspDrag"), P.showArrows && (jb = a('<a class="jspArrow jspArrowUp" />').bind("mousedown.jsp", m(0, -1)).bind("click.jsp", G), kb = a('<a class="jspArrow jspArrowDown" />').bind("mousedown.jsp", m(0, 1)).bind("click.jsp", G), P.arrowScrollOnHover && (jb.bind("mouseover.jsp", m(0, -1, jb)), kb.bind("mouseover.jsp", m(0, 1, kb))), l(fb, P.verticalArrowPositions, jb, kb)), hb = S, T.find(">.jspVerticalBar>.jspCap:visible,>.jspVerticalBar>.jspArrow").each(function() {
                    hb -= a(this).outerHeight()
                }), $.hover(function() {
                    $.addClass("jspHover")
                }, function() {
                    $.removeClass("jspHover")
                }).bind("mousedown.jsp", function(b) {
                    a("html").bind("dragstart.jsp selectstart.jsp", G), $.addClass("jspActive");
                    var c = b.pageY - $.position().top;
                    return a("html").bind("mousemove.jsp", function(a) {
                        r(a.pageY - c, !1)
                    }).bind("mouseup.jsp mouseleave.jsp", q), !1
                }), h())
            }

            function h() {
                fb.height(hb + "px"), ab = 0, gb = P.verticalGutter + fb.outerWidth(), Q.width(R - gb - tb);
                try {
                    0 === eb.position().left && Q.css("margin-left", gb + "px")
                } catch (a) {}
            }

            function i() {
                Z && (T.append(a('<div class="jspHorizontalBar" />').append(a('<div class="jspCap jspCapLeft" />'), a('<div class="jspTrack" />').append(a('<div class="jspDrag" />').append(a('<div class="jspDragLeft" />'), a('<div class="jspDragRight" />'))), a('<div class="jspCap jspCapRight" />'))), lb = T.find(">.jspHorizontalBar"), mb = lb.find(">.jspTrack"), bb = mb.find(">.jspDrag"), P.showArrows && (pb = a('<a class="jspArrow jspArrowLeft" />').bind("mousedown.jsp", m(-1, 0)).bind("click.jsp", G), qb = a('<a class="jspArrow jspArrowRight" />').bind("mousedown.jsp", m(1, 0)).bind("click.jsp", G), P.arrowScrollOnHover && (pb.bind("mouseover.jsp", m(-1, 0, pb)), qb.bind("mouseover.jsp", m(1, 0, qb))), l(mb, P.horizontalArrowPositions, pb, qb)), bb.hover(function() {
                    bb.addClass("jspHover")
                }, function() {
                    bb.removeClass("jspHover")
                }).bind("mousedown.jsp", function(b) {
                    a("html").bind("dragstart.jsp selectstart.jsp", G), bb.addClass("jspActive");
                    var c = b.pageX - bb.position().left;
                    return a("html").bind("mousemove.jsp", function(a) {
                        t(a.pageX - c, !1)
                    }).bind("mouseup.jsp mouseleave.jsp", q), !1
                }), nb = T.innerWidth(), j())
            }

            function j() {
                T.find(">.jspHorizontalBar>.jspCap:visible,>.jspHorizontalBar>.jspArrow").each(function() {
                    nb -= a(this).outerWidth()
                }), mb.width(nb + "px"), db = 0
            }

            function k() {
                if (Z && Y) {
                    var b = mb.outerHeight(),
                        c = fb.outerWidth();
                    hb -= b, a(lb).find(">.jspCap:visible,>.jspArrow").each(function() {
                        nb += a(this).outerWidth()
                    }), nb -= c, S -= c, R -= b, mb.parent().append(a('<div class="jspCorner" />').css("width", b + "px")), h(), j()
                }
                Z && Q.width(T.outerWidth() - tb + "px"), V = Q.outerHeight(), X = V / S, Z && (ob = Math.ceil(1 / W * nb), ob > P.horizontalDragMaxWidth ? ob = P.horizontalDragMaxWidth : ob < P.horizontalDragMinWidth && (ob = P.horizontalDragMinWidth), bb.width(ob + "px"), cb = nb - ob, u(db)), Y && (ib = Math.ceil(1 / X * hb), ib > P.verticalDragMaxHeight ? ib = P.verticalDragMaxHeight : ib < P.verticalDragMinHeight && (ib = P.verticalDragMinHeight), $.height(ib + "px"), _ = hb - ib, s(ab))
            }

            function l(a, b, c, d) {
                var e, f = "before",
                    g = "after";
                "os" == b && (b = /Mac/.test(navigator.platform) ? "after" : "split"), b == f ? g = b : b == g && (f = b, e = c, c = d, d = e), a[f](c)[g](d)
            }

            function m(a, b, c) {
                return function() {
                    return n(a, b, this, c), this.blur(), !1
                }
            }

            function n(b, c, d, e) {
                d = a(d).addClass("jspActive");
                var f, g, h = !0,
                    i = function() {
                        0 !== b && vb.scrollByX(b * P.arrowButtonSpeed), 0 !== c && vb.scrollByY(c * P.arrowButtonSpeed), g = setTimeout(i, h ? P.initialDelay : P.arrowRepeatFreq), h = !1
                    };
                i(), f = e ? "mouseout.jsp" : "mouseup.jsp", e = e || a("html"), e.bind(f, function() {
                    d.removeClass("jspActive"), g && clearTimeout(g), g = null, e.unbind(f)
                })
            }

            function o() {
                p(), Y && fb.bind("mousedown.jsp", function(b) {
                    if (b.originalTarget === c || b.originalTarget == b.currentTarget) {
                        var d, e = a(this),
                            f = e.offset(),
                            g = b.pageY - f.top - ab,
                            h = !0,
                            i = function() {
                                var a = e.offset(),
                                    c = b.pageY - a.top - ib / 2,
                                    f = S * P.scrollPagePercent,
                                    k = _ * f / (V - S);
                                if (0 > g) ab - k > c ? vb.scrollByY(-f) : r(c);
                                else {
                                    if (!(g > 0)) return void j();
                                    c > ab + k ? vb.scrollByY(f) : r(c)
                                }
                                d = setTimeout(i, h ? P.initialDelay : P.trackClickRepeatFreq), h = !1
                            },
                            j = function() {
                                d && clearTimeout(d), d = null, a(document).unbind("mouseup.jsp", j)
                            };
                        return i(), a(document).bind("mouseup.jsp", j), !1
                    }
                }), Z && mb.bind("mousedown.jsp", function(b) {
                    if (b.originalTarget === c || b.originalTarget == b.currentTarget) {
                        var d, e = a(this),
                            f = e.offset(),
                            g = b.pageX - f.left - db,
                            h = !0,
                            i = function() {
                                var a = e.offset(),
                                    c = b.pageX - a.left - ob / 2,
                                    f = R * P.scrollPagePercent,
                                    k = cb * f / (U - R);
                                if (0 > g) db - k > c ? vb.scrollByX(-f) : t(c);
                                else {
                                    if (!(g > 0)) return void j();
                                    c > db + k ? vb.scrollByX(f) : t(c)
                                }
                                d = setTimeout(i, h ? P.initialDelay : P.trackClickRepeatFreq), h = !1
                            },
                            j = function() {
                                d && clearTimeout(d), d = null, a(document).unbind("mouseup.jsp", j)
                            };
                        return i(), a(document).bind("mouseup.jsp", j), !1
                    }
                })
            }

            function p() {
                mb && mb.unbind("mousedown.jsp"), fb && fb.unbind("mousedown.jsp")
            }

            function q() {
                a("html").unbind("dragstart.jsp selectstart.jsp mousemove.jsp mouseup.jsp mouseleave.jsp"), $ && $.removeClass("jspActive"), bb && bb.removeClass("jspActive")
            }

            function r(a, b) {
                Y && (0 > a ? a = 0 : a > _ && (a = _), b === c && (b = P.animateScroll), b ? vb.animate($, "top", a, s) : ($.css("top", a), s(a)))
            }

            function s(a) {
                a === c && (a = $.position().top), T.scrollTop(0), ab = a;
                var b = 0 === ab,
                    e = ab == _,
                    f = a / _,
                    g = -f * (V - S);
                (wb != b || yb != e) && (wb = b, yb = e, d.trigger("jsp-arrow-change", [wb, yb, xb, zb])), v(b, e), Q.css("top", g), d.trigger("jsp-scroll-y", [-g, b, e]).trigger("scroll")
            }

            function t(a, b) {
                Z && (0 > a ? a = 0 : a > cb && (a = cb), b === c && (b = P.animateScroll), b ? vb.animate(bb, "left", a, u) : (bb.css("left", a), u(a)))
            }

            function u(a) {
                a === c && (a = bb.position().left), T.scrollTop(0), db = a;
                var b = 0 === db,
                    e = db == cb,
                    f = a / cb,
                    g = -f * (U - R);
                (xb != b || zb != e) && (xb = b, zb = e, d.trigger("jsp-arrow-change", [wb, yb, xb, zb])), w(b, e), Q.css("left", g), d.trigger("jsp-scroll-x", [-g, b, e]).trigger("scroll")
            }

            function v(a, b) {
                P.showArrows && (jb[a ? "addClass" : "removeClass"]("jspDisabled"), kb[b ? "addClass" : "removeClass"]("jspDisabled"))
            }

            function w(a, b) {
                P.showArrows && (pb[a ? "addClass" : "removeClass"]("jspDisabled"), qb[b ? "addClass" : "removeClass"]("jspDisabled"))
            }

            function x(a, b) {
                var c = a / (V - S);
                r(c * _, b)
            }

            function y(a, b) {
                var c = a / (U - R);
                t(c * cb, b)
            }

            function z(b, c, d) {
                var e, f, g, h, i, j, k, l, m, n = 0,
                    o = 0;
                try {
                    e = a(b)
                } catch (p) {
                    return
                }
                for (f = e.outerHeight(), g = e.outerWidth(), T.scrollTop(0), T.scrollLeft(0); !e.is(".jspPane");)
                    if (n += e.position().top, o += e.position().left, e = e.offsetParent(), /^body|html$/i.test(e[0].nodeName)) return;
                h = B(), j = h + S, h > n || c ? l = n - P.horizontalGutter : n + f > j && (l = n - S + f + P.horizontalGutter), isNaN(l) || x(l, d), i = A(), k = i + R, i > o || c ? m = o - P.horizontalGutter : o + g > k && (m = o - R + g + P.horizontalGutter), isNaN(m) || y(m, d)
            }

            function A() {
                return -Q.position().left
            }

            function B() {
                return -Q.position().top
            }

            function C() {
                var a = V - S;
                return a > 20 && a - B() < 10
            }

            function D() {
                var a = U - R;
                return a > 20 && a - A() < 10
            }

            function E() {
                T.unbind(Bb).bind(Bb, function(a, b, c, d) {
                    var e = db,
                        f = ab,
                        g = a.deltaFactor || P.mouseWheelSpeed;
                    return vb.scrollBy(c * g, -d * g, !1), e == db && f == ab
                })
            }

            function F() {
                T.unbind(Bb)
            }

            function G() {
                return !1
            }

            function H() {
                Q.find(":input,a").unbind("focus.jsp").bind("focus.jsp", function(a) {
                    z(a.target, !1)
                })
            }

            function I() {
                Q.find(":input,a").unbind("focus.jsp")
            }

            function J() {
                function b() {
                    var a = db,
                        b = ab;
                    switch (c) {
                        case 40:
                            vb.scrollByY(P.keyboardSpeed, !1);
                            break;
                        case 38:
                            vb.scrollByY(-P.keyboardSpeed, !1);
                            break;
                        case 34:
                        case 32:
                            vb.scrollByY(S * P.scrollPagePercent, !1);
                            break;
                        case 33:
                            vb.scrollByY(-S * P.scrollPagePercent, !1);
                            break;
                        case 39:
                            vb.scrollByX(P.keyboardSpeed, !1);
                            break;
                        case 37:
                            vb.scrollByX(-P.keyboardSpeed, !1)
                    }
                    return e = a != db || b != ab
                }
                var c, e, f = [];
                Z && f.push(lb[0]), Y && f.push(eb[0]), Q.focus(function() {
                    d.focus()
                }), d.attr("tabindex", 0).unbind("keydown.jsp keypress.jsp").bind("keydown.jsp", function(d) {
                    if (d.target === this || f.length && a(d.target).closest(f).length) {
                        var g = db,
                            h = ab;
                        switch (d.keyCode) {
                            case 40:
                            case 38:
                            case 34:
                            case 32:
                            case 33:
                            case 39:
                            case 37:
                                c = d.keyCode, b();
                                break;
                            case 35:
                                x(V - S), c = null;
                                break;
                            case 36:
                                x(0), c = null
                        }
                        return e = d.keyCode == c && g != db || h != ab, !e
                    }
                }).bind("keypress.jsp", function(a) {
                    return a.keyCode == c && b(), !e
                }), P.hideFocus ? (d.css("outline", "none"), "hideFocus" in T[0] && d.attr("hideFocus", !0)) : (d.css("outline", ""), "hideFocus" in T[0] && d.attr("hideFocus", !1))
            }

            function K() {
                d.attr("tabindex", "-1").removeAttr("tabindex").unbind("keydown.jsp keypress.jsp")
            }

            function L() {
                if (location.hash && location.hash.length > 1) {
                    var b, c, d = escape(location.hash.substr(1));
                    try {
                        b = a("#" + d + ', a[name="' + d + '"]')
                    } catch (e) {
                        return
                    }
                    b.length && Q.find(d) && (0 === T.scrollTop() ? c = setInterval(function() {
                        T.scrollTop() > 0 && (z(b, !0), a(document).scrollTop(T.position().top), clearInterval(c))
                    }, 50) : (z(b, !0), a(document).scrollTop(T.position().top)))
                }
            }

            function M() {
                a(document.body).data("jspHijack") || (a(document.body).data("jspHijack", !0), a(document.body).delegate("a[href*=#]", "click", function(c) {
                    var d, e, f, g, h, i, j = this.href.substr(0, this.href.indexOf("#")),
                        k = location.href;
                    if (-1 !== location.href.indexOf("#") && (k = location.href.substr(0, location.href.indexOf("#"))), j === k) {
                        d = escape(this.href.substr(this.href.indexOf("#") + 1));
                        try {
                            e = a("#" + d + ', a[name="' + d + '"]')
                        } catch (l) {
                            return
                        }
                        e.length && (f = e.closest(".jspScrollable"), g = f.data("jsp"), g.scrollToElement(e, !0), f[0].scrollIntoView && (h = a(b).scrollTop(), i = e.offset().top, (h > i || i > h + a(b).height()) && f[0].scrollIntoView()), c.preventDefault())
                    }
                }))
            }

            function N() {
                var a, b, c, d, e, f = !1;
                T.unbind("touchstart.jsp touchmove.jsp touchend.jsp click.jsp-touchclick").bind("touchstart.jsp", function(g) {
                    var h = g.originalEvent.touches[0];
                    a = A(), b = B(), c = h.pageX, d = h.pageY, e = !1, f = !0
                }).bind("touchmove.jsp", function(g) {
                    if (f) {
                        var h = g.originalEvent.touches[0],
                            i = db,
                            j = ab;
                        return vb.scrollTo(a + c - h.pageX, b + d - h.pageY), e = e || Math.abs(c - h.pageX) > 5 || Math.abs(d - h.pageY) > 5, i == db && j == ab
                    }
                }).bind("touchend.jsp", function() {
                    f = !1
                }).bind("click.jsp-touchclick", function() {
                    return e ? (e = !1, !1) : void 0
                })
            }

            function O() {
                var a = B(),
                    b = A();
                d.removeClass("jspScrollable").unbind(".jsp"), d.replaceWith(Ab.append(Q.children())), Ab.scrollTop(a), Ab.scrollLeft(b), rb && clearInterval(rb)
            }
            var P, Q, R, S, T, U, V, W, X, Y, Z, $, _, ab, bb, cb, db, eb, fb, gb, hb, ib, jb, kb, lb, mb, nb, ob, pb, qb, rb, sb, tb, ub, vb = this,
                wb = !0,
                xb = !0,
                yb = !1,
                zb = !1,
                Ab = d.clone(!1, !1).empty(),
                Bb = a.fn.mwheelIntent ? "mwheelIntent.jsp" : "mousewheel.jsp";
            "border-box" === d.css("box-sizing") ? (sb = 0, tb = 0) : (sb = d.css("paddingTop") + " " + d.css("paddingRight") + " " + d.css("paddingBottom") + " " + d.css("paddingLeft"), tb = (parseInt(d.css("paddingLeft"), 10) || 0) + (parseInt(d.css("paddingRight"), 10) || 0)), a.extend(vb, {
                reinitialise: function(b) {
                    b = a.extend({}, P, b), f(b)
                },
                scrollToElement: function(a, b, c) {
                    z(a, b, c)
                },
                scrollTo: function(a, b, c) {
                    y(a, c), x(b, c)
                },
                scrollToX: function(a, b) {
                    y(a, b)
                },
                scrollToY: function(a, b) {
                    x(a, b)
                },
                scrollToPercentX: function(a, b) {
                    y(a * (U - R), b)
                },
                scrollToPercentY: function(a, b) {
                    x(a * (V - S), b)
                },
                scrollBy: function(a, b, c) {
                    vb.scrollByX(a, c), vb.scrollByY(b, c)
                },
                scrollByX: function(a, b) {
                    var c = A() + Math[0 > a ? "floor" : "ceil"](a),
                        d = c / (U - R);
                    t(d * cb, b)
                },
                scrollByY: function(a, b) {
                    var c = B() + Math[0 > a ? "floor" : "ceil"](a),
                        d = c / (V - S);
                    r(d * _, b)
                },
                positionDragX: function(a, b) {
                    t(a, b)
                },
                positionDragY: function(a, b) {
                    r(a, b)
                },
                animate: function(a, b, c, d) {
                    var e = {};
                    e[b] = c, a.animate(e, {
                        duration: P.animateDuration,
                        easing: P.animateEase,
                        queue: !1,
                        step: d
                    })
                },
                getContentPositionX: function() {
                    return A()
                },
                getContentPositionY: function() {
                    return B()
                },
                getContentWidth: function() {
                    return U
                },
                getContentHeight: function() {
                    return V
                },
                getPercentScrolledX: function() {
                    return A() / (U - R)
                },
                getPercentScrolledY: function() {
                    return B() / (V - S)
                },
                getIsScrollableH: function() {
                    return Z
                },
                getIsScrollableV: function() {
                    return Y
                },
                getContentPane: function() {
                    return Q
                },
                scrollToBottom: function(a) {
                    r(_, a);
                },
                hijackInternalLinks: a.noop,
                destroy: function() {
                    O()
                }
            }), f(e)
        }
        return d = a.extend({}, a.fn.jScrollPane.defaults, d), a.each(["arrowButtonSpeed", "trackClickSpeed", "keyboardSpeed"], function() {
            d[this] = d[this] || d.speed
        }), this.each(function() {
            var b = a(this),
                c = b.data("jsp");
            c ? c.reinitialise(d) : (a("script", b).filter('[type="text/javascript"],:not([type])').remove(), c = new e(b, d), b.data("jsp", c))
        })
    }, a.fn.jScrollPane.defaults = {
        showArrows: !1,
        maintainPosition: !0,
        stickToBottom: !1,
        stickToRight: !1,
        clickOnTrack: !0,
        autoReinitialise: !1,
        autoReinitialiseDelay: 500,
        verticalDragMinHeight: 0,
        verticalDragMaxHeight: 99999,
        horizontalDragMinWidth: 0,
        horizontalDragMaxWidth: 99999,
        contentWidth: c,
        animateScroll: !1,
        animateDuration: 300,
        animateEase: "linear",
        hijackInternalLinks: !1,
        verticalGutter: 4,
        horizontalGutter: 4,
        mouseWheelSpeed: 3,
        arrowButtonSpeed: 0,
        arrowRepeatFreq: 50,
        arrowScrollOnHover: !1,
        trackClickSpeed: 0,
        trackClickRepeatFreq: 70,
        verticalArrowPositions: "split",
        horizontalArrowPositions: "split",
        enableKeyboardNavigation: !0,
        hideFocus: !1,
        keyboardSpeed: 0,
        initialDelay: 300,
        speed: 30,
        scrollPagePercent: .8
    }
}, this),
function(d) {
    "function" == typeof define && define.amd ? define(["jquery"], d) : "object" == typeof exports ? module.exports = d : d(jQuery)
}(function(d) {
    function m(a) {
        var c = a || window.event,
            k = r.call(arguments, 1),
            f = 0,
            e = 0,
            b = 0,
            g = 0;
        return a = d.event.fix(c), a.type = "mousewheel", "detail" in c && (b = -1 * c.detail), "wheelDelta" in c && (b = c.wheelDelta), "wheelDeltaY" in c && (b = c.wheelDeltaY), "wheelDeltaX" in c && (e = -1 * c.wheelDeltaX), "axis" in c && c.axis === c.HORIZONTAL_AXIS && (e = -1 * b, b = 0), f = 0 === b ? e : b, "deltaY" in c && (f = b = -1 * c.deltaY), "deltaX" in c && (e = c.deltaX, 0 === b && (f = -1 * e)), 0 !== b || 0 !== e ? (1 === c.deltaMode ? (g = d.data(this, "mousewheel-line-height"), f *= g, b *= g, e *= g) : 2 === c.deltaMode && (g = d.data(this, "mousewheel-page-height"), f *= g, b *= g, e *= g), g = Math.max(Math.abs(b), Math.abs(e)), (!h || h > g) && (h = g, l.settings.adjustOldDeltas && "mousewheel" === c.type && 0 === g % 120 && (h /= 40)), l.settings.adjustOldDeltas && "mousewheel" === c.type && 0 === g % 120 && (f /= 40, e /= 40, b /= 40), f = Math[f >= 1 ? "floor" : "ceil"](f / h), e = Math[e >= 1 ? "floor" : "ceil"](e / h), b = Math[b >= 1 ? "floor" : "ceil"](b / h), a.deltaX = e, a.deltaY = b, a.deltaFactor = h, a.deltaMode = 0, k.unshift(a, f, e, b), n && clearTimeout(n), n = setTimeout(s, 200), (d.event.dispatch || d.event.handle).apply(this, k)) : void 0
    }

    function s() {
        h = null
    }
    var n, h, p = ["wheel", "mousewheel", "DOMMouseScroll", "MozMousePixelScroll"],
        k = "onwheel" in document || 9 <= document.documentMode ? ["wheel"] : ["mousewheel", "DomMouseScroll", "MozMousePixelScroll"],
        r = Array.prototype.slice;
    if (d.event.fixHooks)
        for (var q = p.length; q;) d.event.fixHooks[p[--q]] = d.event.mouseHooks;
    var l = d.event.special.mousewheel = {
        version: "3.1.9",
        setup: function() {
            if (this.addEventListener)
                for (var a = k.length; a;) this.addEventListener(k[--a], m, !1);
            else this.onmousewheel = m;
            d.data(this, "mousewheel-line-height", l.getLineHeight(this)), d.data(this, "mousewheel-page-height", l.getPageHeight(this))
        },
        teardown: function() {
            if (this.removeEventListener)
                for (var a = k.length; a;) this.removeEventListener(k[--a], m, !1);
            else this.onmousewheel = null
        },
        getLineHeight: function(a) {
            return parseInt(d(a)["offsetParent" in d.fn ? "offsetParent" : "parent"]().css("fontSize"), 10)
        },
        getPageHeight: function(a) {
            return d(a).height()
        },
        settings: {
            adjustOldDeltas: !0
        }
    };
    d.fn.extend({
        mousewheel: function(a) {
            return a ? this.bind("mousewheel", a) : this.trigger("mousewheel")
        },
        unmousewheel: function(a) {
            return this.unbind("mousewheel", a)
        }
    })
}),
function($) {
    function unsetPos() {
        this === mwheelI.elem && (mwheelI.pos = [-260, -260], mwheelI.elem = !1, minDif = 3)
    }
    var longDelay, shortDelay, mwheelI = {
            pos: [-260, -260]
        },
        minDif = 3,
        doc = document,
        root = doc.documentElement,
        body = doc.body;
    $.event.special.mwheelIntent = {
        setup: function() {
            var jElm = $(this).on("mousewheel", $.event.special.mwheelIntent.handler);
            return this !== doc && this !== root && this !== body && jElm.on("mouseleave", unsetPos), jElm = null, !0
        },
        teardown: function() {
            return $(this).off("mousewheel", $.event.special.mwheelIntent.handler).off("mouseleave", unsetPos), !0
        },
        handler: function(e, d) {
            var pos = [e.clientX, e.clientY];
            return this === mwheelI.elem || Math.abs(mwheelI.pos[0] - pos[0]) > minDif || Math.abs(mwheelI.pos[1] - pos[1]) > minDif ? (mwheelI.elem = this, mwheelI.pos = pos, minDif = 250, clearTimeout(shortDelay), shortDelay = setTimeout(function() {
                minDif = 10
            }, 200), clearTimeout(longDelay), longDelay = setTimeout(function() {
                minDif = 3
            }, 1500), e = $.extend({}, e, {
                type: "mwheelIntent"
            }), $.event.dispatch.apply(this, arguments)) : void 0
        }
    }, $.fn.extend({
        mwheelIntent: function(fn) {
            return fn ? this.on("mwheelIntent", fn) : this.trigger("mwheelIntent")
        },
        unmwheelIntent: function(fn) {
            return this.off("mwheelIntent", fn)
        }
    }), $(function() {
        body = doc.body, $(doc).on("mwheelIntent.mwheelIntentDefault", $.noop)
    })
}(jQuery), ! function(a, b) {
    "use strict";

    function c() {
        e.READY || (t.determineEventTypes(), s.each(e.gestures, function(a) {
            v.register(a)
        }), t.onTouch(e.DOCUMENT, o, v.detect), t.onTouch(e.DOCUMENT, p, v.detect), e.READY = !0)
    }

    function d(a, c) {
        Date.now || (Date.now = function() {
            return (new Date).getTime()
        }), a.utils.each(["on", "off"], function(d) {
            a.utils[d] = function(a, e, f) {
                c(a)[d](e, function(a) {
                    var d = c.extend({}, a.originalEvent, a);
                    d.button === b && (d.button = a.which - 1), f.call(this, d)
                })
            }
        }), a.Instance.prototype.trigger = function(a, b) {
            var d = c(this.element);
            return d.has(b.target).length && (d = c(b.target)), d.trigger({
                type: a,
                gesture: b
            })
        }, c.fn.hammer = function(b) {
            return this.each(function() {
                var d = c(this),
                    e = d.data("hammer");
                e ? e && b && a.utils.extend(e.options, b) : d.data("hammer", new a(this, b || {}))
            })
        }
    }
    var e = function w(a, b) {
        return new w.Instance(a, b || {})
    };
    e.VERSION = "1.1.3", e.defaults = {
        behavior: {
            userSelect: "none",
            touchAction: "pan-y",
            touchCallout: "none",
            contentZooming: "none",
            userDrag: "none",
            tapHighlightColor: "rgba(0,0,0,0)"
        }
    }, e.DOCUMENT = document, e.HAS_POINTEREVENTS = navigator.pointerEnabled || navigator.msPointerEnabled, e.HAS_TOUCHEVENTS = "ontouchstart" in a, e.IS_MOBILE = /mobile|tablet|ip(ad|hone|od)|android|silk/i.test(navigator.userAgent), e.NO_MOUSEEVENTS = e.HAS_TOUCHEVENTS && e.IS_MOBILE || e.HAS_POINTEREVENTS, e.CALCULATE_INTERVAL = 25;
    var f = {},
        g = e.DIRECTION_DOWN = "down",
        h = e.DIRECTION_LEFT = "left",
        i = e.DIRECTION_UP = "up",
        j = e.DIRECTION_RIGHT = "right",
        k = e.POINTER_MOUSE = "mouse",
        l = e.POINTER_TOUCH = "touch",
        m = e.POINTER_PEN = "pen",
        n = e.EVENT_START = "start",
        o = e.EVENT_MOVE = "move",
        p = e.EVENT_END = "end",
        q = e.EVENT_RELEASE = "release",
        r = e.EVENT_TOUCH = "touch";
    e.READY = !1, e.plugins = e.plugins || {}, e.gestures = e.gestures || {};
    var s = e.utils = {
        extend: function(a, c, d) {
            for (var e in c) !c.hasOwnProperty(e) || a[e] !== b && d || (a[e] = c[e]);
            return a
        },
        on: function(a, b, c) {
            a.addEventListener(b, c, !1)
        },
        off: function(a, b, c) {
            a.removeEventListener(b, c, !1)
        },
        each: function(a, c, d) {
            var e, f;
            if ("forEach" in a) a.forEach(c, d);
            else if (a.length !== b) {
                for (e = 0, f = a.length; f > e; e++)
                    if (c.call(d, a[e], e, a) === !1) return
            } else
                for (e in a)
                    if (a.hasOwnProperty(e) && c.call(d, a[e], e, a) === !1) return
        },
        inStr: function(a, b) {
            return a.indexOf(b) > -1
        },
        inArray: function(a, b) {
            if (a.indexOf) {
                var c = a.indexOf(b);
                return -1 === c ? !1 : c
            }
            for (var d = 0, e = a.length; e > d; d++)
                if (a[d] === b) return d;
            return !1
        },
        toArray: function(a) {
            return Array.prototype.slice.call(a, 0)
        },
        hasParent: function(a, b) {
            for (; a;) {
                if (a == b) return !0;
                a = a.parentNode
            }
            return !1
        },
        getCenter: function(a) {
            var b = [],
                c = [],
                d = [],
                e = [],
                f = Math.min,
                g = Math.max;
            return 1 === a.length ? {
                pageX: a[0].pageX,
                pageY: a[0].pageY,
                clientX: a[0].clientX,
                clientY: a[0].clientY
            } : (s.each(a, function(a) {
                b.push(a.pageX), c.push(a.pageY), d.push(a.clientX), e.push(a.clientY)
            }), {
                pageX: (f.apply(Math, b) + g.apply(Math, b)) / 2,
                pageY: (f.apply(Math, c) + g.apply(Math, c)) / 2,
                clientX: (f.apply(Math, d) + g.apply(Math, d)) / 2,
                clientY: (f.apply(Math, e) + g.apply(Math, e)) / 2
            })
        },
        getVelocity: function(a, b, c) {
            return {
                x: Math.abs(b / a) || 0,
                y: Math.abs(c / a) || 0
            }
        },
        getAngle: function(a, b) {
            var c = b.clientX - a.clientX,
                d = b.clientY - a.clientY;
            return 180 * Math.atan2(d, c) / Math.PI
        },
        getDirection: function(a, b) {
            var c = Math.abs(a.clientX - b.clientX),
                d = Math.abs(a.clientY - b.clientY);
            return c >= d ? a.clientX - b.clientX > 0 ? h : j : a.clientY - b.clientY > 0 ? i : g
        },
        getDistance: function(a, b) {
            var c = b.clientX - a.clientX,
                d = b.clientY - a.clientY;
            return Math.sqrt(c * c + d * d)
        },
        getScale: function(a, b) {
            return a.length >= 2 && b.length >= 2 ? this.getDistance(b[0], b[1]) / this.getDistance(a[0], a[1]) : 1
        },
        getRotation: function(a, b) {
            return a.length >= 2 && b.length >= 2 ? this.getAngle(b[1], b[0]) - this.getAngle(a[1], a[0]) : 0
        },
        isVertical: function(a) {
            return a == i || a == g
        },
        setPrefixedCss: function(a, b, c, d) {
            var e = ["", "Webkit", "Moz", "O", "ms"];
            b = s.toCamelCase(b);
            for (var f = 0; f < e.length; f++) {
                var g = b;
                if (e[f] && (g = e[f] + g.slice(0, 1).toUpperCase() + g.slice(1)), g in a.style) {
                    a.style[g] = (null == d || d) && c || "";
                    break
                }
            }
        },
        toggleBehavior: function(a, b, c) {
            if (b && a && a.style) {
                s.each(b, function(b, d) {
                    s.setPrefixedCss(a, d, b, c)
                });
                var d = c && function() {
                    return !1
                };
                "none" == b.userSelect && (a.onselectstart = d), "none" == b.userDrag && (a.ondragstart = d)
            }
        },
        toCamelCase: function(a) {
            return a.replace(/[_-]([a-z])/g, function(a) {
                return a[1].toUpperCase()
            })
        }
    };
    e.Instance = function(a, b) {
        var d = this;
        c(), this.element = a, this.enabled = !0, s.each(b, function(a, c) {
            delete b[c], b[s.toCamelCase(c)] = a
        }), this.options = s.extend(s.extend({}, e.defaults), b || {}), this.options.behavior && s.toggleBehavior(this.element, this.options.behavior, !0), this.eventStartHandler = t.onTouch(a, n, function(a) {
            d.enabled && a.eventType == n ? v.startDetect(d, a) : a.eventType == r && v.detect(a)
        }), this.eventHandlers = []
    }, e.Instance.prototype = {
        on: function(a, b) {
            var c = this;
            return t.on(c.element, a, b, function(a) {
                c.eventHandlers.push({
                    gesture: a,
                    handler: b
                })
            }), c
        },
        off: function(a, b) {
            var c = this;
            return t.off(c.element, a, b, function(a) {
                var d = s.inArray({
                    gesture: a,
                    handler: b
                });
                d !== !1 && c.eventHandlers.splice(d, 1)
            }), c
        },
        trigger: function(a, b) {
            b || (b = {});
            var c = e.DOCUMENT.createEvent("Event");
            c.initEvent(a, !0, !0), c.gesture = b;
            var d = this.element;
            return s.hasParent(b.target, d) && (d = b.target), d.dispatchEvent(c), this
        },
        enable: function(a) {
            return this.enabled = a, this
        },
        dispose: function() {
            var a, b;
            for (s.toggleBehavior(this.element, this.options.behavior, !1), a = -1; b = this.eventHandlers[++a];) s.off(this.element, b.gesture, b.handler);
            return this.eventHandlers = [], t.off(this.element, f[n], this.eventStartHandler), null
        }
    };
    var t = e.event = {
            preventMouseEvents: !1,
            started: !1,
            shouldDetect: !1,
            on: function(a, b, c, d) {
                var e = b.split(" ");
                s.each(e, function(b) {
                    s.on(a, b, c), d && d(b)
                })
            },
            off: function(a, b, c, d) {
                var e = b.split(" ");
                s.each(e, function(b) {
                    s.off(a, b, c), d && d(b)
                })
            },
            onTouch: function(a, b, c) {
                var d = this,
                    g = function(f) {
                        var g, h = f.type.toLowerCase(),
                            i = e.HAS_POINTEREVENTS,
                            j = s.inStr(h, "mouse");
                        j && d.preventMouseEvents || (j && b == n && 0 === f.button ? (d.preventMouseEvents = !1, d.shouldDetect = !0) : i && b == n ? d.shouldDetect = 1 === f.buttons || u.matchType(l, f) : j || b != n || (d.preventMouseEvents = !0, d.shouldDetect = !0), i && b != p && u.updatePointer(b, f), d.shouldDetect && (g = d.doDetect.call(d, f, b, a, c)), g == p && (d.preventMouseEvents = !1, d.shouldDetect = !1, u.reset()), i && b == p && u.updatePointer(b, f))
                    };
                return this.on(a, f[b], g), g
            },
            doDetect: function(a, b, c, d) {
                var e = this.getTouchList(a, b),
                    f = e.length,
                    g = b,
                    h = e.trigger,
                    i = f;
                b == n ? h = r : b == p && (h = q, i = e.length - (a.changedTouches ? a.changedTouches.length : 1)), i > 0 && this.started && (g = o), this.started = !0;
                var j = this.collectEventData(c, g, e, a);
                return b != p && d.call(v, j), h && (j.changedLength = i, j.eventType = h, d.call(v, j), j.eventType = g, delete j.changedLength), g == p && (d.call(v, j), this.started = !1), g
            },
            determineEventTypes: function() {
                var b;
                return b = e.HAS_POINTEREVENTS ? a.PointerEvent ? ["pointerdown", "pointermove", "pointerup pointercancel lostpointercapture"] : ["MSPointerDown", "MSPointerMove", "MSPointerUp MSPointerCancel MSLostPointerCapture"] : e.NO_MOUSEEVENTS ? ["touchstart", "touchmove", "touchend touchcancel"] : ["touchstart mousedown", "touchmove mousemove", "touchend touchcancel mouseup"], f[n] = b[0], f[o] = b[1], f[p] = b[2], f
            },
            getTouchList: function(a, b) {
                if (e.HAS_POINTEREVENTS) return u.getTouchList();
                if (a.touches) {
                    if (b == o) return a.touches;
                    var c = [],
                        d = [].concat(s.toArray(a.touches), s.toArray(a.changedTouches)),
                        f = [];
                    return s.each(d, function(a) {
                        s.inArray(c, a.identifier) === !1 && f.push(a), c.push(a.identifier)
                    }), f
                }
                return a.identifier = 1, [a]
            },
            collectEventData: function(a, b, c, d) {
                var e = l;
                return s.inStr(d.type, "mouse") || u.matchType(k, d) ? e = k : u.matchType(m, d) && (e = m), {
                    center: s.getCenter(c),
                    timeStamp: Date.now(),
                    target: d.target,
                    touches: c,
                    eventType: b,
                    pointerType: e,
                    srcEvent: d,
                    preventDefault: function() {
                        var a = this.srcEvent;
                        a.preventManipulation && a.preventManipulation(), a.preventDefault && a.preventDefault()
                    },
                    stopPropagation: function() {
                        this.srcEvent.stopPropagation()
                    },
                    stopDetect: function() {
                        return v.stopDetect()
                    }
                }
            }
        },
        u = e.PointerEvent = {
            pointers: {},
            getTouchList: function() {
                var a = [];
                return s.each(this.pointers, function(b) {
                    a.push(b)
                }), a
            },
            updatePointer: function(a, b) {
                a == p || a != p && 1 !== b.buttons ? delete this.pointers[b.pointerId] : (b.identifier = b.pointerId, this.pointers[b.pointerId] = b)
            },
            matchType: function(a, b) {
                if (!b.pointerType) return !1;
                var c = b.pointerType,
                    d = {};
                return d[k] = c === (b.MSPOINTER_TYPE_MOUSE || k), d[l] = c === (b.MSPOINTER_TYPE_TOUCH || l), d[m] = c === (b.MSPOINTER_TYPE_PEN || m), d[a]
            },
            reset: function() {
                this.pointers = {}
            }
        },
        v = e.detection = {
            gestures: [],
            current: null,
            previous: null,
            stopped: !1,
            startDetect: function(a, b) {
                this.current || (this.stopped = !1, this.current = {
                    inst: a,
                    startEvent: s.extend({}, b),
                    lastEvent: !1,
                    lastCalcEvent: !1,
                    futureCalcEvent: !1,
                    lastCalcData: {},
                    name: ""
                }, this.detect(b))
            },
            detect: function(a) {
                if (this.current && !this.stopped) {
                    a = this.extendEventData(a);
                    var b = this.current.inst,
                        c = b.options;
                    return s.each(this.gestures, function(d) {
                        !this.stopped && b.enabled && c[d.name] && d.handler.call(d, a, b)
                    }, this), this.current && (this.current.lastEvent = a), a.eventType == p && this.stopDetect(), a
                }
            },
            stopDetect: function() {
                this.previous = s.extend({}, this.current), this.current = null, this.stopped = !0
            },
            getCalculatedData: function(a, b, c, d, f) {
                var g = this.current,
                    h = !1,
                    i = g.lastCalcEvent,
                    j = g.lastCalcData;
                i && a.timeStamp - i.timeStamp > e.CALCULATE_INTERVAL && (b = i.center, c = a.timeStamp - i.timeStamp, d = a.center.clientX - i.center.clientX, f = a.center.clientY - i.center.clientY, h = !0), (a.eventType == r || a.eventType == q) && (g.futureCalcEvent = a), (!g.lastCalcEvent || h) && (j.velocity = s.getVelocity(c, d, f), j.angle = s.getAngle(b, a.center), j.direction = s.getDirection(b, a.center), g.lastCalcEvent = g.futureCalcEvent || a, g.futureCalcEvent = a), a.velocityX = j.velocity.x, a.velocityY = j.velocity.y, a.interimAngle = j.angle, a.interimDirection = j.direction
            },
            extendEventData: function(a) {
                var b = this.current,
                    c = b.startEvent,
                    d = b.lastEvent || c;
                (a.eventType == r || a.eventType == q) && (c.touches = [], s.each(a.touches, function(a) {
                    c.touches.push({
                        clientX: a.clientX,
                        clientY: a.clientY
                    })
                }));
                var e = a.timeStamp - c.timeStamp,
                    f = a.center.clientX - c.center.clientX,
                    g = a.center.clientY - c.center.clientY;
                return this.getCalculatedData(a, d.center, e, f, g), s.extend(a, {
                    startEvent: c,
                    deltaTime: e,
                    deltaX: f,
                    deltaY: g,
                    distance: s.getDistance(c.center, a.center),
                    angle: s.getAngle(c.center, a.center),
                    direction: s.getDirection(c.center, a.center),
                    scale: s.getScale(c.touches, a.touches),
                    rotation: s.getRotation(c.touches, a.touches)
                }), a
            },
            register: function(a) {
                var c = a.defaults || {};
                return c[a.name] === b && (c[a.name] = !0), s.extend(e.defaults, c, !0), a.index = a.index || 1e3, this.gestures.push(a), this.gestures.sort(function(a, b) {
                    return a.index < b.index ? -1 : a.index > b.index ? 1 : 0
                }), this.gestures
            }
        };
    ! function(a) {
        function b(b, d) {
            var e = v.current;
            if (!(d.options.dragMaxTouches > 0 && b.touches.length > d.options.dragMaxTouches)) switch (b.eventType) {
                case n:
                    c = !1;
                    break;
                case o:
                    if (b.distance < d.options.dragMinDistance && e.name != a) return;
                    var f = e.startEvent.center;
                    if (e.name != a && (e.name = a, d.options.dragDistanceCorrection && b.distance > 0)) {
                        var k = Math.abs(d.options.dragMinDistance / b.distance);
                        f.pageX += b.deltaX * k, f.pageY += b.deltaY * k, f.clientX += b.deltaX * k, f.clientY += b.deltaY * k, b = v.extendEventData(b)
                    }(e.lastEvent.dragLockToAxis || d.options.dragLockToAxis && d.options.dragLockMinDistance <= b.distance) && (b.dragLockToAxis = !0);
                    var l = e.lastEvent.direction;
                    b.dragLockToAxis && l !== b.direction && (b.direction = s.isVertical(l) ? b.deltaY < 0 ? i : g : b.deltaX < 0 ? h : j), c || (d.trigger(a + "start", b), c = !0), d.trigger(a, b), d.trigger(a + b.direction, b);
                    var m = s.isVertical(b.direction);
                    (d.options.dragBlockVertical && m || d.options.dragBlockHorizontal && !m) && b.preventDefault();
                    break;
                case q:
                    c && b.changedLength <= d.options.dragMaxTouches && (d.trigger(a + "end", b), c = !1);
                    break;
                case p:
                    c = !1
            }
        }
        var c = !1;
        e.gestures.Drag = {
            name: a,
            index: 50,
            handler: b,
            defaults: {
                dragMinDistance: 10,
                dragDistanceCorrection: !0,
                dragMaxTouches: 1,
                dragBlockHorizontal: !1,
                dragBlockVertical: !1,
                dragLockToAxis: !1,
                dragLockMinDistance: 25
            }
        }
    }("drag"), e.gestures.Gesture = {
            name: "gesture",
            index: 1337,
            handler: function(a, b) {
                b.trigger(this.name, a)
            }
        },
        function(a) {
            function b(b, d) {
                var e = d.options,
                    f = v.current;
                switch (b.eventType) {
                    case n:
                        clearTimeout(c), f.name = a, c = setTimeout(function() {
                            f && f.name == a && d.trigger(a, b)
                        }, e.holdTimeout);
                        break;
                    case o:
                        b.distance > e.holdThreshold && clearTimeout(c);
                        break;
                    case q:
                        clearTimeout(c)
                }
            }
            var c;
            e.gestures.Hold = {
                name: a,
                index: 10,
                defaults: {
                    holdTimeout: 500,
                    holdThreshold: 2
                },
                handler: b
            }
        }("hold"), e.gestures.Release = {
            name: "release",
            index: 1 / 0,
            handler: function(a, b) {
                a.eventType == q && b.trigger(this.name, a)
            }
        }, e.gestures.Swipe = {
            name: "swipe",
            index: 40,
            defaults: {
                swipeMinTouches: 1,
                swipeMaxTouches: 1,
                swipeVelocityX: .6,
                swipeVelocityY: .6
            },
            handler: function(a, b) {
                if (a.eventType == q) {
                    var c = a.touches.length,
                        d = b.options;
                    if (c < d.swipeMinTouches || c > d.swipeMaxTouches) return;
                    (a.velocityX > d.swipeVelocityX || a.velocityY > d.swipeVelocityY) && (b.trigger(this.name, a), b.trigger(this.name + a.direction, a))
                }
            }
        },
        function(a) {
            function b(b, d) {
                var e, f, g = d.options,
                    h = v.current,
                    i = v.previous;
                switch (b.eventType) {
                    case n:
                        c = !1;
                        break;
                    case o:
                        c = c || b.distance > g.tapMaxDistance;
                        break;
                    case p:
                        !s.inStr(b.srcEvent.type, "cancel") && b.deltaTime < g.tapMaxTime && !c && (e = i && i.lastEvent && b.timeStamp - i.lastEvent.timeStamp, f = !1, i && i.name == a && e && e < g.doubleTapInterval && b.distance < g.doubleTapDistance && (d.trigger("doubletap", b), f = !0), (!f || g.tapAlways) && (h.name = a, d.trigger(h.name, b)))
                }
            }
            var c = !1;
            e.gestures.Tap = {
                name: a,
                index: 100,
                handler: b,
                defaults: {
                    tapMaxTime: 250,
                    tapMaxDistance: 10,
                    tapAlways: !0,
                    doubleTapDistance: 20,
                    doubleTapInterval: 300
                }
            }
        }("tap"), e.gestures.Touch = {
            name: "touch",
            index: -1 / 0,
            defaults: {
                preventDefault: !1,
                preventMouse: !1
            },
            handler: function(a, b) {
                return b.options.preventMouse && a.pointerType == k ? void a.stopDetect() : (b.options.preventDefault && a.preventDefault(), void(a.eventType == r && b.trigger("touch", a)))
            }
        },
        function(a) {
            function b(b, d) {
                switch (b.eventType) {
                    case n:
                        c = !1;
                        break;
                    case o:
                        if (b.touches.length < 2) return;
                        var e = Math.abs(1 - b.scale),
                            f = Math.abs(b.rotation);
                        if (e < d.options.transformMinScale && f < d.options.transformMinRotation) return;
                        v.current.name = a, c || (d.trigger(a + "start", b), c = !0), d.trigger(a, b), f > d.options.transformMinRotation && d.trigger("rotate", b), e > d.options.transformMinScale && (d.trigger("pinch", b), d.trigger("pinch" + (b.scale < 1 ? "in" : "out"), b));
                        break;
                    case q:
                        c && b.changedLength < 2 && (d.trigger(a + "end", b), c = !1)
                }
            }
            var c = !1;
            e.gestures.Transform = {
                name: a,
                index: 45,
                defaults: {
                    transformMinScale: .01,
                    transformMinRotation: 1
                },
                handler: b
            }
        }("transform"), a.Hammer = e, "undefined" != typeof module && module.exports && (module.exports = e), "function" == typeof define && define.amd ? define(["jquery"], function(b) {
            return d(a.Hammer, b)
        }) : d(a.Hammer, a.jQuery || a.Zepto)
}(window),
function(a) {
    "function" == typeof define && define.amd && define.amd.jQuery ? define(["jquery"], a) : a(jQuery)
}(function(f) {
    function w(E) {
        return !E || void 0 !== E.allowPageScroll || void 0 === E.swipe && void 0 === E.swipeStatus || (E.allowPageScroll = m), void 0 !== E.click && void 0 === E.tap && (E.tap = E.click), E || (E = {}), E = f.extend({}, f.fn.swipe.defaults, E), this.each(function() {
            var G = f(this),
                F = G.data(B);
            F || (F = new C(this, E), G.data(B, F))
        })
    }

    function C(a4, av) {
        function aN(bd) {
            if (!(aB() || f(bd.target).closest(av.excludedElements, aR).length > 0)) {
                var bc, be = bd.originalEvent ? bd.originalEvent : bd,
                    bb = a ? be.touches[0] : be;
                return Z = g, a ? W = be.touches.length : bd.preventDefault(), ag = 0, aP = null, aJ = null, ab = 0, a1 = 0, aZ = 0, G = 1, aq = 0, aQ = aj(), M = aa(), R(), !a || W === av.fingers || av.fingers === i || aX() ? (ai(0, bb), T = at(), 2 == W && (ai(1, be.touches[1]), a1 = aZ = au(aQ[0].start, aQ[1].start)), (av.swipeStatus || av.pinchStatus) && (bc = O(be, Z))) : bc = !1, bc === !1 ? (Z = q, O(be, Z), bc) : (av.hold && (af = setTimeout(f.proxy(function() {
                    aR.trigger("hold", [be.target]), av.hold && (bc = av.hold.call(aR, be, be.target))
                }, this), av.longTapThreshold)), ao(!0), null)
            }
        }

        function a3(be) {
            var bh = be.originalEvent ? be.originalEvent : be;
            if (Z !== h && Z !== q && !am()) {
                var bd, bc = a ? bh.touches[0] : bh,
                    bf = aH(bc);
                if (a2 = at(), a && (W = bh.touches.length), av.hold && clearTimeout(af), Z = k, 2 == W && (0 == a1 ? (ai(1, bh.touches[1]), a1 = aZ = au(aQ[0].start, aQ[1].start)) : (aH(bh.touches[1]), aZ = au(aQ[0].end, aQ[1].end), aJ = ar(aQ[0].end, aQ[1].end)), G = a7(a1, aZ), aq = Math.abs(a1 - aZ)), W === av.fingers || av.fingers === i || !a || aX()) {
                    if (aP = aL(bf.start, bf.end), al(be, aP), ag = aS(bf.start, bf.end), ab = aM(), aI(aP, ag), (av.swipeStatus || av.pinchStatus) && (bd = O(bh, Z)), !av.triggerOnTouchEnd || av.triggerOnTouchLeave) {
                        var bb = !0;
                        if (av.triggerOnTouchLeave) {
                            var bg = aY(this);
                            bb = E(bf.end, bg)
                        }!av.triggerOnTouchEnd && bb ? Z = aC(k) : av.triggerOnTouchLeave && !bb && (Z = aC(h)), (Z == q || Z == h) && O(bh, Z)
                    }
                } else Z = q, O(bh, Z);
                bd === !1 && (Z = q, O(bh, Z))
            }
        }

        function L(bb) {
            var bc = bb.originalEvent;
            return a && bc.touches.length > 0 ? (F(), !0) : (am() && (W = ad), a2 = at(), ab = aM(), ba() || !an() ? (Z = q, O(bc, Z)) : av.triggerOnTouchEnd || 0 == av.triggerOnTouchEnd && Z === k ? (bb.preventDefault(), Z = h, O(bc, Z)) : !av.triggerOnTouchEnd && a6() ? (Z = h, aF(bc, Z, A)) : Z === k && (Z = q, O(bc, Z)), ao(!1), null)
        }

        function a9() {
            W = 0, a2 = 0, T = 0, a1 = 0, aZ = 0, G = 1, R(), ao(!1)
        }

        function K(bb) {
            var bc = bb.originalEvent;
            av.triggerOnTouchLeave && (Z = aC(h), O(bc, Z))
        }

        function aK() {
            aR.unbind(J, aN), aR.unbind(aD, a9), aR.unbind(ay, a3), aR.unbind(U, L), S && aR.unbind(S, K), ao(!1)
        }

        function aC(bf) {
            var be = bf,
                bd = aA(),
                bc = an(),
                bb = ba();
            return !bd || bb ? be = q : !bc || bf != k || av.triggerOnTouchEnd && !av.triggerOnTouchLeave ? !bc && bf == h && av.triggerOnTouchLeave && (be = q) : be = h, be
        }

        function O(bd, bb) {
            var bc = void 0;
            return I() || V() ? bc = aF(bd, bb, l) : (P() || aX()) && bc !== !1 && (bc = aF(bd, bb, t)), aG() && bc !== !1 ? bc = aF(bd, bb, j) : ap() && bc !== !1 ? bc = aF(bd, bb, b) : ah() && bc !== !1 && (bc = aF(bd, bb, A)), bb === q && a9(bd), bb === h && (a ? 0 == bd.touches.length && a9(bd) : a9(bd)), bc
        }

        function aF(be, bb, bd) {
            var bc = void 0;
            if (bd == l) {
                if (aR.trigger("swipeStatus", [bb, aP || null, ag || 0, ab || 0, W, aQ]), av.swipeStatus && (bc = av.swipeStatus.call(aR, be, bb, aP || null, ag || 0, ab || 0, W, aQ), bc === !1)) return !1;
                if (bb == h && aV()) {
                    if (aR.trigger("swipe", [aP, ag, ab, W, aQ]), av.swipe && (bc = av.swipe.call(aR, be, aP, ag, ab, W, aQ), bc === !1)) return !1;
                    switch (aP) {
                        case p:
                            aR.trigger("swipeLeft", [aP, ag, ab, W, aQ]), av.swipeLeft && (bc = av.swipeLeft.call(aR, be, aP, ag, ab, W, aQ));
                            break;
                        case o:
                            aR.trigger("swipeRight", [aP, ag, ab, W, aQ]), av.swipeRight && (bc = av.swipeRight.call(aR, be, aP, ag, ab, W, aQ));
                            break;
                        case e:
                            aR.trigger("swipeUp", [aP, ag, ab, W, aQ]), av.swipeUp && (bc = av.swipeUp.call(aR, be, aP, ag, ab, W, aQ));
                            break;
                        case x:
                            aR.trigger("swipeDown", [aP, ag, ab, W, aQ]), av.swipeDown && (bc = av.swipeDown.call(aR, be, aP, ag, ab, W, aQ))
                    }
                }
            }
            if (bd == t) {
                if (aR.trigger("pinchStatus", [bb, aJ || null, aq || 0, ab || 0, W, G, aQ]), av.pinchStatus && (bc = av.pinchStatus.call(aR, be, bb, aJ || null, aq || 0, ab || 0, W, G, aQ), bc === !1)) return !1;
                if (bb == h && a8()) switch (aJ) {
                    case c:
                        aR.trigger("pinchIn", [aJ || null, aq || 0, ab || 0, W, G, aQ]), av.pinchIn && (bc = av.pinchIn.call(aR, be, aJ || null, aq || 0, ab || 0, W, G, aQ));
                        break;
                    case z:
                        aR.trigger("pinchOut", [aJ || null, aq || 0, ab || 0, W, G, aQ]), av.pinchOut && (bc = av.pinchOut.call(aR, be, aJ || null, aq || 0, ab || 0, W, G, aQ))
                }
            }
            return bd == A ? (bb === q || bb === h) && (clearTimeout(aW), clearTimeout(af), Y() && !H() ? (N = at(), aW = setTimeout(f.proxy(function() {
                N = null, aR.trigger("tap", [be.target]), av.tap && (bc = av.tap.call(aR, be, be.target))
            }, this), av.doubleTapThreshold)) : (N = null, aR.trigger("tap", [be.target]), av.tap && (bc = av.tap.call(aR, be, be.target)))) : bd == j ? (bb === q || bb === h) && (clearTimeout(aW), N = null, aR.trigger("doubletap", [be.target]), av.doubleTap && (bc = av.doubleTap.call(aR, be, be.target))) : bd == b && (bb === q || bb === h) && (clearTimeout(aW), N = null, aR.trigger("longtap", [be.target]), av.longTap && (bc = av.longTap.call(aR, be, be.target))), bc
        }

        function an() {
            var bb = !0;
            return null !== av.threshold && (bb = ag >= av.threshold), bb
        }

        function ba() {
            var bb = !1;
            return null !== av.cancelThreshold && null !== aP && (bb = aT(aP) - ag >= av.cancelThreshold), bb
        }

        function ae() {
            return null !== av.pinchThreshold ? aq >= av.pinchThreshold : !0
        }

        function aA() {
            var bb;
            return bb = av.maxTimeThreshold && ab >= av.maxTimeThreshold ? !1 : !0
        }

        function al(bb, bc) {
            if (av.allowPageScroll === m || aX()) bb.preventDefault();
            else {
                var bd = av.allowPageScroll === s;
                switch (bc) {
                    case p:
                        (av.swipeLeft && bd || !bd && av.allowPageScroll != D) && bb.preventDefault();
                        break;
                    case o:
                        (av.swipeRight && bd || !bd && av.allowPageScroll != D) && bb.preventDefault();
                        break;
                    case e:
                        (av.swipeUp && bd || !bd && av.allowPageScroll != u) && bb.preventDefault();
                        break;
                    case x:
                        (av.swipeDown && bd || !bd && av.allowPageScroll != u) && bb.preventDefault()
                }
            }
        }

        function a8() {
            var bc = aO(),
                bb = X(),
                bd = ae();
            return bc && bb && bd
        }

        function aX() {
            return !!(av.pinchStatus || av.pinchIn || av.pinchOut)
        }

        function P() {
            return !(!a8() || !aX())
        }

        function aV() {
            var be = aA(),
                bg = an(),
                bd = aO(),
                bb = X(),
                bc = ba(),
                bf = !bc && bb && bd && bg && be;
            return bf
        }

        function V() {
            return !!(av.swipe || av.swipeStatus || av.swipeLeft || av.swipeRight || av.swipeUp || av.swipeDown)
        }

        function I() {
            return !(!aV() || !V())
        }

        function aO() {
            return W === av.fingers || av.fingers === i || !a
        }

        function X() {
            return 0 !== aQ[0].end.x
        }

        function a6() {
            return !!av.tap
        }

        function Y() {
            return !!av.doubleTap
        }

        function aU() {
            return !!av.longTap
        }

        function Q() {
            if (null == N) return !1;
            var bb = at();
            return Y() && bb - N <= av.doubleTapThreshold
        }

        function H() {
            return Q()
        }

        function ax() {
            return (1 === W || !a) && (isNaN(ag) || ag < av.threshold)
        }

        function a0() {
            return ab > av.longTapThreshold && r > ag
        }

        function ah() {
            return !(!ax() || !a6())
        }

        function aG() {
            return !(!Q() || !Y())
        }

        function ap() {
            return !(!a0() || !aU())
        }

        function F() {
            a5 = at(), ad = event.touches.length + 1
        }

        function R() {
            a5 = 0, ad = 0
        }

        function am() {
            var bb = !1;
            if (a5) {
                var bc = at() - a5;
                bc <= av.fingerReleaseThreshold && (bb = !0)
            }
            return bb
        }

        function aB() {
            return !(aR.data(B + "_intouch") !== !0)
        }

        function ao(bb) {
            bb === !0 ? (aR.bind(ay, a3), aR.bind(U, L), S && aR.bind(S, K)) : (aR.unbind(ay, a3, !1), aR.unbind(U, L, !1), S && aR.unbind(S, K, !1)), aR.data(B + "_intouch", bb === !0)
        }

        function ai(bc, bb) {
            var bd = void 0 !== bb.identifier ? bb.identifier : 0;
            return aQ[bc].identifier = bd, aQ[bc].start.x = aQ[bc].end.x = bb.pageX || bb.clientX, aQ[bc].start.y = aQ[bc].end.y = bb.pageY || bb.clientY, aQ[bc]
        }

        function aH(bb) {
            var bd = void 0 !== bb.identifier ? bb.identifier : 0,
                bc = ac(bd);
            return bc.end.x = bb.pageX || bb.clientX, bc.end.y = bb.pageY || bb.clientY, bc
        }

        function ac(bc) {
            for (var bb = 0; bb < aQ.length; bb++)
                if (aQ[bb].identifier == bc) return aQ[bb]
        }

        function aj() {
            for (var bb = [], bc = 0; 5 >= bc; bc++) bb.push({
                start: {
                    x: 0,
                    y: 0
                },
                end: {
                    x: 0,
                    y: 0
                },
                identifier: 0
            });
            return bb
        }

        function aI(bb, bc) {
            bc = Math.max(bc, aT(bb)), M[bb].distance = bc
        }

        function aT(bb) {
            return M[bb] ? M[bb].distance : void 0
        }

        function aa() {
            var bb = {};
            return bb[p] = aw(p), bb[o] = aw(o), bb[e] = aw(e), bb[x] = aw(x), bb
        }

        function aw(bb) {
            return {
                direction: bb,
                distance: 0
            }
        }

        function aM() {
            return a2 - T
        }

        function au(be, bd) {
            var bc = Math.abs(be.x - bd.x),
                bb = Math.abs(be.y - bd.y);
            return Math.round(Math.sqrt(bc * bc + bb * bb))
        }

        function a7(bb, bc) {
            var bd = bc / bb * 1;
            return bd.toFixed(2)
        }

        function ar() {
            return 1 > G ? z : c
        }

        function aS(bc, bb) {
            return Math.round(Math.sqrt(Math.pow(bb.x - bc.x, 2) + Math.pow(bb.y - bc.y, 2)))
        }

        function aE(be, bc) {
            var bb = be.x - bc.x,
                bg = bc.y - be.y,
                bd = Math.atan2(bg, bb),
                bf = Math.round(180 * bd / Math.PI);
            return 0 > bf && (bf = 360 - Math.abs(bf)), bf
        }

        function aL(bc, bb) {
            var bd = aE(bc, bb);
            return 45 >= bd && bd >= 0 ? p : 360 >= bd && bd >= 315 ? p : bd >= 135 && 225 >= bd ? o : bd > 45 && 135 > bd ? x : e
        }

        function at() {
            var bb = new Date;
            return bb.getTime()
        }

        function aY(bb) {
            bb = f(bb);
            var bd = bb.offset(),
                bc = {
                    left: bd.left,
                    right: bd.left + bb.outerWidth(),
                    top: bd.top,
                    bottom: bd.top + bb.outerHeight()
                };
            return bc
        }

        function E(bb, bc) {
            return bb.x > bc.left && bb.x < bc.right && bb.y > bc.top && bb.y < bc.bottom
        }
        var az = a || d || !av.fallbackToMouseEvents,
            J = az ? d ? v ? "MSPointerDown" : "pointerdown" : "touchstart" : "mousedown",
            ay = az ? d ? v ? "MSPointerMove" : "pointermove" : "touchmove" : "mousemove",
            U = az ? d ? v ? "MSPointerUp" : "pointerup" : "touchend" : "mouseup",
            S = az ? null : "mouseleave",
            aD = d ? v ? "MSPointerCancel" : "pointercancel" : "touchcancel",
            ag = 0,
            aP = null,
            ab = 0,
            a1 = 0,
            aZ = 0,
            G = 1,
            aq = 0,
            aJ = 0,
            M = null,
            aR = f(a4),
            Z = "start",
            W = 0,
            aQ = null,
            T = 0,
            a2 = 0,
            a5 = 0,
            ad = 0,
            N = 0,
            aW = null,
            af = null;
        try {
            aR.bind(J, aN), aR.bind(aD, a9)
        } catch (ak) {
            f.error("events not supported " + J + "," + aD + " on jQuery.swipe")
        }
        this.enable = function() {
            return aR.bind(J, aN), aR.bind(aD, a9), aR
        }, this.disable = function() {
            return aK(), aR
        }, this.destroy = function() {
            return aK(), aR.data(B, null), aR
        }, this.option = function(bc, bb) {
            if (void 0 !== av[bc]) {
                if (void 0 === bb) return av[bc];
                av[bc] = bb
            } else f.error("Option " + bc + " does not exist on jQuery.swipe.options");
            return null
        }
    }
    var p = "left",
        o = "right",
        e = "up",
        x = "down",
        c = "in",
        z = "out",
        m = "none",
        s = "auto",
        l = "swipe",
        t = "pinch",
        A = "tap",
        j = "doubletap",
        b = "longtap",
        D = "horizontal",
        u = "vertical",
        i = "all",
        r = 10,
        g = "start",
        k = "move",
        h = "end",
        q = "cancel",
        a = "ontouchstart" in window,
        v = window.navigator.msPointerEnabled && !window.navigator.pointerEnabled,
        d = window.navigator.pointerEnabled || window.navigator.msPointerEnabled,
        B = "TouchSwipe",
        n = {
            fingers: 1,
            threshold: 75,
            cancelThreshold: null,
            pinchThreshold: 20,
            maxTimeThreshold: null,
            fingerReleaseThreshold: 250,
            longTapThreshold: 500,
            doubleTapThreshold: 200,
            swipe: null,
            swipeLeft: null,
            swipeRight: null,
            swipeUp: null,
            swipeDown: null,
            swipeStatus: null,
            pinchIn: null,
            pinchOut: null,
            pinchStatus: null,
            click: null,
            tap: null,
            doubleTap: null,
            longTap: null,
            hold: null,
            triggerOnTouchEnd: !0,
            triggerOnTouchLeave: !1,
            allowPageScroll: "auto",
            fallbackToMouseEvents: !0,
            excludedElements: "label, button, input, select, textarea, a, .noSwipe"
        };
    f.fn.swipe = function(G) {
        var F = f(this),
            E = F.data(B);
        if (E && "string" == typeof G) {
            if (E[G]) return E[G].apply(this, Array.prototype.slice.call(arguments, 1));
            f.error("Method " + G + " does not exist on jQuery.swipe")
        } else if (!(E || "object" != typeof G && G)) return w.apply(this, arguments);
        return F
    }, f.fn.swipe.defaults = n, f.fn.swipe.phases = {
        PHASE_START: g,
        PHASE_MOVE: k,
        PHASE_END: h,
        PHASE_CANCEL: q
    }, f.fn.swipe.directions = {
        LEFT: p,
        RIGHT: o,
        UP: e,
        DOWN: x,
        IN: c,
        OUT: z
    }, f.fn.swipe.pageScroll = {
        NONE: m,
        HORIZONTAL: D,
        VERTICAL: u,
        AUTO: s
    }, f.fn.swipe.fingers = {
        ONE: 1,
        TWO: 2,
        THREE: 3,
        ALL: i
    }
}),
function(f) {
    var j = "placeholder" in document.createElement("input"),
        h = "-moz-box-sizing -webkit-box-sizing box-sizing padding-top padding-right padding-bottom padding-left margin-top margin-right margin-bottom margin-left border-top-width border-right-width border-bottom-width border-left-width line-height font-size font-family width height top left right bottom".split(" ");
    f.fn.placeholder = function(g) {
        var k = this;
        return g = g || {}, j && !g.force ? this : (window.setTimeout(function() {
            k.each(function() {
                var e = this.tagName.toLowerCase();
                if ("input" === e || "textarea" === e) a: {
                    var b, d, c, a = f(this);
                    try {
                        if (b = a[0].getAttributeNode("placeholder"), !b) break a;
                        if (d = a[0].getAttribute("placeholder"), !d || !d.length) break a;
                        a[0].setAttribute("placeholder", ""), a.data("placeholder", d)
                    } catch (g) {
                        break a
                    }
                    for (e = {}, b = 0; b < h.length; b++) e[h[b]] = a.css(h[b]);b = parseInt(a.css("z-index"), 10),
                    (isNaN(b) || !b) && (b = 1),
                    c = f("<span>").addClass("placeholder").html(d),
                    c.css(e),
                    c.css({
                        cursor: a.css("cursor") || "text",
                        display: "block",
                        position: "absolute",
                        overflow: "hidden",
                        "z-index": b + 1,
                        background: "none",
                        "border-top-style": "solid",
                        "border-right-style": "solid",
                        "border-bottom-style": "solid",
                        "border-left-style": "solid",
                        "border-top-color": "transparent",
                        "border-right-color": "transparent",
                        "border-bottom-color": "transparent",
                        "border-left-color": "transparent"
                    }),
                    c.insertBefore(a),
                    e = a.offset().top - c.offset().top,
                    d = parseInt(c.css("margin-top")),
                    isNaN(d) && (d = 0),
                    c.css("margin-top", d + e),
                    c.on("mousedown", function() {
                        a.is(":enabled") && window.setTimeout(function() {
                            a.trigger("focus")
                        }, 0)
                    }),
                    a.on("focus.placeholder", function() {
                        c.hide()
                    }),
                    a.on("blur.placeholder", function() {
                        c.toggle(!f.trim(a.val()).length)
                    }),
                    a[0].onpropertychange = function() {
                        "value" === event.propertyName && a.trigger("focus.placeholder")
                    },
                    a.trigger("blur.placeholder")
                }
            })
        }, 0), this)
    }
}(jQuery), ! function(a) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], a) : "undefined" != typeof exports ? module.exports = a(require("jquery")) : a(jQuery)
}(function(a) {
    "use strict";
    var b = window.Slick || {};
    b = function() {
        function c(c, d) {
            var f, g, h, e = this;
            if (e.defaults = {
                    accessibility: !0,
                    adaptiveHeight: !1,
                    appendArrows: a(c),
                    appendDots: a(c),
                    arrows: !0,
                    asNavFor: null,
                    prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="previous">Previous</button>',
                    nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="next">Next</button>',
                    autoplay: !1,
                    autoplaySpeed: 3e3,
                    centerMode: !1,
                    centerPadding: "50px",
                    cssEase: "ease",
                    customPaging: function(a, b) {
                        return '<button type="button" data-role="none">' + (b + 1) + "</button>"
                    },
                    dots: !1,
                    dotsClass: "slick-dots",
                    draggable: !0,
                    easing: "linear",
                    edgeFriction: .35,
                    fade: !1,
                    focusOnSelect: !1,
                    infinite: !0,
                    initialSlide: 0,
                    lazyLoad: "ondemand",
                    mobileFirst: !1,
                    pauseOnHover: !0,
                    pauseOnDotsHover: !1,
                    respondTo: "window",
                    responsive: null,
                    rows: 1,
                    rtl: !1,
                    slide: "",
                    slidesPerRow: 1,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    speed: 500,
                    swipe: !0,
                    swipeToSlide: !1,
                    touchMove: !0,
                    touchThreshold: 5,
                    useCSS: !0,
                    variableWidth: !1,
                    vertical: !1,
                    verticalSwiping: !1,
                    waitForAnimate: !0
                }, e.initials = {
                    animating: !1,
                    dragging: !1,
                    autoPlayTimer: null,
                    currentDirection: 0,
                    currentLeft: null,
                    currentSlide: 0,
                    direction: 1,
                    $dots: null,
                    listWidth: null,
                    listHeight: null,
                    loadIndex: 0,
                    $nextArrow: null,
                    $prevArrow: null,
                    slideCount: null,
                    slideWidth: null,
                    $slideTrack: null,
                    $slides: null,
                    sliding: !1,
                    slideOffset: 0,
                    swipeLeft: null,
                    $list: null,
                    touchObject: {},
                    transformsEnabled: !1
                }, a.extend(e, e.initials), e.activeBreakpoint = null, e.animType = null, e.animProp = null, e.breakpoints = [], e.breakpointSettings = [], e.cssTransitions = !1, e.hidden = "hidden", e.paused = !1, e.positionProp = null, e.respondTo = null, e.rowCount = 1, e.shouldClick = !0, e.$slider = a(c), e.$slidesCache = null, e.transformType = null, e.transitionType = null, e.visibilityChange = "visibilitychange", e.windowWidth = 0, e.windowTimer = null, f = a(c).data("slick") || {}, e.options = a.extend({}, e.defaults, f, d), e.currentSlide = e.options.initialSlide, e.originalSettings = e.options, g = e.options.responsive || null, g && g.length > -1) {
                e.respondTo = e.options.respondTo || "window";
                for (h in g) g.hasOwnProperty(h) && (e.breakpoints.push(g[h].breakpoint), e.breakpointSettings[g[h].breakpoint] = g[h].settings);
                e.breakpoints.sort(function(a, b) {
                    return e.options.mobileFirst === !0 ? a - b : b - a
                })
            }
            "undefined" != typeof document.mozHidden ? (e.hidden = "mozHidden", e.visibilityChange = "mozvisibilitychange") : "undefined" != typeof document.msHidden ? (e.hidden = "msHidden", e.visibilityChange = "msvisibilitychange") : "undefined" != typeof document.webkitHidden && (e.hidden = "webkitHidden", e.visibilityChange = "webkitvisibilitychange"), e.autoPlay = a.proxy(e.autoPlay, e), e.autoPlayClear = a.proxy(e.autoPlayClear, e), e.changeSlide = a.proxy(e.changeSlide, e), e.clickHandler = a.proxy(e.clickHandler, e), e.selectHandler = a.proxy(e.selectHandler, e), e.setPosition = a.proxy(e.setPosition, e), e.swipeHandler = a.proxy(e.swipeHandler, e), e.dragHandler = a.proxy(e.dragHandler, e), e.keyHandler = a.proxy(e.keyHandler, e), e.autoPlayIterator = a.proxy(e.autoPlayIterator, e), e.instanceUid = b++, e.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/, e.init(), e.checkResponsive(!0)
        }
        var b = 0;
        return c
    }(), b.prototype.addSlide = b.prototype.slickAdd = function(b, c, d) {
        var e = this;
        if ("boolean" == typeof c) d = c, c = null;
        else if (0 > c || c >= e.slideCount) return !1;
        e.unload(), "number" == typeof c ? 0 === c && 0 === e.$slides.length ? a(b).appendTo(e.$slideTrack) : d ? a(b).insertBefore(e.$slides.eq(c)) : a(b).insertAfter(e.$slides.eq(c)) : d === !0 ? a(b).prependTo(e.$slideTrack) : a(b).appendTo(e.$slideTrack), e.$slides = e.$slideTrack.children(this.options.slide), e.$slideTrack.children(this.options.slide).detach(), e.$slideTrack.append(e.$slides), e.$slides.each(function(b, c) {
            a(c).attr("data-slick-index", b)
        }), e.$slidesCache = e.$slides, e.reinit()
    }, b.prototype.animateHeight = function() {
        var a = this;
        if (1 === a.options.slidesToShow && a.options.adaptiveHeight === !0 && a.options.vertical === !1) {
            var b = a.$slides.eq(a.currentSlide).outerHeight(!0);
            a.$list.animate({
                height: b
            }, a.options.speed)
        }
    }, b.prototype.animateSlide = function(b, c) {
        var d = {},
            e = this;
        e.animateHeight(), e.options.rtl === !0 && e.options.vertical === !1 && (b = -b), e.transformsEnabled === !1 ? e.options.vertical === !1 ? e.$slideTrack.animate({
            left: b
        }, e.options.speed, e.options.easing, c) : e.$slideTrack.animate({
            top: b
        }, e.options.speed, e.options.easing, c) : e.cssTransitions === !1 ? (e.options.rtl === !0 && (e.currentLeft = -e.currentLeft), a({
            animStart: e.currentLeft
        }).animate({
            animStart: b
        }, {
            duration: e.options.speed,
            easing: e.options.easing,
            step: function(a) {
                a = Math.ceil(a), e.options.vertical === !1 ? (d[e.animType] = "translate(" + a + "px, 0px)", e.$slideTrack.css(d)) : (d[e.animType] = "translate(0px," + a + "px)", e.$slideTrack.css(d))
            },
            complete: function() {
                c && c.call()
            }
        })) : (e.applyTransition(), b = Math.ceil(b), d[e.animType] = e.options.vertical === !1 ? "translate3d(" + b + "px, 0px, 0px)" : "translate3d(0px," + b + "px, 0px)", e.$slideTrack.css(d), c && setTimeout(function() {
            e.disableTransition(), c.call()
        }, e.options.speed))
    }, b.prototype.asNavFor = function(b) {
        var c = this,
            d = null !== c.options.asNavFor ? a(c.options.asNavFor).slick("getSlick") : null;
        null !== d && d.slideHandler(b, !0)
    }, b.prototype.applyTransition = function(a) {
        var b = this,
            c = {};
        c[b.transitionType] = b.options.fade === !1 ? b.transformType + " " + b.options.speed + "ms " + b.options.cssEase : "opacity " + b.options.speed + "ms " + b.options.cssEase, b.options.fade === !1 ? b.$slideTrack.css(c) : b.$slides.eq(a).css(c)
    }, b.prototype.autoPlay = function() {
        var a = this;
        a.autoPlayTimer && clearInterval(a.autoPlayTimer), a.slideCount > a.options.slidesToShow && a.paused !== !0 && (a.autoPlayTimer = setInterval(a.autoPlayIterator, a.options.autoplaySpeed))
    }, b.prototype.autoPlayClear = function() {
        var a = this;
        a.autoPlayTimer && clearInterval(a.autoPlayTimer)
    }, b.prototype.autoPlayIterator = function() {
        var a = this;
        a.options.infinite === !1 ? 1 === a.direction ? (a.currentSlide + 1 === a.slideCount - 1 && (a.direction = 0), a.slideHandler(a.currentSlide + a.options.slidesToScroll)) : (0 === a.currentSlide - 1 && (a.direction = 1), a.slideHandler(a.currentSlide - a.options.slidesToScroll)) : a.slideHandler(a.currentSlide + a.options.slidesToScroll)
    }, b.prototype.buildArrows = function() {
        var b = this;
        b.options.arrows === !0 && b.slideCount > b.options.slidesToShow && (b.$prevArrow = a(b.options.prevArrow), b.$nextArrow = a(b.options.nextArrow), b.htmlExpr.test(b.options.prevArrow) && b.$prevArrow.appendTo(b.options.appendArrows), b.htmlExpr.test(b.options.nextArrow) && b.$nextArrow.appendTo(b.options.appendArrows), b.options.infinite !== !0 && b.$prevArrow.addClass("slick-disabled"))
    }, b.prototype.buildDots = function() {
        var c, d, b = this;
        if (b.options.dots === !0 && b.slideCount > b.options.slidesToShow) {
            for (d = '<ul class="' + b.options.dotsClass + '">', c = 0; c <= b.getDotCount(); c += 1) d += "<li>" + b.options.customPaging.call(this, b, c) + "</li>";
            d += "</ul>", b.$dots = a(d).appendTo(b.options.appendDots), b.$dots.find("li").first().addClass("slick-active").attr("aria-hidden", "false")
        }
    }, b.prototype.buildOut = function() {
        var b = this;
        b.$slides = b.$slider.children(":not(.slick-cloned)").addClass("slick-slide"), b.slideCount = b.$slides.length, b.$slides.each(function(b, c) {
            a(c).attr("data-slick-index", b)
        }), b.$slidesCache = b.$slides, b.$slider.addClass("slick-slider"), b.$slideTrack = 0 === b.slideCount ? a('<div class="slick-track"/>').appendTo(b.$slider) : b.$slides.wrapAll('<div class="slick-track"/>').parent(), b.$list = b.$slideTrack.wrap('<div aria-live="polite" class="slick-list"/>').parent(), b.$slideTrack.css("opacity", 0), (b.options.centerMode === !0 || b.options.swipeToSlide === !0) && (b.options.slidesToScroll = 1), a("img[data-lazy]", b.$slider).not("[src]").addClass("slick-loading"), b.setupInfinite(), b.buildArrows(), b.buildDots(), b.updateDots(), b.options.accessibility === !0 && b.$list.prop("tabIndex", 0), b.setSlideClasses("number" == typeof this.currentSlide ? this.currentSlide : 0), b.options.draggable === !0 && b.$list.addClass("draggable")
    }, b.prototype.buildRows = function() {
        var b, c, d, e, f, g, h, a = this;
        if (e = document.createDocumentFragment(), g = a.$slider.children(), a.options.rows > 1) {
            for (h = a.options.slidesPerRow * a.options.rows, f = Math.ceil(g.length / h), b = 0; f > b; b++) {
                var i = document.createElement("div");
                for (c = 0; c < a.options.rows; c++) {
                    var j = document.createElement("div");
                    for (d = 0; d < a.options.slidesPerRow; d++) {
                        var k = b * h + (c * a.options.slidesPerRow + d);
                        g.get(k) && j.appendChild(g.get(k))
                    }
                    i.appendChild(j)
                }
                e.appendChild(i)
            }
            a.$slider.html(e), a.$slider.children().children().children().width(100 / a.options.slidesPerRow + "%").css({
                display: "inline-block"
            })
        }
    }, b.prototype.checkResponsive = function(b) {
        var d, e, f, c = this,
            g = c.$slider.width(),
            h = window.innerWidth || a(window).width();
        if ("window" === c.respondTo ? f = h : "slider" === c.respondTo ? f = g : "min" === c.respondTo && (f = Math.min(h, g)), c.originalSettings.responsive && c.originalSettings.responsive.length > -1 && null !== c.originalSettings.responsive) {
            e = null;
            for (d in c.breakpoints) c.breakpoints.hasOwnProperty(d) && (c.originalSettings.mobileFirst === !1 ? f < c.breakpoints[d] && (e = c.breakpoints[d]) : f > c.breakpoints[d] && (e = c.breakpoints[d]));
            null !== e ? null !== c.activeBreakpoint ? e !== c.activeBreakpoint && (c.activeBreakpoint = e, "unslick" === c.breakpointSettings[e] ? c.unslick() : (c.options = a.extend({}, c.originalSettings, c.breakpointSettings[e]), b === !0 && (c.currentSlide = c.options.initialSlide), c.refresh())) : (c.activeBreakpoint = e, "unslick" === c.breakpointSettings[e] ? c.unslick() : (c.options = a.extend({}, c.originalSettings, c.breakpointSettings[e]), b === !0 && (c.currentSlide = c.options.initialSlide), c.refresh())) : null !== c.activeBreakpoint && (c.activeBreakpoint = null, c.options = c.originalSettings, b === !0 && (c.currentSlide = c.options.initialSlide), c.refresh())
        }
    }, b.prototype.changeSlide = function(b, c) {
        var f, g, h, d = this,
            e = a(b.target);
        switch (e.is("a") && b.preventDefault(), h = 0 !== d.slideCount % d.options.slidesToScroll, f = h ? 0 : (d.slideCount - d.currentSlide) % d.options.slidesToScroll, b.data.message) {
            case "previous":
                g = 0 === f ? d.options.slidesToScroll : d.options.slidesToShow - f, d.slideCount > d.options.slidesToShow && d.slideHandler(d.currentSlide - g, !1, c);
                break;
            case "next":
                g = 0 === f ? d.options.slidesToScroll : f, d.slideCount > d.options.slidesToShow && d.slideHandler(d.currentSlide + g, !1, c);
                break;
            case "index":
                var i = 0 === b.data.index ? 0 : b.data.index || a(b.target).parent().index() * d.options.slidesToScroll;
                d.slideHandler(d.checkNavigable(i), !1, c);
                break;
            default:
                return
        }
    }, b.prototype.checkNavigable = function(a) {
        var c, d, b = this;
        if (c = b.getNavigableIndexes(), d = 0, a > c[c.length - 1]) a = c[c.length - 1];
        else
            for (var e in c) {
                if (a < c[e]) {
                    a = d;
                    break
                }
                d = c[e]
            }
        return a
    }, b.prototype.cleanUpEvents = function() {
        var b = this;
        b.options.dots === !0 && b.slideCount > b.options.slidesToShow && a("li", b.$dots).off("click.slick", b.changeSlide), b.options.dots === !0 && b.options.pauseOnDotsHover === !0 && b.options.autoplay === !0 && a("li", b.$dots).off("mouseenter.slick", b.setPaused.bind(b, !0)).off("mouseleave.slick", b.setPaused.bind(b, !1)), b.options.arrows === !0 && b.slideCount > b.options.slidesToShow && (b.$prevArrow && b.$prevArrow.off("click.slick", b.changeSlide), b.$nextArrow && b.$nextArrow.off("click.slick", b.changeSlide)), b.$list.off("touchstart.slick mousedown.slick", b.swipeHandler), b.$list.off("touchmove.slick mousemove.slick", b.swipeHandler), b.$list.off("touchend.slick mouseup.slick", b.swipeHandler), b.$list.off("touchcancel.slick mouseleave.slick", b.swipeHandler), b.$list.off("click.slick", b.clickHandler), b.options.autoplay === !0 && a(document).off(b.visibilityChange, b.visibility), b.$list.off("mouseenter.slick", b.setPaused.bind(b, !0)), b.$list.off("mouseleave.slick", b.setPaused.bind(b, !1)), b.options.accessibility === !0 && b.$list.off("keydown.slick", b.keyHandler), b.options.focusOnSelect === !0 && a(b.$slideTrack).children().off("click.slick", b.selectHandler), a(window).off("orientationchange.slick.slick-" + b.instanceUid, b.orientationChange), a(window).off("resize.slick.slick-" + b.instanceUid, b.resize), a("[draggable!=true]", b.$slideTrack).off("dragstart", b.preventDefault), a(window).off("load.slick.slick-" + b.instanceUid, b.setPosition), a(document).off("ready.slick.slick-" + b.instanceUid, b.setPosition)
    }, b.prototype.cleanUpRows = function() {
        var b, a = this;
        a.options.rows > 1 && (b = a.$slides.children().children(), b.removeAttr("style"), a.$slider.html(b))
    }, b.prototype.clickHandler = function(a) {
        var b = this;
        b.shouldClick === !1 && (a.stopImmediatePropagation(), a.stopPropagation(), a.preventDefault())
    }, b.prototype.destroy = function() {
        var b = this;
        b.autoPlayClear(), b.touchObject = {}, b.cleanUpEvents(), a(".slick-cloned", b.$slider).remove(), b.$dots && b.$dots.remove(), b.$prevArrow && "object" != typeof b.options.prevArrow && b.$prevArrow.remove(), b.$nextArrow && "object" != typeof b.options.nextArrow && b.$nextArrow.remove(), b.$slides && (b.$slides.removeClass("slick-slide slick-active slick-center slick-visible").attr("aria-hidden", "true").removeAttr("data-slick-index").css({
            position: "",
            left: "",
            top: "",
            zIndex: "",
            opacity: "",
            width: ""
        }), b.$slider.html(b.$slides)), b.cleanUpRows(), b.$slider.removeClass("slick-slider"), b.$slider.removeClass("slick-initialized")
    }, b.prototype.disableTransition = function(a) {
        var b = this,
            c = {};
        c[b.transitionType] = "", b.options.fade === !1 ? b.$slideTrack.css(c) : b.$slides.eq(a).css(c)
    }, b.prototype.fadeSlide = function(a, b) {
        var c = this;
        c.cssTransitions === !1 ? (c.$slides.eq(a).css({
            zIndex: 1e3
        }), c.$slides.eq(a).animate({
            opacity: 1
        }, c.options.speed, c.options.easing, b)) : (c.applyTransition(a), c.$slides.eq(a).css({
            opacity: 1,
            zIndex: 1e3
        }), b && setTimeout(function() {
            c.disableTransition(a), b.call()
        }, c.options.speed))
    }, b.prototype.filterSlides = b.prototype.slickFilter = function(a) {
        var b = this;
        null !== a && (b.unload(), b.$slideTrack.children(this.options.slide).detach(), b.$slidesCache.filter(a).appendTo(b.$slideTrack), b.reinit())
    }, b.prototype.getCurrent = b.prototype.slickCurrentSlide = function() {
        var a = this;
        return a.currentSlide
    }, b.prototype.getDotCount = function() {
        var a = this,
            b = 0,
            c = 0,
            d = 0;
        if (a.options.infinite === !0) d = Math.ceil(a.slideCount / a.options.slidesToScroll);
        else if (a.options.centerMode === !0) d = a.slideCount;
        else
            for (; b < a.slideCount;) ++d, b = c + a.options.slidesToShow, c += a.options.slidesToScroll <= a.options.slidesToShow ? a.options.slidesToScroll : a.options.slidesToShow;
        return d - 1
    }, b.prototype.getLeft = function(a) {
        var c, d, f, b = this,
            e = 0;
        return b.slideOffset = 0, d = b.$slides.first().outerHeight(), b.options.infinite === !0 ? (b.slideCount > b.options.slidesToShow && (b.slideOffset = -1 * b.slideWidth * b.options.slidesToShow, e = -1 * d * b.options.slidesToShow), 0 !== b.slideCount % b.options.slidesToScroll && a + b.options.slidesToScroll > b.slideCount && b.slideCount > b.options.slidesToShow && (a > b.slideCount ? (b.slideOffset = -1 * (b.options.slidesToShow - (a - b.slideCount)) * b.slideWidth, e = -1 * (b.options.slidesToShow - (a - b.slideCount)) * d) : (b.slideOffset = -1 * b.slideCount % b.options.slidesToScroll * b.slideWidth, e = -1 * b.slideCount % b.options.slidesToScroll * d))) : a + b.options.slidesToShow > b.slideCount && (b.slideOffset = (a + b.options.slidesToShow - b.slideCount) * b.slideWidth, e = (a + b.options.slidesToShow - b.slideCount) * d), b.slideCount <= b.options.slidesToShow && (b.slideOffset = 0, e = 0), b.options.centerMode === !0 && b.options.infinite === !0 ? b.slideOffset += b.slideWidth * Math.floor(b.options.slidesToShow / 2) - b.slideWidth : b.options.centerMode === !0 && (b.slideOffset = 0, b.slideOffset += b.slideWidth * Math.floor(b.options.slidesToShow / 2)), c = b.options.vertical === !1 ? -1 * a * b.slideWidth + b.slideOffset : -1 * a * d + e, b.options.variableWidth === !0 && (f = b.slideCount <= b.options.slidesToShow || b.options.infinite === !1 ? b.$slideTrack.children(".slick-slide").eq(a) : b.$slideTrack.children(".slick-slide").eq(a + b.options.slidesToShow), c = f[0] ? -1 * f[0].offsetLeft : 0, b.options.centerMode === !0 && (f = b.options.infinite === !1 ? b.$slideTrack.children(".slick-slide").eq(a) : b.$slideTrack.children(".slick-slide").eq(a + b.options.slidesToShow + 1), c = f[0] ? -1 * f[0].offsetLeft : 0, c += (b.$list.width() - f.outerWidth()) / 2)), c
    }, b.prototype.getOption = b.prototype.slickGetOption = function(a) {
        var b = this;
        return b.options[a]
    }, b.prototype.getNavigableIndexes = function() {
        var e, a = this,
            b = 0,
            c = 0,
            d = [];
        for (a.options.infinite === !1 ? (e = a.slideCount - a.options.slidesToShow + 1, a.options.centerMode === !0 && (e = a.slideCount)) : (b = -1 * a.options.slidesToScroll, c = -1 * a.options.slidesToScroll, e = 2 * a.slideCount); e > b;) d.push(b), b = c + a.options.slidesToScroll, c += a.options.slidesToScroll <= a.options.slidesToShow ? a.options.slidesToScroll : a.options.slidesToShow;
        return d
    }, b.prototype.getSlick = function() {
        return this
    }, b.prototype.getSlideCount = function() {
        var c, d, e, b = this;
        return e = b.options.centerMode === !0 ? b.slideWidth * Math.floor(b.options.slidesToShow / 2) : 0, b.options.swipeToSlide === !0 ? (b.$slideTrack.find(".slick-slide").each(function(c, f) {
            return f.offsetLeft - e + a(f).outerWidth() / 2 > -1 * b.swipeLeft ? (d = f, !1) : void 0
        }), c = Math.abs(a(d).attr("data-slick-index") - b.currentSlide) || 1) : b.options.slidesToScroll
    }, b.prototype.goTo = b.prototype.slickGoTo = function(a, b) {
        var c = this;
        c.changeSlide({
            data: {
                message: "index",
                index: parseInt(a)
            }
        }, b)
    }, b.prototype.init = function() {
        var b = this;
        a(b.$slider).hasClass("slick-initialized") || (a(b.$slider).addClass("slick-initialized"), b.buildRows(), b.buildOut(), b.setProps(), b.startLoad(), b.loadSlider(), b.initializeEvents(), b.updateArrows(), b.updateDots()), b.$slider.trigger("init", [b])
    }, b.prototype.initArrowEvents = function() {
        var a = this;
        a.options.arrows === !0 && a.slideCount > a.options.slidesToShow && (a.$prevArrow.on("click.slick", {
            message: "previous"
        }, a.changeSlide), a.$nextArrow.on("click.slick", {
            message: "next"
        }, a.changeSlide))
    }, b.prototype.initDotEvents = function() {
        var b = this;
        b.options.dots === !0 && b.slideCount > b.options.slidesToShow && a("li", b.$dots).on("click.slick", {
            message: "index"
        }, b.changeSlide), b.options.dots === !0 && b.options.pauseOnDotsHover === !0 && b.options.autoplay === !0 && a("li", b.$dots).on("mouseenter.slick", b.setPaused.bind(b, !0)).on("mouseleave.slick", b.setPaused.bind(b, !1))
    }, b.prototype.initializeEvents = function() {
        var b = this;
        b.initArrowEvents(), b.initDotEvents(), b.$list.on("touchstart.slick mousedown.slick", {
            action: "start"
        }, b.swipeHandler), b.$list.on("touchmove.slick mousemove.slick", {
            action: "move"
        }, b.swipeHandler), b.$list.on("touchend.slick mouseup.slick", {
            action: "end"
        }, b.swipeHandler), b.$list.on("touchcancel.slick mouseleave.slick", {
            action: "end"
        }, b.swipeHandler), b.$list.on("click.slick", b.clickHandler), b.options.autoplay === !0 && a(document).on(b.visibilityChange, b.visibility.bind(b)), b.$list.on("mouseenter.slick", b.setPaused.bind(b, !0)), b.$list.on("mouseleave.slick", b.setPaused.bind(b, !1)), b.options.accessibility === !0 && b.$list.on("keydown.slick", b.keyHandler), b.options.focusOnSelect === !0 && a(b.$slideTrack).children().on("click.slick", b.selectHandler), a(window).on("orientationchange.slick.slick-" + b.instanceUid, b.orientationChange.bind(b)), a(window).on("resize.slick.slick-" + b.instanceUid, b.resize.bind(b)), a("[draggable!=true]", b.$slideTrack).on("dragstart", b.preventDefault), a(window).on("load.slick.slick-" + b.instanceUid, b.setPosition), a(document).on("ready.slick.slick-" + b.instanceUid, b.setPosition)
    }, b.prototype.initUI = function() {
        var a = this;
        a.options.arrows === !0 && a.slideCount > a.options.slidesToShow && (a.$prevArrow.show(), a.$nextArrow.show()), a.options.dots === !0 && a.slideCount > a.options.slidesToShow && a.$dots.show(), a.options.autoplay === !0 && a.autoPlay()
    }, b.prototype.keyHandler = function(a) {
        var b = this;
        37 === a.keyCode && b.options.accessibility === !0 ? b.changeSlide({
            data: {
                message: "previous"
            }
        }) : 39 === a.keyCode && b.options.accessibility === !0 && b.changeSlide({
            data: {
                message: "next"
            }
        })
    }, b.prototype.lazyLoad = function() {
        function g(b) {
            a("img[data-lazy]", b).each(function() {
                var b = a(this),
                    c = a(this).attr("data-lazy"),
                    d = document.createElement("img");
                d.onload = function() {
                    b.animate({
                        opacity: 1
                    }, 200)
                }, d.src = c, b.css({
                    opacity: 0
                }).attr("src", c).removeAttr("data-lazy").removeClass("slick-loading")
            })
        }
        var c, d, e, f, b = this;
        b.options.centerMode === !0 ? b.options.infinite === !0 ? (e = b.currentSlide + (b.options.slidesToShow / 2 + 1), f = e + b.options.slidesToShow + 2) : (e = Math.max(0, b.currentSlide - (b.options.slidesToShow / 2 + 1)), f = 2 + (b.options.slidesToShow / 2 + 1) + b.currentSlide) : (e = b.options.infinite ? b.options.slidesToShow + b.currentSlide : b.currentSlide, f = e + b.options.slidesToShow, b.options.fade === !0 && (e > 0 && e--, f <= b.slideCount && f++)), c = b.$slider.find(".slick-slide").slice(e, f), g(c), b.slideCount <= b.options.slidesToShow ? (d = b.$slider.find(".slick-slide"), g(d)) : b.currentSlide >= b.slideCount - b.options.slidesToShow ? (d = b.$slider.find(".slick-cloned").slice(0, b.options.slidesToShow), g(d)) : 0 === b.currentSlide && (d = b.$slider.find(".slick-cloned").slice(-1 * b.options.slidesToShow), g(d))
    }, b.prototype.loadSlider = function() {
        var a = this;
        a.setPosition(), a.$slideTrack.css({
            opacity: 1
        }), a.$slider.removeClass("slick-loading"), a.initUI(), "progressive" === a.options.lazyLoad && a.progressiveLazyLoad()
    }, b.prototype.next = b.prototype.slickNext = function() {
        var a = this;
        a.changeSlide({
            data: {
                message: "next"
            }
        })
    }, b.prototype.orientationChange = function() {
        var a = this;
        a.checkResponsive(), a.setPosition()
    }, b.prototype.pause = b.prototype.slickPause = function() {
        var a = this;
        a.autoPlayClear(), a.paused = !0
    }, b.prototype.play = b.prototype.slickPlay = function() {
        var a = this;
        a.paused = !1, a.autoPlay()
    }, b.prototype.postSlide = function(a) {
        var b = this;
        b.$slider.trigger("afterChange", [b, a]), b.animating = !1, b.setPosition(), b.swipeLeft = null, b.options.autoplay === !0 && b.paused === !1 && b.autoPlay()
    }, b.prototype.prev = b.prototype.slickPrev = function() {
        var a = this;
        a.changeSlide({
            data: {
                message: "previous"
            }
        })
    }, b.prototype.preventDefault = function(a) {
        a.preventDefault()
    }, b.prototype.progressiveLazyLoad = function() {
        var c, d, b = this;
        c = a("img[data-lazy]", b.$slider).length, c > 0 && (d = a("img[data-lazy]", b.$slider).first(), d.attr("src", d.attr("data-lazy")).removeClass("slick-loading").load(function() {
            d.removeAttr("data-lazy"), b.progressiveLazyLoad(), b.options.adaptiveHeight === !0 && b.setPosition()
        }).error(function() {
            d.removeAttr("data-lazy"), b.progressiveLazyLoad()
        }))
    }, b.prototype.refresh = function() {
        var b = this,
            c = b.currentSlide;
        b.destroy(), a.extend(b, b.initials), b.init(), b.changeSlide({
            data: {
                message: "index",
                index: c
            }
        }, !1)
    }, b.prototype.reinit = function() {
        var b = this;
        b.$slides = b.$slideTrack.children(b.options.slide).addClass("slick-slide"), b.slideCount = b.$slides.length, b.currentSlide >= b.slideCount && 0 !== b.currentSlide && (b.currentSlide = b.currentSlide - b.options.slidesToScroll), b.slideCount <= b.options.slidesToShow && (b.currentSlide = 0), b.setProps(), b.setupInfinite(), b.buildArrows(), b.updateArrows(), b.initArrowEvents(), b.buildDots(), b.updateDots(), b.initDotEvents(), b.options.focusOnSelect === !0 && a(b.$slideTrack).children().on("click.slick", b.selectHandler), b.setSlideClasses(0), b.setPosition(), b.$slider.trigger("reInit", [b])
    }, b.prototype.resize = function() {
        var b = this;
        a(window).width() !== b.windowWidth && (clearTimeout(b.windowDelay), b.windowDelay = window.setTimeout(function() {
            b.windowWidth = a(window).width(), b.checkResponsive(), b.setPosition()
        }, 50))
    }, b.prototype.removeSlide = b.prototype.slickRemove = function(a, b, c) {
        var d = this;
        return "boolean" == typeof a ? (b = a, a = b === !0 ? 0 : d.slideCount - 1) : a = b === !0 ? --a : a, d.slideCount < 1 || 0 > a || a > d.slideCount - 1 ? !1 : (d.unload(), c === !0 ? d.$slideTrack.children().remove() : d.$slideTrack.children(this.options.slide).eq(a).remove(), d.$slides = d.$slideTrack.children(this.options.slide), d.$slideTrack.children(this.options.slide).detach(), d.$slideTrack.append(d.$slides), d.$slidesCache = d.$slides, void d.reinit())
    }, b.prototype.setCSS = function(a) {
        var d, e, b = this,
            c = {};
        b.options.rtl === !0 && (a = -a), d = "left" == b.positionProp ? Math.ceil(a) + "px" : "0px", e = "top" == b.positionProp ? Math.ceil(a) + "px" : "0px", c[b.positionProp] = a, b.transformsEnabled === !1 ? b.$slideTrack.css(c) : (c = {}, b.cssTransitions === !1 ? (c[b.animType] = "translate(" + d + ", " + e + ")", b.$slideTrack.css(c)) : (c[b.animType] = "translate3d(" + d + ", " + e + ", 0px)", b.$slideTrack.css(c)))
    }, b.prototype.setDimensions = function() {
        var a = this;
        a.options.vertical === !1 ? a.options.centerMode === !0 && a.$list.css({
            padding: "0px " + a.options.centerPadding
        }) : (a.$list.height(a.$slides.first().outerHeight(!0) * a.options.slidesToShow), a.options.centerMode === !0 && a.$list.css({
            padding: a.options.centerPadding + " 0px"
        })), a.listWidth = a.$list.width(), a.listHeight = a.$list.height(), a.options.vertical === !1 && a.options.variableWidth === !1 ? (a.slideWidth = Math.ceil(a.listWidth / a.options.slidesToShow), a.$slideTrack.width(Math.ceil(a.slideWidth * a.$slideTrack.children(".slick-slide").length))) : a.options.variableWidth === !0 ? a.$slideTrack.width(5e3 * a.slideCount) : (a.slideWidth = Math.ceil(a.listWidth), a.$slideTrack.height(Math.ceil(a.$slides.first().outerHeight(!0) * a.$slideTrack.children(".slick-slide").length)));
        var b = a.$slides.first().outerWidth(!0) - a.$slides.first().width();
        a.options.variableWidth === !1 && a.$slideTrack.children(".slick-slide").width(a.slideWidth - b)
    }, b.prototype.setFade = function() {
        var c, b = this;
        b.$slides.each(function(d, e) {
            c = -1 * b.slideWidth * d, b.options.rtl === !0 ? a(e).css({
                position: "relative",
                right: c,
                top: 0,
                zIndex: 800,
                opacity: 0
            }) : a(e).css({
                position: "relative",
                left: c,
                top: 0,
                zIndex: 800,
                opacity: 0
            })
        }), b.$slides.eq(b.currentSlide).css({
            zIndex: 900,
            opacity: 1
        })
    }, b.prototype.setHeight = function() {
        var a = this;
        if (1 === a.options.slidesToShow && a.options.adaptiveHeight === !0 && a.options.vertical === !1) {
            var b = a.$slides.eq(a.currentSlide).outerHeight(!0);
            a.$list.css("height", b)
        }
    }, b.prototype.setOption = b.prototype.slickSetOption = function(a, b, c) {
        var d = this;
        d.options[a] = b, c === !0 && (d.unload(), d.reinit())
    }, b.prototype.setPosition = function() {
        var a = this;
        a.setDimensions(), a.setHeight(), a.options.fade === !1 ? a.setCSS(a.getLeft(a.currentSlide)) : a.setFade(), a.$slider.trigger("setPosition", [a])
    }, b.prototype.setProps = function() {
        var a = this,
            b = document.body.style;
        a.positionProp = a.options.vertical === !0 ? "top" : "left", "top" === a.positionProp ? a.$slider.addClass("slick-vertical") : a.$slider.removeClass("slick-vertical"), (void 0 !== b.WebkitTransition || void 0 !== b.MozTransition || void 0 !== b.msTransition) && a.options.useCSS === !0 && (a.cssTransitions = !0), void 0 !== b.OTransform && (a.animType = "OTransform", a.transformType = "-o-transform", a.transitionType = "OTransition", void 0 === b.perspectiveProperty && void 0 === b.webkitPerspective && (a.animType = !1)), void 0 !== b.MozTransform && (a.animType = "MozTransform", a.transformType = "-moz-transform", a.transitionType = "MozTransition", void 0 === b.perspectiveProperty && void 0 === b.MozPerspective && (a.animType = !1)), void 0 !== b.webkitTransform && (a.animType = "webkitTransform", a.transformType = "-webkit-transform", a.transitionType = "webkitTransition", void 0 === b.perspectiveProperty && void 0 === b.webkitPerspective && (a.animType = !1)), void 0 !== b.msTransform && (a.animType = "msTransform", a.transformType = "-ms-transform", a.transitionType = "msTransition", void 0 === b.msTransform && (a.animType = !1)), void 0 !== b.transform && a.animType !== !1 && (a.animType = "transform", a.transformType = "transform", a.transitionType = "transition"), a.transformsEnabled = null !== a.animType && a.animType !== !1
    }, b.prototype.setSlideClasses = function(a) {
        var c, d, e, f, b = this;
        b.$slider.find(".slick-slide").removeClass("slick-active").attr("aria-hidden", "true").removeClass("slick-center"), d = b.$slider.find(".slick-slide"), b.options.centerMode === !0 ? (c = Math.floor(b.options.slidesToShow / 2), b.options.infinite === !0 && (a >= c && a <= b.slideCount - 1 - c ? b.$slides.slice(a - c, a + c + 1).addClass("slick-active").attr("aria-hidden", "false") : (e = b.options.slidesToShow + a, d.slice(e - c + 1, e + c + 2).addClass("slick-active").attr("aria-hidden", "false")), 0 === a ? d.eq(d.length - 1 - b.options.slidesToShow).addClass("slick-center") : a === b.slideCount - 1 && d.eq(b.options.slidesToShow).addClass("slick-center")), b.$slides.eq(a).addClass("slick-center")) : a >= 0 && a <= b.slideCount - b.options.slidesToShow ? b.$slides.slice(a, a + b.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false") : d.length <= b.options.slidesToShow ? d.addClass("slick-active").attr("aria-hidden", "false") : (f = b.slideCount % b.options.slidesToShow, e = b.options.infinite === !0 ? b.options.slidesToShow + a : a, b.options.slidesToShow == b.options.slidesToScroll && b.slideCount - a < b.options.slidesToShow ? d.slice(e - (b.options.slidesToShow - f), e + f).addClass("slick-active").attr("aria-hidden", "false") : d.slice(e, e + b.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false")), "ondemand" === b.options.lazyLoad && b.lazyLoad()
    }, b.prototype.setupInfinite = function() {
        var c, d, e, b = this;
        if (b.options.fade === !0 && (b.options.centerMode = !1), b.options.infinite === !0 && b.options.fade === !1 && (d = null, b.slideCount > b.options.slidesToShow)) {
            for (e = b.options.centerMode === !0 ? b.options.slidesToShow + 1 : b.options.slidesToShow, c = b.slideCount; c > b.slideCount - e; c -= 1) d = c - 1, a(b.$slides[d]).clone(!0).attr("id", "").attr("data-slick-index", d - b.slideCount).prependTo(b.$slideTrack).addClass("slick-cloned");
            for (c = 0; e > c; c += 1) d = c, a(b.$slides[d]).clone(!0).attr("id", "").attr("data-slick-index", d + b.slideCount).appendTo(b.$slideTrack).addClass("slick-cloned");
            b.$slideTrack.find(".slick-cloned").find("[id]").each(function() {
                a(this).attr("id", "")
            })
        }
    }, b.prototype.setPaused = function(a) {
        var b = this;
        b.options.autoplay === !0 && b.options.pauseOnHover === !0 && (b.paused = a, b.autoPlayClear())
    }, b.prototype.selectHandler = function(b) {
        var c = this,
            d = a(b.target).is(".slick-slide") ? a(b.target) : a(b.target).parents(".slick-slide"),
            e = parseInt(d.attr("data-slick-index"));
        return e || (e = 0), c.slideCount <= c.options.slidesToShow ? (c.$slider.find(".slick-slide").removeClass("slick-active").attr("aria-hidden", "true"), c.$slides.eq(e).addClass("slick-active").attr("aria-hidden", "false"), c.options.centerMode === !0 && (c.$slider.find(".slick-slide").removeClass("slick-center"), c.$slides.eq(e).addClass("slick-center")), void c.asNavFor(e)) : void c.slideHandler(e)
    }, b.prototype.slideHandler = function(a, b, c) {
        var d, e, f, g, h = null,
            i = this;
        return b = b || !1, i.animating === !0 && i.options.waitForAnimate === !0 || i.options.fade === !0 && i.currentSlide === a || i.slideCount <= i.options.slidesToShow ? void 0 : (b === !1 && i.asNavFor(a), d = a, h = i.getLeft(d), g = i.getLeft(i.currentSlide), i.currentLeft = null === i.swipeLeft ? g : i.swipeLeft, i.options.infinite === !1 && i.options.centerMode === !1 && (0 > a || a > i.getDotCount() * i.options.slidesToScroll) ? void(i.options.fade === !1 && (d = i.currentSlide, c !== !0 ? i.animateSlide(g, function() {
            i.postSlide(d)
        }) : i.postSlide(d))) : i.options.infinite === !1 && i.options.centerMode === !0 && (0 > a || a > i.slideCount - i.options.slidesToScroll) ? void(i.options.fade === !1 && (d = i.currentSlide, c !== !0 ? i.animateSlide(g, function() {
            i.postSlide(d)
        }) : i.postSlide(d))) : (i.options.autoplay === !0 && clearInterval(i.autoPlayTimer), e = 0 > d ? 0 !== i.slideCount % i.options.slidesToScroll ? i.slideCount - i.slideCount % i.options.slidesToScroll : i.slideCount + d : d >= i.slideCount ? 0 !== i.slideCount % i.options.slidesToScroll ? 0 : d - i.slideCount : d, i.animating = !0, i.$slider.trigger("beforeChange", [i, i.currentSlide, e]), f = i.currentSlide, i.currentSlide = e, i.setSlideClasses(i.currentSlide), i.updateDots(), i.updateArrows(), i.options.fade === !0 ? (c !== !0 ? i.fadeSlide(e, function() {
            i.postSlide(e)
        }) : i.postSlide(e), void i.animateHeight()) : void(c !== !0 ? i.animateSlide(h, function() {
            i.postSlide(e)
        }) : i.postSlide(e))))
    }, b.prototype.startLoad = function() {
        var a = this;
        a.options.arrows === !0 && a.slideCount > a.options.slidesToShow && (a.$prevArrow.hide(), a.$nextArrow.hide()), a.options.dots === !0 && a.slideCount > a.options.slidesToShow && a.$dots.hide(), a.$slider.addClass("slick-loading")
    }, b.prototype.swipeDirection = function() {
        var a, b, c, d, e = this;
        return a = e.touchObject.startX - e.touchObject.curX, b = e.touchObject.startY - e.touchObject.curY, c = Math.atan2(b, a), d = Math.round(180 * c / Math.PI), 0 > d && (d = 360 - Math.abs(d)), 45 >= d && d >= 0 ? e.options.rtl === !1 ? "left" : "right" : 360 >= d && d >= 315 ? e.options.rtl === !1 ? "left" : "right" : d >= 135 && 225 >= d ? e.options.rtl === !1 ? "right" : "left" : e.options.verticalSwiping === !0 ? d >= 35 && 135 >= d ? "left" : "right" : "vertical"
    }, b.prototype.swipeEnd = function() {
        var c, b = this;
        if (b.dragging = !1, b.shouldClick = b.touchObject.swipeLength > 10 ? !1 : !0, void 0 === b.touchObject.curX) return !1;
        if (b.touchObject.edgeHit === !0 && b.$slider.trigger("edge", [b, b.swipeDirection()]), b.touchObject.swipeLength >= b.touchObject.minSwipe) switch (b.swipeDirection()) {
            case "left":
                c = b.options.swipeToSlide ? b.checkNavigable(b.currentSlide + b.getSlideCount()) : b.currentSlide + b.getSlideCount(), b.slideHandler(c), b.currentDirection = 0, b.touchObject = {}, b.$slider.trigger("swipe", [b, "left"]);
                break;
            case "right":
                c = b.options.swipeToSlide ? b.checkNavigable(b.currentSlide - b.getSlideCount()) : b.currentSlide - b.getSlideCount(), b.slideHandler(c), b.currentDirection = 1, b.touchObject = {}, b.$slider.trigger("swipe", [b, "right"])
        } else b.touchObject.startX !== b.touchObject.curX && (b.slideHandler(b.currentSlide), b.touchObject = {})
    }, b.prototype.swipeHandler = function(a) {
        var b = this;
        if (!(b.options.swipe === !1 || "ontouchend" in document && b.options.swipe === !1 || b.options.draggable === !1 && -1 !== a.type.indexOf("mouse"))) switch (b.touchObject.fingerCount = a.originalEvent && void 0 !== a.originalEvent.touches ? a.originalEvent.touches.length : 1, b.touchObject.minSwipe = b.listWidth / b.options.touchThreshold, b.options.verticalSwiping === !0 && (b.touchObject.minSwipe = b.listHeight / b.options.touchThreshold), a.data.action) {
            case "start":
                b.swipeStart(a);
                break;
            case "move":
                b.swipeMove(a);
                break;
            case "end":
                b.swipeEnd(a)
        }
    }, b.prototype.swipeMove = function(a) {
        var d, e, f, g, h, b = this;
        return h = void 0 !== a.originalEvent ? a.originalEvent.touches : null, !b.dragging || h && 1 !== h.length ? !1 : (d = b.getLeft(b.currentSlide), b.touchObject.curX = void 0 !== h ? h[0].pageX : a.clientX, b.touchObject.curY = void 0 !== h ? h[0].pageY : a.clientY, b.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(b.touchObject.curX - b.touchObject.startX, 2))), b.options.verticalSwiping === !0 && (b.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(b.touchObject.curY - b.touchObject.startY, 2)))),
            e = b.swipeDirection(), "vertical" !== e ? (void 0 !== a.originalEvent && b.touchObject.swipeLength > 4 && a.preventDefault(), g = (b.options.rtl === !1 ? 1 : -1) * (b.touchObject.curX > b.touchObject.startX ? 1 : -1), b.options.verticalSwiping === !0 && (g = b.touchObject.curY > b.touchObject.startY ? 1 : -1), f = b.touchObject.swipeLength, b.touchObject.edgeHit = !1, b.options.infinite === !1 && (0 === b.currentSlide && "right" === e || b.currentSlide >= b.getDotCount() && "left" === e) && (f = b.touchObject.swipeLength * b.options.edgeFriction, b.touchObject.edgeHit = !0), b.swipeLeft = b.options.vertical === !1 ? d + f * g : d + f * (b.$list.height() / b.listWidth) * g, b.options.verticalSwiping === !0 && (b.swipeLeft = d + f * g), b.options.fade === !0 || b.options.touchMove === !1 ? !1 : b.animating === !0 ? (b.swipeLeft = null, !1) : void b.setCSS(b.swipeLeft)) : void 0)
    }, b.prototype.swipeStart = function(a) {
        var c, b = this;
        return 1 !== b.touchObject.fingerCount || b.slideCount <= b.options.slidesToShow ? (b.touchObject = {}, !1) : (void 0 !== a.originalEvent && void 0 !== a.originalEvent.touches && (c = a.originalEvent.touches[0]), b.touchObject.startX = b.touchObject.curX = void 0 !== c ? c.pageX : a.clientX, b.touchObject.startY = b.touchObject.curY = void 0 !== c ? c.pageY : a.clientY, void(b.dragging = !0))
    }, b.prototype.unfilterSlides = b.prototype.slickUnfilter = function() {
        var a = this;
        null !== a.$slidesCache && (a.unload(), a.$slideTrack.children(this.options.slide).detach(), a.$slidesCache.appendTo(a.$slideTrack), a.reinit())
    }, b.prototype.unload = function() {
        var b = this;
        a(".slick-cloned", b.$slider).remove(), b.$dots && b.$dots.remove(), b.$prevArrow && "object" != typeof b.options.prevArrow && b.$prevArrow.remove(), b.$nextArrow && "object" != typeof b.options.nextArrow && b.$nextArrow.remove(), b.$slides.removeClass("slick-slide slick-active slick-visible").attr("aria-hidden", "true").css("width", "")
    }, b.prototype.unslick = function() {
        var a = this;
        a.destroy()
    }, b.prototype.updateArrows = function() {
        var b, a = this;
        b = Math.floor(a.options.slidesToShow / 2), a.options.arrows === !0 && a.options.infinite !== !0 && a.slideCount > a.options.slidesToShow && (a.$prevArrow.removeClass("slick-disabled"), a.$nextArrow.removeClass("slick-disabled"), 0 === a.currentSlide ? (a.$prevArrow.addClass("slick-disabled"), a.$nextArrow.removeClass("slick-disabled")) : a.currentSlide >= a.slideCount - a.options.slidesToShow && a.options.centerMode === !1 ? (a.$nextArrow.addClass("slick-disabled"), a.$prevArrow.removeClass("slick-disabled")) : a.currentSlide >= a.slideCount - 1 && a.options.centerMode === !0 && (a.$nextArrow.addClass("slick-disabled"), a.$prevArrow.removeClass("slick-disabled")))
    }, b.prototype.updateDots = function() {
        var a = this;
        null !== a.$dots && (a.$dots.find("li").removeClass("slick-active").attr("aria-hidden", "true"), a.$dots.find("li").eq(Math.floor(a.currentSlide / a.options.slidesToScroll)).addClass("slick-active").attr("aria-hidden", "false"))
    }, b.prototype.visibility = function() {
        var a = this;
        document[a.hidden] ? (a.paused = !0, a.autoPlayClear()) : (a.paused = !1, a.autoPlay())
    }, a.fn.slick = function() {
        var g, a = this,
            c = arguments[0],
            d = Array.prototype.slice.call(arguments, 1),
            e = a.length,
            f = 0;
        for (f; e > f; f++)
            if ("object" == typeof c || "undefined" == typeof c ? a[f].slick = new b(a[f], c) : g = a[f].slick[c].apply(a[f].slick, d), "undefined" != typeof g) return g;
        return a
    }
}),
function(a) {
    "function" == typeof define && define.amd ? define(["jquery"], a) : "object" == typeof exports ? module.exports = a(require("jquery")) : a(jQuery)
}(function($) {
    if ($.support.cors || !$.ajaxTransport || !window.XDomainRequest) return $;
    var n = /^(https?:)?\/\//i,
        o = /^get|post$/i,
        p = new RegExp("^(//|" + location.protocol + ")", "i");
    return $.ajaxTransport("* text html xml json", function(j, k, l) {
        if (j.crossDomain && j.async && o.test(j.type) && n.test(j.url) && p.test(j.url)) {
            var m = null;
            return {
                send: function(f, g) {
                    var h = "",
                        i = (k.dataType || "").toLowerCase();
                    m = new XDomainRequest, /^\d+$/.test(k.timeout) && (m.timeout = k.timeout), m.ontimeout = function() {
                        g(500, "timeout")
                    }, m.onload = function() {
                        var a = "Content-Length: " + m.responseText.length + "\r\nContent-Type: " + m.contentType,
                            b = {
                                code: 200,
                                message: "success"
                            },
                            c = {
                                text: m.responseText
                            };
                        try {
                            if ("html" === i || /text\/html/i.test(m.contentType)) c.html = m.responseText;
                            else if ("json" === i || "text" !== i && /\/json/i.test(m.contentType)) try {
                                c.json = $.parseJSON(m.responseText)
                            } catch (e) {
                                b.code = 500, b.message = "parseerror"
                            } else if ("xml" === i || "text" !== i && /\/xml/i.test(m.contentType)) {
                                var d = new ActiveXObject("Microsoft.XMLDOM");
                                d.async = !1;
                                try {
                                    d.loadXML(m.responseText)
                                } catch (e) {
                                    d = void 0
                                }
                                if (!d || !d.documentElement || d.getElementsByTagName("parsererror").length) throw b.code = 500, b.message = "parseerror", "Invalid XML: " + m.responseText;
                                c.xml = d
                            }
                        } catch (parseMessage) {
                            throw parseMessage
                        } finally {
                            g(b.code, b.message, c, a)
                        }
                    }, m.onprogress = function() {}, m.onerror = function() {
                        g(500, "error", {
                            text: m.responseText
                        })
                    }, k.data && (h = "string" === $.type(k.data) ? k.data : $.param(k.data)), m.open(j.type, j.url), m.send(h)
                },
                abort: function() {
                    m && m.abort()
                }
            }
        }
    }), $
}), ! function() {
    var t;
    Function && Function.prototype && Function.prototype.bind && (/MSIE [678]/.test(navigator.userAgent) || ! function e(t, n, i) {
        function r(s, a) {
            if (!n[s]) {
                if (!t[s]) {
                    var c = "function" == typeof require && require;
                    if (!a && c) return c(s, !0);
                    if (o) return o(s, !0);
                    var u = new Error("Cannot find module '" + s + "'");
                    throw u.code = "MODULE_NOT_FOUND", u
                }
                var l = n[s] = {
                    exports: {}
                };
                t[s][0].call(l.exports, function(e) {
                    var n = t[s][1][e];
                    return r(n ? n : e)
                }, l, l.exports, e, t, n, i)
            }
            return n[s].exports
        }
        for (var o = "function" == typeof require && require, s = 0; s < i.length; s++) r(i[s]);
        return r
    }({
        1: [function(e, n, i) {
            (function() {
                "use strict";

                function e(t) {
                    return "function" == typeof t || "object" == typeof t && null !== t
                }

                function i(t) {
                    return "function" == typeof t
                }

                function r(t) {
                    return "object" == typeof t && null !== t
                }

                function o() {}

                function s() {
                    return function() {
                        process.nextTick(l)
                    }
                }

                function a() {
                    var t = 0,
                        e = new U(l),
                        n = document.createTextNode("");
                    return e.observe(n, {
                            characterData: !0
                        }),
                        function() {
                            n.data = t = ++t % 2
                        }
                }

                function c() {
                    var t = new MessageChannel;
                    return t.port1.onmessage = l,
                        function() {
                            t.port2.postMessage(0)
                        }
                }

                function u() {
                    return function() {
                        setTimeout(l, 1)
                    }
                }

                function l() {
                    for (var t = 0; M > t; t += 2) {
                        var e = q[t],
                            n = q[t + 1];
                        e(n), q[t] = void 0, q[t + 1] = void 0
                    }
                    M = 0
                }

                function d() {}

                function h() {
                    return new TypeError("You cannot resolve a promise with itself")
                }

                function f() {
                    return new TypeError("A promises callback cannot return that same promise.")
                }

                function p(t) {
                    try {
                        return t.then
                    } catch (e) {
                        return V.error = e, V
                    }
                }

                function m(t, e, n, i) {
                    try {
                        t.call(e, n, i)
                    } catch (r) {
                        return r
                    }
                }

                function g(t, e, n) {
                    H(function(t) {
                        var i = !1,
                            r = m(n, e, function(n) {
                                i || (i = !0, e !== n ? w(t, n) : _(t, n))
                            }, function(e) {
                                i || (i = !0, E(t, e))
                            }, "Settle: " + (t._label || " unknown promise"));
                        !i && r && (i = !0, E(t, r))
                    }, t)
                }

                function v(t, e) {
                    e._state === z ? _(t, e._result) : t._state === B ? E(t, e._result) : A(e, void 0, function(e) {
                        w(t, e)
                    }, function(e) {
                        E(t, e)
                    })
                }

                function y(t, e) {
                    if (e.constructor === t.constructor) v(t, e);
                    else {
                        var n = p(e);
                        n === V ? E(t, V.error) : void 0 === n ? _(t, e) : i(n) ? g(t, e, n) : _(t, e)
                    }
                }

                function w(t, n) {
                    t === n ? E(t, h()) : e(n) ? y(t, n) : _(t, n)
                }

                function b(t) {
                    t._onerror && t._onerror(t._result), T(t)
                }

                function _(t, e) {
                    t._state === F && (t._result = e, t._state = z, 0 === t._subscribers.length || H(T, t))
                }

                function E(t, e) {
                    t._state === F && (t._state = B, t._result = e, H(b, t))
                }

                function A(t, e, n, i) {
                    var r = t._subscribers,
                        o = r.length;
                    t._onerror = null, r[o] = e, r[o + z] = n, r[o + B] = i, 0 === o && t._state && H(T, t)
                }

                function T(t) {
                    var e = t._subscribers,
                        n = t._state;
                    if (0 !== e.length) {
                        for (var i, r, o = t._result, s = 0; s < e.length; s += 3) i = e[s], r = e[s + n], i ? S(n, i, r, o) : r(o);
                        t._subscribers.length = 0
                    }
                }

                function x() {
                    this.error = null
                }

                function I(t, e) {
                    try {
                        return t(e)
                    } catch (n) {
                        return G.error = n, G
                    }
                }

                function S(t, e, n, r) {
                    var o, s, a, c, u = i(n);
                    if (u) {
                        if (o = I(n, r), o === G ? (c = !0, s = o.error, o = null) : a = !0, e === o) return void E(e, f())
                    } else o = r, a = !0;
                    e._state !== F || (u && a ? w(e, o) : c ? E(e, s) : t === z ? _(e, o) : t === B && E(e, o))
                }

                function D(t, e) {
                    try {
                        e(function(e) {
                            w(t, e)
                        }, function(e) {
                            E(t, e)
                        })
                    } catch (n) {
                        E(t, n)
                    }
                }

                function N(t, e, n, i) {
                    this._instanceConstructor = t, this.promise = new t(d, i), this._abortOnReject = n, this._validateInput(e) ? (this._input = e, this.length = e.length, this._remaining = e.length, this._init(), 0 === this.length ? _(this.promise, this._result) : (this.length = this.length || 0, this._enumerate(), 0 === this._remaining && _(this.promise, this._result))) : E(this.promise, this._validationError())
                }

                function C() {
                    throw new TypeError("You must pass a resolver function as the first argument to the promise constructor")
                }

                function P() {
                    throw new TypeError("Failed to construct 'Promise': Please use the 'new' operator, this object constructor cannot be called as a function.")
                }

                function R(t) {
                    this._id = X++, this._state = void 0, this._result = void 0, this._subscribers = [], d !== t && (i(t) || C(), this instanceof R || P(), D(this, t))
                }
                var L;
                L = Array.isArray ? Array.isArray : function(t) {
                    return "[object Array]" === Object.prototype.toString.call(t)
                };
                var k, O = L,
                    M = (Date.now || function() {
                        return (new Date).getTime()
                    }, Object.create || function(t) {
                        if (arguments.length > 1) throw new Error("Second argument not supported");
                        if ("object" != typeof t) throw new TypeError("Argument must be an object");
                        return o.prototype = t, new o
                    }, 0),
                    H = function(t, e) {
                        q[M] = t, q[M + 1] = e, M += 2, 2 === M && k()
                    },
                    W = "undefined" != typeof window ? window : {},
                    U = W.MutationObserver || W.WebKitMutationObserver,
                    j = "undefined" != typeof Uint8ClampedArray && "undefined" != typeof importScripts && "undefined" != typeof MessageChannel,
                    q = new Array(1e3);
                k = "undefined" != typeof process && "[object process]" === {}.toString.call(process) ? s() : U ? a() : j ? c() : u();
                var F = void 0,
                    z = 1,
                    B = 2,
                    V = new x,
                    G = new x;
                N.prototype._validateInput = function(t) {
                    return O(t)
                }, N.prototype._validationError = function() {
                    return new Error("Array Methods must be provided an Array")
                }, N.prototype._init = function() {
                    this._result = new Array(this.length)
                };
                var $ = N;
                N.prototype._enumerate = function() {
                    for (var t = this.length, e = this.promise, n = this._input, i = 0; e._state === F && t > i; i++) this._eachEntry(n[i], i)
                }, N.prototype._eachEntry = function(t, e) {
                    var n = this._instanceConstructor;
                    r(t) ? t.constructor === n && t._state !== F ? (t._onerror = null, this._settledAt(t._state, e, t._result)) : this._willSettleAt(n.resolve(t), e) : (this._remaining--, this._result[e] = this._makeResult(z, e, t))
                }, N.prototype._settledAt = function(t, e, n) {
                    var i = this.promise;
                    i._state === F && (this._remaining--, this._abortOnReject && t === B ? E(i, n) : this._result[e] = this._makeResult(t, e, n)), 0 === this._remaining && _(i, this._result)
                }, N.prototype._makeResult = function(t, e, n) {
                    return n
                }, N.prototype._willSettleAt = function(t, e) {
                    var n = this;
                    A(t, void 0, function(t) {
                        n._settledAt(z, e, t)
                    }, function(t) {
                        n._settledAt(B, e, t)
                    })
                };
                var J = function(t, e) {
                        return new $(this, t, !0, e).promise
                    },
                    K = function(t, e) {
                        function n(t) {
                            w(o, t)
                        }

                        function i(t) {
                            E(o, t)
                        }
                        var r = this,
                            o = new r(d, e);
                        if (!O(t)) return E(o, new TypeError("You must pass an array to race.")), o;
                        for (var s = t.length, a = 0; o._state === F && s > a; a++) A(r.resolve(t[a]), void 0, n, i);
                        return o
                    },
                    Y = function(t, e) {
                        var n = this;
                        if (t && "object" == typeof t && t.constructor === n) return t;
                        var i = new n(d, e);
                        return w(i, t), i
                    },
                    Q = function(t, e) {
                        var n = this,
                            i = new n(d, e);
                        return E(i, t), i
                    },
                    X = 0,
                    Z = R;
                R.all = J, R.race = K, R.resolve = Y, R.reject = Q, R.prototype = {
                    constructor: R,
                    then: function(t, e) {
                        var n = this,
                            i = n._state;
                        if (i === z && !t || i === B && !e) return this;
                        var r = new this.constructor(d),
                            o = n._result;
                        if (i) {
                            var s = arguments[i - 1];
                            H(function() {
                                S(i, r, s, o)
                            })
                        } else A(n, r, t, e);
                        return r
                    },
                    "catch": function(t) {
                        return this.then(null, t)
                    }
                };
                var tt = function() {
                        var t;
                        t = "undefined" != typeof global ? global : "undefined" != typeof window && window.document ? window : self;
                        var e = "Promise" in t && "resolve" in t.Promise && "reject" in t.Promise && "all" in t.Promise && "race" in t.Promise && function() {
                            var e;
                            return new t.Promise(function(t) {
                                e = t
                            }), i(e)
                        }();
                        e || (t.Promise = Z)
                    },
                    et = {
                        Promise: Z,
                        polyfill: tt
                    };
                "function" == typeof t && t.amd ? t(function() {
                    return et
                }) : "undefined" != typeof n && n.exports ? n.exports = et : "undefined" != typeof this && (this.ES6Promise = et)
            }).call(this)
        }, {}],
        2: [function(t, e, n) {
            function i(t, e) {
                var n;
                return e = e || s, (n = e.requestAnimationFrame || e.webkitRequestAnimationFrame || e.mozRequestAnimationFrame || e.msRequestAnimationFrame || e.oRequestAnimationFrame || function() {
                    e.setTimeout(function() {
                        t(+new Date)
                    }, 1e3 / 60)
                })(t)
            }

            function r(t, e) {
                return Math.sin(Math.PI / 2 * e) * t
            }

            function o(t, e, n, r, o) {
                function s() {
                    var c = +new Date,
                        u = c - a,
                        l = Math.min(u / n, 1),
                        d = r ? r(e, l) : e * l,
                        h = 1 == l;
                    t(d, h), h || i(s, o)
                }
                var a = +new Date;
                i(s)
            }
            var s = t(16);
            e.exports = {
                animate: o,
                requestAnimationFrame: i,
                easeOut: r
            }
        }, {
            16: 16
        }],
        3: [function(t, e, n) {
            function i() {
                return a.builtUrl(u)
            }

            function r(t) {
                return "dark" === t ? "dark" : "light"
            }

            function o(t, e, n) {
                var i, o;
                return n = r(n), i = s.isRtlLang(e) ? "rtl" : "ltr", o = [t, c.css, n, i, "css"].join("."), a.base() + "/css/" + o
            }
            var s = t(23),
                a = t(43),
                c = t(82),
                u = "embed/timeline.css";
            e.exports = {
                tweet: o.bind(null, "tweet"),
                timeline: i,
                video: o.bind(null, "video")
            }
        }, {
            23: 23,
            43: 43,
            82: 82
        }],
        4: [function(t, e, n) {
            function i(t) {
                return {
                    success: !0,
                    resp: t
                }
            }
            e.exports = i
        }, {}],
        5: [function(t, e, n) {
            function i() {
                return l + d++
            }

            function r(t, e, n, r) {
                var l, d, h;
                return r = r || i(), l = s.fullPath(["callbacks", r]), d = o.createElement("script"), h = new a, e = c.aug({}, e, {
                    callback: l,
                    suppress_response_codes: !0
                }), s.set(["callbacks", r], function(t) {
                    var e, i;
                    e = n(t || !1), t = e.resp, i = e.success, i ? h.resolve(t) : h.reject(t), d.onload = d.onreadystatechange = null, d.parentNode && d.parentNode.removeChild(d), s.unset(["callbacks", r])
                }), d.onerror = function() {
                    h.reject(new Error("failed to fetch " + d.src))
                }, d.src = u.url(t, e), d.async = "async", o.body.appendChild(d), h.promise
            }
            var o = t(13),
                s = t(20),
                a = t(70),
                c = t(79),
                u = t(73),
                l = "cb",
                d = 0;
            e.exports = {
                fetch: r
            }
        }, {
            13: 13,
            20: 20,
            70: 70,
            73: 73,
            79: 79
        }],
        6: [function(t, e, n) {
            function i(t) {
                var e, n;
                return e = t.headers && t.headers.status, n = t && !t.error && 200 === e, !n && t.headers && t.headers.message && r.warn(t.headers.message), {
                    success: n,
                    resp: t
                }
            }
            var r = t(67);
            e.exports = i
        }, {
            67: 67
        }],
        7: [function(t, e, n) {
            function i(t) {
                return 10 > t ? "0" + t : t
            }

            function r(t) {
                function e(t, e) {
                    return n && n[t] && (t = n[t]), t.replace(/%\{([\w_]+)\}/g, function(t, n) {
                        return void 0 !== e[n] ? e[n] : t
                    })
                }
                var n = t && t.phrases,
                    o = t && t.months || c,
                    s = t && t.formats || u;
                this.timeAgo = function(t) {
                    var n, i = r.parseDate(t),
                        a = +new Date,
                        c = a - i;
                    return i ? isNaN(c) || 2 * l > c ? e("now") : d > c ? (n = Math.floor(c / l), e(s.abbr, {
                        number: n,
                        symbol: e(p, {
                            abbr: e("s"),
                            expanded: e(n > 1 ? "seconds" : "second")
                        })
                    })) : h > c ? (n = Math.floor(c / d), e(s.abbr, {
                        number: n,
                        symbol: e(p, {
                            abbr: e("m"),
                            expanded: e(n > 1 ? "minutes" : "minute")
                        })
                    })) : f > c ? (n = Math.floor(c / h), e(s.abbr, {
                        number: n,
                        symbol: e(p, {
                            abbr: e("h"),
                            expanded: e(n > 1 ? "hours" : "hour")
                        })
                    })) : 365 * f > c ? e(s.shortdate, {
                        day: i.getDate(),
                        month: e(o[i.getMonth()])
                    }) : e(s.longdate, {
                        day: i.getDate(),
                        month: e(o[i.getMonth()]),
                        year: i.getFullYear().toString().slice(2)
                    }) : ""
                }, this.localTimeStamp = function(t) {
                    var n = r.parseDate(t),
                        a = n && n.getHours();
                    return n ? e(s.full, {
                        day: n.getDate(),
                        month: e(o[n.getMonth()]),
                        year: n.getFullYear(),
                        hours24: i(a),
                        hours12: 13 > a ? a ? a : "12" : a - 12,
                        minutes: i(n.getMinutes()),
                        seconds: i(n.getSeconds()),
                        amPm: e(12 > a ? "AM" : "PM")
                    }) : ""
                }
            }
            var o = /(\d{4})-?(\d{2})-?(\d{2})T(\d{2}):?(\d{2}):?(\d{2})(Z|[\+\-]\d{2}:?\d{2})/,
                s = /[a-z]{3,4} ([a-z]{3}) (\d{1,2}) (\d{1,2}):(\d{2}):(\d{2}) ([\+\-]\d{2}:?\d{2}) (\d{4})/i,
                a = /^\d+$/,
                c = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                u = {
                    abbr: "%{number}%{symbol}",
                    shortdate: "%{day} %{month}",
                    longdate: "%{day} %{month} %{year}",
                    full: "%{hours12}:%{minutes} %{amPm} - %{day} %{month} %{year}"
                },
                l = 1e3,
                d = 60 * l,
                h = 60 * d,
                f = 24 * h,
                p = '<abbr title="%{expanded}">%{abbr}</abbr>';
            r.parseDate = function(t) {
                var e, n, i = t || "",
                    r = i.toString();
                return (e = function() {
                    var t;
                    return a.test(r) ? parseInt(r, 10) : (t = r.match(s)) ? Date.UTC(t[7], c.indexOf(t[1]), t[2], t[3], t[4], t[5]) : (t = r.match(o)) ? Date.UTC(t[1], t[2] - 1, t[3], t[4], t[5], t[6]) : void 0
                }()) ? (n = new Date(e), !isNaN(n.getTime()) && n) : !1
            }, e.exports = r
        }, {}],
        8: [function(t, e, n) {
            function i(t) {
                return new RegExp("\\b" + t + "\\b", "g")
            }

            function r(t, e) {
                return t.classList ? void t.classList.add(e) : void(i(e).test(t.className) || (t.className += " " + e))
            }

            function o(t, e) {
                return t.classList ? void t.classList.remove(e) : void(t.className = t.className.replace(i(e), " "))
            }

            function s(t, e, n) {
                return void 0 === n && t.classList && t.classList.toggle ? t.classList.toggle(e, n) : (n ? r(t, e) : o(t, e), n)
            }

            function a(t, e, n) {
                return t.classList && c(t, e) ? (o(t, e), void r(t, n)) : void(t.className = t.className.replace(i(e), n))
            }

            function c(t, e) {
                return t.classList ? t.classList.contains(e) : i(e).test(t.className)
            }
            e.exports = {
                add: r,
                remove: o,
                replace: a,
                toggle: s,
                present: c
            }
        }, {}],
        9: [function(t, e, n) {
            function i(t) {
                var e = t.getAttribute("data-twitter-event-id");
                return e ? e : (t.setAttribute("data-twitter-event-id", ++m), m)
            }

            function r(t, e, n) {
                var i = 0,
                    r = t && t.length || 0;
                for (i = 0; r > i; i++) t[i].call(e, n, e)
            }

            function o(t, e, n) {
                for (var i = n || t.target || t.srcElement, s = i.className.split(" "), a = 0, c = s.length; c > a; a++) r(e["." + s[a]], i, t);
                r(e[i.tagName], i, t), t.cease || i !== this && o.call(this, t, e, i.parentElement || i.parentNode)
            }

            function s(t, e, n, i) {
                function r(i) {
                    o.call(t, i, n[e])
                }
                a(t, r, e, i), t.addEventListener(e, r, !1)
            }

            function a(t, e, n, i) {
                t.id && (g[t.id] = g[t.id] || [], g[t.id].push({
                    el: t,
                    listener: e,
                    type: n,
                    rootId: i
                }))
            }

            function c(t) {
                var e = g[t];
                e && (e.forEach(function(t) {
                    t.el.removeEventListener(t.type, t.listener, !1), delete p[t.rootId]
                }), delete g[t])
            }

            function u(t, e, n, r) {
                var o = i(t);
                p[o] = p[o] || {}, p[o][e] || (p[o][e] = {}, s(t, e, p[o], o)), p[o][e][n] = p[o][e][n] || [], p[o][e][n].push(r)
            }

            function l(t, e, n) {
                var r = i(e),
                    s = p[r] && p[r];
                o.call(e, {
                    target: n
                }, s[t])
            }

            function d(t) {
                return f(t), h(t), !1
            }

            function h(t) {
                t && t.preventDefault ? t.preventDefault() : t.returnValue = !1
            }

            function f(t) {
                t && (t.cease = !0) && t.stopPropagation ? t.stopPropagation() : t.cancelBubble = !0
            }
            var p = {},
                m = -1,
                g = {};
            e.exports = {
                stop: d,
                stopPropagation: f,
                preventDefault: h,
                delegate: u,
                simulate: l,
                removeDelegatesForWidget: c
            }
        }, {}],
        10: [function(t, e, n) {
            function i(t) {
                var e = t.charAt(0);
                return "." === e ? function(e) {
                    var n = e.className ? e.className.split(/\s+/) : [];
                    return o.contains(n, t.slice(1))
                } : "#" === e ? function(e) {
                    return e.id === t.slice(1)
                } : function(e) {
                    return e.tagName === t.toUpperCase()
                }
            }

            function r(t, e, n) {
                var s;
                return e ? (n = n || e && e.ownerDocument, s = o.isType("function", t) ? t : i(t), e === n ? s(e) ? e : void 0 : s(e) ? e : r(s, e.parentNode, n)) : void 0
            }
            var o = t(79);
            e.exports = {
                closest: r
            }
        }, {
            79: 79
        }],
        11: [function(t, e, n) {
            function i(t) {
                return t = t || o, t.getSelection && t.getSelection()
            }

            function r(t) {
                var e = i(t);
                return e ? e.toString() : ""
            }
            var o = t(16);
            e.exports = {
                getSelection: i,
                getSelectedText: r
            }
        }, {
            16: 16
        }],
        12: [function(t, e, n) {
            function i(t) {
                return t && 1 === t.nodeType ? t.offsetWidth || i(t.parentNode) : 0
            }
            e.exports = {
                effectiveWidth: i
            }
        }, {}],
        13: [function(t, e, n) {
            e.exports = document
        }, {}],
        14: [function(t, e, n) {
            e.exports = location
        }, {}],
        15: [function(t, e, n) {
            e.exports = navigator
        }, {}],
        16: [function(t, e, n) {
            e.exports = window
        }, {}],
        17: [function(t, e, n) {
            function i(t, e, n) {
                e.ready = t.then.bind(t), n && Array.isArray(e[n]) && (e[n].forEach(t.then.bind(t)), delete e[n])
            }
            e.exports = {
                exposeReadyPromise: i
            }
        }, {}],
        18: [function(t, e, n) {
            function i(t) {
                return a.isType("string", t) ? t.split(".") : a.isType("array", t) ? t : []
            }

            function r(t, e) {
                var n = i(e),
                    r = n.slice(0, -1);
                return r.reduce(function(t, e, n) {
                    if (t[e] = t[e] || {}, !a.isObject(t[e])) throw new Error(r.slice(0, n + 1).join(".") + " is already defined with a value.");
                    return t[e]
                }, t)
            }

            function o(t, e) {
                e = e || s, e[t] = e[t] || {}, Object.defineProperty(this, "base", {
                    value: e[t]
                }), Object.defineProperty(this, "name", {
                    value: t
                })
            }
            var s = t(16),
                a = t(79);
            a.aug(o.prototype, {
                get: function(t) {
                    var e = i(t);
                    return e.reduce(function(t, e) {
                        return a.isObject(t) ? t[e] : void 0
                    }, this.base)
                },
                set: function(t, e, n) {
                    var o = i(t),
                        s = r(this.base, t),
                        a = o.slice(-1);
                    return n && a in s ? s[a] : s[a] = e
                },
                init: function(t, e) {
                    return this.set(t, e, !0)
                },
                unset: function(t) {
                    var e = i(t),
                        n = this.get(e.slice(0, -1));
                    n && delete n[e.slice(-1)]
                },
                aug: function(t) {
                    var e = this.get(t),
                        n = a.toRealArray(arguments).slice(1);
                    if (e = "undefined" != typeof e ? e : {}, n.unshift(e), !n.every(a.isObject)) throw new Error("Cannot augment non-object.");
                    return this.set(t, a.aug.apply(null, n))
                },
                call: function(t) {
                    var e = this.get(t),
                        n = a.toRealArray(arguments).slice(1);
                    if (!a.isType("function", e)) throw new Error("Function " + t + "does not exist.");
                    return e.apply(null, n)
                },
                fullPath: function(t) {
                    var e = i(t);
                    return e.unshift(this.name), e.join(".")
                }
            }), e.exports = o
        }, {
            16: 16,
            79: 79
        }],
        19: [function(t, e, n) {
            function i(t) {
                var e, n, i, r = 0;
                for (o = {}, t = t || s, e = t.getElementsByTagName("meta"); n = e[r]; r++) /^twitter:/.test(n.name) && (i = n.name.replace(/^twitter:/, ""), o[i] = n.content)
            }

            function r(t) {
                return o[t]
            }
            var o, s = t(13);
            i(), e.exports = {
                init: i,
                val: r
            }
        }, {
            13: 13
        }],
        20: [function(t, e, n) {
            var i = t(18);
            e.exports = new i("__twttr")
        }, {
            18: 18
        }],
        21: [function(t, e, n) {
            var i = t(18);
            e.exports = new i("twttr")
        }, {
            18: 18
        }],
        22: [function(t, e, n) {
            e.exports = ["hi", "zh-cn", "fr", "zh-tw", "msa", "fil", "fi", "sv", "pl", "ja", "ko", "de", "it", "pt", "es", "ru", "id", "tr", "da", "no", "nl", "hu", "fa", "ar", "ur", "he", "th", "cs", "uk", "vi", "ro", "bn"]
        }, {}],
        23: [function(t, e, n) {
            function i(t) {
                return t = String(t).toLowerCase(), r.contains(o, t)
            }
            var r = t(79),
                o = ["ar", "fa", "he", "ur"];
            e.exports = {
                isRtlLang: i
            }
        }, {
            79: 79
        }],
        24: [function(t, e, n) {
            function i(t) {
                var e = ~s.host.indexOf("poptip.com") ? "https://poptip.com" : s.href,
                    n = "original_referer=" + e;
                return [t, n].join(-1 == t.indexOf("?") ? "?" : "&")
            }

            function r(t) {
                var e, n;
                t.altKey || t.metaKey || t.shiftKey || (e = c.closest(function(t) {
                    return "A" === t.tagName || "AREA" === t.tagName
                }, t.target), e && l.isIntentURL(e.href) && (n = i(e.href), n = n.replace(/^http[:]/, "https:"), n = n.replace(/^\/\//, "https://"), u.open(n, e), a.preventDefault(t)))
            }

            function o(t) {
                t.addEventListener("click", r, !1)
            }
            var s = t(14),
                a = t(9),
                c = t(10),
                u = t(52),
                l = t(75);
            e.exports = {
                attachTo: o
            }
        }, {
            10: 10,
            14: 14,
            52: 52,
            75: 75,
            9: 9
        }],
        25: [function(t, e, n) {
            function i(t) {
                var e = [];
                return h.forIn(t, function(t, n) {
                    e.push(t + "=" + n)
                }), e.join(",")
            }

            function r() {
                return f + d.generate()
            }

            function o(t, e) {
                function n(t) {
                    return Math.round(t / 2)
                }
                return t > e ? {
                    coordinate: 0,
                    size: e
                } : {
                    coordinate: n(e) - n(t),
                    size: t
                }
            }

            function s(t, e, n) {
                var r, s;
                e = a.parse(e), n = n || {}, r = o(e.width, n.width || p), e.left = r.coordinate, e.width = r.size, s = o(e.height, n.height || m), e.top = s.coordinate, e.height = s.size, this.win = t, this.features = i(e)
            }
            var a, c = t(16),
                u = t(68),
                l = t(75),
                d = t(77),
                h = t(79),
                f = "intent_",
                p = c.screen.width,
                m = c.screen.height;
            a = (new u).defaults({
                width: 550,
                height: 520,
                personalbar: "0",
                toolbar: "0",
                location: "1",
                scrollbars: "1",
                resizable: "1"
            }), s.prototype.open = function(t) {
                return l.isTwitterURL(t) ? (this.name = r(), this.popup = this.win.open(t, this.name, this.features), this) : void 0
            }, s.open = function(t, e) {
                var n = new s(c, e);
                return n.open(t)
            }, e.exports = s
        }, {
            16: 16,
            68: 68,
            75: 75,
            77: 77,
            79: 79
        }],
        26: [function(t, e, n) {
            function i(t) {
                u[t] = +new Date
            }

            function r(t) {
                return u[t] ? +new Date - u[t] : null
            }

            function o(t, e, n, i, o) {
                var a = r(e);
                a && s(t, n, i, a, o)
            }

            function s(t, e, n, i, r) {
                var o, s = void 0 === r ? l : r;
                100 * Math.random() > s || (n = c.aug(n || {}, {
                    duration_ms: i
                }), o = {
                    page: e,
                    component: "performance",
                    action: t
                }, a.clientEvent(o, n, !0))
            }
            var a = t(38),
                c = t(79),
                u = {},
                l = 1;
            e.exports = {
                start: i,
                end: r,
                track: s,
                endAndTrack: o
            }
        }, {
            38: 38,
            79: 79
        }],
        27: [function(t, e, n) {
            function i(t) {
                if (!t) throw new Error("JsonRpcClient requires a dispatcher");
                this.idIterator = 0, this.dispatcher = t, this.idPrefix = String(+new Date) + a++
            }

            function r(t) {
                var e = {
                    jsonrpc: s,
                    method: t
                };
                return arguments.length > 1 && (e.params = [].slice.call(arguments, 1)), e
            }
            var o = t(71),
                s = "2.0",
                a = 0;
            i.prototype._generateId = function() {
                return this.idPrefix + this.idIterator++
            }, i.prototype.notify = function() {
                this.dispatcher.send(r.apply(null, arguments))
            }, i.prototype.request = function() {
                var t = r.apply(null, arguments);
                return t.id = this._generateId(), this.dispatcher.send(t).then(function(t) {
                    return "result" in t ? t.result : o.reject(t.error)
                })
            }, e.exports = i
        }, {
            71: 71
        }],
        28: [function(t, e, n) {
            e.exports = {
                PARSE_ERROR: {
                    code: -32700,
                    message: "Parse error"
                },
                INVALID_REQUEST: {
                    code: -32600,
                    message: "Invalid Request"
                },
                INVALID_PARAMS: {
                    code: -32602,
                    message: "Invalid params"
                },
                METHOD_NOT_FOUND: {
                    code: -32601,
                    message: "Method not found"
                },
                INTERNAL_ERROR: {
                    code: -32603,
                    message: "Internal error"
                }
            }
        }, {}],
        29: [function(t, e, n) {
            function i(t) {
                this.registry = t || {}
            }

            function r(t) {
                return h.isType("string", t) ? JSON.parse(t) : t
            }

            function o(t) {
                var e, n, i;
                return h.isObject(t) ? (e = t.jsonrpc === m, n = h.isType("string", t.method), i = !("id" in t) || s(t.id), e && n && i) : !1
            }

            function s(t) {
                var e, n, i;
                return e = h.isType("string", t), n = h.isType("number", t), i = null === t, e || n || i
            }

            function a(t) {
                return h.isObject(t) && !h.isType("function", t)
            }

            function c(t, e) {
                return {
                    jsonrpc: m,
                    id: t,
                    result: e
                }
            }

            function u(t, e) {
                return {
                    jsonrpc: m,
                    id: s(t) ? t : null,
                    error: e
                }
            }

            function l(t) {
                return f.all(t).then(function(t) {
                    return t = t.filter(function(t) {
                        return void 0 !== t
                    }), t.length ? t : void 0
                })
            }
            var d = t(28),
                h = t(79),
                f = t(71),
                p = t(72),
                m = "2.0";
            i.prototype._invoke = function(t, e) {
                var n, i, r;
                n = this.registry[t.method], i = t.params || [], i = h.isType("array", i) ? i : [i];
                try {
                    r = n.apply(e.source || null, i)
                } catch (o) {
                    r = f.reject(o.message)
                }
                return p.isPromise(r) ? r : f.resolve(r)
            }, i.prototype._processRequest = function(t, e) {
                function n(e) {
                    return c(t.id, e)
                }

                function i() {
                    return u(t.id, d.INTERNAL_ERROR)
                }
                var r;
                return o(t) ? (r = "params" in t && !a(t.params) ? f.resolve(u(t.id, d.INVALID_PARAMS)) : this.registry[t.method] ? this._invoke(t, {
                    source: e
                }).then(n, i) : f.resolve(u(t.id, d.METHOD_NOT_FOUND)), null != t.id ? r : f.resolve()) : f.resolve(u(t.id, d.INVALID_REQUEST))
            }, i.prototype.attachReceiver = function(t) {
                return t.attachTo(this), this
            }, i.prototype.bind = function(t, e) {
                return this.registry[t] = e, this
            }, i.prototype.receive = function(t, e) {
                var n, i, o, s = this;
                try {
                    t = r(t)
                } catch (a) {
                    return f.resolve(u(null, d.PARSE_ERROR))
                }
                return e = e || null, n = h.isType("array", t), i = n ? t : [t], o = i.map(function(t) {
                    return s._processRequest(t, e)
                }), n ? l(o) : o[0]
            }, e.exports = i
        }, {
            28: 28,
            71: 71,
            72: 72,
            79: 79
        }],
        30: [function(t, e, n) {
            function i(t, e, n) {
                var i;
                t && t.postMessage && (m ? i = (n || "") + JSON.stringify(e) : n ? (i = {}, i[n] = e) : i = e, t.postMessage(i, "*"))
            }

            function r(t) {
                return f.isType("string", t) ? t : "JSONRPC"
            }

            function o(t, e) {
                return e ? f.isType("string", t) && 0 === t.indexOf(e) ? t.substring(e.length) : t[e] ? t[e] : void 0 : t
            }

            function s(t, e) {
                var n = t.document;
                this.filter = r(e), this.server = null, this.isTwitterFrame = p.isTwitterURL(n.location.href), t.addEventListener("message", this._onMessage.bind(this), !1)
            }

            function a(t, e) {
                this.pending = {}, this.target = t, this.isTwitterHost = p.isTwitterURL(u.href), this.filter = r(e), l.addEventListener("message", this._onMessage.bind(this), !1)
            }

            function c(t) {
                return arguments.length > 0 && (m = !!t), m
            }
            var u = t(14),
                l = t(16),
                d = t(70),
                h = t(62),
                f = t(79),
                p = t(75),
                m = h.ie9();
            f.aug(s.prototype, {
                _onMessage: function(t) {
                    var e, n = this;
                    this.server && (!this.isTwitterFrame || p.isTwitterURL(t.origin)) && (e = o(t.data, this.filter), e && this.server.receive(e, t.source).then(function(e) {
                        e && i(t.source, e, n.filter)
                    }))
                },
                attachTo: function(t) {
                    this.server = t
                },
                detach: function() {
                    this.server = null
                }
            }), f.aug(a.prototype, {
                _processResponse: function(t) {
                    var e = this.pending[t.id];
                    e && (e.resolve(t), delete this.pending[t.id])
                },
                _onMessage: function(t) {
                    var e;
                    if ((!this.isTwitterHost || p.isTwitterURL(t.origin)) && (e = o(t.data, this.filter))) {
                        if (f.isType("string", e)) try {
                            e = JSON.parse(e)
                        } catch (n) {
                            return
                        }
                        e = f.isType("array", e) ? e : [e], e.forEach(this._processResponse.bind(this))
                    }
                },
                send: function(t) {
                    var e = new d;
                    return t.id ? this.pending[t.id] = e : e.resolve(), i(this.target, t, this.filter), e.promise
                }
            }), e.exports = {
                Receiver: s,
                Dispatcher: a,
                _stringifyPayload: c
            }
        }, {
            14: 14,
            16: 16,
            62: 62,
            70: 70,
            75: 75,
            79: 79
        }],
        31: [function(t, e, n) {
            function i(t, e, n, i) {
                var s, u = this;
                this.readyDeferred = new o, this.attrs = t || {}, this.styles = e || {}, this.appender = n || function(t) {
                    r.body.appendChild(t)
                }, this.layout = i || function(t) {
                    return c.resolve(t())
                }, this.frame = s = a(this.attrs, this.styles), s.onreadystatechange = s.onload = this.getCallback(this.onLoad), this.layout(function() {
                    u.appender(s)
                })
            }
            var r = t(13),
                o = t(70),
                s = t(62),
                a = t(65),
                c = t(71),
                u = t(20),
                l = 0;
            i.prototype.getCallback = function(t) {
                var e = this,
                    n = !1;
                return function() {
                    n || (n = !0, t.call(e))
                }
            }, i.prototype.registerCallback = function(t) {
                var e = "cb" + l++;
                return u.set(["sandbox", e], t), e
            }, i.prototype.onLoad = function() {
                try {
                    this.document = this.frame.contentWindow.document
                } catch (t) {
                    return void this.setDocDomain()
                }
                this.writeStandardsDoc(), this.readyDeferred.resolve(this)
            }, i.prototype.ready = function() {
                return this.readyDeferred.promise
            }, i.prototype.setDocDomain = function() {
                var t = this,
                    e = a(this.attrs, this.styles),
                    n = this.registerCallback(this.getCallback(this.onLoad));
                e.src = ["javascript:", 'document.write("");', "try { window.parent.document; }", "catch (e) {", 'document.domain="' + r.domain + '";', "}", "window.parent." + u.fullPath(["sandbox", n]) + "();"].join(""), this.layout(function() {
                    t.frame.parentNode.removeChild(t.frame), t.frame = null, t.appender ? t.appender(e) : r.body.appendChild(e), t.frame = e
                })
            }, i.prototype.writeStandardsDoc = function() {
                if (s.anyIE() && !s.cspEnabled()) {
                    var t = ["<!DOCTYPE html>", "<html>", "<head>", "<scr", "ipt>", "try { window.parent.document; }", 'catch (e) {document.domain="' + r.domain + '";}', "</scr", "ipt>", "</head>", "<body></body>", "</html>"].join("");
                    this.document.write(t), this.document.close()
                }
            }, e.exports = i
        }, {
            13: 13,
            20: 20,
            62: 62,
            65: 65,
            70: 70,
            71: 71
        }],
        32: [function(t, e, n) {
            function i() {
                var t, e;
                y = {}, s || (t = a.body.offsetHeight, e = a.body.offsetWidth, (t != b || e != w) && (v.forEach(function(t) {
                    t.dispatchFrameResize(w, b)
                }), b = t, w = e))
            }

            function r(t) {
                var e;
                return t.id ? t.id : (e = t.getAttribute("data-twttr-id")) ? e : (e = "twttr-sandbox-" + g++, t.setAttribute("data-twttr-id", e), e)
            }

            function o(t, e) {
                var n = this;
                l.apply(this, [t, e]), this._resizeHandlers = [], v = v.filter(function(t) {
                    var e = t._frame.parentElement;
                    return e || f.async(function() {
                        p.removeDelegatesForWidget(t._frame.id)
                    }), e
                }), v.push(this), this._win.addEventListener("resize", function() {
                    n.dispatchFrameResize()
                }, !1)
            }
            var s, a = t(13),
                c = t(16),
                u = t(31),
                l = t(33),
                d = t(62),
                h = t(71),
                f = t(79),
                p = t(9),
                m = t(8),
                g = 0,
                v = [],
                y = {},
                w = 0,
                b = 0;
            c.addEventListener("resize", i, !1), o.prototype = new l, f.aug(o.prototype, {
                _addStyleSheet: function(t, e, n) {
                    function i() {
                        var t = o._head.children[0];
                        return t ? o._head.insertBefore(s, t) : o._head.appendChild(s)
                    }

                    function r() {
                        return o._head.appendChild(s)
                    }
                    var o = this,
                        s = this._doc.createElement("link");
                    return s.type = "text/css", s.rel = "stylesheet", s.href = t, n && (s.onload = n), this.layout(e ? i : r)
                },
                dispatchFrameResize: function() {
                    var t = this._frame.parentNode,
                        e = r(t),
                        n = y[e];
                    s = !0, this._resizeHandlers.length && (n || (n = y[e] = {
                        w: this._frame.offsetWidth,
                        h: this._frame.offsetHeight
                    }), (this._frameWidth != n.w || this._frameHeight != n.h) && (this._frameWidth = n.w, this._frameHeight = n.h, this._resizeHandlers.forEach(function(t) {
                        t(n.w, n.h)
                    }), c.setTimeout(function() {
                        y = {}
                    }, 50)))
                },
                addClass: function(t) {
                    var e = this._doc.documentElement;
                    return this.layout(function() {
                        m.add(e, t)
                    })
                },
                prependStyleSheet: function(t, e) {
                    return this._addStyleSheet(t, !0, e)
                },
                appendStyleSheet: function(t, e) {
                    return this._addStyleSheet(t, !1, e)
                },
                removeStyleSheet: function(t) {
                    var e, n = this;
                    return e = 'link[rel="stylesheet"][href="' + t + '"]', this.layout(function() {
                        var t = n._doc.querySelector(e);
                        return t && n._head.removeChild(t)
                    })
                },
                appendCss: function(t) {
                    var e, n = this;
                    return d.cspEnabled() ? h.reject("CSP enabled; cannot embed inline styles.") : (e = this._doc.createElement("style"), e.type = "text/css", e.styleSheet ? e.styleSheet.cssText = t : e.appendChild(this._doc.createTextNode(t)), this.layout(function() {
                        return n._head.appendChild(e)
                    }))
                },
                style: function(t, e) {
                    var n = this;
                    return this.layout(function() {
                        e && (n._frame.style.cssText = ""), f.forIn(t, function(t, e) {
                            n._frame.style[t] = e
                        })
                    })
                },
                onresize: function(t) {
                    this._resizeHandlers.push(t)
                },
                width: function(t) {
                    return void 0 !== t && (this._frame.style.width = t + "px"), d.ios() ? Math.min(this._frame.parentNode.offsetWidth, this._frame.offsetWidth) : this._frame.offsetWidth
                },
                height: function(t) {
                    return void 0 !== t && (this._frame.height = t), this._frame.offsetHeight
                },
                contentHeight: function() {
                    return this._doc.body.firstChild.offsetHeight
                },
                hasContent: function() {
                    return !!this._doc.body.firstChild
                },
                resizeToContent: function() {
                    var t = this;
                    return this.layout(function() {
                        return t.height(t.contentHeight())
                    })
                }
            }), o.createSandbox = function(t, e, n, i) {
                var r = new u(t, e, n, i);
                return r.ready().then(function(t) {
                    return new o(t.frame, t.layout)
                })
            }, e.exports = o
        }, {
            13: 13,
            16: 16,
            31: 31,
            33: 33,
            62: 62,
            71: 71,
            79: 79,
            8: 8,
            9: 9
        }],
        33: [function(t, e, n) {
            function i(t, e) {
                t && (this._frame = t, this._win = t.contentWindow, this._doc = this._win.document, this._body = this._doc.body, this._head = this._body.parentNode.children[0], this.layout = e, this.root = this._doc.documentElement, this.root.className = "SandboxRoot")
            }
            var r = t(31),
                o = t(79);
            o.aug(i.prototype, {
                createElement: function(t) {
                    return this._doc.createElement(t)
                },
                createDocumentFragment: function() {
                    return this._doc.createDocumentFragment()
                },
                appendChild: function(t) {
                    var e = this;
                    return this.layout(function() {
                        return e._body.appendChild(t)
                    })
                },
                setBaseTarget: function(t) {
                    var e = this,
                        n = this._doc.createElement("base");
                    return n.target = t, this.layout(function() {
                        return e._head.appendChild(n)
                    })
                },
                setTitle: function(t) {
                    t && (this._frame.title = t)
                },
                element: function() {
                    return this._frame
                },
                document: function() {
                    return this._doc
                }
            }), i.createSandbox = function(t, e, n, o) {
                var s = new r(t, e, n, o);
                return s.ready().then(function(t) {
                    return new i(t.frame, t.layout)
                })
            }, e.exports = i
        }, {
            31: 31,
            79: 79
        }],
        34: [function(t, e, n) {
            function i() {
                return l.formatGenericEventData("syndicated_impression", {});
            }

            function r() {
                c("tweet")
            }

            function o() {
                c("timeline")
            }

            function s() {
                c("video")
            }

            function a() {
                c("partnertweet")
            }

            function c(t) {
                d.isHostPageSensitive() || h[t] || (h[t] = !0, u.scribe(l.formatClientEventNamespace({
                    page: t,
                    action: "impression"
                }), i(), l.AUDIENCE_ENDPOINT))
            }
            var u = t(38),
                l = t(39),
                d = t(74),
                h = {};
            e.exports = {
                scribeAudienceImpression: c,
                scribePartnerTweetAudienceImpression: a,
                scribeTweetAudienceImpression: r,
                scribeTimelineAudienceImpression: o,
                scribeVideoAudienceImpression: s
            }
        }, {
            38: 38,
            39: 39,
            74: 74
        }],
        35: [function(t, e, n) {
            function i(t) {
                return t ? (t = Array.isArray(t) ? t : [t], t.reduce(function(t, e) {
                    var n = e.getAttribute("data-tweet-id"),
                        i = e.getAttribute("data-rendered-tweet-id") || n;
                    return n === i ? t[i] = {
                        item_type: r.TWEET
                    } : n && (t[i] = {
                        item_type: r.RETWEET,
                        target_type: r.TWEET,
                        target_id: n
                    }), t
                }, {})) : {}
            }
            var r = t(37);
            e.exports = i
        }, {
            37: 37
        }],
        36: [function(t, e, n) {
            function i() {
                return x ? I.promise : (m.createSandbox({
                    id: "rufous-sandbox"
                }, {
                    display: "none"
                }).then(function(t) {
                    h = t, l = c(), d = u(), I.resolve([l, d])
                }), x = !0, I.promise)
            }

            function r(t, e) {
                var n, i, r;
                w.isObject(t) && w.isObject(e) && (r = y.flattenClientEventPayload(t, e), n = l.firstChild, n.value = +(+n.value || r.dnt || 0), i = h.createElement("input"), i.type = "hidden", i.name = "l", i.value = y.stringify(r), l.appendChild(i))
            }

            function o(t, e, n) {
                var i = !w.isObject(t),
                    o = e ? !w.isObject(e) : !1;
                i || o || I.promise.then(function() {
                    r(y.formatClientEventNamespace(t), y.formatClientEventData(e, n))
                })
            }

            function s() {
                return I.promise.then(function() {
                    if (l.children.length <= 2) return v.reject();
                    var t = v.all([h.appendChild(l), h.appendChild(d)]).then(function(t) {
                        var e = t[0],
                            n = t[1];
                        return n.addEventListener("load", function() {
                            a(e, n)(), b.get("events").trigger("logFlushed")
                        }), e.submit(), t
                    });
                    return l = c(), d = u(), t
                })
            }

            function a(t, e) {
                return function() {
                    var n = t.parentNode;
                    n && (n.removeChild(t), n.removeChild(e))
                }
            }

            function c() {
                var t = h.createElement("form"),
                    e = h.createElement("input"),
                    n = h.createElement("input");
                return T++, t.action = y.CLIENT_EVENT_ENDPOINT, t.method = "POST", t.target = E + T, t.id = A + T, e.type = "hidden", e.name = "dnt", e.value = p.enabled(), n.type = "hidden", n.name = "tfw_redirect", n.value = y.RUFOUS_REDIRECT, t.appendChild(e), t.appendChild(n), t
            }

            function u() {
                var t = E + T;
                return f({
                    id: t,
                    name: t,
                    width: 0,
                    height: 0,
                    border: 0
                }, {
                    display: "none"
                }, h.document())
            }
            var l, d, h, f = t(65),
                p = t(61),
                m = t(33),
                g = t(70),
                v = t(71),
                y = t(39),
                w = t(79),
                b = t(21),
                _ = Math.floor(1e3 * Math.random()) + "_",
                E = "rufous-frame-" + _ + "-",
                A = "rufous-form-" + _ + "-",
                T = 0,
                x = !1,
                I = new g;
            e.exports = {
                clientEvent: o,
                flush: s,
                init: i
            }
        }, {
            21: 21,
            33: 33,
            39: 39,
            61: 61,
            65: 65,
            70: 70,
            71: 71,
            79: 79
        }],
        37: [function(t, e, n) {
            e.exports = {
                TWEET: 0,
                RETWEET: 10,
                CUSTOM_TIMELINE: 17
            }
        }, {}],
        38: [function(t, e, n) {
            function i(t, e, n) {
                return r(t, e, n, 2)
            }

            function r(t, e, n, i) {
                var r = !p.isObject(t),
                    o = e ? !p.isObject(e) : !1;
                r || o || s(f.formatClientEventNamespace(t), f.formatClientEventData(e, n, i), f.CLIENT_EVENT_ENDPOINT)
            }

            function o(t, e, n, i) {
                var o = f.extractTermsFromDOM(t.target || t.srcElement);
                o.action = i || "click", r(o, e, n)
            }

            function s(t, e, n) {
                var i, r;
                n && p.isObject(t) && p.isObject(e) && (i = f.flattenClientEventPayload(t, e), r = {
                    l: f.stringify(i)
                }, i.dnt && (r.dnt = 1), l(h.url(n, r)))
            }

            function a(t, e, n, i) {
                var r, o = !p.isObject(t),
                    s = e ? !p.isObject(e) : !1;
                return o || s ? void 0 : (r = f.flattenClientEventPayload(f.formatClientEventNamespace(t), f.formatClientEventData(e, n, i)), c(r))
            }

            function c(t) {
                return g.push(t), g
            }

            function u() {
                var t, e, n = h.url(f.CLIENT_EVENT_ENDPOINT, {
                        dnt: 0,
                        l: ""
                    }),
                    i = encodeURIComponent(n).length;
                return g.length > 1 && a({
                    page: "widgets_js",
                    component: "scribe_pixel",
                    action: "batch_log"
                }, {}), t = g, g = [], e = t.reduce(function(e, n, r) {
                    var o, s, a = e.length,
                        c = a && e[a - 1],
                        u = r + 1 == t.length;
                    return u && n.event_namespace && "batch_log" == n.event_namespace.action && (n.message = ["entries:" + r, "requests:" + a].join("/")), o = f.stringify(n), s = encodeURIComponent(o).length + 3, i + s > m ? e : ((!c || c.urlLength + s > m) && (c = {
                        urlLength: i,
                        items: []
                    }, e.push(c)), c.urlLength += s, c.items.push(o), e)
                }, []), e.map(function(t) {
                    var e = {
                        l: t.items
                    };
                    return d.enabled() && (e.dnt = 1), l(h.url(f.CLIENT_EVENT_ENDPOINT, e))
                })
            }

            function l(t) {
                var e = new Image;
                return e.src = t
            }
            var d = t(61),
                h = t(73),
                f = t(39),
                p = t(79),
                m = 2083,
                g = [];
            e.exports = {
                _enqueueRawObject: c,
                scribe: s,
                clientEvent: r,
                clientEvent2: i,
                enqueueClientEvent: a,
                flushClientEvents: u,
                interaction: o
            }
        }, {
            39: 39,
            61: 61,
            73: 73,
            79: 79
        }],
        39: [function(t, e, n) {
            function i(t, e) {
                var n;
                return e = e || {}, t && t.nodeType === Node.ELEMENT_NODE ? ((n = t.getAttribute("data-scribe")) && n.split(" ").forEach(function(t) {
                    var n = t.trim().split(":"),
                        i = n[0],
                        r = n[1];
                    i && r && !e[i] && (e[i] = r)
                }), i(t.parentNode, e)) : e
            }

            function r(t) {
                return d.aug({
                    client: "tfw"
                }, t || {})
            }

            function o(t, e, n) {
                var i = t && t.widget_origin || u.referrer;
                return t = s("tfw_client_event", t, i), t.client_version = p, t.format_version = void 0 !== n ? n : 1, e || (t.widget_origin = i), t
            }

            function s(t, e, n) {
                return e = e || {}, d.aug({}, e, {
                    _category_: t,
                    triggered_on: e.triggered_on || +new Date,
                    dnt: l.enabled(n)
                })
            }

            function a(t, e) {
                return d.aug({}, e, {
                    event_namespace: t
                })
            }

            function c(t) {
                var e, n = Array.prototype.toJSON;
                return delete Array.prototype.toJSON, e = JSON.stringify(t), n && (Array.prototype.toJSON = n), e
            }
            var u = t(13),
                l = t(61),
                d = t(79),
                h = t(81),
                f = t(20),
                p = h.version,
                m = f.get("endpoints.rufous") || "https://syndication.twitter.com/i/jot",
                g = f.get("endpoints.rufousAudience") || "https://syndication.twitter.com/i/jot/syndication",
                v = f.get("endpoints.rufousRedirect") || "https://platform.twitter.com/jot.html";
            e.exports = {
                extractTermsFromDOM: i,
                flattenClientEventPayload: a,
                formatGenericEventData: s,
                formatClientEventData: o,
                formatClientEventNamespace: r,
                stringify: c,
                AUDIENCE_ENDPOINT: g,
                CLIENT_EVENT_ENDPOINT: m,
                RUFOUS_REDIRECT: v
            }
        }, {
            13: 13,
            20: 20,
            61: 61,
            79: 79,
            81: 81
        }],
        40: [function(t, e, n) {
            function i(t, e, n, i) {
                return t = t || [], n = n || {},
                    function() {
                        var r, o, c, l, d = Array.prototype.slice.apply(arguments, [0, t.length]),
                            h = Array.prototype.slice.apply(arguments, [t.length]);
                        return h.forEach(function(t) {
                            return t ? 1 === t.nodeType ? void(c = t) : s.isType("function", t) ? void(r = t) : void(s.isType("object", t) && (o = t)) : void 0
                        }), d.length != t.length || 0 === h.length ? (r && s.async(function() {
                            r(!1)
                        }), a.reject("Not enough parameters")) : c ? (o = s.aug(o || {}, n), o.targetEl = c, t.forEach(function(t) {
                            o[t] = d.shift()
                        }), l = new e(o), u.doLayout(), l.render(), i && i(), r && l.completed().then(r, function() {
                            r(!1)
                        }), l.completed()) : (r && s.async(function() {
                            r(!1)
                        }), a.reject("No target specified"))
                    }
            }

            function r(t) {
                var e;
                t.linkColor = t.linkColor || t.previewParams.link_color, t.theme = t.theme || t.previewParams.theme, t.height = t.height || t.previewParams.height, e = new p(t), this.render = e.render.bind(e), this.completed = e.completed.bind(e)
            }
            var o = t(16),
                s = t(79),
                a = t(71),
                c = t(75),
                u = t(49),
                l = t(55),
                d = t(51),
                h = t(50),
                f = t(56),
                p = t(54),
                m = i(["url"], l, {
                    type: "share"
                }),
                g = i(["hashtag"], l, {
                    type: "hashtag"
                }),
                v = i(["screenName"], l, {
                    type: "mention"
                }),
                y = i(["screenName"], d),
                w = i(["tweetId"], h, {}, h.fetchAndRender),
                b = i(["tweetId"], f, {}, f.fetchAndRender),
                _ = i(["widgetId"], p),
                E = i(["previewParams"], r),
                A = {
                    createShareButton: m,
                    createMentionButton: v,
                    createHashtagButton: g,
                    createFollowButton: y,
                    createTweet: w,
                    createVideo: b,
                    createTweetEmbed: w,
                    createTimeline: _
                };
            c.isTwitterURL(o.location.href) && (A.createTimelinePreview = E), e.exports = A
        }, {
            16: 16,
            49: 49,
            50: 50,
            51: 51,
            54: 54,
            55: 55,
            56: 56,
            71: 71,
            75: 75,
            79: 79
        }],
        41: [function(t, e, n) {
            function i(t, e) {
                var n = u.connect({
                    src: t,
                    iframe: {
                        name: e,
                        style: "position:absolute;top:-9999em;width:10px;height:10px"
                    }
                });
                return l(n).expose({
                    trigger: function(t, e, n) {
                        e = e || {};
                        var i = e.region;
                        delete e.region, f.get("events").trigger(t, {
                            target: d.find(n),
                            data: e,
                            region: i,
                            type: t
                        })
                    },
                    initXPHub: function() {
                        o(!0)
                    }
                }), n
            }

            function r(t) {
                return t ? h.secureHubId : h.contextualHubId
            }

            function o(t) {
                var e = c.base(t) + "/widgets/hub.37638732bcf6df3115558c59e29ab4be.html",
                    n = r(t);
                return a.getElementById(n) ? void 0 : i(e, n)
            }

            function s(t, e) {
                var n = u.connect({
                    window: {
                        width: 550,
                        height: 450
                    },
                    src: t
                });
                l(n).expose({
                    trigger: function(t, n) {
                        f.get("events").trigger(t, {
                            target: e,
                            region: "intent",
                            type: t,
                            data: n
                        })
                    }
                })
            }
            var a = t(13),
                c = t(43),
                u = t(90),
                l = t(89),
                d = t(49),
                h = t(80),
                f = t(21);
            e.exports = {
                init: o,
                openIntent: s
            }
        }, {
            13: 13,
            21: 21,
            43: 43,
            49: 49,
            80: 80,
            89: 89,
            90: 90
        }],
        42: [function(t, e, n) {
            function i() {
                return o !== o.top ? r.request(d).then(function(t) {
                    s.rootDocumentLocation(t.url), t.dnt && a.setOn()
                }) : void 0
            }
            var r, o = t(16),
                s = t(59),
                a = t(61),
                c = t(29),
                u = t(27),
                l = t(30),
                d = "twttr.private.requestArticleUrl",
                h = "twttr.article";
            o === o.top ? (new c).attachReceiver(new l.Receiver(o, h)).bind(d, function() {
                return {
                    url: s.rootDocumentLocation(),
                    dnt: a.enabled()
                }
            }) : r = new u(new l.Dispatcher(o.top, h)), e.exports = {
                requestArticleUrl: i
            }
        }, {
            16: 16,
            27: 27,
            29: 29,
            30: 30,
            59: 59,
            61: 61
        }],
        43: [function(t, e, n) {
            function i(t, e) {
                var n, i = u[t];
                return "embed/timeline.css" === t && c.contains(o.href, "localhost.twitter.com") ? "/node_modules/syndication-templates/lib/css/index.css" : (n = a.retina() ? "2x" : "default", e && (n += ".rtl"), r() + "/" + i[n])
            }

            function r(t) {
                var e = s.get("host");
                return l(t) + "://" + e
            }
            var o = t(14),
                s = t(20),
                a = t(62),
                c = t(79),
                u = {
                    "embed/timeline.css": {
                        "default": "embed/timeline.b67ffafa652943d0ebac45e422e410f7.default.css",
                        "2x": "embed/timeline.b67ffafa652943d0ebac45e422e410f7.2x.css",
                        gif: "embed/timeline.b67ffafa652943d0ebac45e422e410f7.gif.css",
                        "default.rtl": "embed/timeline.b67ffafa652943d0ebac45e422e410f7.default.rtl.css",
                        "2x.rtl": "embed/timeline.b67ffafa652943d0ebac45e422e410f7.2x.rtl.css",
                        "gif.rtl": "embed/timeline.b67ffafa652943d0ebac45e422e410f7.gif.rtl.css"
                    }
                },
                l = function() {
                    return /^http\:$/.test(o.protocol) ? function(t) {
                        return t ? "https" : "http"
                    } : function() {
                        return "https"
                    }
                }();
            e.exports = {
                builtUrl: i,
                base: r
            }
        }, {
            14: 14,
            20: 20,
            62: 62,
            79: 79
        }],
        44: [function(t, e, n) {
            var i = t(14),
                r = t(20),
                o = t(79),
                s = t(5),
                a = t(4),
                c = t(6),
                u = {},
                l = o.aug({
                    tweets: "https://syndication.twitter.com/tweets.json",
                    timeline: "https://cdn.syndication.twimg.com/widgets/timelines/",
                    timelinePoll: "https://syndication.twitter.com/widgets/timelines/paged/",
                    timelinePreview: "https://syndication.twitter.com/widgets/timelines/preview/",
                    videos: "https://syndication.twitter.com/widgets/video/"
                }, r.get("endpoints") || {});
            u.tweets = function(t) {
                var e = {
                    ids: t.ids.join(","),
                    lang: t.lang,
                    new_html: !0
                };
                return s.fetch(l.tweets, e, a)
            }, u.videos = function(t) {
                return s.fetch(l.videos, {
                    ids: t.ids.join(","),
                    lang: t.lang
                }, a)
            }, u.timeline = function(t) {
                var e = "tl_" + t.id + "_" + t.instanceId,
                    n = 9e5,
                    r = Math.floor(+new Date / n),
                    a = {
                        lang: t.lang,
                        t: r,
                        domain: i.host,
                        dnt: t.dnt,
                        override_type: t.overrideType,
                        override_id: t.overrideId,
                        override_name: t.overrideName,
                        override_owner_id: t.overrideOwnerId,
                        override_owner_name: t.overrideOwnerName,
                        with_replies: t.withReplies
                    };
                return o.compact(a), s.fetch(l.timeline + t.id, a, c, e)
            }, u.timelinePoll = function(t) {
                var e = t.sinceId || t.maxId || t.maxPosition || t.minPosition,
                    n = "tlPoll_" + t.id + "_" + t.instanceId + "_" + e,
                    r = {
                        lang: t.lang,
                        since_id: t.sinceId,
                        max_id: t.maxId,
                        min_position: t.minPosition,
                        max_position: t.maxPosition,
                        domain: i.host,
                        dnt: t.dnt,
                        override_type: t.overrideType,
                        override_id: t.overrideId,
                        override_name: t.overrideName,
                        override_owner_id: t.overrideOwnerId,
                        override_owner_name: t.overrideOwnerName,
                        with_replies: t.withReplies
                    };
                return o.compact(r), s.fetch(l.timelinePoll + t.id, r, c, n)
            }, u.timelinePreview = function(t) {
                return s.fetch(l.timelinePreview, t.params, c)
            }, e.exports = u
        }, {
            14: 14,
            20: 20,
            4: 4,
            5: 5,
            6: 6,
            79: 79
        }],
        45: [function(t, e, n) {
            function i() {
                var t = 36e5,
                    e = s.combined(o)._;
                return void 0 !== r ? r : (r = !1, e && /^\d+$/.test(e) && (r = +new Date - parseInt(e) < t), r)
            }
            var r, o = t(14),
                s = t(69);
            e.exports = {
                isDynamicWidget: i
            }
        }, {
            14: 14,
            69: 69
        }],
        46: [function(t, e, n) {
            function i(t) {
                var e = t.split(" ");
                this.url = decodeURIComponent(e[0].trim()), this.width = +e[1].replace(/w$/, "").trim()
            }

            function r(t, e, n) {
                var r, o, s, a;
                if (t = p.devicePixelRatio ? t * p.devicePixelRatio : t, o = e.split(",").map(function(t) {
                        return new i(t.trim())
                    }), n)
                    for (a = 0; a < o.length; a++) o[a].url === n && (r = o[a]);
                return s = o.reduce(function(e, n) {
                    return n.width < e.width && n.width >= t ? n : e
                }, o[0]), r && r.width > s.width ? r : s
            }

            function o(t, e) {
                var n, i = t.getAttribute("data-srcset"),
                    o = t.src;
                i && (n = r(e, i, o), t.src = n.url)
            }

            function s(t, e) {
                e = void 0 !== e ? !!e : g.retina(), m.toRealArray(t.getElementsByTagName("IMG")).forEach(function(t) {
                    var n = t.getAttribute("data-src-1x") || t.getAttribute("src"),
                        i = t.getAttribute("data-src-2x");
                    e && i ? t.src = i : n && (t.src = n)
                })
            }

            function a(t, e, n) {
                t && (m.toRealArray(t.querySelectorAll(".NaturalImage-image")).forEach(function(t) {
                    n(function() {
                        o(t, e)
                    })
                }), m.toRealArray(t.querySelectorAll(".CroppedImage-image")).forEach(function(t) {
                    n(function() {
                        o(t, e / 2)
                    })
                }), m.toRealArray(t.querySelectorAll("img.autosized-media")).forEach(function(t) {
                    n(function() {
                        o(t, e), t.removeAttribute("width"), t.removeAttribute("height")
                    })
                }))
            }

            function c(t, e, n, i) {
                t && ((g.ios() || g.android()) && m.toRealArray(t.querySelectorAll(".FilledIframe")).forEach(function(t) {
                    i(function() {
                        d(t, {
                            width: t.offsetWidth,
                            height: t.offsetHeight
                        })
                    })
                }), m.toRealArray(t.querySelectorAll("iframe.autosized-media")).forEach(function(t) {
                    var r = l(t.getAttribute("data-width"), t.getAttribute("data-height"), y.effectiveWidth(t.parentElement) || e, n);
                    i(function() {
                        t.width = r.width, t.height = r.height, d(t, r)
                    })
                }))
            }

            function u(t, e, n, i) {
                a(t, e, i), c(t, e, n, i)
            }

            function l(t, e, n, i, r, o) {
                return n = n || t, i = i || e, r = r || 0, o = o || 0, t > n && (e *= n / t, t = n), e > i && (t *= i / e, e = i), r > t && (e *= r / t, t = r), o > e && (t *= o / e, e = o), {
                    width: Math.floor(t),
                    height: Math.floor(e)
                }
            }

            function d(t, e) {
                function n() {
                    var t = {
                        name: "tfw:resize",
                        dimensions: e
                    };
                    r.postMessage(t, "*")
                }
                var i, r, o, s, a;
                t && (r = t.contentWindow, i = t.ownerDocument && t.ownerDocument.defaultView, o = g.ios() || g.android(), s = v.isTwitterURL(t.src), a = r && g.canPostMessage(r), o && s && a && (n(), i && i.addEventListener("message", function(t) {
                    "tfw:requestsize" === t.data && n()
                }, !1)))
            }

            function h(t, e, n, i) {
                m.toRealArray(t.querySelectorAll(e)).forEach(function(t) {
                    var e = t.getAttribute("style") || t.getAttribute("data-style"),
                        r = i.test(e) && RegExp.$1;
                    r && (t.setAttribute("data-csp-fix", !0), t.style[n] = r)
                })
            }

            function f(t) {
                g.cspEnabled() && (h(t, ".MediaCard-widthConstraint", "maxWidth", w), h(t, ".MediaCard-mediaContainer", "paddingBottom", E), h(t, ".CroppedImage-image", "top", b), h(t, ".CroppedImage-image", "left", _))
            }
            var p = t(16),
                m = t(79),
                g = t(62),
                v = t(75),
                y = t(12),
                w = /max-width:\s*([\d\.]+px)/,
                b = /top:\s*(\-?[\d\.]+%)/,
                _ = /left:\s*(\-?[\d\.]+%)/,
                E = /padding-bottom:\s*([\d\.]+%)/;
            e.exports = {
                scaleDimensions: l,
                retinize: s,
                setSrcForImgs: a,
                sizeIframes: c,
                constrainMedia: u,
                fixMediaCardLayout: f,
                __setSrcFromSet: o
            }
        }, {
            12: 12,
            16: 16,
            62: 62,
            75: 75,
            79: 79
        }],
        47: [function(t, e, n) {
            var i = t(73),
                r = t(75);
            e.exports = function(t, e) {
                return function(n) {
                    var o, s, a = "data-tw-params";
                    if (n && r.isTwitterURL(n.href) && !n.getAttribute(a)) {
                        if (n.setAttribute(a, !0), "function" == typeof e) {
                            o = e.call(this, n);
                            for (s in o) o.hasOwnProperty(s) && (t[s] = o[s])
                        }
                        n.href = i.url(n.href, t)
                    }
                }
            }
        }, {
            73: 73,
            75: 75
        }],
        48: [function(t, e, n) {
            function i(t) {
                (new o).attachReceiver(new s.Receiver(r, "twttr.resize")).bind("twttr.private.resizeButton", function(e) {
                    var n = c(this),
                        i = n && n.id,
                        r = a.asInt(e.width),
                        o = a.asInt(e.height);
                    i && r && o && t(i, r, o)
                })
            }
            var r = t(16),
                o = t(29),
                s = t(30),
                a = t(76),
                c = t(64);
            e.exports = i
        }, {
            16: 16,
            29: 29,
            30: 30,
            64: 64,
            76: 76
        }],
        49: [function(t, e, n) {
            function i(t) {
                var e;
                t && (t.ownerDocument ? (this.srcEl = t, this.classAttr = t.className.split(" ")) : (this.srcOb = t, this.classAttr = []), e = this.params(), this.id = this.generateId(), this.setLanguage(), this.related = e.related || this.dataAttr("related"), this.partner = e.partner || this.dataAttr("partner") || y.val("partner"), this.styleAttr = [], this.targetEl = t.targetEl, g.asBoolean(e.dnt || this.dataAttr("dnt")) && w.setOn(), x[this.id] = this, this.completeDeferred = new d, this.completed().then(function(t) {
                    t && t != a.body && E.get("events").trigger("rendered", {
                        target: t
                    })
                }))
            }

            function r() {
                I.forEach(function(t) {
                    t()
                }), i.doLayout()
            }

            function o(t) {
                return t ? t.lang ? t.lang : o(t.parentNode) : void 0
            }
            var s, a = t(13),
                c = t(16),
                u = t(43),
                l = t(26),
                d = t(70),
                h = t(65),
                f = t(66),
                p = t(71),
                m = t(73),
                g = t(76),
                v = t(79),
                y = t(19),
                w = t(61),
                b = t(67),
                _ = t(59),
                E = t(21),
                A = t(22),
                T = 0,
                x = {},
                I = [],
                S = new f,
                D = "data-twttr-rendered";
            v.aug(i.prototype, {
                setLanguage: function(t) {
                    var e;
                    return t || (t = this.params().lang || this.dataAttr("lang") || o(this.srcEl)), (t = t && t.toLowerCase()) ? v.contains(A, t) ? this.lang = t : (e = t.replace(/[\-_].*/, ""), v.contains(A, e) ? this.lang = e : void(this.lang = "en")) : this.lang = "en"
                },
                ringo: function(t, e, n) {
                    return n = n || /\{\{([\w_]+)\}\}/g, t.replace(n, function(t, n) {
                        return void 0 !== e[n] ? e[n] : t
                    })
                },
                makeIframeSource: function() {
                    if (this.iframeSource) {
                        var t = m.encode(this.widgetUrlParams());
                        return [u.base(), "/", this.ringo(this.iframeSource, {
                            lang: this.lang
                        }), "#", t].join("")
                    }
                },
                add: function(t) {
                    x[this.id] = t
                },
                create: function(t, e) {
                    var n, i = this;
                    return e[D] = !0, n = h(v.aug({
                        id: this.id,
                        src: t,
                        "class": this.classAttr.join(" ")
                    }, e), {
                        position: "absolute",
                        visibility: "hidden"
                    }, this.targetEl && this.targetEl.ownerDocument), this.srcEl ? this.layout(function() {
                        return i.srcEl.parentNode.replaceChild(n, i.srcEl), n
                    }) : this.targetEl ? this.layout(function() {
                        return i.targetEl.appendChild(n), n
                    }) : p.reject("Did not append widget")
                },
                setInitialSize: function(t, e) {
                    var n = this,
                        i = this.element;
                    return i ? void this.layout(function() {
                        n.width = t, n.height = e, i.style.width = t + "px", i.style.height = e + "px", i.style.position = "static", i.style.visibility = "visible"
                    }).then(function() {
                        n.completeDeferred.resolve(i)
                    }) : !1
                },
                params: function() {
                    var t, e;
                    return this.srcOb ? e = this.srcOb : (t = this.srcEl && this.srcEl.href && this.srcEl.href.split("?")[1], e = t ? m.decode(t) : {}), this.params = function() {
                        return e
                    }, e
                },
                widgetUrlParams: function() {
                    return {}
                },
                dataAttr: function(t) {
                    return this.srcEl && this.srcEl.getAttribute("data-" + t)
                },
                attr: function(t) {
                    return this.srcEl && this.srcEl.getAttribute(t)
                },
                layout: function(t) {
                    return S.enqueue(t)
                },
                generateId: function() {
                    return this.srcEl && this.srcEl.id || "twitter-widget-" + T++
                },
                completed: function() {
                    return this.completeDeferred ? this.completeDeferred.promise : void 0
                }
            }), i.afterLoad = function(t) {
                I.push(t)
            }, i.doLayout = function() {
                S.exec()
            }, i.doLayoutAsync = function() {
                S.delayedExec()
            }, i.init = function(t) {
                s = t
            }, i.reset = function() {
                x = {}
            }, i.findInstance = function(t) {
                return t && x[t] ? x[t] : null
            }, i.find = function(t) {
                var e = i.findInstance(t);
                return e && e.element || null
            }, i.embed = function(t) {
                var e = [],
                    n = [],
                    o = [];
                g.isArray(t) || (t = [t || a]), b.time("sandboxes"), t.forEach(function(t) {
                    v.forIn(s, function(n, i) {
                        var r = t.querySelectorAll(n);
                        v.toRealArray(r).forEach(function(t) {
                            var n;
                            t.getAttribute(D) || (t.setAttribute(D, "true"), n = new i(t), e.push(n), o.push(n.sandboxCreated))
                        })
                    })
                }), p.all(o).then(function() {
                    b.timeEnd("sandboxes")
                }), i.doLayout(), e.forEach(function(t) {
                    n.push(t.completed()), t.render()
                }), p.all(n).then(function(t) {
                    t = t.filter(function(t) {
                        return t
                    }), t.length && (E.get("events").trigger("loaded", {
                        widgets: t
                    }), b.timeEnd("load"))
                }).then(i.trackRender), i.doLayoutAsync(), r()
            }, i.trackRender = function() {
                l.endAndTrack("render", "widgets-js-load", "page", {
                    widget_origin: _.rootDocumentLocation(),
                    widget_frame: _.isFramed() && _.currentDocumentLocation()
                })
            }, c.setInterval(function() {
                i.doLayout()
            }, 500), e.exports = i
        }, {
            13: 13,
            16: 16,
            19: 19,
            21: 21,
            22: 22,
            26: 26,
            43: 43,
            59: 59,
            61: 61,
            65: 65,
            66: 66,
            67: 67,
            70: 70,
            71: 71,
            73: 73,
            76: 76,
            79: 79
        }],
        50: [function(t, e, n) {
            function i(t, e) {
                var n = t.querySelector("blockquote.subject"),
                    i = t.querySelector("blockquote.reply"),
                    r = n && n.getAttribute("data-tweet-id"),
                    o = i && i.getAttribute("data-tweet-id"),
                    s = {},
                    a = {};
                r && (s[r] = {
                    item_type: 0
                }, A.clientEvent({
                    page: "tweet",
                    section: "subject",
                    component: "tweet",
                    action: "results"
                }, w.aug({}, e, {
                    item_ids: [r],
                    item_details: s
                }), !0), E.scribeTweetAudienceImpression(), o && (a[o] = {
                    item_type: 0
                }, A.clientEvent({
                    page: "tweet",
                    section: "conversation",
                    component: "tweet",
                    action: "results"
                }, w.aug({}, e, {
                    item_ids: [o],
                    item_details: a,
                    associations: {
                        4: {
                            association_id: r,
                            association_type: 4
                        }
                    }
                }), !0)))
            }

            function r(t, e) {
                var n = {};
                t && (n[t] = {
                    item_type: 0
                }, A.clientEvent({
                    page: "tweet",
                    section: "subject",
                    component: "rawembedcode",
                    action: "no_results"
                }, {
                    widget_origin: x.rootDocumentLocation(),
                    widget_frame: x.isFramed() && x.currentDocumentLocation(),
                    message: e,
                    item_ids: [t],
                    item_details: n
                }, !0), E.scribeTweetAudienceImpression())
            }

            function o(t, e, n, i) {
                P[t] = P[t] || [], P[t].push({
                    s: n,
                    f: i,
                    lang: e
                })
            }

            function s(t) {
                if (t) {
                    var e, n, i;
                    d.apply(this, [t]), e = this.params(), n = this.srcEl && this.srcEl.getElementsByTagName("A"), i = n && n[n.length - 1], this.hideThread = "none" == (e.conversation || this.dataAttr("conversation")) || w.contains(this.classAttr, "tw-hide-thread"), this.hideCard = "hidden" == (e.cards || this.dataAttr("cards")) || w.contains(this.classAttr, "tw-hide-media"), "left" == (e.align || this.attr("align")) || w.contains(this.classAttr, "tw-align-left") ? this.align = "left" : "right" == (e.align || this.attr("align")) || w.contains(this.classAttr, "tw-align-right") ? this.align = "right" : ("center" == (e.align || this.attr("align")) || w.contains(this.classAttr, "tw-align-center")) && (this.align = "center", this.containerWidth > this.dimensions.MIN_WIDTH * (1 / .7) && this.width > .7 * this.containerWidth && (this.width = .7 * this.containerWidth)), this.narrow = e.narrow || this.width <= this.dimensions.NARROW_WIDTH, this.tweetId = e.tweetId || i && b.status(i.href)
                }
            }
            var a = t(3),
                c = t(16),
                u = t(11),
                l = t(49),
                d = t(53),
                h = t(7),
                f = t(47),
                p = t(8),
                m = t(10),
                g = t(26),
                v = t(71),
                y = t(72),
                w = t(79),
                b = t(75),
                _ = t(44),
                E = t(34),
                A = t(36),
                T = t(46),
                x = t(59),
                I = t(83),
                S = t(21),
                D = t(9),
                N = t(38),
                C = "tweetembed",
                P = {},
                R = [];
            s.prototype = new d, w.aug(s.prototype, {
                renderedClassNames: "twitter-tweet twitter-tweet-rendered",
                dimensions: {
                    DEFAULT_HEIGHT: "0",
                    DEFAULT_WIDTH: "500",
                    NARROW_WIDTH: "350",
                    maxHeight: "375",
                    FULL_BLEED_PHOTO_MAX_HEIGHT: "600",
                    MIN_WIDTH: "220",
                    MIN_HEIGHT: "0",
                    MARGIN: "10px 0",
                    WIDE_MEDIA_PADDING: 32,
                    NARROW_MEDIA_PADDING: 32,
                    BORDERS: 0
                },
                linkColorSelectors: ["a", "a:visited"],
                linkStateColorSelectors: ["a:hover", "a:focus", "a:active"],
                bgColorSelectors: [],
                borderColorSelectors: [],
                styleSheetUrl: a.tweet,
                addSiteStylesPrefix: function(t) {
                    return t
                },
                onStyleSheetLoad: function() {
                    var t = this;
                    this.sandbox.hasContent() && (l.doLayoutAsync(), this.sandbox.resizeToContent().then(function(e) {
                        t.height = e
                    }))
                },
                scribeCardShown: function(t) {
                    var e, n;
                    e = {
                        page: "tweet",
                        component: "card",
                        action: "shown"
                    }, n = {
                        card_details: {
                            card_name: t.getAttribute("data-card-name")
                        }
                    }, N.clientEvent2(e, n, !1)
                },
                loadCardCss: function(t) {
                    function e() {
                        r && (l.doLayoutAsync(), n.sandbox.resizeToContent().then(function(t) {
                            n.height = t
                        }))
                    }
                    var n = this,
                        i = t && t.getAttribute("data-css"),
                        r = !1;
                    i && (w.toRealArray(t.querySelectorAll("img")).forEach(function(t) {
                        t.addEventListener("load", e, !1)
                    }), this.sandbox.prependStyleSheet(i, function() {
                        p.add(t, "is-ready"), n.scribeCardShown(t), l.doLayoutAsync(), n.sandbox.resizeToContent().then(function(t) {
                            r = !0, n.height = t
                        })
                    }))
                },
                create: function(t) {
                    var e, n, r, o = this,
                        s = this.sandbox.createElement("div");
                    return s.innerHTML = t, (e = s.children[0] || !1) ? ("dark" == this.theme && this.classAttr.push("thm-dark"), this.linkColor && this.addSiteStyles(), p.present(e, "media-forward") && (this.fullBleedPhoto = !0, this.dimensions.maxHeight = this.dimensions.FULL_BLEED_PHOTO_MAX_HEIGHT), n = e.querySelector(".GifPlayer"), n && (this.gifPlayer = new I({
                        rootEl: n,
                        videoEl: n.querySelector(".GifPlayer-video"),
                        playButtonEl: n.querySelector(".GifPlayer-playButton"),
                        fallbackUrl: this.extractPermalinkUrl(this.getTweetElement(e))
                    })), T.retinize(e), T.fixMediaCardLayout(e), e.id = this.id, e.className += " " + this.classAttr.join(" "), e.lang = this.lang, this.sandbox.setTitle(e.getAttribute("data-iframe-title") || "Tweet"), this.loadCardCss(e.querySelector(".PrerenderedCard")), this.sandbox.appendChild(e).then(function() {
                        o.renderedDeferred.resolve(o.sandbox)
                    }), r = this.layout(function() {
                        o.predefinedWidth = o.width, o.width = o.sandbox.width(o.width), o.collapseRegions()
                    }), r.then(function() {
                        o.constrainMedia(e, o.contentWidth(o.width)), o.setNarrow().then(function() {
                            o.layout(function() {
                                o.completeDeferred.resolve(o.sandbox.element())
                            })
                        })
                    }), i(e, this.baseScribeData(), this.partner), e) : void 0
                },
                render: function() {
                    var t = this,
                        e = "",
                        n = this.tweetId;
                    return n ? (this.hideCard && (e += "c"), this.hideThread && (e += "t"), e && (n += "-" + e), this.rendered().then(function(e) {
                        var n = t.srcEl;
                        n && n.parentNode && t.layout(function() {
                            n && n.parentNode && n.parentNode.removeChild(n)
                        }), "center" == t.align ? e.style({
                            margin: "7px auto",
                            cssFloat: "none"
                        }) : t.align && (t.width == t.dimensions.DEFAULT_WIDTH && (t.predefinedWidth = t.width = t.dimensions.NARROW_WIDTH), e.style({
                            cssFloat: t.align
                        })), t.sandbox.resizeToContent().then(function(e) {
                            return t.height = e, l.doLayoutAsync(), t.sandbox.resizeToContent().then(function(e) {
                                t.height = e
                            })
                        }).then(function() {
                            e.onresize(t.handleResize.bind(t))
                        }), e.style({
                            position: "static",
                            visibility: "visible"
                        }), l.doLayoutAsync()
                    }), o(n, this.lang, function(e) {
                        t.ready().then(function() {
                            t.element = t.create(e), t.readTimestampTranslations(), t.updateTimeStamps(), t.bindIntentHandlers(), t.bindUIHandlers(), t.bindPermalinkHandler(), l.doLayoutAsync()
                        })
                    }, function() {
                        r(t.tweetId, t.partner), t.completeDeferred.resolve(t.srcEl)
                    }), R.push(this.completed()), this.completed().then(this.scribePerformance.bind(this)), this.completed()) : (this.completeDeferred.resolve(this.srcEl), this.completed())
                },
                bindPermalinkHandler: function() {
                    var t = this;
                    D.delegate(this.element, "click", "A", function(t) {
                        D.stopPropagation(t)
                    }), D.delegate(this.element, "click", ".twitter-tweet", function(e) {
                        var n = t.getTweetElement();
                        u.getSelectedText(t.sandbox._win) || (t.openPermalink(n), t.scribePermalinkClick(n, e), D.stopPropagation(e))
                    })
                },
                scribePermalinkClick: function(t, e) {
                    var n = this.createScribeData(t);
                    N.interaction(e, n, !1)
                },
                getTweetElement: function(t) {
                    var e;
                    return t = t || this.element, t ? (e = t.querySelectorAll("blockquote.tweet"), e[e.length - 1]) : void 0
                },
                extractPermalinkUrl: function(t) {
                    var e = t && t.cite;
                    return b.isStatus(e) && e
                },
                openPermalink: function(t) {
                    var e = this.extractPermalinkUrl(t);
                    e && c.open(e)
                },
                scribePerformance: function() {
                    g.endAndTrack("render", "widgets-js-load", "tweet", this.baseScribeData())
                },
                addUrlParams: function(t) {
                    var e = this,
                        n = {
                            related: this.related,
                            partner: this.partner,
                            original_referer: x.rootDocumentLocation(),
                            tw_p: C
                        };
                    return this.addUrlParams = f(n, function(t) {
                        var n = m.closest(".tweet", t, e.element);
                        return {
                            tw_i: n.getAttribute("data-tweet-id")
                        }
                    }), this.addUrlParams(t)
                },
                baseScribeData: function() {
                    return {
                        widget_origin: x.rootDocumentLocation(),
                        widget_frame: x.isFramed() && x.currentDocumentLocation(),
                        message: this.partner
                    }
                },
                handleResize: function(t) {
                    var e = this;
                    t != this.width && (this.width = t, this.setNarrow(), this.constrainMedia(this.element, this.contentWidth(t)), this.collapseRegions(), this.sandbox.resizeToContent().then(function(t) {
                        e.height = t, S.get("events").trigger("resize", {
                            target: e.sandbox.element()
                        })
                    }), l.doLayoutAsync())
                },
                readTimestampTranslations: function() {
                    var t = this.element,
                        e = "data-dt-",
                        n = t.getAttribute(e + "months") || "";
                    this.datetime = new h(w.compact({
                        phrases: {
                            AM: t.getAttribute(e + "am"),
                            PM: t.getAttribute(e + "pm")
                        },
                        months: n.split("|"),
                        formats: {
                            full: t.getAttribute(e + "full")
                        }
                    }))
                },
                updateTimeStamps: function() {
                    var t = this.element.querySelector(".long-permalink"),
                        e = t.getAttribute("data-datetime"),
                        n = e && this.datetime.localTimeStamp(e),
                        i = t.getElementsByTagName("TIME")[0];
                    n && (this.layout(function() {
                        return i && i.innerHTML ? void(i.innerHTML = n) : void(t.innerHTML = n)
                    }, "Update Timestamp"), l.doLayoutAsync())
                }
            }), s.fetchAndRender = function() {
                function t(t) {
                    w.forIn(t, function(t, e) {
                        var n = i[t];
                        n.forEach(function(t) {
                            t.s && t.s.call(this, e)
                        }), delete i[t]
                    }), l.doLayout(), w.forIn(i, function(t, e) {
                        e.forEach(function(e) {
                            e.f && e.f.call(this, t)
                        })
                    }), l.doLayout()
                }
                var e, n, i = P,
                    r = [];
                if (P = {}, i.keys) r = i.keys();
                else
                    for (e in i) i.hasOwnProperty(e) && r.push(e);
                r.length && (A.init(), n = i[r[0]][0].lang, y.always(_.tweets({
                    ids: r.sort(),
                    lang: n
                }), t), v.all(R).then(function() {
                    A.flush()
                }), R = [])
            }, l.afterLoad(s.fetchAndRender), e.exports = s
        }, {
            10: 10,
            11: 11,
            16: 16,
            21: 21,
            26: 26,
            3: 3,
            34: 34,
            36: 36,
            38: 38,
            44: 44,
            46: 46,
            47: 47,
            49: 49,
            53: 53,
            59: 59,
            7: 7,
            71: 71,
            72: 72,
            75: 75,
            79: 79,
            8: 8,
            83: 83,
            9: 9
        }],
        51: [function(t, e, n) {
            function i(t) {
                if (t) {
                    var e, n, i, r;
                    s.apply(this, [t]), e = this.params(), n = e.size || this.dataAttr("size"), i = e.showScreenName || this.dataAttr("show-screen-name"), r = e.count || this.dataAttr("count"), this.classAttr.push("twitter-follow-button"), this.showScreenName = "false" != i, this.showCount = !(e.showCount === !1 || "false" == this.dataAttr("show-count")), "none" == r && (this.showCount = !1), this.explicitWidth = e.width || this.dataAttr("width") || "", this.screenName = e.screen_name || e.screenName || a.screenName(this.attr("href")), this.preview = e.preview || this.dataAttr("preview") || "", this.align = e.align || this.dataAttr("align") || "", this.size = "large" == n ? "l" : "m"
                }
            }
            var r = t(61),
                o = t(79),
                s = t(49),
                a = t(75),
                c = t(71);
            i.prototype = new s, o.aug(i.prototype, {
                iframeSource: "widgets/follow_button.403b2a3ca10837290e034c22c8a16c06.{{lang}}.html",
                widgetUrlParams: function() {
                    return o.compact({
                        screen_name: this.screenName,
                        lang: this.lang,
                        show_count: this.showCount,
                        show_screen_name: this.showScreenName,
                        align: this.align,
                        id: this.id,
                        preview: this.preview,
                        size: this.size,
                        partner: this.partner,
                        dnt: r.enabled(),
                        _: +new Date
                    })
                },
                render: function() {
                    if (!this.screenName) return c.reject("Missing Screen Name");
                    var t = this,
                        e = this.makeIframeSource(),
                        n = this.create(e, {
                            title: "Twitter Follow Button"
                        }).then(function(e) {
                            return t.element = e
                        });
                    return n
                }
            }), e.exports = i
        }, {
            49: 49,
            61: 61,
            71: 71,
            75: 75,
            79: 79
        }],
        52: [function(t, e, n) {
            function i(t) {
                p.open(t)
            }

            function r(e, n) {
                var i = t(41);
                i.openIntent(e, n)
            }

            function o(t, e) {
                if (f.isTwitterURL(t))
                    if (g.get("eventsHub") && e) {
                        var n = new s(c.generateId(), e);
                        c.add(n), r(t, e), m.get("events").trigger("click", {
                            target: e,
                            region: "intent",
                            type: "click",
                            data: {}
                        })
                    } else i(t)
            }

            function s(t, e) {
                this.id = t, this.element = this.srcEl = e
            }

            function a(t) {
                this.srcEl = [], this.element = t
            }
            var c, u = t(13),
                l = t(49),
                d = t(79),
                h = t(71),
                f = t(75),
                p = t(25),
                m = t(21),
                g = t(20);
            a.prototype = new l, d.aug(a.prototype, {
                render: function() {
                    return c = this, h.resolve(u.body)
                }
            }), a.open = o, e.exports = a
        }, {
            13: 13,
            20: 20,
            21: 21,
            25: 25,
            41: 41,
            49: 49,
            71: 71,
            75: 75,
            79: 79
        }],
        53: [function(t, e, n) {
            function i() {
                s = r.VALID_COLOR.test(h.val("widgets:link-color")) && RegExp.$1, c = r.VALID_COLOR.test(h.val("widgets:border-color")) && RegExp.$1, a = h.val("widgets:theme")
            }

            function r(t) {
                if (t) {
                    var e, n = this;
                    this.readyDeferred = new A, this.renderedDeferred = new A, l.apply(this, [t]), e = this.params(), this.targetEl = this.srcEl && this.srcEl.parentNode || e.targetEl || u.body, this.predefinedWidth = r.VALID_UNIT.test(e.width || this.attr("width")) && RegExp.$1, this.layout(function() {
                        return n.containerWidth = b.effectiveWidth(n.targetEl)
                    }).then(function(t) {
                        var i = n.predefinedWidth || t || n.dimensions.DEFAULT_WIDTH;
                        n.height = r.VALID_UNIT.test(e.height || n.attr("height")) && RegExp.$1, n.width = Math.max(n.dimensions.MIN_WIDTH, Math.min(i, n.dimensions.DEFAULT_WIDTH))
                    }), r.VALID_COLOR.test(e.linkColor || this.dataAttr("link-color")) ? this.linkColor = RegExp.$1 : this.linkColor = s, r.VALID_COLOR.test(e.borderColor || this.dataAttr("border-color")) ? this.borderColor = RegExp.$1 : this.borderColor = c, this.theme = e.theme || this.attr("data-theme") || a, this.theme = /(dark|light)/.test(this.theme) ? this.theme : "", T.ie9() && this.classAttr.push("ie9"), this.sandboxCreated = _.createSandbox({
                        "class": this.renderedClassNames,
                        id: this.id,
                        allowfullscreen: ""
                    }, {
                        position: "absolute",
                        visibility: "hidden"
                    }, function(t) {
                        n.modifyFrame && (t = n.modifyFrame(t)), n.srcEl ? n.targetEl.insertBefore(t, n.srcEl) : n.targetEl.appendChild(t)
                    }, this.layout).then(function(t) {
                        n.setupSandbox(t), new g(t.element().contentWindow)
                    }), this.rendered().then(function(t) {
                        n.applyVisibleSandboxStyles(t)
                    })
                }
            }

            function o(t, e) {
                return t + e
            }
            var s, a, c, u = t(13),
                l = t(49),
                d = t(52),
                h = t(19),
                f = t(46),
                p = t(38),
                m = t(35),
                g = t(85),
                v = t(8),
                y = t(10),
                w = t(9),
                b = t(12),
                _ = t(32),
                E = t(57),
                A = t(70),
                T = t(62),
                x = t(71),
                I = t(75),
                S = t(76),
                D = t(79),
                N = t(72),
                C = [".timeline-header h1.summary", ".timeline-header h1.summary a:link", ".timeline-header h1.summary a:visited"];
            r.prototype = new l, D.aug(r.prototype, {
                dimensions: {},
                linkColorSelectors: [".customisable", ".customisable:link", ".customisable:visited"],
                linkStateColorSelectors: [".customisable:hover", ".customisable:focus", ".customisable:active", ".customisable-highlight:hover", ".customisable-highlight:focus", "a:hover .customisable-highlight", "a:focus .customisable-highlight"],
                bgColorSelectors: ["a:hover .ic-mask", "a:focus .ic-mask"],
                borderColorSelectors: [".customisable-border"],
                styleSheetUrl: function() {
                    throw new Error("must set styleSheetUrl")
                },
                onStyleSheetLoad: function() {},
                setupSandbox: function(t) {
                    var e, n, i = this;
                    this.sandbox = t, T.ios() && v.add(this.sandbox.root, "env-ios"), T.touch() && v.add(this.sandbox.root, "is-touch"), e = this.styleSheetUrl(this.lang, this.theme), n = this.onStyleSheetLoad.bind(this), N.some([i.applyInitialSandboxStyles(t), t.appendCss(".SandboxRoot { display:none }"), t.setBaseTarget("_blank"), t.appendStyleSheet(e, n)]).then(function() {
                        i.readyDeferred.resolve(t)
                    })
                },
                ready: function() {
                    return this.readyDeferred.promise
                },
                rendered: function() {
                    return this.renderedDeferred.promise
                },
                contentWidth: function(t) {
                    var e = this.dimensions,
                        n = this.borderless ? 0 : e.BORDERS,
                        i = this.fullBleedPhoto ? 0 : this.chromeless && this.narrow ? e.NARROW_MEDIA_PADDING_CL : this.chromeless ? e.WIDE_MEDIA_PADDING_CL : this.narrow ? e.NARROW_MEDIA_PADDING : e.WIDE_MEDIA_PADDING;
                    return (t || this.width) - (i + n)
                },
                applyInitialSandboxStyles: function(t) {
                    var e = this;
                    return t.style({
                        border: "none",
                        maxWidth: "100%",
                        minWidth: e.dimensions.MIN_WIDTH + "px",
                        margin: e.dimensions.MARGIN,
                        padding: "0",
                        display: "block",
                        position: "absolute",
                        visibility: "hidden"
                    }, !0)
                },
                applyVisibleSandboxStyles: function(t) {
                    return t.style({
                        position: "static",
                        visibility: "visible"
                    })
                },
                addSiteStylesPrefix: function(t) {
                    return ("dark" == this.theme ? ".thm-dark " : "") + t
                },
                addSiteStyles: function() {
                    var t = [],
                        e = this.addSiteStylesPrefix.bind(this);
                    return this.headingStyle && t.push(C.map(e).join(",") + "{" + this.headingStyle + "}"), this.linkColor && (t.push(this.linkColorSelectors.map(e).join(",") + "{color:" + this.linkColor + "}"), t.push(this.bgColorSelectors.map(e).join(",") + "{background-color:" + this.linkColor + "}"), t.push(this.linkStateColorSelectors.map(e).join(",") + "{color:" + E.lighten(this.linkColor, .2) + "}")), this.borderColor && t.push(this.borderColorSelectors.map(e).concat("dark" == this.theme ? [".thm-dark.customisable-border"] : []).join(",") + "{border-color:" + this.borderColor + "}"), t.length ? this.sandbox.appendCss(t.join("")) : void 0
                },
                setNarrow: function() {
                    var t = this,
                        e = this.narrow;
                    return this.narrow = this.width < this.dimensions.NARROW_WIDTH, e != this.narrow ? this.layout(function() {
                        v.toggle(t.sandbox.root, "env-narrow", t.narrow)
                    }) : x.resolve(this.narrow)
                },
                createScribeData: function(t) {
                    var e = D.aug({}, this.baseScribeData(), {
                        item_ids: [],
                        item_details: this.extractTweetScribeDetails(t)
                    });
                    return D.forIn(e.item_details, function(t) {
                        e.item_ids.push(t)
                    }), e
                },
                bindUIHandlers: function() {
                    var t = this.element;
                    w.delegate(t, "click", ".MediaCard-dismissNsfw", function() {
                        var e = y.closest(".MediaCard", this, t);
                        v.remove(e, "is-nsfw")
                    })
                },
                bindIntentHandlers: function() {
                    function t(t) {
                        var i = y.closest(".tweet", this, n),
                            r = e.createScribeData(i);
                        p.interaction(t, r, !0)
                    }
                    var e = this,
                        n = this.element;
                    w.delegate(n, "click", "A", t), w.delegate(n, "click", "BUTTON", t), w.delegate(n, "click", ".profile", function() {
                        e.addUrlParams(this)
                    }), w.delegate(n, "click", ".follow-button", function(t) {
                        var n;
                        t.altKey || t.metaKey || t.shiftKey || T.ios() || T.android() || S.asBoolean(this.getAttribute("data-age-gate")) || (n = I.intentForFollowURL(this.href, !0), n && (d.open(n, e.sandbox.element()), w.preventDefault(t)))
                    }), w.delegate(n, "click", ".web-intent", function(t) {
                        e.addUrlParams(this), t.altKey || t.metaKey || t.shiftKey || (d.open(this.href, e.sandbox.element()), w.preventDefault(t))
                    })
                },
                baseScribeData: function() {
                    return {}
                },
                extractTweetScribeDetails: m,
                constrainMedia: function(t, e, n) {
                    return f.constrainMedia(t || this.element, e || this.contentWidth(), this.dimensions.maxHeight, n || this.layout)
                },
                collapseRegions: function() {
                    var t = this;
                    D.toRealArray(this.element.querySelectorAll(".collapsible-container")).forEach(function(e) {
                        var n, i, r = D.toRealArray(e.children),
                            s = r.length && e.offsetWidth,
                            a = r.length && r.map(function(t) {
                                return t.offsetWidth
                            }),
                            c = r.length;
                        if (r.length)
                            for (; c > 0;) {
                                if (c--, n = a.reduce(o, 0), !s || !n) return;
                                if (s > n) return;
                                i = r[c].getAttribute("data-collapsed-class"), i && (v.add(t.element, i), a[c] = r[c].offsetWidth)
                            }
                    })
                }
            }), r.VALID_UNIT = /^([0-9]+)( ?px)?$/, r.VALID_COLOR = /^(#(?:[0-9a-f]{3}|[0-9a-f]{6}))$/i, i(), e.exports = r
        }, {
            10: 10,
            12: 12,
            13: 13,
            19: 19,
            32: 32,
            35: 35,
            38: 38,
            46: 46,
            49: 49,
            52: 52,
            57: 57,
            62: 62,
            70: 70,
            71: 71,
            72: 72,
            75: 75,
            76: 76,
            79: 79,
            8: 8,
            85: 85,
            9: 9
        }],
        54: [function(t, e, n) {
            function i(t) {
                if (t) {
                    var e, n, i, r, o, s, c, u;
                    a.apply(this, [t]), e = this.params(), n = (e.chrome || this.dataAttr("chrome") || "").split(" "), this.preview = e.previewParams, this.widgetId = e.widgetId || this.dataAttr("widget-id"), this.instanceId = ++B, this.cursors = {
                        maxPosition: 0,
                        minPosition: 0
                    }, (r = e.screenName || this.dataAttr("screen-name")) || (o = e.userId || this.dataAttr("user-id")) ? this.override = {
                        overrideType: "user",
                        overrideId: o,
                        overrideName: r,
                        withReplies: y.asBoolean(e.showReplies || this.dataAttr("show-replies")) ? "true" : "false"
                    } : (r = e.favoritesScreenName || this.dataAttr("favorites-screen-name")) || (o = e.favoritesUserId || this.dataAttr("favorites-user-id")) ? this.override = {
                        overrideType: "favorites",
                        overrideId: o,
                        overrideName: r
                    } : ((r = e.listOwnerScreenName || this.dataAttr("list-owner-screen-name")) || (o = e.listOwnerId || this.dataAttr("list-owner-id"))) && ((s = e.listId || this.dataAttr("list-id")) || (c = e.listSlug || this.dataAttr("list-slug"))) ? this.override = {
                        overrideType: "list",
                        overrideOwnerId: o,
                        overrideOwnerName: r,
                        overrideId: s,
                        overrideName: c
                    } : (u = e.customTimelineId || this.dataAttr("custom-timeline-id")) ? this.override = {
                        overrideType: "custom",
                        overrideId: u
                    } : this.override = {}, this.tweetLimit = y.asInt(e.tweetLimit || this.dataAttr("tweet-limit")), this.staticTimeline = this.tweetLimit > 0, n.length && (i = w.contains(n, "none"), this.chromeless = i || w.contains(n, "transparent"), this.headerless = i || w.contains(n, "noheader"), this.footerless = i || w.contains(n, "nofooter"), this.borderless = i || w.contains(n, "noborders"), this.noscrollbar = w.contains(n, "noscrollbar")), this.headingStyle = g.sanitize(e.headingStyle || this.dataAttr("heading-style"), void 0, !0), this.classAttr.push("twitter-timeline-rendered"), this.ariaPolite = e.ariaPolite || this.dataAttr("aria-polite")
                }
            }
            var r = t(16),
                o = t(3),
                s = t(49),
                a = t(53),
                c = t(7),
                u = t(2),
                l = t(26),
                d = t(44),
                h = t(46),
                f = t(34),
                p = t(36),
                m = t(47),
                g = t(58),
                v = t(62),
                y = t(76),
                w = t(79),
                b = t(9),
                _ = t(8),
                E = t(10),
                A = t(61),
                T = t(59),
                x = t(21),
                I = t(20),
                S = t(37),
                D = {
                    CLIENT_SIDE_USER: 0,
                    CLIENT_SIDE_APP: 2
                },
                N = ".timeline",
                C = ".new-tweets-bar",
                P = ".timeline-header",
                R = ".timeline-footer",
                L = ".stream",
                k = ".h-feed",
                O = ".tweet",
                M = ".detail-expander",
                H = ".expand",
                W = ".permalink",
                U = ".no-more-pane",
                j = "expanded",
                q = "pending-scroll-in",
                F = "pending-new-tweet-display",
                z = "pending-new-tweet",
                B = 0;
            i.prototype = new a, w.aug(i.prototype, {
                renderedClassNames: "twitter-timeline twitter-timeline-rendered",
                dimensions: {
                    DEFAULT_HEIGHT: "600",
                    DEFAULT_WIDTH: "520",
                    NARROW_WIDTH: "320",
                    maxHeight: "375",
                    MIN_WIDTH: "180",
                    MIN_HEIGHT: "200",
                    MARGIN: "0",
                    WIDE_MEDIA_PADDING: 81,
                    NARROW_MEDIA_PADDING: 16,
                    WIDE_MEDIA_PADDING_CL: 60,
                    NARROW_MEDIA_PADDING_CL: 12,
                    BORDERS: 2
                },
                styleSheetUrl: o.timeline,
                create: function(t) {
                    var e, n, i, r, o = this,
                        s = this.sandbox.createElement("div"),
                        a = [];
                    return s.innerHTML = t.body, (e = s.children[0] || !1) ? (this.reconfigure(t.config), this.discardStaticOverflow(e), this.sandbox.setTitle(e.getAttribute("data-iframe-title") || "Timeline"), h.retinize(e), this.constrainMedia(e), this.searchQuery = e.getAttribute("data-search-query"), this.profileId = e.getAttribute("data-profile-id"), this.timelineType = e.getAttribute("data-timeline-type"), this.collectionId = e.getAttribute("data-collection-id"), r = this.getTweetDetails(s.querySelector(k)), w.forIn(r, function(t) {
                        a.push(t)
                    }), i = this.baseScribeData(), i.item_ids = a, i.item_details = r, this.collectionId && (i.item_ids.push(this.collectionId), i.item_details[this.collectionId] = {
                        item_type: S.CUSTOM_TIMELINE
                    }), this.timelineType && p.clientEvent({
                        page: this.timelineType + "_timeline",
                        component: "timeline",
                        element: "initial",
                        action: a.length ? "results" : "no_results"
                    }, i, !0), p.clientEvent({
                        page: "timeline",
                        component: "timeline",
                        element: "initial",
                        action: a.length ? "results" : "no_results"
                    }, i, !0), f.scribeTimelineAudienceImpression(), p.flush(), "assertive" == this.ariaPolite && (n = e.querySelector(C), n.setAttribute("aria-polite", "assertive")), e.id = this.id, e.className += " " + this.classAttr.join(" "), e.lang = this.lang, this.ready().then(function(t) {
                        t.appendChild(e).then(function() {
                            o.renderedDeferred.resolve(o.sandbox)
                        }), t.style({
                            display: "inline-block"
                        }), o.layout(function() {
                            o.srcEl && o.srcEl.parentNode && o.srcEl.parentNode.removeChild(o.srcEl), o.predefinedWidth = o.width, o.predefinedHeight = o.height, o.width = t.width(o.width), o.height = t.height(o.height)
                        }).then(function() {
                            o.setNarrow(), o.sandbox.onresize(o.handleResize.bind(o)), o.completeDeferred.resolve(o.sandbox.element())
                        })
                    }), e) : void 0
                },
                render: function() {
                    var t = this;
                    return this.preview || this.widgetId ? (this.rendered().then(this.staticTimeline ? function(t) {
                        t.resizeToContent(), s.doLayoutAsync()
                    } : function() {
                        t.recalculateStreamHeight(), s.doLayoutAsync()
                    }), this.preview ? this.getPreviewTimeline() : this.getTimeline(), this.completed().then(this.scribePerformance.bind(this)), this.completed()) : (this.completeDeferred.reject(400), this.completed())
                },
                scribePerformance: function() {
                    l.endAndTrack("render", "widgets-js-load", "timeline", this.baseScribeData())
                },
                getPreviewTimeline: function() {
                    function t(t) {
                        n.ready().then(function() {
                            n.element = n.create(t), n.readTranslations(), n.bindInteractions(), n.updateCursors(t.headers, {
                                initial: !0
                            }), s.doLayoutAsync()
                        })
                    }

                    function e(t) {
                        return t && t.headers ? void n.completeDeferred.reject(t.headers.status) : void n.completeDeferred.resolve(n.srcEl)
                    }
                    var n = this;
                    d.timelinePreview({
                        params: this.preview
                    }).then(t, e)
                },
                getTimeline: function() {
                    function t(t) {
                        n.ready().then(function() {
                            n.element = n.create(t), n.readTranslations(), n.bindInteractions(), n.updateTimeStamps(), n.updateCursors(t.headers, {
                                initial: !0
                            }), t.headers.xPolling && /\d/.test(t.headers.xPolling) && (n.pollInterval = 1e3 * t.headers.xPolling), n.staticTimeline || n.schedulePolling(), s.doLayoutAsync()
                        })
                    }

                    function e(t) {
                        return t && t.headers ? void n.completeDeferred.reject(t.headers.status) : void n.completeDeferred.resolve(n.srcEl)
                    }
                    var n = this;
                    p.init(), d.timeline(w.aug({
                        id: this.widgetId,
                        instanceId: this.instanceId,
                        dnt: A.enabled(),
                        lang: this.lang
                    }, this.override)).then(t, e)
                },
                reconfigure: function(t) {
                    this.lang = t.lang, this.theme || (this.theme = t.theme), "dark" == this.theme && this.classAttr.push("thm-dark"), this.chromeless && this.classAttr.push("var-chromeless"), this.borderless && this.classAttr.push("var-borderless"), this.headerless && this.classAttr.push("var-headerless"), this.footerless && this.classAttr.push("var-footerless"), this.staticTimeline && this.classAttr.push("var-static"), !this.linkColor && t.linkColor && a.VALID_COLOR.test(t.linkColor) && (this.linkColor = RegExp.$1), !this.height && a.VALID_UNIT.test(t.height) && (this.height = RegExp.$1), this.height = Math.max(this.dimensions.MIN_HEIGHT, this.height ? this.height : this.dimensions.DEFAULT_HEIGHT), this.preview && this.classAttr.push("var-preview"), this.narrow = this.width <= this.dimensions.NARROW_WIDTH, this.narrow && _.add(this.sandbox.root, "env-narrow"), this.addSiteStyles()
                },
                getTweetDetails: function(t) {
                    var e, n = this,
                        i = {};
                    return e = t && t.children || [], w.toRealArray(e).forEach(function(t) {
                        w.aug(i, n.extractTweetScribeDetails(t))
                    }), i
                },
                baseScribeData: function() {
                    return {
                        widget_id: this.widgetId,
                        widget_origin: T.rootDocumentLocation(),
                        widget_frame: T.isFramed() && T.currentDocumentLocation(),
                        message: this.partner,
                        query: this.searchQuery,
                        profile_id: this.profileId
                    }
                },
                bindInteractions: function() {
                    var t = this,
                        e = this.element,
                        n = !0;
                    this.bindIntentHandlers(), this.bindUIHandlers(), b.delegate(e, "click", ".load-tweets", function(e) {
                        n && (n = !1, t.forceLoad(), b.stop(e))
                    }), b.delegate(e, "click", ".display-sensitive-image", function(n) {
                        t.showNSFW(E.closest(O, this, e)), b.stop(n)
                    }), b.delegate(e, "mouseover", N, function() {
                        t.mouseOver = !0
                    }), b.delegate(e, "mouseout", N, function() {
                        t.mouseOver = !1
                    }), b.delegate(e, "mouseover", C, function() {
                        t.mouseOverNotifier = !0
                    }), b.delegate(e, "mouseout", C, function() {
                        t.mouseOverNotifier = !1, r.setTimeout(function() {
                            t.hideNewTweetNotifier()
                        }, 3e3)
                    }), this.staticTimeline || (b.delegate(e, "click", H, function(n) {
                        n.altKey || n.metaKey || n.shiftKey || (t.toggleExpando(E.closest(O, this, e)), b.stop(n))
                    }), b.delegate(e, "click", "A", function(t) {
                        b.stopPropagation(t)
                    }), b.delegate(e, "click", ".with-expansion", function(e) {
                        t.toggleExpando(this), b.stop(e)
                    }), b.delegate(e, "click", ".load-more", function() {
                        t.loadMore()
                    }), b.delegate(e, "click", C, function() {
                        t.scrollToTop(), t.hideNewTweetNotifier(!0)
                    }))
                },
                scrollToTop: function() {
                    var t = this.element.querySelector(L);
                    t.scrollTop = 0, t.focus()
                },
                update: function() {
                    var t = this,
                        e = this.element.querySelector(k),
                        n = e && e.children[0],
                        i = n && n.getAttribute("data-tweet-id");
                    this.updateTimeStamps(), this.requestTweets(i, !0, function(e) {
                        e.childNodes.length > 0 && t.insertNewTweets(e)
                    })
                },
                loadMore: function() {
                    var t = this,
                        e = w.toRealArray(this.element.querySelectorAll(O)).pop(),
                        n = e && e.getAttribute("data-tweet-id");
                    this.requestTweets(n, !1, function(e) {
                        var i = t.element.querySelector(U),
                            r = e.childNodes[0];
                        return i.style.cssText = "", r && r.getAttribute("data-tweet-id") == n && e.removeChild(r), e.childNodes.length > 0 ? void t.appendTweets(e) : (_.add(t.element, "no-more"), void i.focus())
                    })
                },
                forceLoad: function() {
                    var t = this,
                        e = !!this.element.querySelectorAll(k).length;
                    this.requestTweets(1, !0, function(n) {
                        n.childNodes.length && (t[e ? "insertNewTweets" : "appendTweets"](n), _.add(t.element, "has-tweets"))
                    })
                },
                schedulePolling: function(t) {
                    var e = this;
                    null !== this.pollInterval && (t = I.get("timeline.pollInterval") || t || this.pollInterval || 1e4, t > -1 && r.setTimeout(function() {
                        e.isUpdating || e.update(), e.schedulePolling()
                    }, t))
                },
                updateCursors: function(t, e) {
                    (e || {}).initial ? (this.cursors.maxPosition = t.maxPosition, this.cursors.minPosition = t.minPosition) : (e || {}).newer ? this.cursors.maxPosition = t.maxPosition || this.cursors.maxPosition : this.cursors.minPosition = t.minPosition || this.cursors.minPosition
                },
                requestTweets: function(t, e, n) {
                    function i(t) {
                        if (o.isUpdating = !1, t && t.headers) {
                            if ("404" == t.headers.status) return void(o.pollInterval = null);
                            if ("503" == t.headers.status) return void(o.pollInterval *= 1.5)
                        }
                    }

                    function r(t) {
                        var i, r, s = o.sandbox.createDocumentFragment(),
                            a = o.sandbox.createElement("ol"),
                            c = [];
                        if (o.isUpdating = !1, o.updateCursors(t.headers, {
                                newer: e
                            }), t && t.headers && t.headers.xPolling && /\d+/.test(t.headers.xPolling) && (o.pollInterval = 1e3 * t.headers.xPolling), t && void 0 !== t.body) {
                            if (a.innerHTML = t.body, a.children[0] && "LI" != a.children[0].tagName) return;
                            for (r = o.getTweetDetails(a), w.forIn(r, function(t) {
                                    c.push(t)
                                }), c.length && (i = o.baseScribeData(), i.item_ids = c, i.item_details = r, i.event_initiator = e ? D.CLIENT_SIDE_APP : D.CLIENT_SIDE_USER, o.timelineType && p.clientEvent({
                                    page: o.timelineType + "_timeline",
                                    component: "timeline",
                                    element: "initial",
                                    action: c.length ? "results" : "no_results"
                                }, i, !0), p.clientEvent({
                                    page: "timeline",
                                    component: "timeline",
                                    element: e ? "newer" : "older",
                                    action: "results"
                                }, i, !0), p.flush()), h.retinize(a), o.constrainMedia(a); a.children[0];) s.appendChild(a.children[0]);
                            n(s)
                        }
                    }
                    var o = this,
                        s = {
                            id: this.widgetId,
                            instanceId: this.instanceId,
                            screenName: this.widgetScreenName,
                            userId: this.widgetUserId,
                            withReplies: this.widgetShowReplies,
                            dnt: A.enabled(),
                            lang: this.lang
                        };
                    e && this.cursors.maxPosition ? s.minPosition = this.cursors.maxPosition : !e && this.cursors.minPosition ? s.maxPosition = this.cursors.minPosition : e ? s.sinceId = t : s.maxId = t, d.timelinePoll(w.aug(s, this.override)).then(r, i)
                },
                insertNewTweets: function(t) {
                    var e, n = this,
                        i = this.element.querySelector(L),
                        o = i.querySelector(k),
                        s = o.offsetHeight;
                    return o.insertBefore(t, o.firstChild), e = o.offsetHeight - s, x.get("events").trigger("timelineUpdated", {
                        target: this.sandbox.element(),
                        region: "newer"
                    }), i.scrollTop > 40 || this.mouseIsOver() ? (i.scrollTop = i.scrollTop + e, this.updateTimeStamps(), void this.showNewTweetNotifier()) : (_.remove(this.element, q), o.style.cssText = "margin-top: -" + e + "px", r.setTimeout(function() {
                        i.scrollTop = 0, _.add(n.element, q), v.cssTransitions() ? o.style.cssText = "" : u.animate(function(t) {
                            e > t ? o.style.cssText = "margin-top: -" + (e - t) + "px" : o.style.cssText = ""
                        }, e, 500, u.easeOut)
                    }, 500), this.updateTimeStamps(), void("custom" != this.timelineType && this.gcTweets(50)))
                },
                appendTweets: function(t) {
                    var e = this.element.querySelector(k);
                    e.appendChild(t), this.updateTimeStamps(), x.get("events").trigger("timelineUpdated", {
                        target: this.sandbox.element(),
                        region: "older"
                    })
                },
                gcTweets: function(t) {
                    var e, n = this.element.querySelector(k),
                        i = n.children.length;
                    for (t = t || 50; i > t && (e = n.children[i - 1]); i--) n.removeChild(e)
                },
                showNewTweetNotifier: function() {
                    var t = this,
                        e = this.element.querySelector(C),
                        n = e.children[0];
                    e.style.cssText = "", e.removeChild(n), e.appendChild(n), _.add(this.element, F), r.setTimeout(function() {
                        _.add(t.element, z)
                    }, 10), this.newNoticeDisplayTime = +new Date, r.setTimeout(function() {
                        t.hideNewTweetNotifier()
                    }, 5e3)
                },
                hideNewTweetNotifier: function(t) {
                    var e = this;
                    (t || !this.mouseOverNotifier) && (_.remove(this.element, z), r.setTimeout(function() {
                        _.remove(e.element, F)
                    }, 500))
                },
                discardStaticOverflow: function(t) {
                    var e, n = t.querySelector(k);
                    if (this.staticTimeline)
                        for (this.height = 0; e = n.children[this.tweetLimit];) n.removeChild(e)
                },
                hideStreamScrollBar: function() {
                    var t, e = this.element.querySelector(L),
                        n = this.element.querySelector(k);
                    e.style.width = "", t = this.element.offsetWidth - n.offsetWidth, t > 0 && (e.style.width = this.element.offsetWidth + t + "px")
                },
                readTranslations: function() {
                    var t = this.element,
                        e = "data-dt-";
                    this.datetime = new c(w.compact({
                        phrases: {
                            now: t.getAttribute(e + "now"),
                            s: t.getAttribute(e + "s"),
                            m: t.getAttribute(e + "m"),
                            h: t.getAttribute(e + "h"),
                            second: t.getAttribute(e + "second"),
                            seconds: t.getAttribute(e + "seconds"),
                            minute: t.getAttribute(e + "minute"),
                            minutes: t.getAttribute(e + "minutes"),
                            hour: t.getAttribute(e + "hour"),
                            hours: t.getAttribute(e + "hours")
                        },
                        months: t.getAttribute(e + "months").split("|"),
                        formats: {
                            abbr: t.getAttribute(e + "abbr"),
                            shortdate: t.getAttribute(e + "short"),
                            longdate: t.getAttribute(e + "long")
                        }
                    }))
                },
                updateTimeStamps: function() {
                    for (var t, e, n, i, r = this.element.querySelectorAll(W), o = 0; t = r[o]; o++) n = t.getAttribute("data-datetime"), i = n && this.datetime.timeAgo(n, this.i18n), e = t.getElementsByTagName("TIME")[0], i && (e && e.innerHTML ? e.innerHTML = i : t.innerHTML = i)
                },
                mouseIsOver: function() {
                    return this.mouseOver
                },
                addUrlParams: function(t) {
                    var e = this,
                        n = {
                            tw_w: this.widgetId,
                            related: this.related,
                            partner: this.partner,
                            query: this.searchQuery,
                            profile_id: this.profileId,
                            original_referer: T.rootDocumentLocation(),
                            tw_p: "embeddedtimeline"
                        };
                    return this.addUrlParams = m(n, function(t) {
                        var n = E.closest(O, t, e.element);
                        return n && {
                            tw_i: n.getAttribute("data-tweet-id")
                        }
                    }), this.addUrlParams(t)
                },
                showNSFW: function(t) {
                    var e, n, i, r, o, s, a = t.querySelector(".nsfw"),
                        c = 0;
                    a && (n = h.scaleDimensions(a.getAttribute("data-width"), a.getAttribute("data-height"), this.contentWidth(), a.getAttribute("data-height")), e = !!(r = a.getAttribute("data-player")), e ? o = this.sandbox.createElement("iframe") : (o = this.sandbox.createElement("img"), r = a.getAttribute(v.retina() ? "data-image-2x" : "data-image"), o.alt = a.getAttribute("data-alt"), s = this.sandbox.createElement("a"), s.href = a.getAttribute("data-href"), s.appendChild(o)), o.title = a.getAttribute("data-title"), o.src = r, o.width = n.width, o.height = n.height, i = E.closest(M, a, t), c = n.height - a.offsetHeight, a.parentNode.replaceChild(e ? o : s, a), i.style.cssText = "height:" + (i.offsetHeight + c) + "px")
                },
                toggleExpando: function(t) {
                    var e, n = this,
                        i = t.querySelector(M),
                        r = i && i.children[0],
                        o = r && r.getAttribute("data-expanded-media"),
                        a = t.querySelector(H),
                        c = a && a.getElementsByTagName("B")[0],
                        u = c && (c.innerText || c.textContent);
                    if (c) {
                        if (this.layout(function() {
                                c.innerHTML = a.getAttribute("data-toggled-text"), a.setAttribute("data-toggled-text", u)
                            }), _.present(t, j)) return this.layout(function() {
                            _.remove(t, j)
                        }), i ? (this.layout(function() {
                            i.style.cssText = "", r.innerHTML = ""
                        }), void s.doLayout()) : void s.doLayout();
                        o && (e = this.sandbox.createElement("DIV"), e.innerHTML = o, h.retinize(e), this.layout(function() {
                            r.appendChild(e), n.constrainMedia(e, null, function(t) {
                                t()
                            })
                        })), i && this.layout(function() {
                            i.style.maxHeight = "500px"
                        }), this.layout(function() {
                            _.add(t, j)
                        }), s.doLayout()
                    }
                },
                recalculateStreamHeight: function(t) {
                    var e = this,
                        n = this.element.querySelector(P),
                        i = this.element.querySelector(R),
                        r = this.element.querySelector(L);
                    this.layout(function() {
                        var o = n.offsetHeight + (i ? i.offsetHeight : 0),
                            s = t || e.sandbox.height();
                        r.style.cssText = "height:" + (s - o - 2) + "px", e.noscrollbar && e.hideStreamScrollBar()
                    })
                },
                handleResize: function(t, e) {
                    var n = this,
                        i = Math.min(this.dimensions.DEFAULT_WIDTH, Math.max(this.dimensions.MIN_WIDTH, Math.min(this.predefinedWidth || this.dimensions.DEFAULT_WIDTH, t)));
                    (i != this.width || e != this.height) && (this.width = i, this.height = e, this.setNarrow(), this.constrainMedia(this.element, this.contentWidth(i)), this.staticTimeline ? this.layout(function() {
                        n.height = n.element.offsetHeight, n.sandbox.height(n.height), x.get("events").trigger("resize", {
                            target: n.sandbox.element()
                        })
                    }) : (this.recalculateStreamHeight(e), x.get("events").trigger("resize", {
                        target: this.sandbox.element()
                    })), s.doLayoutAsync())
                }
            }), e.exports = i
        }, {
            10: 10,
            16: 16,
            2: 2,
            20: 20,
            21: 21,
            26: 26,
            3: 3,
            34: 34,
            36: 36,
            37: 37,
            44: 44,
            46: 46,
            47: 47,
            49: 49,
            53: 53,
            58: 58,
            59: 59,
            61: 61,
            62: 62,
            7: 7,
            76: 76,
            79: 79,
            8: 8,
            9: 9
        }],
        55: [function(t, e, n) {
            function i(t) {
                s.apply(this, [t]);
                var e = this.params(),
                    n = e.count || this.dataAttr("count"),
                    i = e.size || this.dataAttr("size"),
                    r = u.getScreenNameFromPage(),
                    o = "" + (e.shareWithRetweet || this.dataAttr("share-with-retweet") || a.val("share-with-retweet"));
                this.classAttr.push("twitter-tweet-button"), "hashtag" == e.type || c.contains(this.classAttr, "twitter-hashtag-button") ? (this.type = "hashtag", this.classAttr.push("twitter-hashtag-button")) : "mention" == e.type || c.contains(this.classAttr, "twitter-mention-button") ? (this.type = "mention", this.classAttr.push("twitter-mention-button")) : this.classAttr.push("twitter-share-button"), this.text = e.text || this.dataAttr("text"), this.text && /\+/.test(this.text) && !/ /.test(this.text) && (this.text = this.text.replace(/\+/g, " ")), this.counturl = e.counturl || this.dataAttr("counturl"), this.searchlink = e.searchlink || this.dataAttr("searchlink"), this.button_hashtag = l.hashTag(e.button_hashtag || e.hashtag || this.dataAttr("button-hashtag"), !1), this.size = "large" == i ? "l" : "m", this.align = e.align || this.dataAttr("align") || "", this.via = e.via || this.dataAttr("via"), this.hashtags = e.hashtags || this.dataAttr("hashtags"), this.screen_name = l.screenName(e.screen_name || e.screenName || this.dataAttr("button-screen-name")), this.url = e.url || this.dataAttr("url"), this.type ? (this.count = "none", this.shareWithRetweet = "never", r && (this.related = this.related ? r + "," + this.related : r)) : (this.text = this.text || h, this.url = this.url || u.getCanonicalURL() || f, this.count = c.contains(p, n) ? n : "horizontal", this.via = this.via || r, o && c.contains(m, o) && (this.shareWithRetweet = o.replace("-", "_")))
            }
            var r = t(13),
                o = t(14),
                s = t(49),
                a = t(19),
                c = t(79),
                u = t(78),
                l = t(75),
                d = t(61),
                h = r.title,
                f = o.href,
                p = ["vertical", "horizontal", "none"],
                m = ["never", "publisher-first", "publisher-only", "author-first", "author-only"];
            i.prototype = new s, c.aug(i.prototype, {
                iframeSource: "widgets/tweet_button.55a4019ea66c5d005a6e6d9d41c5e068.{{lang}}.html",
                widgetUrlParams: function() {
                    return c.compact({
                        text: this.text,
                        url: this.url,
                        via: this.via,
                        related: this.related,
                        count: this.count,
                        lang: this.lang,
                        counturl: this.counturl,
                        searchlink: this.searchlink,
                        placeid: this.placeid,
                        original_referer: o.href,
                        id: this.id,
                        size: this.size,
                        type: this.type,
                        screen_name: this.screen_name,
                        share_with_retweet: this.shareWithRetweet,
                        button_hashtag: this.button_hashtag,
                        hashtags: this.hashtags,
                        align: this.align,
                        partner: this.partner,
                        dnt: d.enabled(),
                        _: +new Date
                    })
                },
                render: function() {
                    var t, e = this,
                        n = this.makeIframeSource();
                    return this.count && this.classAttr.push("twitter-count-" + this.count), t = this.create(n, {
                        title: "Twitter Tweet Button"
                    }).then(function(t) {
                        return e.element = t
                    })
                }
            }), e.exports = i
        }, {
            13: 13,
            14: 14,
            19: 19,
            49: 49,
            61: 61,
            75: 75,
            78: 78,
            79: 79
        }],
        56: [function(t, e, n) {
            function i(t, e, n, i) {
                b[t] = b[t] || [], b[t].push({
                    s: n,
                    f: i,
                    lang: e
                })
            }

            function r(t, e) {
                var n = {};
                n[t] = {
                    item_type: 0
                }, v.clientEvent({
                    page: "video",
                    component: "tweet",
                    action: "results"
                }, f.aug({}, e, {
                    item_ids: [t],
                    item_details: n
                }), !0), g.scribeVideoAudienceImpression()
            }

            function o(t, e) {
                var n = {};
                n[t] = {
                    item_type: 0
                }, v.clientEvent({
                    page: "video",
                    component: "rawembedcode",
                    action: "no_results"
                }, {
                    widget_origin: p.rootDocumentLocation(),
                    widget_frame: p.isFramed() && p.currentDocumentLocation(),
                    message: e,
                    item_ids: [t],
                    item_details: n
                }, !0), g.scribeVideoAudienceImpression()
            }

            function s(t) {
                if (t) {
                    u.apply(this, [t]);
                    var e = this.srcEl && this.srcEl.getElementsByTagName("A"),
                        n = e && e[e.length - 1],
                        i = this.params();
                    this.hideStatus = "hidden" === (i.status || this.dataAttr("status")), this.tweetId = i.tweetId || n && y.status(n.href)
                }
            }
            var a = t(3),
                c = t(49),
                u = t(53),
                l = t(7),
                d = t(71),
                h = t(72),
                f = t(79),
                p = t(59),
                m = t(44),
                g = t(34),
                v = t(36),
                y = t(75),
                w = t(13),
                b = {},
                _ = [];
            s.prototype = new u, f.aug(s.prototype, {
                renderedClassNames: "twitter-video twitter-video-rendered",
                videoPlayer: !0,
                dimensions: {
                    DEFAULT_HEIGHT: "0",
                    DEFAULT_WIDTH: "0",
                    maxHeight: "500",
                    MIN_WIDTH: "320",
                    MIN_HEIGHT: "180",
                    MARGIN: "10px 0",
                    WIDE_MEDIA_PADDING: 0,
                    NARROW_MEDIA_PADDING: 0,
                    BORDERS: 0
                },
                styleSheetUrl: a.video,
                applyVisibleSandboxStyles: function(t) {
                    return t.style({
                        visibility: "visible"
                    })
                },
                applyInitialSandboxStyles: function(t) {
                    return t.style({
                        position: "absolute",
                        top: 0,
                        left: 0,
                        width: "100%",
                        height: "100%",
                        visiblity: "hidden"
                    })
                },
                modifyFrame: function(t) {
                    return this.constrainingWrapper = w.createElement("div"), this.constrainingWrapper.style.minWidth = this.dimensions.MIN_WIDTH + "px", this.constrainingWrapper.style.margin = this.dimensions.MARGIN, this.wrapper = w.createElement("div"), this.wrapper.style.position = "relative", this.wrapper.style.height = 0, this.constrainingWrapper.appendChild(this.wrapper), this.wrapper.appendChild(t), this.constrainingWrapper
                },
                create: function(t) {
                    var e, n, i = this,
                        o = this.sandbox.createElement("div");
                    if (o.innerHTML = t, e = o.children[0]) {
                        n = e.children[0], this.playerConfig = JSON.parse(e.getAttribute("data-player-config")), this.sandbox.setTitle(e.getAttribute("data-iframe-title") || "Video"), this.sandbox.appendChild(e).then(function() {
                            i.renderedDeferred.resolve(i.sandbox), i.completeDeferred.resolve(i.sandbox.element())
                        });
                        var s = n.getAttribute("data-width"),
                            a = n.getAttribute("data-height"),
                            c = s / a,
                            u = 1 / c * 100 + "%";
                        return this.layout(function() {
                            i.wrapper.style.paddingBottom = u, i.constrainingWrapper.style.maxWidth = parseInt(i.dimensions.maxHeight, 10) * c + "px"
                        }), r(this.tweetId, this.baseScribeData()), e
                    }
                },
                render: function() {
                    var t = this;
                    return this.tweetId ? (this.rendered().then(function() {
                        var e = t.srcEl;
                        e && e.parentNode && t.layout(function() {
                            e.parentNode.removeChild(e)
                        })
                    }), i(this.tweetId, this.lang, function(e) {
                        t.ready().then(function() {
                            t.element = t.create(e), t.readTimestampTranslations(), t.writePlayerConfig()
                        })
                    }, function() {
                        o(t.tweetId, t.partner), t.completeDeferred.resolve(t.srcEl)
                    }), _.push(this.completed()), this.completed()) : (this.completeDeferred.resolve(this.srcEl), this.completed())
                },
                baseScribeData: function() {
                    return {
                        widget_origin: p.rootDocumentLocation(),
                        widget_frame: p.isFramed() && p.currentDocumentLocation(),
                        message: this.partner
                    }
                },
                readTimestampTranslations: function() {
                    var t = this.element,
                        e = "data-dt-",
                        n = t.getAttribute(e + "months") || "";
                    this.datetime = new l(f.compact({
                        phrases: {
                            AM: t.getAttribute(e + "am"),
                            PM: t.getAttribute(e + "pm")
                        },
                        months: n.split("|"),
                        formats: {
                            full: t.getAttribute(e + "full")
                        }
                    }))
                },
                getTimestamp: function() {
                    var t = this.element.getAttribute("data-datetime"),
                        e = t && this.datetime.localTimeStamp(t);
                    return {
                        local: e
                    }
                },
                writePlayerConfig: function() {
                    this.playerConfig.statusTimestamp = this.getTimestamp(), this.playerConfig.hideStatus = this.hideStatus, this.element.setAttribute("data-player-config", JSON.stringify(this.playerConfig))
                }
            }), s.fetchAndRender = function() {
                function t(t) {
                    f.forIn(t, function(t, n) {
                        var i = e[t];
                        i.forEach(function(t) {
                            t.s && t.s.call(this, n)
                        }), delete e[t]
                    }), f.forIn(e, function(t, e) {
                        e.forEach(function(e) {
                            e.f && e.f.call(this, t)
                        })
                    })
                }
                var e = b,
                    n = [];
                b = {};
                for (var i in e) e.hasOwnProperty(i) && n.push(i);
                n.length && (h.always(m.videos({
                    ids: n.sort(),
                    lang: e[n[0]][0].lang
                }), t), d.all(_), _ = [])
            }, c.afterLoad(s.fetchAndRender), e.exports = s
        }, {
            13: 13,
            3: 3,
            34: 34,
            36: 36,
            44: 44,
            49: 49,
            53: 53,
            59: 59,
            7: 7,
            71: 71,
            72: 72,
            75: 75,
            79: 79
        }],
        57: [function(t, e, n) {
            function i(t) {
                return c.parseInt(t, 16)
            }

            function r(t) {
                return u.isType("string", t) ? (t = t.replace(l, ""), t += 3 === t.length ? t : "") : null
            }

            function o(t, e) {
                var n, o, s, a;
                return t = r(t), e = e || 0, t ? (n = 0 > e ? 0 : 255, e = 0 > e ? -Math.max(e, -1) : Math.min(e, 1), o = i(t.substring(0, 2)), s = i(t.substring(2, 4)), a = i(t.substring(4, 6)), "#" + (16777216 + 65536 * (Math.round((n - o) * e) + o) + 256 * (Math.round((n - s) * e) + s) + (Math.round((n - a) * e) + a)).toString(16).slice(1)) : void 0
            }

            function s(t, e) {
                return o(t, -e)
            }

            function a(t, e) {
                return o(t, e)
            }
            var c = t(16),
                u = t(79),
                l = /^#/;
            e.exports = {
                darken: s,
                lighten: a
            }
        }, {
            16: 16,
            79: 79
        }],
        58: [function(t, e, n) {
            e.exports = {
                sanitize: function(t, e, n) {
                    var i, r = /^[\w ,%\/"'\-_#]+$/,
                        o = t && t.split(";").map(function(t) {
                            return t.split(":").slice(0, 2).map(function(t) {
                                return t.trim()
                            })
                        }),
                        s = 0,
                        a = [],
                        c = n ? "!important" : "";
                    for (e = e || /^(font|text\-|letter\-|color|line\-)[\w\-]*$/; o && (i = o[s]); s++) i[0].match(e) && i[1].match(r) && a.push(i.join(":") + c);
                    return a.join(";")
                }
            }
        }, {}],
        59: [function(t, e, n) {
            function i(t) {
                return t && c.isType("string", t) && (u = t), u
            }

            function r() {
                return l
            }

            function o() {
                return u !== l
            }
            var s = t(14),
                a = t(78),
                c = t(79),
                u = a.getCanonicalURL() || s.href,
                l = u;
            e.exports = {
                isFramed: o,
                rootDocumentLocation: i,
                currentDocumentLocation: r
            }
        }, {
            14: 14,
            78: 78,
            79: 79
        }],
        60: [function(t, e, n) {
            function i() {
                u = 1;
                for (var t = 0, e = l.length; e > t; t++) l[t]()
            }
            var r, o, s, a = t(13),
                c = t(16),
                u = 0,
                l = [],
                d = !1,
                h = a.createElement("a");
            /^loade|c/.test(a.readyState) && (u = 1), a.addEventListener && a.addEventListener("DOMContentLoaded", o = function() {
                a.removeEventListener("DOMContentLoaded", o, d), i()
            }, d), h.doScroll && a.attachEvent("onreadystatechange", r = function() {
                /^c/.test(a.readyState) && (a.detachEvent("onreadystatechange", r), i())
            }), s = h.doScroll ? function(t) {
                c.self != c.top ? u ? t() : l.push(t) : ! function() {
                    try {
                        h.doScroll("left")
                    } catch (e) {
                        return setTimeout(function() {
                            s(t)
                        }, 50)
                    }
                    t()
                }()
            } : function(t) {
                u ? t() : l.push(t)
            }, e.exports = s
        }, {
            13: 13,
            16: 16
        }],
        61: [function(t, e, n) {
            function i() {
                h = !0
            }

            function r(t, e) {
                return h ? !0 : l.asBoolean(d.val("dnt")) ? !0 : !a || 1 != a.doNotTrack && 1 != a.msDoNotTrack ? u.isUrlSensitive(e || s.host) ? !0 : c.isFramed() && u.isUrlSensitive(c.rootDocumentLocation()) ? !0 : (t = f.test(t || o.referrer) && RegExp.$1, t && u.isUrlSensitive(t) ? !0 : !1) : !0
            }
            var o = t(13),
                s = t(14),
                a = t(15),
                c = t(59),
                u = t(74),
                l = t(76),
                d = t(19),
                h = !1,
                f = /https?:\/\/([^\/]+).*/i;
            e.exports = {
                setOn: i,
                enabled: r
            }
        }, {
            13: 13,
            14: 14,
            15: 15,
            19: 19,
            59: 59,
            74: 74,
            76: 76
        }],
        62: [function(t, e, n) {
            function i(t) {
                return t = t || g, t.devicePixelRatio ? t.devicePixelRatio >= 1.5 : t.matchMedia ? t.matchMedia("only screen and (min-resolution: 144dpi)").matches : !1
            }

            function r(t) {
                return t = t || A, /(Trident|MSIE \d)/.test(t)
            }

            function o(t) {
                return t = t || A, /MSIE 9/.test(t)
            }

            function s(t) {
                return t = t || A, /(iPad|iPhone|iPod)/.test(t)
            }

            function a(t) {
                return t = t || A, /^Mozilla\/5\.0 \(Linux; (U; )?Android/.test(t)
            }

            function c() {
                return T
            }

            function u(t, e) {
                return t = t || g, e = e || A, t.postMessage && !(r(e) && t.opener)
            }

            function l(t) {
                t = t || m;
                try {
                    return !!t.plugins["Shockwave Flash"] || !!new ActiveXObject("ShockwaveFlash.ShockwaveFlash")
                } catch (e) {
                    return !1
                }
            }

            function d(t, e, n) {
                return t = t || g, e = e || m, n = n || A, "ontouchstart" in t || /Opera Mini/.test(n) || e.msMaxTouchPoints > 0
            }

            function h() {
                var t = p.body.style;
                return void 0 !== t.transition || void 0 !== t.webkitTransition || void 0 !== t.mozTransition || void 0 !== t.oTransition || void 0 !== t.msTransition
            }

            function f() {
                return !!(g.Promise && g.Promise.resolve && g.Promise.reject && g.Promise.all && g.Promise.race && function() {
                    var t;
                    return new g.Promise(function(e) {
                        t = e
                    }), b.isType("function", t)
                }())
            }
            var p = t(13),
                m = t(15),
                g = t(16),
                v = t(60),
                y = t(67),
                w = t(76),
                b = t(79),
                _ = t(19),
                E = t(20),
                A = m.userAgent,
                T = !1,
                x = !1,
                I = "twitter-csp-test";
            E.set("verifyCSP", function(t) {
                var e = p.getElementById(I);
                x = !0, T = !!t, e && e.parentNode.removeChild(e)
            }), v(function() {
                var t;
                return w.asBoolean(_.val("widgets:csp")) ? T = !0 : (t = p.createElement("script"), t.id = I, t.text = E.fullPath("verifyCSP") + "(false);", p.body.appendChild(t), void g.setTimeout(function() {
                    x || (y.warn('TWITTER: Content Security Policy restrictions may be applied to your site. Add <meta name="twitter:widgets:csp" content="on"> to supress this warning.'), y.warn("TWITTER: Please note: Not all embedded timeline and embedded Tweet functionality is supported when CSP is applied."))
                }, 5e3))
            }), e.exports = {
                retina: i,
                anyIE: r,
                ie9: o,
                ios: s,
                android: a,
                cspEnabled: c,
                flashEnabled: l,
                canPostMessage: u,
                touch: d,
                cssTransitions: h,
                hasPromiseSupport: f
            }
        }, {
            13: 13,
            15: 15,
            16: 16,
            19: 19,
            20: 20,
            60: 60,
            67: 67,
            76: 76,
            79: 79
        }],
        63: [function(t, e, n) {
            var i = t(79),
                r = {
                    bind: function(t, e) {
                        return this._handlers = this._handlers || {}, this._handlers[t] = this._handlers[t] || [], this._handlers[t].push(e)
                    },
                    unbind: function(t, e) {
                        if (this._handlers[t])
                            if (e) {
                                var n = this._handlers[t].indexOf(e);
                                n >= 0 && this._handlers[t].splice(n, 1)
                            } else this._handlers[t] = []
                    },
                    trigger: function(t, e) {
                        var n = this._handlers && this._handlers[t];
                        e = e || {}, e.type = t, n && n.forEach(function(t) {
                            i.async(t.bind(this, e))
                        })
                    }
                };
            e.exports = {
                Emitter: r
            }
        }, {
            79: 79
        }],
        64: [function(t, e, n) {
            function i(t) {
                for (var e, n = r.getElementsByTagName("iframe"), i = 0; e = n[i]; i++)
                    if (e.contentWindow === t) return e
            }
            var r = t(13);
            e.exports = i
        }, {
            13: 13
        }],
        65: [function(t, e, n) {
            var i = t(13),
                r = t(79);
            e.exports = function(t, e, n) {
                var o;
                if (n = n || i, t = t || {}, e = e || {}, t.name) {
                    try {
                        o = n.createElement('<iframe name="' + t.name + '"></iframe>')
                    } catch (s) {
                        o = n.createElement("iframe"), o.name = t.name
                    }
                    delete t.name
                } else o = n.createElement("iframe");
                return t.id && (o.id = t.id, delete t.id), o.allowtransparency = "true", o.scrolling = "no", o.setAttribute("frameBorder", 0), o.setAttribute("allowTransparency", !0), r.forIn(t, function(t, e) {
                    o.setAttribute(t, e)
                }), r.forIn(e, function(t, e) {
                    o.style[t] = e
                }), o
            }
        }, {
            13: 13,
            79: 79
        }],
        66: [function(t, e, n) {
            function i() {}
            var r, o = t(16),
                s = t(70),
                a = [];
            i.prototype.enqueue = function(t, e) {
                var n = new s;
                return a.push({
                    action: t,
                    deferred: n,
                    note: e
                }), n.promise
            }, i.prototype.exec = function() {
                var t, e = a;
                if (e.length)
                    for (a = []; e.length;) t = e.shift(), t && t.action ? t.deferred.resolve(t.action()) : t.deferred.reject()
            }, i.prototype.delayedExec = function() {
                r && o.clearTimeout(r), r = o.setTimeout(this.exec, 100)
            }, e.exports = i
        }, {
            16: 16,
            70: 70
        }],
        67: [function(t, e, n) {
            function i() {
                u("info", h.toRealArray(arguments))
            }

            function r() {
                u("warn", h.toRealArray(arguments))
            }

            function o() {
                u("error", h.toRealArray(arguments))
            }

            function s(t) {
                m && (p[t] = c())
            }

            function a(t) {
                var e;
                m && (p[t] ? (e = c(), i("_twitter", t, e - p[t])) : o("timeEnd() called before time() for id: ", t))
            }

            function c() {
                return d.performance && +d.performance.now() || +new Date
            }

            function u(t, e) {
                if (d[f] && d[f][t]) switch (e.length) {
                    case 1:
                        d[f][t](e[0]);
                        break;
                    case 2:
                        d[f][t](e[0], e[1]);
                        break;
                    case 3:
                        d[f][t](e[0], e[1], e[2]);
                        break;
                    case 4:
                        d[f][t](e[0], e[1], e[2], e[3]);
                        break;
                    case 5:
                        d[f][t](e[0], e[1], e[2], e[3], e[4]);
                        break;
                    default:
                        0 !== e.length && d[f].warn && d[f].warn("too many params passed to logger." + t)
                }
            }
            var l = t(14),
                d = t(16),
                h = t(79),
                f = ["con", "sole"].join(""),
                p = {},
                m = h.contains(l.href, "tw_debug=true");
            e.exports = {
                info: i,
                warn: r,
                error: o,
                time: s,
                timeEnd: a
            }
        }, {
            14: 14,
            16: 16,
            79: 79
        }],
        68: [function(t, e, n) {
            function i(t) {
                return function(e) {
                    return o.hasValue(e[t])
                }
            }

            function r() {
                this.assertions = [], this._defaults = {}
            }
            var o = t(76),
                s = t(79);
            r.prototype.assert = function(t, e) {
                return this.assertions.push({
                    fn: t,
                    msg: e || "assertion failed"
                }), this
            }, r.prototype.defaults = function(t) {
                return this._defaults = t || this._defaults, this
            }, r.prototype.require = function(t) {
                var e = this;
                return t = Array.isArray(t) ? t : s.toRealArray(arguments), t.forEach(function(t) {
                    e.assert(i(t), "required: " + t)
                }), this
            }, r.prototype.parse = function(t) {
                var e, n;
                if (e = s.aug({}, this._defaults, t || {}), n = this.assertions.reduce(function(t, n) {
                        return n.fn(e) || t.push(n.msg), t
                    }, []), n.length > 0) throw new Error(n.join("\n"));
                return e
            }, e.exports = r
        }, {
            76: 76,
            79: 79
        }],
        69: [function(t, e, n) {
            var i, r, o, s = t(73);
            i = function(t) {
                var e = t.search.substr(1);
                return s.decode(e)
            }, r = function(t) {
                var e = t.href,
                    n = e.indexOf("#"),
                    i = 0 > n ? "" : e.substring(n + 1);
                return s.decode(i)
            }, o = function(t) {
                var e, n = {},
                    o = i(t),
                    s = r(t);
                for (e in o) o.hasOwnProperty(e) && (n[e] = o[e]);
                for (e in s) s.hasOwnProperty(e) && (n[e] = s[e]);
                return n
            }, e.exports = {
                combined: o,
                fromQuery: i,
                fromFragment: r
            }
        }, {
            73: 73
        }],
        70: [function(t, e, n) {
            function i() {
                var t = this;
                this.promise = new r(function(e, n) {
                    t.resolve = e, t.reject = n
                })
            }
            var r = t(71);
            e.exports = i
        }, {
            71: 71
        }],
        71: [function(t, e, n) {
            var i = t(1).Promise,
                r = t(16),
                o = t(62);
            e.exports = o.hasPromiseSupport() ? r.Promise : i
        }, {
            1: 1,
            16: 16,
            62: 62
        }],
        72: [function(t, e, n) {
            function i(t, e) {
                return t.then(e, e)
            }

            function r(t) {
                var e;
                return t = t || [], e = t.length, t = t.filter(o), e ? e !== t.length ? s.reject("non-Promise passed to .some") : new s(function(e, n) {
                    function i() {
                        r += 1, r === t.length && n()
                    }
                    var r = 0;
                    t.forEach(function(t) {
                        t.then(e, i)
                    })
                }) : s.reject("no promises passed to .some")
            }

            function o(t) {
                return t instanceof s
            }
            var s = t(71);
            e.exports = {
                always: i,
                some: r,
                isPromise: o
            }
        }, {
            71: 71
        }],
        73: [function(t, e, n) {
            function i(t) {
                return encodeURIComponent(t).replace(/\+/g, "%2B").replace(/'/g, "%27")
            }

            function r(t) {
                return decodeURIComponent(t)
            }

            function o(t) {
                var e = [];
                return l.forIn(t, function(t, n) {
                    var r = i(t);
                    l.isType("array", n) || (n = [n]), n.forEach(function(t) {
                        u.hasValue(t) && e.push(r + "=" + i(t))
                    })
                }), e.sort().join("&")
            }

            function s(t) {
                var e, n = {};
                return t ? (e = t.split("&"), e.forEach(function(t) {
                    var e = t.split("="),
                        i = r(e[0]),
                        o = r(e[1]);
                    return 2 == e.length ? l.isType("array", n[i]) ? void n[i].push(o) : i in n ? (n[i] = [n[i]], void n[i].push(o)) : void(n[i] = o) : void 0
                }), n) : {}
            }

            function a(t, e) {
                var n = o(e);
                return n.length > 0 ? l.contains(t, "?") ? t + "&" + o(e) : t + "?" + o(e) : t
            }

            function c(t) {
                var e = t && t.split("?");
                return 2 == e.length ? s(e[1]) : {}
            }
            var u = t(76),
                l = t(79);
            e.exports = {
                url: a,
                decodeURL: c,
                decode: s,
                encode: o,
                encodePart: i,
                decodePart: r
            }
        }, {
            76: 76,
            79: 79
        }],
        74: [function(t, e, n) {
            function i(t) {
                return t in a ? a[t] : a[t] = s.test(t)
            }

            function r() {
                return i(o.host)
            }
            var o = t(14),
                s = /^[^#?]*\.(gov|mil)(:\d+)?([#?].*)?$/i,
                a = {};
            e.exports = {
                isUrlSensitive: i,
                isHostPageSensitive: r
            }
        }, {
            14: 14
        }],
        75: [function(t, e, n) {
            function i(t) {
                return "string" == typeof t && m.test(t) && RegExp.$1.length <= 20
            }

            function r(t) {
                return i(t) ? RegExp.$1 : void 0
            }

            function o(t, e) {
                var n = p.decodeURL(t);
                return e = e || !1, n.screen_name = r(t), n.screen_name ? p.url("https://twitter.com/intent/" + (e ? "follow" : "user"), n) : void 0
            }

            function s(t) {
                return o(t, !0)
            }

            function a(t) {
                return "string" == typeof t && w.test(t)
            }

            function c(t, e) {
                return e = void 0 === e ? !0 : e, a(t) ? (e ? "#" : "") + RegExp.$1 : void 0
            }

            function u(t) {
                return "string" == typeof t && g.test(t)
            }

            function l(t) {
                return u(t) && RegExp.$1
            }

            function d(t) {
                return v.test(t)
            }

            function h(t) {
                return y.test(t)
            }

            function f(t) {
                return b.test(t)
            }
            var p = t(73),
                m = /(?:^|(?:https?\:)?\/\/(?:www\.)?twitter\.com(?:\:\d+)?(?:\/intent\/(?:follow|user)\/?\?screen_name=|(?:\/#!)?\/))@?([\w]+)(?:\?|&|$)/i,
                g = /(?:^|(?:https?\:)?\/\/(?:www\.)?twitter\.com(?:\:\d+)?\/(?:#!\/)?[\w_]+\/status(?:es)?\/)(\d+)/i,
                v = /^http(s?):\/\/(\w+\.)*twitter\.com([\:\/]|$)/i,
                y = /^http(s?):\/\/pbs\.twimg\.com\//,
                w = /^#?([^.,<>!\s\/#\-\(\)\'\"]+)$/,
                b = /twitter\.com(\:\d{2,4})?\/intent\/(\w+)/;
            e.exports = {
                isHashTag: a,
                hashTag: c,
                isScreenName: i,
                screenName: r,
                isStatus: u,
                status: l,
                intentForProfileURL: o,
                intentForFollowURL: s,
                isTwitterURL: d,
                isTwimgURL: h,
                isIntentURL: f,
                regexen: {
                    profile: m
                }
            }
        }, {
            73: 73
        }],
        76: [function(t, e, n) {
            function i(t) {
                return void 0 !== t && null !== t && "" !== t
            }

            function r(t) {
                return s(t) && t % 1 === 0
            }

            function o(t) {
                return s(t) && !r(t)
            }

            function s(t) {
                return i(t) && !isNaN(t)
            }

            function a(t) {
                return i(t) && "array" == p.toType(t)
            }

            function c(t) {
                return p.contains(g, t)
            }

            function u(t) {
                return p.contains(m, t)
            }

            function l(t) {
                return i(t) ? u(t) ? !0 : c(t) ? !1 : !!t : !1
            }

            function d(t) {
                return s(t) ? t : void 0
            }

            function h(t) {
                return o(t) ? t : void 0
            }

            function f(t) {
                return r(t) ? t : void 0
            }
            var p = t(79),
                m = [!0, 1, "1", "on", "ON", "true", "TRUE", "yes", "YES"],
                g = [!1, 0, "0", "off", "OFF", "false", "FALSE", "no", "NO"];
            e.exports = {
                hasValue: i,
                isInt: r,
                isFloat: o,
                isNumber: s,
                isArray: a,
                isTruthValue: u,
                isFalseValue: c,
                asInt: f,
                asFloat: h,
                asNumber: d,
                asBoolean: l
            }
        }, {
            79: 79
        }],
        77: [function(t, e, n) {
            function i() {
                return String(+new Date) + Math.floor(1e5 * Math.random()) + r++
            }
            var r = 0;
            e.exports = {
                generate: i
            }
        }, {}],
        78: [function(t, e, n) {
            function i(t, e) {
                var n, i;
                return e = e || a, /^https?:\/\//.test(t) ? t : /^\/\//.test(t) ? e.protocol + t : (n = e.host + (e.port.length ? ":" + e.port : ""), 0 !== t.indexOf("/") && (i = e.pathname.split("/"), i.pop(), i.push(t), t = "/" + i.join("/")), [e.protocol, "//", n, t].join(""))
            }

            function r() {
                for (var t, e = s.getElementsByTagName("link"), n = 0; t = e[n]; n++)
                    if ("canonical" == t.rel) return i(t.href)
            }

            function o() {
                for (var t, e, n, i = s.getElementsByTagName("a"), r = s.getElementsByTagName("link"), o = [i, r], a = 0, u = 0, l = /\bme\b/; t = o[a]; a++)
                    for (u = 0; e = t[u]; u++)
                        if (l.test(e.rel) && (n = c.screenName(e.href))) return n
            }
            var s = t(13),
                a = t(14),
                c = t(75);
            e.exports = {
                absolutize: i,
                getCanonicalURL: r,
                getScreenNameFromPage: o
            }
        }, {
            13: 13,
            14: 14,
            75: 75
        }],
        79: [function(t, e, n) {
            function i(t) {
                return d(arguments).slice(1).forEach(function(e) {
                    o(e, function(e, n) {
                        t[e] = n
                    })
                }), t
            }

            function r(t) {
                return o(t, function(e, n) {
                    c(n) && (r(n), u(n) && delete t[e]), (void 0 === n || null === n || "" === n) && delete t[e]
                }), t
            }

            function o(t, e) {
                for (var n in t)(!t.hasOwnProperty || t.hasOwnProperty(n)) && e(n, t[n]);
                return t
            }

            function s(t) {
                return {}.toString.call(t).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
            }

            function a(t, e) {
                return t == s(e)
            }

            function c(t) {
                return t === Object(t)
            }

            function u(t) {
                if (!c(t)) return !1;
                if (Object.keys) return !Object.keys(t).length;
                for (var e in t)
                    if (t.hasOwnProperty(e)) return !1;
                return !0
            }

            function l(t, e) {
                f.setTimeout(function() {
                    t.call(e || null)
                }, 0)
            }

            function d(t) {
                return Array.prototype.slice.call(t)
            }

            function h(t, e) {
                return t && t.indexOf ? t.indexOf(e) > -1 : !1
            }
            var f = t(16);
            e.exports = {
                aug: i,
                async: l,
                compact: r,
                contains: h,
                forIn: o,
                isObject: c,
                isEmptyObject: u,
                toType: s,
                isType: a,
                toRealArray: d
            }
        }, {
            16: 16
        }],
        80: [function(t, e, n) {
            function i() {
                if (o) return o;
                if (u.isDynamicWidget()) {
                    var t, e = 0,
                        n = c.parent.frames.length;
                    try {
                        if (o = c.parent.frames[h]) return o
                    } catch (i) {}
                    if (l.anyIE())
                        for (; n > e; e++) try {
                            if (t = c.parent.frames[e], t && "function" == typeof t.openIntent) return o = t
                        } catch (i) {}
                }
            }

            function r() {
                var t, e, n, o, a, l, d = {};
                if ("function" === (typeof arguments[0]).toLowerCase() ? d.success = arguments[0] : d = arguments[0], t = d.success || function() {}, e = d.timeout || function() {}, n = d.nohub || function() {}, o = d.complete || function() {}, a = void 0 !== d.attempt ? d.attempt : m, !u.isDynamicWidget() || s) return n(), o(), !1;
                l = i(), a--;
                try {
                    if (l && l.trigger) return t(l), void o()
                } catch (h) {}
                return 0 >= a ? (s = !0, e(), void o()) : +new Date - f > p * m ? (s = !0, void n()) : void c.setTimeout(function() {
                    r({
                        success: t,
                        timeout: e,
                        nohub: n,
                        attempt: a,
                        complete: o
                    })
                }, p)
            }
            var o, s, a = t(14),
                c = t(16),
                u = t(45),
                l = t(62),
                d = "twttrHubFrameSecure",
                h = "http:" == a.protocol ? "twttrHubFrame" : d,
                f = +new Date,
                p = 100,
                m = 20;
            e.exports = {
                withHub: r,
                contextualHubId: h,
                secureHubId: d
            }
        }, {
            14: 14,
            16: 16,
            45: 45,
            62: 62
        }],
        81: [function(t, e, n) {
            e.exports = {
                version: "86e7da0909e9ac95c583604466357eaa29354a5e:1435181106333"
            }
        }, {}],
        82: [function(t, e, n) {
            e.exports = {
                css: "bdd2a0d299634c64db074e7c7b24c394"
            }
        }, {}],
        83: [function(t, e, n) {
            function i(t) {
                t = r.parse(t), this.rootEl = t.rootEl, this.videoEl = t.videoEl, this.playButtonEl = t.playButtonEl, this.fallbackUrl = t.fallbackUrl, this.player = new u({
                    videoEl: this.videoEl,
                    loop: !0,
                    autoplay: !1
                }), this._attachClickListener()
            }
            var r, o = t(8),
                s = t(9),
                a = t(16),
                c = t(68),
                u = t(84);
            r = (new c).require("rootEl", "videoEl", "playButtonEl").defaults({
                fallbackUrl: null
            }), i.prototype._attachClickListener = function() {
                function t(t) {
                    s.stopPropagation(t), e._togglePlayer()
                }
                var e = this;
                this.videoEl.addEventListener("click", t, !1), this.playButtonEl.addEventListener("click", t, !1)
            }, i.prototype._togglePlayer = function() {
                return this.player.hasPlayableSource() ? (this.player.toggle(), void o.toggle(this.rootEl, "is-playing", !this.player.isPaused())) : void(this.fallbackUrl && a.open(this.fallbackUrl))
            }, e.exports = i
        }, {
            16: 16,
            68: 68,
            8: 8,
            84: 84,
            9: 9
        }],
        84: [function(t, e, n) {
            function i(t) {
                var e;
                t = r.parse(t), this.videoEl = t.videoEl, "loop" in t && (this.videoEl.loop = t.loop), "autoplay" in t && (this.videoEl.autoplay = t.autoplay), "poster" in t && (this.videoEl.poster = t.poster), e = a.toRealArray(this.videoEl.querySelectorAll("source")), this.sourceTypes = e.map(function(t) {
                    return t.type
                })
            }
            var r, o = t(13),
                s = t(68),
                a = t(79);
            r = (new s).require("videoEl"), i.prototype.isPaused = function() {
                return this.videoEl.paused
            }, i.prototype.play = function() {
                return this.videoEl.play(), this
            }, i.prototype.pause = function() {
                return this.videoEl.pause(), this
            }, i.prototype.toggle = function() {
                return this.videoEl.paused ? this.play() : this.pause()
            }, i.prototype.addSource = function(t, e) {
                var n = o.createElement("source");
                return n.src = t, n.type = e, this.sourceTypes.push(e), this.videoEl.appendChild(n), this
            }, i.prototype.hasPlayableSource = function() {
                var t = this.videoEl;
                return t.canPlayType ? this.sourceTypes.reduce(function(e, n) {
                    return e || !!t.canPlayType(n).replace("no", "")
                }, !1) : !1
            }, i.prototype.setDimensions = function(t, e) {
                return this.videoEl.width = t, this.videoEl.height = e, this
            }, e.exports = i
        }, {
            13: 13,
            68: 68,
            79: 79
        }],
        85: [function(t, e, n) {
            function i(t, e) {
                return t && t.getAttribute ? t.getAttribute("data-" + e) : void 0
            }

            function r(t, e) {
                return {
                    element: t.element || v,
                    action: t.action || y,
                    page: o(e) ? "video" : void 0
                }
            }

            function o(t) {
                return d.closest(".embedded-video", t)
            }

            function s(t) {
                var e = d.closest(".tweet", t),
                    n = !e && d.closest(".EmbeddedTweet", t);
                return n && (e = n.querySelector(".tweet.subject")), e
            }

            function a(t) {
                return JSON.parse(i(o(t), "player-config"))
            }

            function c(t, e) {
                var n, r, a, c = o(e);
                return c ? n = l.aug({
                    item_type: m,
                    card_type: g,
                    id: i(c, "tweet-id"),
                    card_name: i(c, "card-name"),
                    publisher_id: i(c, "publisher-id"),
                    content_id: i(c, "content-id")
                }, t.itemData || {}) : (r = d.closest(".cards-multimedia", e), a = s(e), n = l.aug({
                    item_type: m,
                    card_type: g,
                    id: i(a, "tweet-id"),
                    card_name: i(r, "card-name"),
                    publisher_id: i(r, "publisher-id"),
                    content_id: i(r, "video-content-id")
                }, t.itemData || {})), {
                    items: [n]
                }
            }

            function u(t) {
                var e = this;
                this.global = t, this.server = (new h).attachReceiver(new p.Receiver(t, "")).bind("scribe", function(t) {
                    e.scribe(t, this)
                }).bind("requestPlayerConfig", function() {
                    return e.requestPlayerConfig(this)
                })
            }
            var l = t(79),
                d = t(10),
                h = t(29),
                f = t(38),
                p = t(30),
                m = 0,
                g = 6,
                v = "amplify_player",
                y = "undefined";
            u.prototype.findIframeByWindow = function(t) {
                for (var e = this.global.document.getElementsByTagName("iframe"), n = e.length, i = 0; n > i; i++)
                    if (e[i].contentWindow == t) return e[i]
            }, u.prototype.requestPlayerConfig = function(t) {
                var e = this.findIframeByWindow(t);
                return e ? a(e) : void 0
            }, u.prototype.scribe = function(t, e) {
                var n, i, o, s;
                n = t && t.customScribe, i = this.findIframeByWindow(e), n && i && (o = r(n, i), s = c(n, i), f.clientEvent2(o, s, !0))
            }, e.exports = u
        }, {
            10: 10,
            29: 29,
            30: 30,
            38: 38,
            79: 79
        }],
        86: [function(t, e, n) {
            ! function() {
                var e = t(13),
                    n = t(42),
                    i = t(60),
                    r = t(67),
                    o = t(26),
                    s = t(49),
                    a = t(51),
                    c = t(55),
                    u = t(50),
                    l = t(54),
                    d = t(56),
                    h = t(52),
                    f = t(40),
                    p = t(63),
                    m = t(41),
                    g = t(24),
                    v = t(21),
                    y = t(20),
                    w = t(17),
                    b = t(70),
                    _ = t(48);
                if (y.init("host", "platform.twitter.com"), o.start("widgets-js-load"), n.requestArticleUrl(), _(function(t, e, n) {
                        var i = t && s.findInstance(t);
                        i && i.setInitialSize(e, n)
                    }), y.get("widgets.loaded")) return v.call("widgets.load"), !1;
                if (y.get("widgets.init")) return !1;
                y.set("widgets.init", !0), v.set("init", !0);
                var E = new b;
                w.exposeReadyPromise(E.promise, v.base, "_e"), v.set("events", {
                    bind: function(t, e) {
                        E.promise.then(function(n) {
                            n.events.bind(t, e)
                        })
                    }
                }), i(function() {
                    function t() {
                        y.set("eventsHub", m.init()), m.init(!0)
                    }
                    var n, i = {
                            "a.twitter-share-button": c,
                            "a.twitter-mention-button": c,
                            "a.twitter-hashtag-button": c,
                            "a.twitter-follow-button": a,
                            "blockquote.twitter-tweet": u,
                            "a.twitter-timeline": l,
                            "div.twitter-timeline": l,
                            "blockquote.twitter-video": d,
                            body: h
                        },
                        o = y.get("eventsHub") ? v.get("events") : {};
                    v.aug("widgets", f, {
                        load: function(t) {
                            r.time("load"), s.init(i), s.embed(t), y.set("widgets.loaded", !0)
                        }
                    }), v.aug("events", o, p.Emitter), n = v.get("events.bind"), v.set("events.bind", function(e, i) {
                        t(), this.bind = n, this.bind(e, i)
                    }), E.resolve(v.base), g.attachTo(e), v.call("widgets.load")
                })
            }()
        }, {
            13: 13,
            17: 17,
            20: 20,
            21: 21,
            24: 24,
            26: 26,
            40: 40,
            41: 41,
            42: 42,
            48: 48,
            49: 49,
            50: 50,
            51: 51,
            52: 52,
            54: 54,
            55: 55,
            56: 56,
            60: 60,
            63: 63,
            67: 67,
            70: 70
        }],
        87: [function(t, e, n) {
            function i() {}
            var r = t(79),
                o = t(63);
            r.aug(i.prototype, o.Emitter, {
                transportMethod: "",
                init: function() {},
                send: function(t) {
                    var e;
                    this._ready ? this._performSend(t) : e = this.bind("ready", function() {
                        this.unbind("ready", e), this._performSend(t)
                    })
                },
                ready: function() {
                    this.trigger("ready", this), this._ready = !0
                },
                isReady: function() {
                    return !!this._ready
                },
                receive: function(t) {
                    this.trigger("message", t)
                }
            }), e.exports = {
                Connection: i
            }
        }, {
            63: 63,
            79: 79
        }],
        88: [function(t, e, n) {
            function i(t, e) {
                var n = e || Math.floor(100 * Math.random()),
                    i = ['<object id="xdflashshim' + n + '" name="xdflashshim' + n + '"', 'type="application/x-shockwave-flash" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"', 'width="1" height="1" style="position:absolute;left:-9999px;top:-9999px;">', '<param name="movie" value="' + t + "&debug=" + r.__XDDEBUG__ + '">', '<param name="wmode" value="window">', '<param name="allowscriptaccess" value="always">', "</object>"].join(" ");
                return i
            }
            var r = t(16);
            e.exports = {
                object: i
            }
        }, {
            16: 16
        }],
        89: [function(t, e, n) {
            function i(t) {
                return (JSON.parse || JSON.decode)(t)
            }

            function r(t) {
                this.con = t
            }

            function o() {
                this.id = o.id++
            }
            var s = t(79),
                a = t(63);
            s.aug(r.prototype, {
                expose: function(t) {
                    this.con.bind("message", this._handleRequest(t))
                },
                call: function(t) {
                    var e, n = this;
                    return this._requests || (this._requests = {}, this.con.bind("message", function(t) {
                        var e;
                        try {
                            t = i(t)
                        } catch (r) {
                            return
                        }
                        t.callback && "number" == typeof t.id && (e = n._requests[t.id]) && (t.error ? e.trigger("error", t) : e.trigger("success", t), delete n._requests[t.id])
                    })), e = new o, this._requests[e.id] = e, e.send(this.con, t, Array.prototype.slice.call(arguments, 1))
                },
                _handleRequest: function(t) {
                    var e = this;
                    return function(n) {
                        var r, o;
                        try {
                            n = i(n)
                        } catch (s) {
                            return
                        }
                        n.callback || "number" == typeof n.id && "function" == typeof t[n.method] && (o = e._responseCallbacks(n.id), r = t[n.method].apply(t, n.params.concat(o)), "undefined" != typeof r && o[0](r))
                    }
                },
                _responseCallbacks: function(t) {
                    var e = this.con;
                    return [function(n) {
                        e.send(JSON.stringify({
                            id: t,
                            result: n,
                            callback: !0
                        }))
                    }, function n(i) {
                        e.send(JSON.stringify({
                            id: t,
                            error: n,
                            callback: i
                        }))
                    }]
                }
            }), o.id = 0, s.aug(o.prototype, a.Emitter, {
                send: function(t, e, n) {
                    return t.send(JSON.stringify({
                        id: this.id,
                        method: e,
                        params: n
                    })), this
                },
                success: function(t) {
                    return this.bind("success", t), this
                },
                error: function(t) {
                    return this.bind("error", t), this
                }
            }), e.exports = function(t) {
                return new r(t)
            }
        }, {
            63: 63,
            79: 79
        }],
        90: [function(t, e, n) {
            function i() {}

            function r(t) {
                this.transportMethod = "PostMessage", this.options = t, this._createChild()
            }

            function o(t) {
                this.transportMethod = "Flash", this.options = t, this.token = Math.random().toString(16).substring(2), this._setup()
            }

            function s(t) {
                this.transportMethod = "Fallback", this.options = t, this._createChild()
            }
            var a, c = t(13),
                u = t(16),
                l = t(87),
                d = t(79),
                h = t(62),
                f = t(25),
                p = "__ready__",
                m = 0;
            i.prototype = new l.Connection, d.aug(i.prototype, {
                _createChild: function() {
                    this.options.window ? this._createWindow() : this._createIframe()
                },
                _createIframe: function() {
                    function t() {
                        o.child = e.contentWindow, o._ready || o.init()
                    }
                    var e, n, i, r, o = this,
                        s = {
                            allowTransparency: !0,
                            frameBorder: "0",
                            scrolling: "no",
                            tabIndex: "0",
                            name: this._name()
                        },
                        l = d.aug(d.aug({}, s), this.options.iframe);
                    u.postMessage ? (a || (a = c.createElement("iframe")), e = a.cloneNode(!1)) : e = c.createElement('<iframe name="' + l.name + '">'), e.id = l.name, d.forIn(l, function(t, n) {
                        "style" != t && e.setAttribute(t, n)
                    }), r = e.getAttribute("style"), r && "undefined" != typeof r.cssText ? r.cssText = l.style : e.style.cssText = l.style, e.addEventListener("load", t, !1), e.src = this._source(), (n = this.options.appendTo) ? n.appendChild(e) : (i = this.options.replace) ? (n = i.parentNode, n && n.replaceChild(e, i)) : c.body.insertBefore(e, c.body.firstChild)
                },
                _createWindow: function() {
                    var t = f.open(this._source()).popup;
                    t && t.focus(), this.child = t, this.init()
                },
                _source: function() {
                    return this.options.src
                },
                _name: function() {
                    var t = "_xd_" + m++;
                    return u.parent && u.parent != u && u.name && (t = u.name + t), t
                }
            }), r.prototype = new i, d.aug(r.prototype, {
                init: function() {
                    function t(t) {
                        t.source === e.child && (e._ready || t.data !== p ? e.receive(t.data) : e.ready())
                    }
                    var e = this;
                    u.addEventListener("message", t, !1)
                },
                _performSend: function(t) {
                    this.child.postMessage(t, this.options.src)
                }
            }), o.prototype = new i, d.aug(o.prototype, {
                _setup: function() {
                    var e = this,
                        n = t(88);
                    u["__xdcb" + e.token] = {
                        receive: function(t) {
                            e._ready || t !== p ? e.receive(t) : e.ready()
                        },
                        loaded: function() {}
                    };
                    var i = c.createElement("div");
                    i.innerHTML = n.object("https://platform.twitter.com/xd/ft.swf?&token=" + e.token + "&parent=true&callback=__xdcb" + e.token + "&xdomain=" + e._host(), e.token), c.body.insertBefore(i, c.body.firstChild), e.proxy = i.firstChild, e._createChild()
                },
                init: function() {},
                _performSend: function(t) {
                    this.proxy.send(t)
                },
                _host: function() {
                    return this.options.src.replace(/https?:\/\//, "").split(/(:|\/)/)[0]
                },
                _source: function() {
                    return this.options.src + (this.options.src.match(/\?/) ? "&" : "?") + "xd_token=" + u.escape(this.token)
                }
            }), s.prototype = new i, d.aug(s.prototype, {
                init: function() {},
                _performSend: function() {}
            }), e.exports = {
                connect: function(t) {
                    return !h.canPostMessage() || h.anyIE() && t.window ? h.anyIE() && h.flashEnabled() ? new o(t) : new s(t) : new r(t)
                }
            }
        }, {
            13: 13,
            16: 16,
            25: 25,
            62: 62,
            79: 79,
            87: 87,
            88: 88
        }]
    }, {}, [86]))
}(),
function() {
    function m() {
        return function() {}
    }

    function n(a) {
        return function() {
            return this[a]
        }
    }

    function p(a) {
        return function() {
            return a
        }
    }

    function t(a, c, d) {
        if ("string" == typeof a) {
            if (0 === a.indexOf("#") && (a = a.slice(1)), t.Da[a]) return c && t.log.warn('Player "' + a + '" is already initialised. Options will not be applied.'), d && t.Da[a].I(d), t.Da[a];
            a = t.m(a)
        }
        if (!a || !a.nodeName) throw new TypeError("The element or ID supplied is not valid. (videojs)");
        return a.player || new t.Player(a, c, d)
    }

    function v(a, c, d, e) {
        t.xc.forEach(d, function(d) {
            a(c, d, e)
        })
    }

    function F(a, c) {
        var d, e;
        d = Array.prototype.slice.call(c), e = m(), e = window.console || {
            log: e,
            warn: e,
            error: e
        }, a ? d.unshift(a.toUpperCase() + ":") : a = "log", t.log.history.push(d), d.unshift("VIDEOJS:"), e[a].apply ? e[a].apply(e, d) : e[a](d.join(" "))
    }

    function G(a) {
        a.r("vjs-lock-showing")
    }

    function ca(a, c, d, e) {
        return d !== b ? ((d === j || t.le(d)) && (d = 0), a.c.style[c] = -1 !== ("" + d).indexOf("%") || -1 !== ("" + d).indexOf("px") ? d : "auto" === d ? "" : d + "px", e || a.o("resize"), a) : a.c ? (d = a.c.style[c], e = d.indexOf("px"), -1 !== e ? parseInt(d.slice(0, e), 10) : parseInt(a.c["offset" + t.wa(c)], 10)) : 0
    }

    function da(a) {
        var c, d, e, g, h, k, q, r;
        c = 0, d = j, a.b("touchstart", function(a) {
            1 === a.touches.length && (d = t.i.copy(a.touches[0]), c = (new Date).getTime(), g = f)
        }), a.b("touchmove", function(a) {
            1 < a.touches.length ? g = l : d && (k = a.touches[0].pageX - d.pageX, q = a.touches[0].pageY - d.pageY, r = Math.sqrt(k * k + q * q), r > 10 && (g = l))
        }), h = function() {
            g = l
        }, a.b("touchleave", h), a.b("touchcancel", h), a.b("touchend", function(a) {
            d = j, g === f && (e = (new Date).getTime() - c, 200 > e && (a.preventDefault(), this.o("tap")))
        })
    }

    function ea(a, c) {
        var d, e, g, h;
        return d = a.c, e = t.Zd(d), h = g = d.offsetWidth, d = a.handle, a.options().vertical ? (h = e.top, e = c.changedTouches ? c.changedTouches[0].pageY : c.pageY, d && (d = d.m().offsetHeight, h += d / 2, g -= d), Math.max(0, Math.min(1, (h - e + g) / g))) : (g = e.left, e = c.changedTouches ? c.changedTouches[0].pageX : c.pageX, d && (d = d.m().offsetWidth, g += d / 2, h -= d), Math.max(0, Math.min(1, (e - g) / h)))
    }

    function fa(a, c) {
        a.ba(c), c.b("click", t.bind(a, function() {
            G(this)
        }))
    }

    function ga(a) {
        a.Ka = f, a.Aa.p("vjs-lock-showing"), a.c.setAttribute("aria-pressed", f), a.H && 0 < a.H.length && a.H[0].m().focus()
    }

    function H(a) {
        a.Ka = l, G(a.Aa), a.c.setAttribute("aria-pressed", l)
    }

    function ia(a) {
        var c, d, e = {
            sources: [],
            tracks: []
        };
        if (c = t.Pa(a), d = c["data-setup"], d !== j && t.i.D(c, t.JSON.parse(d || "{}")), t.i.D(e, c), a.hasChildNodes()) {
            var g, h;
            for (a = a.childNodes, g = 0, h = a.length; h > g; g++) c = a[g], d = c.nodeName.toLowerCase(), "source" === d ? e.sources.push(t.Pa(c)) : "track" === d && e.tracks.push(t.Pa(c))
        }
        return e
    }

    function ka(a, c, d) {
        a.h && (a.za = l, a.h.dispose(), a.h = l), "Html5" !== c && a.L && (t.e.Nb(a.L), a.L = j), a.Va = c, a.za = l;
        var e = t.i.D({
            source: d,
            parentEl: a.c
        }, a.q[c.toLowerCase()]);
        d && (a.Gc = d.type, d.src == a.K.src && 0 < a.K.currentTime && (e.startTime = a.K.currentTime), a.K.src = d.src), a.h = new window.videojs[c](a, e), a.h.I(function() {
            this.d.Xa()
        })
    }

    function la(a, c) {
        c !== b && a.Nc !== c && ((a.Nc = c) ? (a.p("vjs-has-started"), a.o("firstplay")) : a.r("vjs-has-started"))
    }

    function N(a, c, d) {
        if (a.h && !a.h.za) a.h.I(function() {
            this[c](d)
        });
        else try {
            a.h[c](d)
        } catch (e) {
            throw t.log(e), e
        }
    }

    function M(a, c) {
        if (a.h && a.h.za) try {
            return a.h[c]()
        } catch (d) {
            throw a.h[c] === b ? t.log("Video.js: " + c + " method not defined for " + a.Va + " playback technology.", d) : "TypeError" == d.name ? (t.log("Video.js: " + c + " unavailable on " + a.Va + " playback technology element.", d), a.h.za = l) : t.log(d), d
        }
    }

    function ma(a, c) {
        var d = a.selectSource(c);
        d ? d.h === a.Va ? a.src(d.source) : ka(a, d.h, d.source) : (a.setTimeout(function() {
            this.error({
                code: 4,
                message: this.v(this.options().notSupportedMessage)
            })
        }, 0), a.Xa())
    }

    function ja(a, c) {
        return c !== b ? (a.Pc = !!c, a) : a.Pc
    }

    function na(a) {
        return a.k().h && a.k().h.featuresPlaybackRate && a.k().options().playbackRates && 0 < a.k().options().playbackRates.length
    }

    function oa(a, c) {
        var d = /^blob\:/i;
        return c && a && d.test(a) ? c : a
    }

    function ta() {
        var a = T[U],
            c = a.charAt(0).toUpperCase() + a.slice(1);
        ua["set" + c] = function(c) {
            return this.c.vjs_setProperty(a, c)
        }
    }

    function va(a) {
        ua[a] = function() {
            return this.c.vjs_getProperty(a)
        }
    }

    function P(a, c) {
        var d = a.Wa.length;
        "" + d in a || Object.defineProperty(a, d, {
            get: function() {
                return this.Wa[d]
            }
        }), c.addEventListener("modechange", t.bind(a, function() {
            this.o("change")
        })), a.Wa.push(c), a.o({
            type: "addtrack",
            T: c
        })
    }

    function Q(a, c) {
        for (var g, d = 0, e = a.length; e > d; d++)
            if (g = a[d], g === c) {
                a.Wa.splice(d, 1);
                break
            }
        a.o({
            type: "removetrack",
            T: c
        })
    }

    function W(a, c) {
        return "rgba(" + parseInt(a[1] + a[1], 16) + "," + parseInt(a[2] + a[2], 16) + "," + parseInt(a[3] + a[3], 16) + "," + c + ")"
    }

    function X(a) {
        var c;
        return a.Le ? c = a.Le[0] : a.options && (c = a.options[a.options.selectedIndex]), c.value
    }

    function Y(a, c) {
        var d, e;
        if (c) {
            for (d = 0; d < a.options.length && (e = a.options[d], !(e.value === c)); d++);
            a.selectedIndex = d
        }
    }

    function $(a, c) {
        var d = a.split("."),
            e = Ba;
        !(d[0] in e) && e.execScript && e.execScript("var " + d[0]);
        for (var g; d.length && (g = d.shift());) d.length || c === b ? e = e[g] ? e[g] : e[g] = {} : e[g] = c
    }
    var b = void 0,
        f = !0,
        j = null,
        l = !1,
        s;
    document.createElement("video"), document.createElement("audio"), document.createElement("track");
    var videojs = window.videojs = t;
    t.jc = "4.12", t.wd = "https:" == document.location.protocol ? "https://" : "http://", t.VERSION = "4.12.11", t.options = {
        techOrder: ["html5", "flash"],
        html5: {},
        flash: {},
        width: 300,
        height: 150,
        defaultVolume: 0,
        playbackRates: [],
        inactivityTimeout: 2e3,
        children: {
            mediaLoader: {},
            posterImage: {},
            loadingSpinner: {},
            textTrackDisplay: {},
            bigPlayButton: {},
            controlBar: {},
            errorDisplay: {},
            textTrackSettings: {}
        },
        language: document.getElementsByTagName("html")[0].getAttribute("lang") || navigator.languages && navigator.languages[0] || navigator.Jf || navigator.language || "en",
        languages: {},
        notSupportedMessage: "No compatible source was found for this video."
    }, "GENERATED_CDN_VSN" !== t.jc && (videojs.options.flash.swf = t.wd + "vjs.zencdn.net/" + t.jc + "/video-js.swf"), t.Kd = function(a, c) {
        return t.options.languages[a] = t.options.languages[a] !== b ? t.$.Ba(t.options.languages[a], c) : c, t.options.languages
    }, t.Da = {}, "function" == typeof define && define.amd ? define("videojs", [], function() {
        return videojs
    }) : "object" == typeof exports && "object" == typeof module && (module.exports = videojs), t.Ha = t.CoreObject = m(), t.Ha.extend = function(a) {
        var c, d;
        a = a || {}, c = a.init || a.l || this.prototype.init || this.prototype.l || m(), d = function() {
            c.apply(this, arguments)
        }, d.prototype = t.i.create(this.prototype), d.prototype.constructor = d, d.extend = t.Ha.extend, d.create = t.Ha.create;
        for (var e in a) a.hasOwnProperty(e) && (d.prototype[e] = a[e]);
        return d
    }, t.Ha.create = function() {
        var a = t.i.create(this.prototype);
        return this.apply(a, arguments), a
    }, t.b = function(a, c, d) {
        if (t.i.isArray(c)) return v(t.b, a, c, d);
        var e = t.getData(a);
        e.G || (e.G = {}), e.G[c] || (e.G[c] = []), d.s || (d.s = t.s++), e.G[c].push(d), e.ca || (e.disabled = l, e.ca = function(c) {
            if (!e.disabled) {
                c = t.Qb(c);
                var d = e.G[c.type];
                if (d)
                    for (var d = d.slice(0), k = 0, q = d.length; q > k && !c.Rc(); k++) d[k].call(a, c)
            }
        }), 1 == e.G[c].length && (a.addEventListener ? a.addEventListener(c, e.ca, l) : a.attachEvent && a.attachEvent("on" + c, e.ca))
    }, t.n = function(a, c, d) {
        if (t.Mc(a)) {
            var e = t.getData(a);
            if (e.G) {
                if (t.i.isArray(c)) return v(t.n, a, c, d);
                if (c) {
                    var g = e.G[c];
                    if (g) {
                        if (d) {
                            if (d.s)
                                for (e = 0; e < g.length; e++) g[e].s === d.s && g.splice(e--, 1)
                        } else e.G[c] = [];
                        t.Bc(a, c)
                    }
                } else
                    for (g in e.G) c = g, e.G[c] = [], t.Bc(a, c)
            }
        }
    }, t.Bc = function(a, c) {
        var d = t.getData(a);
        0 === d.G[c].length && (delete d.G[c], a.removeEventListener ? a.removeEventListener(c, d.ca, l) : a.detachEvent && a.detachEvent("on" + c, d.ca)), t.kb(d.G) && (delete d.G, delete d.ca, delete d.disabled), t.kb(d) && t.cd(a)
    }, t.Qb = function(a) {
        function c() {
            return f
        }

        function d() {
            return l
        }
        if (!a || !a.Wb) {
            var e = a || window.event;
            a = {};
            for (var g in e) "layerX" !== g && "layerY" !== g && "keyLocation" !== g && ("returnValue" == g && e.preventDefault || (a[g] = e[g]));
            if (a.target || (a.target = a.srcElement || document), a.relatedTarget = a.fromElement === a.target ? a.toElement : a.fromElement, a.preventDefault = function() {
                    e.preventDefault && e.preventDefault(), a.returnValue = l, a.je = c, a.defaultPrevented = f
                }, a.je = d, a.defaultPrevented = l, a.stopPropagation = function() {
                    e.stopPropagation && e.stopPropagation(), a.cancelBubble = f, a.Wb = c
                }, a.Wb = d, a.stopImmediatePropagation = function() {
                    e.stopImmediatePropagation && e.stopImmediatePropagation(), a.Rc = c, a.stopPropagation()
                }, a.Rc = d, a.clientX != j) {
                g = document.documentElement;
                var h = document.body;
                a.pageX = a.clientX + (g && g.scrollLeft || h && h.scrollLeft || 0) - (g && g.clientLeft || h && h.clientLeft || 0), a.pageY = a.clientY + (g && g.scrollTop || h && h.scrollTop || 0) - (g && g.clientTop || h && h.clientTop || 0)
            }
            a.which = a.charCode || a.keyCode, a.button != j && (a.button = 1 & a.button ? 0 : 4 & a.button ? 1 : 2 & a.button ? 2 : 0)
        }
        return a
    }, t.o = function(a, c) {
        var d = t.Mc(a) ? t.getData(a) : {},
            e = a.parentNode || a.ownerDocument;
        return "string" == typeof c && (c = {
            type: c,
            target: a
        }), c = t.Qb(c), d.ca && d.ca.call(a, c), e && !c.Wb() && c.bubbles !== l ? t.o(e, c) : e || c.defaultPrevented || (d = t.getData(c.target), !c.target[c.type]) || (d.disabled = f, "function" == typeof c.target[c.type] && c.target[c.type](), d.disabled = l), !c.defaultPrevented
    }, t.N = function(a, c, d) {
        function e() {
            t.n(a, c, e), d.apply(this, arguments)
        }
        return t.i.isArray(c) ? v(t.N, a, c, d) : (e.s = d.s = d.s || t.s++, void t.b(a, c, e))
    };
    var w = Object.prototype.hasOwnProperty;
    t.f = function(a, c) {
        var d;
        return c = c || {}, d = document.createElement(a || "div"), t.i.da(c, function(a, c) {
            -1 !== a.indexOf("aria-") || "role" == a ? d.setAttribute(a, c) : d[a] = c
        }), d
    }, t.wa = function(a) {
        return a.charAt(0).toUpperCase() + a.slice(1)
    }, t.i = {}, t.i.create = Object.create || function(a) {
        function c() {}
        return c.prototype = a, new c
    }, t.i.da = function(a, c, d) {
        for (var e in a) w.call(a, e) && c.call(d || this, e, a[e])
    }, t.i.D = function(a, c) {
        if (!c) return a;
        for (var d in c) w.call(c, d) && (a[d] = c[d]);
        return a
    }, t.i.Sd = function(a, c) {
        var d, e, g;
        a = t.i.copy(a);
        for (d in c) w.call(c, d) && (e = a[d], g = c[d], a[d] = t.i.lb(e) && t.i.lb(g) ? t.i.Sd(e, g) : c[d]);
        return a
    }, t.i.copy = function(a) {
        return t.i.D({}, a)
    }, t.i.lb = function(a) {
        return !!a && "object" == typeof a && "[object Object]" === a.toString() && a.constructor === Object
    }, t.i.isArray = Array.isArray || function(a) {
        return "[object Array]" === Object.prototype.toString.call(a)
    }, t.le = function(a) {
        return a !== a
    }, t.bind = function(a, c, d) {
        function e() {
            return c.apply(a, arguments)
        }
        return c.s || (c.s = t.s++), e.s = d ? d + "_" + c.s : c.s, e
    }, t.va = {}, t.s = 1, t.expando = "vdata" + (new Date).getTime(), t.getData = function(a) {
        var c = a[t.expando];
        return c || (c = a[t.expando] = t.s++), t.va[c] || (t.va[c] = {}), t.va[c]
    }, t.Mc = function(a) {
        return a = a[t.expando], !(!a || t.kb(t.va[a]))
    }, t.cd = function(a) {
        var c = a[t.expando];
        if (c) {
            delete t.va[c];
            try {
                delete a[t.expando]
            } catch (d) {
                a.removeAttribute ? a.removeAttribute(t.expando) : a[t.expando] = j
            }
        }
    }, t.kb = function(a) {
        for (var c in a)
            if (a[c] !== j) return l;
        return f
    }, t.Qa = function(a, c) {
        return -1 !== (" " + a.className + " ").indexOf(" " + c + " ")
    }, t.p = function(a, c) {
        t.Qa(a, c) || (a.className = "" === a.className ? c : a.className + " " + c)
    }, t.r = function(a, c) {
        var d, e;
        if (t.Qa(a, c)) {
            for (d = a.className.split(" "), e = d.length - 1; e >= 0; e--) d[e] === c && d.splice(e, 1);
            a.className = d.join(" ")
        }
    }, t.A = t.f("video");
    var x = document.createElement("track");
    x.Xb = "captions", x.jd = "en", x.label = "English", t.A.appendChild(x), t.P = navigator.userAgent, t.Dd = /iPhone/i.test(t.P), t.Cd = /iPad/i.test(t.P), t.Ed = /iPod/i.test(t.P), t.Bd = t.Dd || t.Cd || t.Ed;
    var aa = t,
        y, z = t.P.match(/OS (\d+)_/i);
    y = z && z[1] ? z[1] : b, aa.lf = y, t.Ad = /Android/i.test(t.P);
    var ba = t,
        B, C = t.P.match(/Android (\d+)(?:\.(\d+))?(?:\.(\d+))*/i),
        D, E;
    C ? (D = C[1] && parseFloat(C[1]), E = C[2] && parseFloat(C[2]), B = D && E ? parseFloat(C[1] + "." + C[2]) : D ? D : j) : B = j, ba.ic = B, t.Fd = t.Ad && /webkit/i.test(t.P) && 2.3 > t.ic, t.kc = /Firefox/i.test(t.P), t.mf = /Chrome/i.test(t.P), t.qa = /MSIE\s8\.0/.test(t.P), t.Gb = !!("ontouchstart" in window || window.yd && document instanceof window.yd), t.xd = "backgroundSize" in t.A.style, t.ed = function(a, c) {
        t.i.da(c, function(c, e) {
            e === j || "undefined" == typeof e || e === l ? a.removeAttribute(c) : a.setAttribute(c, e === f ? "" : e)
        })
    }, t.Pa = function(a) {
        var c, d, e, g;
        if (c = {}, a && a.attributes && 0 < a.attributes.length) {
            d = a.attributes;
            for (var h = d.length - 1; h >= 0; h--) e = d[h].name, g = d[h].value, ("boolean" == typeof a[e] || -1 !== ",autoplay,controls,loop,muted,default,".indexOf("," + e + ",")) && (g = g !== j ? f : l), c[e] = g
        }
        return c
    }, t.wf = function(a, c) {
        var d = "";
        return document.defaultView && document.defaultView.getComputedStyle ? d = document.defaultView.getComputedStyle(a, "").getPropertyValue(c) : a.currentStyle && (d = a["client" + c.substr(0, 1).toUpperCase() + c.substr(1)] + "px"), d
    }, t.Vb = function(a, c) {
        c.firstChild ? c.insertBefore(a, c.firstChild) : c.appendChild(a)
    }, t.eb = {}, t.m = function(a) {
        return 0 === a.indexOf("#") && (a = a.slice(1)), document.getElementById(a)
    }, t.Oa = function(a, c) {
        c = c || a;
        var d = Math.floor(a % 60),
            e = Math.floor(a / 60 % 60),
            g = Math.floor(a / 3600),
            h = Math.floor(c / 60 % 60),
            k = Math.floor(c / 3600);
        return (isNaN(a) || 1 / 0 === a) && (g = e = d = "-"), g = g > 0 || k > 0 ? g + ":" : "", g + (((g || h >= 10) && 10 > e ? "0" + e : e) + ":") + (10 > d ? "0" + d : d)
    }, t.Md = function() {
        document.body.focus(), document.onselectstart = p(l)
    }, t.bf = function() {
        document.onselectstart = p(f)
    }, t.trim = function(a) {
        return (a + "").replace(/^\s+|\s+$/g, "")
    }, t.round = function(a, c) {
        return c || (c = 0), Math.round(a * Math.pow(10, c)) / Math.pow(10, c)
    }, t.ya = function(a, c) {
        return a === b && c === b ? {
            length: 0,
            start: function() {
                throw Error("This TimeRanges object is empty")
            },
            end: function() {
                throw Error("This TimeRanges object is empty")
            }
        } : {
            length: 1,
            start: function() {
                return a
            },
            end: function() {
                return c
            }
        }
    }, t.Ne = function(a) {
        try {
            var c = window.localStorage || l;
            c && (c.volume = a)
        } catch (d) {
            22 == d.code || 1014 == d.code ? t.log("LocalStorage Full (VideoJS)", d) : 18 == d.code ? t.log("LocalStorage not allowed (VideoJS)", d) : t.log("LocalStorage Error (VideoJS)", d)
        }
    }, t.ae = function(a) {
        return a.match(/^https?:\/\//) || (a = t.f("div", {
            innerHTML: '<a href="' + a + '">x</a>'
        }).firstChild.href), a
    }, t.Fe = function(a) {
        var c, d, e, g;
        g = "protocol hostname port pathname search hash host".split(" "), d = t.f("a", {
            href: a
        }), (e = "" === d.host && "file:" !== d.protocol) && (c = t.f("div"), c.innerHTML = '<a href="' + a + '"></a>', d = c.firstChild, c.setAttribute("style", "display:none; position:absolute;"), document.body.appendChild(c)), a = {};
        for (var h = 0; h < g.length; h++) a[g[h]] = d[g[h]];
        return "http:" === a.protocol && (a.host = a.host.replace(/:80$/, "")), "https:" === a.protocol && (a.host = a.host.replace(/:443$/, "")), e && document.body.removeChild(c), a
    }, t.log = function() {
        F(j, arguments)
    }, t.log.history = [], t.log.error = function() {
        F("error", arguments)
    }, t.log.warn = function() {
        F("warn", arguments)
    }, t.Zd = function(a) {
        var c, d;
        return a.getBoundingClientRect && a.parentNode && (c = a.getBoundingClientRect()), c ? (a = document.documentElement, d = document.body, {
            left: t.round(c.left + (window.pageXOffset || d.scrollLeft) - (a.clientLeft || d.clientLeft || 0)),
            top: t.round(c.top + (window.pageYOffset || d.scrollTop) - (a.clientTop || d.clientTop || 0))
        }) : {
            left: 0,
            top: 0
        }
    }, t.xc = {}, t.xc.forEach = function(a, c, d) {
        if (t.i.isArray(a) && c instanceof Function)
            for (var e = 0, g = a.length; g > e; ++e) c.call(d || t, a[e], e, a);
        return a
    }, t.gf = function(a, c) {
        var d, e, g, h, k, q, r;
        "string" == typeof a && (a = {
            uri: a
        }), videojs.$.Ba({
            method: "GET",
            timeout: 45e3
        }, a), c = c || m(), q = function() {
            window.clearTimeout(k), c(j, e, e.response || e.responseText)
        }, r = function(a) {
            window.clearTimeout(k), a && "string" != typeof a || (a = Error(a)), c(a, e)
        }, d = window.XMLHttpRequest, "undefined" == typeof d && (d = function() {
            try {
                return new window.ActiveXObject("Msxml2.XMLHTTP.6.0")
            } catch (a) {}
            try {
                return new window.ActiveXObject("Msxml2.XMLHTTP.3.0")
            } catch (c) {}
            try {
                return new window.ActiveXObject("Msxml2.XMLHTTP")
            } catch (d) {}
            throw Error("This browser does not support XMLHttpRequest.")
        }), e = new d, e.uri = a.uri, d = t.Fe(a.uri), g = window.location, d.protocol + d.host === g.protocol + g.host || !window.XDomainRequest || "withCredentials" in e ? (h = "file:" == d.protocol || "file:" == g.protocol, e.onreadystatechange = function() {
            if (4 === e.readyState) {
                if (e.Ze) return r("timeout");
                200 === e.status || h && 0 === e.status ? q() : r()
            }
        }, a.timeout && (k = window.setTimeout(function() {
            4 !== e.readyState && (e.Ze = f, e.abort())
        }, a.timeout))) : (e = new window.XDomainRequest, e.onload = q, e.onerror = r, e.onprogress = m(), e.ontimeout = m());
        try {
            e.open(a.method || "GET", a.uri, f)
        } catch (u) {
            return void r(u)
        }
        a.withCredentials && (e.withCredentials = f), a.responseType && (e.responseType = a.responseType);
        try {
            e.send()
        } catch (A) {
            r(A)
        }
    }, t.$ = {}, t.$.Ba = function(a, c) {
        var d, e, g;
        a = t.i.copy(a);
        for (d in c) c.hasOwnProperty(d) && (e = a[d], g = c[d], a[d] = t.i.lb(e) && t.i.lb(g) ? t.$.Ba(e, g) : c[d]);
        return a
    }, t.z = m(), s = t.z.prototype, s.cb = {}, s.b = function(a, c) {
        var d = this.addEventListener;
        this.addEventListener = Function.prototype, t.b(this, a, c), this.addEventListener = d
    }, s.addEventListener = t.z.prototype.b, s.n = function(a, c) {
        t.n(this, a, c)
    }, s.removeEventListener = t.z.prototype.n, s.N = function(a, c) {
        t.N(this, a, c)
    }, s.o = function(a) {
        var c = a.type || a;
        "string" == typeof a && (a = {
            type: c
        }), a = t.Qb(a), this.cb[c] && this["on" + c] && this["on" + c](a), t.o(this, a)
    }, s.dispatchEvent = t.z.prototype.o, t.a = t.Ha.extend({
        l: function(a, c, d) {
            if (this.d = a, this.q = t.i.copy(this.q), c = this.options(c), this.Ra = c.id || c.el && c.el.id, this.Ra || (this.Ra = (a.id && a.id() || "no_player") + "_component_" + t.s++), this.ue = c.name || j, this.c = c.el || this.f(), this.R = [], this.gb = {}, this.hb = {}, this.Oc(), this.I(d), c.dd !== l) {
                var e, g;
                this.k().reportUserActivity && (e = t.bind(this.k(), this.k().reportUserActivity), this.b("touchstart", function() {
                    e(), this.clearInterval(g), g = this.setInterval(e, 250)
                }), a = function() {
                    e(), this.clearInterval(g)
                }, this.b("touchmove", e), this.b("touchend", a), this.b("touchcancel", a))
            }
        }
    }), s = t.a.prototype, s.dispose = function() {
        if (this.o({
                type: "dispose",
                bubbles: l
            }), this.R)
            for (var a = this.R.length - 1; a >= 0; a--) this.R[a].dispose && this.R[a].dispose();
        this.hb = this.gb = this.R = j, this.n(), this.c.parentNode && this.c.parentNode.removeChild(this.c), t.cd(this.c), this.c = j
    }, s.d = f, s.k = n("d"), s.options = function(a) {
        return a === b ? this.q : this.q = t.$.Ba(this.q, a)
    }, s.f = function(a, c) {
        return t.f(a, c)
    }, s.v = function(a) {
        var c = this.d.language(),
            d = this.d.languages();
        return d && d[c] && d[c][a] ? d[c][a] : a
    }, s.m = n("c"), s.xa = function() {
        return this.B || this.c
    }, s.id = n("Ra"), s.name = n("ue"), s.children = n("R"), s.be = function(a) {
        return this.gb[a]
    }, s.ea = function(a) {
        return this.hb[a]
    }, s.ba = function(a, c) {
        var d, e;
        return "string" == typeof a ? (e = a, c = c || {}, d = c.componentClass || t.wa(e), c.name = e, d = new window.videojs[d](this.d || this, c)) : d = a, this.R.push(d), "function" == typeof d.id && (this.gb[d.id()] = d), (e = e || d.name && d.name()) && (this.hb[e] = d), "function" == typeof d.el && d.el() && this.xa().appendChild(d.el()), d
    }, s.removeChild = function(a) {
        if ("string" == typeof a && (a = this.ea(a)), a && this.R) {
            for (var c = l, d = this.R.length - 1; d >= 0; d--)
                if (this.R[d] === a) {
                    c = f, this.R.splice(d, 1);
                    break
                }
            c && (this.gb[a.id()] = j, this.hb[a.name()] = j, (c = a.m()) && c.parentNode === this.xa() && this.xa().removeChild(a.m()))
        }
    }, s.Oc = function() {
        var a, c, d, e, g, h;
        if (a = this, c = a.options(), d = c.children)
            if (h = function(d, e) {
                    c[d] !== b && (e = c[d]), e !== l && (a[d] = a.ba(d, e))
                }, t.i.isArray(d))
                for (var k = 0; k < d.length; k++) e = d[k], "string" == typeof e ? (g = e, e = {}) : g = e.name, h(g, e);
            else t.i.da(d, h)
    }, s.V = p(""), s.b = function(a, c, d) {
        var e, g, h;
        return "string" == typeof a || t.i.isArray(a) ? t.b(this.c, a, t.bind(this, c)) : (e = t.bind(this, d), h = this, g = function() {
            h.n(a, c, e)
        }, g.s = e.s, this.b("dispose", g), d = function() {
            h.n("dispose", g)
        }, d.s = e.s, a.nodeName ? (t.b(a, c, e), t.b(a, "dispose", d)) : "function" == typeof a.b && (a.b(c, e), a.b("dispose", d))), this
    }, s.n = function(a, c, d) {
        return !a || "string" == typeof a || t.i.isArray(a) ? t.n(this.c, a, c) : (d = t.bind(this, d), this.n("dispose", d), a.nodeName ? (t.n(a, c, d), t.n(a, "dispose", d)) : (a.n(c, d), a.n("dispose", d))), this
    }, s.N = function(a, c, d) {
        var e, g, h;
        return "string" == typeof a || t.i.isArray(a) ? t.N(this.c, a, t.bind(this, c)) : (e = t.bind(this, d), g = this, h = function() {
            g.n(a, c, h), e.apply(this, arguments)
        }, h.s = e.s, this.b(a, c, h)), this
    }, s.o = function(a) {
        return t.o(this.c, a), this
    }, s.I = function(a) {
        return a && (this.za ? a.call(this) : (this.pb === b && (this.pb = []), this.pb.push(a))), this
    }, s.Xa = function() {
        this.za = f;
        var a = this.pb;
        if (a && 0 < a.length) {
            for (var c = 0, d = a.length; d > c; c++) a[c].call(this);
            this.pb = [], this.o("ready")
        }
    }, s.Qa = function(a) {
        return t.Qa(this.c, a)
    }, s.p = function(a) {
        return t.p(this.c, a), this
    }, s.r = function(a) {
        return t.r(this.c, a), this
    }, s.show = function() {
        return this.r("vjs-hidden"), this
    }, s.Y = function() {
        return this.p("vjs-hidden"), this
    }, s.width = function(a, c) {
        return ca(this, "width", a, c)
    }, s.height = function(a, c) {
        return ca(this, "height", a, c)
    }, s.Ud = function(a, c) {
        return this.width(a, f).height(c)
    }, s.setTimeout = function(a, c) {
        function d() {
            this.clearTimeout(e)
        }
        a = t.bind(this, a);
        var e = setTimeout(a, c);
        return d.s = "vjs-timeout-" + e, this.b("dispose", d), e
    }, s.clearTimeout = function(a) {
        function c() {}
        return clearTimeout(a), c.s = "vjs-timeout-" + a, this.n("dispose", c), a
    }, s.setInterval = function(a, c) {
        function d() {
            this.clearInterval(e)
        }
        a = t.bind(this, a);
        var e = setInterval(a, c);
        return d.s = "vjs-interval-" + e, this.b("dispose", d), e
    }, s.clearInterval = function(a) {
        function c() {}
        return clearInterval(a), c.s = "vjs-interval-" + a, this.n("dispose", c), a
    }, t.w = t.a.extend({
        l: function(a, c) {
            t.a.call(this, a, c), da(this), this.b("tap", this.u), this.b("click", this.u), this.b("focus", this.nb), this.b("blur", this.mb)
        }
    }), s = t.w.prototype, s.f = function(a, c) {
        var d;
        return c = t.i.D({
            className: this.V(),
            role: "button",
            "aria-live": "polite",
            tabIndex: 0
        }, c), d = t.a.prototype.f.call(this, a, c), c.innerHTML || (this.B = t.f("div", {
            className: "vjs-control-content"
        }), this.Lb = t.f("span", {
            className: "vjs-control-text",
            innerHTML: this.v(this.ua) || "Need Text"
        }), this.B.appendChild(this.Lb), d.appendChild(this.B)), d
    }, s.V = function() {
        return "vjs-control " + t.a.prototype.V.call(this)
    }, s.u = m(), s.nb = function() {
        t.b(document, "keydown", t.bind(this, this.la))
    }, s.la = function(a) {
        (32 == a.which || 13 == a.which) && (a.preventDefault(), this.u())
    }, s.mb = function() {
        t.n(document, "keydown", t.bind(this, this.la))
    }, t.U = t.a.extend({
        l: function(a, c) {
            t.a.call(this, a, c), this.Ld = this.ea(this.q.barName), this.handle = this.ea(this.q.handleName), this.b("mousedown", this.ob), this.b("touchstart", this.ob), this.b("focus", this.nb), this.b("blur", this.mb), this.b("click", this.u), this.b(a, "controlsvisible", this.update), this.b(a, this.Yc, this.update)
        }
    }), s = t.U.prototype, s.f = function(a, c) {
        return c = c || {}, c.className += " vjs-slider", c = t.i.D({
            role: "slider",
            "aria-valuenow": 0,
            "aria-valuemin": 0,
            "aria-valuemax": 100,
            tabIndex: 0
        }, c), t.a.prototype.f.call(this, a, c)
    }, s.ob = function(a) {
        a.preventDefault(), t.Md(), this.p("vjs-sliding"), this.b(document, "mousemove", this.ma), this.b(document, "mouseup", this.Ca), this.b(document, "touchmove", this.ma), this.b(document, "touchend", this.Ca), this.ma(a)
    }, s.ma = m(), s.Ca = function() {
        t.bf(), this.r("vjs-sliding"), this.n(document, "mousemove", this.ma), this.n(document, "mouseup", this.Ca), this.n(document, "touchmove", this.ma), this.n(document, "touchend", this.Ca), this.update()
    }, s.update = function() {
        if (this.c) {
            var a, c = this.Tb(),
                d = this.handle,
                e = this.Ld;
            if (("number" != typeof c || c !== c || 0 > c || 1 / 0 === c) && (c = 0), a = c, d) {
                a = this.c.offsetWidth;
                var g = d.m().offsetWidth;
                a = g ? g / a : 0, c *= 1 - a, a = c + a / 2, d.m().style.left = t.round(100 * c, 2) + "%"
            }
            e && (e.m().style.width = t.round(100 * a, 2) + "%")
        }
    }, s.nb = function() {
        this.b(document, "keydown", this.la)
    }, s.la = function(a) {
        37 == a.which || 40 == a.which ? (a.preventDefault(), this.kd()) : (38 == a.which || 39 == a.which) && (a.preventDefault(), this.ld())
    }, s.mb = function() {
        this.n(document, "keydown", this.la)
    }, s.u = function(a) {
        a.stopImmediatePropagation(), a.preventDefault()
    }, t.ha = t.a.extend(), t.ha.prototype.defaultValue = 0, t.ha.prototype.f = function(a, c) {
        return c = c || {}, c.className += " vjs-slider-handle", c = t.i.D({
            innerHTML: '<span class="vjs-control-text">' + this.defaultValue + "</span>"
        }, c), t.a.prototype.f.call(this, "div", c)
    }, t.ra = t.a.extend(), t.ra.prototype.f = function() {
        var a = this.options().Dc || "ul";
        return this.B = t.f(a, {
            className: "vjs-menu-content"
        }), a = t.a.prototype.f.call(this, "div", {
            append: this.B,
            className: "vjs-menu"
        }), a.appendChild(this.B), t.b(a, "click", function(a) {
            a.preventDefault(), a.stopImmediatePropagation()
        }), a
    }, t.M = t.w.extend({
        l: function(a, c) {
            t.w.call(this, a, c), this.selected(c.selected)
        }
    }), t.M.prototype.f = function(a, c) {
        return t.w.prototype.f.call(this, "li", t.i.D({
            className: "vjs-menu-item",
            innerHTML: this.v(this.q.label)
        }, c))
    }, t.M.prototype.u = function() {
        this.selected(f)
    }, t.M.prototype.selected = function(a) {
        a ? (this.p("vjs-selected"), this.c.setAttribute("aria-selected", f)) : (this.r("vjs-selected"), this.c.setAttribute("aria-selected", l))
    }, t.O = t.w.extend({
        l: function(a, c) {
            t.w.call(this, a, c), this.update(), this.b("keydown", this.la), this.c.setAttribute("aria-haspopup", f), this.c.setAttribute("role", "button")
        }
    }), s = t.O.prototype, s.update = function() {
        var a = this.Ma();
        this.Aa && this.removeChild(this.Aa), this.Aa = a, this.ba(a), this.H && 0 === this.H.length ? this.Y() : this.H && 1 < this.H.length && this.show()
    }, s.Ka = l, s.Ma = function() {
        var a = new t.ra(this.d);
        if (this.options().title && a.xa().appendChild(t.f("li", {
                className: "vjs-menu-title",
                innerHTML: t.wa(this.options().title),
                Xe: -1
            })), this.H = this.createItems())
            for (var c = 0; c < this.H.length; c++) fa(a, this.H[c]);
        return a
    }, s.La = m(), s.V = function() {
        return this.className + " vjs-menu-button " + t.w.prototype.V.call(this)
    }, s.nb = m(), s.mb = m(), s.u = function() {
        this.N("mouseout", t.bind(this, function() {
            G(this.Aa), this.c.blur()
        })), this.Ka ? H(this) : ga(this)
    }, s.la = function(a) {
        32 == a.which || 13 == a.which ? (this.Ka ? H(this) : ga(this), a.preventDefault()) : 27 == a.which && (this.Ka && H(this), a.preventDefault())
    }, t.J = function(a) {
        "number" == typeof a ? this.code = a : "string" == typeof a ? this.message = a : "object" == typeof a && t.i.D(this, a), this.message || (this.message = t.J.Td[this.code] || "")
    }, t.J.prototype.code = 0, t.J.prototype.message = "", t.J.prototype.status = j, t.J.jb = "MEDIA_ERR_CUSTOM MEDIA_ERR_ABORTED MEDIA_ERR_NETWORK MEDIA_ERR_DECODE MEDIA_ERR_SRC_NOT_SUPPORTED MEDIA_ERR_ENCRYPTED".split(" "), t.J.Td = {
        1: "You aborted the video playback",
        2: "A network error caused the video download to fail part-way.",
        3: "The video playback was aborted due to a corruption problem or because the video used features your browser did not support.",
        4: "The video could not be loaded, either because the server or network failed or because the format is not supported.",
        5: "The video is encrypted and we do not have the keys to decrypt it."
    };
    for (var I = 0; I < t.J.jb.length; I++) t.J[t.J.jb[I]] = I, t.J.prototype[t.J.jb[I]] = I;
    var J, ha, K, L;
    for (J = ["requestFullscreen exitFullscreen fullscreenElement fullscreenEnabled fullscreenchange fullscreenerror".split(" "), "webkitRequestFullscreen webkitExitFullscreen webkitFullscreenElement webkitFullscreenEnabled webkitfullscreenchange webkitfullscreenerror".split(" "), "webkitRequestFullScreen webkitCancelFullScreen webkitCurrentFullScreenElement webkitCancelFullScreen webkitfullscreenchange webkitfullscreenerror".split(" "), "mozRequestFullScreen mozCancelFullScreen mozFullScreenElement mozFullScreenEnabled mozfullscreenchange mozfullscreenerror".split(" "), "msRequestFullscreen msExitFullscreen msFullscreenElement msFullscreenEnabled MSFullscreenChange MSFullscreenError".split(" ")], ha = J[0], L = 0; L < J.length; L++)
        if (J[L][1] in document) {
            K = J[L];
            break
        }
    if (K)
        for (t.eb.Sb = {}, L = 0; L < K.length; L++) t.eb.Sb[ha[L]] = K[L];
    t.Player = t.a.extend({
        l: function(a, c, d) {
            this.L = a, a.id = a.id || "vjs_video_" + t.s++, this.Ye = a && t.Pa(a), c = t.i.D(ia(a), c), this.Tc = c.language || t.options.language, this.oe = c.languages || t.options.languages, this.K = {}, this.Zc = c.poster || "", this.Mb = !!c.controls, a.controls = l, c.dd = l, ja(this, "audio" === this.L.nodeName.toLowerCase()), t.a.call(this, this, c, d), this.controls() ? this.p("vjs-controls-enabled") : this.p("vjs-controls-disabled"), ja(this) && this.p("vjs-audio"), t.Da[this.Ra] = this, c.plugins && t.i.da(c.plugins, function(a, c) {
                this[a](c)
            }, this);
            var e, g, h, k, q;
            e = t.bind(this, this.reportUserActivity), this.b("mousedown", function() {
                e(), this.clearInterval(g), g = this.setInterval(e, 250)
            }), this.b("mousemove", function(a) {
                (a.screenX != k || a.screenY != q) && (k = a.screenX, q = a.screenY, e())
            }), this.b("mouseup", function() {
                e(), this.clearInterval(g)
            }), this.b("keydown", e), this.b("keyup", e), this.setInterval(function() {
                if (this.Ga) {
                    this.Ga = l, this.userActive(f), this.clearTimeout(h);
                    var a = this.options().inactivityTimeout;
                    a > 0 && (h = this.setTimeout(function() {
                        this.Ga || this.userActive(l)
                    }, a))
                }
            }, 250)
        }
    }), s = t.Player.prototype, s.language = function(a) {
        return a === b ? this.Tc : (this.Tc = a, this)
    }, s.languages = n("oe"), s.q = t.options, s.dispose = function() {
        this.o("dispose"), this.n("dispose"), t.Da[this.Ra] = j, this.L && this.L.player && (this.L.player = j), this.c && this.c.player && (this.c.player = j), this.h && this.h.dispose(), t.a.prototype.dispose.call(this)
    }, s.f = function() {
        var d, a = this.c = t.a.prototype.f.call(this, "div"),
            c = this.L;
        return c.removeAttribute("width"), c.removeAttribute("height"), d = t.Pa(c), t.i.da(d, function(c) {
            "class" == c ? a.className = d[c] : a.setAttribute(c, d[c])
        }), c.id += "_html5_api", c.className = "vjs-tech", c.player = a.player = this, this.p("vjs-paused"), this.width(this.q.width, f), this.height(this.q.height, f), c.he = c.networkState, c.parentNode && c.parentNode.insertBefore(a, c), t.Vb(c, a), this.c = a, this.b("loadstart", this.ye), this.b("waiting", this.Ee), this.b(["canplay", "canplaythrough", "playing", "ended"], this.De), this.b("seeking", this.Be), this.b("seeked", this.Ae), this.b("ended", this.ve), this.b("play", this.ac), this.b("firstplay", this.we), this.b("pause", this.$b), this.b("progress", this.ze), this.b("durationchange", this.Wc), this.b("fullscreenchange", this.xe), a
    }, s.ye = function() {
        this.r("vjs-ended"), this.error(j), this.paused() ? la(this, l) : this.o("firstplay")
    }, s.Nc = l, s.ac = function() {
        this.r("vjs-ended"), this.r("vjs-paused"), this.p("vjs-playing"), la(this, f)
    }, s.Ee = function() {
        this.p("vjs-waiting")
    }, s.De = function() {
        this.r("vjs-waiting")
    }, s.Be = function() {
        this.p("vjs-seeking")
    }, s.Ae = function() {
        this.r("vjs-seeking")
    }, s.we = function() {
        this.q.starttime && this.currentTime(this.q.starttime), this.p("vjs-has-started")
    }, s.$b = function() {
        this.r("vjs-playing"), this.p("vjs-paused")
    }, s.ze = function() {
        1 == this.bufferedPercent() && this.o("loadedalldata")
    }, s.ve = function() {
        this.p("vjs-ended"), this.q.loop ? (this.currentTime(0), this.play()) : this.paused() || this.pause()
    }, s.Wc = function() {
        var a = M(this, "duration");
        a && (0 > a && (a = 1 / 0), this.duration(a), 1 / 0 === a ? this.p("vjs-live") : this.r("vjs-live"))
    }, s.xe = function() {
        this.isFullscreen() ? this.p("vjs-fullscreen") : this.r("vjs-fullscreen")
    }, s.play = function() {
        return N(this, "play"), this
    }, s.pause = function() {
        return N(this, "pause"), this
    }, s.paused = function() {
        return M(this, "paused") === l ? l : f
    }, s.currentTime = function(a) {
        return a !== b ? (N(this, "setCurrentTime", a), this) : this.K.currentTime = M(this, "currentTime") || 0
    }, s.duration = function(a) {
        return a !== b ? (this.K.duration = parseFloat(a), this) : (this.K.duration === b && this.Wc(), this.K.duration || 0)
    }, s.remainingTime = function() {
        return this.duration() - this.currentTime()
    }, s.buffered = function() {
        var a = M(this, "buffered");
        return a && a.length || (a = t.ya(0, 0)), a
    }, s.bufferedPercent = function() {
        var e, g, a = this.duration(),
            c = this.buffered(),
            d = 0;
        if (!a) return 0;
        for (var h = 0; h < c.length; h++) e = c.start(h), g = c.end(h), g > a && (g = a), d += g - e;
        return d / a
    }, s.volume = function(a) {
        return a !== b ? (a = Math.max(0, Math.min(1, parseFloat(a))), this.K.volume = a, N(this, "setVolume", a), t.Ne(a), this) : (a = parseFloat(M(this, "volume")), isNaN(a) ? 1 : a)
    }, s.muted = function(a) {
        return a !== b ? (N(this, "setMuted", a), this) : M(this, "muted") || l
    }, s.Ua = function() {
        return M(this, "supportsFullScreen") || l
    }, s.Qc = l, s.isFullscreen = function(a) {
        return a !== b ? (this.Qc = !!a, this) : this.Qc
    }, s.isFullScreen = function(a) {
        return t.log.warn('player.isFullScreen() has been deprecated, use player.isFullscreen() with a lowercase "s")'), this.isFullscreen(a)
    }, s.requestFullscreen = function() {
        var a = t.eb.Sb;
        return this.isFullscreen(f), a ? (t.b(document, a.fullscreenchange, t.bind(this, function(c) {
            this.isFullscreen(document[a.fullscreenElement]), this.isFullscreen() === l && t.n(document, a.fullscreenchange, arguments.callee), this.o("fullscreenchange")
        })), this.c[a.requestFullscreen]()) : this.h.Ua() ? N(this, "enterFullScreen") : (this.Jc(), this.o("fullscreenchange")), this
    }, s.requestFullScreen = function() {
        return t.log.warn('player.requestFullScreen() has been deprecated, use player.requestFullscreen() with a lowercase "s")'), this.requestFullscreen()
    }, s.exitFullscreen = function() {
        var a = t.eb.Sb;
        return this.isFullscreen(l), a ? document[a.exitFullscreen]() : this.h.Ua() ? N(this, "exitFullScreen") : (this.Ob(), this.o("fullscreenchange")), this
    }, s.cancelFullScreen = function() {
        return t.log.warn("player.cancelFullScreen() has been deprecated, use player.exitFullscreen()"), this.exitFullscreen()
    }, s.Jc = function() {
        this.ke = f, this.Vd = document.documentElement.style.overflow, t.b(document, "keydown", t.bind(this, this.Kc)), document.documentElement.style.overflow = "hidden", t.p(document.body, "vjs-full-window"), this.o("enterFullWindow")
    }, s.Kc = function(a) {
        27 === a.keyCode && (this.isFullscreen() === f ? this.exitFullscreen() : this.Ob())
    }, s.Ob = function() {
        this.ke = l, t.n(document, "keydown", this.Kc), document.documentElement.style.overflow = this.Vd, t.r(document.body, "vjs-full-window"), this.o("exitFullWindow")
    }, s.selectSource = function(a) {
        for (var c = 0, d = this.q.techOrder; c < d.length; c++) {
            var e = t.wa(d[c]),
                g = window.videojs[e];
            if (g) {
                if (g.isSupported())
                    for (var h = 0, k = a; h < k.length; h++) {
                        var q = k[h];
                        if (g.canPlaySource(q)) return {
                            source: q,
                            h: e
                        }
                    }
            } else t.log.error('The "' + e + '" tech is undefined. Skipped browser support check for that tech.')
        }
        return l
    }, s.src = function(a) {
        return a === b ? M(this, "src") : (t.i.isArray(a) ? ma(this, a) : "string" == typeof a ? this.src({
            src: a
        }) : a instanceof Object && (a.type && !window.videojs[this.Va].canPlaySource(a) ? ma(this, [a]) : (this.K.src = a.src, this.Gc = a.type || "", this.I(function() {
            window.videojs[this.Va].prototype.hasOwnProperty("setSource") ? N(this, "setSource", a) : N(this, "src", a.src), "auto" == this.q.preload && this.load(), this.q.autoplay && this.play()
        }))), this)
    }, s.load = function() {
        return N(this, "load"), this
    }, s.currentSrc = function() {
        return M(this, "currentSrc") || this.K.src || ""
    }, s.Rd = function() {
        return this.Gc || ""
    }, s.Sa = function(a) {
        return a !== b ? (N(this, "setPreload", a), this.q.preload = a, this) : M(this, "preload")
    }, s.autoplay = function(a) {
        return a !== b ? (N(this, "setAutoplay", a), this.q.autoplay = a, this) : M(this, "autoplay")
    }, s.loop = function(a) {
        return a !== b ? (N(this, "setLoop", a), this.q.loop = a, this) : M(this, "loop")
    }, s.poster = function(a) {
        return a === b ? this.Zc : (a || (a = ""), this.Zc = a, N(this, "setPoster", a), this.o("posterchange"), this)
    }, s.controls = function(a) {
        return a !== b ? (a = !!a, this.Mb !== a && ((this.Mb = a) ? (this.r("vjs-controls-disabled"), this.p("vjs-controls-enabled"), this.o("controlsenabled")) : (this.r("vjs-controls-enabled"), this.p("vjs-controls-disabled"), this.o("controlsdisabled"))), this) : this.Mb
    }, t.Player.prototype.fc, s = t.Player.prototype, s.usingNativeControls = function(a) {
        return a !== b ? (a = !!a, this.fc !== a && ((this.fc = a) ? (this.p("vjs-using-native-controls"), this.o("usingnativecontrols")) : (this.r("vjs-using-native-controls"), this.o("usingcustomcontrols"))), this) : this.fc
    }, s.ka = j, s.error = function(a) {
        return a === b ? this.ka : a === j ? (this.ka = a, this.r("vjs-error"), this) : (this.ka = a instanceof t.J ? a : new t.J(a), this.o("error"), this.p("vjs-error"), t.log.error("(CODE:" + this.ka.code + " " + t.J.jb[this.ka.code] + ")", this.ka.message, this.ka), this)
    }, s.ended = function() {
        return M(this, "ended")
    }, s.seeking = function() {
        return M(this, "seeking")
    }, s.seekable = function() {
        return M(this, "seekable")
    }, s.Ga = f, s.reportUserActivity = function() {
        this.Ga = f
    }, s.ec = f, s.userActive = function(a) {
        return a !== b ? (a = !!a, a !== this.ec && ((this.ec = a) ? (this.Ga = f, this.r("vjs-user-inactive"), this.p("vjs-user-active"), this.o("useractive")) : (this.Ga = l, this.h && this.h.N("mousemove", function(a) {
            a.stopPropagation(), a.preventDefault()
        }), this.r("vjs-user-active"), this.p("vjs-user-inactive"), this.o("userinactive"))), this) : this.ec
    }, s.playbackRate = function(a) {
        return a !== b ? (N(this, "setPlaybackRate", a), this) : this.h && this.h.featuresPlaybackRate ? M(this, "playbackRate") : 1
    }, s.Pc = l, s.networkState = function() {
        return M(this, "networkState")
    }, s.readyState = function() {
        return M(this, "readyState")
    }, s.textTracks = function() {
        return this.h && this.h.textTracks()
    }, s.Z = function() {
        return this.h && this.h.remoteTextTracks()
    }, s.addTextTrack = function(a, c, d) {
        return this.h && this.h.addTextTrack(a, c, d)
    }, s.ia = function(a) {
        return this.h && this.h.addRemoteTextTrack(a)
    }, s.Ea = function(a) {
        this.h && this.h.removeRemoteTextTrack(a)
    }, t.wb = t.a.extend(), t.wb.prototype.q = {
        xf: "play",
        children: {
            playToggle: {},
            currentTimeDisplay: {},
            timeDivider: {},
            durationDisplay: {},
            remainingTimeDisplay: {},
            liveDisplay: {},
            progressControl: {},
            fullscreenToggle: {},
            volumeControl: {},
            muteToggle: {},
            playbackRateMenuButton: {},
            subtitlesButton: {},
            captionsButton: {},
            chaptersButton: {}
        }
    }, t.wb.prototype.f = function() {
        return t.f("div", {
            className: "vjs-control-bar"
        })
    }, t.lc = t.a.extend({
        l: function(a, c) {
            t.a.call(this, a, c)
        }
    }), t.lc.prototype.f = function() {
        var a = t.a.prototype.f.call(this, "div", {
            className: "vjs-live-controls vjs-control"
        });
        return this.B = t.f("div", {
            className: "vjs-live-display",
            innerHTML: '<span class="vjs-control-text">' + this.v("Stream Type") + "</span>" + this.v("LIVE"),
            "aria-live": "off"
        }), a.appendChild(this.B), a
    }, t.oc = t.w.extend({
        l: function(a, c) {
            t.w.call(this, a, c), this.b(a, "play", this.ac), this.b(a, "pause", this.$b)
        }
    }), s = t.oc.prototype, s.ua = "Play", s.V = function() {
        return "vjs-play-control " + t.w.prototype.V.call(this)
    }, s.u = function() {
        this.d.paused() ? this.d.play() : this.d.pause()
    }, s.ac = function() {
        this.r("vjs-paused"), this.p("vjs-playing"), this.c.children[0].children[0].innerHTML = this.v("Pause")
    }, s.$b = function() {
        this.r("vjs-playing"), this.p("vjs-paused"), this.c.children[0].children[0].innerHTML = this.v("Play")
    }, t.xb = t.a.extend({
        l: function(a, c) {
            t.a.call(this, a, c), this.b(a, "timeupdate", this.ga)
        }
    }), t.xb.prototype.f = function() {
        var a = t.a.prototype.f.call(this, "div", {
            className: "vjs-current-time vjs-time-controls vjs-control"
        });
        return this.B = t.f("div", {
            className: "vjs-current-time-display",
            innerHTML: '<span class="vjs-control-text">Current Time </span>0:00',
            "aria-live": "off"
        }), a.appendChild(this.B), a
    }, t.xb.prototype.ga = function() {
        var a = this.d.qb ? this.d.K.currentTime : this.d.currentTime();
        this.B.innerHTML = '<span class="vjs-control-text">' + this.v("Current Time") + "</span> " + t.Oa(a, this.d.duration())
    }, t.yb = t.a.extend({
        l: function(a, c) {
            t.a.call(this, a, c), this.b(a, "timeupdate", this.ga), this.b(a, "loadedmetadata", this.ga)
        }
    }), t.yb.prototype.f = function() {
        var a = t.a.prototype.f.call(this, "div", {
            className: "vjs-duration vjs-time-controls vjs-control"
        });
        return this.B = t.f("div", {
            className: "vjs-duration-display",
            innerHTML: '<span class="vjs-control-text">' + this.v("Duration Time") + "</span> 0:00",
            "aria-live": "off"
        }), a.appendChild(this.B), a
    }, t.yb.prototype.ga = function() {
        var a = this.d.duration();
        a && (this.B.innerHTML = '<span class="vjs-control-text">' + this.v("Duration Time") + "</span> " + t.Oa(a))
    }, t.uc = t.a.extend({
        l: function(a, c) {
            t.a.call(this, a, c)
        }
    }), t.uc.prototype.f = function() {
        return t.a.prototype.f.call(this, "div", {
            className: "vjs-time-divider",
            innerHTML: "<div><span>/</span></div>"
        })
    }, t.Fb = t.a.extend({
        l: function(a, c) {
            t.a.call(this, a, c), this.b(a, "timeupdate", this.ga)
        }
    }), t.Fb.prototype.f = function() {
        var a = t.a.prototype.f.call(this, "div", {
            className: "vjs-remaining-time vjs-time-controls vjs-control"
        });
        return this.B = t.f("div", {
            className: "vjs-remaining-time-display",
            innerHTML: '<span class="vjs-control-text">' + this.v("Remaining Time") + "</span> -0:00",
            "aria-live": "off"
        }), a.appendChild(this.B), a
    }, t.Fb.prototype.ga = function() {
        this.d.duration() && (this.B.innerHTML = '<span class="vjs-control-text">' + this.v("Remaining Time") + "</span> -" + t.Oa(this.d.remainingTime()))
    }, t.$a = t.w.extend({
        l: function(a, c) {
            t.w.call(this, a, c)
        }
    }), t.$a.prototype.ua = "Fullscreen", t.$a.prototype.V = function() {
        return "vjs-fullscreen-control " + t.w.prototype.V.call(this)
    }, t.$a.prototype.u = function() {
        this.d.isFullscreen() ? (this.d.exitFullscreen(), this.Lb.innerHTML = this.v("Fullscreen")) : (this.d.requestFullscreen(), this.Lb.innerHTML = this.v("Non-Fullscreen"))
    }, t.Eb = t.a.extend({
        l: function(a, c) {
            t.a.call(this, a, c)
        }
    }), t.Eb.prototype.q = {
        children: {
            seekBar: {}
        }
    }, t.Eb.prototype.f = function() {
        return t.a.prototype.f.call(this, "div", {
            className: "vjs-progress-control vjs-control"
        })
    }, t.rc = t.U.extend({
        l: function(a, c) {
            t.U.call(this, a, c), this.b(a, "timeupdate", this.Fa), a.I(t.bind(this, this.Fa))
        }
    }), s = t.rc.prototype, s.q = {
        children: {
            loadProgressBar: {},
            playProgressBar: {},
            seekHandle: {}
        },
        barName: "playProgressBar",
        handleName: "seekHandle"
    }, s.Yc = "timeupdate", s.f = function() {
        return t.U.prototype.f.call(this, "div", {
            className: "vjs-progress-holder",
            "aria-label": "video progress bar"
        })
    }, s.Fa = function() {
        var a = this.d.qb ? this.d.K.currentTime : this.d.currentTime();
        this.c.setAttribute("aria-valuenow", t.round(100 * this.Tb(), 2)), this.c.setAttribute("aria-valuetext", t.Oa(a, this.d.duration()))
    }, s.Tb = function() {
        return this.d.currentTime() / this.d.duration()
    }, s.ob = function(a) {
        t.U.prototype.ob.call(this, a), this.d.qb = f, this.d.p("vjs-scrubbing"), this.ef = !this.d.paused(), this.d.pause()
    }, s.ma = function(a) {
        a = ea(this, a) * this.d.duration(), a == this.d.duration() && (a -= .1), this.d.currentTime(a)
    }, s.Ca = function(a) {
        t.U.prototype.Ca.call(this, a), this.d.qb = l, this.d.r("vjs-scrubbing"), this.ef && this.d.play()
    }, s.ld = function() {
        this.d.currentTime(this.d.currentTime() + 5)
    }, s.kd = function() {
        this.d.currentTime(this.d.currentTime() - 5)
    }, t.Bb = t.a.extend({
        l: function(a, c) {
            t.a.call(this, a, c), this.b(a, "progress", this.update)
        }
    }), t.Bb.prototype.f = function() {
        return t.a.prototype.f.call(this, "div", {
            className: "vjs-load-progress",
            innerHTML: '<span class="vjs-control-text"><span>' + this.v("Loaded") + "</span>: 0%</span>"
        })
    }, t.Bb.prototype.update = function() {
        var a, c, d, e, g = this.d.buffered();
        a = this.d.duration();
        var h, k = this.d;
        for (h = k.buffered(), k = k.duration(), h = h.end(h.length - 1), h > k && (h = k), k = this.c.children, this.c.style.width = 100 * (h / a || 0) + "%", a = 0; a < g.length; a++) c = g.start(a), d = g.end(a), (e = k[a]) || (e = this.c.appendChild(t.f())), e.style.left = 100 * (c / h || 0) + "%", e.style.width = 100 * ((d - c) / h || 0) + "%";
        for (a = k.length; a > g.length; a--) this.c.removeChild(k[a - 1])
    }, t.nc = t.a.extend({
        l: function(a, c) {
            t.a.call(this, a, c)
        }
    }), t.nc.prototype.f = function() {
        return t.a.prototype.f.call(this, "div", {
            className: "vjs-play-progress",
            innerHTML: '<span class="vjs-control-text"><span>' + this.v("Progress") + "</span>: 0%</span>"
        })
    }, t.ab = t.ha.extend({
        l: function(a, c) {
            t.ha.call(this, a, c), this.b(a, "timeupdate", this.ga)
        }
    }), t.ab.prototype.defaultValue = "00:00", t.ab.prototype.f = function() {
        return t.ha.prototype.f.call(this, "div", {
            className: "vjs-seek-handle",
            "aria-live": "off"
        })
    }, t.ab.prototype.ga = function() {
        var a = this.d.qb ? this.d.K.currentTime : this.d.currentTime();
        this.c.innerHTML = '<span class="vjs-control-text">' + t.Oa(a, this.d.duration()) + "</span>"
    }, t.Ib = t.a.extend({
        l: function(a, c) {
            t.a.call(this, a, c), a.h && a.h.featuresVolumeControl === l && this.p("vjs-hidden"), this.b(a, "loadstart", function() {
                a.h.featuresVolumeControl === l ? this.p("vjs-hidden") : this.r("vjs-hidden")
            })
        }
    }), t.Ib.prototype.q = {
        children: {
            volumeBar: {}
        }
    }, t.Ib.prototype.f = function() {
        return t.a.prototype.f.call(this, "div", {
            className: "vjs-volume-control vjs-control"
        })
    }, t.Hb = t.U.extend({
        l: function(a, c) {
            t.U.call(this, a, c), this.b(a, "volumechange", this.Fa), a.I(t.bind(this, this.Fa))
        }
    }), s = t.Hb.prototype, s.Fa = function() {
        this.c.setAttribute("aria-valuenow", t.round(100 * this.d.volume(), 2)), this.c.setAttribute("aria-valuetext", t.round(100 * this.d.volume(), 2) + "%")
    }, s.q = {
        children: {
            volumeLevel: {},
            volumeHandle: {}
        },
        barName: "volumeLevel",
        handleName: "volumeHandle"
    }, s.Yc = "volumechange", s.f = function() {
        return t.U.prototype.f.call(this, "div", {
            className: "vjs-volume-bar",
            "aria-label": "volume level"
        })
    }, s.ma = function(a) {
        this.d.muted() && this.d.muted(l), this.d.volume(ea(this, a))
    }, s.Tb = function() {
        return this.d.muted() ? 0 : this.d.volume()
    }, s.ld = function() {
        this.d.volume(this.d.volume() + .1)
    }, s.kd = function() {
        this.d.volume(this.d.volume() - .1)
    }, t.vc = t.a.extend({
        l: function(a, c) {
            t.a.call(this, a, c)
        }
    }), t.vc.prototype.f = function() {
        return t.a.prototype.f.call(this, "div", {
            className: "vjs-volume-level",
            innerHTML: '<span class="vjs-control-text"></span>'
        })
    }, t.Jb = t.ha.extend(), t.Jb.prototype.defaultValue = "00:00", t.Jb.prototype.f = function() {
        return t.ha.prototype.f.call(this, "div", {
            className: "vjs-volume-handle"
        })
    }, t.sa = t.w.extend({
        l: function(a, c) {
            t.w.call(this, a, c), this.b(a, "volumechange", this.update), a.h && a.h.featuresVolumeControl === l && this.p("vjs-hidden"), this.b(a, "loadstart", function() {
                a.h.featuresVolumeControl === l ? this.p("vjs-hidden") : this.r("vjs-hidden")
            })
        }
    }), t.sa.prototype.f = function() {
        return t.w.prototype.f.call(this, "div", {
            className: "vjs-mute-control vjs-control",
            innerHTML: '<div><span class="vjs-control-text">' + this.v("Mute") + "</span></div>"
        })
    }, t.sa.prototype.u = function() {
        this.d.muted(this.d.muted() ? l : f)
    }, t.sa.prototype.update = function() {
        var a = this.d.volume(),
            c = 3;
        for (0 === a || this.d.muted() ? c = 0 : .33 > a ? c = 1 : .67 > a && (c = 2), this.d.muted() ? this.c.children[0].children[0].innerHTML != this.v("Unmute") && (this.c.children[0].children[0].innerHTML = this.v("Unmute")) : this.c.children[0].children[0].innerHTML != this.v("Mute") && (this.c.children[0].children[0].innerHTML = this.v("Mute")), a = 0; 4 > a; a++) t.r(this.c, "vjs-vol-" + a);
        t.p(this.c, "vjs-vol-" + c)
    }, t.Ia = t.O.extend({
        l: function(a, c) {
            t.O.call(this, a, c), this.b(a, "volumechange", this.ff), a.h && a.h.featuresVolumeControl === l && this.p("vjs-hidden"), this.b(a, "loadstart", function() {
                a.h.featuresVolumeControl === l ? this.p("vjs-hidden") : this.r("vjs-hidden")
            }), this.p("vjs-menu-button")
        }
    }), t.Ia.prototype.Ma = function() {
        var a = new t.ra(this.d, {
                Dc: "div"
            }),
            c = new t.Hb(this.d, this.q.volumeBar);
        return c.b("focus", function() {
            a.p("vjs-lock-showing")
        }), c.b("blur", function() {
            G(a)
        }), a.ba(c), a
    }, t.Ia.prototype.u = function() {
        t.sa.prototype.u.call(this), t.O.prototype.u.call(this)
    }, t.Ia.prototype.f = function() {
        return t.w.prototype.f.call(this, "div", {
            className: "vjs-volume-menu-button vjs-menu-button vjs-control",
            innerHTML: '<div><span class="vjs-control-text">' + this.v("Mute") + "</span></div>"
        })
    }, t.Ia.prototype.ff = t.sa.prototype.update, t.pc = t.O.extend({
        l: function(a, c) {
            t.O.call(this, a, c), this.td(), this.sd(), this.b(a, "loadstart", this.td), this.b(a, "ratechange", this.sd)
        }
    }), s = t.pc.prototype, s.ua = "Playback Rate", s.className = "vjs-playback-rate", s.f = function() {
        var a = t.O.prototype.f.call(this);
        return this.Sc = t.f("div", {
            className: "vjs-playback-rate-value",
            innerHTML: 1
        }), a.appendChild(this.Sc), a
    }, s.Ma = function() {
        var a = new t.ra(this.k()),
            c = this.k().options().playbackRates;
        if (c)
            for (var d = c.length - 1; d >= 0; d--) a.ba(new t.Db(this.k(), {
                rate: c[d] + "x"
            }));
        return a
    }, s.Fa = function() {
        this.m().setAttribute("aria-valuenow", this.k().playbackRate());
    }, s.u = function() {
        for (var a = this.k().playbackRate(), c = this.k().options().playbackRates, d = c[0], e = 0; e < c.length; e++)
            if (c[e] > a) {
                d = c[e];
                break
            }
        this.k().playbackRate(d)
    }, s.td = function() {
        na(this) ? this.r("vjs-hidden") : this.p("vjs-hidden")
    }, s.sd = function() {
        na(this) && (this.Sc.innerHTML = this.k().playbackRate() + "x")
    }, t.Db = t.M.extend({
        Dc: "button",
        l: function(a, c) {
            var d = this.label = c.rate,
                e = this.$c = parseFloat(d, 10);
            c.label = d, c.selected = 1 === e, t.M.call(this, a, c), this.b(a, "ratechange", this.update)
        }
    }), t.Db.prototype.u = function() {
        t.M.prototype.u.call(this), this.k().playbackRate(this.$c)
    }, t.Db.prototype.update = function() {
        this.selected(this.k().playbackRate() == this.$c)
    }, t.qc = t.w.extend({
        l: function(a, c) {
            t.w.call(this, a, c), this.update(), a.b("posterchange", t.bind(this, this.update))
        }
    }), s = t.qc.prototype, s.dispose = function() {
        this.k().n("posterchange", this.update), t.w.prototype.dispose.call(this)
    }, s.f = function() {
        var a = t.f("div", {
            className: "vjs-poster",
            tabIndex: -1
        });
        return t.xd || (this.Pb = t.f("img"), a.appendChild(this.Pb)), a
    }, s.update = function() {
        var a = this.k().poster();
        this.oa(a), a ? this.show() : this.Y()
    }, s.oa = function(a) {
        var c;
        this.Pb ? this.Pb.src = a : (c = "", a && (c = 'url("' + a + '")'), this.c.style.backgroundImage = c)
    }, s.u = function() {
        this.d.play()
    }, t.mc = t.a.extend({
        l: function(a, c) {
            t.a.call(this, a, c)
        }
    }), t.mc.prototype.f = function() {
        return t.a.prototype.f.call(this, "div", {
            className: "vjs-loading-spinner"
        })
    }, t.ub = t.w.extend(), t.ub.prototype.f = function() {
        return t.w.prototype.f.call(this, "div", {
            className: "vjs-big-play-button",
            innerHTML: '<span aria-hidden="true"></span>',
            "aria-label": "play video"
        })
    }, t.ub.prototype.u = function() {
        this.d.play()
    }, t.zb = t.a.extend({
        l: function(a, c) {
            t.a.call(this, a, c), this.update(), this.b(a, "error", this.update)
        }
    }), t.zb.prototype.f = function() {
        var a = t.a.prototype.f.call(this, "div", {
            className: "vjs-error-display"
        });
        return this.B = t.f("div"), a.appendChild(this.B), a
    }, t.zb.prototype.update = function() {
        this.k().error() && (this.B.innerHTML = this.v(this.k().error().message))
    };
    var O;
    t.j = t.a.extend({
        l: function(a, c, d) {
            c = c || {}, c.dd = l, t.a.call(this, a, c, d), this.featuresProgressEvents || this.se(), this.featuresTimeupdateEvents || this.te(), this.ge(), this.featuresNativeTextTracks || this.Wd(), this.ie()
        }
    }), s = t.j.prototype, s.ge = function() {
        var a, c;
        a = this.k(), c = function() {
            a.controls() && !a.usingNativeControls() && this.Jd()
        }, this.I(c), this.b(a, "controlsenabled", c), this.b(a, "controlsdisabled", this.Ie), this.I(function() {
            this.networkState && 0 < this.networkState() && this.k().o("loadstart")
        })
    }, s.Jd = function() {
        var a;
        this.b("mousedown", this.u), this.b("touchstart", function() {
            a = this.d.userActive()
        }), this.b("touchmove", function() {
            a && this.k().reportUserActivity()
        }), this.b("touchend", function(a) {
            a.preventDefault()
        }), da(this), this.b("tap", this.Ce)
    }, s.Ie = function() {
        this.n("tap"), this.n("touchstart"), this.n("touchmove"), this.n("touchleave"), this.n("touchcancel"), this.n("touchend"), this.n("click"), this.n("mousedown")
    }, s.u = function(a) {
        0 === a.button && this.k().controls() && (this.k().paused() ? this.k().play() : this.k().pause())
    }, s.Ce = function() {
        this.k().userActive(!this.k().userActive())
    }, s.se = function() {
        this.Uc = f, this.af()
    }, s.re = function() {
        this.Uc = l, this.md()
    }, s.af = function() {
        this.He = this.setInterval(function() {
            var a = this.k().bufferedPercent();
            this.Nd != a && this.k().o("progress"), this.Nd = a, 1 === a && this.md()
        }, 500)
    }, s.md = function() {
        this.clearInterval(this.He)
    }, s.te = function() {
        var a = this.d;
        this.Zb = f, this.b(a, "play", this.qd), this.b(a, "pause", this.tb), this.N("timeupdate", function() {
            this.featuresTimeupdateEvents = f, this.Vc()
        })
    }, s.Vc = function() {
        var a = this.d;
        this.Zb = l, this.tb(), this.n(a, "play", this.qd), this.n(a, "pause", this.tb)
    }, s.qd = function() {
        this.Fc && this.tb(), this.Fc = this.setInterval(function() {
            this.k().o("timeupdate")
        }, 250)
    }, s.tb = function() {
        this.clearInterval(this.Fc), this.k().o("timeupdate")
    }, s.dispose = function() {
        this.Uc && this.re(), this.Zb && this.Vc(), t.a.prototype.dispose.call(this)
    }, s.cc = function() {
        this.Zb && this.k().o("timeupdate")
    }, s.ie = function() {
        function a() {
            var a = c.ea("textTrackDisplay");
            a && a.C()
        }
        var d, c = this.d;
        (d = this.textTracks()) && (d.addEventListener("removetrack", a), d.addEventListener("addtrack", a), this.b("dispose", t.bind(this, function() {
            d.removeEventListener("removetrack", a), d.removeEventListener("addtrack", a)
        })))
    }, s.Wd = function() {
        var c, d, e, a = this.d;
        window.WebVTT || (e = document.createElement("script"), e.src = a.options()["vtt.js"] || "../node_modules/vtt.js/dist/vtt.js", a.m().appendChild(e), window.WebVTT = f), (d = this.textTracks()) && (c = function() {
            var c, d, e;
            for (e = a.ea("textTrackDisplay"), e.C(), c = 0; c < this.length; c++) d = this[c], d.removeEventListener("cuechange", t.bind(e, e.C)), "showing" === d.mode && d.addEventListener("cuechange", t.bind(e, e.C))
        }, d.addEventListener("change", c), this.b("dispose", t.bind(this, function() {
            d.removeEventListener("change", c)
        })))
    }, s.textTracks = function() {
        return this.d.pd = this.d.pd || new t.F, this.d.pd
    }, s.Z = function() {
        return this.d.ad = this.d.ad || new t.F, this.d.ad
    }, O = function(a, c, d, e, g) {
        var h = a.textTracks();
        return g = g || {}, g.kind = c, d && (g.label = d), e && (g.language = e), g.player = a.d, a = new t.t(g), P(h, a), a
    }, t.j.prototype.addTextTrack = function(a, c, d) {
        if (!a) throw Error("TextTrack kind is required but was not provided");
        return O(this, a, c, d)
    }, t.j.prototype.ia = function(a) {
        return a = O(this, a.kind, a.label, a.language, a), P(this.Z(), a), {
            T: a
        }
    }, t.j.prototype.Ea = function(a) {
        Q(this.textTracks(), a), Q(this.Z(), a)
    }, t.j.prototype.fd = m(), t.j.prototype.featuresVolumeControl = f, t.j.prototype.featuresFullscreenResize = l, t.j.prototype.featuresPlaybackRate = l, t.j.prototype.featuresProgressEvents = l, t.j.prototype.featuresTimeupdateEvents = l, t.j.prototype.featuresNativeTextTracks = l, t.j.hc = function(a) {
        a.Ta = function(c, d) {
            var e = a.gd;
            e || (e = a.gd = []), d === b && (d = e.length), e.splice(d, 0, c)
        }, a.rb = function(c) {
            for (var e, d = a.gd || [], g = 0; g < d.length; g++)
                if (e = d[g].fb(c)) return d[g];
            return j
        }, a.Ac = function(c) {
            var d = a.rb(c);
            return d ? d.fb(c) : ""
        }, a.prototype.na = function(c) {
            var d = a.rb(c);
            return d || (a.S ? d = a.S : t.log.error("No source hander found for the current source.")), this.ja(), this.n("dispose", this.ja), this.ib = c, this.dc = d.Ub(c, this), this.b("dispose", this.ja), this
        }, a.prototype.ja = function() {
            this.dc && this.dc.dispose && this.dc.dispose()
        }
    }, t.media = {}, t.e = t.j.extend({
        l: function(a, c, d) {
            var e, g, h;
            for ((c.nativeCaptions === l || c.nativeTextTracks === l) && (this.featuresNativeTextTracks = l), t.j.call(this, a, c, d), d = t.e.Ab.length - 1; d >= 0; d--) this.b(t.e.Ab[d], this.Xd);
            if ((c = c.source) && (this.c.currentSrc !== c.src || a.L && 3 === a.L.he) && this.na(c), this.c.hasChildNodes()) {
                for (d = this.c.childNodes, e = d.length, c = []; e--;) g = d[e], h = g.nodeName.toLowerCase(), "track" === h && (this.featuresNativeTextTracks ? P(this.Z(), g.track) : c.push(g));
                for (d = 0; d < c.length; d++) this.c.removeChild(c[d])
            }
            if (this.featuresNativeTextTracks && this.b("loadstart", t.bind(this, this.fe)), t.Gb && a.options().nativeControlsForTouch === f) {
                var k, q, r, u;
                k = this, q = this.k(), c = q.controls(), k.c.controls = !!c, r = function() {
                    k.c.controls = f
                }, u = function() {
                    k.c.controls = l
                }, q.b("controlsenabled", r), q.b("controlsdisabled", u), c = function() {
                    q.n("controlsenabled", r), q.n("controlsdisabled", u)
                }, k.b("dispose", c), q.b("usingcustomcontrols", c), q.usingNativeControls(f)
            }
            a.I(function() {
                this.src() && this.L && this.q.autoplay && this.paused() && (delete this.L.poster, this.play())
            }), this.Xa()
        }
    }), s = t.e.prototype, s.dispose = function() {
        t.e.Nb(this.c), t.j.prototype.dispose.call(this)
    }, s.f = function() {
        var c, d, e, a = this.d,
            g = a.L;
        if (!g || this.movingMediaElementInDOM === l) {
            if (g ? (e = g.cloneNode(l), t.e.Nb(g), g = e, a.L = j) : (g = t.f("video"), e = videojs.$.Ba({}, a.Ye), (!t.Gb || a.options().nativeControlsForTouch !== f) && delete e.controls, t.ed(g, t.i.D(e, {
                    id: a.id() + "_html5_api",
                    "class": "vjs-tech"
                }))), g.player = a, a.q.rd)
                for (e = 0; e < a.q.rd.length; e++) c = a.q.rd[e], d = document.createElement("track"), d.Xb = c.Xb, d.label = c.label, d.jd = c.jd, d.src = c.src, "default" in c && d.setAttribute("default", "default"), g.appendChild(d);
            t.Vb(g, a.m())
        }
        for (c = ["autoplay", "preload", "loop", "muted"], e = c.length - 1; e >= 0; e--) {
            d = c[e];
            var h = {};
            "undefined" != typeof a.q[d] && (h[d] = a.q[d]), t.ed(g, h)
        }
        return g
    }, s.fe = function() {
        for (var c, a = this.c.querySelectorAll("track"), d = a.length, e = {
                captions: 1,
                subtitles: 1
            }; d--;)(c = a[d].T) && c.kind in e && !a[d]["default"] && (c.mode = "disabled")
    }, s.Xd = function(a) {
        "error" == a.type && this.error() ? this.k().error(this.error().code) : (a.bubbles = l, this.k().o(a))
    }, s.play = function() {
        this.c.play()
    }, s.pause = function() {
        this.c.pause()
    }, s.paused = function() {
        return this.c.paused
    }, s.currentTime = function() {
        return this.c.currentTime
    }, s.cc = function(a) {
        try {
            this.c.currentTime = a
        } catch (c) {
            t.log(c, "Video is not ready. (Video.js)")
        }
    }, s.duration = function() {
        return this.c.duration || 0
    }, s.buffered = function() {
        return this.c.buffered
    }, s.volume = function() {
        return this.c.volume
    }, s.Te = function(a) {
        this.c.volume = a
    }, s.muted = function() {
        return this.c.muted
    }, s.Pe = function(a) {
        this.c.muted = a
    }, s.width = function() {
        return this.c.offsetWidth
    }, s.height = function() {
        return this.c.offsetHeight
    }, s.Ua = function() {
        return "function" != typeof this.c.webkitEnterFullScreen || !/Android/.test(t.P) && /Chrome|Mac OS X 10.5/.test(t.P) ? l : f
    }, s.Ic = function() {
        var a = this.c;
        "webkitDisplayingFullscreen" in a && this.N("webkitbeginfullscreen", function() {
            this.d.isFullscreen(f), this.N("webkitendfullscreen", function() {
                this.d.isFullscreen(l), this.d.o("fullscreenchange")
            }), this.d.o("fullscreenchange")
        }), a.paused && a.networkState <= a.kf ? (this.c.play(), this.setTimeout(function() {
            a.pause(), a.webkitEnterFullScreen()
        }, 0)) : a.webkitEnterFullScreen()
    }, s.Yd = function() {
        this.c.webkitExitFullScreen()
    }, s.src = function(a) {
        var c = this.c.src;
        return a === b ? oa(c, this.hd) : void this.oa(a)
    }, s.oa = function(a) {
        this.c.src = a
    }, s.load = function() {
        this.c.load()
    }, s.currentSrc = function() {
        var a = this.c.currentSrc;
        return this.ib ? oa(a, this.ib.src) : a
    }, s.poster = function() {
        return this.c.poster
    }, s.fd = function(a) {
        this.c.poster = a
    }, s.Sa = function() {
        return this.c.Sa
    }, s.Re = function(a) {
        this.c.Sa = a
    }, s.autoplay = function() {
        return this.c.autoplay
    }, s.Me = function(a) {
        this.c.autoplay = a
    }, s.controls = function() {
        return this.c.controls
    }, s.loop = function() {
        return this.c.loop
    }, s.Oe = function(a) {
        this.c.loop = a
    }, s.error = function() {
        return this.c.error
    }, s.seeking = function() {
        return this.c.seeking
    }, s.seekable = function() {
        return this.c.seekable
    }, s.ended = function() {
        return this.c.ended
    }, s.playbackRate = function() {
        return this.c.playbackRate
    }, s.Qe = function(a) {
        this.c.playbackRate = a
    }, s.networkState = function() {
        return this.c.networkState
    }, s.readyState = function() {
        return this.c.readyState
    }, s.textTracks = function() {
        return this.featuresNativeTextTracks ? this.c.textTracks : t.j.prototype.textTracks.call(this)
    }, s.addTextTrack = function(a, c, d) {
        return this.featuresNativeTextTracks ? this.c.addTextTrack(a, c, d) : t.j.prototype.addTextTrack.call(this, a, c, d)
    }, s.ia = function(a) {
        if (!this.featuresNativeTextTracks) return t.j.prototype.ia.call(this, a);
        var c = document.createElement("track");
        return a = a || {}, a.kind && (c.kind = a.kind), a.label && (c.label = a.label), (a.language || a.srclang) && (c.srclang = a.language || a.srclang), a["default"] && (c["default"] = a["default"]), a.id && (c.id = a.id), a.src && (c.src = a.src), this.m().appendChild(c), c.track.mode = "metadata" === c.T.kind ? "hidden" : "disabled", c.onload = function() {
            var a = c.track;
            2 <= c.readyState && ("metadata" === a.kind && "hidden" !== a.mode ? a.mode = "hidden" : "metadata" !== a.kind && "disabled" !== a.mode && (a.mode = "disabled"), c.onload = j)
        }, P(this.Z(), c.T), c
    }, s.Ea = function(a) {
        if (!this.featuresNativeTextTracks) return t.j.prototype.Ea.call(this, a);
        var c, d;
        for (Q(this.Z(), a), c = this.m().querySelectorAll("track"), d = 0; d < c.length; d++)
            if (c[d] === a || c[d].track === a) {
                c[d].parentNode.removeChild(c[d]);
                break
            }
    }, t.e.isSupported = function() {
        try {
            t.A.volume = .5
        } catch (a) {
            return l
        }
        return !!t.A.canPlayType
    }, t.j.hc(t.e);
    var pa = t.e.prototype.na,
        qa = t.e.prototype.ja;
    t.e.prototype.na = function(a) {
        var c = pa.call(this, a);
        return this.hd = a.src, c
    }, t.e.prototype.ja = function() {
        return this.hd = b, qa.call(this)
    }, t.e.S = {}, t.e.S.fb = function(a) {
        function c(a) {
            try {
                return t.A.canPlayType(a)
            } catch (c) {
                return ""
            }
        }
        return a.type ? c(a.type) : a.src ? (a = (a = a.src.match(/\.([^.\/\?]+)(\?[^\/]+)?$/i)) && a[1], c("video/" + a)) : ""
    }, t.e.S.Ub = function(a, c) {
        c.oa(a.src)
    }, t.e.S.dispose = m(), t.e.Ta(t.e.S), t.e.Pd = function() {
        var a = t.A.volume;
        return t.A.volume = a / 2 + .1, a !== t.A.volume
    }, t.e.Od = function() {
        var a = t.A.playbackRate;
        return t.A.playbackRate = a / 2 + .1, a !== t.A.playbackRate
    }, t.e.We = function() {
        var a;
        return (a = !!t.A.textTracks) && 0 < t.A.textTracks.length && (a = "number" != typeof t.A.textTracks[0].mode), a && t.kc && (a = l), a
    }, t.e.prototype.featuresVolumeControl = t.e.Pd(), t.e.prototype.featuresPlaybackRate = t.e.Od(), t.e.prototype.movingMediaElementInDOM = !t.Bd, t.e.prototype.featuresFullscreenResize = f, t.e.prototype.featuresProgressEvents = f, t.e.prototype.featuresNativeTextTracks = t.e.We();
    var S, ra = /^application\/(?:x-|vnd\.apple\.)mpegurl/i,
        sa = /^video\/mp4/i;
    t.e.Xc = function() {
        4 <= t.ic && (S || (S = t.A.constructor.prototype.canPlayType), t.A.constructor.prototype.canPlayType = function(a) {
            return a && ra.test(a) ? "maybe" : S.call(this, a)
        }), t.Fd && (S || (S = t.A.constructor.prototype.canPlayType), t.A.constructor.prototype.canPlayType = function(a) {
            return a && sa.test(a) ? "maybe" : S.call(this, a)
        })
    }, t.e.cf = function() {
        var a = t.A.constructor.prototype.canPlayType;
        return t.A.constructor.prototype.canPlayType = S, S = j, a
    }, t.e.Xc(), t.e.Ab = "loadstart suspend abort error emptied stalled loadedmetadata loadeddata canplay canplaythrough playing waiting seeking seeked ended durationchange timeupdate progress play pause ratechange volumechange".split(" "), t.e.Nb = function(a) {
        if (a) {
            for (a.player = j, a.parentNode && a.parentNode.removeChild(a); a.hasChildNodes();) a.removeChild(a.firstChild);
            if (a.removeAttribute("src"), "function" == typeof a.load) try {
                a.load()
            } catch (c) {}
        }
    }, t.g = t.j.extend({
        l: function(a, c, d) {
            t.j.call(this, a, c, d);
            var e = c.source;
            d = a.id() + "_flash_api";
            var g = a.q,
                g = t.i.D({
                    readyFunction: "videojs.Flash.onReady",
                    eventProxyFunction: "videojs.Flash.onEvent",
                    errorEventProxyFunction: "videojs.Flash.onError",
                    autoplay: g.autoplay,
                    preload: g.Sa,
                    loop: g.loop,
                    muted: g.muted
                }, c.flashVars),
                h = t.i.D({
                    wmode: "opaque",
                    bgcolor: "#000000"
                }, c.params);
            d = t.i.D({
                id: d,
                name: d,
                "class": "vjs-tech"
            }, c.attributes), e && this.I(function() {
                this.na(e)
            }), t.Vb(this.c, c.parentEl), c.startTime && this.I(function() {
                this.load(), this.play(), this.currentTime(c.startTime)
            }), t.kc && this.I(function() {
                this.b("mousemove", function() {
                    this.k().o({
                        type: "mousemove",
                        bubbles: l
                    })
                })
            }), a.b("stageclick", a.reportUserActivity), this.c = t.g.Hc(c.swf, this.c, g, h, d)
        }
    }), s = t.g.prototype, s.dispose = function() {
        t.j.prototype.dispose.call(this)
    }, s.play = function() {
        this.c.vjs_play()
    }, s.pause = function() {
        this.c.vjs_pause()
    }, s.src = function(a) {
        return a === b ? this.currentSrc() : this.oa(a)
    }, s.oa = function(a) {
        if (a = t.ae(a), this.c.vjs_src(a), this.d.autoplay()) {
            var c = this;
            this.setTimeout(function() {
                c.play()
            }, 0)
        }
    }, t.g.prototype.setCurrentTime = function(a) {
        this.pe = a, this.c.vjs_setProperty("currentTime", a), t.j.prototype.cc.call(this)
    }, t.g.prototype.currentTime = function() {
        return this.seeking() ? this.pe || 0 : this.c.vjs_getProperty("currentTime")
    }, t.g.prototype.currentSrc = function() {
        return this.ib ? this.ib.src : this.c.vjs_getProperty("currentSrc")
    }, t.g.prototype.load = function() {
        this.c.vjs_load()
    }, t.g.prototype.poster = function() {
        this.c.vjs_getProperty("poster")
    }, t.g.prototype.setPoster = m(), s = t.g.prototype, s.seekable = function() {
        return 0 === this.duration() ? t.ya() : t.ya(0, this.duration())
    }, s.buffered = function() {
        return this.c.vjs_getProperty ? t.ya(0, this.c.vjs_getProperty("buffered")) : t.ya()
    }, s.duration = function() {
        return this.c.vjs_getProperty ? this.c.vjs_getProperty("duration") : 0
    }, s.Ua = p(l), s.Ic = p(l);
    var ua = t.g.prototype,
        T = "rtmpConnection rtmpStream preload defaultPlaybackRate playbackRate autoplay loop mediaGroup controller controls volume muted defaultMuted".split(" "),
        wa = "error networkState readyState seeking initialTime startOffsetTime paused played ended videoTracks audioTracks videoWidth videoHeight".split(" "),
        U;
    for (U = 0; U < T.length; U++) va(T[U]), ta();
    for (U = 0; U < wa.length; U++) va(wa[U]);
    t.g.isSupported = function() {
        return 10 <= t.g.version()[0]
    }, t.j.hc(t.g), t.g.S = {}, t.g.S.fb = function(a) {
        return a.type && a.type.replace(/;.*/, "").toLowerCase() in t.g.$d ? "maybe" : ""
    }, t.g.S.Ub = function(a, c) {
        c.oa(a.src)
    }, t.g.S.dispose = m(), t.g.Ta(t.g.S), t.g.$d = {
        "video/flv": "FLV",
        "video/x-flv": "FLV",
        "video/mp4": "MP4",
        "video/m4v": "MP4"
    }, t.g.onReady = function(a) {
        var c;
        (c = (a = t.m(a)) && a.parentNode && a.parentNode.player) && (a.player = c, t.g.checkReady(c.h))
    }, t.g.checkReady = function(a) {
        a.m() && (a.m().vjs_getProperty ? a.Xa() : this.setTimeout(function() {
            t.g.checkReady(a)
        }, 50))
    }, t.g.onEvent = function(a, c) {
        t.m(a).player.o(c)
    }, t.g.onError = function(a, c) {
        var d = t.m(a).player,
            e = "FLASH: " + c;
        "srcnotfound" == c ? d.error({
            code: 4,
            message: e
        }) : d.error(e)
    }, t.g.version = function() {
        var a = "0,0,0";
        try {
            a = new window.ActiveXObject("ShockwaveFlash.ShockwaveFlash").GetVariable("$version").replace(/\D+/g, ",").match(/^,?(.+),?$/)[1]
        } catch (c) {
            try {
                navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin && (a = (navigator.plugins["Shockwave Flash 2.0"] || navigator.plugins["Shockwave Flash"]).description.replace(/\D+/g, ",").match(/^,?(.+),?$/)[1])
            } catch (d) {}
        }
        return a.split(",")
    }, t.g.Hc = function(a, c, d, e, g) {
        a = t.g.de(a, d, e, g), a = t.f("div", {
            innerHTML: a
        }).childNodes[0], d = c.parentNode, c.parentNode.replaceChild(a, c), a[t.expando] = c[t.expando];
        var h = d.childNodes[0];
        return setTimeout(function() {
            h.style.display = "block"
        }, 1e3), a
    }, t.g.de = function(a, c, d, e) {
        var g = "",
            h = "",
            k = "";
        return c && t.i.da(c, function(a, c) {
            g += a + "=" + c + "&amp;"
        }), d = t.i.D({
            movie: a,
            flashvars: g,
            allowScriptAccess: "always",
            allowNetworking: "all"
        }, d), t.i.da(d, function(a, c) {
            h += '<param name="' + a + '" value="' + c + '" />'
        }), e = t.i.D({
            data: a,
            width: "100%",
            height: "100%"
        }, e), t.i.da(e, function(a, c) {
            k += a + '="' + c + '" '
        }), '<object type="application/x-shockwave-flash" ' + k + ">" + h + "</object>"
    }, t.g.Ve = {
        "rtmp/mp4": "MP4",
        "rtmp/flv": "FLV"
    }, t.g.If = function(a, c) {
        return a + "&" + c
    }, t.g.Ue = function(a) {
        var c = {
            Cc: "",
            nd: ""
        };
        if (!a) return c;
        var e, d = a.indexOf("&");
        return -1 !== d ? e = d + 1 : (d = e = a.lastIndexOf("/") + 1, 0 === d && (d = e = a.length)), c.Cc = a.substring(0, d), c.nd = a.substring(e, a.length), c
    }, t.g.ne = function(a) {
        return a in t.g.Ve
    }, t.g.Hd = /^rtmp[set]?:\/\//i, t.g.me = function(a) {
        return t.g.Hd.test(a)
    }, t.g.bc = {}, t.g.bc.fb = function(a) {
        return t.g.ne(a.type) || t.g.me(a.src) ? "maybe" : ""
    }, t.g.bc.Ub = function(a, c) {
        var d = t.g.Ue(a.src);
        c.setRtmpConnection(d.Cc), c.setRtmpStream(d.nd)
    }, t.g.Ta(t.g.bc), t.Gd = t.a.extend({
        l: function(a, c, d) {
            if (t.a.call(this, a, c, d), a.q.sources && 0 !== a.q.sources.length) a.src(a.q.sources);
            else
                for (c = 0, d = a.q.techOrder; c < d.length; c++) {
                    var e = t.wa(d[c]),
                        g = window.videojs[e];
                    if (g && g.isSupported()) {
                        ka(a, e);
                        break
                    }
                }
        }
    }), t.sc = {
        disabled: "disabled",
        hidden: "hidden",
        showing: "showing"
    }, t.Id = {
        subtitles: "subtitles",
        captions: "captions",
        descriptions: "descriptions",
        chapters: "chapters",
        metadata: "metadata"
    }, t.t = function(a) {
        var c, d, e, g, h, k, q, r, u, A, R;
        if (a = a || {}, !a.player) throw Error("A player was not provided.");
        if (c = this, t.qa)
            for (R in c = document.createElement("custom"), t.t.prototype) c[R] = t.t.prototype[R];
        return c.d = a.player, e = t.sc[a.mode] || "disabled", g = t.Id[a.kind] || "subtitles", h = a.label || "", k = a.language || a.srclang || "", d = a.id || "vjs_text_track_" + t.s++, ("metadata" === g || "chapters" === g) && (e = "hidden"), c.X = [], c.Ja = [], q = new t.W(c.X), r = new t.W(c.Ja), A = l, u = t.bind(c, function() {
            this.activeCues, A && (this.trigger("cuechange"), A = l)
        }), "disabled" !== e && c.d.b("timeupdate", u), Object.defineProperty(c, "kind", {
            get: function() {
                return g
            },
            set: Function.prototype
        }), Object.defineProperty(c, "label", {
            get: function() {
                return h
            },
            set: Function.prototype
        }), Object.defineProperty(c, "language", {
            get: function() {
                return k
            },
            set: Function.prototype
        }), Object.defineProperty(c, "id", {
            get: function() {
                return d
            },
            set: Function.prototype
        }), Object.defineProperty(c, "mode", {
            get: function() {
                return e
            },
            set: function(a) {
                t.sc[a] && (e = a, "showing" === e && this.d.b("timeupdate", u), this.o("modechange"))
            }
        }), Object.defineProperty(c, "cues", {
            get: function() {
                return this.Yb ? q : j
            },
            set: Function.prototype
        }), Object.defineProperty(c, "activeCues", {
            get: function() {
                var a, c, d, e, g;
                if (!this.Yb) return j;
                if (0 === this.cues.length) return r;
                for (e = this.d.currentTime(), a = 0, c = this.cues.length, d = []; c > a; a++) g = this.cues[a], g.startTime <= e && g.endTime >= e ? d.push(g) : g.startTime === g.endTime && g.startTime <= e && g.startTime + .5 >= e && d.push(g);
                if (A = l, d.length !== this.Ja.length) A = f;
                else
                    for (a = 0; a < d.length; a++) - 1 === xa.call(this.Ja, d[a]) && (A = f);
                return this.Ja = d, r.sb(this.Ja), r
            },
            set: Function.prototype
        }), a.src ? ya(a.src, c) : c.Yb = f, t.qa ? c : void 0
    }, t.t.prototype = t.i.create(t.z.prototype), t.t.prototype.constructor = t.t, t.t.prototype.cb = {
        cuechange: "cuechange"
    }, t.t.prototype.wc = function(a) {
        var c = this.d.textTracks(),
            d = 0;
        if (c)
            for (; d < c.length; d++) c[d] !== this && c[d].bd(a);
        this.X.push(a), this.cues.sb(this.X)
    }, t.t.prototype.bd = function(a) {
        for (var e, c = 0, d = this.X.length, g = l; d > c; c++) e = this.X[c], e === a && (this.X.splice(c, 1), g = f);
        g && this.Ec.sb(this.X)
    };
    var ya, V, xa;
    ya = function(a, c) {
        t.gf(a, t.bind(this, function(a, e, g) {
            return a ? t.log.error(a) : (c.Yb = f, void V(g, c))
        }))
    }, V = function(a, c) {
        if ("function" != typeof window.WebVTT) window.setTimeout(function() {
            V(a, c)
        }, 25);
        else {
            var d = new window.WebVTT.Parser(window, window.vttjs, window.WebVTT.StringDecoder());
            d.oncue = function(a) {
                c.wc(a)
            }, d.onparsingerror = function(a) {
                t.log.error(a)
            }, d.parse(a), d.flush()
        }
    }, xa = function(a, c) {
        var d;
        if (this == j) throw new TypeError('"this" is null or not defined');
        var e = Object(this),
            g = e.length >>> 0;
        if (0 === g) return -1;
        if (d = +c || 0, 1 / 0 === Math.abs(d) && (d = 0), d >= g) return -1;
        for (d = Math.max(d >= 0 ? d : g - Math.abs(d), 0); g > d;) {
            if (d in e && e[d] === a) return d;
            d++
        }
        return -1
    }, t.F = function(a) {
        var d, c = this,
            e = 0;
        if (t.qa)
            for (d in c = document.createElement("custom"), t.F.prototype) c[d] = t.F.prototype[d];
        for (a = a || [], c.Wa = [], Object.defineProperty(c, "length", {
                get: function() {
                    return this.Wa.length
                }
            }); e < a.length; e++) P(c, a[e]);
        return t.qa ? c : void 0
    }, t.F.prototype = t.i.create(t.z.prototype), t.F.prototype.constructor = t.F, t.F.prototype.cb = {
        change: "change",
        addtrack: "addtrack",
        removetrack: "removetrack"
    };
    for (var za in t.F.prototype.cb) t.F.prototype["on" + za] = j;
    t.F.prototype.ee = function(a) {
        for (var g, c = 0, d = this.length, e = j; d > c; c++)
            if (g = this[c], g.id === a) {
                e = g;
                break
            }
        return e
    }, t.W = function(a) {
        var d, c = this;
        if (t.qa)
            for (d in c = document.createElement("custom"), t.W.prototype) c[d] = t.W.prototype[d];
        return t.W.prototype.sb.call(c, a), Object.defineProperty(c, "length", {
            get: n("qe")
        }), t.qa ? c : void 0
    }, t.W.prototype.sb = function(a) {
        var c = this.length || 0,
            d = 0,
            e = a.length;
        if (this.X = a, this.qe = a.length, a = function(a) {
                "" + a in this || Object.defineProperty(this, "" + a, {
                    get: function() {
                        return this.X[a]
                    }
                })
            }, e > c)
            for (d = c; e > d; d++) a.call(this, d)
    }, t.W.prototype.ce = function(a) {
        for (var g, c = 0, d = this.length, e = j; d > c; c++)
            if (g = this[c], g.id === a) {
                e = g;
                break
            }
        return e
    }, t.ta = t.a.extend({
        l: function(a, c, d) {
            t.a.call(this, a, c, d), a.b("loadstart", t.bind(this, this.$e)), a.I(t.bind(this, function() {
                if (a.h && a.h.featuresNativeTextTracks) this.Y();
                else {
                    var c, d, h;
                    for (a.b("fullscreenchange", t.bind(this, this.C)), d = a.q.tracks || [], c = 0; c < d.length; c++) h = d[c], this.d.ia(h)
                }
            }))
        }
    }), t.ta.prototype.$e = function() {
        this.d.h && this.d.h.featuresNativeTextTracks ? this.Y() : this.show()
    }, t.ta.prototype.f = function() {
        return t.a.prototype.f.call(this, "div", {
            className: "vjs-text-track-display"
        })
    }, t.ta.prototype.Qd = function() {
        "function" == typeof window.WebVTT && window.WebVTT.processCues(window, [], this.c)
    };
    var Aa = {
        yf: "monospace",
        Ef: "sans-serif",
        Gf: "serif",
        zf: '"Andale Mono", "Lucida Console", monospace',
        Af: '"Courier New", monospace',
        Cf: "sans-serif",
        Df: "serif",
        pf: '"Comic Sans MS", Impact, fantasy',
        Ff: '"Monotype Corsiva", cursive',
        Hf: '"Andale Mono", "Lucida Console", monospace, sans-serif'
    };
    if (t.ta.prototype.C = function() {
            var d, a = this.d.textTracks(),
                c = 0;
            if (this.Qd(), a)
                for (; c < a.length; c++) d = a[c], "showing" === d.mode && this.df(d)
        }, t.ta.prototype.df = function(a) {
            if ("function" == typeof window.WebVTT && a.activeCues) {
                for (var e, c = 0, d = this.d.textTrackSettings.Lc(), g = []; c < a.activeCues.length; c++) g.push(a.activeCues[c]);
                for (window.WebVTT.processCues(window, a.activeCues, this.c), c = g.length; c--;) {
                    if (a = g[c].qf, d.color && (a.firstChild.style.color = d.color), d.od) try {
                        a.firstChild.style.color = W(d.color || "#fff", d.od)
                    } catch (h) {}
                    if (d.backgroundColor && (a.firstChild.style.backgroundColor = d.backgroundColor), d.zc) try {
                        a.firstChild.style.backgroundColor = W(d.backgroundColor || "#000", d.zc)
                    } catch (k) {}
                    if (d.gc)
                        if (d.vd) try {
                            a.style.backgroundColor = W(d.gc, d.vd)
                        } catch (q) {} else a.style.backgroundColor = d.gc;
                    d.Na && ("dropshadow" === d.Na ? a.firstChild.style.textShadow = "2px 2px 3px #222, 2px 2px 4px #222, 2px 2px 5px #222" : "raised" === d.Na ? a.firstChild.style.textShadow = "1px 1px #222, 2px 2px #222, 3px 3px #222" : "depressed" === d.Na ? a.firstChild.style.textShadow = "1px 1px #ccc, 0 1px #ccc, -1px -1px #222, 0 -1px #222" : "uniform" === d.Na && (a.firstChild.style.textShadow = "0 0 4px #222, 0 0 4px #222, 0 0 4px #222, 0 0 4px #222")), d.Rb && 1 !== d.Rb && (e = window.Bf(a.style.fontSize), a.style.fontSize = e * d.Rb + "px", a.style.height = "auto", a.style.top = "auto", a.style.bottom = "2px"), d.fontFamily && "default" !== d.fontFamily && ("small-caps" === d.fontFamily ? a.firstChild.style.fontVariant = "small-caps" : a.firstChild.style.fontFamily = Aa[d.fontFamily])
                }
            }
        }, t.aa = t.M.extend({
            l: function(a, c) {
                var g, h, d = this.T = c.track,
                    e = a.textTracks();
                e && (g = t.bind(this, function() {
                    var c, d, g, a = "showing" === this.T.mode;
                    if (this instanceof t.Cb)
                        for (a = f, d = 0, g = e.length; g > d; d++)
                            if (c = e[d], c.kind === this.T.kind && "showing" === c.mode) {
                                a = l;
                                break
                            }
                    this.selected(a)
                }), e.addEventListener("change", g), a.b("dispose", function() {
                    e.removeEventListener("change", g)
                })), c.label = d.label || d.language || "Unknown", c.selected = d["default"] || "showing" === d.mode, t.M.call(this, a, c), e && e.onchange === b && this.b(["tap", "click"], function() {
                    if ("object" != typeof window.zd) try {
                        h = new window.zd("change")
                    } catch (a) {}
                    h || (h = document.createEvent("Event"), h.initEvent("change", f, f)), e.dispatchEvent(h)
                })
            }
        }), t.aa.prototype.u = function() {
            var d, a = this.T.kind,
                c = this.d.textTracks(),
                e = 0;
            if (t.M.prototype.u.call(this), c)
                for (; e < c.length; e++) d = c[e], d.kind === a && (d.mode = d === this.T ? "showing" : "disabled")
        }, t.Cb = t.aa.extend({
            l: function(a, c) {
                c.track = {
                    kind: c.kind,
                    player: a,
                    label: c.kind + " off",
                    "default": l,
                    mode: "disabled"
                }, t.aa.call(this, a, c), this.selected(f)
            }
        }), t.vb = t.aa.extend({
            l: function(a, c) {
                c.track = {
                    kind: c.kind,
                    player: a,
                    label: c.kind + " settings",
                    "default": l,
                    mode: "disabled"
                }, t.aa.call(this, a, c), this.p("vjs-texttrack-settings")
            }
        }), t.vb.prototype.u = function() {
            this.k().ea("textTrackSettings").show()
        }, t.Q = t.O.extend({
            l: function(a, c) {
                var d, e;
                t.O.call(this, a, c), d = this.d.textTracks(), 1 >= this.H.length && this.Y(), d && (e = t.bind(this, this.update), d.addEventListener("removetrack", e), d.addEventListener("addtrack", e), this.d.b("dispose", function() {
                    d.removeEventListener("removetrack", e), d.removeEventListener("addtrack", e)
                }))
            }
        }), t.Q.prototype.La = function() {
            var c, d, a = [];
            if (this instanceof t.pa && (!this.k().h || !this.k().h.featuresNativeTextTracks) && a.push(new t.vb(this.d, {
                    kind: this.fa
                })), a.push(new t.Cb(this.d, {
                    kind: this.fa
                })), d = this.d.textTracks(), !d) return a;
            for (var e = 0; e < d.length; e++) c = d[e], c.kind === this.fa && a.push(new t.aa(this.d, {
                track: c
            }));
            return a
        }, t.pa = t.Q.extend({
            l: function(a, c, d) {
                t.Q.call(this, a, c, d), this.c.setAttribute("aria-label", "Captions Menu")
            }
        }), t.pa.prototype.fa = "captions", t.pa.prototype.ua = "Captions", t.pa.prototype.className = "vjs-captions-button", t.pa.prototype.update = function() {
            var a = 2;
            t.Q.prototype.update.call(this), this.k().h && this.k().h.featuresNativeTextTracks && (a = 1), this.H && this.H.length > a ? this.show() : this.Y()
        }, t.bb = t.Q.extend({
            l: function(a, c, d) {
                t.Q.call(this, a, c, d), this.c.setAttribute("aria-label", "Subtitles Menu")
            }
        }), t.bb.prototype.fa = "subtitles", t.bb.prototype.ua = "Subtitles", t.bb.prototype.className = "vjs-subtitles-button", t.Ya = t.Q.extend({
            l: function(a, c, d) {
                t.Q.call(this, a, c, d), this.c.setAttribute("aria-label", "Chapters Menu")
            }
        }), s = t.Ya.prototype, s.fa = "chapters", s.ua = "Chapters", s.className = "vjs-chapters-button", s.La = function() {
            var c, d, a = [];
            if (d = this.d.textTracks(), !d) return a;
            for (var e = 0; e < d.length; e++) c = d[e], c.kind === this.fa && a.push(new t.aa(this.d, {
                track: c
            }));
            return a
        }, s.Ma = function() {
            for (var e, g, a = this.d.textTracks() || [], c = 0, d = a.length, h = this.H = []; d > c; c++)
                if (e = a[c], e.kind == this.fa) {
                    if (e.Ec) {
                        g = e;
                        break
                    }
                    e.mode = "hidden", window.setTimeout(t.bind(this, function() {
                        this.Ma()
                    }), 100)
                }
            if (a = this.Aa, a === b && (a = new t.ra(this.d), a.xa().appendChild(t.f("li", {
                    className: "vjs-menu-title",
                    innerHTML: t.wa(this.fa),
                    Xe: -1
                }))), g) {
                e = g.cues;
                for (var k, c = 0, d = e.length; d > c; c++) k = e[c], k = new t.Za(this.d, {
                    track: g,
                    cue: k
                }), h.push(k), a.ba(k);
                this.ba(a)
            }
            return 0 < this.H.length && this.show(), a
        }, t.Za = t.M.extend({
            l: function(a, c) {
                var d = this.T = c.track,
                    e = this.cue = c.cue,
                    g = a.currentTime();
                c.label = e.text, c.selected = e.startTime <= g && g < e.endTime, t.M.call(this, a, c), d.addEventListener("cuechange", t.bind(this, this.update))
            }
        }), t.Za.prototype.u = function() {
            t.M.prototype.u.call(this), this.d.currentTime(this.cue.startTime), this.update(this.cue.startTime)
        }, t.Za.prototype.update = function() {
            var a = this.cue,
                c = this.d.currentTime();
            this.selected(a.startTime <= c && c < a.endTime)
        }, t.tc = t.a.extend({
            l: function(a, c) {
                t.a.call(this, a, c), this.Y(), t.b(this.m().querySelector(".vjs-done-button"), "click", t.bind(this, function() {
                    this.Ke(), this.Y()
                })), t.b(this.m().querySelector(".vjs-default-button"), "click", t.bind(this, function() {
                    this.m().querySelector(".vjs-fg-color > select").selectedIndex = 0, this.m().querySelector(".vjs-bg-color > select").selectedIndex = 0, this.m().querySelector(".window-color > select").selectedIndex = 0, this.m().querySelector(".vjs-text-opacity > select").selectedIndex = 0, this.m().querySelector(".vjs-bg-opacity > select").selectedIndex = 0, this.m().querySelector(".vjs-window-opacity > select").selectedIndex = 0, this.m().querySelector(".vjs-edge-style select").selectedIndex = 0, this.m().querySelector(".vjs-font-family select").selectedIndex = 0, this.m().querySelector(".vjs-font-percent select").selectedIndex = 2, this.C()
                })), t.b(this.m().querySelector(".vjs-fg-color > select"), "change", t.bind(this, this.C)), t.b(this.m().querySelector(".vjs-bg-color > select"), "change", t.bind(this, this.C)), t.b(this.m().querySelector(".window-color > select"), "change", t.bind(this, this.C)), t.b(this.m().querySelector(".vjs-text-opacity > select"), "change", t.bind(this, this.C)), t.b(this.m().querySelector(".vjs-bg-opacity > select"), "change", t.bind(this, this.C)), t.b(this.m().querySelector(".vjs-window-opacity > select"), "change", t.bind(this, this.C)), t.b(this.m().querySelector(".vjs-font-percent select"), "change", t.bind(this, this.C)), t.b(this.m().querySelector(".vjs-edge-style select"), "change", t.bind(this, this.C)), t.b(this.m().querySelector(".vjs-font-family select"), "change", t.bind(this, this.C)), a.options().persistTextTrackSettings && this.Je()
            }
        }), s = t.tc.prototype, s.f = function() {
            return t.a.prototype.f.call(this, "div", {
                className: "vjs-caption-settings vjs-modal-overlay",
                innerHTML: '<div class="vjs-tracksettings"><div class="vjs-tracksettings-colors"><div class="vjs-fg-color vjs-tracksetting"><label class="vjs-label">Foreground</label><select><option value="">---</option><option value="#FFF">White</option><option value="#000">Black</option><option value="#F00">Red</option><option value="#0F0">Green</option><option value="#00F">Blue</option><option value="#FF0">Yellow</option><option value="#F0F">Magenta</option><option value="#0FF">Cyan</option></select><span class="vjs-text-opacity vjs-opacity"><select><option value="">---</option><option value="1">Opaque</option><option value="0.5">Semi-Opaque</option></select></span></div><div class="vjs-bg-color vjs-tracksetting"><label class="vjs-label">Background</label><select><option value="">---</option><option value="#FFF">White</option><option value="#000">Black</option><option value="#F00">Red</option><option value="#0F0">Green</option><option value="#00F">Blue</option><option value="#FF0">Yellow</option><option value="#F0F">Magenta</option><option value="#0FF">Cyan</option></select><span class="vjs-bg-opacity vjs-opacity"><select><option value="">---</option><option value="1">Opaque</option><option value="0.5">Semi-Transparent</option><option value="0">Transparent</option></select></span></div><div class="window-color vjs-tracksetting"><label class="vjs-label">Window</label><select><option value="">---</option><option value="#FFF">White</option><option value="#000">Black</option><option value="#F00">Red</option><option value="#0F0">Green</option><option value="#00F">Blue</option><option value="#FF0">Yellow</option><option value="#F0F">Magenta</option><option value="#0FF">Cyan</option></select><span class="vjs-window-opacity vjs-opacity"><select><option value="">---</option><option value="1">Opaque</option><option value="0.5">Semi-Transparent</option><option value="0">Transparent</option></select></span></div></div><div class="vjs-tracksettings-font"><div class="vjs-font-percent vjs-tracksetting"><label class="vjs-label">Font Size</label><select><option value="0.50">50%</option><option value="0.75">75%</option><option value="1.00" selected>100%</option><option value="1.25">125%</option><option value="1.50">150%</option><option value="1.75">175%</option><option value="2.00">200%</option><option value="3.00">300%</option><option value="4.00">400%</option></select></div><div class="vjs-edge-style vjs-tracksetting"><label class="vjs-label">Text Edge Style</label><select><option value="none">None</option><option value="raised">Raised</option><option value="depressed">Depressed</option><option value="uniform">Uniform</option><option value="dropshadow">Dropshadow</option></select></div><div class="vjs-font-family vjs-tracksetting"><label class="vjs-label">Font Family</label><select><option value="">Default</option><option value="monospaceSerif">Monospace Serif</option><option value="proportionalSerif">Proportional Serif</option><option value="monospaceSansSerif">Monospace Sans-Serif</option><option value="proportionalSansSerif">Proportional Sans-Serif</option><option value="casual">Casual</option><option value="script">Script</option><option value="small-caps">Small Caps</option></select></div></div></div><div class="vjs-tracksettings-controls"><button class="vjs-default-button">Defaults</button><button class="vjs-done-button">Done</button></div>'
            })
        }, s.Lc = function() {
            var a, c, d, e, g, h, k, q, r, u;
            a = this.m(), g = X(a.querySelector(".vjs-edge-style select")), h = X(a.querySelector(".vjs-font-family select")), k = X(a.querySelector(".vjs-fg-color > select")), d = X(a.querySelector(".vjs-text-opacity > select")), q = X(a.querySelector(".vjs-bg-color > select")), c = X(a.querySelector(".vjs-bg-opacity > select")), r = X(a.querySelector(".window-color > select")), e = X(a.querySelector(".vjs-window-opacity > select")), a = window.parseFloat(X(a.querySelector(".vjs-font-percent > select"))), c = {
                backgroundOpacity: c,
                textOpacity: d,
                windowOpacity: e,
                edgeStyle: g,
                fontFamily: h,
                color: k,
                backgroundColor: q,
                windowColor: r,
                fontPercent: a
            };
            for (u in c)("" === c[u] || "none" === c[u] || "fontPercent" === u && 1 === c[u]) && delete c[u];
            return c
        }, s.Se = function(a) {
            var c = this.m();
            Y(c.querySelector(".vjs-edge-style select"), a.Na), Y(c.querySelector(".vjs-font-family select"), a.fontFamily), Y(c.querySelector(".vjs-fg-color > select"), a.color), Y(c.querySelector(".vjs-text-opacity > select"), a.od), Y(c.querySelector(".vjs-bg-color > select"), a.backgroundColor), Y(c.querySelector(".vjs-bg-opacity > select"), a.zc), Y(c.querySelector(".window-color > select"), a.gc), Y(c.querySelector(".vjs-window-opacity > select"), a.vd), (a = a.Rb) && (a = a.toFixed(2)), Y(c.querySelector(".vjs-font-percent > select"), a)
        }, s.Je = function() {
            var a;
            try {
                a = JSON.parse(window.localStorage.getItem("vjs-text-track-settings"))
            } catch (c) {}
            a && this.Se(a)
        }, s.Ke = function() {
            var a;
            if (this.d.options().persistTextTrackSettings) {
                a = this.Lc();
                try {
                    t.kb(a) ? window.localStorage.removeItem("vjs-text-track-settings") : window.localStorage.setItem("vjs-text-track-settings", JSON.stringify(a))
                } catch (c) {}
            }
        }, s.C = function() {
            var a = this.d.ea("textTrackDisplay");
            a && a.C()
        }, "undefined" != typeof window.JSON && "function" == typeof window.JSON.parse) t.JSON = window.JSON;
    else {
        t.JSON = {};
        var Z = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
        t.JSON.parse = function(a, c) {
            function d(a, e) {
                var k, q, r = a[e];
                if (r && "object" == typeof r)
                    for (k in r) Object.prototype.hasOwnProperty.call(r, k) && (q = d(r, k), q !== b ? r[k] = q : delete r[k]);
                return c.call(a, e, r)
            }
            var e;
            if (a = String(a), Z.lastIndex = 0, Z.test(a) && (a = a.replace(Z, function(a) {
                    return "\\u" + ("0000" + a.charCodeAt(0).toString(16)).slice(-4)
                })), /^[\],:{}\s]*$/.test(a.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, "@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, "]").replace(/(?:^|:|,)(?:\s*\[)+/g, ""))) return e = eval("(" + a + ")"), "function" == typeof c ? d({
                "": e
            }, "") : e;
            throw new SyntaxError("JSON.parse(): invalid or malformed JSON data")
        }
    }
    t.yc = function() {
        var a, c, d, e;
        a = document.getElementsByTagName("video"), c = document.getElementsByTagName("audio");
        var g = [];
        if (a && 0 < a.length)
            for (d = 0, e = a.length; e > d; d++) g.push(a[d]);
        if (c && 0 < c.length)
            for (d = 0, e = c.length; e > d; d++) g.push(c[d]);
        if (g && 0 < g.length)
            for (d = 0, e = g.length; e > d; d++) {
                if (!(c = g[d]) || !c.getAttribute) {
                    t.Kb();
                    break
                }
                c.player === b && (a = c.getAttribute("data-setup"), a !== j && videojs(c))
            } else t.ud || t.Kb()
    }, t.Kb = function() {
        setTimeout(t.yc, 1)
    }, "complete" === document.readyState ? t.ud = f : t.N(window, "load", function() {
        t.ud = f
    }), t.Kb(), t.Ge = function(a, c) {
        t.Player.prototype[a] = c
    };
    var Ba = this;
    $("videojs", t), $("_V_", t), $("videojs.options", t.options), $("videojs.players", t.Da), $("videojs.TOUCH_ENABLED", t.Gb), $("videojs.cache", t.va), $("videojs.Component", t.a), t.a.prototype.player = t.a.prototype.k, t.a.prototype.options = t.a.prototype.options, t.a.prototype.init = t.a.prototype.l, t.a.prototype.dispose = t.a.prototype.dispose, t.a.prototype.createEl = t.a.prototype.f, t.a.prototype.contentEl = t.a.prototype.xa, t.a.prototype.el = t.a.prototype.m, t.a.prototype.addChild = t.a.prototype.ba, t.a.prototype.getChild = t.a.prototype.ea, t.a.prototype.getChildById = t.a.prototype.be, t.a.prototype.children = t.a.prototype.children, t.a.prototype.initChildren = t.a.prototype.Oc, t.a.prototype.removeChild = t.a.prototype.removeChild, t.a.prototype.on = t.a.prototype.b, t.a.prototype.off = t.a.prototype.n, t.a.prototype.one = t.a.prototype.N, t.a.prototype.trigger = t.a.prototype.o, t.a.prototype.triggerReady = t.a.prototype.Xa, t.a.prototype.show = t.a.prototype.show, t.a.prototype.hide = t.a.prototype.Y, t.a.prototype.width = t.a.prototype.width, t.a.prototype.height = t.a.prototype.height, t.a.prototype.dimensions = t.a.prototype.Ud, t.a.prototype.ready = t.a.prototype.I, t.a.prototype.addClass = t.a.prototype.p, t.a.prototype.removeClass = t.a.prototype.r, t.a.prototype.hasClass = t.a.prototype.Qa, t.a.prototype.buildCSSClass = t.a.prototype.V, t.a.prototype.localize = t.a.prototype.v, t.a.prototype.setInterval = t.a.prototype.setInterval, t.a.prototype.setTimeout = t.a.prototype.setTimeout, $("videojs.EventEmitter", t.z), t.z.prototype.on = t.z.prototype.b, t.z.prototype.addEventListener = t.z.prototype.addEventListener, t.z.prototype.off = t.z.prototype.n, t.z.prototype.removeEventListener = t.z.prototype.removeEventListener, t.z.prototype.one = t.z.prototype.N, t.z.prototype.trigger = t.z.prototype.o, t.z.prototype.dispatchEvent = t.z.prototype.dispatchEvent, t.Player.prototype.ended = t.Player.prototype.ended, t.Player.prototype.enterFullWindow = t.Player.prototype.Jc, t.Player.prototype.exitFullWindow = t.Player.prototype.Ob, t.Player.prototype.preload = t.Player.prototype.Sa, t.Player.prototype.remainingTime = t.Player.prototype.remainingTime, t.Player.prototype.supportsFullScreen = t.Player.prototype.Ua, t.Player.prototype.currentType = t.Player.prototype.Rd, t.Player.prototype.requestFullScreen = t.Player.prototype.requestFullScreen, t.Player.prototype.requestFullscreen = t.Player.prototype.requestFullscreen, t.Player.prototype.cancelFullScreen = t.Player.prototype.cancelFullScreen, t.Player.prototype.exitFullscreen = t.Player.prototype.exitFullscreen, t.Player.prototype.isFullScreen = t.Player.prototype.isFullScreen, t.Player.prototype.isFullscreen = t.Player.prototype.isFullscreen, t.Player.prototype.textTracks = t.Player.prototype.textTracks, t.Player.prototype.remoteTextTracks = t.Player.prototype.Z, t.Player.prototype.addTextTrack = t.Player.prototype.addTextTrack, t.Player.prototype.addRemoteTextTrack = t.Player.prototype.ia, t.Player.prototype.removeRemoteTextTrack = t.Player.prototype.Ea, t.Player.prototype.seekable = t.Player.prototype.seekable, $("videojs.MediaLoader", t.Gd), $("videojs.TextTrackDisplay", t.ta), $("videojs.ControlBar", t.wb), $("videojs.Button", t.w), $("videojs.PlayToggle", t.oc), $("videojs.FullscreenToggle", t.$a), $("videojs.BigPlayButton", t.ub), $("videojs.LoadingSpinner", t.mc), $("videojs.CurrentTimeDisplay", t.xb), $("videojs.DurationDisplay", t.yb), $("videojs.TimeDivider", t.uc), $("videojs.RemainingTimeDisplay", t.Fb), $("videojs.LiveDisplay", t.lc), $("videojs.ErrorDisplay", t.zb), $("videojs.Slider", t.U), $("videojs.ProgressControl", t.Eb), $("videojs.SeekBar", t.rc), $("videojs.LoadProgressBar", t.Bb), $("videojs.PlayProgressBar", t.nc), $("videojs.SeekHandle", t.ab), $("videojs.VolumeControl", t.Ib), $("videojs.VolumeBar", t.Hb), $("videojs.VolumeLevel", t.vc), $("videojs.VolumeMenuButton", t.Ia), $("videojs.VolumeHandle", t.Jb), $("videojs.MuteToggle", t.sa), $("videojs.PosterImage", t.qc), $("videojs.Menu", t.ra), $("videojs.MenuItem", t.M), $("videojs.MenuButton", t.O), $("videojs.PlaybackRateMenuButton", t.pc), $("videojs.ChaptersTrackMenuItem", t.Za), $("videojs.TextTrackButton", t.Q), $("videojs.TextTrackMenuItem", t.aa), $("videojs.OffTextTrackMenuItem", t.Cb), $("videojs.CaptionSettingsMenuItem", t.vb), t.O.prototype.createItems = t.O.prototype.La, t.Q.prototype.createItems = t.Q.prototype.La, t.Ya.prototype.createItems = t.Ya.prototype.La, $("videojs.SubtitlesButton", t.bb), $("videojs.CaptionsButton", t.pa), $("videojs.ChaptersButton", t.Ya), $("videojs.MediaTechController", t.j), t.j.withSourceHandlers = t.j.hc, t.j.prototype.featuresVolumeControl = t.j.prototype.vf, t.j.prototype.featuresFullscreenResize = t.j.prototype.rf, t.j.prototype.featuresPlaybackRate = t.j.prototype.sf, t.j.prototype.featuresProgressEvents = t.j.prototype.tf, t.j.prototype.featuresTimeupdateEvents = t.j.prototype.uf, t.j.prototype.setPoster = t.j.prototype.fd, t.j.prototype.textTracks = t.j.prototype.textTracks, t.j.prototype.remoteTextTracks = t.j.prototype.Z, t.j.prototype.addTextTrack = t.j.prototype.addTextTrack, t.j.prototype.addRemoteTextTrack = t.j.prototype.ia, t.j.prototype.removeRemoteTextTrack = t.j.prototype.Ea, $("videojs.Html5", t.e), t.e.Events = t.e.Ab, t.e.isSupported = t.e.isSupported, t.e.canPlaySource = t.e.Ac, t.e.patchCanPlayType = t.e.Xc, t.e.unpatchCanPlayType = t.e.cf, t.e.prototype.setCurrentTime = t.e.prototype.cc, t.e.prototype.setVolume = t.e.prototype.Te, t.e.prototype.setMuted = t.e.prototype.Pe, t.e.prototype.setPreload = t.e.prototype.Re, t.e.prototype.setAutoplay = t.e.prototype.Me, t.e.prototype.setLoop = t.e.prototype.Oe, t.e.prototype.enterFullScreen = t.e.prototype.Ic, t.e.prototype.exitFullScreen = t.e.prototype.Yd, t.e.prototype.playbackRate = t.e.prototype.playbackRate, t.e.prototype.setPlaybackRate = t.e.prototype.Qe, t.e.registerSourceHandler = t.e.Ta, t.e.selectSourceHandler = t.e.rb, t.e.prototype.setSource = t.e.prototype.na, t.e.prototype.disposeSourceHandler = t.e.prototype.ja, t.e.prototype.textTracks = t.e.prototype.textTracks, t.e.prototype.remoteTextTracks = t.e.prototype.Z, t.e.prototype.addTextTrack = t.e.prototype.addTextTrack, t.e.prototype.addRemoteTextTrack = t.e.prototype.ia, t.e.prototype.removeRemoteTextTrack = t.e.prototype.Ea, $("videojs.Flash", t.g), t.g.isSupported = t.g.isSupported, t.g.canPlaySource = t.g.Ac, t.g.onReady = t.g.onReady, t.g.embed = t.g.Hc, t.g.version = t.g.version, t.g.prototype.setSource = t.g.prototype.na, t.g.registerSourceHandler = t.g.Ta, t.g.selectSourceHandler = t.g.rb, t.g.prototype.setSource = t.g.prototype.na, t.g.prototype.disposeSourceHandler = t.g.prototype.ja, $("videojs.TextTrack", t.t), $("videojs.TextTrackList", t.F), $("videojs.TextTrackCueList", t.W), $("videojs.TextTrackSettings", t.tc), t.t.prototype.id = t.t.prototype.id, t.t.prototype.label = t.t.prototype.label, t.t.prototype.kind = t.t.prototype.Xb, t.t.prototype.mode = t.t.prototype.mode, t.t.prototype.cues = t.t.prototype.Ec, t.t.prototype.activeCues = t.t.prototype.of, t.t.prototype.addCue = t.t.prototype.wc, t.t.prototype.removeCue = t.t.prototype.bd, t.F.prototype.getTrackById = t.F.prototype.ee, t.W.prototype.getCueById = t.F.prototype.ce, $("videojs.CaptionsTrack", t.hf), $("videojs.SubtitlesTrack", t.nf), $("videojs.ChaptersTrack", t.jf), $("videojs.autoSetup", t.yc), $("videojs.plugin", t.Ge), $("videojs.createTimeRange", t.ya), $("videojs.util", t.$), t.$.mergeOptions = t.$.Ba, t.addLanguage = t.Kd
}(), ! function(a) {
    var b = a.vttjs = {},
        c = b.VTTCue,
        d = b.VTTRegion,
        e = a.VTTCue,
        f = a.VTTRegion;
    b.shim = function() {
        b.VTTCue = c, b.VTTRegion = d
    }, b.restore = function() {
        b.VTTCue = e, b.VTTRegion = f
    }
}(this),
function(a, b) {
    function c(a) {
        if ("string" != typeof a) return !1;
        var b = h[a.toLowerCase()];
        return b ? a.toLowerCase() : !1
    }

    function d(a) {
        if ("string" != typeof a) return !1;
        var b = i[a.toLowerCase()];
        return b ? a.toLowerCase() : !1
    }

    function e(a) {
        for (var b = 1; b < arguments.length; b++) {
            var c = arguments[b];
            for (var d in c) a[d] = c[d]
        }
        return a
    }

    function f(a, b, f) {
        var h = this,
            i = /MSIE\s8\.0/.test(navigator.userAgent),
            j = {};
        i ? h = document.createElement("custom") : j.enumerable = !0, h.hasBeenReset = !1;
        var k = "",
            l = !1,
            m = a,
            n = b,
            o = f,
            p = null,
            q = "",
            r = !0,
            s = "auto",
            t = "start",
            u = 50,
            v = "middle",
            w = 50,
            x = "middle";
        return Object.defineProperty(h, "id", e({}, j, {
            get: function() {
                return k
            },
            set: function(a) {
                k = "" + a
            }
        })), Object.defineProperty(h, "pauseOnExit", e({}, j, {
            get: function() {
                return l
            },
            set: function(a) {
                l = !!a
            }
        })), Object.defineProperty(h, "startTime", e({}, j, {
            get: function() {
                return m
            },
            set: function(a) {
                if ("number" != typeof a) throw new TypeError("Start time must be set to a number.");
                m = a, this.hasBeenReset = !0
            }
        })), Object.defineProperty(h, "endTime", e({}, j, {
            get: function() {
                return n
            },
            set: function(a) {
                if ("number" != typeof a) throw new TypeError("End time must be set to a number.");
                n = a, this.hasBeenReset = !0
            }
        })), Object.defineProperty(h, "text", e({}, j, {
            get: function() {
                return o
            },
            set: function(a) {
                o = "" + a, this.hasBeenReset = !0
            }
        })), Object.defineProperty(h, "region", e({}, j, {
            get: function() {
                return p
            },
            set: function(a) {
                p = a, this.hasBeenReset = !0
            }
        })), Object.defineProperty(h, "vertical", e({}, j, {
            get: function() {
                return q
            },
            set: function(a) {
                var b = c(a);
                if (b === !1) throw new SyntaxError("An invalid or illegal string was specified.");
                q = b, this.hasBeenReset = !0
            }
        })), Object.defineProperty(h, "snapToLines", e({}, j, {
            get: function() {
                return r
            },
            set: function(a) {
                r = !!a, this.hasBeenReset = !0
            }
        })), Object.defineProperty(h, "line", e({}, j, {
            get: function() {
                return s
            },
            set: function(a) {
                if ("number" != typeof a && a !== g) throw new SyntaxError("An invalid number or illegal string was specified.");
                s = a, this.hasBeenReset = !0
            }
        })), Object.defineProperty(h, "lineAlign", e({}, j, {
            get: function() {
                return t
            },
            set: function(a) {
                var b = d(a);
                if (!b) throw new SyntaxError("An invalid or illegal string was specified.");
                t = b, this.hasBeenReset = !0
            }
        })), Object.defineProperty(h, "position", e({}, j, {
            get: function() {
                return u
            },
            set: function(a) {
                if (0 > a || a > 100) throw new Error("Position must be between 0 and 100.");
                u = a, this.hasBeenReset = !0
            }
        })), Object.defineProperty(h, "positionAlign", e({}, j, {
            get: function() {
                return v
            },
            set: function(a) {
                var b = d(a);
                if (!b) throw new SyntaxError("An invalid or illegal string was specified.");
                v = b, this.hasBeenReset = !0
            }
        })), Object.defineProperty(h, "size", e({}, j, {
            get: function() {
                return w
            },
            set: function(a) {
                if (0 > a || a > 100) throw new Error("Size must be between 0 and 100.");
                w = a, this.hasBeenReset = !0
            }
        })), Object.defineProperty(h, "align", e({}, j, {
            get: function() {
                return x
            },
            set: function(a) {
                var b = d(a);
                if (!b) throw new SyntaxError("An invalid or illegal string was specified.");
                x = b, this.hasBeenReset = !0
            }
        })), h.displayState = void 0, i ? h : void 0
    }
    var g = "auto",
        h = {
            "": !0,
            lr: !0,
            rl: !0
        },
        i = {
            start: !0,
            middle: !0,
            end: !0,
            left: !0,
            right: !0
        };
    f.prototype.getCueAsHTML = function() {
        return WebVTT.convertCueToDOMTree(window, this.text)
    }, a.VTTCue = a.VTTCue || f, b.VTTCue = f
}(this, this.vttjs || {}),
function(a, b) {
    function c(a) {
        if ("string" != typeof a) return !1;
        var b = f[a.toLowerCase()];
        return b ? a.toLowerCase() : !1
    }

    function d(a) {
        return "number" == typeof a && a >= 0 && 100 >= a
    }

    function e() {
        var a = 100,
            b = 3,
            e = 0,
            f = 100,
            g = 0,
            h = 100,
            i = "";
        Object.defineProperties(this, {
            width: {
                enumerable: !0,
                get: function() {
                    return a
                },
                set: function(b) {
                    if (!d(b)) throw new Error("Width must be between 0 and 100.");
                    a = b
                }
            },
            lines: {
                enumerable: !0,
                get: function() {
                    return b
                },
                set: function(a) {
                    if ("number" != typeof a) throw new TypeError("Lines must be set to a number.");
                    b = a
                }
            },
            regionAnchorY: {
                enumerable: !0,
                get: function() {
                    return f
                },
                set: function(a) {
                    if (!d(a)) throw new Error("RegionAnchorX must be between 0 and 100.");
                    f = a
                }
            },
            regionAnchorX: {
                enumerable: !0,
                get: function() {
                    return e
                },
                set: function(a) {
                    if (!d(a)) throw new Error("RegionAnchorY must be between 0 and 100.");
                    e = a
                }
            },
            viewportAnchorY: {
                enumerable: !0,
                get: function() {
                    return h
                },
                set: function(a) {
                    if (!d(a)) throw new Error("ViewportAnchorY must be between 0 and 100.");
                    h = a
                }
            },
            viewportAnchorX: {
                enumerable: !0,
                get: function() {
                    return g
                },
                set: function(a) {
                    if (!d(a)) throw new Error("ViewportAnchorX must be between 0 and 100.");
                    g = a
                }
            },
            scroll: {
                enumerable: !0,
                get: function() {
                    return i
                },
                set: function(a) {
                    var b = c(a);
                    if (b === !1) throw new SyntaxError("An invalid or illegal string was specified.");
                    i = b
                }
            }
        })
    }
    var f = {
        "": !0,
        up: !0
    };
    a.VTTRegion = a.VTTRegion || e, b.VTTRegion = e
}(this, this.vttjs || {}),
function(a) {
    function b(a, b) {
        this.name = "ParsingError", this.code = a.code, this.message = b || a.message
    }

    function c(a) {
        function b(a, b, c, d) {
            return 3600 * (0 | a) + 60 * (0 | b) + (0 | c) + (0 | d) / 1e3
        }
        var c = a.match(/^(\d+):(\d{2})(:\d{2})?\.(\d{3})/);
        return c ? c[3] ? b(c[1], c[2], c[3].replace(":", ""), c[4]) : c[1] > 59 ? b(c[1], c[2], 0, c[4]) : b(0, c[1], c[2], c[4]) : null
    }

    function d() {
        this.values = o(null)
    }

    function e(a, b, c, d) {
        var e = d ? a.split(d) : [a];
        for (var f in e)
            if ("string" == typeof e[f]) {
                var g = e[f].split(c);
                if (2 === g.length) {
                    var h = g[0],
                        i = g[1];
                    b(h, i)
                }
            }
    }

    function f(a, f, g) {
        function h() {
            var d = c(a);
            if (null === d) throw new b(b.Errors.BadTimeStamp, "Malformed timestamp: " + k);
            return a = a.replace(/^[^\sa-zA-Z-]+/, ""), d
        }

        function i(a, b) {
            var c = new d;
            e(a, function(a, b) {
                switch (a) {
                    case "region":
                        for (var d = g.length - 1; d >= 0; d--)
                            if (g[d].id === b) {
                                c.set(a, g[d].region);
                                break
                            }
                        break;
                    case "vertical":
                        c.alt(a, b, ["rl", "lr"]);
                        break;
                    case "line":
                        var e = b.split(","),
                            f = e[0];
                        c.integer(a, f), c.percent(a, f) ? c.set("snapToLines", !1) : null, c.alt(a, f, ["auto"]), 2 === e.length && c.alt("lineAlign", e[1], ["start", "middle", "end"]);
                        break;
                    case "position":
                        e = b.split(","), c.percent(a, e[0]), 2 === e.length && c.alt("positionAlign", e[1], ["start", "middle", "end"]);
                        break;
                    case "size":
                        c.percent(a, b);
                        break;
                    case "align":
                        c.alt(a, b, ["start", "middle", "end", "left", "right"])
                }
            }, /:/, /\s/), b.region = c.get("region", null), b.vertical = c.get("vertical", ""), b.line = c.get("line", "auto"), b.lineAlign = c.get("lineAlign", "start"), b.snapToLines = c.get("snapToLines", !0), b.size = c.get("size", 100), b.align = c.get("align", "middle"), b.position = c.get("position", {
                start: 0,
                left: 0,
                middle: 50,
                end: 100,
                right: 100
            }, b.align), b.positionAlign = c.get("positionAlign", {
                start: "start",
                left: "start",
                middle: "middle",
                end: "end",
                right: "end"
            }, b.align)
        }

        function j() {
            a = a.replace(/^\s+/, "")
        }
        var k = a;
        if (j(), f.startTime = h(), j(), "-->" !== a.substr(0, 3)) throw new b(b.Errors.BadTimeStamp, "Malformed time stamp (time stamps must be separated by '-->'): " + k);
        a = a.substr(3), j(), f.endTime = h(), j(), i(a, f)
    }

    function g(a, b) {
        function d() {
            function a(a) {
                return b = b.substr(a.length), a
            }
            if (!b) return null;
            var c = b.match(/^([^<]*)(<[^>]+>?)?/);
            return a(c[1] ? c[1] : c[2])
        }

        function e(a) {
            return p[a]
        }

        function f(a) {
            for (; o = a.match(/&(amp|lt|gt|lrm|rlm|nbsp);/);) a = a.replace(o[0], e);
            return a
        }

        function g(a, b) {
            return !s[b.localName] || s[b.localName] === a.localName
        }

        function h(b, c) {
            var d = q[b];
            if (!d) return null;
            var e = a.document.createElement(d);
            e.localName = d;
            var f = r[b];
            return f && c && (e[f] = c.trim()), e
        }
        for (var i, j = a.document.createElement("div"), k = j, l = []; null !== (i = d());)
            if ("<" !== i[0]) k.appendChild(a.document.createTextNode(f(i)));
            else {
                if ("/" === i[1]) {
                    l.length && l[l.length - 1] === i.substr(2).replace(">", "") && (l.pop(), k = k.parentNode);
                    continue
                }
                var m, n = c(i.substr(1, i.length - 2));
                if (n) {
                    m = a.document.createProcessingInstruction("timestamp", n), k.appendChild(m);
                    continue
                }
                var o = i.match(/^<([^.\s\/0-9>]+)(\.[^\s\\>]+)?([^>\\]+)?(\\?)>?$/);
                if (!o) continue;
                if (m = h(o[1], o[3]), !m) continue;
                if (!g(k, m)) continue;
                o[2] && (m.className = o[2].substr(1).replace(".", " ")), l.push(o[1]), k.appendChild(m), k = m
            }
        return j
    }

    function h(a) {
        function b(a, b) {
            for (var c = b.childNodes.length - 1; c >= 0; c--) a.push(b.childNodes[c])
        }

        function c(a) {
            if (!a || !a.length) return null;
            var d = a.pop(),
                e = d.textContent || d.innerText;
            if (e) {
                var f = e.match(/^.*(\n|\r)/);
                return f ? (a.length = 0, f[0]) : e
            }
            return "ruby" === d.tagName ? c(a) : d.childNodes ? (b(a, d), c(a)) : void 0
        }
        var d, e = [],
            f = "";
        if (!a || !a.childNodes) return "ltr";
        for (b(e, a); f = c(e);)
            for (var g = 0; g < f.length; g++) {
                d = f.charCodeAt(g);
                for (var h = 0; h < t.length; h++)
                    if (t[h] === d) return "rtl"
            }
        return "ltr"
    }

    function i(a) {
        if ("number" == typeof a.line && (a.snapToLines || a.line >= 0 && a.line <= 100)) return a.line;
        if (!a.track || !a.track.textTrackList || !a.track.textTrackList.mediaElement) return -1;
        for (var b = a.track, c = b.textTrackList, d = 0, e = 0; e < c.length && c[e] !== b; e++) "showing" === c[e].mode && d++;
        return -1 * ++d
    }

    function j() {}

    function k(a, b, c) {
        var d = /MSIE\s8\.0/.test(navigator.userAgent),
            e = "rgba(255, 255, 255, 1)",
            f = "rgba(0, 0, 0, 0.8)";
        d && (e = "rgb(255, 255, 255)", f = "rgb(0, 0, 0)"), j.call(this), this.cue = b, this.cueDiv = g(a, b.text);
        var i = {
            color: e,
            backgroundColor: f,
            position: "relative",
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            display: "inline"
        };
        d || (i.writingMode = "" === b.vertical ? "horizontal-tb" : "lr" === b.vertical ? "vertical-lr" : "vertical-rl", i.unicodeBidi = "plaintext"), this.applyStyles(i, this.cueDiv), this.div = a.document.createElement("div"), i = {
            textAlign: "middle" === b.align ? "center" : b.align,
            font: c.font,
            whiteSpace: "pre-line",
            position: "absolute"
        }, d || (i.direction = h(this.cueDiv), i.writingMode = "" === b.vertical ? "horizontal-tb" : "lr" === b.vertical ? "vertical-lr" : "vertical-rl".stylesunicodeBidi = "plaintext"), this.applyStyles(i), this.div.appendChild(this.cueDiv);
        var k = 0;
        switch (b.positionAlign) {
            case "start":
                k = b.position;
                break;
            case "middle":
                k = b.position - b.size / 2;
                break;
            case "end":
                k = b.position - b.size
        }
        this.applyStyles("" === b.vertical ? {
            left: this.formatStyle(k, "%"),
            width: this.formatStyle(b.size, "%")
        } : {
            top: this.formatStyle(k, "%"),
            height: this.formatStyle(b.size, "%")
        }), this.move = function(a) {
            this.applyStyles({
                top: this.formatStyle(a.top, "px"),
                bottom: this.formatStyle(a.bottom, "px"),
                left: this.formatStyle(a.left, "px"),
                right: this.formatStyle(a.right, "px"),
                height: this.formatStyle(a.height, "px"),
                width: this.formatStyle(a.width, "px")
            })
        }
    }

    function l(a) {
        var b, c, d, e, f = /MSIE\s8\.0/.test(navigator.userAgent);
        if (a.div) {
            c = a.div.offsetHeight, d = a.div.offsetWidth, e = a.div.offsetTop;
            var g = (g = a.div.childNodes) && (g = g[0]) && g.getClientRects && g.getClientRects();
            a = a.div.getBoundingClientRect(), b = g ? Math.max(g[0] && g[0].height || 0, a.height / g.length) : 0
        }
        this.left = a.left, this.right = a.right, this.top = a.top || e, this.height = a.height || c, this.bottom = a.bottom || e + (a.height || c), this.width = a.width || d, this.lineHeight = void 0 !== b ? b : a.lineHeight, f && !this.lineHeight && (this.lineHeight = 13)
    }

    function m(a, b, c, d) {
        function e(a, b) {
            for (var e, f = new l(a), g = 1, h = 0; h < b.length; h++) {
                for (; a.overlapsOppositeAxis(c, b[h]) || a.within(c) && a.overlapsAny(d);) a.move(b[h]);
                if (a.within(c)) return a;
                var i = a.intersectPercentage(c);
                g > i && (e = new l(a), g = i), a = new l(f)
            }
            return e || f
        }
        var f = new l(b),
            g = b.cue,
            h = i(g),
            j = [];
        if (g.snapToLines) {
            var k;
            switch (g.vertical) {
                case "":
                    j = ["+y", "-y"], k = "height";
                    break;
                case "rl":
                    j = ["+x", "-x"], k = "width";
                    break;
                case "lr":
                    j = ["-x", "+x"], k = "width"
            }
            var m = f.lineHeight,
                n = m * Math.round(h),
                o = c[k] + m,
                p = j[0];
            Math.abs(n) > o && (n = 0 > n ? -1 : 1, n *= Math.ceil(o / m) * m), 0 > h && (n += "" === g.vertical ? c.height : c.width, j = j.reverse()), f.move(p, n)
        } else {
            var q = f.lineHeight / c.height * 100;
            switch (g.lineAlign) {
                case "middle":
                    h -= q / 2;
                    break;
                case "end":
                    h -= q
            }
            switch (g.vertical) {
                case "":
                    b.applyStyles({
                        top: b.formatStyle(h, "%")
                    });
                    break;
                case "rl":
                    b.applyStyles({
                        left: b.formatStyle(h, "%")
                    });
                    break;
                case "lr":
                    b.applyStyles({
                        right: b.formatStyle(h, "%")
                    })
            }
            j = ["+y", "-x", "+x", "-y"], f = new l(b)
        }
        var r = e(f, j);
        b.move(r.toCSSCompatValues(c))
    }

    function n() {}
    var o = Object.create || function() {
        function a() {}
        return function(b) {
            if (1 !== arguments.length) throw new Error("Object.create shim only accepts one parameter.");
            return a.prototype = b, new a
        }
    }();
    b.prototype = o(Error.prototype), b.prototype.constructor = b, b.Errors = {
        BadSignature: {
            code: 0,
            message: "Malformed WebVTT signature."
        },
        BadTimeStamp: {
            code: 1,
            message: "Malformed time stamp."
        }
    }, d.prototype = {
        set: function(a, b) {
            this.get(a) || "" === b || (this.values[a] = b)
        },
        get: function(a, b, c) {
            return c ? this.has(a) ? this.values[a] : b[c] : this.has(a) ? this.values[a] : b
        },
        has: function(a) {
            return a in this.values
        },
        alt: function(a, b, c) {
            for (var d = 0; d < c.length; ++d)
                if (b === c[d]) {
                    this.set(a, b);
                    break
                }
        },
        integer: function(a, b) {
            /^-?\d+$/.test(b) && this.set(a, parseInt(b, 10))
        },
        percent: function(a, b) {
            var c;
            return (c = b.match(/^([\d]{1,3})(\.[\d]*)?%$/)) && (b = parseFloat(b), b >= 0 && 100 >= b) ? (this.set(a, b), !0) : !1
        }
    };
    var p = {
            "&amp;": "&",
            "&lt;": "<",
            "&gt;": ">",
            "&lrm;": "‎",
            "&rlm;": "‏",
            "&nbsp;": " "
        },
        q = {
            c: "span",
            i: "i",
            b: "b",
            u: "u",
            ruby: "ruby",
            rt: "rt",
            v: "span",
            lang: "span"
        },
        r = {
            v: "title",
            lang: "lang"
        },
        s = {
            rt: "ruby"
        },
        t = [1470, 1472, 1475, 1478, 1488, 1489, 1490, 1491, 1492, 1493, 1494, 1495, 1496, 1497, 1498, 1499, 1500, 1501, 1502, 1503, 1504, 1505, 1506, 1507, 1508, 1509, 1510, 1511, 1512, 1513, 1514, 1520, 1521, 1522, 1523, 1524, 1544, 1547, 1549, 1563, 1566, 1567, 1568, 1569, 1570, 1571, 1572, 1573, 1574, 1575, 1576, 1577, 1578, 1579, 1580, 1581, 1582, 1583, 1584, 1585, 1586, 1587, 1588, 1589, 1590, 1591, 1592, 1593, 1594, 1595, 1596, 1597, 1598, 1599, 1600, 1601, 1602, 1603, 1604, 1605, 1606, 1607, 1608, 1609, 1610, 1645, 1646, 1647, 1649, 1650, 1651, 1652, 1653, 1654, 1655, 1656, 1657, 1658, 1659, 1660, 1661, 1662, 1663, 1664, 1665, 1666, 1667, 1668, 1669, 1670, 1671, 1672, 1673, 1674, 1675, 1676, 1677, 1678, 1679, 1680, 1681, 1682, 1683, 1684, 1685, 1686, 1687, 1688, 1689, 1690, 1691, 1692, 1693, 1694, 1695, 1696, 1697, 1698, 1699, 1700, 1701, 1702, 1703, 1704, 1705, 1706, 1707, 1708, 1709, 1710, 1711, 1712, 1713, 1714, 1715, 1716, 1717, 1718, 1719, 1720, 1721, 1722, 1723, 1724, 1725, 1726, 1727, 1728, 1729, 1730, 1731, 1732, 1733, 1734, 1735, 1736, 1737, 1738, 1739, 1740, 1741, 1742, 1743, 1744, 1745, 1746, 1747, 1748, 1749, 1765, 1766, 1774, 1775, 1786, 1787, 1788, 1789, 1790, 1791, 1792, 1793, 1794, 1795, 1796, 1797, 1798, 1799, 1800, 1801, 1802, 1803, 1804, 1805, 1807, 1808, 1810, 1811, 1812, 1813, 1814, 1815, 1816, 1817, 1818, 1819, 1820, 1821, 1822, 1823, 1824, 1825, 1826, 1827, 1828, 1829, 1830, 1831, 1832, 1833, 1834, 1835, 1836, 1837, 1838, 1839, 1869, 1870, 1871, 1872, 1873, 1874, 1875, 1876, 1877, 1878, 1879, 1880, 1881, 1882, 1883, 1884, 1885, 1886, 1887, 1888, 1889, 1890, 1891, 1892, 1893, 1894, 1895, 1896, 1897, 1898, 1899, 1900, 1901, 1902, 1903, 1904, 1905, 1906, 1907, 1908, 1909, 1910, 1911, 1912, 1913, 1914, 1915, 1916, 1917, 1918, 1919, 1920, 1921, 1922, 1923, 1924, 1925, 1926, 1927, 1928, 1929, 1930, 1931, 1932, 1933, 1934, 1935, 1936, 1937, 1938, 1939, 1940, 1941, 1942, 1943, 1944, 1945, 1946, 1947, 1948, 1949, 1950, 1951, 1952, 1953, 1954, 1955, 1956, 1957, 1969, 1984, 1985, 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2e3, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2036, 2037, 2042, 2048, 2049, 2050, 2051, 2052, 2053, 2054, 2055, 2056, 2057, 2058, 2059, 2060, 2061, 2062, 2063, 2064, 2065, 2066, 2067, 2068, 2069, 2074, 2084, 2088, 2096, 2097, 2098, 2099, 2100, 2101, 2102, 2103, 2104, 2105, 2106, 2107, 2108, 2109, 2110, 2112, 2113, 2114, 2115, 2116, 2117, 2118, 2119, 2120, 2121, 2122, 2123, 2124, 2125, 2126, 2127, 2128, 2129, 2130, 2131, 2132, 2133, 2134, 2135, 2136, 2142, 2208, 2210, 2211, 2212, 2213, 2214, 2215, 2216, 2217, 2218, 2219, 2220, 8207, 64285, 64287, 64288, 64289, 64290, 64291, 64292, 64293, 64294, 64295, 64296, 64298, 64299, 64300, 64301, 64302, 64303, 64304, 64305, 64306, 64307, 64308, 64309, 64310, 64312, 64313, 64314, 64315, 64316, 64318, 64320, 64321, 64323, 64324, 64326, 64327, 64328, 64329, 64330, 64331, 64332, 64333, 64334, 64335, 64336, 64337, 64338, 64339, 64340, 64341, 64342, 64343, 64344, 64345, 64346, 64347, 64348, 64349, 64350, 64351, 64352, 64353, 64354, 64355, 64356, 64357, 64358, 64359, 64360, 64361, 64362, 64363, 64364, 64365, 64366, 64367, 64368, 64369, 64370, 64371, 64372, 64373, 64374, 64375, 64376, 64377, 64378, 64379, 64380, 64381, 64382, 64383, 64384, 64385, 64386, 64387, 64388, 64389, 64390, 64391, 64392, 64393, 64394, 64395, 64396, 64397, 64398, 64399, 64400, 64401, 64402, 64403, 64404, 64405, 64406, 64407, 64408, 64409, 64410, 64411, 64412, 64413, 64414, 64415, 64416, 64417, 64418, 64419, 64420, 64421, 64422, 64423, 64424, 64425, 64426, 64427, 64428, 64429, 64430, 64431, 64432, 64433, 64434, 64435, 64436, 64437, 64438, 64439, 64440, 64441, 64442, 64443, 64444, 64445, 64446, 64447, 64448, 64449, 64467, 64468, 64469, 64470, 64471, 64472, 64473, 64474, 64475, 64476, 64477, 64478, 64479, 64480, 64481, 64482, 64483, 64484, 64485, 64486, 64487, 64488, 64489, 64490, 64491, 64492, 64493, 64494, 64495, 64496, 64497, 64498, 64499, 64500, 64501, 64502, 64503, 64504, 64505, 64506, 64507, 64508, 64509, 64510, 64511, 64512, 64513, 64514, 64515, 64516, 64517, 64518, 64519, 64520, 64521, 64522, 64523, 64524, 64525, 64526, 64527, 64528, 64529, 64530, 64531, 64532, 64533, 64534, 64535, 64536, 64537, 64538, 64539, 64540, 64541, 64542, 64543, 64544, 64545, 64546, 64547, 64548, 64549, 64550, 64551, 64552, 64553, 64554, 64555, 64556, 64557, 64558, 64559, 64560, 64561, 64562, 64563, 64564, 64565, 64566, 64567, 64568, 64569, 64570, 64571, 64572, 64573, 64574, 64575, 64576, 64577, 64578, 64579, 64580, 64581, 64582, 64583, 64584, 64585, 64586, 64587, 64588, 64589, 64590, 64591, 64592, 64593, 64594, 64595, 64596, 64597, 64598, 64599, 64600, 64601, 64602, 64603, 64604, 64605, 64606, 64607, 64608, 64609, 64610, 64611, 64612, 64613, 64614, 64615, 64616, 64617, 64618, 64619, 64620, 64621, 64622, 64623, 64624, 64625, 64626, 64627, 64628, 64629, 64630, 64631, 64632, 64633, 64634, 64635, 64636, 64637, 64638, 64639, 64640, 64641, 64642, 64643, 64644, 64645, 64646, 64647, 64648, 64649, 64650, 64651, 64652, 64653, 64654, 64655, 64656, 64657, 64658, 64659, 64660, 64661, 64662, 64663, 64664, 64665, 64666, 64667, 64668, 64669, 64670, 64671, 64672, 64673, 64674, 64675, 64676, 64677, 64678, 64679, 64680, 64681, 64682, 64683, 64684, 64685, 64686, 64687, 64688, 64689, 64690, 64691, 64692, 64693, 64694, 64695, 64696, 64697, 64698, 64699, 64700, 64701, 64702, 64703, 64704, 64705, 64706, 64707, 64708, 64709, 64710, 64711, 64712, 64713, 64714, 64715, 64716, 64717, 64718, 64719, 64720, 64721, 64722, 64723, 64724, 64725, 64726, 64727, 64728, 64729, 64730, 64731, 64732, 64733, 64734, 64735, 64736, 64737, 64738, 64739, 64740, 64741, 64742, 64743, 64744, 64745, 64746, 64747, 64748, 64749, 64750, 64751, 64752, 64753, 64754, 64755, 64756, 64757, 64758, 64759, 64760, 64761, 64762, 64763, 64764, 64765, 64766, 64767, 64768, 64769, 64770, 64771, 64772, 64773, 64774, 64775, 64776, 64777, 64778, 64779, 64780, 64781, 64782, 64783, 64784, 64785, 64786, 64787, 64788, 64789, 64790, 64791, 64792, 64793, 64794, 64795, 64796, 64797, 64798, 64799, 64800, 64801, 64802, 64803, 64804, 64805, 64806, 64807, 64808, 64809, 64810, 64811, 64812, 64813, 64814, 64815, 64816, 64817, 64818, 64819, 64820, 64821, 64822, 64823, 64824, 64825, 64826, 64827, 64828, 64829, 64848, 64849, 64850, 64851, 64852, 64853, 64854, 64855, 64856, 64857, 64858, 64859, 64860, 64861, 64862, 64863, 64864, 64865, 64866, 64867, 64868, 64869, 64870, 64871, 64872, 64873, 64874, 64875, 64876, 64877, 64878, 64879, 64880, 64881, 64882, 64883, 64884, 64885, 64886, 64887, 64888, 64889, 64890, 64891, 64892, 64893, 64894, 64895, 64896, 64897, 64898, 64899, 64900, 64901, 64902, 64903, 64904, 64905, 64906, 64907, 64908, 64909, 64910, 64911, 64914, 64915, 64916, 64917, 64918, 64919, 64920, 64921, 64922, 64923, 64924, 64925, 64926, 64927, 64928, 64929, 64930, 64931, 64932, 64933, 64934, 64935, 64936, 64937, 64938, 64939, 64940, 64941, 64942, 64943, 64944, 64945, 64946, 64947, 64948, 64949, 64950, 64951, 64952, 64953, 64954, 64955, 64956, 64957, 64958, 64959, 64960, 64961, 64962, 64963, 64964, 64965, 64966, 64967, 65008, 65009, 65010, 65011, 65012, 65013, 65014, 65015, 65016, 65017, 65018, 65019, 65020, 65136, 65137, 65138, 65139, 65140, 65142, 65143, 65144, 65145, 65146, 65147, 65148, 65149, 65150, 65151, 65152, 65153, 65154, 65155, 65156, 65157, 65158, 65159, 65160, 65161, 65162, 65163, 65164, 65165, 65166, 65167, 65168, 65169, 65170, 65171, 65172, 65173, 65174, 65175, 65176, 65177, 65178, 65179, 65180, 65181, 65182, 65183, 65184, 65185, 65186, 65187, 65188, 65189, 65190, 65191, 65192, 65193, 65194, 65195, 65196, 65197, 65198, 65199, 65200, 65201, 65202, 65203, 65204, 65205, 65206, 65207, 65208, 65209, 65210, 65211, 65212, 65213, 65214, 65215, 65216, 65217, 65218, 65219, 65220, 65221, 65222, 65223, 65224, 65225, 65226, 65227, 65228, 65229, 65230, 65231, 65232, 65233, 65234, 65235, 65236, 65237, 65238, 65239, 65240, 65241, 65242, 65243, 65244, 65245, 65246, 65247, 65248, 65249, 65250, 65251, 65252, 65253, 65254, 65255, 65256, 65257, 65258, 65259, 65260, 65261, 65262, 65263, 65264, 65265, 65266, 65267, 65268, 65269, 65270, 65271, 65272, 65273, 65274, 65275, 65276, 67584, 67585, 67586, 67587, 67588, 67589, 67592, 67594, 67595, 67596, 67597, 67598, 67599, 67600, 67601, 67602, 67603, 67604, 67605, 67606, 67607, 67608, 67609, 67610, 67611, 67612, 67613, 67614, 67615, 67616, 67617, 67618, 67619, 67620, 67621, 67622, 67623, 67624, 67625, 67626, 67627, 67628, 67629, 67630, 67631, 67632, 67633, 67634, 67635, 67636, 67637, 67639, 67640, 67644, 67647, 67648, 67649, 67650, 67651, 67652, 67653, 67654, 67655, 67656, 67657, 67658, 67659, 67660, 67661, 67662, 67663, 67664, 67665, 67666, 67667, 67668, 67669, 67671, 67672, 67673, 67674, 67675, 67676, 67677, 67678, 67679, 67840, 67841, 67842, 67843, 67844, 67845, 67846, 67847, 67848, 67849, 67850, 67851, 67852, 67853, 67854, 67855, 67856, 67857, 67858, 67859, 67860, 67861, 67862, 67863, 67864, 67865, 67866, 67867, 67872, 67873, 67874, 67875, 67876, 67877, 67878, 67879, 67880, 67881, 67882, 67883, 67884, 67885, 67886, 67887, 67888, 67889, 67890, 67891, 67892, 67893, 67894, 67895, 67896, 67897, 67903, 67968, 67969, 67970, 67971, 67972, 67973, 67974, 67975, 67976, 67977, 67978, 67979, 67980, 67981, 67982, 67983, 67984, 67985, 67986, 67987, 67988, 67989, 67990, 67991, 67992, 67993, 67994, 67995, 67996, 67997, 67998, 67999, 68e3, 68001, 68002, 68003, 68004, 68005, 68006, 68007, 68008, 68009, 68010, 68011, 68012, 68013, 68014, 68015, 68016, 68017, 68018, 68019, 68020, 68021, 68022, 68023, 68030, 68031, 68096, 68112, 68113, 68114, 68115, 68117, 68118, 68119, 68121, 68122, 68123, 68124, 68125, 68126, 68127, 68128, 68129, 68130, 68131, 68132, 68133, 68134, 68135, 68136, 68137, 68138, 68139, 68140, 68141, 68142, 68143, 68144, 68145, 68146, 68147, 68160, 68161, 68162, 68163, 68164, 68165, 68166, 68167, 68176, 68177, 68178, 68179, 68180, 68181, 68182, 68183, 68184, 68192, 68193, 68194, 68195, 68196, 68197, 68198, 68199, 68200, 68201, 68202, 68203, 68204, 68205, 68206, 68207, 68208, 68209, 68210, 68211, 68212, 68213, 68214, 68215, 68216, 68217, 68218, 68219, 68220, 68221, 68222, 68223, 68352, 68353, 68354, 68355, 68356, 68357, 68358, 68359, 68360, 68361, 68362, 68363, 68364, 68365, 68366, 68367, 68368, 68369, 68370, 68371, 68372, 68373, 68374, 68375, 68376, 68377, 68378, 68379, 68380, 68381, 68382, 68383, 68384, 68385, 68386, 68387, 68388, 68389, 68390, 68391, 68392, 68393, 68394, 68395, 68396, 68397, 68398, 68399, 68400, 68401, 68402, 68403, 68404, 68405, 68416, 68417, 68418, 68419, 68420, 68421, 68422, 68423, 68424, 68425, 68426, 68427, 68428, 68429, 68430, 68431, 68432, 68433, 68434, 68435, 68436, 68437, 68440, 68441, 68442, 68443, 68444, 68445, 68446, 68447, 68448, 68449, 68450, 68451, 68452, 68453, 68454, 68455, 68456, 68457, 68458, 68459, 68460, 68461, 68462, 68463, 68464, 68465, 68466, 68472, 68473, 68474, 68475, 68476, 68477, 68478, 68479, 68608, 68609, 68610, 68611, 68612, 68613, 68614, 68615, 68616, 68617, 68618, 68619, 68620, 68621, 68622, 68623, 68624, 68625, 68626, 68627, 68628, 68629, 68630, 68631, 68632, 68633, 68634, 68635, 68636, 68637, 68638, 68639, 68640, 68641, 68642, 68643, 68644, 68645, 68646, 68647, 68648, 68649, 68650, 68651, 68652, 68653, 68654, 68655, 68656, 68657, 68658, 68659, 68660, 68661, 68662, 68663, 68664, 68665, 68666, 68667, 68668, 68669, 68670, 68671, 68672, 68673, 68674, 68675, 68676, 68677, 68678, 68679, 68680, 126464, 126465, 126466, 126467, 126469, 126470, 126471, 126472, 126473, 126474, 126475, 126476, 126477, 126478, 126479, 126480, 126481, 126482, 126483, 126484, 126485, 126486, 126487, 126488, 126489, 126490, 126491, 126492, 126493, 126494, 126495, 126497, 126498, 126500, 126503, 126505, 126506, 126507, 126508, 126509, 126510, 126511, 126512, 126513, 126514, 126516, 126517, 126518, 126519, 126521, 126523, 126530, 126535, 126537, 126539, 126541, 126542, 126543, 126545, 126546, 126548, 126551, 126553, 126555, 126557, 126559, 126561, 126562, 126564, 126567, 126568, 126569, 126570, 126572, 126573, 126574, 126575, 126576, 126577, 126578, 126580, 126581, 126582, 126583, 126585, 126586, 126587, 126588, 126590, 126592, 126593, 126594, 126595, 126596, 126597, 126598, 126599, 126600, 126601, 126603, 126604, 126605, 126606, 126607, 126608, 126609, 126610, 126611, 126612, 126613, 126614, 126615, 126616, 126617, 126618, 126619, 126625, 126626, 126627, 126629, 126630, 126631, 126632, 126633, 126635, 126636, 126637, 126638, 126639, 126640, 126641, 126642, 126643, 126644, 126645, 126646, 126647, 126648, 126649, 126650, 126651, 1114109];
    j.prototype.applyStyles = function(a, b) {
        b = b || this.div;
        for (var c in a) a.hasOwnProperty(c) && (b.style[c] = a[c])
    }, j.prototype.formatStyle = function(a, b) {
        return 0 === a ? 0 : a + b
    }, k.prototype = o(j.prototype), k.prototype.constructor = k, l.prototype.move = function(a, b) {
        switch (b = void 0 !== b ? b : this.lineHeight, a) {
            case "+x":
                this.left += b, this.right += b;
                break;
            case "-x":
                this.left -= b, this.right -= b;
                break;
            case "+y":
                this.top += b, this.bottom += b;
                break;
            case "-y":
                this.top -= b, this.bottom -= b
        }
    }, l.prototype.overlaps = function(a) {
        return this.left < a.right && this.right > a.left && this.top < a.bottom && this.bottom > a.top
    }, l.prototype.overlapsAny = function(a) {
        for (var b = 0; b < a.length; b++)
            if (this.overlaps(a[b])) return !0;
        return !1
    }, l.prototype.within = function(a) {
        return this.top >= a.top && this.bottom <= a.bottom && this.left >= a.left && this.right <= a.right
    }, l.prototype.overlapsOppositeAxis = function(a, b) {
        switch (b) {
            case "+x":
                return this.left < a.left;
            case "-x":
                return this.right > a.right;
            case "+y":
                return this.top < a.top;
            case "-y":
                return this.bottom > a.bottom
        }
    }, l.prototype.intersectPercentage = function(a) {
        var b = Math.max(0, Math.min(this.right, a.right) - Math.max(this.left, a.left)),
            c = Math.max(0, Math.min(this.bottom, a.bottom) - Math.max(this.top, a.top)),
            d = b * c;
        return d / (this.height * this.width)
    }, l.prototype.toCSSCompatValues = function(a) {
        return {
            top: this.top - a.top,
            bottom: a.bottom - this.bottom,
            left: this.left - a.left,
            right: a.right - this.right,
            height: this.height,
            width: this.width
        }
    }, l.getSimpleBoxPosition = function(a) {
        var b = a.div ? a.div.offsetHeight : a.tagName ? a.offsetHeight : 0,
            c = a.div ? a.div.offsetWidth : a.tagName ? a.offsetWidth : 0,
            d = a.div ? a.div.offsetTop : a.tagName ? a.offsetTop : 0;
        a = a.div ? a.div.getBoundingClientRect() : a.tagName ? a.getBoundingClientRect() : a;
        var e = {
            left: a.left,
            right: a.right,
            top: a.top || d,
            height: a.height || b,
            bottom: a.bottom || d + (a.height || b),
            width: a.width || c
        };
        return e
    }, n.StringDecoder = function() {
        return {
            decode: function(a) {
                if (!a) return "";
                if ("string" != typeof a) throw new Error("Error - expected string data.");
                return decodeURIComponent(encodeURIComponent(a))
            }
        }
    }, n.convertCueToDOMTree = function(a, b) {
        return a && b ? g(a, b) : null
    };
    var u = .05,
        v = "sans-serif",
        w = "1.5%";
    n.processCues = function(a, b, c) {
        function d(a) {
            for (var b = 0; b < a.length; b++)
                if (a[b].hasBeenReset || !a[b].displayState) return !0;
            return !1
        }
        if (!a || !b || !c) return null;
        for (; c.firstChild;) c.removeChild(c.firstChild);
        var e = a.document.createElement("div");
        if (e.style.position = "absolute", e.style.left = "0", e.style.right = "0", e.style.top = "0", e.style.bottom = "0", e.style.margin = w, c.appendChild(e), d(b)) {
            var f = [],
                g = l.getSimpleBoxPosition(e),
                h = Math.round(g.height * u * 100) / 100,
                i = {
                    font: h + "px " + v
                };
            ! function() {
                for (var c, d, h = 0; h < b.length; h++) d = b[h], c = new k(a, d, i), e.appendChild(c.div), m(a, c, g, f), d.displayState = c.div, f.push(l.getSimpleBoxPosition(c))
            }()
        } else
            for (var j = 0; j < b.length; j++) e.appendChild(b[j].displayState)
    }, n.Parser = function(a, b, c) {
        c || (c = b, b = {}), b || (b = {}), this.window = a, this.vttjs = b, this.state = "INITIAL", this.buffer = "", this.decoder = c || new TextDecoder("utf8"), this.regionList = []
    }, n.Parser.prototype = {
        reportOrThrowError: function(a) {
            if (!(a instanceof b)) throw a;
            this.onparsingerror && this.onparsingerror(a)
        },
        parse: function(a) {
            function c() {
                for (var a = i.buffer, b = 0; b < a.length && "\r" !== a[b] && "\n" !== a[b];) ++b;
                var c = a.substr(0, b);
                return "\r" === a[b] && ++b, "\n" === a[b] && ++b, i.buffer = a.substr(b), c
            }

            function g(a) {
                var b = new d;
                if (e(a, function(a, c) {
                        switch (a) {
                            case "id":
                                b.set(a, c);
                                break;
                            case "width":
                                b.percent(a, c);
                                break;
                            case "lines":
                                b.integer(a, c);
                                break;
                            case "regionanchor":
                            case "viewportanchor":
                                var e = c.split(",");
                                if (2 !== e.length) break;
                                var f = new d;
                                if (f.percent("x", e[0]), f.percent("y", e[1]), !f.has("x") || !f.has("y")) break;
                                b.set(a + "X", f.get("x")), b.set(a + "Y", f.get("y"));
                                break;
                            case "scroll":
                                b.alt(a, c, ["up"])
                        }
                    }, /=/, /\s/), b.has("id")) {
                    var c = new(i.vttjs.VTTRegion || i.window.VTTRegion);
                    c.width = b.get("width", 100), c.lines = b.get("lines", 3), c.regionAnchorX = b.get("regionanchorX", 0), c.regionAnchorY = b.get("regionanchorY", 100), c.viewportAnchorX = b.get("viewportanchorX", 0), c.viewportAnchorY = b.get("viewportanchorY", 100), c.scroll = b.get("scroll", ""), i.onregion && i.onregion(c), i.regionList.push({
                        id: b.get("id"),
                        region: c
                    })
                }
            }

            function h(a) {
                e(a, function(a, b) {
                    switch (a) {
                        case "Region":
                            g(b)
                    }
                }, /:/)
            }
            var i = this;
            a && (i.buffer += i.decoder.decode(a, {
                stream: !0
            }));
            try {
                var j;
                if ("INITIAL" === i.state) {
                    if (!/\r\n|\n/.test(i.buffer)) return this;
                    j = c();
                    var k = j.match(/^WEBVTT([ \t].*)?$/);
                    if (!k || !k[0]) throw new b(b.Errors.BadSignature);
                    i.state = "HEADER"
                }
                for (var l = !1; i.buffer;) {
                    if (!/\r\n|\n/.test(i.buffer)) return this;
                    switch (l ? l = !1 : j = c(), i.state) {
                        case "HEADER":
                            /:/.test(j) ? h(j) : j || (i.state = "ID");
                            continue;
                        case "NOTE":
                            j || (i.state = "ID");
                            continue;
                        case "ID":
                            if (/^NOTE($|[ \t])/.test(j)) {
                                i.state = "NOTE";
                                break
                            }
                            if (!j) continue;
                            if (i.cue = new(i.vttjs.VTTCue || i.window.VTTCue)(0, 0, ""), i.state = "CUE", -1 === j.indexOf("-->")) {
                                i.cue.id = j;
                                continue
                            }
                        case "CUE":
                            try {
                                f(j, i.cue, i.regionList)
                            } catch (m) {
                                i.reportOrThrowError(m), i.cue = null, i.state = "BADCUE";
                                continue
                            }
                            i.state = "CUETEXT";
                            continue;
                        case "CUETEXT":
                            var n = -1 !== j.indexOf("-->");
                            if (!j || n && (l = !0)) {
                                i.oncue && i.oncue(i.cue), i.cue = null, i.state = "ID";
                                continue
                            }
                            i.cue.text && (i.cue.text += "\n"), i.cue.text += j;
                            continue;
                        case "BADCUE":
                            j || (i.state = "ID");
                            continue
                    }
                }
            } catch (m) {
                i.reportOrThrowError(m), "CUETEXT" === i.state && i.cue && i.oncue && i.oncue(i.cue), i.cue = null, i.state = "INITIAL" === i.state ? "BADWEBVTT" : "BADCUE"
            }
            return this
        },
        flush: function() {
            var a = this;
            try {
                if (a.buffer += a.decoder.decode(), (a.cue || "HEADER" === a.state) && (a.buffer += "\n\n", a.parse()), "INITIAL" === a.state) throw new b(b.Errors.BadSignature)
            } catch (c) {
                a.reportOrThrowError(c)
            }
            return a.onflush && a.onflush(), this
        }
    }, a.WebVTT = n
}(this, this.vttjs || {}), ! function(a, t, e, n, m) {
    m = t.location, Math.random() > .01 || (a.src = "//www.google-analytics.com/__utm.gif?utmwv=5.4.2&utmac=UA-16505296-2&utmn=1&utmhn=" + n(m.hostname) + "&utmsr=" + t.screen.availWidth + "x" + t.screen.availHeight + "&utmul=" + (e.language || e.userLanguage || "").toLowerCase() + "&utmr=" + n(m.href) + "&utmp=" + n(m.hostname + m.pathname) + "&utmcc=__utma%3D1." + Math.floor(1e10 * Math.random()) + ".1.1.1.1%3B&utme=8(vjsv)9(v4.12.11)")
}(new Image, window, navigator, encodeURIComponent),
function() {
    videojs.plugin("omniVideoTracker", function(e) {
        var a, n, t, d, o, r, u, c, f, h, p, l, M, y, c, k, m;
        e = e || {}, t = [], f = [], a = {}, this.options()["data-tracking"] && (a = JSON.parse(this.options()["data-tracking"]).omniVideoTracker), h = function() {
            d = a.interval || 10, n = a.videoName || this.id(), m = a.player || "Video.js Player", k = Math.round(this.duration()), u = !1, r = o = 0, c("Loaded")
        }, p = function() {
            var e, i, a, n;
            if (e = Math.round(this.currentTime()), n = Math.round(e / k * 100), r = o, o = e, Math.abs(r - o) > 1) u = !0, c("Seeking", e);
            else
                for (a = i = 0; 99 >= i; a = i += d) n >= a && t.indexOf(a) < 0 && (n > 0 && 0 === a ? c("Started") : 0 === n || u || c("Interval", a), n > 0 && t.push(a));
            f.length > 0 && clearQueue()
        }, clearQueue = function() {
            for (i = f.length; i--;) f.splice(i, 1)
        }, l = function() {
            c("End")
        }, M = function() {
            var e = Math.round(this.currentTime());
            u = !1, c("Play", e)
        }, y = function() {
            var e = Math.round(this.currentTime());
            e === k || u || c("Paused")
        }, c = function(e, i) {
            if ("object" != typeof window.s) {
                var a = (new Date).getTime();
                f.push([e, i, a])
            } else _send(e, i)
        }, _send = function(e, i) {
            switch (e) {
                case "Paused":
                    "undefined" == typeof s.Media || s.Media.stop(n, i);
                    break;
                case "Started":
                    "undefined" == typeof s.Media || (s.Media.open(n, k, m), s.Media.play(n, 0));
                    break;
                case "Play":
                    "undefined" == typeof s.Media || s.Media.play(n, i);
                    break;
                case "Ended":
                    "undefined" == typeof s.Media || (s.Media.stop(n, i), s.Media.close(n));
                    break;
                case "Seeking":
                    "undefined" == typeof s.Media || s.Media.stop(n, i)
            }
        }, this.ready(function() {
            this.on("loadedmetadata", h), this.on("timeupdate", p), this.on("ended", l), this.on("play", M), this.on("pause", y)
        })
    })
}.call(this), "undefined" != typeof __scriptCount__ && (__scriptCount__ += 1);
var requestor_cc = "US",
    MTIProjectId = "1c3a6904-ecfd-4585-b175-511c2f7287c8",
    aniRun = !1;
setRequestorLocation();
var MOPROErrorMessages = {};

var HotelInformation = {};
$(document).ready(function() {
        HotelInformation = $.ajax({
            type: "GET",
            dataType: "text",
            global: !1,
            async: !1,
            success: function(data) {
                return data
            }
        })
    }),
    function($) {
        $.fn.imgSwap = function(path) {
            $(this).attr("src", path)
        }, $.fn.initSlidingMenu = function() {
            var $gallery = $(".gallery"),
                $slide_link = $(this).find("a.slide"),
                $nav = $(this).find(".secondary-nav"),
                $close = $(this).find("a.close"),
                $this = this;
            $slide_link.click(function(e) {
                var $clicked = $(this),
                    target = this.hash;
                if ($(".slide.active") && $(".slide.active").attr("href") != target) {
                    var closeThis = $(".slide.active").attr("href");
                    $(".slide.active").removeClass("active"), $(closeThis).animate({
                        width: "0px"
                    }, 1e3, function() {})
                }
                return $clicked.hasClass("active") ? ($nav.removeClass("active"), $(target).animate({
                    width: "0px"
                }, 1e3, function() {}), $clicked.removeClass("active")) : ($nav.addClass("active"), $(target).animate({
                    width: "350px"
                }, 1e3, function() {
                    $gallery.length > 0 && $(this).hasClass("gallery-menu-item") && ($gallery.is(":visible") || $gallery.fadeIn("fast"))
                }), $clicked.addClass("active")), $($this).find($(this).attr("href") + " .wrap-contents").each(function() {
                    var api = $(this).data("jsp");
                    api.scrollToY(0)
                }), !1
            }), $close.click(function(e) {
                return e.preventDefault(), $(this).parents(".slide-menu-item").animate({
                    width: "0px"
                }, 1e3, function() {
                    $nav.find("a.active").removeClass("active"), $nav.removeClass("active")
                }), !1
            }), $(this).find(".slide-menu-item .wrap .wrap-contents").each(function() {
                $(this).parent().parent().show(), $(this).jScrollPane()
            })
        }, $.fn.initGallery = function() {
            var $gallery = $(this).find(".gallery"),
                $nav = $(".gallery-nav");
            $nav.find("li").hover(function() {
                $(this).find(".rollover").fadeOut("fast")
            }, function() {
                $(this).hasClass("active") || $(this).find(".rollover").fadeIn("fast")
            }).find("a").click(function() {
                return $nav.find(".active").removeClass("active"), $(this).parent("li").addClass("active"), $nav.find("li:not(.active) .rollover").fadeIn("fast"), gotoGalleryImg(this.hash, $gallery), !1
            })
        }, $.fn.initAccordion = function() {
            var $container = $(this),
                $toggle = $container.find("a.toggle");
            $toggle.click(function() {
                var $block = $(this).parents(".block");
                return $block.hasClass("active") ? (hideBlock($block), $("html, body").animate({
                    scrollTop: 0
                }, 800)) : (showBlock($block), $("html, body").animate({
                    scrollTop: $block.offset().top - 10
                }, 800)), !1
            })
        }, $.fn.initFilters = function() {
            if (!$("body").hasClass("mobile")) {
                var $blocks = $(this).parents(".contact-us").find(".block > .wrap > .sub-block").children(":not(.contact-filters,:hidden)").parents(".block"),
                    $target = $(this).find("input");
                $target.change(function() {
                    var goto = $(this).val(),
                        scrollto = "";
                    "all" == goto ? ($blocks.each(function() {
                        $(this).hasClass("active") || showBlock($(this), !1)
                    }), scrollto = ".block:eq(0)", $("html, body").animate({
                        scrollTop: $(scrollto).offset().top - 10
                    }, 800)) : ($blocks.each(function() {
                        $(this).hasClass("active") && hideBlock($(this), !0)
                    }), scrollto = "#" + goto, showBlock($(scrollto), function() {
                        $("html, body").animate({
                            scrollTop: $(scrollto).offset().top - 60
                        }, 800)
                    }))
                })
            }
        }
    }(jQuery), "undefined" != typeof __scriptCount__ && (__scriptCount__ += 1);
var addThisInitialized = !1,
    currentGalleryDetail = null,
    galleryInstantAnimate = !1,
    currentOpenProfileBlock = null,
    profileInstantAnimate = !1,
    denyImageZoomClosing = !1,
    passwordRegex = /^[^0-9][A-Za-z0-9]{7,}$/,
    emailRegex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/,
    videoViewer = null,
    isMobile = {
        A: function() {
            return navigator.userAgent.match(/Android/i)
        },
        B: function() {
            return navigator.userAgent.match(/BlackBerry/i)
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i)
        },
        O: function() {
            return navigator.userAgent.match(/Opera Mini/i)
        },
        W: function() {
            return navigator.userAgent.match(/IEMobile/i)
        },
        any: function() {
            return isMobile.A() || isMobile.B() || isMobile.iOS() || isMobile.O() || isMobile.W()
        }
    };
! function() {
    if ("-ms-user-select" in document.documentElement.style && navigator.userAgent.match(/IEMobile\/10\.0/)) {
        var msViewportStyle = document.createElement("style");
        msViewportStyle.appendChild(document.createTextNode("@-ms-viewport{width:auto!important}")), document.getElementsByTagName("head")[0].appendChild(msViewportStyle)
    }
}();
var hoverTimeouts = {};
! function($) {
    function centerVideo() {
        if ($(".testvideo").length > 0) {
            var newVideoLeft = $(".testvideo").width() / 2 - $(".testvideo video").width() / 2;
            $(".testvideo video").css("left", newVideoLeft + "px")
        }
    }
    $(document).ready(function() {
        function endRequestHandler(sender, args) {
            $("body.mobile").length < 1 && $("body").find(".fieldset select, .callout-inner select, select, .fieldset input[type=radio], .fieldset input[type=checkbox]").uniform(), $(".newsletter").find(".primary, .button").addClass("loader-button")
        }

        function scriptDelay() {
            2 !== __scriptCount__ ? setTimeout(function() {
                scriptDelay()
            }, 100) : $("body").mohginit()
        }
        if (Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequestHandler), $("input[placeholder], textarea[placeholder]").placeholder(), 0 == $("body.mobile").length) {
            if (navigator.userAgent.toString().toLowerCase().indexOf("ipad") >= 0) {
                var viewport = document.querySelector("meta[name=viewport]");
                "undefined" != typeof viewport && null != viewport && viewport.setAttribute("content", "")
            }
        } else {
            $(".gotoFullSite").length > 0 && $(".gotoFullSite").click(function(event) {
                event.preventDefault();
                var domain = window.location.host,
                    now = new Date,
                    time = now.getTime();
                time += 36e5, now.setTime(time), domain.indexOf("siteworx.com") >= 0 && (domain = "siteworx.com"), domain.indexOf("m.") >= 0 && (domain = domain.replace("m.", "")), document.cookie = "view_desktop=true; expires=" + now.toUTCString() + "; Domain=" + domain + "; path=/", window.location.href.indexOf("siteworx") > 0 ? setTimeout(function() {
                    window.location = window.location.href.replace("-m", "")
                }, 500) : setTimeout(function() {
                    -1 != window.location.search.indexOf("?") ? window.location.search += "&reload=true" : window.location.search = "?reload=true"
                }, 500)
            });
            var os_regex = new RegExp("os ((\\d+_?){2,3})\\s", "g"),
                matchesOSversion = navigator.userAgent.toString().toLowerCase().match(os_regex),
                iOSVersion = 1;
            if (null != matchesOSversion) {
                matchesOSversion = matchesOSversion[0].replace("os ", "");
                var matchesOSversionSplit = matchesOSversion.split("_");
                iOSVersion = parseInt(matchesOSversionSplit[0])
            }
            if (navigator.userAgent.toString().toLowerCase().indexOf("iphone") >= 0 && iOSVersion >= 5) {
                var viewport = document.querySelector("meta[name=viewport]");
                "undefined" != typeof viewport && null != viewport && viewport.setAttribute("content", "width=450px")
            } else {
                var viewport = document.querySelector("meta[name=viewport]");
                "undefined" != typeof viewport && null != viewport && viewport.setAttribute("content", "width=device-width, initial-scale=1")
            }
        }
        var myloc = "",
            pn = window.location.pathname,
            pnarr = pn.split("/");
        if (pnarr.length > 0 && (myloc = pnarr[1]), "" != myloc && $('nav li a[href*="residences/' + myloc + '/"]').parent().addClass("res-marker"), "undefined" != typeof banneritems && banneritems.length > 0) {
            for (var i = 0; i < banneritems.length; ++i) {
                var cssClass = banneritems[i].cssClass,
                    text = banneritems[i].text;
                if (text = $("<textarea />").html(text).text(), -1 === document.cookie.indexOf(cssClass + "_banner")) {
                    switch (cssClass) {
                        case "ie8":
                            if (!$("html").hasClass("oldie")) continue;
                            break;
                        case "cookie":
                            if (!(location.host.endsWith(".cz") || location.host.endsWith(".de") || location.host.endsWith(".es") || location.host.endsWith(".fr") || location.host.endsWith(".it") || $.inArray(requestor_cc, ["AT", "BE", "BG", "CY", "CZ", "DE", "DK", "EE", "ES", "FI", "FR", "GB", "GR", "HR", "HU", "IE", "IT", "LT", "LU", "LV", "MT", "NL", "PL", "PT", "RO", "SE", "SI", "SK"]) > -1)) continue;
                            break;
                        default:
                            continue
                    }
                    $('<div class="' + cssClass + ' notice-banner"><div><a class="close" href="#">Close X</a><p>' + text + "</p></div></div>").insertBefore("#page-wrap > header")
                }
            }
            var totalHeight = 0;
            $(".notice-banner").each(function() {
                $(this).css("top", totalHeight), totalHeight += $(this).outerHeight()
            }), $("#page-wrap > header, .corpglobal.home-page #main").css("margin-top", totalHeight)
        }
        $(".notice-banner .close").click(function() {
            var $banner = $(this).closest(".notice-banner"),
                classes = $banner.attr("class").split(/\s+/);
            if (classes = $.grep(classes, function(c) {
                    return -1 !== $.inArray(c, ["ie8", "cookie"])
                }), classes.length > 0) {
                var cssClass = classes[0],
                    t = new Date;
                t.setFullYear(t.getFullYear() + 1);
                var expiration = t.toUTCString(),
                    domain = window.location.host;
                domain.indexOf("siteworx.com") >= 0 && (domain = "siteworx.com"), domain.indexOf("m.") >= 0 && (domain = domain.replace("m.", "")), document.cookie = cssClass + "_banner=hide; expires=" + expiration + "; Domain=" + domain + "; path=/"
            }
            $(this).closest(".notice-banner").remove();
            var totalHeight = 0;
            $(".notice-banner").each(function() {
                $(this).css("top", totalHeight), totalHeight += $(this).outerHeight()
            }), $("#page-wrap > header, .corpglobal.home-page #main").css("margin-top", 0 != totalHeight ? totalHeight : "")
        }), "undefined" != typeof __scriptCount__ ? setTimeout(function() {
            scriptDelay()
        }, 100) : $(window).load(function() {
            $("body").mohginit()
        })
    }), $.fn.mohginit = function() {
        function setNavigationItemHover($item, checkStatus, e) {
            $item.hasClass("touched") || (checkStatus === !1 || "undefined" != typeof fan_club_signedin && 1 === fan_club_signedin) && ($this.find("#header-lists ul > li.more-action.hover").removeClass("hover"), $item.addClass("hover"), clearTimeout(hoverTimeouts[$item.uniqueId()[0].id]))
        }

        function setupImageZoom() {
            0 == $("#image-zoom").length && ($("body").append('<div id="image-zoom" style="display:none;"><div class="close"></div></div>'), $("#image-zoom").click(function() {
                closeImageZoom()
            }), $(window).resize(function() {
                if ($("#image-zoom img").length > 0) {
                    var imgSrc = $("#image-zoom img").attr("src"),
                        imgPresetStart = imgSrc.indexOf("?hei"); - 1 == imgPresetStart && (imgPresetStart = imgSrc.indexOf("?wid")), imgSrc = imgSrc.substring(0, imgPresetStart);
                    var winHeight = $(window).height();
                    !isMobile.any(), $("#image-zoom img").attr("src", imgSrc + "?hei=" + winHeight), imageZoomOnImageLoad(imgSrc)
                }
                if ($("#image-zoom .zoom_video_cont").length > 0) {
                    var $container = $("#image-zoom .zoom_video_cont"),
                        winHeight = $(window).height(),
                        contHeight = $(this).height(),
                        newTop = winHeight / 2 - contHeight / 2;
                    $(this).css("margin-top", newTop + "px");
                    var $video = $container.find("video");
                    if ($video.length > 0) {
                        var vidWidth = $video.attr("width"),
                            vidHeight = $video.attr("height");
                        vidWidth && $container.css("width", vidWidth), vidHeight && $container.css("height", vidHeight)
                    }
                }
            }), $(window).scroll(function() {
                $("#image-zoom").length > 0 && !isMobile.any()
            }), $("#image-zoom").hover(function() {
                mouseInImageZoom = !0
            }, function() {
                mouseInImageZoom = !1
            }))
        }

        function dateGen(t) {
            var weekDay = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"),
                monthDay = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
            return weekDay[t.getDay()] + ", " + monthDay[t.getMonth()] + " " + t.getDate() + ", " + t.getFullYear()
        }

        function TranlateDateString(inputDate) {
            var transDate = inputDate;
            if ("undefined" != typeof globalLang) {
                var weekDays = $.datepicker.regional[globalLang].dayNames,
                    months = $.datepicker.regional[globalLang].monthNames,
                    englishDayNames = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"),
                    englishMonthNames = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
                if (globalLang.indexOf("zh") > -1) {
                    var asiaDay = inputDate.match(/\d*日/g);
                    asiaDay = asiaDay[0].replace("日", "");
                    var asiaMonth = inputDate.match(/\d*月/g);
                    asiaMonth = asiaMonth[0].replace("月", "");
                    var asiaYear = inputDate.match(/\d*年/g);
                    asiaYear = asiaYear[0].replace("年", "");
                    var asiaDate = new Date(asiaYear, asiaMonth - 1, asiaDay);
                    transDate = asiaDate
                } else
                    for (var i = 0; 12 > i; i++) inputDate.indexOf(weekDays[i]) >= 0 && (transDate = transDate.replace(weekDays[i], englishDayNames[i])), inputDate.indexOf(months[i]) >= 0 && (transDate = transDate.replace(months[i], englishMonthNames[i]))
            }
            return transDate
        }

        function dateCalendar() {
            var arrivalDate = new Date,
                ddTime = parseInt(arrivalDate.getTime()) + 1728e5,
                departureDate = new Date(parseInt(ddTime)),
                imageLocation = "/static/images/icons/calendar-icon-checkout.png";
            $this.find('.room-availability input[name*="from"]').not('.carousel-check-availability input[name*="from"], .overview-check-availability input[name*="from"]').val(dateGen(arrivalDate)), $this.find('.room-availability input[name*="to"]').not('.carousel-check-availability input[name*="to"], .overview-check-availability input[name*="to"]').val(dateGen(departureDate)), $this.find('.room-availability input[name*="Date"]').not('.carousel-check-availability input[name*="Date"], .overview-check-availability input[name*="Date"]').val(dateGen(arrivalDate)), $("body.residences").length > 0 && (imageLocation = "/static/images/icons/residences-cal-icon.png");
            var dates = $(".room-availability input[name*='from'],.room-availability input[name*='to'], .typeDateSelectorWrap .gridview-universal input.date-display[name*='from'], .typeDateSelectorWrap .gridview-universal input.date-display[name*='to']").datepicker({
                minDate: "0",
                changeMonth: !0,
                changeYear: !0,
                yearRange: "c:c+1",
                maxDate: "+1y",
                numberOfMonths: 1,
                dateFormat: pageDateFormat,
                showOn: "button",
                buttonImage: imageLocation,
                buttonImageOnly: !0,
                onSelect: function(selectedDate) {
                    var nextDate, option = $(this).attr("name").indexOf("from") > -1 ? "minDate" : "",
                        instance = $(this).data("datepicker"),
                        conJPDate = ($.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings), selectedDate),
                        PageURL = document.URL;
                    if (PageURL.indexOf("mandarinoriental.co.jp") >= 0 || PageURL.indexOf("jp.siteworx.com") >= 0) {
                        var numberPattern = /\d+/g,
                            datestr = conJPDate.match(numberPattern).toString(),
                            datesarr = datestr.split(",");
                        if (datesarr.length > 2) {
                            var myMonth = datesarr[1],
                                myDay = datesarr[2],
                                myYear = datesarr[0];
                            conJPDate = myYear + " " + myMonth + " " + myDay
                        }
                        nextDate = new Date(conJPDate + ",00:00:00")
                    } else nextDate = new Date(TranlateDateString(selectedDate));
                    "undefined" == typeof minStay && (minStay = 1), nextDate.setDate(nextDate.getDate() + minStay), dates.not(this).datepicker("option", option, nextDate), $(this).attr("name").indexOf("from") > -1 && dates.not(this).datepicker("setDate", nextDate)
                }
            });
            $('.carousel-check-availability input[name*="from"], .overview-check-availability input[name*="from"], .carousel-check-availability input[name*="to"], .overview-check-availability input[name*="to"], .typeDateSelectorWrap .gridview-universal input.date-display[name*="from"], .typeDateSelectorWrap .gridview-universal input.date-display[name*="to"]').on("click", function() {
                $(this).datepicker().datepicker("show")
            }), "undefined" != typeof minCheckAvailCarouselDate && $('.carousel-check-availability input[name*="from"], .overview-check-availability input[name*="from"]').datepicker("option", "minDate", new Date(minCheckAvailCarouselDate)), !$("body").hasClass("mobile") || "rtl" !== $("html").attr("dir") && "ar" !== $("html").attr("lang") || dates.datepicker("option", "isRTL", !1), $('[for="from"],[for="to"]').attr("for", function() {
                return $(this).siblings('input[name="' + $(this).attr("for") + '"]').attr("id")
            });
            $(".room-availability input[name*='Date']").datepicker({
                numberOfMonths: 1,
                minDate: "-10y",
                changeMonth: !0,
                changeYear: !0,
                yearRange: "1998:2098",
                maxDate: "+10y",
                dateFormat: pageDateFormat,
                showOn: "button",
                buttonImage: imageLocation,
                buttonImageOnly: !0,
                onSelect: function(selectedDate) {
                    instance = $(this).data("datepicker"), date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings)
                }
            });
            "undefined" != typeof overrideAvailabilityDates && setRoomAvailabilityDates(overrideAvailabilityDates)
        }

        function setRoomAvailabilityDates(dateRanges) {
            if (dateRanges) {
                var availModules = $(".room-availability").not(".carousel-check-availability .room-availability, .overview-check-availability .room-availability");
                availModules.length > 0 && availModules.each(function(index) {
                    if (void 0 != dateRanges[index]) {
                        var tempRangeObject = dateRanges[index];
                        void 0 != tempRangeObject.from && $(this).find("input[id*='_from']").length > 0 && $(this).find("input[id*='_from']").datepicker("setDate", new Date(tempRangeObject.from)), void 0 != tempRangeObject.to && $(this).find("input[id*='_to']").length > 0 && $(this).find("input[id*='_to']").datepicker("setDate", new Date(tempRangeObject.to))
                    }
                })
            }
        }

        function galleryLoading() {
            currentGalleryDetail = null, $(".image-gallery-loader").show()
        }

        function galleryKeyboardNav(event) {
            if ($(".image-gallery").hasClass("has-keyboard-focus")) {
                var activeDetailRow = ($this.find("ul.image-gallery>li.detail-view>ul>li").length, $this.find("ul.image-gallery>li.detail-view").length, []),
                    activeDetailItemRel = 0,
                    newActiveDetailItemRel = 0,
                    arrowPress = !1;
                if ($this.find("ul.image-gallery>li.detail-view>ul>li:visible") && (activeDetailItemRel = parseInt($this.find("ul.image-gallery>li.detail-view>ul>li:visible").attr("rel"))), activeDetailItemRel && $this.find("ul.image-gallery>li.detail-view").hasClass("active")) {
                    switch (activeDetailRow = $this.find("ul.image-gallery>li.detail-view.active"), event.which) {
                        case 27:
                            arrowPress = !0;
                            break;
                        case 37:
                            arrowPress = !0, 1 == activeDetailItemRel || activeDetailItemRel >= 2 && (newActiveDetailItemRel = activeDetailItemRel - 1);
                            break;
                        case 38:
                            arrowPress = !0, 1 == Math.ceil(activeDetailItemRel / 3) || Math.ceil(activeDetailItemRel / 3) >= 2 && (newActiveDetailItemRel = activeDetailItemRel - 3);
                            break;
                        case 39:
                            arrowPress = !0;
                            var nADIR = activeDetailItemRel + 1;
                            $this.find('ul.image-gallery>li.detail-view>ul>li[value="' + nADIR + '"]') && (newActiveDetailItemRel = nADIR);
                            break;
                        case 40:
                            arrowPress = !0;
                            var nADIR = activeDetailItemRel + 3;
                            $this.find('ul.image-gallery>li.detail-view>ul>li[value="' + nADIR + '"]') && (newActiveDetailItemRel = nADIR)
                    }
                    arrowPress && (currentGalleryDetail = $this.find(".image-gallery li a[rel=" + newActiveDetailItemRel + "]"), galleryGoTo($this.find("ul.image-gallery"), activeDetailRow, activeDetailItemRel, newActiveDetailItemRel, numPerRow))
                }
            }
        }

        function setScale() {
            if ($(".image-gallery-overlay:visible").length > 0 && !navigator.userAgent.match(/(iPhone|iPod|iPad)/i)) switch (window.orientation) {
                case -90:
                case 90:
                    $("meta[name=viewport]").attr("content", "width=device-width,initial-scale=0.5");
                    break;
                default:
                    $("meta[name=viewport]").attr("content", "width=device-width,initial-scale=1")
            }
        }

        function showLightboxOverlay() {
            clearTimeout(overlayTimeout), $(".bottom-gradient, .upper-gradient, .wrap-cycle-prev, .wrap-cycle-next").fadeIn("fast", function() {
                overlayIsVisible = !0
            })
        }

        function hideLightboxOverlay() {
            clearTimeout(overlayTimeout), $(".bottom-gradient, .upper-gradient, .wrap-cycle-prev, .wrap-cycle-next").fadeOut("fast", function() {
                overlayIsVisible = !1
            })
        }

        function showHideLightboxOverlay(target) {
            0 == $(target).parents(".addShare, .wrap-cycle-prev, .wrap-cycle-next, .close-wrap, .action-wrap").length && (overlayIsVisible ? hideLightboxOverlay() : showLightboxOverlay())
        }

        function lightboxStyleGalleryCarouselClose() {
            $(".image-gallery-overlay").fadeOut("fast"), $("nav").css("z-index", "4"), $("header").css("z-index", "3"), $("#hotel-gallery-overlay-items").fadeOut("fast"), showLightboxOverlay(), clearTimeout(overlayTimeout), navigator.userAgent.match(/(iPhone|iPod|iPad)/i) || $("meta[name=viewport]").attr("content", "width=device-width,initial-scale=1")
        }

        function addMultivalueSelection(select, list, selectedOption) {
            list.append('<li><span class="remove"></span><span class="text">' + selectedOption.text() + '</span><span class="value">' + selectedOption.val() + "</span></li>");
            list.children(":last").find(".remove").click(function() {
                var hotelName = $(this).siblings(".text").html(),
                    hotelValue = $(this).siblings(".value").html();
                select.append('<option value="' + hotelValue + '">' + hotelName + "</option>"), $(this).parent().remove(), reorderMultivalueSelectOptions(select), buildMultivalueFieldValue(select)
            }), selectedOption.remove()
        }

        function reorderMultivalueSelectOptions(select) {
            var options = select.children("option"),
                selectOptions = [],
                orderedOptions = [];
            options.each(function() {
                selectOptions.push({
                    name: $(this).text(),
                    value: $(this).val()
                }), orderedOptions.push($(this).text())
            }), orderedOptions.sort(), select.children("option").remove();
            for (var i in orderedOptions)
                for (var j in selectOptions)
                    if (selectOptions[j].name == orderedOptions[i]) {
                        select.append('<option value="' + selectOptions[j].value + '">' + selectOptions[j].name + "</option>");
                        break
                    }
        }

        function buildMultivalueFieldValue(select) {
            var selectedValues = select.parents(".form-wrap").find(".multivalue-select-list:first").children("li"),
                hiddenInput = select.parents(".form-wrap").children("input[type=hidden]"),
                optionValues = [];
            selectedValues.each(function() {
                optionValues.push($(this).find(".value").html())
            }), hiddenInput.val(optionValues.join("|"))
        }

        function resetGallery(gallery) {
            gallery.find(".detail-view.active").slideUp(400, function() {
                addRemoveActive(gallery.find('> li a[class="active"]'), !0), gallery.removeClass("opaque"), $(this).find("ul > li").hide(), $(".mobile #hotel-gallery").length > 0 && ($(".image-gallery-overlay").hide(), $("nav").css("z-index", "4"), $("header").css("z-index", "3"))
            })
        }

        function currencyConverter() {
            $this.find('[name="fromCurrencyTextField"]').numeric({
                negative: !1
            }), $this.find('[name="toCurrencyTextField"]').numeric({
                negative: !1
            }), generateCurrencyDropdown(), $this.find('[name="fromCurrencyTextField"]').focus(function() {
                $this.find('[name="fromCurrencyTextField"]').attr("value") == DefaultErrorLabel + " amount" && $this.find('[name="fromCurrencyTextField"]').attr("value", "")
            }).blur(function() {
                "" == $this.find('[name="fromCurrencyTextField"]').attr("value") && $this.find('[name="fromCurrencyTextField"]').attr("value", DefaultErrorLabel + " amount")
            }), $this.find('[name="fromCurrencyTextField"]').keyup(function(event) {
                calcNewConversion()
            }), $this.find('[name="toCurrencyTextField"]').keyup(function(event) {
                calcNewConversion("rtol")
            }), $this.find('[name="fromCurrency"]').change(function() {
                calcNewConversion()
            }), $this.find('[name="toCurrency"]').change(function() {
                calcNewConversion()
            })
        }

        function eyebrowNavigation() {
            $("#select-nav-dropdown").change(function() {
                "" != $("#select-nav-dropdown").val() && (window.location = $("#select-nav-dropdown").val())
            })
        }

        function checkFieldsForValue(fields) {
            var fieldFilled = !1,
                radioBtnChecked = !1,
                errors = 0;
            if ($("#optional-information [id$='rbResidentialSecondary']").is(":checked") && (fieldFilled = !0, radioBtnChecked = !0), $("#optional-information [id$='rbBusinessSecondary']").is(":checked") && (fieldFilled = !0, radioBtnChecked = !0), fields.each(function() {
                    "" != $.trim($(this).val()) && ($(this).attr("id").indexOf("txtSecAddress2") >= 0 || $(this).attr("id").indexOf("txtSecAddress3") >= 0 || (fieldFilled = !0))
                }), fieldFilled) {
                if ($("#optional-information [id$='rbResidentialSecondary']").is(":checked") || $("#optional-information [id$='rbBusinessSecondary']").is(":checked")) $(".right-rail.my-profile-right .error_messages .error_message.radioBtnErrMsg").remove();
                else {
                    var secAddHasVal = !1;
                    if ($("#optional-information .sub-block.home-address-block:first input[type=text], #optional-information .sub-block.home-address-block:first select[id$='ddlSecCountry'], #optional-information .sub-block.home-address-block:first select[id$='ddlSecState']").each(function() {
                            null != $(this).val() && "" != $(this).val() && (secAddHasVal = !0)
                        }), secAddHasVal) {
                        var radioBtnMsg = LookupError("SecAddRadioBtn", "Type of address must be selected");
                        DisplayError($("#optional-information [id$='rbResidentialSecondary']"), radioBtnMsg), errors += 1
                    }
                }
                "" != $("#optional-information select[id$='ddlSecState']").val() && $(".right-rail.my-profile-right .error_messages [id$='ddlSecState'].error_message").remove(), fields.each(function() {
                    if ("" == $.trim($(this).val())) {
                        var msgErr = GetValErrMsg($(this).attr("id"));
                        $(this).attr("id").indexOf("txtSecAddress2") >= 0 || $(this).attr("id").indexOf("txtSecAddress3") >= 0 || (DisplayError($(this), msgErr), errors += 1)
                    }
                })
            }
            return errors
        }

        function GetValErrMsg(elem) {
            var message = LookupError("SecAddDefault", "Please fill in required fields");
            return elem.indexOf("phone") >= 0 && (message = LookupError("PhoneFields", "All phone fields must be filled")), elem.indexOf("txtSecAddress1") >= 0 && (message = LookupError("SecAddLine", "Address line must be filled")), elem.indexOf("ddlSecCountry") >= 0 && (message = LookupError("SecAddCountry", "Country must be selected")), elem.indexOf("txtSecCity") >= 0 && (message = LookupError("SecAddCity", "City must be filled")), elem.indexOf("ddlSecState") >= 0 && (message = LookupError("SecAddState", "State must be selected")), elem.indexOf("txtSecZip") >= 0 && (message = LookupError("SecAddZip", "Postal Code must be filled")), message
        }

        function DisplayError(elem, msg) {
            if (isLoginModule || elem.parents("section:first").parents("header").length > 0 || elem.parents(".residences-contact-form") || elem.parents("section:first").find(".newsletter .nav-login.signup-module").length > 0 || elem.parents("section:first").parents(".newsletter-unsubscribe").length > 0) elem.parent(".form-wrap").length > 0 ? (elem.parent(".form-wrap").find(".error_message").remove(), elem.parent(".form-wrap").append('<div class="error_message">' + msg + "</div>"), elem.parent(".form-wrap").find(".error_message").show()) : (elem.parents(".form-wrap").find(".error_message").remove(), elem.parents(".form-wrap").append('<div class="error_message">' + msg + "</div>"), elem.parents(".form-wrap").find(".error_message").show());
            else {
                var labelId = elem.attr("id"),
                    this_section = elem.parents("section:first");
                if (0 == $("#em_" + labelId).length)
                    if ($(this_section).find(".module.my-profile").length > 0) elem.closest(".fieldset").append('<div id="em_' + labelId + '" class="error_message" style="top:' + elem.parent().position().top + 'px;">' + msg + "</div>");
                    else if (labelId.indexOf("fanclub") > 0) {
                    var mrgn = 0,
                        dispErr = !0;
                    if (labelId.indexOf("ddlTitle") > 0 || labelId.indexOf("ddlGuestTitle") > 0) {
                        mrgn = 4;
                        var otherId = labelId.replace("ddlTitle", "txtFirstName");
                        otherId = labelId.replace("ddlGuestTitle", "txtGuestFirstName"), msg = "Enter Title / First Name";
                        var otherLbl = $(this_section).find(".error_messages").find("#em_" + otherId);
                        otherLbl.length > 0 && (dispErr = !1)
                    }
                    if (labelId.indexOf("txtFirstName") > 0 || labelId.indexOf("txtGuestFirstName") > 0) {
                        var otherId = labelId.replace("txtFirstName", "ddlTitle");
                        otherId = labelId.replace("txtGuestFirstName", "ddlGuestTitle"), msg = "Enter Title / First Name";
                        var otherLbl = $(this_section).find(".error_messages").find("#em_" + otherId);
                        otherLbl.length > 0 && (dispErr = !1)
                    }
                    dispErr && $(this_section).find(".error_messages").append('<div id="em_' + labelId + '" class="error_message" style="top:' + (elem.parent().position().top + mrgn) + 'px;"><ul><li> ' + msg + "</li></ul></div>")
                } else if (elem.attr("id").indexOf("txtSecZip") >= 0) {
                    var spacing = elem.parent().position().top + 30;
                    $(this_section).find(".error_messages").append('<div id="em_' + labelId + '" class="error_message" style="top:' + spacing + 'px;"><ul><li> ' + msg + "</li></ul></div>")
                } else if (elem.attr("id").indexOf("rbResidentialSecondary") >= 0) $(this_section).find(".error_messages").append('<div id="em_' + labelId + '" class="error_message radioBtnErrMsg" style="top:' + elem.parent().position().top + 'px;"><ul><li> ' + msg + "</li></ul></div>");
                else if (elem.attr("id").indexOf("brpSubmission") >= 0 && elem.attr("id").indexOf("ddlTitle") >= 0) {
                    var spacing = elem.parent().position().top - 20;
                    $(this_section).find(".error_messages").append('<div id="em_' + labelId + '" class="error_message" style="top:' + spacing + 'px;"><ul><li> ' + msg + "</li></ul></div>")
                } else $(this_section).find(".error_messages").append('<div id="em_' + labelId + '" class="error_message" style="top:' + elem.parent().position().top + 'px;"><ul><li> ' + msg + "</li></ul></div>")
            }
        }

        function VerifyPasswords(p1, p2) {
            var reqField = p2.next(".req-field");
            if (p1.val() != p2.val()) {
                var msg = LookupError("FailedPasswordConfirmation", "Passwords must match exactly!");
                p2.parents("section");
                reqField && (reqField.addClass("error"), reqField.show()), DisplayError(p2, msg)
            } else {
                (isLoginModule || p2.parents("section:first").parents("header").length > 0) && p2.parent(".form-wrap").find(".error_message").remove(), reqField.removeClass("error");
                var labelId = p2.attr("id");
                $("#em_" + labelId).remove()
            }
        }

        function checkActiveNavigation($link) {
            var linkHref = $link.attr("href"),
                currentPath = window.location.pathname;
            currentPath == linkHref ? $link.addClass("active") : "/fanclub/default.aspx" != linkHref && "/fanclub/" != linkHref && "/fanclub" != linkHref || "/fanclub/" != currentPath && "/fanclub/fan-club-log-in/" != currentPath || $link.addClass("active")
        }

        function setHeightCheckAvailabilityOverlay() {
            $(".carousel-check-availability-wrap").length > 0 && $(".carousel-check-availability").length > 0 && $(".carousel-check-availability-wrap").height($(".carousel-check-availability").outerHeight())
        }

        function closeLanguagePopup() {
            $(".language-popup").hide(), $(".language-popup-lightbox-overlay").hide()
        }

        function daydiff(first, second) {
            return (second - first) / 864e5
        }
        var $this = $(this),
            home_speed = 2e3,
            home_timeout = 4e3,
            page_wrap = $("#page-wrap"),
            has_homecarousel = !1;
        if ($(window).resize(function(e) {
                has_homecarousel && adjustHomeCarousel(page_wrap)
            }), $.inArray(requestor_cc, ["CN"]) > -1 && $(".share-this-wrap,.addthis-toolbox").remove(), $(".mobile .home_menu > h1").length > 0 && $(".mobile .home_menu > h1").html($(".mobile .home_menu > h1").html().replace(", ", ", <br />")), -1 != $.inArray($("html").attr("lang"), ["en", "zh"]) && ($("#footer-wrap .unionpay-footer + li").addClass("first"), $.inArray(requestor_cc, ["CN", "HK", "MO"]) > -1 && ($("body").hasClass("home-page") ? ($("#footer-wrap .unionpay-banner").show(), $("#footer-wrap .unionpay-footer").hide(), $("#footer-wrap .unionpay-banner").appendTo("#home-carousel-wrap")) : ($("#footer-wrap .unionpay-banner").hide(), $("#footer-wrap .unionpay-footer").addClass("first").show(), $("#footer-wrap .unionpay-footer + li").removeClass("first")))), "CN" != requestor_cc && ! function() {
                var mtiTracking = document.createElement("script");
                mtiTracking.type = "text/javascript", mtiTracking.async = "true", mtiTracking.src = ("https:" == document.location.protocol ? "https:" : "http:") + "//fast.fonts.com/t/trackingCode.js", (document.getElementsByTagName("head")[0] || document.getElementsByTagName("body")[0]).appendChild(mtiTracking)
            }(), "CN" == requestor_cc) {
            var currLang = $("html").attr("lang") || "";
            "zh" == currLang && "undefined" != typeof globalLang && (currLang = globalLang.replace("zh-", "")), "en" == currLang && (currLang = ""), "" != currLang && (currLang += "/");
            var pubUrl = ("/" + currLang + $("#logo a").attr("href").replace("/default.aspx", "")).replace("//", "/"),
                hotelResults = LookupHotelInformation({
                    startsWith: {
                        hotel_pubUrl: pubUrl
                    }
                }),
                hotelAddress = "";
            hotelResults.length > 0 && (hotelAddress = hotelResults[0].hotel_address.replace("<br />", ""), $('a[href*="hotel/hotel-directions"]').each(function(i) {
                $(this).attr("href", "http://www.google.cn/maps/place/" + hotelAddress), $(this).attr("target", "_blank")
            }))
        }
        var mouseInGallery = !1,
            mouseInImageZoom = !1;
        $(".image-gallery").hover(function() {
            mouseInGallery = !0
        }, function() {
            mouseInGallery = !1
        }), $("body").unbind("mouseup"), $("body").bind("mouseup", function() {
            0 == mouseInGallery && 0 == mouseInImageZoom && resetGallery($(".image-gallery"))
        }), $this.find("#info-list > li.more-action").on("mouseover", function(e) {
            setNavigationItemHover($(this), !1, e)
        });
        var globalListMoreActionItems = "";
        globalListMoreActionItems = "https:" != window.location.protocol ? $this.find("#global-list > li.more-action").not(".newsletter") : $this.find("#global-list > li.more-action"), globalListMoreActionItems.each(function(index) {
            var $item = $(this),
                checkStatus = !1;
            document.URL.indexOf("/fanclub/") >= 0 && (0 === index || 1 === index) && (checkStatus = !0), $item.on("mouseover", function(e) {
                setNavigationItemHover($(this), checkStatus, e)
            }), $item.find("*").on("click", function(e) {
                setNavigationItemHover($(this).parents(".more-action"), checkStatus, e)
            })
        }), $this.find("#info-list > li.more-action *").on("click", function(e) {
            setNavigationItemHover($(this).parents(".more-action"), !1, e)
        }), $this.find("#header-lists ul > li.more-action input").on("focus", function(e) {
            clearTimeout(hoverTimeouts[$(this).parents(".more-action").uniqueId()[0].id])
        }), $this.find("#header-lists ul > li.more-action").on("mouseout", function(e) {
            var me = $(this);
            me.hasClass("touched") || (hoverTimeouts[me.uniqueId()[0].id] = setTimeout(function() {
                me.removeClass("hover")
            }, 750))
        }), $this.find("#header-lists ul > li.more-action").on("touchend", function(e) {
            $(event.target).is("a") && !$(event.target).parents().is("section") && (e.preventDefault(), $(this).hasClass("touched") ? $(this).removeClass("hover touched") : $(this).addClass("hover touched"))
        }), $this.find("#header-lists ul > li.more-action").on("pointerup MSPointerUp", function(e) {
            $(this).hasClass("touched") ? $(this).removeClass("hover touched") : $(this).addClass("hover touched")
        }), $this.find(".book-restaurant").each(function() {
            $(this).attr("target", "_blank")
        }), $this.find(".book-restaurant-reservation").each(function() {
            $(this).attr("target", "_blank")
        }), $(".gotoMobileSite").length > 0 && ($(".gotoMobileSite").click(function(event) {
            event.preventDefault();
            var domain = window.location.host;
            domain.indexOf("siteworx.com") >= 0 && (domain = "siteworx.com"), domain.indexOf("m.") >= 0 && (domain = domain.replace("m.", "")), document.cookie = "view_desktop=; expires=Thu, 01 Jan 1970 00:00:00 GMT; Domain=" + domain + "; path=/", window.location.href.indexOf("siteworx") > 0 ? window.location.href.indexOf("-") > 0 ? setTimeout(function() {
                window.location = window.location.href.replace("-", "-m-")
            }, 500) : setTimeout(function() {
                window.location = window.location.href.replace(".siteworx", "-m.siteworx")
            }, 500) : setTimeout(function() {
                -1 != window.location.search.indexOf("?") ? window.location.search += "&reload=true" : window.location.search = "?reload=true"
            }, 500)
        }), -1 == document.cookie.indexOf("view_desktop") && $(".gotoMobileSite").hide()), $("#delight-accordion").length > 0 && $("#delight-accordion").hrzAccordion({
            fixedWidth: 556,
            handleClass: "heading",
            handlePosition: "left",
            contentInnerWrapper: "accord-inner-wrapper",
            listItemSelected: "list-item-selected",
            openOnLoad: !1,
            numPanels: $("#delight-accordion li").length,
            containerWidth: $("#delight-accordion").width()
        }), $(".modalImg").click(function(e) {
            e.preventDefault();
            var source = $(".modal img").attr("src"),
                winHeight = $(window).height() - 200; - 1 == source.indexOf("hei") && (source += "&hei=" + winHeight, source = source.replace("$DetailSidebarLandscapeHeight$&", ""), $(".modal img").attr("src", source)), $(window).scrollTop(0), $(".modal").show()
        }), $(window).resize(function() {
            if ($(".modal img").length > 0) {
                var imgSrc = $(".modal img").attr("src"),
                    imgPresetStart = imgSrc.indexOf("&hei");
                imgSrc = imgSrc.substring(0, imgPresetStart);
                var winHeight = $(window).height() - 200;
                $(".modal img").attr("src", imgSrc + "&hei=" + winHeight)
            }
        }), $(".modal .close").click(function() {
            $(".modal").hide()
        }), $(".modal .print").click(function(event) {
            event.preventDefault();
            var container = $(this).attr("rel");
            return $("." + container).printArea(), !1
        }), $(window).scroll(function() {
            "none" != $(".modal").css("display") && $(".modal").css("top", $(window).scrollTop() + "px")
        }), $this.delegate(".right-rail .module.relatedImg li a", "click", function(ev) {
            ev.preventDefault(), openImageZoom($($(this).find("img")[0]).attr("src"))
        }), $(".celebrity-gallery").length > 0 ? $this.find(".detail-cycle").each(function() {
            var detailCycle = $(this),
                numCaptions = 0;
            detailCycle.find("img").each(function() {
                "undefined" != typeof $(this).attr("alt") && "" !== $(this).attr("alt") && numCaptions++
            }), numCaptions > 0 ? (detailCycle.siblings(".pagerBar").show(), 1 == detailCycle.children("img").length && detailCycle.siblings(".pagerBar").find(".pager").hide()) : detailCycle.parent(".sub-page-carousel").hover(function() {
                $(this).children(".pagerBar").fadeIn()
            }, function() {
                $(this).children(".pagerBar").fadeOut()
            }), $this.find(".detail-cycle figure").length > 0 ? detailCycle.cycle({
                log: !1,
                fx: "fade",
                speed: home_speed,
                timeout: home_timeout,
                pager: detailCycle.siblings(".pagerBar").children(".pager"),
                slides: "figure"
            }).on("cycle-before", function() {
                showCelebrityCaption(detailCycle)
            }) : detailCycle.cycle({
                log: !1,
                fx: "fade",
                speed: home_speed,
                timeout: home_timeout,
                pager: detailCycle.siblings(".pagerBar").children(".pager"),
                slides: "img"
            }).on("cycle-before", function() {
                showCelebrityCaption(detailCycle)
            }), detailCycle.find("img").css("visibility", "visible"), detailCycle.siblings(".pagerBar").find(".pagerZoom").click(function() {
                $("#image-zoom").click(function() {
                    detailCycle.cycle("resume")
                }), detailCycle.cycle("pause");
                var imgSrc = detailCycle.find("img.cycle-slide-active").attr("src");
                openImageZoom(imgSrc)
            })
        }) : $this.find("#detail-cycle img").length > 0 ? $this.find("#detail-cycle").each(function() {
            var detailCycle = $(this),
                firstImage = detailCycle.find("img"),
                checkHeight = function() {
                    var z = firstImage.height();
                    if (z > 0) detailCycle.height(z), detailCycle.width(detailCycle.find("img").width()), detailCycle.closest(".sub-page-carousel").height(z);
                    else {
                        if (p++, p > 20) return;
                        setTimeout(checkHeight, 100)
                    }
                },
                p = 0;
            checkHeight();
            var numCaptions = 0;
            detailCycle.children("img, figure").each(function() {
                ("undefined" != typeof $(this).attr("alt") && "" !== $(this).attr("alt") || $(this).find("figcaption").length > 0 && "" != $.trim($(this).find("figcaption").html())) && numCaptions++
            }), numCaptions > 0 ? detailCycle.siblings("#pagerBar").show() : detailCycle.parent(".sub-page-carousel").hover(function() {
                $(this).children("#pagerBar").fadeIn()
            }, function() {
                $(this).children("#pagerBar").fadeOut()
            }), $this.find("#detail-cycle figure").length > 0 ? (detailCycle.on("cycle-post-initialize", function(event, optionHash) {
                showCaption($this.find("#detail-cycle figure").get(0))
            }), detailCycle.cycle({
                log: !1,
                fx: "fade",
                speed: 750,
                timeout: home_timeout,
                pager: "#pager",
                slides: "figure",
                swipe: !0
            }).on("cycle-before", function(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag) {
                showCaption(incomingSlideEl)
            })) : (detailCycle.on("cycle-post-initialize", function(event, optionHash) {
                showCaption($this.find("#detail-cycle figure").get(0))
            }), detailCycle.cycle({
                log: !1,
                fx: "fade",
                speed: 750,
                timeout: home_timeout,
                pager: "#pager",
                slides: "img",
                swipe: !0
            }).on("cycle-before", function(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag) {
                showCaption(incomingSlideEl)
            })), detailCycle.find("img").css("visibility", "visible"), detailCycle.siblings("#pagerBar").find("#pagerZoom").click(function() {
                $("#image-zoom").click(function() {
                    detailCycle.cycle("resume")
                }), detailCycle.cycle("pause");
                var imgSrc = detailCycle.find(".cycle-slide-active img:first").attr("src");
                openImageZoom(imgSrc)
            })
        }) : showCaption(1 == $("#detail-cycle figure").length ? $this.find("#detail-cycle figure").get(0) : $this.find("#detail-cycle img").get(0)), $this.find(".restaurants-gallery #detail-cycle").length > 0 && ($this.find("#pagerBar").hide(), $this.find(".restaurants-gallery #detail-cycle").hide(), $this.find(".restaurants-gallery #detail-cycle").cycle("pause"), $this.find(".restaurants-splash .slide").click(function() {
            $(this).hasClass("carouselToggle") && !$(this).hasClass("active") ? ($this.find(".restaurants-gallery-background").hide(), $this.find("#pagerBar").show(), $this.find(".restaurants-gallery #detail-cycle").show(), $this.find(".restaurants-gallery #detail-cycle").cycle("resume")) : ($this.find(".restaurants-gallery-background").show(), $this.find("#pagerBar").hide(), $this.find(".restaurants-gallery #detail-cycle").hide(), $this.find(".restaurants-gallery #detail-cycle").cycle("pause"))
        })), !isMobile.any() && $this.find("#pickersFields").length > 0 && $this.find("#pickersFields").show(), setupImageZoom(), $(".details-buttons .button-clear").length > 0 && $(".details-buttons,.sub-content .module-body").swipe({
            swipe: function(event, direction, distance, duration, fingerCount) {
                $(".details-buttons .button-clear.right").length > 0 && "left" == direction && (document.location.href = $(".details-buttons .button-clear.right").get(0).href), $(".details-buttons .button-clear.left").length > 0 && "right" == direction && (document.location.href = $(".details-buttons .button-clear.left").get(0).href)
            },
            threshold: 100,
            allowPageScroll: "vertical"
        }), $(".detail-view").length > 0 && $(".detail-view > ul").swipe({
            swipe: function(event, direction, distance, duration, fingerCount) {
                "left" == direction && $(this).parent(".detail-view").find(".action-wrap .next").click(), "right" == direction && $(this).parent(".detail-view").find(".action-wrap .prev").click()
            },
            threshold: 100,
            allowPageScroll: "vertical"
        });
        var PageURL = document.URL,
            pageDateFormat = "DD, d MM yy";
        if (PageURL.indexOf("mandarinoriental.co.jp") >= 0 || PageURL.indexOf("jp.siteworx.com") >= 0 ? pageDateFormat = "yy年, MM dD" : (PageURL.indexOf("mandarinoriental.com.cn") >= 0 || PageURL.indexOf("cn.siteworx.com") >= 0 || PageURL.indexOf("mandarinoriental.com.hk") >= 0 || PageURL.indexOf("hk.siteworx.com") >= 0) && (pageDateFormat = "yy年 m月 d日"), dateCalendar(), $this.find("ul#home-carousel li").length > 0) {
            has_homecarousel = !0, 0 == $(".byline_no_border").length && adjustHomeCarousel(page_wrap), $this.find("#home-carousel-wrap").css("visibility", "visible");
            var $carousel = $this.find("#home-carousel");
            if ($carousel.find("li").length > 1) {
                var $phantom = {};
                if ($this.hasClass("mobile")) {
                    var cycle_fx = "scrollHorz",
                        cycle_speed = 750,
                        cycle_timeout = 5e3;
                    $("#pager-wrap-pos").prepend('<div class="phantom-home-carousel" />'), $phantom = $("#pager-wrap-pos .phantom-home-carousel"), $phantom.append(function() {
                        for (var l = $carousel.find("li").length, imgs = "", a = 0; l > a; a++) imgs += '<img src="'+URL_G+'public/template/images/bg/x.gif" class="" />';
                        return imgs
                    }).cycle({
                        log: !1,
                        speed: 0,
                        timeout: 0,
                        slides: ">img",
                        swipe: !0
                    }), $("html.rtl").length > 0 && $("#pager-wrap-pos .phantom-home-carousel > img").hammer().on("swipeleft", function(event) {
                        $($(".cycle-slide-active")[0]).trigger("swipeleft")
                    }), $carousel.on("cycle-post-initialize", function(event, optionHash) {
                        $("#pager-wrap").addClass("mobile-show-pager"), $("#home-carousel-wrap").height($("#home-carousel .cycle-slide-active img").height() + $("#home-carousel-wrap #pager-caption").height() + 32)
                    })
                } else {
                    var cycle_fx = "scrollHorz",
                        cycle_speed = 750,
                        cycle_timeout = home_timeout;
                    $("body").hasClass("mobile") || 2 == window.location.pathname.split("/").length || -1 == window.location.pathname.indexOf("/residences/") && $("#pager-caption").hide()
                }
                if ($carousel.on("cycle-bootstrap", function(event, opts) {
                        $("body").hasClass("mobile") && $this.find(".slide-caption br").remove();
                        var $li = $(this).find("li").not(".cycle-sentinel").eq(0);
                        showHomeCaption($li)
                    }).cycle({
                        log: !1,
                        fx: cycle_fx,
                        speed: cycle_speed,
                        timeout: cycle_timeout,
                        pager: "#pager",
                        pagerTemplate: "<a>&bull;</a>",
                        pagerActiveClass: "activeSlide",
                        slides: "li",
                        swipe: !0
                    }).on("cycle-before", function(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag) {
                        $("body").hasClass("mobile") ? showHomeCaption($(incomingSlideEl)) : 2 == window.location.pathname.split("/").length || $("body").hasClass("residences") ? showHomeCaption($(incomingSlideEl)) : $("#pager-caption").hide()
                    }), $this.hasClass("mobile")) {
                    var triggered = !1,
                        $slidewrappers = $(".phantom-home-carousel,#home-carousel");
                    $("#home-carousel,.phantom-home-carousel").on("cycle-before", function(e, h, outslide, inslide, ff) {
                        if (!triggered) {
                            if (triggered = !0, $(this).hasClass("phantom-home-carousel")) {
                                var dir = ff ? "next" : "prev";
                                $carousel.cycle(dir)
                            } else {
                                var i = $(inslide).index() - 1;
                                $(".phantom-home-carousel").cycle("goto", i)
                            }
                            triggered = !1
                        }
                    })
                }
            } else {
                var $li = $carousel.find("li").not(".cycle-sentinel").eq(0);
                $("body").hasClass("mobile") && $this.find(".slide-caption br").remove(), showHomeCaption($li)
            }
        }
        if ($this.find("ul.gallery-filters li").length > 0 && $this.find("ul.gallery-filters > li > input").change(function(e) {
                "*" == $(this).val() ? $("ul.image-gallery > li:not(.detail-view").show() : ($("ul.image-gallery > li:not(.detail-view").show(), $("ul.image-gallery > li:not(." + $(this).val() + ")").hide()), $(".image-gallery-loader").hide()
            }), $this.find(".hotel-gallery-selector select").change(function() {
                galleryLoading(), window.location.search = "?loc=" + $(this).val()
            }), $this.find("ul.image-gallery li").length > 1) {
            var numPerRow = 3;
            $this.find("ul.image-gallery.mobile").length > 0 && (numPerRow = 2, $("html").hasClass("rtl") ? $("ul.image-gallery > li:not(.detail-view):odd").css("margin-left", "0") : $("ul.image-gallery > li:not(.detail-view):odd").css("margin-right", "0")), $(document).bind("keyup", function(e) {
                galleryKeyboardNav(e)
            }), $this.find("ul.image-gallery img,ul.image-gallery span,.gallery-navigation").click(function() {
                $(".image-gallery").addClass("has-keyboard-focus")
            }), $this.find("header *,footer *").click(function() {
                $this.find(".image-gallery").removeClass("has-keyboard-focus").unbind("focus").children("img").unbind("focus")
            }), $this.find("input,textarea,button,#side-contents a,header a,footer a").focus(function() {
                $this.find(".image-gallery").removeClass("has-keyboard-focus").unbind("focus").children("img").unbind("focus")
            }), $this.find("ul.gallery-navigation>li>a").each(function(i) {
                $(this).attr("rel", i + 1)
            }), $this.find("ul.gallery-navigation>li>a").click(function(e) {
                e.preventDefault, $this.find(".image-gallery li a[rel=" + $(this).attr("rel") + "]").click().focus()
            });
            var lightboxStyleGalleryCarousel = null,
                overlayTimeout = null;
            $(".lightbox-type-gallery #hotel-gallery-overlay-items ul").length > 0 && (lightboxStyleGalleryCarousel = $(".lightbox-type-gallery #hotel-gallery-overlay-items ul")), $this.find("ul.image-gallery > li > a").click(function(e) {
                if (null != lightboxStyleGalleryCarousel)
                    if ($(this).hasClass("isVideo")) e.preventDefault();
                    else {
                        $(".image-gallery-overlay").show(), $("nav, header").css("z-index", "0"), $("#hotel-gallery-overlay-items").show();
                        var relIndex = $(this).attr("rel");
                        lightboxStyleGalleryCarousel.cycle("goto", parseInt(relIndex) - 1), overlayTimeout = setTimeout(function() {
                            hideLightboxOverlay()
                        }, 3e3), setScale()
                    }
                else if (currentGalleryDetail = $(this), e.preventDefault(), currentGalleryDetail.hasClass("isVideo"));
                else {
                    $("ul.image-gallery > li > a").removeClass("active");
                    var relIndex = $(this).attr("rel"),
                        detailedView = $this.find("ul.image-gallery li.detail-view-" + Math.ceil(relIndex / numPerRow));
                    hideShowDetailed($this.find("ul.image-gallery"), detailedView, relIndex, null, numPerRow)
                }
                return !1
            });
            var isSwipeEvent = !1,
                overlayIsVisible = !0,
                overlayTimeoutTime = 2e3;
            null != lightboxStyleGalleryCarousel && ($(window).on("orientationchange", function(event) {
                setScale()
            }), setScale(), lightboxStyleGalleryCarousel.cycle({
                log: !1,
                fx: "scrollHorz",
                speed: 750,
                paused: !0,
                timeout: 750,
                next: ".cycle-next",
                prev: ".cycle-prev",
                slides: "> li"
            }).on("cycle-after", function(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag) {
                !isSwipeEvent || overlayIsVisible ? (clearTimeout(overlayTimeout), overlayTimeout = setTimeout(function() {
                    hideLightboxOverlay()
                }, overlayTimeoutTime)) : clearTimeout(overlayTimeout), isSwipeEvent = !1, lightboxStyleGalleryCarousel.find(".img-title").fadeIn("fast"), overlayIsVisible && $(".wrap-cycle-prev, .wrap-cycle-next").show(), lightboxStyleGalleryCarousel.find(".detail-wrap").show(), lightboxStyleGalleryCarousel.find(".close").show()
            }).on("cycle-before", function(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag) {
                $(incomingSlideEl).find(".isVideo").length > 0 ? lightboxStyleGalleryCarouselClose() : (lightboxStyleGalleryCarousel.find(".img-title").fadeOut("fast"), $(".wrap-cycle-prev, .wrap-cycle-next").hide(), lightboxStyleGalleryCarousel.find(".detail-wrap").hide(), lightboxStyleGalleryCarousel.find(".close").hide())
            }), lightboxStyleGalleryCarousel.find(".share-this-wrap .share").on("click", function(event) {
                event.stopPropagation(), clearTimeout(overlayTimeout)
            }), $(lightboxStyleGalleryCarousel).swipe({
                tap: function(event, target) {
                    showHideLightboxOverlay(target)
                },
                doubleTap: function(event, target) {
                    showHideLightboxOverlay(target)
                },
                longTap: function(event, target) {
                    showHideLightboxOverlay(target)
                },
                swipe: function(event, direction, distance, duration, fingerCount) {
                    "left" == direction && (isSwipeEvent = !0, lightboxStyleGalleryCarousel.cycle("next")), "right" == direction && (isSwipeEvent = !0, lightboxStyleGalleryCarousel.cycle("prev"))
                },
                threshold: 100
            }), lightboxStyleGalleryCarousel.find(".close").click(function(event) {
                event.stopImmediatePropagation(), lightboxStyleGalleryCarouselClose()
            }), navigator.userAgent.match(/(iPhone|iPod|iPad)/i) && ! function(a) {
                function m() {
                    d.setAttribute("content", g), h = !0
                }

                function n() {
                    d.setAttribute("content", f), h = !1
                }

                function o(b) {
                    l = b.accelerationIncludingGravity, i = Math.abs(l.x), j = Math.abs(l.y), k = Math.abs(l.z), a.orientation && 180 !== a.orientation || !(i > 7 || (k > 6 && 8 > j || 8 > k && j > 6) && i > 5) ? h || m() : h && n()
                }
                var b = navigator.userAgent;
                if (/iPhone|iPad|iPod/.test(navigator.platform) && /OS [1-5]_[0-9_]* like Mac OS X/i.test(b) && b.indexOf("AppleWebKit") > -1) {
                    var c = a.document;
                    if (c.querySelector) {
                        var i, j, k, l, d = c.querySelector("meta[name=viewport]"),
                            e = d && d.getAttribute("content"),
                            f = e + ",maximum-scale=1",
                            g = e + ",maximum-scale=10",
                            h = !0;
                        d && (a.addEventListener("orientationchange", m, !1), a.addEventListener("devicemotion", o, !1))
                    }
                }
            }(this)), null != currentGalleryDetail && (galleryInstantAnimate = !0, currentGalleryDetail.click()), $this.find("ul.image-gallery > li > a").hover(function(e) {
                $(this).addClass("hover")
            }, function(e) {
                $(this).removeClass("hover")
            }), $("ul.image-gallery .detail-view > .action-wrap a, .mobile .image-gallery-content #hotel-gallery.image-gallery .action-wrap a").click(function(e) {
                e.preventDefault();
                var newIndex, activeDetailItemCount = $this.find("ul.image-gallery>li.detail-view>ul>li").length,
                    detailedView = $(this).closest(".detail-view"),
                    relIndex = parseInt(detailedView.find("ul > li:visible").attr("rel")),
                    gallery = $this.find("ul.image-gallery");
                return newIndex = $(this).hasClass("prev") ? relIndex - 1 : relIndex + 1, 1 > newIndex || newIndex > activeDetailItemCount || $(this).hasClass("close") ? (resetGallery($(this).parents(".image-gallery")), !1) : (currentGalleryDetail = $this.find(".image-gallery li a[rel=" + newIndex + "]"), currentGalleryDetail.hasClass("isVideo") ? resetGallery($(this).parents(".image-gallery")) : galleryGoTo(gallery, detailedView, relIndex, newIndex, numPerRow), !1)
            }), $this.find("ul.image-gallery .detail-view .img-view-wrap").hover(function(e) {
                $(this).closest("div").addClass("hover")
            }, function(e) {
                $(this).closest("div").removeClass("hover")
            })
        }
        $this.find(".image-gallery").length > 0 && "" != window.location.hash && ("english" == s.prop2 ? $(".thumb-" + window.location.hash.replace("#", "")).click() : "spanish" == s.prop2 ? $(".thumb-" + window.location.hash.replace("#17", "328")).click() : "german" == s.prop2 ? $(".thumb-" + window.location.hash.replace("#17", "334")).click() : "french" == s.prop2 ? $(".thumb-" + window.location.hash.replace("#17", "330")).click() : "traditional chinese" == s.prop2 ? $(".thumb-" + window.location.hash.replace("#17", "332")).click() : "simplified chinese" == s.prop2 ? $(".thumb-" + window.location.hash.replace("#17", "331")).click() : "japanese" == s.prop2 && $(".thumb-" + window.location.hash.replace("#17", "329")).click()), $this.find("ul#rooms-suites-list li").length > 2 && adjustRoomCards($this.find("ul#rooms-suites-list")), $("body.mobile").length < 1 && $this.find(".fieldset select, .callout-inner select, select, .fieldset input[type=radio], .fieldset input[type=checkbox], .nav-login.signup-module input").uniform(), $this.find(".multivalue-select").each(function() {
            var multivalList = null;
            if (0 == $(this).parents(".form-wrap").children(".multivalue-select-list").length && $(this).parents(".form-wrap").append('<ul class="multivalue-select-list"></ul>'), multivalList = $(this).parents(".form-wrap").children(".multivalue-select-list"), $(this).parents(".form-wrap").children("input[type=hidden]").length > 0) {
                var selectedHotelsString = $(this).parents(".form-wrap").children("input[type=hidden]").val(),
                    selectedHotels = selectedHotelsString.split("|");
                for (var i in selectedHotels)
                    if ("" != selectedHotels[i]) {
                        var selectedHotelInDropdown = $(this).children("option[value='" + selectedHotels[i] + "']");
                        selectedHotelInDropdown && (addMultivalueSelection($(this), multivalList, selectedHotelInDropdown), buildMultivalueFieldValue($(this)))
                    }
            }
            $(this).change(function() {
                var hotelSelect = $(this),
                    selectedHotel = hotelSelect.children("option:selected:first");
                "" != selectedHotel.val() && (addMultivalueSelection(hotelSelect, multivalList, selectedHotel), buildMultivalueFieldValue(hotelSelect), hotelSelect.children("option:first").prop("selected", !0), hotelSelect.siblings("span").html(" -- Select -- "))
            })
        }), $this.find(".nav-login .email").blur(function(e) {
            var me = $(this);
            me.val().length > 0 ? emailRegex.test(me.val()) ? me.parent().find(".error_message").hide() : me.parent().find(".error_message").show() : me.parent().find(".error_message").hide()
        }), $this.find("#footer a.sitemap").attr("title", $this.find("#footer a.sitemap span").text()).bind("click.togglesitemap", function(e) {
            e.preventDefault();
            var $sitemap = $("#footer nav.sitemap"),
                $thislink = $(this),
                $thistitle = $thislink.find("span"),
                ShowSiteMapLabel = ($thislink.attr("title"), LookupError("ctFooter_ShowLocalSiteMap", "Show Sitemap")),
                HideSiteMapLabel = LookupError("ctFooter_HideLocalSiteMap", "Hide Sitemap"),
                fnGetTitle = function() {
                    return $sitemap.is(":hidden") ? ShowSiteMapLabel : HideSiteMapLabel
                };
            $sitemap.length > 0 ? $(this).hasClass("init") ? ($sitemap.slideToggle(800, function() {
                $thistitle.text(fnGetTitle), $thislink.toggleClass("active")
            }), $("html, body").animate({
                scrollTop: $sitemap.offset().top
            }, 800)) : ($thistitle.text(fnGetTitle), $thislink.addClass("init")) : $thislink.hide()
        }).trigger("click.togglesitemap"), $this.find("a.specialcodes").attr("title", $this.find("a.specialcodes span").text()).bind("click.togglespecialcodes", function(e) {
            e.preventDefault();
            var $specialcodes = $("#codes"),
                $thislink = $(this);
            $thislink.find("span"), $thislink.attr("title");
            $specialcodes.length > 0 ? $(this).hasClass("init") ? $specialcodes.slideToggle(800, function() {
                $thislink.toggleClass("active")
            }) : $thislink.addClass("init") : $thislink.hide()
        }).trigger("click.togglespecialcodes"), $this.find("#search a.slide").bind("mouseover.togglesearch", function(e) {
            e.preventDefault();
            var $search = $("#search #slide-search"),
                $thislink = $(this);
            if ($search.length > 0) {
                if (!$(this).hasClass("open")) {
                    var setRight = parseInt($("#header").width()) - parseInt($thislink.position().left) - 2;
                    $search.css("right", setRight + "px"), $search.animate({
                        width: "toggle"
                    }), $("#search").addClass("active"), $thislink.addClass("open"), $("input.focus").focus()
                }
            } else $thislink.hide()
        }), $this.find("#search a.slide").bind("click.togglesearch", function(e) {
            e.preventDefault();
            var $search = $("#search #slide-search"),
                $thislink = $(this);
            $search.length > 0 ? $(this).hasClass("open") && ($search.animate({
                width: "toggle"
            }), $("#search").removeClass("active"), $thislink.removeClass("open"), $thislink.blur()) : $thislink.hide()
        }), $this.find(".img-view-wrap .close").click(function() {
            resetGallery($(this).parents(".image-gallery"))
        }), $(".right-two-col-container").each(function() {
            0 == $(this).children().length && $(this).remove()
        }), $threeCol = $this.find("section:has(.three-col)").not(".residences section.section-row, .homepage-module section:has(.three-col)");
        for (var sectionCount = 0, insideRightTwoColCont = !1, i = 0; i <= $threeCol.length - 1; i++) $threeCol.eq(i).parent(".right-two-col-container").length > 0 ? insideRightTwoColCont = !0 : (1 == insideRightTwoColCont && (insideRightTwoColCont = !1, sectionCount += 2), sectionCount % 3 == 0 ? $threeCol.eq(i).find(".module.three-col").addClass("clear-section") : sectionCount % 3 == 2 && $threeCol.eq(i).find(".module.three-col").addClass("right"), sectionCount++);
        $(".homepage-module").each(function() {
            $threeColHomepage = $(this).find("section:has(.three-col)");
            for (var sectionCountHomepage = 0, insideRightTwoColContHomepage = !1, i = 0; i <= $threeColHomepage.length - 1; i++) $threeColHomepage.eq(i).parent(".right-two-col-container").length > 0 ? insideRightTwoColContHomepage = !0 : (1 == insideRightTwoColContHomepage && (insideRightTwoColContHomepage = !1, sectionCountHomepage += 2), sectionCountHomepage % 3 == 0 ? $threeColHomepage.eq(i).find(".module.three-col").addClass("clear-section") : sectionCountHomepage % 3 == 2 && $threeColHomepage.eq(i).find(".module.three-col").addClass("right"), sectionCountHomepage++)
        }), $threeColInContainer = $this.find(".right-two-col-container section:has(.three-col)");
        for (var i = 1; i <= $threeColInContainer.length - 1; i += 2) $threeColInContainer.eq(i).find(".module.three-col").addClass("right");
        $this.find(".module.three-col").each(function() {
            var adjacentRightTwoCol = $(this).parent().next();
            adjacentRightTwoCol.length > 0 && adjacentRightTwoCol.hasClass("right-two-col-container") && ($(this).removeClass("module"), $(this).addClass("module-tall"))
        }), $this.normalizeHeight(".module", ".clear .single-col .right-rail"), $this.find(".module-tall").each(function() {
            var adjacentRightTwoCol = $(this).parent().next();
            if (adjacentRightTwoCol.length > 0 && adjacentRightTwoCol.hasClass("right-two-col-container")) {
                var thisHeight = $(this).outerHeight(),
                    rightTwoColHeight = adjacentRightTwoCol.outerHeight(),
                    marginBottom = parseInt($(this).css("margin-bottom").replace("px", ""));
                if (rightTwoColHeight > thisHeight) $(this).height(rightTwoColHeight - marginBottom);
                else if (thisHeight > rightTwoColHeight) {
                    var firstChild = adjacentRightTwoCol.find(".module.three-col:first"),
                        colGroup = "";
                    firstChild.length > 0 && $(firstChild.attr("class").split(" ")).each(function() {
                        this.match(/js-colgroup-/) && (colGroup = this)
                    }), $("." + colGroup).css("min-height", $("." + colGroup).height() + (thisHeight - rightTwoColHeight) + marginBottom)
                }
            }
        }), $this.find(".restaurants-gallery").initGallery(), $this.find(".restaurants-splash").initSlidingMenu(), $this.find(".restaurants-splash .slide").first().click(), $this.find(".contact-us").initAccordion(), $this.find(".contact-us .contact-filters").initFilters(), $this.find("#main").supersleight({
            shim: "static/images/bg/x.gif",
            apply_positioning: !0
        }), $this.find("header").children().each(function() {
            $(this).supersleight({
                shim: "static/images/bg/x.gif",
                apply_positioning: !0
            })
        });
        var DefaultErrorLabel = LookupError("defaultErrorMessageLabel", "Enter");
        $this.find(".cconverter").length > 0 && currencyConverter(), $this.find(".select-nav").length > 0 && eyebrowNavigation(), $(".share-this-wrap .share").on("click", function(e) {
            e.preventDefault(), $(".share-this-wrap.active").removeClass("active");
            var $d = $("#page-wrap, #page-wrap div, .share-this-wrap .share-this a");
            $d.unbind("click"), $(this).closest(".share-this-wrap").find(".share-this").fadeIn(500, function() {
                $(this).closest(".share-this-wrap").addClass("active"), $d.on("click.shareThisCloser", function(e) {
                    e.preventDefault(), e.stopPropagation(), $(this).hasClass("share-this") || ($d.off("click.shareThisCloser"), $(".share-this").fadeOut(500, function() {
                        $(".share-this-wrap.active").removeClass("active")
                    }))
                })
            })
        }), $this.find(".modal-link").click(function() {
            var matchingWindow = $(this).siblings(".modal-window");
            if ($("html").hasClass("oldie") && matchingWindow.addClass($("html").attr("class").replace(/.*\b(ie\d).*/, "$1")), matchingWindow.is(":visible")) matchingWindow.hide();
            else {
                var modalLeft = $(this).position().left - matchingWindow.outerWidth(),
                    modalTop = $(this).position().top;
                matchingWindow.css("left", modalLeft + "px").css("top", modalTop + "px").show()
            }
        }), $this.find(".modal-window-close").click(function() {
            $(this).parent(".modal-window").hide()
        }), $this.find(".relatedContent").each(function() {
            $(this).parents(".module").addClass("relatedImg")
        }), $this.find("a.expand").click(function() {
            $(this).hasClass("open") ? $(this).removeClass("open") : $(this).addClass("open"), $(this).siblings(".expand_content").slideToggle()
        }), $this.find(".my-profile .profile-block-views .radio").click(function() {
            currentOpenProfileBlock = $(this).attr("id"), $(".error_message").remove();
            var inputId = $(this).find("input").attr("id").replace("bv-", "");
            "all" == inputId ? profileInstantAnimate ? $(".my-profile .block .wrap").show() : $(".my-profile .block .wrap").slideDown() : $(".my-profile .block").each(function() {
                $(this).attr("id") == inputId ? profileInstantAnimate ? $(this).find(".wrap").show() : $(this).find(".wrap").slideDown() : profileInstantAnimate ? $(this).find(".wrap").hide() : $(this).find(".wrap").slideUp()
            }), profileInstantAnimate = !1
        }), $this.find(".my-profile .block .header").hover(function() {
            $(this).find("h3").addClass("hover")
        }, function() {
            $(this).find("h3").removeClass("hover")
        }), $this.find(".my-profile .block .header").click(function() {
            inputId = $(this).parent().attr("id"), currentOpenProfileBlock = "uniform-bv-" + inputId, $(".error_message").remove(), $(".my-profile .profile-block-views .radio span.checked").removeClass("checked"), $("#uniform-bv-" + inputId + " span").addClass("checked"), $(".my-profile .block").each(function() {
                $(this).attr("id") == inputId ? profileInstantAnimate ? $(this).find(".wrap").show() : $(this).find(".wrap").slideDown() : profileInstantAnimate ? $(this).find(".wrap").hide() : $(this).find(".wrap").slideUp()
            })
        }), $this.find(".my-profile .block .all-regions").click(function() {
            var checkboxInput = null;
            "" != $(this).attr("type") ? checkboxInput = $(this).find("input[type='checkbox']:first") : $(this).find("input[type='checkbox']").length > 0 && (checkboxInput = $(this)), checkboxInput.is(":checked") ? checkboxInput.parents(".checkboxes:first").find("input[type='checkbox']").each(function() {
                $(this).attr("id") != checkboxInput.attr("id") && ($(this).prop("checked", !0), $(this).parent("span").addClass("checked"))
            }) : checkboxInput.parents(".checkboxes:first").find("input[type='checkbox']").each(function() {
                $(this).attr("id") != checkboxInput.attr("id") && ($(this).prop("checked", !1), $(this).parent("span").removeClass("checked"))
            })
        }), null != currentOpenProfileBlock ? (profileInstantAnimate = !0, $(".my-profile .profile-block-views .radio span.checked").removeClass("checked"), $("#" + currentOpenProfileBlock + " span").addClass("checked"), $("#" + currentOpenProfileBlock).click()) : $this.find(".my-profile .profile-block-views .radio span.checked").parent(".radio").click(), $(".login-module input").keyup(function(event) {
            if (13 == event.which) {
                var submitButton = $(this).parents(".login-module:first").find(".module-footer .loader-button");
                submitButton.click(), eval(submitButton.attr("href").replace("javascript:", ""))
            }
        });
        var isLoginModule = $(".login-module").not(".newsletter .login-module").length > 0 || $(".resetPassword").length > 0 ? !0 : !1,
            isRequired = [];
        $this.find(".req-field").prev("input").blur(function(e) {
            var reqField = $(this).next(".req-field"),
                this_section = $(this).parents("section:first"),
                blankFields = [],
                numErrors = 0,
                labelId = $(this).attr("id");
            if (isRequired[labelId] = !0, "" == $(this).val()) {
                var label = $(this).parent().siblings(".label-wrap").find("label[for=" + labelId + "]");
                "undefined" != typeof $(this).attr("data-validationkey") && $(this).attr("data-validationkey").length > 0 ? blankFields.push($(this).attr("data-validationkey")) : label.length > 0 && blankFields.push(label.html().replace(":", ""))
            }
            if (blankFields.length > 0) {
                reqField.removeClass("error"), $("#em_" + labelId).remove(), numErrors += blankFields.length, reqField.addClass("error"), reqField.show();
                var keyfield = blankFields.join(", "),
                    key = keyfield + ":Required",
                    msg = LookupError(key, DefaultErrorLabel + " " + keyfield);
                DisplayError($(this), msg)
            } else(isLoginModule || this_section.parents("header").length > 0) && $(this).parent(".form-wrap").find(".error_message").remove(), reqField.removeClass("error"), $("#em_" + labelId).remove();
            numErrors > 0 && e.preventDefault()
        }), $this.find(".single-byte").blur(function(e) {
            var numErrors = ($(this).parents("section:first"), 0),
                labelId = $(this).attr("id");
            if ("" != $(this).val()) {
                var reg = ($(this).parents("section"), /^[\x20-\x7F]+$/),
                    field = $(this).val();
                if (0 == reg.test(field)) {
                    var reqField = $(this).next(".req-field");
                    reqField.removeClass("error"), $("#em_" + labelId).remove(), numErrors += 1, reqField.addClass("error"), $(this).show();
                    var msg = LookupError("EnglishOnlyError", "English characters only");
                    DisplayError($(this), msg)
                }
            } else 1 == isRequired[labelId] && (numErrors += 1);
            numErrors > 0 ? e.preventDefault() : ($(this).removeClass("error"), $("#em_" + labelId).remove())
        }), $this.find(".req-field").prev().find("input:radio").bind("blur change", function(e) {
            var reqField = $(this).parents(".form-wrap").next(".req-field"),
                blankFields = ($(this).parents("section:first"), []),
                numErrors = 0,
                labelId = $(this).attr("id"),
                IsChecked = !1;
            if (reqField.prev().find("input:radio").each(function() {
                    $(this).is(":checked") && (IsChecked = !0)
                }), !IsChecked) {
                var label = $(this).parents(".form-wrap").find(".RadioButtonErrorLabel").val();
                "undefined" != typeof $(this).attr("data-validationkey") && $(this).attr("data-validationkey").length > 0 ? blankFields.push($(this).attr("data-validationkey")) : label.length > 0 && blankFields.push(label)
            }
            if (blankFields.length > 0) {
                numErrors += blankFields.length, reqField.addClass("error"), reqField.show();
                var keyfield = blankFields.join(", "),
                    key = keyfield + ":Required",
                    msg = LookupError(key, DefaultErrorLabel + " " + keyfield);
                DisplayError($(this), msg)
            }
            numErrors > 0 ? e.preventDefault() : (reqField && reqField.removeClass("error"), $("#em_" + labelId).remove())
        }), $this.find(".req-field").prev().find("select").bind("blur change", function(e) {
            var reqField = $(this).parent().next(".req-field"),
                blankFields = ($(this).parents("section:first"), []),
                numErrors = 0,
                labelId = $(this).attr("id"),
                defaultvalue = $(this).siblings().find("input[id$=DefaultValue]").val();
            if (defaultvalue || (defaultvalue = ""), $(this).val() == defaultvalue) {
                var label = $(this).parents(".sub-block").find(".label-wrap").find("label[for=" + labelId + "]");
                "undefined" != typeof $(this).attr("data-validationkey") && $(this).attr("data-validationkey").length > 0 ? blankFields.push($(this).attr("data-validationkey")) : label.length > 0 && blankFields.push(label.html().replace(":", ""))
            }
            if (blankFields.length > 0) {
                numErrors += blankFields.length, reqField.addClass("error"), reqField.show();
                var keyfield = blankFields.join(", "),
                    key = keyfield + ":Required",
                    msg = LookupError(key, DefaultErrorLabel + " " + keyfield);
                DisplayError($(this), msg)
            }
            numErrors > 0 ? e.preventDefault() : (reqField && reqField.removeClass("error"), $("#em_" + labelId).remove())
        }), $(this).find("input[id$=MiddleInitial]").blur(function(e) {
            var numErrors = 0;
            if ("" != $(this).val()) {
                var reg = /^[A-Za-z]$/,
                    midInitial = $(this).val();
                if (0 == reg.test(midInitial)) {
                    numErrors += 1;
                    var msg = LookupError("MiddleInitialError", "Middle Initial must be one alpha character.");
                    DisplayError($(this), msg)
                }
                numErrors > 0 ? e.preventDefault() : $("#em_" + labelId).remove()
            }
        }), $(this).find("input[id$=Email]").blur(function(e) {
            var numErrors = 0,
                labelId = $(this).attr("id");
            if ("" != $(this).val()) {
                $(this).val($(this).val().replace(/ /g, ""));
                var reg = ($(this).parents("section"), emailRegex),
                    address = $(this).val();
                if (0 == reg.test(address)) {
                    numErrors += 1;
                    var reqField = $(this).next(".req-field");
                    reqField && (reqField.addClass("error"), reqField.show());
                    var msg = LookupError("EmailFormattingError", "Please enter a valid Email Address.");
                    DisplayError($(this), msg)
                }
                if (numErrors > 0) e.preventDefault();
                else {
                    var reqField = $(this).next(".req-field");
                    reqField && reqField.removeClass("error"), $("#em_" + labelId).remove()
                }
            }
        }), $(this).find("input[id$=PhoneAreaCode], input[id$=PhoneNumber]").blur(function(e) {
            var numErrors = 0,
                labelId = $(this).attr("id");
            if ("" != $(this).val()) {
                $(this).parents("section");
                if (this.id.indexOf("AreaCode") >= 0) var reg = /^([0-9\-\.]|N\/A)+$/;
                else var reg = /^.+$/;
                var areaCode = $(this).val();
                if (0 == reg.test(areaCode)) {
                    numErrors += 1;
                    var reqField = $(this).next(".req-field");
                    reqField && (reqField.addClass("error"), reqField.show());
                    var msg = LookupError("PhoneFormattingError", "Please enter a valid phone number.");
                    DisplayError($(this), msg)
                }
                if (numErrors > 0) e.preventDefault();
                else {
                    var reqField = $(this).next(".req-field");
                    reqField && reqField.removeClass("error"), $("#em_" + labelId).remove()
                }
            }
        }), $(this).find(".phoneCtryCode").change(function(e) {
            var labelId = $(this).attr("id");
            "" != $(this).val() && $("#em_" + labelId).remove()
        }), $(this).find("input[id$=Password]").blur(function(e) {
            if ($(this).attr("id").match(/ConfirmPassword$/)) {
                var p2 = $(this).parents("section").find("input[id$=txtPassword]");
                return void VerifyPasswords($(this), p2)
            }
            if ($(this).attr("id").match(/RetypePassword$/)) {
                var p2 = $(this).parents("section").find("input[id$=txtNewPassword]");
                return void VerifyPasswords($(this), p2)
            }
            var numErrors = 0;
            if ("" != $(this).val()) {
                var this_section = $(this).parents("section");
                if (document.URL.toString().toLowerCase().indexOf("fan-club-log-in") >= 0) var reg = /^.+$/;
                else var reg = passwordRegex;
                var pwd = $(this).val(),
                    reqField = $(this).next(".req-field");
                if (0 == reg.test(pwd)) {
                    numErrors += 1, reqField && (reqField.addClass("error"), reqField.show());
                    var msg = LookupError("FailedPasswordRequirements", "Please ensure password meets requirements: 8 characters, alphanumeric");
                    DisplayError($(this), msg)
                } else(isLoginModule || this_section.parents("header").length > 0) && $(this).parent(".form-wrap").find(".error_message").remove(), reqField.removeClass("error"), $("#em_pass_reqs").remove()
            }
            numErrors > 0 && e.preventDefault()
        });
        var condReq = !1;
        if ($(this).find("#optional-information .sub-block.home-address-block input[type=text]").blur(function(e) {
                checkFieldsForValue($("#optional-information .sub-block.home-address-block:first input[type=text], #optional-information .sub-block.home-address-block:first select[id$='ddlSecCountry'], #optional-information .sub-block.home-address-block:first select[id$='ddlSecState']")), condReq = !0
            }), $("#optional-information .form-wrap.radio-buttons").change(function() {
                checkFieldsForValue($("#optional-information .sub-block.home-address-block:first input[type=text], #optional-information .sub-block.home-address-block:first select[id$='ddlSecCountry'], #optional-information .sub-block.home-address-block:first select[id$='ddlSecState']")), condReq = !0
            }), document.URL.toString().indexOf("edit-profile") > 0) var CountryDropList = $(".fieldset.personal-details .form-wrap-sub.select-long .selector select").val();
        else var CountryDropList = "EMPTY";
        if ($(this).find(".home-phone-block input[type=text]").blur(function(e) {
                CountryDropList.indexOf("HK") >= 0 || CountryDropList.indexOf("SG") >= 0 ? ($(".home-phone-block .short-input").val("N/A"), checkFieldsForValue($(".home-phone-block .phoneCtryCode,.home-phone-block .medium-input"))) : checkFieldsForValue($(".home-phone-block .phoneCtryCode,.home-phone-block input[type=text]"))
            }), $(this).find(".home-phone-block .phoneCtryCode").change(function(e) {
                CountryDropList.indexOf("HK") >= 0 || CountryDropList.indexOf("SG") >= 0 ? ($(".home-phone-block .short-input").val("N/A"), checkFieldsForValue($(".home-phone-block .phoneCtryCode,.home-phone-block .medium-input"))) : checkFieldsForValue($(".home-phone-block .phoneCtryCode,.home-phone-block input[type=text]"))
            }), $this.find(".primary, .button").not(".newsletter .nav-login .alt.button").addClass("loader-button"), $this.find(".loader-button").click(function(e) {
                $(".my-profile .block .wrap").show(), $(".my-profile .profile-block-views .radio span.checked").removeClass("checked"), $("#uniform-bv-all span").addClass("checked");
                var isLoginModule = $(".login-module").not(".newsletter .login-module").length > 0 || $(".resetPassword").length > 0 ? !0 : !1,
                    this_section = $(this).parents("section:first");
                $(this_section).find(".error_message").remove();
                var numErrors = 0;
                if ($(this_section).find(".req-field").each(function() {
                        var labelIdToUse = null,
                            reqField = $(this),
                            blankFields = [];
                        if (reqField.prev("input").each(function() {
                                if ("" == $(this).val()) {
                                    var labelId = $(this).attr("id");
                                    labelIdToUse = labelId;
                                    var label = $(this).parent().siblings(".label-wrap").find("label[for=" + labelId + "]");
                                    if ("undefined" != typeof $(this).attr("data-validationkey") && $(this).attr("data-validationkey").length > 0) blankFields.push($(this).attr("data-validationkey"));
                                    else if (label.length > 0) blankFields.push(label.html().replace(":", ""));
                                    else if ("undefined" != typeof $(this).attr("placeholder") && $(this).attr("placeholder").length > 0) blankFields.push($(this).attr("placeholder").replace(":", ""));
                                    else switch (labelId) {
                                        case "home-address-city":
                                            blankFields.push("City");
                                            break;
                                        case "home-address-zip":
                                            blankFields.push("Postal Code");
                                            break;
                                        case "resFirstName":
                                            blankFields.push("FIRST NAME");
                                            break;
                                        case "resLastName":
                                            blankFields.push("LAST NAME");
                                            break;
                                        case "resEmail":
                                            0 == $("#resEmail").val().length && 0 == $("#resTelephone").val().length && -1 == $.inArray("EMAIL_PHONE", blankFields) && blankFields.push("EMAIL_PHONE");
                                            break;
                                        case "resTelephone":
                                            0 == $("#resEmail").val().length && 0 == $("#resTelephone").val().length && (-1 == $.inArray("EMAIL_PHONE", blankFields) && blankFields.push("EMAIL_PHONE"), labelIdToUse = "resEmail");
                                            break;
                                        default:
                                            blankFields.push("this field")
                                    }
                                }
                            }), reqField.prev().find("select").each(function() {
                                if ("" == $(this).val()) {
                                    var labelId = $(this).attr("id");
                                    labelIdToUse = labelId;
                                    var label = $(this_section).find("label[for=" + labelId + "]");
                                    "undefined" != typeof $(this).attr("data-validationkey") && $(this).attr("data-validationkey").length > 0 ? blankFields.push($(this).attr("data-validationkey")) : label.length > 0 ? blankFields.push(label.html().replace(":", "")) : blankFields.push("this field")
                                }
                            }), reqField.prev().find("input:radio:first").each(function() {
                                var NextReqField = $(this).parents(".form-wrap").next(".req-field"),
                                    IsChecked = !1;
                                if (NextReqField.prev().find("input:radio").each(function() {
                                        $(this).is(":checked") && (IsChecked = !0)
                                    }), !IsChecked) {
                                    var labelId = $(this).attr("id");
                                    labelIdToUse = labelId;
                                    var label = $(this).parents(".form-wrap").find(".RadioButtonErrorLabel").val();
                                    "undefined" != typeof $(this).attr("data-validationkey") && $(this).attr("data-validationkey").length > 0 ? blankFields.push($(this).attr("data-validationkey")) : "undefined" != typeof label && label.length > 0 && blankFields.push(label)
                                }
                            }), blankFields.length > 0) {
                            numErrors += blankFields.length, reqField.addClass("error"), reqField.show();
                            var keyfield = blankFields.join(", "),
                                key = keyfield + ":Required",
                                msg = LookupError(key, DefaultErrorLabel + " " + keyfield),
                                elem = document.getElementById(labelIdToUse);
                            elem && DisplayError($(elem), msg)
                        } else isLoginModule && $(this).parent(".form-wrap").find(".error_message").remove(), reqField.removeClass("error"), $("#em_" + labelIdToUse).remove()
                    }), $(this_section).find(".single-byte").each(function() {
                        if ("" != $(this).val()) {
                            var reg = /^[\x20-\x7F]+$/,
                                field = $(this).val();
                            if (0 == reg.test(field)) {
                                numErrors += 1;
                                var msg = LookupError("EnglishOnlyError", "English characters only");
                                DisplayError($(this), msg)
                            }
                        }
                    }), $(this_section).find("input[id$=MiddleInitial]").each(function() {
                        if ("" != $(this).val()) {
                            var reg = /^[A-Za-z]$/,
                                midInitial = $(this).val();
                            if (0 == reg.test(midInitial)) {
                                numErrors += 1;
                                var msg = LookupError("MiddleInitialError", "Middle Initial must be one alpha character.");
                                DisplayError($(this), msg)
                            }
                        }
                    }), $(this_section).find("input[id$=Email]").each(function() {
                        if ("" != $(this).val()) {
                            $(this).val($(this).val().replace(/ /g, ""));
                            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/,
                                address = $(this).val();
                            if (0 == reg.test(address)) {
                                numErrors += 1;
                                var reqField = $(this).next(".req-field");
                                reqField.addClass("error"), reqField.show();
                                var msg = LookupError("EmailFormattingError", "Please enter a valid Email Address");
                                DisplayError($(this), msg)
                            }
                        }
                    }), $(this_section).find("input[id$=PhoneAreaCode], input[id$=PhoneNumber]").each(function() {
                        if ("" != $(this).val()) {
                            if (this.id.indexOf("AreaCode") >= 0) var reg = /^([0-9\-\.]|N\/A)+$/;
                            else var reg = /^.+$/;
                            var areaCode = $(this).val();
                            if (0 == reg.test(areaCode)) {
                                numErrors += 1;
                                var reqField = $(this).next(".req-field");
                                reqField && (reqField.addClass("error"), reqField.show());
                                var msg = LookupError("PhoneFormattingError", "Please enter a valid phone number.");
                                DisplayError($(this), msg)
                            }
                        }
                    }), $(this_section).find("input[id$=Password]").each(function() {
                        if ($(this).attr("id").match(/ConfirmPassword$/)) {
                            var p2 = $(this).parents("section").find("input[id$=txtPassword]"),
                                reqField = p2.next(".req-field");
                            if ($(this).val() != p2.val()) {
                                numErrors += 1, reqField && (reqField.addClass("error"), reqField.show());
                                var msg = LookupError("FailedPasswordConfirmation", "Passwords must match exactly!");
                                DisplayError($(this), msg)
                            } else isLoginModule && $(this).parent(".form-wrap").find(".error_message").remove(), reqField.removeClass("error"), $(".em_pass_match").remove()
                        } else if ($(this).attr("id").match(/RetypePassword$/)) {
                            var p2 = $(this).parents("section").find("input[id$=txtNewPassword]"),
                                reqField = p2.next(".req-field");
                            if ($(this).val() != p2.val()) {
                                reqField && (reqField.addClass("error"), reqField.show()), numErrors += 1;
                                var msg = LookupError("FailedPasswordConfirmation", "Passwords must match exactly!");
                                DisplayError($(this), msg)
                            } else isLoginModule && $(this).parent(".form-wrap").find(".error_message").remove(), reqField.removeClass("error"), $(".em_pass_match").remove()
                        } else if ("" != $(this).val()) {
                            $(this).parents("section");
                            if (document.URL.toString().toLowerCase().indexOf("fan-club-log-in") >= 0) var reg = /^.+$/;
                            else var reg = /^([A-Za-z])([A-Za-z0-9]{7,11})$/;
                            var pwd = $(this).val(),
                                reqField = $(this).next(".req-field");
                            if (0 == reg.test(pwd)) {
                                numErrors += 1, reqField && (reqField.addClass("error"), reqField.show());
                                var msg = LookupError("FailedPasswordRequirements", "Please ensure password meets requirements: 8 characters, alphanumeric");
                                DisplayError($(this), msg)
                            } else isLoginModule && $(this).parent(".form-wrap").find(".error_message").remove(), reqField.removeClass("error"), $("#em_pass_reqs").remove()
                        }
                    }), $(this_section).find("input[id$=chkAcceptTerms]").each(function() {
                        if (!$(this).is(":checked")) {
                            numErrors += 1;
                            var msg = LookupError("Terms_Not_Accepted", "Must be 'checked' to proceed with Registration");
                            DisplayError($(this), msg)
                        }
                    }), $(this_section).find("input[id$=chkIncludeWebsiteBRP]").each(function() {
                        if (!$(this).is(":checked")) {
                            numErrors += 1;
                            var msg = LookupError("BRP:WebsiteNotAccepted", "Must be 'checked' to process your claim");
                            DisplayError($(this), msg)
                        }
                    }), $(this_section).find("input[id$=chkAcceptTermsBRP]").each(function() {
                        if (!$(this).is(":checked")) {
                            numErrors += 1;
                            var msg = LookupError("BRP:TermsNotAccepted", "Must be 'checked' to process your claim");
                            DisplayError($(this), msg)
                        }
                    }), $(this_section).find("input[id$=chkAcceptTermsGiveaway]").each(function() {
                        if (!$(this).is(":checked")) {
                            numErrors += 1;
                            var msg = LookupError("Giveaway:TermsNotAccepted", "Accept the Terms &amp; Conditions to proceed with submission");
                            DisplayError($(this), msg)
                        }
                    }), CountryDropList.indexOf("HK") >= 0 || CountryDropList.indexOf("SG") >= 0 ? numErrors += checkFieldsForValue($this.find(".home-phone-block .phoneCtryCode,.home-phone-block .medium-input")) : condReq || (numErrors += checkFieldsForValue($this.find(".home-phone-block .phoneCtryCode,.home-phone-block input[type=text]"))), condReq && (numErrors += checkFieldsForValue($("#optional-information .sub-block.home-address-block:first input[type=text], #optional-information .sub-block.home-address-block:first select[id$='ddlSecCountry'], #optional-information .sub-block.home-address-block:first select[id$='ddlSecState']"))), numErrors > 0) {
                    if (e.preventDefault(), $(this_section).find(".req-field:first").show(), $(this).parents("div:first").find(".ajaxloader").remove(), element = $(".error_messages div:first"), element.length > 0) {
                        var errElem = $("#" + $(element).attr("id").substr(3))[0];
                        $(errElem).focus(), $("html, body").stop().animate({
                            scrollTop: $(errElem).offset().top - 50
                        }, 0)
                    }
                } else isLoginModule && $(this).parent(".form-wrap").find(".error_message").remove(), $(this_section).find(".req-field").hide(), $(this).parents("div:first").find(".ajaxloader").remove(), $(this).parents("div:first").append(' ')
            }), $this.find("input[id$=btnCancel]").click(function(e) {
                var confirmCancel = confirm("Are you sure you want to cancel?");
                1 != confirmCancel && e.preventDefault()
            }), $this.find("[id$=lbForgotPassword],[id$=lblLoginForgotPassword]").click(function(e) {
                var isLoginModule = $(this).parents(".login-module") ? !0 : !1,
                    this_section = $(this).parents("section");
                $(this_section).find("input[id$=Email]").each(function() {
                    var numErrors = 0,
                        reqField = $(this).next(".req-field");
                    if ("" == $(this).val()) {
                        var blankFields = [],
                            labelId = $(this).attr("id"),
                            label = $(this).parent().siblings(".label-wrap").find("label[for=" + labelId + "]");
                        if ("undefined" != typeof $(this).attr("data-validationkey") && $(this).attr("data-validationkey").length > 0 ? blankFields.push($(this).attr("data-validationkey")) : label.length > 0 ? blankFields.push(label.html().replace(":", "")) : $(this).attr("placeholder").length > 0 && blankFields.push($(this).attr("placeholder").replace(":", "")), blankFields.length > 0) {
                            numErrors += blankFields.length, reqField.addClass("error"), reqField.show();
                            var keyfield = blankFields.join(", "),
                                key = keyfield + ":Required",
                                msg = LookupError(key, DefaultErrorLabel + " " + keyfield);
                            isLoginModule ? ($(this).parent(".form-wrap").find(".error_message").remove(), $(this).parent(".form-wrap").append('<div class="error_message">' + msg + "</div>"), $(this).parent(".form-wrap").find(".error_message").show()) : $(this_section).find(".error_messages").append('<div class="error_message" style="top:' + $(this).position().top + 'px;"><ul><li> ' + msg + "</li></ul></div>")
                        }
                    } else {
                        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                        $(this).val($(this).val().replace(/ /g, ""));
                        var address = $(this).val();
                        if (0 == reg.test(address)) {
                            numErrors += 1, reqField.addClass("error"), reqField.show();
                            var msg = LookupError("EmailFormattingError", "Please enter a valid Email Address");
                            isLoginModule ? ($(this).parent(".form-wrap").find(".error_message").remove(), $(this).parent(".form-wrap").append('<div class="error_message">' + msg + "</div>"), $(this).parent(".form-wrap").find(".error_message").show()) : $(this_section).find(".error_messages").append('<div class="error_message" style="top:' + $(this).position().top + 'px;"><ul><li>' + msg + "</li></ul></div>")
                        }
                    }
                    numErrors > 0 && e.preventDefault()
                })
            }), $(".room-availability .long-picker").length > 0) {
            var initclick = $(".room-availability .button").attr("onclick");
            $(".room-availability .button").attr("onclick", null), $(".room-availability .button").click(function() {
                if ("" == $('.room-availability [name*="ddlHotels"]').val()) {
                    var msg = LookupError("CheckAvailabilitySelectHotelError", "Please select a hotel.");
                    return alert(msg), !1
                }
                return eval(initclick.replace("javascript:", "").replace("return false;", "")), !1
            })
        }
        if ($("#search input[type=text]").focus(clearText), $("#search input[type=text]").blur(clearText), $this.find("select[id$=ConnectSelect]").change(function() {
                var connectUrl = $(this).val();
                "/" != connectUrl && (connectUrl += "/"), window.location.href = connectUrl + "connect/"
            }), $this.find("select[id$=PressKitSelect]").change(function() {
                var pkUrl = $(this).val();
                window.location.href = "/media/press-information" + pkUrl
            }), window.addthis)
            if (0 == addThisInitialized) addthis.init(), addThisInitialized = !0;
            else {
                var script = window.location.protocol + "//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4e80c55d6a278822";
                window.addthis = null, window._adr = null, window._atc = null, window._atd = null, window._ate = null, window._atr = null, window._atw = null, $("script[src*='addthis']").remove(), $.getScript(script)
            }
        var homepageSlickCarousel = $("#home-carousel-slick"),
            transitionSpeedSlick = 1e3,
            slickRTL = !1,
            infiniteLoop = !0,
            stopSlideShow = !1;
        $("#home-carousel-slick.hotel-specific #promo-wrap").length > 0 && (infiniteLoop = !1), "rtl" == $("html").attr("dir") && (slickRTL = !0),
            homepageSlickCarousel.on("init", function() {
                setHeightCheckAvailabilityOverlay()
            }), homepageSlickCarousel.slick({
                arrows: !1,
                dots: !0,
                autoplay: !0,
                autoplaySpeed: 5e3,
                cssEase: "cubic-bezier(0.275, 0.720, 0.565, 1.000)",
                pauseOnHover: !1,
                speed: transitionSpeedSlick,
                rtl: slickRTL
            }).on("beforeChange", function(event, slick, currentSlide, nextSlide) {
                homepageSlickCarousel.find(".slide-caption").hide(), $("#home-carousel-wrap.hotel-specific #promo-wrap").length > 0 && 0 == nextSlide && (stopSlideShow = !0)
            }).on("afterChange", function(event, slick, currentSlide) {
                if (homepageSlickCarousel.find(".slick-active .slide-caption").fadeIn(transitionSpeedSlick), stopSlideShow) {
                    homepageSlickCarousel.slick("slickPause");
                    var directionToSlideFrom = "right";
                    slickRTL && (directionToSlideFrom = "left"), $("#home-carousel-wrap.hotel-specific #promo-wrap").show("slide", {
                        direction: directionToSlideFrom
                    }, 1e3)
                }
            });
        var homepageSlickSmallCarousel = $(".smallCarousel"),
            transitionSpeedSmallCarouselSlick = 1e3;
        homepageSlickSmallCarousel.slick({
            arrows: !1,
            dots: !0,
            autoplay: !0,
            autoplaySpeed: 5e3,
            pauseOnHover: !1,
            speed: transitionSpeedSlick,
            rtl: slickRTL
        }), $("#slideDownbox, .carousel-check-availability .closedButton").click(function(event) {
            return $(".slideDownbox-container").slideToggle("fast", setHeightCheckAvailabilityOverlay), $(this).toggleClass("iconize"), $(this).toggleClass("iconize2"), $(this).parent().find(".openedButton").toggle("fast"), $(this).parent().find(".closedButton").toggle("fast", function() {
                $(this).is(":visible") && $(this).css("display", "block")
            }), event.preventDefault(), !1
        }), $("a[id^=specialcodesbox]").click(function(event) {
            return $("#codes").slideToggle("fast", function() {
                setHeightCheckAvailabilityOverlay(), $(this).find(".placeholder:visible").length > 0 ? $(this).find(".placeholder:hidden").hide() : $(this).find(".placeholder:visible").show()
            }), $(this).toggleClass("iconplus"), $(this).toggleClass("iconminus"), event.preventDefault(), !1
        }), $("a[id^=searchbox]").click(function(event) {
            return $("#savedSearchHolder").slideToggle("fast", function() {
                setHeightCheckAvailabilityOverlay(), $(this).find(".placeholder:visible").length > 0 ? $(this).find(".placeholder:hidden").hide() : $(this).find(".placeholder:visible").show()
            }), $(this).toggleClass("iconplus"), $(this).toggleClass("iconminus"), event.preventDefault(), !1
        }), $(".language-popup-link.close-btn").click(function(event) {
            return closeLanguagePopup(), event.preventDefault(), !1
        });
        var parameterToNotShowPopup = getParameterByName("nlp"),
            languagesPopupArray = ["AD", "AE", "AF", "AR", "AT", "BE", "BF", "BH", "BI", "BJ", "BO", "BR", "BY", "CD", "CG", "CH", "CL", "CM", "CN", "CO", "CR", "CU", "DE", "DJ", "DZ", "EC", "EG", "EH", "ER", "ES", "FR", "GA", "GF", "GG", "GN", "GP", "GQ", "GT", "GW", "HK", "HN", "HT", "IL", "IQ", "IR", "JE", "JO", "JP", "KG", "KM", "KW", "KZ", "LB", "LI", "LU", "LY", "MA", "MC", "MG", "MO", "MQ", "MR", "MX", "NC", "NE", "NI", "OM", "PA", "PE", "PF", "PR", "PY", "QA", "RE", "RU", "RW", "SA", "SC", "SD", "SN", "SO", "SV", "SY", "TD", "TF", "TG", "TJ", "TN", "TW", "UA", "UY", "VE", "VU", "WF", "YE", "YT"];
        if ("1" != parameterToNotShowPopup) {
            if (-1 != $.inArray(requestor_cc, languagesPopupArray) && (null == readCookie("showLangPop") || "" == readCookie("showLangPop")) && $(".language-popup").length > 0) {
                var currentLanguage = $($("#language > a").get(0));
                $(".LanguageSelectorPopupLanguageSelect").append($("<option></option>").attr("value", window.location.protocol + "//" + window.location.host).text(currentLanguage.text())).attr("selected", "selected"), $.uniform.update(".LanguageSelectorPopupLanguageSelect"), $("#language ul li a").each(function(index) {
                    $(".LanguageSelectorPopupLanguageSelect").append($("<option></option>").attr("value", this.protocol + "//" + this.hostname).text($(this).text()))
                }), $.uniform.update(".LanguageSelectorPopupLanguageSelect"), $(".language-popup").show(), $(".language-popup-wrap").height($(window).height()), $(".language-popup-lightbox-overlay").height(2 * $(window).height()).show(), $(".language-popup .dismiss").click(function(e) {
                    return closeLanguagePopup(), createCookie("showLangPop", "false", 30), e.preventDefault(), !1
                }), $(".language-popup .continue").click(function(e) {
                    var path = window.location.pathname || "/",
                        qs = window.location.search || "?";
                    return qs.length > 1 && (qs += "&"), window.location = $(".LanguageSelectorPopupLanguageSelect").val() + path + qs + "nlp=1", e.preventDefault(), !1
                })
            }
        } else createCookie("showLangPop", "false", 30);
        "false" == readCookie("showLangPop") && createCookie("showLangPop", "false", 30);
        var langForTripAdvisor = $("html").attr("lang");
        if ("zh" == langForTripAdvisor && (langForTripAdvisor = "zhCN"), $(".trip-advisor.review").each(function(index, element) {
                var locationID = $(this).attr("data-reviewid"),
                    elementWrap = $(this);
                locationID && $.ajax({
                    url: "//api.tripadvisor.com/api/partner/2.0/location/" + locationID + "/reviews?key=47CB3849333C4D1DAC6080F8B16C0117&lang=" + langForTripAdvisor,
                    cache: !1,
                    dataType: "json"
                }).done(function(json) {
                    $.each(json.data, function() {
                        if (this.rating >= 4) {
                            elementWrap.find(".star-rating").html("<img src=" + this.rating_image_url + "/>"), elementWrap.find(".rtf blockquote p").text(this.title), null != this.user && null != this.user.username && null != this.user.user_location.name ? elementWrap.find(".testimonial .author").text(this.user.username + ": " + this.user.user_location.name) : null != this.user && null != this.user.username && elementWrap.find(".testimonial .author").text(this.user.username);
                            var permUserText = "A TRIPADVISOR TRAVELER";
                            elementWrap.find(".testimonial .desc").text(permUserText);
                            var publishDate = new Date(this.published_date.replace(/T.*/gi, ""));
                            return elementWrap.find(".testimonial .date").text($.datepicker.formatDate("dd M yy", publishDate)), elementWrap.find(".trip-advisor-more").attr("href", this.url), elementWrap.parents(".homepage-module").each(function() {
                                $(this).find(".three-col.module").removeClass(function(index, css) {
                                    return (css.match(/(^|\s)js-colgroup-\S+/g) || []).join(" ")
                                }), $(this).normalizeHeight(".three-col.module")
                            }), !1
                        }
                    })
                }).fail(function(jqXHR, textStatus) {})
            }), $(".savedSearches").length > 0) {
            $(".savedSearches, #searchbox").show();
            var cookieVal = getCookie("recentSearches"),
                searchesJSON = {
                    items: []
                };
            "" != cookieVal && (searchesJSON = JSON.parse(getCookie("recentSearches")));
            var savedItem = $($("#savedSearchHolder .individualSavedSearch").get(0));
            $.each(searchesJSON.items, function(index, value) {
                var currentItem = savedItem;
                0 != index && (currentItem = savedItem.clone());
                var dateArrive = new Date(value.Arrive),
                    dateDepart = new Date(value.Depart);
                currentItem.find(".nameValue").html(value.hotelName), currentItem.find(".dateValue").html($.datepicker.formatDate("M d", dateArrive)), currentItem.find(".dateNightsValue").html(daydiff(dateArrive, dateDepart)), currentItem.find(".roomTypeValue").html(value.roomTitle), currentItem.find(".booklink").attr("onclick", 'javascript:bookmystay("hotel:' + value.hotel + '","room:' + value.room + '","Arrive:' + value.Arrive + '","Depart:' + value.Depart + '");return false;'), currentItem.find(".removelink").attr("onclick", "javascript:removeFromCookie(" + JSON.stringify(value) + ");$(this).parent().hide();"), 0 != index && currentItem.appendTo("#savedSearchHolder")
            })
        }
        $('input[name="grid_view_radio"]').length > 0 && ($($('input[name="grid_view_radio"]').get(0)).prop("checked", !0), $("#grid_view_radio_list span.checked").removeClass("checked"), $('input[name="grid_view_radio"]:checked').parents("span").addClass("checked"), $('input[name="grid_view_radio"]').on("change", function() {
            $("#main-contents section").hide(), "all" == $(this).val().toLocaleLowerCase() ? $("#main-contents section").show() : ($.each($(this).val().split(","), function(index, value) {
                $("#main-contents section").find(".radioValue-" + value).parents("section").show()
            }), $("#main-contents section .module.three-col").css("min-height", "auto"), $("#main-contents section .module.three-col").removeClass("right"), $("#main-contents section .module.three-col").removeClass(function(index, css) {
                return (css.match(/(^|\s)js-colgroup-\d+/g) || []).join(" ")
            })), normalizeHeightGridView()
        }))
    }, $(window).resize(function() {
        centerVideo()
    }), $(document).ready(function() {
        centerVideo(), $("a.DiningForm[rel^=]").each(function() {
            var href = $(this).attr("href"),
                rel = $(this).attr("rel");
            $(this).attr("href", href + rel)
        })
    });
    var isIE = function() {
        return "Microsoft Internet Explorer" == navigator.appName || "Netscape" == navigator.appName && null != new RegExp("Trident/.*rv:([0-9]{1,}[.0-9]{0,})").exec(navigator.userAgent)
    };
    $("<style type='text/css'> .s7emaildialog{ z-index:111111!important;} .hideIEScrollbar{-ms-overflow-style:none}</style>").appendTo("head");
    var script = document.createElement("script");
    script.src = window.location.protocol + "//photos.mandarinoriental.com/s7viewers/html5/js/VideoViewer.js", script.type = "text/javascript", document.getElementsByTagName("head")[0].appendChild(script);
    var exmp, flagfirstOpen = 1;
    window.openVideoZoom = function(playerUrl, videoSrc) {
        var html5VideoCapable = !1;
        $("html").hasClass("video") && (html5VideoCapable = !0), exmp = videoSrc;
        var last = exmp.split("/")[exmp.split("/").length - 1],
            assetID = "MandarinOriental/" + last.replace(".mp4", "");
        if ("https:" == document.location.protocol && (playerUrl = playerUrl.replace("http:", "https:")), !isMobile.any(), navigator.appVersion.indexOf("MSIE 8") > -1) {
            $("#image-zoom").append('<div class="zoom_video_cont"><div id="zoom_video"></div></div>'), $("#image-zoom").show(), isMobile.any() ? window.location = videoSrc : swfobject.embedSWF(playerUrl + "?t=" + (new Date).getTime(), "zoom_video", "640", "480", "9.0.0", !1, {
                videoURL: videoSrc,
                useFullScreen: "false",
                useShareButtons: "false",
                useInfoButton: "false",
                playerBackgroundColor: "47413a",
                playerBackgroundAlpha: "100",
                controlsColor: "c39122",
                controlsAlpha: "100",
                controlsSeparation: "15",
                use3Dstyle: "false",
                initialVol: "70",
                volBackgroundColor: "9c998e",
                volBarColor: "c39122",
                timelineDownloadColor: "c39122",
                timelineBackgroundColor: "9c998e",
                timelineBackgroundColor: "9c998e",
                timelineBackgroundAlpha: "100"
            });
            var winHeight = $(window).height(),
                contHeight = $("#image-zoom .zoom_video_cont").height(),
                newTop = winHeight / 2 - contHeight / 2;
            $("#image-zoom .zoom_video_cont").css("margin-top", newTop + "px")
        } else {
            if (html5VideoCapable) {
                var VideoMarkup = '<div class="zoom_video_cont"><div id="zoom_video"><div class="video-container"> ';
                VideoMarkup += navigator.userAgent.indexOf("Trident/6.0") > -1 || navigator.userAgent.indexOf("Trident/7.0") > -1 ? '<video id="video-1" class="video-js vjs-sublime-skin" preload="none" width="940" height="530" src="' + videoSrc + "\" controls data-setup='{}' " : '<video id="video-1" class="video-js vjs-sublime-skin" preload="auto" width="940" height="530" autoplay="" src="' + videoSrc + "\" controls data-setup='{}' ", VideoMarkup += '><p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p></video> <div class="social-share-overlay"><div class="social-share-container"></div></div></div>', $("#image-zoom").append(VideoMarkup);
                var player = videojs("video-1", {}, function() {
                    this.omniVideoTracker();
                    this.on("ended", function() {
                        $(".social-share-overlay").click()
                    })
                });
                player.on("play", function() {
                    this.posterImage.hide(), $(".vjs-poster").css("display", "none")
                }), player.on("started", function() {
                    this.posterImage.hide(), $(".vjs-poster").css("display", "none")
                }), $(".social-share-open, .social-share-overlay").click(function() {
                    $(".video-container").toggleClass("active"), $(".video-container").hasClass("active") ? ($("#video-1").addClass("vjs-controls-disabled"), $("#video-1")[0].player.pause()) : ($("#video-1").removeClass("vjs-controls-disabled"), $("#video-1")[0].player.play())
                })
            } else {
                if (1 == flagfirstOpen) {
                    $("body").append('<div id="s7_videoview"></div>');
                    var v_width = $(window).width() / 2 - 320,
                        v_height = $(window).height() / 2 - 240,
                        Style = {
                            position: "fixed",
                            top: v_height,
                            "z-index": "100001",
                            left: v_width
                        };
                    $("#s7_videoview").css(Style), $(window).resize(function() {
                        v_width = $(window).width() / 2 - 320, v_height = $(window).height() / 2 - 240, Style = {
                            position: "fixed",
                            top: v_height,
                            "z-index": "100001",
                            left: v_width
                        }, $("#s7_videoview").css(Style), $(".s7emaildialog").css({
                            "z-index": "100002!important"
                        })
                    }), videoViewer = new s7viewers.VideoViewer, videoViewer.setContainerId("s7_videoview"), videoViewer.setParam("serverurl", window.location.protocol + "//s7d9.scene7.com/is/image/"), videoViewer.setParam("contenturl", window.location.protocol + "//s7d9.scene7.com/skins/"), videoViewer.setParam("videoserverurl", window.location.protocol + "//s7d9.scene7.com/is/content/"), videoViewer.setParam("asset", assetID), videoViewer.setParam("config", "MandarinOriental/AutoPlayPreset_HTML5_Social_final"), videoViewer.setParam("emailurl", window.location.protocol + "//s7d9.scene7.com/s7/emailFriend"), videoViewer.setParam("stagesize", "640,480"), videoViewer.init(), flagfirstOpen = 0
                } else {
                    $("body").append('<div id="s7_videoview"></div>');
                    var v_width = $(window).width() / 2 - 320,
                        v_height = $(window).height() / 2 - 240,
                        Style = {
                            position: "fixed",
                            top: v_height,
                            "z-index": "100001",
                            left: v_width,
                            color: "black"
                        };
                    $("#s7_videoview").css(Style), $(window).resize(function() {
                        v_width = $(window).width() / 2 - 320, v_height = $(window).height() / 2 - 240, Style = {
                            position: "fixed",
                            top: v_height,
                            "z-index": "100001",
                            left: v_width,
                            color: "black"
                        }, $("#s7_videoview").css(Style), $(".s7emaildialog").css({
                            "z-index": "100002!important"
                        })
                    }), videoViewer = new s7viewers.VideoViewer, videoViewer.setContainerId("s7_videoview"), videoViewer.setParam("serverurl", window.location.protocol + "//s7d9.scene7.com/is/image/"), videoViewer.setParam("contenturl", window.location.protocol + "//s7d9.scene7.com/skins/"), videoViewer.setParam("videoserverurl", window.location.protocol + "//s7d9.scene7.com/is/content/"), videoViewer.setParam("asset", assetID), videoViewer.setParam("config", "MandarinOriental/AutoPlayPreset_HTML5_Social_final"), videoViewer.setParam("emailurl", window.location.protocol + "//s7d9.scene7.com/s7/emailFriend"), videoViewer.setParam("stagesize", "640,480"), videoViewer.init()
                }
                $("#s7_videoview").click(function(e) {
                    e.stopPropagation()
                })
            }
            $("#image-zoom .zoom_video_cont").click(function(e) {
                e.stopPropagation()
            }), $("#image-zoom").show(), isMobile.any();
            var winHeight = $(window).height(),
                contHeight = $("#image-zoom .zoom_video_cont").height(),
                newTop = winHeight / 2 - contHeight / 2;
            if (!isMobile.any()) {
                var $container = $("#image-zoom .zoom_video_cont"),
                    $video = $container.find("video");
                if ($video.length > 0) {
                    var vidWidth = $video.attr("width"),
                        vidHeight = $video.attr("height");
                    vidWidth && $container.css("width", vidWidth), vidHeight && $container.css("height", vidHeight)
                }
            }
            isIE() && $("body").addClass("hideIEScrollbar")
        }
    }, window.closeImageZoom = function() {
        $("#video-1").length > 0 && $("#video-1")[0].player.dispose();
        try {
            isIE() && $("body").removeClass("hideIEScrollbar"), $("#s7_videoview").hide(), $("#s7_videoview").remove()
        } catch (e) {}
        $("#image-zoom .zoom_video_cont video").length > 0 && ($("#image-zoom .zoom_video_cont video")[0].pause(), currentGalleryDetail = null), 0 == denyImageZoomClosing && ($("#image-zoom, #image-zoom-fansof").find("img").remove(), $("#image-zoom, #image-zoom-fansof").find(".zoom_video_cont").remove(), $("#image-zoom, #image-zoom-fansof").hide())
    }, $(".experienceMO-wrap").length > 0 && $(".experienceMO-wrap").parents("#main-contents").css("margin-top", 0).css("width", "1400px").css("max-width", "100%")
}(jQuery);
var idleTime = 0;
$(document).ready(function() {
    function closeModalFunction() {
        $(".modal-window-close-icon").click(function(e) {
            $(".atp-modal-window").hide(), $(".booking-modal-window").hide(), $("header").css("z-index", headerZ), $("nav").css("z-index", navZ)
        })
    }

    function moveModalMarkup() {
        var modalMarkup = $(".booking-modal-window").clone().wrap("<p>").parent().html();
        $(".booking-modal-window").remove(), $(modalMarkup).appendTo("#page-wrap"), $("#page-wrap .booking-modal-window").hide()
    }
    $("input[id$='from']").click(function() {
        $(".ui-datepicker-trigger.picker0").click()
    }), $("input[id$='to']").click(function() {
        $(".ui-datepicker-trigger.picker1").click()
    });
    var URLpattern = /my-profile\/.*/;
    if (URLpattern.test(document.URL.toString())) {
        var URLSnippet = document.URL.match(URLpattern).toString();
        if (URLSnippet.length > 11) {
            setInterval("timerIncrement()", 6e4);
            $(this).click(function(e) {
                idleTime = 0
            }), $(this).keypress(function(e) {
                idleTime = 0
            })
        }
    }
    var mobileButtonOnly = $(".details-buttons.details-button-only.room-availability");
    null != mobileButtonOnly && ($("#main-contents .details-buttons:first").append($("#main-contents .offers-nav")), $(".details-buttons.details-button-only.room-availability").css("padding-bottom", "0")), $(".last.sitemap.navgroup ul:first").append("<li id='footerLangNav' class='navgroup'><p class='footerLang'>Languages</p></li>"), $("#language .small section ul").clone().appendTo(".last.sitemap.navgroup ul #footerLangNav.navgroup"), $(".last.sitemap.navgroup ul li:first").attr("id", "footerHotelNav").append("<ul></ul>"), $("#info-list .more-action:first section ul li").clone().appendTo(".last.sitemap.navgroup ul #footerHotelNav.navgroup ul"), $("#footer .navgroup .first.social a").each(function(index) {
        "" == $(this).attr("href").toString() && $(this).css("display", "none")
    });
    var lang = "";
    void 0 !== s.prop2 && (lang = s.prop2), "undefined" != typeof window.keepLanguages && null !== window.keepLanguages && "" !== window.keepLanguages && (window.keepLanguages.indexOf(lang) >= 0 || $("ul#promo-wrap").css("display", "none")), $("#header-lists li a#fanClubLogout").css("display", "none"), 0 != document.URL.indexOf("/fanclub/") || document.URL.indexOf("/fan-club-log-in/") >= 0 || ($("#page-wrap #main-nav li.first").append('<a id="fanClubLogout" href="/fanclub/fan-club-log-in/">SIGN OUT</a>'), $("#page-wrap #main-nav li.first a").css("display", "inline"), $("#page-wrap #main-nav li.first a#fanClubLogout").css("margin-left", "24px")), $("#page-wrap #main-nav li.first a#fanClubLogout").click(function() {
        var date = new Date;
        date.setTime(date.getTime() + -864e5);
        var expires = "; expires=" + date.toGMTString();
        document.cookie = "profile_email=" + expires + "; path=/"
    }), $("#page-wrap div[id$='UpdateSearchResultsPanel1']").addClass("GalleryFooterFix"), $(".celebrity-gallery").length > 0 && $(".detail-cycle").each(function() {
        var detailCycle = $(this);
        detailCycle.find("img").first();
        detailCycle.parents(".sub-page-carousel").height($(".celebrity-gallery .port-img").height())
    });
    var openHatMenu = function($t) {
            if (!$t.hasClass("active")) {
                var $a = $t.add($(".hat")).add($t.closest(".nav-menu,.lang-menu")).add($t.siblings("ul"));
                $(".hat").after('<img src="/static/images/bg/space.png" class="hat-menu-spacer">'), $(".hat-menu-spacer").on("click", function(e) {
                    e.preventDefault(), closeHatMenu()
                }).width(function() {
                    return $(document).width() > $(window).width() ? $(document).width() : $(window).width()
                }).height(function() {
                    return $(document).height() > $(window).height() ? $(document).height() : $(window).height()
                }), $t.siblings("ul").height(function() {
                    var height = $(this).height(),
                        windowHeight = window.innerHeight,
                        hatHeight = $(this).closest(".hat").height();
                    return windowHeight - hatHeight > height ? height : windowHeight - hatHeight
                }).fadeIn(200, function() {
                    $a.addClass("active")
                })
            }
        },
        closeHatMenu = function($t) {
            var $o = $(".hat .current-lang.active,.hat .menu-label.active"),
                $a = $(".hat.active, .hat .active");
            0 != $o.length && ($(".hat-menu-spacer").remove(), $o.siblings("ul").eq(0).height("").fadeOut(200, function() {
                $a.removeClass("active")
            }), "undefined" != typeof $t && $t && $t.length > 0 && openHatMenu($t))
        };
    $(".hat .menu-label, .hat .current-lang").on("click.hatMenu", function(e) {
        e.preventDefault(), e.stopPropagation(), $("body").hasClass("mobile") && ($(".hat").hasClass("active") ? closeHatMenu($(this)) : openHatMenu($(this)))
    }), $(".group-directory-selection").click(function(e) {
        e.preventDefault();
        var o = $("#rs-dropdown-example").find("option:selected").val();
        o && o.match(/http/) && window.open(o)
    });
    var formLabelToPlaceholder = function() {
        $("body").hasClass("mobile") && $("fieldset input, fieldset textarea").each(function() {
            var $t = $(this);
            ($t.is("textarea") || $t.attr("type").match(/(text|password)/)) && ($t.attr("placeholder") || 1 != $('label[for="' + $t.attr("id") + '"]').length || $t.attr("placeholder", $('label[for="' + $t.attr("id") + '"]').text()))
        })
    };
    formLabelToPlaceholder();
    var contactUsPageNavigator = function() {
        if ($("body").hasClass("mobile")) {
            var b = $("#contact-picker option:selected").val();
            $(".contact-us .block:not(:has(#contact-picker))").removeClass("active"), "all" == b ? $(".contact-us .block").addClass("active") : $("#" + b).show().addClass("active"), $("#contact-picker").change(function() {
                b = $(this).find("option:selected").val(), "all" == b ? $(".contact-us .block").addClass("active") : ($(".contact-us .block:not(:has(#contact-picker))").removeClass("active"), $("#" + b).show().addClass("active"))
            })
        }
    };
    contactUsPageNavigator(), $("body").hasClass("mobile") && $(".contact-us .sub-block .column").not(".right,:first-of-type").addClass("not-first-column"), $.fn.addToProfileLayerFix = function() {
        var rrZ, mainZ;
        $(this).on("click.atpl", ".addToProfile", function() {
            rrZ = $(".right-rail").css("z-index"), mainZ = $("#main").css("z-index"), $(".right-rail").css("z-index", 999), $("#main").css("z-index", 999)
        }).on("click.atpl", ".atp-modal-window a", function() {
            $(".right-rail").css("z-index", rrZ), $("#main").css("z-index", mainZ)
        })
    }, $(".GalleryFooterFix").addToProfileLayerFix();
    var headerZ, navZ;
    $(".addToProfile").click(function(event) {
        headerZ = $("#header").css("z-index"), navZ = $("nav").css("z-index"), $("body.mobile").length > 0 ? ($("header").css("z-index", 1), $("nav").css("z-index", 1)) : ($("header").css("z-index", 105), $("nav").css("z-index", 105)), $(this).siblings(".atp-modal-window").show(), closeModalFunction()
    }), $(".book-restaurant").click(function(event) {
        event.preventDefault();
        var width = 400,
            height = 600,
            left = parseInt(screen.availWidth / 2 - width / 2),
            top = parseInt(screen.availHeight / 2 - height / 2);
        window.open($(this).attr("href"), "_blank", "height=600,width=400,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top);
        setTimeout(function() {
            $(".book-restaurant").parents("div:first").find(".ajaxloader").remove()
        }, 2e3)
    }), $(".book-restaurant-reservation").click(function(event) {
        event.preventDefault();
        var width = 400,
            height = 600,
            left = parseInt(screen.availWidth / 2 - width / 2),
            top = parseInt(screen.availHeight / 2 - height / 2);
        window.open($(this).attr("href"), "_blank", "height=600,width=400,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top);
        setTimeout(function() {
            $(".book-restaurant-reservation").parents("div:first").find(".ajaxloader").remove()
        }, 2e3)
    }), closeModalFunction(), $("#slide-menu-item-5 .booking-modal-window:hidden").length > 0 && moveModalMarkup(), 0 == window.location.pathname.indexOf("/fanclub/") && ($("header").addClass("fanclub"), "/fanclub/default.aspx" != window.location.pathname && "/fanclub/" != window.location.pathname && "/fanclub/fan-club-log-in/" != window.location.pathname && "/fanclub/fan-club-log-in/default.aspx" != window.location.pathname && $("header").addClass("non-main"))
});
var LoggedInFlag = !0;
$("#header-lists #language a:first").attr("href", "#"), $(window).load(function() {
    $(".tabs").length > 0 && tabLogic()
});
var exploreRegionButton = null,
    currentMapSlideNum = 0,
    currentDetailPage = null;
$(document).ready(function() {
    window.activateMap = function() {
        $("#explore-map").delay(1e3).slideDown("normal", function() {
            $("#maptray").fadeIn(), $("#info .explore_tempting_offers").fadeIn()
        })
    }, window.setupRegionList = function() {
        $(".region-selector>li span").click(function() {
            if ($(this).parent("li").hasClass("active")) $(this).siblings("ul.location-list").slideUp(), $(this).parent("li").removeClass("active"), exploreRegionButton = null, changeMapSlides(0);
            else {
                null != exploreRegionButton && (exploreRegionButton.siblings("ul.location-list").slideUp(), exploreRegionButton.parent("li").removeClass("active")), $(this).parent("li").addClass("active"), $(this).siblings("ul.location-list").slideDown(), exploreRegionButton = $(this);
                var slideNum = parseInt($(this).attr("id").replace("explore-map-link-", ""));
                changeMapSlides(slideNum)
            }
        })
    }, window.setupMapSlides = function() {
        $(window).resize(function() {
            resetMapSlides()
        }), resetMapSlides(), $(".region-information .view-full").click(function() {
            $(".region-selector .active span").click()
        })
    }, window.setupMapLinks = function() {
        $("#explore-map-reel .explore-map-slide .explore-pin, .location-list a").click(function() {
            var cityClass = $(this).attr("class").replace("explore-pin", "").replace(/\s/, ""),
                detailPageClass = cityClass + "-detail";
            $("." + detailPageClass);
            null == currentDetailPage || currentDetailPage.hasClass(detailPageClass) ? showNewDetailPage(cityClass) : currentDetailPage.fadeOut("fast", function() {
                currentDetailPage = null, showNewDetailPage(cityClass)
            })
        }), $(".location-detail-single .close, .location-detail-multi .close").click(function() {
            hideCurrentDetailPage()
        }), $(".region-information-fade").click(function() {
            $(".region-information-fade").is(":visible") && ($(".region-information-fade").hide(), hideCurrentDetailPage())
        })
    }, window.showNewDetailPage = function(cityClass) {
        var detailPageClass = cityClass + "-detail",
            detailPage = $("." + detailPageClass);
        detailPage.length > 0 && ($(".location-list .active").removeClass("active"), $(".location-list a." + cityClass).parent().addClass("active"), $(".location-list a." + cityClass).parents(".location-list").parent().hasClass("active") || (setTimeout(function() {
            $(".location-list a." + cityClass).click()
        }, 500), $(".location-list a." + cityClass).parents(".location-list").siblings("span").click()), detailPage.fadeIn("fast"), currentDetailPage = detailPage, $(".region-information-fade").is(":visible") || $(".region-information-fade").fadeIn())
    }, window.hideCurrentDetailPage = function() {
        $(".location-list .active").removeClass("active"), null != currentDetailPage && currentDetailPage.fadeOut("fast"), $(".region-information-fade").is(":visible") && $(".region-information-fade").fadeOut()
    }, window.resetMapSlides = function() {
        var windowWidth = $(window).width(),
            rtlizer = function(ltr) {
                return "ar" != $("html").attr("lang") && "rtl" != $("html").attr("dir") ? ltr : ltr.match(/left/i) ? "right" : "left"
            };
        slideDir = rtlizer("left"), $("#explore-map-reel .explore-map-slide").width(windowWidth);
        var reelWidth = 0;
        $("#explore-map-reel .explore-map-slide").each(function() {
            reelWidth += parseInt($(this).outerWidth());
            var offset = parseInt($(this).find(".slide-container").width() / 2) - parseInt($(this).width() / 2);
            0 > offset && (offset = 0), $(this).find(".slide-container").css(slideDir, "-" + offset + "px")
        }), $("#explore-map-reel").width(reelWidth), changeMapSlides(currentMapSlideNum, !0)
    }, window.changeMapSlides = function(slideNum, instant) {
        0 == slideNum ? $(".region-information .view-full").hide() : $(".region-information .view-full").show();
        var newSlide = $("#explore-map-slide-" + slideNum);
        if (newSlide.length > 0) {
            var newLeft = newSlide.position().left;
            1 == instant ? $("#explore-map-reel").css("left", -1 * newLeft + "px") : $("#explore-map-reel").animate({
                left: -1 * newLeft
            }, 500)
        }
        currentMapSlideNum = slideNum
    }, window.openSetLoc = function() {
        var query = "loc";
        query = query.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var expr = "[\\?&]" + query + "=([^&#]*)",
            regex = new RegExp(expr),
            results = regex.exec(window.location.href);
        if (null !== results) {
            var locationToOpen = decodeURIComponent(results[1].replace(/\+/g, " "));
            locationToOpen = locationToOpen.toLowerCase(), $(".region-selector ." + locationToOpen + ":first").click()
        }
    }, setupRegionList(), setupMapSlides(), setupMapLinks(), activateMap(), openSetLoc()
})