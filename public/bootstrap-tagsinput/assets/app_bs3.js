var params = parseQueryScript('data-type','app-tags-input');

var citynames = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  prefetch: {
    cache   : false,
    url     : params.url + 'admin_manager_blog/get_tags',
    filter  : function(list) {
      return $.map(list, function(cityname) {
        return { name: cityname }; });
    }
  }
});

citynames.initialize();

/**
 * Typeahead
 */
var elt = $('#tags');
elt.tagsinput({
    typeaheadjs: {
        name: 'citynames',
        displayKey: 'name',
        valueKey: 'name',
        source: citynames.ttAdapter()
    }
});