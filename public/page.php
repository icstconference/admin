<?php
class controller_page extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    function UploadImage(array $data) {
        if ((($data["type"] == "image/gif") || ($data["type"] == "image/jpeg")
                || ($data["type"] == "image/jpg") || ($data["type"] == "image/pjpeg")
                || ($data["type"] == "image/x-png") || ($data["type"] == "image/png"))
            && ($data["size"] < 4000000)) {
            if ($data["error"] == 0) {
                $path = "public/upload/images/page/" . uniqid() . '.jpg';
                move_uploaded_file($data["tmp_name"], $path);
                return $path;
            }
        }
        return false;
    }

    function SaveThumbImage($path) {
        try {
            $thumb = PhpThumbFactory::create($path);
        } catch (Exception $e) {
            // handle error here however you'd like
        }
        $id=  uniqid();
        $name_file = $id.'.jpg';
        $thumb->resize(400);
        $thumb->adaptiveResize(300, 300);
        $thumb->save('public/upload/images/page/' . $name_file, 'jpg');

        $path_result = array(
            'image' => $path,
            'thumb' => 'public/upload/images/page/' . $name_file,
        );
        return $path_result;
    }

    protected function DeleteFile($file_path) {
        $path = str_replace(URL, '', $file_path);
        if (file_exists($path)) {
            unlink($path);
            return true;
        }
        return false;
    }

    public function index(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($create[0]['permission_detail'],'page') || !strpos($delete[0]['permission_detail'],'page') || !strpos($edit[0]['permission_detail'],'page')){
                header('Location:'.URL.'admin/');
            }
        }

    	$page = new model_page();

    	if($page->SelectAllPage()){
    		$this->view->listpage = $page->SelectAllPage();
    	}

    	$this->view->layout('layout-admin1');
        $this->view->Render('page/index');
    }

    public function updatepage(){
        require_once 'plugin/phpthumb/ThumbLib.inc.php';
        require_once 'plugin/url-seo/url-seo.php';
        $model = new model_page();
		
        if($_POST){
            $product_id 				 = $_POST['news_id'];
            $news_info 					 = $model->GetPageById($product_id);
            $product_name 				 = $_POST['news_name'];
            $product_name_english 		 = $_POST['news_name_english'];
            $product_description 		 = $_POST['news_description'];
            $product_description_english = $_POST['news_description_english'];
            $urlnews                     = explode('-',$news_info[0]['page_url']);
            $numarrnews                  = count($urlnews);
            $urlcode                     = $urlnews[$numarrnews-2];
            $news_url 		  			 = post_slug($product_name).'-page-'.$product_id;
            $news_url_english 			 = post_slug($product_name_english).'-page-'.$product_id;
            $page_type                   = $_POST['page_type'];

            if(strlen($_FILES['news_img']['name'])>1){
                $upload_img = $this->UploadImage($_FILES['news_img']);
                $path_save_img = $this->SaveThumbImage($upload_img);

                if($path_save_img){
                    $product_img = URL.$path_save_img['image'];
                    $this->DeleteFile($news_info[0]['page_thumbnail']);
                }else{
                    $product_img = $news_info[0]['page_thumbnail'];
                }
            }else{
                $product_img = $news_info[0]['page_thumbnail'];
            }

            $data = array(
                'page_name'          		=> addslashes($product_name),
                'page_name_english'  		=> addslashes($product_name_english),
                'page_description'   		=> $product_description,
                'page_description_english'  => $product_description_english,
                'page_thumbnail'   			=> $product_img,
                'page_url'           		=> $news_url,
                'page_type'                 => $page_type,
                'page_url_english'   		=> $news_url_english
            );

			//print_r($data);
			
            $result = $model->UpdatePage($data,$product_id);

            if($result){
                $log = new model_log();
                $arr = array(
                    'log_action'        => 'update',
                    'user_id'           => $_SESSION['user_id'],
                    'log_object'        => 'trang',
                    'log_object_id'     => $product_id,
                    'log_object_name'   => $product_name,
                    'user_name'         => $_SESSION['user_name']
                );
                $log->InsertLog($arr);
                echo '1';
            }
        }
    }

    public function createpage(){
        require_once 'plugin/phpthumb/ThumbLib.inc.php';
        require_once 'plugin/url-seo/url-seo.php';
        $model = new model_page();
        if($_POST){
            $product_name 				 = $_POST['news_name'];
            $product_name_english 		 = $_POST['news_name_english'];
            $product_description 		 = $_POST['news_description'];
            $product_description_english = $_POST['news_description_english'];
            $page_type                   = $_POST['page_type'];

            if(strlen($_FILES['news_img']['name'])>1){
                $upload_img = $this->UploadImage($_FILES['news_img']);
                $path_save_img = $this->SaveThumbImage($upload_img);

                if($path_save_img){
                    $product_img = URL.$path_save_img['image'];
                }else{
                    $product_img = '';
                }
            }else{
                $product_img = '';
            }

            $data = array(
                'page_name'          		=> addslashes($product_name),
                'page_name_english'  		=> addslashes($product_name_english),
                'page_description'   		=> $product_description,
                'page_description_english'  => $product_description_english,
                'page_thumbnail'   			=> $product_img,
                'page_type'                 => $page_type,
                'user_id'					=> $_SESSION['user_id']
            );

            $result = $model->InsertPage($data);
			
            if($result){
                $date                = date('dmY');
                $uniqueid            = uniqid();
                $product_url 		 = post_slug($product_name).'-page-'.$result;
                $product_url_english = post_slug($product_name_english).'-page-'.$result;
                $data1 = array(
                    'page_url' 			=> $product_url,
                    'page_url_english'  => $product_url_english
                );
				
                $update = $model->UpdatePage($data1,$result);
                if($update){
                    $log = new model_log();
                    $arr = array(
                        'log_action'        => 'create',
                        'user_id'           => $_SESSION['user_id'],
                        'log_object'        => 'trang',
                        'log_object_id'     => $result,
                        'log_object_name'   => $product_name,
                        'user_name'         => $_SESSION['user_name']
                    );
                    $log->InsertLog($arr);
                    echo 1;
                }
            }
        }
    }

    public function addpage(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($create[0]['permission_detail'],'page')){
                header('Location:'.URL.'admin/');
            }
        }

    	$page = new model_page();

    	$this->view->js = array(
    		URL.'public/ckeditor/ckeditor.js',
    		URL.'public/js/page.js'
    	);

    	$this->view->layout('layout-admin1');
        $this->view->Render('page/addpage');
    }

    public function edit($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($edit[0]['permission_detail'],'page')){
                header('Location:'.URL.'admin/');
            }
        }

    	$page = new model_page();

    	if($path){
    		if($page->GetPageById($path)){
    			$pageinfo = $page->GetPageById($path);

    			$this->view->pageinfo = $pageinfo;
    		}else{
    			header('Location:'.URL.'page');
    		}
    	}else{
    		header('Location:'.URL.'page');
    	}

    	$this->view->js = array(
    		URL.'public/ckeditor/ckeditor.js',
    		URL.'public/js/page.js'
    	);

    	$this->view->layout('layout-admin1');
        $this->view->Render('page/editpage');
    }

    public function delete($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($delete[0]['permission_detail'],'page')){
                header('Location:'.URL.'admin/');
            }
        }

    	$page = new model_page();

    	if($path){
    		if($page->GetPageById($path)){
    			$pageinfo = $page->GetPageById($path);

    			$deletepage = $page->DeletePageById($path);

    			if($deletepage){
    				$this->DeleteFile($pageinfo[0]['page_thumbnail']);
                    $log = new model_log();
                    $arr = array(
                        'log_action'        => 'delete',
                        'user_id'           => $_SESSION['user_id'],
                        'log_object'        => 'trang',
                        'log_object_id'     => $pageinfo[0]['page_id'],
                        'log_object_name'   => $pageinfo[0]['page_name'],
                        'user_name'         => $_SESSION['user_name']
                    );
                    $log->InsertLog($arr);
    				header('Location:'.URL.'page');
    			}
    		}else{
    			header('Location:'.URL.'page');
    		}
    	}else{
    		header('Location:'.URL.'page');
    	}
    }

    public function delete1(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($delete[0]['permission_detail'],'page')){
                header('Location:'.URL.'admin/');
            }
        }

        $page = new model_page();

        if($_POST){
            $idpost = $_POST['idpost'];
            if($page->GetPageById($idpost)){
                $pageinfo = $page->GetPageById($idpost);
                $deletepage = $page->DeletePageById($idpost);

                if($deletepage){
                    $this->DeleteFile($pageinfo[0]['page_thumbnail']);
                    $log = new model_log();
                    $arr = array(
                        'log_action'        => 'delete',
                        'user_id'           => $_SESSION['user_id'],
                        'log_object'        => 'trang',
                        'log_object_id'     => $pageinfo[0]['page_id'],
                        'log_object_name'   => $pageinfo[0]['page_name'],
                        'user_name'         => $_SESSION['user_name']
                    );
                    $log->InsertLog($arr);
                    echo '1';
                }
            }
        }
    }


    public function copy($path){
        require_once 'plugin/url-seo/url-seo.php';
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($create[0]['permission_detail'],'page') || !strpos($delete[0]['permission_detail'],'page') || !strpos($edit[0]['permission_detail'],'page')){
                header('Location:'.URL.'admin/');
            }
        }

        $page = new model_page();

        if($path){
            if($page->GetPageById($path)){
                $pageinfo = $page->GetPageById($path);

                $data = array(
                    'page_name'                 => $pageinfo[0]['page_name'],
                    'page_name_english'         => $pageinfo[0]['page_name_english'],
                    'page_description'          => $pageinfo[0]['page_description'],
                    'page_description_english'  => $pageinfo[0]['page_description_english'],
                    'page_thumbnail'            => $pageinfo[0]['page_thumbnail'],
                    'page_type'                 => $pageinfo[0]['page_type'],
                    'user_id'                   => $_SESSION['user_id']
                );

                $result = $page->InsertPage($data);
                
                if($result){
                    $product_url         = post_slug($pageinfo[0]['page_name']).'-page-'.$result;
                    $product_url_english = post_slug($pageinfo[0]['page_name_english']).'-page-'.$result;
                    $data1 = array(
                        'page_url'          => $product_url,
                        'page_url_english'  => $product_url_english
                    );
                    
                    $update = $page->UpdatePage($data1,$result);

                    $log = new model_log();
                    $arr = array(
                        'log_action'        => 'create',
                        'user_id'           => $_SESSION['user_id'],
                        'log_object'        => 'trang',
                        'log_object_id'     => $pageinfo[0]['page_id'],
                        'log_object_name'   => $pageinfo[0]['page_name'],
                        'user_name'         => $_SESSION['user_name']
                    );
                    $log->InsertLog($arr);
                    header('Location:'.URL.'page');
                }
            }else{
                header('Location:'.URL.'page');
            }
        }else{
            header('Location:'.URL.'page');
        }
    }

    public function detail($path){
        $page = new model_page();

        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $partner = new model_partner();

        $library = new model_library();
        
        $video = new model_video();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
            $this->view->configurl = $allsetting[0]['config_url'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOn()){
            $allmenufather = $model1->SelectAllMenuFatherOn();

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        if($path){
            $part = explode('_',$path);
            $id = $part[count($part)-1];

            if($page->GetPageById($id)){

                $pageinfo = $page->GetPageById($id);

                $this->view->pageinfo = $pageinfo;

                if($this->view->lang == 'vi'){
                    $this->view->title = $pageinfo[0]['page_name'].'-'.$allsetting[0]['config_title'];
                    $this->view->description = $pageinfo[0]['page_name'].'-'.$allsetting[0]['config_description'];
                    $this->view->keyword = $pageinfo[0]['page_name'].','.$allsetting[0]['config_keyword'];
                }else{
                    $this->view->title = $result[0]['page_name_english'].'-'.'Institute for Computational Science and Technology';
                    $this->view->description = $result[0]['page_name_english'].'-'.$allsetting[0]['config_description'];
                    $this->view->keyword = $result[0]['page_name_english'].','.$allsetting[0]['config_keyword'];
                }


                if($pageinfo[0]['page_type'] == 0){
                    $this->view->layout('layout');
                    $this->view->Render('page/detail');
                }elseif($pageinfo[0]['page_type'] == 1){
                    if($page->SelectAllPageByTypeId(1)){
                        $this->view->listpage = $page->SelectAllPageByTypeId(1);
                    }

                    $this->view->layout('layout');
                    $this->view->Render('intro/index');
                }elseif($pageinfo[0]['page_type'] == 2){
                    if($page->SelectAllPageByTypeId(2)){
                        $this->view->listpage = $page->SelectAllPageByTypeId(2);
                    }

                    $this->view->layout('layout');
                    $this->view->Render('intro/index');
                }elseif($pageinfo[0]['page_type'] == 3){
                    if($page->SelectAllPageByTypeId(2)){
                        $this->view->listpage = $page->SelectAllPageByTypeId(3);
                    }

                    $this->view->layout('layout');
                    $this->view->Render('intro/index');
                }
            }else{
                header('Location:'.URL);
            }
        }else{
            header('Location:'.URL);
        }

        
    }
}