function AllTables(){
    TestTable1();
    TestTable2();
    TestTable3();
    LoadSelect2Script(MakeSelect2);
}

function MakeSelect2(){
    $('.dataTables_filter').each(function(){
		if(lang === 'vi'){
			$(this).find('label input[type=text]').attr('placeholder', 'Tìm kiếm');
		}else{
			$(this).find('label input[type=text]').attr('placeholder', 'Search ');
		}
    });
	
	$('.pagination').each(function(){
		//console.log($(this).find('li.prev.disabled'));
		$(this).find('li.prev.disabled a').html('&#8592;');
		$(this).find('li.next.disabled a').html('&#8594;');
	});
}

$(document).ready(function() {
    LoadDataTablesScripts(AllTables);
    // Add Drag-n-Drop feature
    WinMove();

});