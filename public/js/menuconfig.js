(function( $ ) {

	/*
	Nestable 1
	*/
	$('#nestable').nestable({
		maxDepth		:2,
		threshold       : 20,
		onDragFinished	:function(currentNode, parentNode) { 
			var parentelement = parentNode;
			var currentelement = currentNode;

			if(parentelement != null){
				var dditem = $(parentelement).filter(".dd-item");
				var dataid = dditem.attr('data-id');

				var dditem1 = $(currentelement).filter(".dd-item");
				var dataid1 = dditem1.attr('data-id');

				//console.log(dataid);

				$.ajax({
			        url: URL_G+lang+'/menu/updatemenu/',  //Server script to process data
			        type: 'POST',
			        data: 'menuid='+dataid1+'&parentid='+dataid,
			        success:function(g){
			           //console.log(g);
			        }
			    });
			}else{
				var dditem1 = $(currentelement).filter(".dd-item");
				var dataid1 = dditem1.attr('data-id');

				//console.log(0);

				$.ajax({
			        url: URL_G+lang+'/menu/updatemenu/',  //Server script to process data
			        type: 'POST',
			        data: 'menuid='+dataid1+'&parentid=0',
			        success:function(g){
			           //console.log(g); 
			        }
			    });
			}

			var listitem = $('.dd').nestable('serialize');

			var listitemjson = JSON.stringify(listitem);

			$.ajax({
				url: URL_G+lang+'/menu/updatemenusort/',  //Server script to process data
			    type: 'POST',
			    data: 'listmenu='+listitemjson,
			    success:function(g){
			    	console.log(g);
			   	}
			});
		}
	});


}).apply(this, [ jQuery ]);