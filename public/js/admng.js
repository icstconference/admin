function AllTables(){
    TestTable1();
    TestTable2();
    TestTable3();
    LoadSelect2Script(MakeSelect2);
}
function MakeSelect2(){
    $('select').select2();
    $('.dataTables_filter').each(function(){
        $(this).find('label input[type=text]').attr('placeholder', 'Search');
    });
}

$(document).ready(function() {
    LoadDataTablesScripts(AllTables);
    // Add Drag-n-Drop feature
    WinMove();

    $(':file').change(function(){
        var file = this.files[0];
        var name = file.name;
        var size = file.size;
        var type = file.type;
        //Your validation
    });

    $("#hinh_img").change(function(){
        $("#hinhimg").show();
        readURL(this,'hinhimg');
    });

    $("#hinhimg").click(function(){
        $("#hinh_img").trigger('click');
    });


    $("#submit").click(function(){
        var formData = new FormData($('form')[0]);
        $.ajax({
                url: URL_G+'advertise/createadvertise/',  //Server script to process data
                type: 'POST',
                xhr: function() {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // Check if upload property exists
                        $("#progressbar").show();
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                    }
                    return myXhr;
                },
                // Form data
                data: formData,
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false,
                success:function(g){
                    if(g == 1){
                        top.location.href = URL_G+'advertise';
                    }
                }
            });
    });

    $("#update").click(function(){
        var formData = new FormData($('form')[0]);
        $.ajax({
                url: URL_G+'advertise/updateadvertise/',  //Server script to process data
                type: 'POST',
                xhr: function() {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // Check if upload property exists
                        $("#progressbar").show();
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                    }
                    return myXhr;
                },
                // Form data
                data: formData,
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false,
                success:function(g){
                    if(g == 1){
                        top.location.href = URL_G+'advertise';
                    }
                }
        });
    });
});

function progressHandlingFunction(e){
    if(e.lengthComputable){
        $('progress').attr({value:e.loaded,max:e.total});
    }
}

function readURL(input,container) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#'+container).attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
