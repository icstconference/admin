function AllTables(){
    TestTable1();
    TestTable2();
    TestTable3();
    LoadSelect2Script(MakeSelect2);
}
function MakeSelect2(){
    $('select').select2();
    $('.dataTables_filter').each(function(){
        $(this).find('label input[type=text]').attr('placeholder', 'Search');
    });
}

$(document).ready(function () {
    CKEDITOR.config.removePlugins = 'elementspath';
    CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
    CKEDITOR.config.filebrowserBrowseUrl         = '<?= URL?>public/ckfinder/ckfinder.html';
    CKEDITOR.config.filebrowserImageBrowseUrl    = '<?= URL?>public/ckfinder/ckfinder.html?type=Images';
    CKEDITOR.config.filebrowserFlashBrowseUrl    = '<?= URL?>public/ckfinder/ckfinder.html?type=Flash';
    CKEDITOR.config.filebrowserUploadUrl         = '<?= URL?>public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
    CKEDITOR.config.filebrowserImageUploadUrl    = '<?= URL?>public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
    CKEDITOR.config.filebrowserFlashUploadUrl    = '<?= URL?>public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';

    CKEDITOR.replace('wysiwig_full', {
        height: 200
    });
});

$(document).ready(function() {
    LoadDataTablesScripts(AllTables);
    WinMove();

    $(':file').change(function(){
        var file = this.files[0];
        var name = file.name;
        var size = file.size;
        var type = file.type;
        //Your validation
    });

    $("#product_img").change(function(){
        $("#productimgfront").show();
        readURL(this,'productimgfront');
    });

    $("#productimgfront").click(function(){
        $("#product_img").trigger('click');
    });

    $("#product_img_behind").change(function(){
        $("#productimgbehind").show();
        readURL(this,'productimgbehind');
    });

    $("#productimgbehind").click(function(){
        $("#product_img_behind").trigger('click');
    });

    //
    $("#product_img1").change(function(){
        $("#productimg1").show();
        readURL(this,'productimg1');
    });

    $("#productimg1").click(function(){
        $("#product_img1").trigger('click');
    });

    $("#product_img2").change(function(){
        $("#productimg2").show();
        readURL(this,'productimg2');
    });

    $("#productimg2").click(function(){
        $("#product_img2").trigger('click');
    });

    $("#product_img3").change(function(){
        $("#productimg3").show();
        readURL(this,'productimg3');
    });

    $("#productimg3").click(function(){
        $("#product_img3").trigger('click');
    });

    $("#product_img4").change(function(){
        $("#productimg4").show();
        readURL(this,'productimg4');
    });

    $("#productimg4").click(function(){
        $("#product_img4").trigger('click');
    });

    //

    $("#product_imgedit1").change(function(){
        var formData = new FormData($('form')[0]);
        var imgproid = $(this).attr('rel');
        $.ajax({
            url: URL_G+'product/changeimage/',
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success:function(g){
                if(g){
                    $("#productimg1").show();
                    $("#productimgedit1").attr("src",g);
                    $("#deleteimageurl[name=deletebutton1]").hide();
                }
            }
        });
    });

    $("#productimgedit1").click(function(){
        $("#product_imgedit1").trigger('click');
    });

    $("#product_imgedit2").change(function(){
        var formData = new FormData($('form')[0]);
        var imgproid = $(this).attr('rel');
        $.ajax({
            url: URL_G+'product/changeimage/',
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success:function(g){
                if(g){
                    $("#productimg2").show();
                    $("#productimgedit2").attr("src",g);
                    $("#deleteimageurl[name=deletebutton2").hide();
                }
            }
        });
    });

    $("#productimgedit2").click(function(){
        $("#product_imgedit2").trigger('click');
    });

    $("#product_imgedit3").change(function(){
        var formData = new FormData($('form')[0]);
        var imgproid = $(this).attr('rel');
        $.ajax({
            url: URL_G+'product/changeimage/',
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success:function(g){
                if(g){
                    $("#productimg3").show();
                    $("#productimgedit3").attr("src",g);
                    $("#deleteimageurl[name=deletebutton3]").hide();
                }
            }
        });
    });

    $("#productimgedit3").click(function(){
        $("#product_imgedit3").trigger('click');
    });

    $("#product_imgedit4").change(function(){
        var formData = new FormData($('form')[0]);
        var imgproid = $(this).attr('rel');
        $.ajax({
            url: URL_G+'product/changeimage/',
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success:function(g){
                if(g){
                    $("#productimg4").show();
                    $("#productimgedit4").attr("src",g);
                    $("#deleteimageurl[name=deletebutton4]").hide();
                }
            }
        });
    });

    $("#productimgedit4").click(function(){
        $("#product_imgedit4").trigger('click');
    });


    $("#submit").click(function(){
        var value = CKEDITOR.instances.wysiwig_full.getData();
        $("#wysiwig_full").text(value);
        var product_name = $("#product_name").val();
        if(product_name.length >= 1){
            $("#progressbar").show();
            var formData = new FormData($('form')[0]);
            $.ajax({
                url: URL_G+'product/createproduct/',  //Server script to process data
                type: 'POST',
                xhr: function() {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // Check if upload property exists
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                    }
                    return myXhr;
                },
                // Form data
                data: formData,
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false,
                success:function(g){
                    if(g == 1){
                        top.location.href = URL_G+'product';
                    }
                }
            });
        }else{
            $("#error").html('* Tên sản phẩm không được bỏ trống');
        }
    });

    $("#update").click(function(){
        var value = CKEDITOR.instances.wysiwig_full.getData();
        $("#wysiwig_full").text(value);
        var product_name = $("#product_name").val();
        if(product_name.length >= 1){
            var formData = new FormData($('form')[0]);
            $.ajax({
                url: URL_G+'product/updateproduct/',  //Server script to process data
                type: 'POST',
                xhr: function() {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // Check if upload property exists
                        $("#progressbar").show();
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                    }
                    return myXhr;
                },
                // Form data
                data: formData,
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false,
                success:function(g){
                    if(g){
                        top.location.href = URL_G+'product';
                    }
                }
            });
        }else{
            $("#error").html('* Tên sản phẩm không được bỏ trống');
        }
    });

    $("body").on('click','#deleteimageurl',function(){
        var id = $(this).attr('rel');
        var imageurl = $(this).attr('src');
        var idproimg = $(this).attr('rel1');

        $.ajax({
            url: URL_G+'product/deleteimage/',  //Server script to process data
            type: 'POST',
            data: 'idimage='+id+'&urlimage='+imageurl,
            success: function(g){
                if(g == 1){
                    $("#"+idproimg).attr('src',URL_G+'public/img/noneimage.jpg');
                    $("#deleteimageurl").hide();
                }
            }
        });
    });
});

function progressHandlingFunction(e){
    if(e.lengthComputable){
        $('progress').attr({value:e.loaded,max:e.total});
    }
}

function readURL(input,container) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#'+container).attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
