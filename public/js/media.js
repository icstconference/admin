(function( $ ) {

	/*
	Thumbnail: Select
	*/
	$('.mg-option input[type=checkbox]').on('change', function( ev ) {
		var wrapper = $(this).parents('.thumbnail');
		if($(this).is(':checked')) {
			wrapper.addClass('thumbnail-selected');
		} else {
			wrapper.removeClass('thumbnail-selected');
		}
	});

	$('.mg-option input[type=checkbox]:checked').trigger('change');

	/*
	Toolbar: Select All
	*/
	$('#mgSelectAll').on('click', function( ev ) {
		ev.preventDefault();
		var $this = $(this),
			$label = $this.find('> span');
			$checks = $('.mg-option input[type=checkbox]');

		if($this.attr('data-all-selected')) {
			$this.removeAttr('data-all-selected');
			$checks.prop('checked', false).trigger('change');
			$label.html($label.data('all-text'));
		} else {
			$this.attr('data-all-selected', 'true');
			$checks.prop('checked', true).trigger('change');
			$label.html($label.data('none-text'));
		}
	});

}(jQuery));

$(document).ready(function () {
    Dropzone.autoDiscover = false;
    //Simple Dropzonejs 
    $("#dropzone-example1").dropzone({
        url: URL_G+"news/createmedia/",
        addRemoveLinks: true,
        previewsContainer: '#zoneimg',
        success: function (file,response) {
            var obj = jQuery.parseJSON(response);
            idimage = obj.image_id;
            return file.previewElement.classList.add("dz-success");
        },
        removedfile: function(file) {
            var name = file.name;  
            $.ajax({
                url: URL_G+lang+'/news/deletemedia/',
                data: 'id='+name,
                type: 'post',
                success:function(g){
                    //console.log(g);
                    var _ref;
                    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                }
            });
        }
    });

});