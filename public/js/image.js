$(document).ready(function () {
    Dropzone.autoDiscover = false;
    //Simple Dropzonejs 
    $("#dropzone-example").dropzone({
        url: URL_G+lang+"/library/createlibrary/",
        addRemoveLinks: true,
        previewsContainer: '#zoneimg',
        success: function (file,response) {
            var obj = jQuery.parseJSON(response);
            idimage = obj.image_id;
            return file.previewElement.classList.add("dz-success");
        },
        removedfile: function(file) {
            var name = file.name;  
            $.ajax({
                url: URL_G+'library/deletelibrary1/',
                data: 'id='+name,
                type: 'post',
                success:function(g){
                    //console.log(g);
                    var _ref;
                    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                }
            });
        }
    });

});