function AllTables(){
    TestTable1();
    TestTable2();
    TestTable3();
    LoadSelect2Script(MakeSelect2);
}
function MakeSelect2(){
    $('select').select2();
    $('.dataTables_filter').each(function(){
        $(this).find('label input[type=text]').attr('placeholder', 'Search');
    });
}

var pagecheckall = 0;
var newcheckall = 0;
var categorycheckall = 0;

$(document).ready(function() {
	$("#selectallpage").click(function(){
		pagecheckall++;
		if(pagecheckall == 1){
			$(".page").prop('checked', true);
			$(this).attr('class','mb-xs mt-xs mr-xs btn btn-warning');
			$(this).text('Bỏ chọn tất cả');
		}else if (pagecheckall == 2) {
			pagecheckall = 0;
			$(".page").prop('checked', false);
			$(this).attr('class','mb-xs mt-xs mr-xs btn btn-primary');
			$(this).text('Chọn tất cả');
		};
	});

	$("#selectallnew").click(function(){
		newcheckall++;
		if(newcheckall == 1){
			$(".news").prop('checked', true);
			$(this).attr('class','mb-xs mt-xs mr-xs btn btn-warning');
			$(this).text('Bỏ chọn tất cả');
		}else if (newcheckall == 2) {
			newcheckall = 0;
			$(".news").prop('checked', false);
			$(this).attr('class','mb-xs mt-xs mr-xs btn btn-primary');
			$(this).text('Chọn tất cả');
		};
	});

	$("#selectallcategory").click(function(){
		categorycheckall++;
		if(categorycheckall == 1){
			$(".categorynews").prop('checked', true);
			$(this).attr('class','mb-xs mt-xs mr-xs btn btn-warning');
			$(this).text('Bỏ chọn tất cả');
		}else if (categorycheckall == 2) {
			categorycheckall = 0;
			$(".categorynews").prop('checked', false);
			$(this).attr('class','mb-xs mt-xs mr-xs btn btn-primary');
			$(this).text('Chọn tất cả');
		};
	});

	$("#addmenupage").click(function(){
        var formData = new FormData($('form')[0]);
        $.ajax({
            url: URL_G+'menu/createpage/',  //Server script to process data
            type: 'POST',
            data: formData,
            //Options to tell jQuery not to process data or worry about content-type.
            cache: false,
            contentType: false,
            processData: false,
            success:function(g){
                if(g){
                	$('#list').append(g);
                	pagecheckall = 0;
                	$(".page").prop('checked', false);
                	$("#selectallpage").attr('class','mb-xs mt-xs mr-xs btn btn-primary');
					$("#selectallpage").text('Chọn tất cả');
                }
            }
        });
	});

	$("#addmenunews").click(function(){
        var formData = new FormData($('form')[1]);
        $.ajax({
            url: URL_G+'menu/createnews/',  //Server script to process data
            type: 'POST',
            data: formData,
            //Options to tell jQuery not to process data or worry about content-type.
            cache: false,
            contentType: false,
            processData: false,
            success:function(g){
                if(g){
                	$('#list').append(g);
                	newcheckall = 0;
					$(".news").prop('checked', false);
					$("#selectallnew").attr('class','mb-xs mt-xs mr-xs btn btn-primary');
					$("#selectallnew").text('Chọn tất cả');
                }
            }
        });
	});

	$("#addmenucategory").click(function(){
        var formData = new FormData($('form')[2]);
        $.ajax({
            url: URL_G+'menu/createnewscategory/',  //Server script to process data
            type: 'POST',
            data: formData,
            //Options to tell jQuery not to process data or worry about content-type.
            cache: false,
            contentType: false,
            processData: false,
            success:function(g){
                if(g){
                	$('#list').append(g);
                	categorycheckall = 0;
					$(".categorynews").prop('checked', false);
					$("#selectallcategory").attr('class','mb-xs mt-xs mr-xs btn btn-primary');
					$("#selectallcategory").text('Chọn tất cả');
                }
            }
        });
	});

	$("#addcustomenu").click(function(){
		var menuname = $("#menu_name").val();
		var menuurl  = $("#menu_link").val();

		$.ajax({
            url: URL_G+'menu/createcustom/',  //Server script to process data
            type: 'POST',
            data: 'menu_name='+menuname+'&menu_url='+menuurl,
            success:function(g){
                if(g){
                	$('#list').append(g);
                	$("#menu_link").val();
                	$("#menu_name").val();
                }
            }
        });
	});

	$(".dd a").on("mousedown", function(event) { // mousedown prevent nestable click
        event.preventDefault();
        return false;
    });

	function update(id){
		var menuname = $('#menu_name'+id).val();
    	var menunameeg = $('#menu_name_english'+id).val();
    	$.ajax({
            url: URL_G+'menu/updatemenuselect/',  //Server script to process data
            type: 'POST',
            data: 'menu_id='+id+'&menu_name='+menuname+'&menu_name_english='+menunameeg,
            success:function(g){
            	//console.log(g);
                if(g == 1){
                	window.location.href = URL_G+'menu';
                }
            }
        });
	}

    $("body").on('click','#update',function(){
    	var id 		 = $(this).attr('rel');
    	var menuname = $('#menu_name'+id).val();
    	var menunameeg = $('#menu_name_english'+id).val();
    	$.ajax({
            url: URL_G+'menu/updatemenuselect/',  //Server script to process data
            type: 'POST',
            data: 'menu_id='+id+'&menu_name='+menuname+'&menu_name_english='+menunameeg,
            success:function(g){
            	//console.log(g);
                if(g == 1){
                	window.location.href = URL_G+'menu';
                }
            }
        });
    });

    $("body").on('click','#updatecustom',function(){
    	var id 		 = $(this).attr('rel');
    	var menuname = $('#menu_name'+id).val();
    	var menunameeg = $('#menu_name_english'+id).val();
    	var menunameurl = $('#menu_url'+id).val();
    	$.ajax({
            url: URL_G+'menu/updatemenucustom/',  //Server script to process data
            type: 'POST',
            data: 'menu_id='+id+'&menu_name='+menuname+'&menu_name_english='+menunameeg+'&menu_url='+menunameurl,
            success:function(g){
            	console.log(g);
                if(g == 1){
                	window.location.href = URL_G+'menu';
                }
            }
        });
    });

    $("body").on('click','#delete',function(){
    	var id 		 = $(this).attr('rel');
    	$.ajax({
            url: URL_G+'menu/delete/',  //Server script to process data
            type: 'POST',
            data: 'menu_id='+id,
            success:function(g){
            	//console.log(g);
                if(g){
                	window.location.href = URL_G+'menu';
                }
            }
        });
    });
});
