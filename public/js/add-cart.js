var tmpsize = 'man';
var size = '';
var typesize = $("#size-type").val();

$("#size-type").click(function(g){
	typesize = $(this).val();

	if(typesize == 'nam'){
		if(tmpsize){
			$("#"+tmpsize).hide();
			$("#man").show();
			tmpsize = 'man';
			size = $('#manclothes:checked').val();
		}
	}

	if(typesize == 'nu'){
		if(tmpsize){
			$("#"+tmpsize).hide();
			$("#woman").show();
			tmpsize = 'woman';
			size = $('#womanclothes:checked').val();
		}
	}

	if(typesize == 'embe'){
		if(tmpsize){
			$("#"+tmpsize).hide();
			$("#baby").show();
			tmpsize = 'baby';
			size = $('#babyclothes:checked').val();
		}
	}
});

$('input[name="womanclothes"]').click(function() {
    $('#manclothes:checked').attr('checked',false);
    $('#babyclothes:checked').attr('checked',false);
});

$('input[name="manclothes"]').click(function() {
    $('#womanclothes:checked').attr('checked',false);
    $('#babyclothes:checked').attr('checked',false);
});

$('input[name="babyclothes"]').click(function() {
    $('#womanclothes:checked').attr('checked',false);
    $('#manclothes:checked').attr('checked',false);
});

$("body").on('click','#removecard',function(g){
	var id = $(this).attr('rel');
	var keytoken = $(this).attr('rel1');
	$.ajax({
		url:URL_G+'product/deleteproductcart/',
		type:'POST',
		data:'idproduct='+id+'&key='+keytoken,
		success:function(g){
			if(g == 1){
				top.location.href = URL_G+'cart';
			}
		}
	});
});

function deletequantity(id){
	$.ajax({
		url:URL_G+'product/deletequantity/',
		type:'POST',
		data:'idproduct='+id,
		success:function(g){
			$("#carttable").empty();
			$("#carttable").append(g);
			if(($("#quantityy").text()).length > 0)
			{
				$("#quantity").empty();
				$("#quantity").append($("#quantityy").text());
				$("#summoney").empty();
				$("#summoney").append($("#summoneyy").text());
				$("#summoney1").empty();
				$("#summoney1").val($("#summoneyy").text());
			}else{
				$("#quantity").empty();
				$("#quantity").append('0 VNĐ');
				$("#summoney").empty();
				$("#summoney").append('0 Sản phẩm');
				$("#summoney1").empty();
				$("#summoney1").val(0);
			}
		}
	});
};

function addquantity(id){
	$.ajax({
		url:URL_G+'product/addquantity/',
		type:'POST',
		data:'idproduct='+id,
		success:function(g){
			$("#carttable").empty();
			$("#carttable").append(g);
			if(($("#quantityy").text()).length > 0)
			{
				$("#quantity").empty();
				$("#quantity").append($("#quantityy").text());
				$("#summoney").empty();
				$("#summoney").append($("#summoneyy").text());
				$("#summoney1").empty();
				$("#summoney1").val($("#summoneyy").text());
			}else{
				$("#quantity").empty();
				$("#quantity").append('0 VNĐ');
				$("#summoney").empty();
				$("#summoney").append('0 Sản phẩm');
				$("#summoney1").empty();
				$("#summoney1").val(0);
			}
		}
	});
};

function addquantitybyvalue(id,value,key){
	$.ajax({
		url:URL_G+'product/addquantitybyvalue/',
		type:'POST',
		data:'idproduct='+id+'&value='+value+'&key='+key,
		success:function(g){
			if(g == 1){
				top.location.href = URL_G+'cart';
			}
		}
	});
}