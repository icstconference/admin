function AllTables(){
    TestTable1();
    TestTable2();
    TestTable3();
    LoadSelect2Script(MakeSelect2);
}

function MakeSelect2(){
    $('select').select2();
    $('.dataTables_filter').each(function(){
        $(this).find('label input[type=text]').attr('placeholder', 'Tim kiem');
    });
}

$(document).ready(function() {

});

$("body").on("click","#detail",function(){
    var id = $(this).attr("rel");

    $.ajax({
        url:URL_G+'comment/detail/',
        type:'POST',
        data:'idorder='+id,
        success:function(g){
            $("#cartdetail").empty();
            $("#cartdetail").append(g);
        }
    });
});