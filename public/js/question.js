function AllTables(){
    TestTable1();
    TestTable2();
    TestTable3();
    LoadSelect2Script(MakeSelect2);
}

function MakeSelect2(){
    $('select').select2();
    $('.dataTables_filter').each(function(){
        $(this).find('label input[type=text]').attr('placeholder', 'Search');
    });
}

$(document).ready(function () {
    CKEDITOR.config.removePlugins = 'elementspath';
    CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
    CKEDITOR.config.filebrowserBrowseUrl         = '<?= URL?>public/ckfinder/ckfinder.html';
    CKEDITOR.config.filebrowserImageBrowseUrl    = '<?= URL?>public/ckfinder/ckfinder.html?type=Images';
    CKEDITOR.config.filebrowserFlashBrowseUrl    = '<?= URL?>public/ckfinder/ckfinder.html?type=Flash';
    CKEDITOR.config.filebrowserUploadUrl         = '<?= URL?>public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
    CKEDITOR.config.filebrowserImageUploadUrl    = '<?= URL?>public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
    CKEDITOR.config.filebrowserFlashUploadUrl    = '<?= URL?>public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';

    if($('#questionpage').attr('rel')){
        CKEDITOR.replace('wysiwig_full', {
            height: 200
        });
    }
});

$(document).ready(function() {
    LoadDataTablesScripts(AllTables);
    // Add Drag-n-Drop feature
    WinMove();
});