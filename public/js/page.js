$(document).ready(function () {
    CKEDITOR.config.removePlugins = 'elementspath';
	CKEDITOR.config.extraPlugins = 'youtube';
    CKEDITOR.config.extraPlugins = 'embed';
    CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
    CKEDITOR.config.filebrowserBrowseUrl         = URL_G+'public/ckfinder/ckfinder.html';
    CKEDITOR.config.filebrowserImageBrowseUrl    = URL_G+'public/ckfinder/ckfinder.html?type=Images';
    CKEDITOR.config.filebrowserFlashBrowseUrl    = URL_G+'public/ckfinder/ckfinder.html?type=Flash';
    CKEDITOR.config.filebrowserUploadUrl         = URL_G+'public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
    CKEDITOR.config.filebrowserImageUploadUrl    = URL_G+'public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
    CKEDITOR.config.filebrowserFlashUploadUrl    = URL_G+'public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';

    CKEDITOR.replace('wysiwig_full1', {
        height: 200
    });

    CKEDITOR.replace('wysiwig_full2', {
        height: 200
    });
});

$(document).ready(function() {

    $('.cke_inner').css('background','#FAFAFA');

    $(':file').change(function(){
        var file = this.files[0];
        var name = file.name;
        var size = file.size;
        var type = file.type;
        //Your validation
    });

    $("#news_img").change(function(){
        $("#newsimg").show();
        readURL(this,'newsimg');
    });

    $("#newsimg").click(function(){
        $("#news_img").trigger('click');
    });


    $("#submit").click(function(){
        var value = CKEDITOR.instances.wysiwig_full1.getData();
        $("#wysiwig_full1").text(value);
        var value1 = CKEDITOR.instances.wysiwig_full2.getData();
        $("#wysiwig_full2").text(value1);
        var news_name = $("#news_name").val();
        if(news_name.length >= 1){
            var formData = new FormData($('form')[0]);
            $.ajax({
                url: URL_G+'page/createpage/',  //Server script to process data
                type: 'POST',
                xhr: function() {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // Check if upload property exists
                        $("#progressbar").show();
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                    }
                    return myXhr;
                },
                // Form data
                data: formData,
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false,
                success:function(g){
                    //console.log(g);
                    if(g == 1){
                        top.location.href = URL_G+'page';
                    }
                }
            });
        }else{
            $("#error").html('* Tên sản phẩm không được bỏ trống');
        }
    });

    $("#update").click(function(){
        var value = CKEDITOR.instances.wysiwig_full1.getData();
        $("#wysiwig_full1").text(value);
        var value1 = CKEDITOR.instances.wysiwig_full2.getData();
        $("#wysiwig_full2").text(value1);
        var news_name = $("#news_name").val();
        if(news_name.length >= 1){
            var formData = new FormData($('form')[0]);
            $.ajax({
                url: URL_G+'page/updatepage/',  //Server script to process data
                type: 'POST',
                xhr: function() {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // Check if upload property exists
                        $("#progressbar").show();
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                    }
                    return myXhr;
                },
                // Form data
                data: formData,
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false,
                success:function(g){
					//console.log(g);
                    if(g == 1){
                        top.location.href = URL_G+'page';
                    }
                }
            });
        }else{
            $("#error").html('* Tên sản phẩm không được bỏ trống');
        }
    });
});

function progressHandlingFunction(e){
    if(e.lengthComputable){
        $('progress').attr({value:e.loaded,max:e.total});
    }
}

function readURL(input,container) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#'+container).attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
