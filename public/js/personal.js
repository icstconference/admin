function AllTables(){
    TestTable1();
    TestTable2();
    TestTable3();
    LoadSelect2Script(MakeSelect2);
}
function MakeSelect2(){
    $('select').select2();
    $('.dataTables_filter').each(function(){
        $(this).find('label input[type=text]').attr('placeholder', 'Search');
    });
}

$(document).ready(function () {
    CKEDITOR.config.removePlugins = 'elementspath';
    CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
    CKEDITOR.config.filebrowserBrowseUrl         = '<?= URL?>public/ckfinder/ckfinder.html';
    CKEDITOR.config.filebrowserImageBrowseUrl    = '<?= URL?>public/ckfinder/ckfinder.html?type=Images';
    CKEDITOR.config.filebrowserFlashBrowseUrl    = '<?= URL?>public/ckfinder/ckfinder.html?type=Flash';
    CKEDITOR.config.filebrowserUploadUrl         = '<?= URL?>public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
    CKEDITOR.config.filebrowserImageUploadUrl    = '<?= URL?>public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
    CKEDITOR.config.filebrowserFlashUploadUrl    = '<?= URL?>public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';

    CKEDITOR.replace('wysiwig_full', {
        height: 200
    });

    CKEDITOR.replace('wysiwig_full1', {
        height: 200
    });
});

$(document).ready(function() {
    LoadDataTablesScripts(AllTables);
    // Add Drag-n-Drop feature
    WinMove();

    $(':file').change(function(){
        var file = this.files[0];
        var name = file.name;
        var size = file.size;
        var type = file.type;
        //Your validation
    });

    $("#personal_img").change(function(){
        $("#personalimg").show();
        readURL(this,'personalimg');
    });

    $("#personalimg").click(function(){
        $("#personal_img").trigger('click');
    });

    $("#personal_lab_img").change(function(){
        $("#personal_labimg").show();
        readURL(this,'personal_labimg');
    });

    $("#personal_labimg").click(function(){
        $("#personal_lab_img").trigger('click');
    });

    $("#submit").click(function(){
        var value = CKEDITOR.instances.wysiwig_full.getData();
        $("#wysiwig_full").text(value);
        var value = CKEDITOR.instances.wysiwig_full1.getData();
        $("#wysiwig_full1").text(value);
        var news_name = $("#personal_name").val();
        if(news_name.length >= 1){
            var formData = new FormData($('form')[0]);
            $.ajax({
                url: URL_G+lang+'/personal/createpersonal/',  //Server script to process data
                type: 'POST',
                xhr: function() {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // Check if upload property exists
                        $("#progressbar").show();
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                    }
                    return myXhr;
                },
                // Form data
                data: formData,
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false,
                success:function(g){
                    console.log(g);
                    if(g == 1){
                        top.location.href = URL_G+'personal';
                    }
                }
            });
        }else{
            $("#error").html('* Tên sản phẩm không được bỏ trống');
        }
    });

    $("#update").click(function(){
        var value = CKEDITOR.instances.wysiwig_full.getData();
        $("#wysiwig_full").text(value);
        var value = CKEDITOR.instances.wysiwig_full1.getData();
        $("#wysiwig_full1").text(value);
        var news_name = $("#personal_name").val();
        if(news_name.length >= 1){
            var formData = new FormData($('form')[0]);
            $.ajax({
                url: URL_G+lang+'/personal/updatepersonal/',  //Server script to process data
                type: 'POST',
                xhr: function() {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // Check if upload property exists
                        $("#progressbar").show();
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                    }
                    return myXhr;
                },
                // Form data
                data: formData,
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false,
                success:function(g){
                    if(g == 1){
                        top.location.href = URL_G+'personal';
                    }
                }
            });
        }else{
            $("#error").html('* Tên sản phẩm không được bỏ trống');
        }
    });

    $("#submit1").click(function(){
        var value = CKEDITOR.instances.wysiwig_full.getData();
        $("#wysiwig_full").text(value);
        var value = CKEDITOR.instances.wysiwig_full1.getData();
        $("#wysiwig_full1").text(value);
        var news_name = $("#personal_lab_name").val();
        if(news_name.length >= 1){
            var formData = new FormData($('form')[0]);
            $.ajax({
                url: URL_G+lang+'/personal/createpersonal1/',  //Server script to process data
                type: 'POST',
                xhr: function() {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // Check if upload property exists
                        $("#progressbar").show();
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                    }
                    return myXhr;
                },
                // Form data
                data: formData,
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false,
                success:function(g){
                    console.log(g);
                    if(g == 1){
                        top.location.href = URL_G+'personal/lab/';
                    }
                }
            });
        }else{
            $("#error").html('* Tên sản phẩm không được bỏ trống');
        }
    });

    $("#update1").click(function(){
        var value = CKEDITOR.instances.wysiwig_full.getData();
        $("#wysiwig_full").text(value);
        var value = CKEDITOR.instances.wysiwig_full1.getData();
        $("#wysiwig_full1").text(value);
        var news_name = $("#personal_lab_name").val();
        if(news_name.length >= 1){
            var formData = new FormData($('form')[0]);
            $.ajax({
                url: URL_G+lang+'/personal/updatepersonal1/',  //Server script to process data
                type: 'POST',
                xhr: function() {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // Check if upload property exists
                        $("#progressbar").show();
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                    }
                    return myXhr;
                },
                // Form data
                data: formData,
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false,
                success:function(g){
                    console.log(g);
                    // if(g == 1){
                    //     top.location.href = URL_G+'personal/lab/';
                    // }
                }
            });
        }else{
            $("#error").html('* Tên sản phẩm không được bỏ trống');
        }
    });
});

function progressHandlingFunction(e){
    if(e.lengthComputable){
        $('progress').attr({value:e.loaded,max:e.total});
    }
}

function readURL(input,container) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#'+container).attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
