function AllTables(){
    TestTable1();
    TestTable2();
    TestTable3();
    LoadSelect2Script(MakeSelect2);
}

function MakeSelect2(){
    $('select').select2();
    $('.dataTables_filter').each(function(){
        $(this).find('label input[type=text]').attr('placeholder', 'Search');
    });
}

var configkey = 'AIzaSyAju4Ar20Z20DPiOhHCst21S3md2pc1MSg';

$(document).ready(function(){
	LoadDataTablesScripts(AllTables);
    // Add Drag-n-Drop feature
    WinMove();

	$("#youtube_url").keyup(function(g){
		var linkyt = $(this).val();
		var video_id = linkyt.split('v=')[1];
		var ampersandPosition = video_id.indexOf('&');
		if(ampersandPosition != -1) {
		  video_id = video_id.substring(0, ampersandPosition);
		}
		$.ajax({
			url:'https://www.googleapis.com/youtube/v3/videos?id='+video_id+'&key='+configkey+'&part=snippet,contentDetails,statistics,status',
			method:'get',
			success:function(g){
				var snippet = g.items[0].snippet;
				var title = snippet.title;
				var image = snippet.thumbnails.default.url;

				$("#youtube_name").empty();
				$("#youtube_name").val(title);
				$("#youtube_image").empty();
				$("#youtube_image").val(image);
				$("#youtube_link").empty();
				$("#youtube_link").val('https://www.youtube.com/embed/'+video_id);
				var html = '<center><iframe width="640" height="360" src="https://www.youtube.com/embed/'+video_id+'" frameborder="0" allowfullscreen></iframe></center>';
				$("#videoembed").empty();
				$("#videoembed").append(html);
				$("#videobox").show();
			}
		})
	});
});