$(function () {
    var listnutritionname = $("#listnutritionname").attr("rel");
    var listnutritionvalue = $("#listnutritionvalue").attr("rel");
    var listnutritionid = $("#listnutritionid").attr("rel");
    var numnutrition = $("#numnutrition").attr("rel");

    var userid = $("#userid").attr("rel");

    var series = [];

    var itemnutritionname = listnutritionname.split(',');
    var itemnutritionvalue = listnutritionvalue.split(',');

    for (var item=0;item<numnutrition;item++) {
        var targetSeries = {
            name: itemnutritionname[item],
            data: []
        };
        series.push(targetSeries);
        var val = itemnutritionvalue[item];
        targetSeries.data.push(parseInt(val));
    }

    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Biểu đồ dinh dưỡng trong các củ quả bạn đã dùng'
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Hàm lượng (microgram)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} microgram</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series:series
    });

    $('#container1').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Biểu đồ dinh dưỡng củ quả của bạn hôm nay'
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Hàm lượng (microgram)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} microgram</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series:series
    });

    $("#save").click(function(){
        $.ajax({
           url:URL_G+'tu_van/savechart/',
           type:'post',
           data:'listnutritionid='+listnutritionid+'&listnutritionvalue='+listnutritionvalue,
           success:function(g){
                if(g){
                    top.location.href = g;
                }
           }
        });
    });

    var listnutritionname1 = $("#listnutritionname1").attr("rel");
    var listnutritionvalue1 = $("#listnutritionvalue1").attr("rel");
    var listnutritionid1 = $("#listnutritionid1").attr("rel");
    var numnutrition1 = $("#numnutrition1").attr("rel");

    var series1 = [];

    var itemnutritionname1 = listnutritionname1.split(',');
    var itemnutritionvalue1 = listnutritionvalue1.split(',');

    for (var item1=0;item1<numnutrition1;item1++) {
        var targetSeries1 = {
            name: itemnutritionname1[item1],
            data: []
        };
        series1.push(targetSeries1);
        var val1 = itemnutritionvalue1[item1];
        targetSeries1.data.push(parseInt(val1));
    }

    $('#container2').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Biểu đồ dinh dưỡng củ quả của bạn hôm qua'
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Hàm lượng (microgram)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} microgram</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series:series1
    });

    var listnutritionvalue2 = $("#listnutritionvalue2").attr("rel");
    var numnutrition2 = $("#numnutrition2").attr("rel");
    var listdaynutrition = $("#listdaynutrition").attr("rel");

    var series2 = [];
    var categories = [];

    var itemnutritionvalue2 = listnutritionvalue2.split(',');
    var itemlistday = listdaynutrition.split(',');
    var loop = parseInt(numnutrition2)/parseInt(numnutrition);

    categories.push(itemlistday[0]);
    var templ = itemlistday[0];

    for (var iteml=0;iteml<numnutrition2;iteml++){
        if(itemlistday[iteml] != templ){
            categories.push(itemlistday[iteml]);
            templ = itemlistday[iteml];
        }
    }

    for (var item2=0;item2<numnutrition;item2++) {
        var targetSeries1 = {
            name: itemnutritionname[item2],
            data: []
        };
        series2.push(targetSeries1);
        for(var i=0;i<loop;i++){
            var heso = parseInt(item2)+parseInt(numnutrition)*i;
            var val2 = itemnutritionvalue2[heso];
            targetSeries1.data.push(parseInt(val2));
        }
    }

    $('#container3').highcharts({
        title: {
            text: 'Biểu đồ dinh dưỡng củ quả của bạn trong 7 ngày qua',
            x: 0 //center
        },
        xAxis: {
            categories: categories
        },
        yAxis: {
            title: {
                text: 'Hàm lượng (microgram)'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: '°C'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: series2
    });

    var listnutritionvalue3 = $("#listnutritionvalue3").attr("rel");
    var numnutrition3 = $("#numnutrition3").attr("rel");
    var listdaynutrition1 = $("#listdaynutrition1").attr("rel");

    var series3 = [];
    var categories1 = [];

    var itemnutritionvalue3 = listnutritionvalue3.split(',');
    var itemlistday1 = listdaynutrition1.split(',');
    var loop1 = parseInt(numnutrition3)/parseInt(numnutrition);

    categories1.push(itemlistday1[0]);
    var templ1 = itemlistday1[0];

    for (var iteml1=0;iteml1<numnutrition3;iteml1++){
        if(itemlistday1[iteml1] != templ1){
            categories1.push(itemlistday1[iteml1]);
            templ1 = itemlistday1[iteml1];
        }
    }

    for (var item3=0;item3<numnutrition;item3++) {
        var targetSeries1 = {
            name: itemnutritionname[item3],
            data: []
        };
        series3.push(targetSeries1);
        for(var i1=0;i1<loop1;i1++){
            var heso1 = parseInt(item3)+parseInt(numnutrition)*i1;
            var val3 = itemnutritionvalue3[heso1];
            targetSeries1.data.push(parseInt(val3));
        }
    }

    $('#container4').highcharts({
        title: {
            text: 'Biểu đồ dinh dưỡng củ quả của bạn trong 30 ngày qua',
            x: 0 //center
        },
        xAxis: {
            categories: categories1
        },
        yAxis: {
            title: {
                text: 'Hàm lượng (microgram)'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: '°C'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: series3
    });
});