function AllTables(){
    TestTable1();
    TestTable2();
    TestTable3();
    LoadSelect2Script(MakeSelect2);
}

function MakeSelect2(){
    $('select').select2();
    $('.dataTables_filter').each(function(){
        $(this).find('label input[type=text]').attr('placeholder', 'Search');
    });
}

$(document).ready(function () {
    CKEDITOR.config.removePlugins = 'elementspath';
    CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
    CKEDITOR.config.filebrowserBrowseUrl         = URL_G+'public/ckfinder/ckfinder.html';
    CKEDITOR.config.filebrowserImageBrowseUrl    = URL_G+'public/ckfinder/ckfinder.html?type=Images';
    CKEDITOR.config.filebrowserFlashBrowseUrl    = URL_G+'public/ckfinder/ckfinder.html?type=Flash';
    CKEDITOR.config.filebrowserUploadUrl         = URL_G+'public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
    CKEDITOR.config.filebrowserImageUploadUrl    = URL_G+'public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
    CKEDITOR.config.filebrowserFlashUploadUrl    = URL_G+'public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';

    CKEDITOR.replace('wysiwig_full1', {
        height: 200
    });

    CKEDITOR.replace('wysiwig_full2', {
        height: 200
    });
});

$(document).ready(function() {
    LoadDataTablesScripts(AllTables);
    // Add Drag-n-Drop feature
    WinMove();

    $("#submit").click(function(){
        var value = CKEDITOR.instances.wysiwig_full1.getData();
        $("#wysiwig_full1").text(value);
        var value1 = CKEDITOR.instances.wysiwig_full2.getData();
        $("#wysiwig_full1").text(value1);
    });

    $("#submit1").click(function(){
        var formData = new FormData($('form')[0]);
            $.ajax({
                url: URL_G+lang+'/form/createform/',  //Server script to process data
                type: 'POST',
                xhr: function() {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // Check if upload property exists
                        $("#progressbar").show();
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                    }
                    return myXhr;
                },
                // Form data
                data: formData,
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false,
                success:function(g){
                    if(g){
                        top.location.href = URL_G+'form/editform/'+g;
                    }
                }
            });
    });

    $("#submit2").click(function(){
        var formData = new FormData($('form')[0]);
            $.ajax({
                url: URL_G+lang+'/form/createform1/',  //Server script to process data
                type: 'POST',
                xhr: function() {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // Check if upload property exists
                        $("#progressbar").show();
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                    }
                    return myXhr;
                },
                // Form data
                data: formData,
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false,
                success:function(g){
                    if(g){
                        top.location.href = URL_G+'form/editform/'+g;
                    }
                }
            });
    });
});

function progressHandlingFunction(e){
    if(e.lengthComputable){
        $('progress').attr({value:e.loaded,max:e.total});
    }
}