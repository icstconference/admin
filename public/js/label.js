function AllTables(){
    TestTable1();
    TestTable2();
    TestTable3();
    LoadSelect2Script(MakeSelect2);
}

function MakeSelect2(){
    $('select').select2();
    $('.dataTables_filter').each(function(){
        $(this).find('label input[type=text]').attr('placeholder', 'Search');
    });
}

$(document).ready(function() {
    // Load Datatables and run plugin on tables
    LoadDataTablesScripts(AllTables);
    // Add Drag-n-Drop feature
    WinMove();
});

$('#colorpickerHolder').ColorPicker({
    flat:true,
    onChange: function(hsb, hex, rgb, el) {
        $(el).val(hex);
        $("#colorpickerField2").empty();
        $("#colorpickerField2").val('#'+hex);
    }
});

var color = $("#colorpickerField2").val();
var coloritem = color.split('#');
console.log(coloritem[1]);

$('#colorpickerHolder1').ColorPicker({
    flat:true,
    onShow:function(hsb, hex, rgb, el) {
        $(el).ColorPickerSetColor(coloritem[1]);
    },
    onChange: function(hsb, hex, rgb, el) {
        $("#colorpickerField2").empty();
        $("#colorpickerField2").val('#'+hex);
    }
});
