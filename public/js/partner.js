function AllTables(){
    TestTable1();
    TestTable2();
    TestTable3();
    LoadSelect2Script(MakeSelect2);
}
function MakeSelect2(){
    $('select').select2();
    $('.dataTables_filter').each(function(){
        $(this).find('label input[type=text]').attr('placeholder', 'Search');
    });
}

$(document).ready(function() {

    $(':file').change(function(){
        var file = this.files[0];
        var name = file.name;
        var size = file.size;
        var type = file.type;
        //Your validation
    });

    $("#partner_img").change(function(){
        $("#partnerimg").show();
        readURL(this,'partnerimg');
    });

    $("#partnerimg").click(function(){
        $("#partner_img").trigger('click');
    });


    $("#submit").click(function(){
        var news_name = $("#partner_name").val();
        if(news_name.length >= 1){
            var formData = new FormData($('form')[0]);
            $.ajax({
                url: URL_G+'partner/createpartner/',  //Server script to process data
                type: 'POST',
                xhr: function() {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // Check if upload property exists
                        $("#progressbar").show();
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                    }
                    return myXhr;
                },
                // Form data
                data: formData,
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false,
                success:function(g){
                    console.log(g);
                    if(g == 1){
                        top.location.href = URL_G+'partner';
                    }
                }
            });
        }else{
            $("#error").html('* Tên sản phẩm không được bỏ trống');
        }
    });

    $("#update").click(function(){
        var news_name = $("#partner_name").val();
        if(news_name.length >= 1){
            var formData = new FormData($('form')[0]);
            $.ajax({
                url: URL_G+'partner/updatepartner/',  //Server script to process data
                type: 'POST',
                xhr: function() {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // Check if upload property exists
                        $("#progressbar").show();
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                    }
                    return myXhr;
                },
                // Form data
                data: formData,
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false,
                success:function(g){
                    if(g == 1){
                        top.location.href = URL_G+'partner';
                    }
                }
            });
        }else{
            $("#error").html('* Tên sản phẩm không được bỏ trống');
        }
    });
});

function progressHandlingFunction(e){
    if(e.lengthComputable){
        $('progress').attr({value:e.loaded,max:e.total});
    }
}

function readURL(input,container) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#'+container).attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
