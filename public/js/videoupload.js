$(document).ready(function(){
    $("#submit").click(function(){
        $("#progressbar").show();
        var formData = new FormData($('form')[0]);
        $.ajax({
            url: URL_G+lang+'/videoupload/createvideo/',  //Server script to process data
            type: 'POST',
            xhr: function() {  // Custom XMLHttpRequest
                var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // Check if upload property exists
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                    }
                    return myXhr;
                 },
            // Form data
            data: formData,
            //Options to tell jQuery not to process data or worry about content-type.
            cache: false,
            contentType: false,
            processData: false,
            success:function(g){
                //console.log(g);
                if(g){
                     top.location.href = URL_G+lang+'/videoupload/';
                }
            }
        });
    });

    $("#update").click(function(){
        $("#progressbar").show();
        var formData = new FormData($('form')[0]);
        $.ajax({
            url: URL_G+lang+'/videoupload/updatevideo/',  //Server script to process data
            type: 'POST',
            xhr: function() {  // Custom XMLHttpRequest
                var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // Check if upload property exists
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                    }
                    return myXhr;
                 },
            // Form data
            data: formData,
            //Options to tell jQuery not to process data or worry about content-type.
            cache: false,
            contentType: false,
            processData: false,
            success:function(g){
                if(g){
                    top.location.href = URL_G+lang+'/videoupload/';
                }
            }
        });
    });
});

function progressHandlingFunction(e){
    if(e.lengthComputable){
        $('progress').attr({value:e.loaded,max:e.total});
    }
}