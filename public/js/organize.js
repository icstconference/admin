$(document).ready(function () {
    CKEDITOR.config.removePlugins = 'elementspath';
	CKEDITOR.config.extraPlugins = 'youtube';
    CKEDITOR.config.extraPlugins = 'embed';
    CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
    CKEDITOR.config.filebrowserBrowseUrl         = URL_G+'public/ckfinder/ckfinder.html';
    CKEDITOR.config.filebrowserImageBrowseUrl    = URL_G+'public/ckfinder/ckfinder.html?type=Images';
    CKEDITOR.config.filebrowserFlashBrowseUrl    = URL_G+'public/ckfinder/ckfinder.html?type=Flash';
    CKEDITOR.config.filebrowserUploadUrl         = URL_G+'public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
    CKEDITOR.config.filebrowserImageUploadUrl    = URL_G+'public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
    CKEDITOR.config.filebrowserFlashUploadUrl    = URL_G+'public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';

    CKEDITOR.replace('wysiwig_full1', {
        height: 200
    });
});