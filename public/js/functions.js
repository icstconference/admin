var SendRequest = function(url, params, callback){
    $.ajax({
        url     : url,
        type    : 'POST',
        data    : params
    }).done(function(data){
        callback(data);
    });
};

var CheckAll = function(id_check_all, class_check)
{
    $('input.'+class_check).prop('checked', document.getElementById(id_check_all).checked);
};

// Create Alert Follow Boostrap
var Nofitication = function(_class, content)
{
    var string  = '<div class="alert alert-' + _class + ' alert-dismissible" role="alert">';
    string      = string + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
    string      = string + content;
    string      = string + '</div>';
    return string;
};

// Dùng để lấy params từ query string chuyền vào src script
var parseQuery = function (queryString) {
    
    var query = queryString.replace(/^[^\?]+\??/,'');
    var Params = new Object();
    if (!query)
        return Params; // return empty object
    var Pairs = query.split(/[;&]/);
    for (var i = 0; i < Pairs.length; i++) {
        var KeyVal = Pairs[i].split('=');
        if (!KeyVal || KeyVal.length !== 2)
            continue;
        var key = unescape(KeyVal[0]);
        var val = unescape(KeyVal[1]);
        val = val.replace(/\+/g, ' ');
        Params[key] = val;
    }
    return Params;
};
// lấy params từ script
var parseQueryScript = function(attr_name, attr_value)
{
    var scripts = document.getElementsByTagName('script');
    var src     = '';
    for (var item in scripts)
    {
        if (scripts[item].getAttribute(attr_name) === attr_value)
        {
            src = scripts[item].getAttribute('src');
            break;
        }
    }
    var params = parseQuery(src);
    return params;
};