$(document).ready(function(){
    $("#createsitemap").click(function(){
        var formData = new FormData($('form')[0]);
        $.ajax({
            url: URL_G+'google/createsitemap/',  //Server script to process data
            type: 'POST',
            xhr: function() {  // Custom XMLHttpRequest
                var myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){ // Check if upload property exists
                    $("#progressbar").show();
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                }
                return myXhr;
            },
                // Form data
            data: formData,
                //Options to tell jQuery not to process data or worry about content-type.
            cache: false,
            contentType: false,
            processData: false,
            success:function(g){
                if(g){
                    $("#progressbar").hide();
                    $("#sitemap").css({'color':'green'});
                    $("#sitemap").text("Tạo sitemap thành công");
                    $("#sitemap").show();
                    $("#sitemap").fadeOut(3000);
                }
            }
        });
    });

    $("#pinggoogle").click(function(){
        $.ajax({
            url: URL_G+'google/pingsitemap/',  //Server script to process data
            type: 'POST',
            xhr: function() {  // Custom XMLHttpRequest
                var myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){ // Check if upload property exists
                    $("#progressbar").show();
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                }
                return myXhr;
            },
            contentType: false,
            processData: false,
            success:function(g){
                if(g){
                    $("#progressbar").hide();
                    $("#sitemap").css({'color':'green'});
                    $("#sitemap").text("Ping sitemap thành công");
                    $("#sitemap").show();
                    $("#sitemap").fadeOut(3000);
                }
            }
        });
    });
});

function progressHandlingFunction(e){
    if(e.lengthComputable){
        $('progress').attr({value:e.loaded,max:e.total});
    }
}