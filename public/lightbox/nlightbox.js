/*
var inAnimation = false;
$('#imagelightbox').click(function() {
	var src = $(this).attr('src');
	if (src.indexOf("a.png") != -1)
	{
		src = src.replace("a.png", "b.png");		
	}
	else	{	
		src = src.replace("b.png", "a.png");		
	}
	inAnimation = true;
	$(this).animateRotate(0, 90);
	$(this).attr('src', src);
	$(this).animateRotate(270, 360);
	inAnimation = false;
	//alert(src);
	
});
*/
//Close LightBox
function CloseLightBox() {
	$('#nlightbox').css("display", "none"); 

}
$(document).keyup(function(e) {    
    if (e.keyCode == 27){
		if ($('#nlightbox').css("display") != "none")
		{
			CloseLightBox();
			e.preventDefault();
		}
    }
});

$(document).on('click', '.nlightbox-button.nlightbox-close', function (event) {

	CloseLightBox();
	event.preventDefault();
});

$(document).on('click', '.nlightbox-caption', function (event) {	
	CloseLightBox();
	event.preventDefault(); 
});
/*
$.fn.animateRotate = function(startAngle, endAngle, duration, easing, complete){
    return this.each(function(){
        var elem = $(this);

        $({deg: startAngle}).animate({deg: endAngle}, {
            duration: duration,
            easing: easing,
            step: function(now){
                elem.css({					
                  '-moz-transform':'rotateY('+now+'deg)',
                  '-webkit-transform':'rotateY('+now+'deg)',
                  '-o-transform':'rotateY('+now+'deg)',
                  '-ms-transform':'rotateY('+now+'deg)',
                  'transform':'rotateY('+now+'deg)'
                });
            },
            complete: complete || $.noop
        });
    });
};
*/

$(window).on('resize', function(){
    var win = $(this); //this = window
	
    //$("#nlightbox-open").click();
});


$(document).on('click', '#nlightbox-open', function (event) {
	var image = $('#imagelightbox');
	var source = $(this).attr('href');//('.nlightbox-source');
	if (source == '#' || window.location.href.indexOf('-pid-') >= 0) {
		source = $(this).children('.nlightbox-source').attr('src');
		console.log(source);
	}
	image.attr('src', source);
	console.log(window.location.href);
	
	var ratio, wHeight, wWidth, iHeight, iWidth;
	wHeight = $(window).height() - 50;
	wWidth = $(window).outerWidth(true) - 50;
	
	iHeight = image[0].naturalHeight;
	iWidth = image[0].naturalWidth;
	
	
	
		
	if (iWidth > wWidth) {
		ratio = (wWidth) / iWidth;
		iWidth = wWidth;
		iHeight = Math.round(iHeight * ratio);
	}
	if (iHeight > wHeight) {
		ratio = (wHeight) / iHeight;
		iHeight = wHeight;
		iWidth = Math.round(iWidth * ratio);
	}	
	
	if (iHeight == 0 || iWidth == 0)
	{
		iWidth = 970;
		iHeight = 350;		
	}
		
	image.height(iHeight).width(iWidth);
	console.log(image.height());
	$('#nlightbox').css("display", "block");
	var top = ($(window).height() - image.height()) / 2;
	if (top < 0)
		top = 0;
	image.parent().css("padding-top", top + "px");
	
	return false;
	event.preventDefault(); 
	//alert(.height());
});

////End LightBox