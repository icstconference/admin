<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title><?= isset($this->title)?$this->title:'Manager IVISION'?></title>
        <link href="<?= URL ?>public/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?= URL ?>public/bootstrap/css/sb-admin.css" rel="stylesheet">
        <link href="<?= URL ?>public/bootstrap/css/plugins/morris.css" rel="stylesheet">
        <link href="<?= URL ?>public/css/admin-style.css" rel="stylesheet">
        
        <?php
            if (isset($this->head_link)) {
                $this->appendArrayStyleSheet($this->head_link);
            }
        ?>


        <link href="<?= URL ?>public/bootstrap/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <script src="<?= URL ?>public/jquery/jquery-1.11.3.min.js"></script>
        <script src="<?= URL ?>public/bootstrap/js/bootstrap.min.js"></script>

        <?php
            if (isset($this->head_script)) {
                $this->appendArrayScript($this->head_script);
            }
        ?>
        
    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><i class="fa fa-cog fa-spin"></i> Quản Trị Nội Dung I-VISON</a>
                </div>
                <!-- Top Menu Items -->
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li>
                            <a href="<?= URL?>admin_manager_content/homepage"><i class="fa fa-fw fa-file-text"></i> Nội dung HomePage</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-file-text"></i> Nội dung giới thiệu <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="<?= URL?>admin_manager_content/info_ivision_company">Về công ty IVISON</a>
                                </li>
                                <li>
                                    <a href="#">Mội trường làm việc</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa-file-text"></i> Nội dung IVISON <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo1" class="collapse">
                                <li>
                                    <a href="<?= URL?>admin_manager_content/info_ivision_tech">Công nghệ IVO</a>
                                </li>
                                <li>
                                    <a href="#">IVO Nâng cao</a>
                                </li>
                                <li>
                                    <a href="#">Demo IVO</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="<?= URL?>admin_manager_content/product"><i class="fa fa-fw fa-file-text"></i> Sản phẩm IVO</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo2"><i class="fa fa-fw fa-file-text"></i> Nội dung dịch vụ <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo2" class="collapse">
                                <li>
                                    <a href="<?= URL?>admin_manager_content/cooperate">Chương trình hợp tác</a>
                                </li>
                                <li>
                                    <a href="#">Dịch vụ khác trên web</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="<?= URL?>admin"><i class="fa fa-fw fa-backward"></i> Trở lại</a>
                        </li>
                        <li>
                            <a href="<?= URL?>admin_security/logout"><i class="fa fa-fw fa-sign-out"></i> Đăng xuất</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>

            <div id="page-wrapper">
                <?php
                    echo $this->content();
                    if (isset($this->inline_script)) {
                        $this->appendArrayScript($this->inline_script);
                    }
                ?>
            </div>
            <!-- /#page-wrapper -->

        </div>

        <!-- Morris Charts JavaScript -->
        <script src="<?= URL?>public/bootstrap/js/plugins/morris/raphael.min.js"></script>
        <script src="<?= URL?>public/bootstrap/js/plugins/morris/morris.min.js"></script>
        <script src="<?= URL?>public/bootstrap/js/plugins/morris/morris-data.js"></script>

    </body>

</html>



