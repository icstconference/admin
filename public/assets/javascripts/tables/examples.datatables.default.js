/*
Name: 			Tables / Advanced - Examples
Written by: 	Okler Themes - (http://www.okler.net)
Theme Version: 	1.4.1
*/

(function( $ ) {

	'use strict';

  var typepost = $("#typepost").attr('rel');

  if(typepost == 'news' || typepost == 'page' || typepost == 'file' || typepost == 'library' || typepost == 'video'){
  	var datatabletest = $('#datatable-default').DataTable({
  			'columnDefs': [{
  		         'targets': 0,
  		         'searchable': false,
  		         'orderable': false,
  		         'visible': true,
  		         'className': 'dt-body-center',
  		         'render': function (data, type, full, meta){
  		              return '<input type="checkbox" id="stt" value="' + $('<div/>').text(data).html() + '">';
               }
  		      }]
  		});
  }else{
    var datatabletest = $('#datatable-default').DataTable({});
  }


	
	var buttoncheckedall = 0;

	var buttonchecked = 0;

	$('#example-select-all').on('click', function(){
      // Check/uncheck checkboxes for all rows in the table
      	var rows = datatabletest.rows({ 'search': 'applied' }).nodes();
      	$('input[type="checkbox"]', rows).prop('checked', this.checked);
      	if(buttoncheckedall == 1){
      		buttoncheckedall = 0;
      		buttonchecked = 0;
      		$("#deletepost").prop('disabled', true);
      	}else{
      		buttoncheckedall = 1;
      		buttonchecked = 0;
      		$("#deletepost").prop('disabled', false);
      	}
    });

    $('input[id="stt"]').on('click',function(){
    	if($(this).is(":checked")){
    		buttonchecked++;
    		if(buttonchecked >=2){
    			$("#deletepost").prop('disabled', false);
    		}else{
    			$("#deletepost").prop('disabled', true);
    		}
    	}else{
    		buttonchecked--;
    		if(buttonchecked >=2){
    			$("#deletepost").prop('disabled', false);
    		}else{
    			$("#deletepost").prop('disabled', true);
    		}
    	}
    });

   	// Handle click on checkbox to set state of "Select all" control
   	$('#example tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      	if(!this.checked){
         	var el = $('#example-select-all').get(0);
         	// If "Select all" control is checked and has 'indeterminate' property
         	if(el && el.checked && ('indeterminate' in el)){
            	// Set visual state of "Select all" control 
            	// as 'indeterminate'
            	el.indeterminate = true;
         	}
      	}
   	});
   	

   	$('#deletepost').on('click', function(e){
      	var form = this;

      	datatabletest.$('input[id="stt"]').each(function(){
	        if(this.checked){
	        	var stringpost = '';
	            stringpost = $(this).val();
	            $('.modal-basic').magnificPopup({
					type: 'inline',
					preloader: false,
					modal: true
				});

				$(document).on('click', '.modal-dismiss', function (e) {
					e.preventDefault();
					$.magnificPopup.close();
				});

				/*
				Modal Confirm
				*/
				$(document).on('click', '.modal-confirm', function (e) {
                if(typepost == 'news'){
    					     $.ajax({
          						url: URL_G+lang+'/news/deletenews1/',
          						type: 'post',
          						data: 'idpost='+stringpost,
          						success: function(g){
          							e.preventDefault();
          							$.magnificPopup.close();
          							if(g == '1'){
          								window.location.href=URL_G+'news';
          							}
          						}
    					     });
               }else if(typepost == 'page'){
                   $.ajax({
                      url: URL_G+lang+'/page/delete1/',
                      type: 'post',
                      data: 'idpost='+stringpost,
                      success: function(g){
                        e.preventDefault();
                        $.magnificPopup.close();
                        if(g == '1'){
                          window.location.href=URL_G+'page';
                        }
                      }
                   });
               }else if(typepost == 'file'){
                  $.ajax({
                      url: URL_G+lang+'/file/deletefiles1/',
                      type: 'post',
                      data: 'idpost='+stringpost,
                      success: function(g){
                        e.preventDefault();
                        $.magnificPopup.close();
                        if(g == '1'){
                          window.location.href=URL_G+'file';
                        }
                      }
                   });
               }else if(typepost == 'library'){
                  $.ajax({
                      url: URL_G+lang+'/library/deletelibrary2/',
                      type: 'post',
                      data: 'id='+stringpost,
                      success: function(g){
                        e.preventDefault();
                        $.magnificPopup.close();
                        if(g == '1'){
                          window.location.href=URL_G+'library';
                        }
                      }
                   });
               }else if(typepost == 'video'){
                  $.ajax({
                      url: URL_G+lang+'/videoupload/delete1/',
                      type: 'post',
                      data: 'id='+stringpost,
                      success: function(g){
                        e.preventDefault();
                        $.magnificPopup.close();
                        if(g == '1'){
                          window.location.href=URL_G+'videoupload';
                        }
                      }
                   });
               }
    				});
	        }
     	});
    });

}).apply( this, [ jQuery ]);