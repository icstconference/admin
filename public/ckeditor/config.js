/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	config.language = 'vi';
	// config.uiColor = '#AADC6E';
};

CKEDITOR.on( 'dialogDefinition', function( ev )
	{
		// Take the dialog name and its definition from the event data.
		var dialogName = ev.data.name;
		var dialogDefinition = ev.data.definition;
		
		if (dialogName == 'image') {
			var onOk = dialogDefinition.onOk;

			dialogDefinition.onOk = function (e) {
				var width = this.getContentElement('info', 'txtWidth');
				if(width.getValue() > 950){
					width.setValue('950');
				}

				var height = this.getContentElement('info', 'txtHeight');
				height.setValue('');////Set Default height

				onOk && onOk.apply(this, e);
			};
		}
	});