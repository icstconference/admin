<?php

class model_albumn extends lib_db {

    function __construct() {
        parent::__construct();
    }

    public function InsertAlbumn($data){
    	$result = $this->insert('tbl_albumn')
            ->values($data)
            ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function UpdateAlbumn($data,$id){
    	$result = $this->update('tbl_albumn')
            ->set($data)
            ->where('albumn_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteAlbumn($id){
    	$result = $this->delete('tbl_albumn')
            ->where('albumn_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllAlbumn(){
    	$result = $this->select('tbl_albumn')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllAlbumnById($id){
        $result = $this->select('tbl_albumn')
            ->where('albumn_categories ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAlbumnById($id){
    	$result = $this->select('tbl_albumn')
    		->where('albumn_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function InsertAlbumnImage($data){
    	$result = $this->insert('tbl_albumn_image')
            ->values($data)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteAlbumnImage($id){
    	$result = $this->delete('tbl_albumn_image')
            ->where('albumn_image_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function InsertAlbumnCategories($data){
    	$result = $this->insert('tbl_albumn_categories')
            ->values($data)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function UpdateAlbumnCategories($data,$id){
    	$result = $this->update('tbl_albumn_categories')
            ->set($data)
            ->where('albumn_categories_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteAlbumnCategories($id){
    	$result = $this->delete('tbl_albumn_categories')
            ->where('albumn_categories_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllAlbumnCategories(){
    	$result = $this->select('tbl_albumn_categories')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAlbumnCategoriesById($id){
    	$result = $this->select('tbl_albumn_categories')
    		->where('albumn_categories_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function InsertImage($data){
        $result = $this->insert('tbl_albumn_image')
            ->values($data)
            ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function UpdateImage($data,$id){
        $result = $this->update('tbl_albumn_image')
            ->set($data)
            ->where('albumn_image_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteImage($id){
        $result = $this->delete('tbl_albumn_image')
            ->where('albumn_image_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllImage(){
        $result = $this->select('tbl_albumn_image')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllImageById($id){
        $result = $this->select('tbl_albumn_image')
            ->where('albumn_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetImageDefaultById($id){
        $result = $this->query('select * from tbl_albumn_image
            where albumn_id ='.$id.' and albumn_image_default = 1')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetImageById($id){
        $result = $this->select('tbl_albumn_image')
            ->where('albumn_image_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }
}