<?php
class model_personal extends lib_db
{
    public function __construct() {
        parent::__construct();
    }

    public function InsertPersonal($data){
        $result = $this->insert('tbl_personal')
            ->values($data)
            ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function UpdatePersonal($data,$id){
        $result = $this->update('tbl_personal')
            ->set($data)
            ->where('personal_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function DeletePersonal($id){
        $result = $this->delete('tbl_personal')
            ->where('personal_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllPersonal(){
    	$result = $this->select('tbl_personal')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetPersonalById($id){
    	$result = $this->select('tbl_personal')
    		->where('personal_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllPersonalByCatId($id){
        $result = $this->select('tbl_personal')
            ->where('categories_id ='.$id)
			->order('personal_sort asc')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function InsertPersonalLab($data){
        $result = $this->insert('tbl_personal_lab')
            ->values($data)
            ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function UpdatePersonalLab($data,$id){
        $result = $this->update('tbl_personal_lab')
            ->set($data)
            ->where('personal_lab_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function DeletePersonalLab($id){
        $result = $this->delete('tbl_personal_lab')
            ->where('personal_lab_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllPersonalLab(){
        $result = $this->select('tbl_personal_lab')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetPersonalLabById($id){
        $result = $this->select('tbl_personal_lab')
            ->where('personal_lab_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllPersonalLabByCatId($id){
        $result = $this->select('tbl_personal_lab')
            ->where('personal_lab_categories ='.$id)
			->order('personal_lab_sort asc')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function InsertPersonalCategories($data){
        $result = $this->insert('tbl_personal_categories')
            ->values($data)
            ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function UpdatePersonalCategories($data,$id){
        $result = $this->update('tbl_personal_categories')
            ->set($data)
            ->where('personal_categories_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function DeletePersonalCategories($id){
        $result = $this->delete('tbl_personal_categories')
            ->where('personal_categories_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllPersonalCategories(){
    	$result = $this->select('tbl_personal_categories')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetPersonalCategoriesById($id){
    	$result = $this->select('tbl_personal_categories')
    		->where('personal_categories_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }
}
