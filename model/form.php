<?php

class model_form extends lib_db {

    function __construct() {
        parent::__construct();
    }

    public function InsertForm($data){
    	$result = $this->insert('tbl_form')
    				   ->values($data)
    				   ->execute();

    	if($result){
    		return $this->lastInsertId();
    	}
    }

    public function UpdateForm($data,$id){
    	$result = $this->update('tbl_form')
 					   ->set($data)
    				   ->where('form_id ='.$id)
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }

    public function DeleteForm($id){
    	$result = $this->delete('tbl_form')
    				   ->where('form_id ='.$id)
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }

    public function SelectAllForm(){
        $result = $this->select('tbl_form')
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetFormById($id){
        $result = $this->select('tbl_form')
                       ->where('form_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }
	
	public function GetForm1ByIdForm($id){
        $result = $this->select('tbl_form1')
                       ->where('form_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }
	
	public function GetForm2ByIdForm($id){
        $result = $this->select('tbl_form2')
                       ->where('form_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetForm1ByCatId($id){
        $result = $this->query('select * from tbl_form 
                  Where form_categories_id ='.$id.' AND form_type = 1 limit 1')
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetForm2ByCatId($id){
        $result = $this->query('select * from tbl_form 
                 Where form_categories_id ='.$id.' AND form_type = 2 limit 1')
                       ->execute();

        if($result){
            return $result;
        }
    }

    // form categories bai bao khoa hoc

    public function InsertFormCategories($data){
        $result = $this->insert('tbl_form1_categories')
                       ->values($data)
                       ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function UpdateFormCategories($data,$id){
        $result = $this->update('tbl_form1_categories')
                       ->set($data)
                       ->where('form1_categories_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteFormCategories($id){
        $result = $this->delete('tbl_form1_categories')
                       ->where('form1_categories_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectAllFormCategories(){
        $result = $this->select('tbl_form1_categories')
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetFormCategoriesById($id){
        $result = $this->select('tbl_form1_categories')
                       ->where('form1_categories_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    // form bai bao khoa hoc

    public function InsertForm1($data){
        $result = $this->insert('tbl_form1')
                       ->values($data)
                       ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function UpdateForm1($data,$id){
        $result = $this->update('tbl_form1')
                       ->set($data)
                       ->where('form1_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteForm1($id){
        $result = $this->delete('tbl_form1')
                       ->where('form1_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectAllForm1(){
        $result = $this->select('tbl_form1')
					   ->order('form1_id DESC')
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetForm1ById($id){
        $result = $this->select('tbl_form1')
                       ->where('form1_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectAllForm1ByFormId($id){
        $result = $this->select('tbl_form1')
                       ->where('form_id ='.$id)
					   ->order('form1_id DESC')
                       ->execute();

        if($result){
            return $result;
        }
    }

    // form online seminar

    public function InsertForm2($data){
        $result = $this->insert('tbl_form2')
                       ->values($data)
                       ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function UpdateForm2($data,$id){
        $result = $this->update('tbl_form2')
                       ->set($data)
                       ->where('form2_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteForm2($id){
        $result = $this->delete('tbl_form2')
                       ->where('form2_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectAllForm2(){
        $result = $this->select('tbl_form2')
					   ->order('form2_id DESC')
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetForm2ById($id){
        $result = $this->select('tbl_form2')
                       ->where('form2_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectAllForm2ByFormId($id){
        $result = $this->select('tbl_form2')
                       ->where('form_id ='.$id)
					   ->order('form2_id DESC')
                       ->execute();

        if($result){
            return $result;
        }
    }
}