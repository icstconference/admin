<?php
class model_general extends lib_db
{
    public function __construct() {
        parent::__construct();
    }

    public function GetUsers($usertype = null){
        if($usertype){
            $result = $this->select('tbl_user')
                ->where('user_type ="'.$usertype.'"')
                ->execute();

            if($result){
                return $result;
            }
        }else{
            $result = $this->select('tbl_user')
                ->execute();

            if($result){
                return $result;
            }
        }
    }

    public function InsertNutrition($data){
        $result = $this->insert('tbl_product_nutrition')
            ->values($data)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllNutrition(){
        $result = $this->select('tbl_product_nutrition')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function InsertProduction($data){
        $result = $this->insert('tbl_product')
            ->values($data)
            ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function InsertProductionNutritionDetail($data){
        $result = $this->insert('tbl_product_nutrition_detail')
            ->values($data)
            ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function GetAllProduction(){
        $result = $this->select('tbl_product')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetProductionById($id){
        $result = $this->select('tbl_product')
            ->where('product_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteProductionById($id){
        $result = $this->delete('tbl_product')
            ->where('product_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteValueDetailById($id){
        $result = $this->delete('tbl_product_nutrition_detail')
            ->where('product_nutrition_detail_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllValueDetailByProductId($id){
        $result = $this->select('tbl_product_nutrition_detail')
            ->where('product_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllValueDetailByNutritionId($id){
        $result = $this->select('tbl_product_nutrition_detail')
            ->where('product_nutrition_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllValueDetailByProductIdNutritionId($id,$nutritionid){
        $result = $this->query('select ntd.*
                    from tbl_product_nutrition_detail as ntd
                    join tbl_product_nutrition as nt on nt.product_nutrition_id = ntd.product_nutrition_id
                    where ntd.product_id ='.$id.' and ntd.product_nutrition_id ='.$nutritionid
                    .' limit 1')->execute();

        if($result){
            return $result[0];
        }
    }

    public function UpdateValueDetail($data,$id){
        $result = $this->update('tbl_product_nutrition_detail')
            ->set($data)
            ->where('product_nutrition_detail_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function UpdateProductDetail($data,$id){
        $result = $this->update('tbl_product')
            ->set($data)
            ->where('product_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetNutritionById($nutritionid){
        $result = $this->select('tbl_product_nutrition')
            ->where('product_nutrition_id ='.$nutritionid)
            ->limit(1,0)
            ->execute();

        if($result){
            return $result[0];
        }
    }

    public function UpdateNutrition($data,$id){
        $result = $this->update('tbl_product_nutrition')
            ->set($data)
            ->where('product_nutrition_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteNutrion($id){
        $result = $this->delete('tbl_product_nutrition')
            ->where('product_nutrition_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetUserById($id){
        $result = $this->select('tbl_user')
            ->where('user_id ='.$id)
            ->limit(1,0)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteUser($id){
        $result = $this->delete('tbl_user')
            ->where('user_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function UpdateUser($data,$id){
        $result = $this->update('tbl_user')
            ->set($data)
            ->where('user_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function CountUser(){
        $result = $this->query('Select count(*) as numuser from tbl_user')->execute();

        if($result){
            return $result;
        }
    }

    public function CountNews(){
        $result = $this->query('Select count(*) as numnews from tbl_news')->execute();

        if($result){
            return $result;
        }
    }

    public function CountProduct(){
        $result = $this->query('Select count(*) as numproduct from tbl_product')->execute();

        if($result){
            return $result;
        }
    }

    public function CountCategoriesProduct(){
        $result = $this->query('Select count(*) as numcategoriesproduct from tbl_categories')->execute();

        if($result){
            return $result;
        }
    }

    public function CountCategoriesNews(){
        $result = $this->query('Select count(*) as numcategoriesnews from tbl_news_categories')->execute();

        if($result){
            return $result;
        }
    }

    public function CountSlider(){
        $result = $this->query('Select count(*) as numslider from tbl_slider')->execute();

        if($result){
            return $result;
        }
    }

    public function CountOrder(){
        $result = $this->query('Select count(*) as numorder from tbl_product_order')->execute();

        if($result){
            return $result;
        }
    }

    public function CountEvent(){
        $result = $this->query('Select count(*) as numevent from tbl_event')->execute();

        if($result){
            return $result;
        }
    }

    public function CountEmail(){
        $result = $this->query('Select count(*) as numemail from tbl_email')->execute();

        if($result){
            return $result;
        }
    }

    public function CountPartner(){
        $result = $this->query('Select count(*) as numpartner from tbl_partner')->execute();

        if($result){
            return $result;
        }
    }

    public function CountQuestions(){
        $result = $this->query('Select count(*) as numquestions from tbl_questionaire')->execute();

        if($result){
            return $result;
        }
    }

    public function CountEvents(){
        $result = $this->query('Select count(*) as numevent from tbl_event')->execute();

        if($result){
            return $result;
        }
    }

    public function CountContactUser(){
        $result = $this->query('Select count(*) as numcontactuser from tbl_contact_user')->execute();

        if($result){
            return $result;
        }
    }

    public function CountImage(){
        $result = $this->query('Select count(*) as numimage from tbl_media')->execute();

        if($result){
            return $result;
        }
    }

    public function CountAdvertise(){
        $result = $this->query('Select count(*) as numadvertise from tbl_advertise')->execute();

        if($result){
            return $result;
        }
    }
	
	public function CountFile(){
        $result = $this->query('Select count(*) as numfile from tbl_file')->execute();

        if($result){
            return $result;
        }
    }

    public function CountArtist(){
        $result = $this->query('Select count(*) as numartist from tbl_artist')->execute();

        if($result){
            return $result;
        }
    }
	
    public function InsertValueUser($data){
        $result = $this->insert('tbl_user_nutrition_detail')
            ->values($data)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteValueUser($id){
        $result = $this->delete('tbl_user_nutrition_detail')
            ->where('user_nutrition_detail_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetValueByNutritionId($id){
        $result = $this->select('tbl_user_nutrition_detail')
            ->where('product_nutrion_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetValueUser($userid){
        $result = $this->query('Select ndt.*,nt.*
        from tbl_user_nutrition_detail as ndt
        join tbl_product_nutrition as nt on nt.product_nutrition_id=ndt.product_nutrion_id
        where ndt.user_id ='.$userid.' AND DATE(ndt.user_nutrition_detail_create_date) = DATE(NOW())
        GROUP BY ndt.product_nutrion_id ')->execute();

        if($result){
            return $result;
        }
    }

    public function GetValueUserYesterday($userid){
        $result = $this->query('Select ndt.*,nt.*
        from tbl_user_nutrition_detail as ndt
        join tbl_product_nutrition as nt on nt.product_nutrition_id=ndt.product_nutrion_id
        where ndt.user_id ='.$userid.' AND DATE(ndt.user_nutrition_detail_create_date) = DATE_ADD(CURDATE(), INTERVAL -1 day)
        GROUP BY ndt.product_nutrion_id')->execute();

        if($result){
            return $result;
        }
    }

    public function GetValueUserThisWeek($userid){
        $result = $this->query('Select ndt.*,nt.*
        from tbl_user_nutrition_detail as ndt
        join tbl_product_nutrition as nt on nt.product_nutrition_id=ndt.product_nutrion_id
        where ndt.user_id ='.$userid.' AND DATE(ndt.user_nutrition_detail_create_date) BETWEEN DATE_ADD(CURDATE(), INTERVAL 1-DAYOFWEEK(CURDATE()) DAY)
        AND DATE_ADD(CURDATE(), INTERVAL 7-DAYOFWEEK(CURDATE()) DAY)')->execute();

        if($result){
            return $result;
        }
    }

    public function GetValueUserThisMonth($userid){
        $result = $this->query('Select ndt.*,nt.*
        from tbl_user_nutrition_detail as ndt
        join tbl_product_nutrition as nt on nt.product_nutrition_id=ndt.product_nutrion_id
        where ndt.user_id ='.$userid.' AND DATE(ndt.user_nutrition_detail_create_date) BETWEEN DATE_SUB(CURDATE(),INTERVAL (DAY(CURDATE())-1) DAY)
        AND LAST_DAY(NOW()) GROUP BY ndt.product_nutrion_id')->execute();

        if($result){
            return $result;
        }
    }
	
	public function GetAllAdminNoneDefault(){        
		$result = $this->query('select *             
		from tbl_user as us            
		WHERE us.user_id != 1 AND us.user_group_id != 0')            
		->execute();        
		
		if($result){            
			return $result;        
		}
	}

    public function GetAllPermissionByUserId($userid){
        $result = $this->select('tbl_user_permission')
						->where('user_group_id ='.$userid)
						->execute();
						
		if($result){
            return $result;
		}
	}

    public function GetAllPermissionByUserId1($userid,$permission){
        $result = $this->query('select * from tbl_user_permission
		WHERE user_group_id ='.$userid.' AND permission_name ="'.$permission.'"')
		->execute();        
		
		if($result){
            return $result;        
		}
	}

    public function InsertPermission($data){
        $result = $this->insert('tbl_user_permission')
					   ->values($data)                
					   ->execute();  
					   
		if($result){
            return $result;
		}
	}

    public function UpdatePermission($data,$id){
        $result = $this->update('tbl_user_permission')
					   ->set($data)                
					   ->where('user_permission_id ='.$id)
					   ->execute();

		if($result){
            return $result;
		}    
	}
	
	public function InsertPermissionNews($data){
        $result = $this->insert('tbl_user_permission_news')
					   ->values($data)                
					   ->execute();  
					   
		if($result){
            return $result;
		}
	}

    public function UpdatePermissionNews($data,$id){
        $result = $this->update('tbl_user_permission_news')
					   ->set($data)                
					   ->where('permission_news_id ='.$id)
					   ->execute();

		if($result){
            return $result;
		}    
	}
	
	public function DeletePermissionNews($id){
        $result = $this->delete('tbl_user_permission_news')
					   ->where('permission_news_id ='.$id)               
					   ->execute();  
					   
		if($result){
            return $result;
		}
	}
	
	public function GetPermissionNewsById($id){
        $result = $this->select('tbl_user_permission_news')          
					   ->where('permission_news_id ='.$id)
					   ->execute();

		if($result){
            return $result;
		}    
	}
	
	public function GetPermissionNewsByUserId($id){
        $result = $this->select('tbl_user_permission_news')          
					   ->where('user_group_id ='.$id)
					   ->execute();

		if($result){
            return $result;
		}    
	}
	
	public function SelectAllPermissionNews(){
        $result = $this->select('tbl_user_permission_news')          
					   ->execute();  
					   
		if($result){
            return $result;
		}
	}

    public function SelectTop5News(){
        $result = $this->select('tbl_news')
                       ->order('news_create_date DESC')
                       ->execute();

        if ($result) {
            return $result;
        }
    }

    public function SelectTop5Files(){
        $result = $this->select('tbl_file')
                       ->order('file_create_date DESC')
                       ->execute();

        if ($result) {
            return $result;
        }
    }

    public function SelectTop5Events(){
        $result = $this->select('tbl_event')
                       ->order('event_create_date DESC')
                       ->execute();

        if ($result) {
            return $result;
        }
    }

    public function SelectTop5Artist(){
        $result = $this->select('tbl_artist')
                       ->order('artist_create_date DESC')
                       ->execute();

        if ($result) {
            return $result;
        }
    }

    public function SelectAllTableDatabase(){
        $result = $this->query("select table_name from information_schema.tables where table_schema='".DB_NAME."'")
                       ->execute();

        if($result){
            return $result;
        }        
    }

    public function SeleteTableByName($name){
        $result = $this->query("SELECT * FROM ".$name)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteTableByName($name){
        $result = $this->query("DROP TABLE ".$name)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function CountRowTable($name){
        $result = $this->query("SELECT count(*) AS numrow FROM ".$name)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectAllColumnTable($name){
        $result = $this->query("SHOW COLUMNS FROM ".$name)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function InsertColumnTable($query){
        $result = $this->query($query)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectTable($name){
        $result = $this->query('select * from '.$name)
                       ->execute();

        if($result){
            return $result;
        }
    }
}