<?php

class model_advertise extends lib_db {

    function __construct() {
        parent::__construct();
    }

    public function InsertAdvertise($data){
    	$result = $this->insert('tbl_advertise')
    				   ->values($data)
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }

    public function UpdateAdvertise($data,$id){
    	$result = $this->update('tbl_advertise')
 					   ->set($data)
    				   ->where('advertise_id ='.$id)
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }

    public function DeleteAdvertise($id){
    	$result = $this->delete('tbl_advertise')
    				   ->where('advertise_id ='.$id)
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }

    public function SelectAdvertise(){
        $result = $this->select('tbl_advertise')
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAdvertiseById($id){
        $result = $this->select('tbl_advertise')
                       ->where('advertise_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAdvertiseByPosition($position){
        $result = $this->query('select * from tbl_advertise
                       where advertise_position ='.$position.' and advertise_status ="on"')
                       ->execute();

        if($result){
            return $result;
        }
    }
}