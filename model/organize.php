<?php
class model_organize extends lib_db
{
    public function __construct() {
        parent::__construct();
    }

    public function InsertOrganize($data){
    	$result = $this->insert('tbl_organize')
    				   ->values($data)
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }

    public function UpdateOrganize($data,$id){
    	$result = $this->update('tbl_organize')
    				   ->set($data)
    				   ->where('organize_id ='.$id)
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }

    public function DeleteOrganize($id){
    	$result = $this->delete('tbl_organize')
    				   ->where('organize_id ='.$id)
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }

    public function GetOrganizeById($id){
    	$result = $this->select('tbl_organize')
    				   ->where('organize_id ='.$id)
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }

    public function SelectAllOrganize(){
    	$result = $this->select('tbl_organize')
                       ->order('organize_create_date DESC')
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }
}