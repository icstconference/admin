<?php
class model_slider extends lib_db
{
    public function __construct() {
        parent::__construct();
    }

    public function InsertSlider($data){
        $result = $this->insert('tbl_slider')
            ->values($data)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteSlider($id){
        $result = $this->delete('tbl_slider')
            ->where('slider_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function UpdateSlider($data,$id){
        $result = $this->update('tbl_slider')
            ->set($data)
            ->where('slider_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectAllSlider(){
        $result = $this->select('tbl_slider')
			->order('slider_create_date DESC')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectSliderById($id){
        $result = $this->select('tbl_slider')
            ->where('slider_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectAllSliderStatus(){
        $result = $this->select('tbl_slider')
            ->where('slider_status = "on"')
			->order('slider_create_date DESC')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectAllSliderStatusByKind($idtype){
        $result = $this->query('select * from tbl_slider where slider_type = '.$idtype.' and slider_status = "on" order by slider_create_date desc')
            ->execute();

        if($result){
            return $result;
        }
    }
}