<?php
class model_deadline extends lib_db
{
    public function __construct() {
        parent::__construct();
    }

    public function InsertDeadline($data){
        $result = $this->insert('tbl_deadline')
                        ->values($data)
                        ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function UpdateDeadline($data,$id){
        $result = $this->update('tbl_deadline')
            ->set($data)
            ->where('deadline_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetDeadlineinfo($id){
        $result = $this->select('tbl_deadline')
            ->where('deadline_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteDeadline($id){
        $result = $this->delete('tbl_deadline')
            ->where('deadline_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectAllDeadline(){
        $result = $this->select('tbl_deadline')
		    ->order('deadline_create_date DESC')
            ->execute();

        if($result){
            return $result;
        }
    }

}