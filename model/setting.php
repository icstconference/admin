<?php
class model_setting extends lib_db
{
    public function __construct() {
        parent::__construct();
    }

    public function GetAllSetting(){
        $result = $this->select('tbl_config')
            ->limit(1,0)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function UpdateAllSetting($data){
        $result = $this->update('tbl_config')
            ->set($data)
            ->where('config_id = 1')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function InsertAllSeting($data){
        $result = $this->insert('tbl_config')
            ->values($data)
            ->where('config_id = 1')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllContact(){
        $result = $this->select('tbl_contact')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetContactById($id){
        $result = $this->select('tbl_contact')
            ->where('contact_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function InsertContact($data){
        $result = $this->insert('tbl_contact')
            ->values($data)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function UpdateContact($data,$id){
        $result = $this->update('tbl_contact')
            ->set($data)
            ->where('contact_id ='.$id)
            ->execute();

        if($result){
            return $result;

        }
    }

    public function DeleteContact($id){
        $result = $this->delete('tbl_contact')
            ->where('contact_id ='.$id)
            ->execute();

        if($result){
            return $result;

        }
    }
}
