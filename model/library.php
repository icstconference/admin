<?php
class model_library extends lib_db
{
    public function __construct() {
        parent::__construct();
    }

    public function InsertLibrary($data){
    	$result = $this->insert('tbl_media')
    				   ->values($data)
    				   ->execute();

    	if($result){
    		return $this->lastInsertId();
    	}
    }

    public function DeleteLibrary($id){
    	$result = $this->delete('tbl_media')
    				   ->where('media_id ='.$id)
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }

    public function SelectAllLibrary(){
        $result = $this->query('select * from tbl_media order by media_create_date DESC')
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetImageById($id){
        $result = $this->select('tbl_media')
                       ->where('media_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetImageByName($id){
        $result = $this->select('tbl_media')
                       ->where('media_name ="'.$id.'"')
                       ->execute();

        if($result){
            return $result;
        }
    }
}