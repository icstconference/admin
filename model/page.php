<?php
class model_page extends lib_db
{
    public function __construct() {
        parent::__construct();
    }

    public function InsertPage($data){
      $result = $this->insert('tbl_page')
            ->values($data)
            ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function UpdatePage($data,$id){
      $result = $this->update('tbl_page')
                     ->set($data)
                     ->where('page_id ='.$id)
                     ->execute();

        if($result){
            return $result;
        }
    }

    public function DeletePageById($id){
      $result = $this->delete('tbl_page')
                     ->where('page_id ='.$id)
                     ->execute();

      if($result){
         return $result;
      }
    }

    public function SelectAllPage(){
      $result = $this->select('tbl_page')
					 ->order('page_create_date DESC')
                     ->execute();

      if($result){
         return $result;
      }
    }

    public function SelectAllPageByTypeId($type){
      $result = $this->select('tbl_page')
                     ->where('page_type ='.$type)
                     ->execute();

      if($result){
         return $result;
      }
    }

    public function GetPageById($id){
      $result = $this->select('tbl_page')
                     ->where('page_id ='.$id)
                     ->execute();

      if($result){
        return $result;
      }
    }

    public function GetPageByUrl($url){
      $result = $this->select('tbl_page')
                     ->where('page_url ="'.$url.'"')
                     ->execute();

      if($result){
        return $result;
      }
    }
}