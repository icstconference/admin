<?php
class model_product extends lib_db
{
    public function __construct() {
        parent::__construct();
    }

    public function GetAllCategoriesProduct(){
        $result = $this->select('tbl_categories')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetCategoriesProductById($id){
        $result = $this->select('tbl_categories')
            ->where('categories_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetCategoriesProductByUrl($url){
        $result = $this->select('tbl_categories')
            ->where('categories_url ="'.$url.'"')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllCategoriesFather($id){
        $result = $this->select('tbl_categories')
            ->where('categories_id !='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function InsertCategoriesProduct($data){
        $result = $this->insert('tbl_categories')
            ->values($data)
            ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function UpdateCategoriesProduct($data,$id){
        $result = $this->update('tbl_categories')
            ->set($data)
            ->where('categories_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteCategoriesProduct($id){
        $result = $this->delete('tbl_categories')
            ->where('categories_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function InsertLabelProduct($data){
        $result = $this->insert('tbl_product_label')
            ->values($data)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function UpdateLabelProduct($data,$id){
        $result = $this->update('tbl_product_label')
            ->set($data)
            ->where('label_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteLabelProduct($id){
        $result = $this->delete('tbl_product_label')
            ->where('label_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetLabelProductById($id){
        $result = $this->select('tbl_product_label')
            ->where('label_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllLabelProduct(){
        $result = $this->select('tbl_product_label')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllProduct(){
        $result = $this->select('tbl_product')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetProductById($id){
        $result = $this->select('tbl_product')
            ->where('product_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetProductByUrl($url){
        $result = $this->select('tbl_product')
            ->where('product_url ="'.$url.'"')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function InsertProduct($data){
        $result = $this->insert('tbl_product')
            ->values($data)
            ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function UpdateProduct($data,$id){
        $result = $this->update('tbl_product')
            ->set($data)
            ->where('product_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteProduct($id){
        $result = $this->delete('tbl_product')
            ->where('product_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function TopProduct($limit){
        $result = $this->select('tbl_product')
            ->limit($limit,0)
            ->order('product_id DESC')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function TopSale($limit){
        $result = $this->select('tbl_product')
            ->where('label_id = 3')
            ->limit($limit,0)
            ->order('product_id DESC')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function RelatedProduct($limit,$categories_id,$product_id){
        $result = $this->query('SELECT * 
            FROM tbl_product
            WHERE categories_id ='.$categories_id.' AND product_id !='.$product_id.' 
            ORDER BY rand() limit '.$limit)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectAllProduct(){
        $result = $this->query('select pr.*,cat.*
            from tbl_product as pr
            join tbl_categories as cat on cat.categories_id = pr.categories_id
            order by pr.product_id DESC')->execute();

        if($result){
            return $result;
        }
    }

    public function GetProductByCatId($id){
        $result = $this->select('tbl_product')
            ->where('categories_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function NumberProduct($catid = null){
        if($catid){
            $result = $this->query('select count(*) as numproduct
                from tbl_product 
                where categories_id ='.$catid)->execute();
        }else{
            $result = $this->query('select count(*) as numproduct
                from tbl_product')->execute();
        }

        if($result){
            return $result;
        }
    }

    public function GetChildByParentCatid($parentid){
        $result = $this->select('tbl_categories')
            ->where('categories_father ='.$parentid)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function PaginationProduct($page,$rowinpage){
        $pagestart = (intval($page)-1)*intval($rowinpage);
        $result = $this->query('select pr.*,cat.*
            from tbl_product as pr
            join tbl_categories as cat on cat.categories_id = pr.categories_id
            order by pr.product_id DESC limit '.$pagestart.','.$rowinpage)->execute();

        if($result){
            return $result;
        }
    }

    public function PaginationProductByCatId($page,$rowinpage,$catid){
        $pagestart = (intval($page)-1)*intval($rowinpage);
        $result = $this->query('select pr.*,cat.*
            from tbl_product as pr
            join tbl_categories as cat on cat.categories_id = pr.categories_id
            where pr.categories_id ='.$catid.' order by pr.product_id DESC limit '.$pagestart.','.$rowinpage)->execute();

        if($result){
            return $result;
        }
    }

    public function InsertImageProduct($data){
        $result = $this->insert('tbl_product_images')
            ->values($data)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetImagesProduct($product_id){
        $result = $this->select('tbl_product_images')
            ->where('images_product_id ='.$product_id)
            ->limit(4,0)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetImagesProductByImageId($images_product_id){
         $result = $this->select('tbl_product_images')
            ->where('images_id ='.$images_product_id)
            ->limit(1,0)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllImagesProductByImageId($images_product_id){
         $result = $this->select('tbl_product_images')
            ->where('images_product_id  ='.$images_product_id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteImagesProduct($images_product_id){
        $result = $this->delete('tbl_product_images')
            ->where('images_id ='.$images_product_id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function UpdateImagesProduct($data,$images_product_id){
        $result = $this->update('tbl_product_images')
                       ->set($data)
                       ->where('images_id ='.$images_product_id)
                       ->execute();

        if($result){
            return $result;
        }
    }
}