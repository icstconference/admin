<?php
class model_login extends lib_db
{
    public function __construct() {
        parent::__construct();
    }

    public function GetUserbyUsername($username){
        $result = $this->select('tbl_user')
            ->where('user_name = "'.$username.'"')
            ->limit(1,0)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetUserbyUserid($userid){
        $result = $this->select('tbl_user')
            ->where('user_id = "'.$userid.'"')
            ->limit(1,0)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function SetCookie($select = array()){
        setcookie('user_id', $select['user_id'],time()+2592000, '/');
        setcookie('user_name', $select['user_name'],time()+2592000, '/');
        setcookie('user_sex', $select['user_sex'],time()+2592000, '/');
        setcookie('user_email', $select['user_email'],time()+2592000, '/');
        setcookie('user_avatar_small', $select['user_avatar_small'],time()+2592000, '/');
        setcookie('user_avatar_big', $select['user_avatar_big'],time()+2592000, '/');
        setcookie('user_type', $select['user_group_id'],time()+2592000, '/');
    }

    public function SetSession($select = array()){
        lib_session::Set('user_id', $select['user_id']);
        lib_session::Set('user_name', $select['user_name']);
        lib_session::Set('user_sex', $select['user_sex']);
        lib_session::Set('user_email', $select['user_email']);
        lib_session::Set('user_avatar_small', $select['user_avatar_small']);
        lib_session::Set('user_avatar_big', $select['user_avatar_big']);
        lib_session::Set('user_type',$select['user_group_id']);

        return true;
    }
}