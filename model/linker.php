<?php
class model_linker extends lib_db
{
    public function __construct() {
        parent::__construct();
    }

    public function Insertlinker($data){
      $result = $this->insert('tbl_linker')
            ->values($data)
            ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function Updatelinker($data,$id){
      $result = $this->update('tbl_linker')
                     ->set($data)
                     ->where('linker_id ='.$id)
                     ->execute();

        if($result){
            $result;
        }
    }

    public function DeletelinkerById($id){
      $result = $this->delete('tbl_linker')
                     ->where('linker_id ='.$id)
                     ->execute();

      if($result){
         return $result;
      }
    }

    public function SelectAlllinker(){
      $result = $this->select('tbl_linker')
					 ->order('linker_sort ASC')
                     ->execute();

      if($result){
         return $result;
      }
    }

    public function GetlinkerById($id){
      $result = $this->select('tbl_linker')
                     ->where('linker_id ='.$id)
                     ->execute();

      if($result){
        return $result;
      }
    }
}