<?php
class model_email extends lib_db
{
    public function __construct() {
        parent::__construct();
    }

    public function InsertEmail($data){
      $result = $this->insert('tbl_email')
            ->values($data)
            ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function DeleteEmailById($id){
      $result = $this->delete('tbl_email')
                     ->where('email_id ='.$id)
                     ->execute();

      if($result){
         return $result;
      }
    }

    public function SelectAllEmail(){
      $result = $this->select('tbl_email')
                     ->execute();

      if($result){
         return $result;
      }
    }

    public function GetEmailById($id){
      $result = $this->select('tbl_email')
                     ->where('email_id ='.$id)
                     ->execute();

      if($result){
        return $result;
      }
    }
}