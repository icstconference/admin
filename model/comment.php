<?php
class model_comment extends lib_db
{
    public function __construct() {
        parent::__construct();
    }

    public function InsertComment($data){
      $result = $this->insert('tbl_comment')
            ->values($data)
            ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function DeleteCommentById($id){
      $result = $this->delete('tbl_comment')
                     ->where('comment_id ='.$id)
                     ->execute();

      if($result){
         return $result;
      }
    }

    public function UpdateCommentById($data,$id){
      $result = $this->update('tbl_comment')
                     ->set($data)
                     ->where('comment_id ='.$id)
                     ->execute();

      if($result){
         return $result;
      }
    }

    public function SelectAllComment(){
      $result = $this->select('tbl_comment')
                     ->execute();

      if($result){
         return $result;
      }
    }

    public function GetCommentById($id){
      $result = $this->select('tbl_comment')
                     ->where('comment_id ='.$id)
                     ->execute();

      if($result){
         return $result;
      }
    }

    public function GetCommentByPostId($id,$type){
      $result = $this->query('select * from tbl_comment
                     where news_id ='.$id.' and comment_read = 1 and comment_type ="'.$type.'"')
                     ->execute();

      if($result){
        return $result;
      }
    }

    public function NumCommentByPostId($id,$type){
      $result = $this->query('SELECT count(*) AS numcomment FROM tbl_comment WHERE news_id ='.$id.' AND comment_read = 1 AND comment_type ="'.$type.'"')
                     ->execute();

      if($result){
        return $result;
      }
    }
}