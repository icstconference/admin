<?php
    class model_abstract extends lib_db
    {
        private $table_name;
        
        public function __construct() {
            parent::__construct();
        }
        
        public function SetTableName($table_name)
        {
            $this->table_name = $table_name;
            return $this;
        }
        
        public function GetListObjects()
        {
            return $this->select($this->table_name)
                        ->execute();
        }
        
        public function GetObjectsByWhere($where)
        {
            return $this->select($this->table_name)
                        ->where($where)
                        ->execute();
        }
        
        public function InsertObject($data)
        {
            return $this->insert($this->table_name)
                        ->values($data)
                        ->execute();
        }
        
        public function UpdateObjectByWhere($where, $data)
        {
            return $this->update($this->table_name)
                        ->set($data)
                        ->where($where)
                        ->execute();
        }
        
        public function DeleteObjectByWhere($where)
        {
            return $this->delete($this->table_name)
                        ->where($where)
                        ->execute();
        }
        
        public function GetListObjectsWithOrder($order=NULL)
        {
            return $this->select($this->table_name)
                        ->order($order)
                        ->execute();
        }
        
        public function GetObjectsByWhereWithOrder($where, $order=NULL)
        {
            return $this->select($this->table_name)
                        ->where($where)
                        ->order($order)
                        ->execute();
        }
        
        public function GetListObjectsWithOrderColumns($order=NULL, $columns=array())
        {
            return $this->select($this->table_name)
                        ->columns($columns)
                        ->order($order)
                        ->execute();
        }
        
        public function GetObjectsByWhereWithOrderColumns($where, $order=NULL, $columns=array())
        {
            return $this->select($this->table_name)
                        ->columns($columns)
                        ->where($where)
                        ->order($order)
                        ->execute();
        }
        
        public function GetObjectsByWhereHasOPWithOrderColumns($where, $where_opeator='AND', $order=NULL, $columns=array())
        {
            return $this->select($this->table_name)
                        ->columns($columns)
                        ->where($where, $where_opeator)
                        ->order($order)
                        ->execute();
        }
        
        public function GetLastColumn($columns_name)
        {
            $data = $this->select($this->table_name)
                         ->order($columns_name . ' DESC')
                         ->limit(1)
                         ->execute();
            if($data)
            {
                return $data[0][$columns_name];
            }
            return false;
        }
    }
?>