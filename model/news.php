<?php
class model_news extends lib_db
{
    public function __construct() {
        parent::__construct();
    }

    public function InsertNews($data){
        $result = $this->insert('tbl_news')
            ->values($data)
            ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function UpdateNews($data,$id){
        $result = $this->update('tbl_news')
            ->set($data)
            ->where('news_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteNews($id){
        $result = $this->delete('tbl_news')
            ->where('news_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectAllNews(){
        $result = $this->select('tbl_news')
		    ->order('news_create_date DESC')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectAllNewsSpecial(){
        $result = $this->select('tbl_news')
                       ->where('label_id = 1')
                       ->limit(5,0)
					   ->order('news_create_date DESC')
                       ->execute();

        if($result){
            return $result;
        }
    }
	
	public function SelectAllNewsmains(){
        $result = $this->select('tbl_news')
                       ->where('label_id = 2')
                       ->limit(5,0)
					   ->order('news_create_date DESC')
                       ->execute();

        if($result){
            return $result;
        }
    }
	
	public function SelectAllNewsResearch(){
        $result = $this->select('tbl_news')
                       ->where('label_id = 4')
                       ->limit(5,0)
					   ->order('news_create_date DESC')
                       ->execute();

        if($result){
            return $result;
        }
    }
	
	public function SelectAllNewsServices(){
        $result = $this->select('tbl_news')
                       ->where('label_id = 5')
                       ->limit(5,0)
					   ->order('news_create_date DESC')
                       ->execute();

        if($result){
            return $result;
        }
    }
	
	public function SelectAllNewsins(){
        $result = $this->select('tbl_news')
                       ->where('label_id = 3')
                       ->limit(5,0)
					   ->order('news_create_date DESC')
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectAllNewsByCatId($id){
        $result = $this->select('tbl_news')
            ->where('news_categories_id ='.$id)
            ->order('news_create_date DESC')
            ->execute();

        if($result){
            return $result;
        }
    }
	
	public function SelectAllNewsByCatId1($id){
        $result = $this->select('tbl_news')
            ->where('news_categories_id ='.$id)
            ->order('news_create_date ASC')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectNewsById($id){
        $result = $this->select('tbl_news')
            ->where('news_id ='.$id)
            ->limit(1,0)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectNewByUrl($url){
        $result = $this->select('tbl_news')
            ->where('news_url ="'.$url.'"')
            ->limit(1,0)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectTopNews($limit){
        $result = $this->select('tbl_news')
            ->limit($limit,0)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectTopNewsView($limit){
        $result = $this->select('tbl_news')
            ->order('view DESC')
            ->limit($limit,0)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectTopNewsRelease($limit){
        $result = $this->select('tbl_news')
            ->where('news_display ="news"')
            ->order('news_create_date DESC')
            ->limit($limit,0)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function InsertCategoriesNews($data){
        $result = $this->insert('tbl_news_categories')
            ->values($data)
            ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function UpdateCategoriesNews($data,$id){
        $result = $this->update('tbl_news_categories')
            ->set($data)
            ->where('news_categories_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }
	
	public function GetCategoriesNewsById($id){
        $result = $this->select('tbl_news_categories')
					   ->where('news_categories_id ='.$id)
					   ->execute();

        if($result){
            return $result;
        }
    }

    public function GetCategoriesNewsByUrl($url){
        $result = $this->select('tbl_news_categories')
                       ->where('news_categories_url ="'.$url.'"')
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteCategoriesNews($id){
        $result = $this->delete('tbl_news_categories')
            ->where('news_categories_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectAllNewsCategoriesChildByFatherId($id){
        $result = $this->select('tbl_news_categories')
            ->where('news_categories_father ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectAllCategoriesNews(){
        $result = $this->select('tbl_news_categories')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectAllCategoriesFatherNews($id){
        $result = $this->select('tbl_news_categories')
            ->where('news_categories_father !='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetCategoriesById($id){
        $result = $this->select('tbl_news_categories')
            ->where('news_categories_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function InsertCategoriesNewsLabel($data){
        $result = $this->insert('tbl_news_label')
            ->values($data)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function UpdateCategoriesNewsLabel($data,$id){
        $result = $this->update('tbl_news_label')
            ->set($data)
            ->where('label_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteCategoriesNewsLabel($id){
        $result = $this->delete('tbl_news_label')
            ->where('label_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectAllCategoriesNewsLabel(){
        $result = $this->select('tbl_news_label')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function CategoriesNewsLabelById($id){
        $result = $this->select('tbl_news_label')
            ->where('label_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function PaginationNews($page,$rowinpage,$catid){
        $pagestart = (intval($page)-1)*intval($rowinpage);

        if($catid != 0){
            $result = $this->query('select *
                from tbl_news
                where news_categories_id ='.$catid.' and news_display ="news" order by news_create_date DESC limit '.$pagestart.','.$rowinpage)->execute();
        }else{
            $result = $this->query('select *
                from tbl_news where news_display ="news" order by news_create_date DESC limit '.$pagestart.','.$rowinpage)->execute();
        }

        if($result){
            return $result;
        }
    }

    public function PaginationSnapshot($page,$rowinpage,$catid){
        $pagestart = (intval($page)-1)*intval($rowinpage);

        if($catid != 0){
            $result = $this->query('select *
                from tbl_news
                where news_categories_id ='.$catid.' and news_display ="company" order by news_create_date DESC limit '.$pagestart.','.$rowinpage)->execute();
        }else{
            $result = $this->query('select *
                from tbl_news where news_display ="company" order by news_create_date DESC limit '.$pagestart.','.$rowinpage)->execute();
        }

        if($result){
            return $result;
        }
    }
}