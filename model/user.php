<?php
class model_user extends lib_db
{
    public function __construct() {
        parent::__construct();
    }

    public function InsertGroup($data){
      $result = $this->insert('tbl_user_group')
            ->values($data)
            ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function UpdateGroup($data,$id){
      $result = $this->update('tbl_user_group')
                     ->set($data)
                     ->where('user_group_id ='.$id)
                     ->execute();

        if($result){
            $result;
        }
    }

    public function DeleteGroupById($id){
      $result = $this->delete('tbl_user_group')
                     ->where('user_group_id ='.$id)
                     ->execute();

      if($result){
         return $result;
      }
    }

    public function SelectAllGroup(){
      $result = $this->select('tbl_user_group')
					 ->order('user_group_create_date ASC')
                     ->execute();

      if($result){
         return $result;
      }
    }

    public function GetGroupById($id){
      $result = $this->select('tbl_user_group')
                     ->where('user_group_id ='.$id)
                     ->execute();

      if($result){
        return $result;
      }
    }
}