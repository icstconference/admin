<?php
class model_video extends lib_db
{
    public function __construct() {
        parent::__construct();
    }

    public function InsertVideo($data){
    	$result = $this->insert('tbl_video')
    				   ->values($data)
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }

    public function DeleteVideo($id){
    	$result = $this->delete('tbl_video')
    				   ->where('video_id ='.$id)
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }
	
	public function UpdateVideo($data,$videoid){
    	$result = $this->update('tbl_video')
    				   ->set($data)
					   ->where('video_id ='.$videoid)
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }

    public function SelectAllVideo(){
        $result = $this->query('select * from tbl_video order by video_sort ASC')
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetVideoById($id){
        $result = $this->select('tbl_video')
                       ->where('video_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function InsertVideoUpload($data){
        $result = $this->insert('tbl_video_upload')
                       ->values($data)
                       ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function DeleteVideoUpload($id){
        $result = $this->delete('tbl_video_upload')
                       ->where('video_upload_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }
    
    public function UpdateVideoUpload($data,$videoid){
        $result = $this->update('tbl_video_upload')
                       ->set($data)
                       ->where('video_upload_id ='.$videoid)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectAllVideoUpload(){
        $result = $this->query('select * from tbl_video_upload order by video_upload_create_date DESC')
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetVideoUploadById($id){
        $result = $this->select('tbl_video_upload')
                       ->where('video_upload_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }
}