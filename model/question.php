<?php
class model_question extends lib_db
{
    public function __construct() {
        parent::__construct();
    }

    public function SelectAllQuestion(){
    	$result = $this->select('tbl_questionaire')
                       ->order('questionaire_id DESC')
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }

    public function InsertQuestion($data){
    	$result = $this->insert('tbl_questionaire')
    				   ->values($data)
    				   ->execute();

    	if($result){
    		return $this->lastInsertId();
    	}
    }

    public function UpdateQuestion($data,$id){
    	$result = $this->update('tbl_questionaire')
    				   ->set($data)
    				   ->where('questionaire_id ='.$id)
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }

    public function DeleteQuestion($id){
    	$result = $this->delete('tbl_questionaire')
    				   ->where('questionaire_id ='.$id)
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }

    public function GetQuestionById($id){
    	$result = $this->select('tbl_questionaire')
    				   ->where('questionaire_id ='.$id)
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }
}