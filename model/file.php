<?php
class model_file extends lib_db
{
    public function __construct() {
        parent::__construct();
    }

    public function InsertFile($data){
        $result = $this->insert('tbl_file')
			           ->values($data)
			           ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function UpdateFile($data,$id){
        $result = $this->update('tbl_file')
                       ->set($data)
                       ->where('file_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteFile($id){
    	$result = $this->delete('tbl_file')
                       ->where('file_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllFile(){
    	$result = $this->select('tbl_file')
                       ->order('file_create_date DESC')
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetFileById($id){
    	$result = $this->select('tbl_file')
                       ->where('file_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllFileByCategoriesId($id){
    	$result = $this->select('tbl_file')
                       ->where('file_categories_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    // danh muc file

    public function InsertFileCategories($data){
        $result = $this->insert('tbl_file_categories')
            ->values($data)
            ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function UpdateFileCategories($data,$id){
        $result = $this->update('tbl_file_categories')
                       ->set($data)
                       ->where('file_categories_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteFileCategories($id){
    	$result = $this->delete('tbl_file_categories')
                       ->where('file_categories_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllFileCategories(){
    	$result = $this->select('tbl_file_categories')
                       ->order('file_categories_id DESC')
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllFileCategories1(){
        $result = $this->select('tbl_file_categories')
                       ->order('file_categories_id ASC')
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllFileCategoriesFather(){
    	$result = $this->select('tbl_file_categories')
    				   ->where('file_categories_father = "0"')
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetFirstFileCategoriesFather(){
    	$result = $this->select('tbl_file_categories')
    				   ->where('file_categories_father = 0')
    				   ->limit(1,0)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllFileCategoriesChildByFatherId($id){
    	$result = $this->select('tbl_file_categories')
    				   ->where('file_categories_father ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllFileCategoriesChildByFatherId1($id){
        $result = $this->select('tbl_file_categories')
                       ->where('file_categories_father ='.$id)
                       ->order('file_categories_create_date DESC')
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetFileCategoriesById($id){
    	$result = $this->select('tbl_file_categories')
                       ->where('file_categories_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetFileCategoriesByUrl($url){
      $result = $this->select('tbl_file_categories')
                       ->where('file_categories_url ="'.$url.'"')
                       ->execute();

        if($result){
            return $result;
        }
    }

}