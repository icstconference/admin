<?php
class model_packet extends lib_db
{
    public function __construct() {
        parent::__construct();
    }

    public function InsertPacket($data){
    	$result = $this->insert('tbl_packet')
    				   ->values($data)
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }

    public function UpdatePacket($data,$id){
    	$result = $this->update('tbl_packet')
    				   ->set($data)
    				   ->where('packet_id ='.$id)
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }

    public function DeletePacket($id){
    	$result = $this->delete('tbl_packet')
    				   ->where('packet_id ='.$id)
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }

    public function GetPacketById($id){
    	$result = $this->select('tbl_packet')
    				   ->where('packet_id ='.$id)
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }

    public function SelectAllPacket(){
    	$result = $this->select('tbl_packet')
                       ->order('packet_create_date DESC')
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }
}