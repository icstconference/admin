<?php
class model_event extends lib_db
{
    public function __construct() {
        parent::__construct();
    }

    public function InsertEvent($data){
        $result = $this->insert('tbl_event')
            ->values($data)
            ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function UpdateEvent($data,$id){
        $result = $this->update('tbl_event')
                       ->set($data)
                       ->where('event_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function InsertEventDetail($data){
        $result = $this->insert('tbl_event_detail')
                       ->values($data)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function UpdateEventDetail($data,$id){
        $result = $this->update('tbl_event_detail')
                       ->set($data)
                       ->where('event_detail_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }   
    }

    public function GetEventById($id){
        $result = $this->select('tbl_event')
                       ->where('event_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetEventByUrl($url){
        $result = $this->select('tbl_event')
                       ->where('event_url ="'.$url.'"')
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetEventDetailById($id){
        $result = $this->select('tbl_event_detail')
                       ->where('event_detail_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllEvent(){
        $result = $this->select('tbl_event')
                       ->order('event_create_date DESC')
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllEventAnother($id){
        $result = $this->select('tbl_event')
                       ->where('event_id !='.$id)
                       ->order('event_create_date DESC')
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllEventDetailByEventId($id){
        $result = $this->select('tbl_event_detail')
                       ->where('event_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteEventDetail($id){
        $result = $this->delete('tbl_event_detail')
                       ->where('event_detail_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteEvent($id){
        $result = $this->delete('tbl_event')
                       ->where('event_id ='.$id)
                       ->execute();

        if($result){
            if($this->GetAllEventDetailByEventId($id)){
                $result1 = $this->GetAllEventDetailByEventId($id);
                foreach ($result1 as $value) {
                    $this->DeleteEventDetail($value['event_detail_id']);
                }
            }
            return 1;
        }
    }

    public function PaginationEvent($page,$rowinpage){
        $pagestart = (intval($page)-1)*intval($rowinpage);
        $result = $this->query('select *
            from tbl_event order by event_id DESC limit '.$pagestart.','.$rowinpage)->execute();

        if($result){
            return $result;
        }
    }
}