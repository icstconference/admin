<?php
class model_partner extends lib_db
{
    public function __construct() {
        parent::__construct();
    }

    public function InsertPartner($data){
    	$result = $this->insert('tbl_partner')
    				   ->values($data)
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }

    public function UpdatePartner($data,$id){
    	$result = $this->update('tbl_partner')
    				   ->set($data)
    				   ->where('partner_id ='.$id)
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }

    public function DeletePartner($id){
    	$result = $this->delete('tbl_partner')
    				   ->where('partner_id ='.$id)
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }

    public function GetPartnerById($id){
    	$result = $this->select('tbl_partner')
    				   ->where('partner_id ='.$id)
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }

    public function SelectAllPartner(){
    	$result = $this->select('tbl_partner')
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }

    public function InsertPartnerCategories($data){
        $result = $this->insert('tbl_partner_categories')
                       ->values($data)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function UpdatePartnerCategories($data,$id){
        $result = $this->update('tbl_partner_categories')
                       ->set($data)
                       ->where('categories_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function DeletePartnerCategories($id){
        $result = $this->delete('tbl_partner_categories')
                       ->where('categories_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function GetPartnerCategoriesById($id){
        $result = $this->select('tbl_partner_categories')
                       ->where('categories_id ='.$id)
                       ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectAllPartnerCategories(){
        $result = $this->select('tbl_partner_categories')
                       ->execute();

        if($result){
            return $result;
        }
    }
}