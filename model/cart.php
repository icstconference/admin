<?php

class model_cart extends lib_db {

    function __construct() {
        parent::__construct();
    }

    public function InsertCart($data){
        $result = $this->insert('tbl_product_order')
            ->values($data)
            ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function UpdateCart($data,$id){
        $result = $this->update('tbl_product_order')
            ->set($data)
            ->where('order_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteCart($id){
        $result = $this->delete('tbl_product_order')
            ->where('order_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectCart($id){
        $result = $this->select('tbl_product_order')
            ->where('order_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectAllCart(){
        $result = $this->select('tbl_product_order')
            ->execute();

        if($result){
            return $result;
        }
    }

    
}

?>