<?php

class model_artist extends lib_db {

    function __construct() {
        parent::__construct();
    }

    public function InsertArtist($data){
    	$result = $this->insert('tbl_artist')
            ->values($data)
            ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function UpdateArtist($data,$id){
    	$result = $this->update('tbl_artist')
            ->set($data)
            ->where('artist_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteArtist($id){
    	$result = $this->delete('tbl_artist')
            ->where('artist_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllArtist(){
    	$result = $this->select('tbl_artist')
    			 ->order('artist_id DESC')
           		 ->execute();

        if($result){
            return $result;
        }
    }

    public function GetArtistById($id){
    	$result = $this->select('tbl_artist')
            ->where('artist_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetArtistByUrl($url){
      $result = $this->select('tbl_artist')
                       ->where('artist_url ="'.$url.'"')
                       ->execute();

        if($result){
            return $result;
        }
    }
}