<?php
class model_register extends lib_model{
    function __construct() {
        parent::__construct();
    }

    function Index($data=array()){
        $query = "select user_id from tbl_user where user_email=:email limit 1";
        $select = $this->db->Select($query, array(
            ":email" => $data['user_name']
        ));
        if($select){
            return -1;
        }

        $query = "select user_id from tbl_user where user_name=:user_name limit 1";
        $select = $this->db->Select($query, array(
            ":user_name" => $data['user_name']
        ));
        if($select){
            return -2;
        }
        $insert = $this->db->insert('tbl_user', array(
            'user_email'        => $data['user_email'],
            'user_name'         => $data['user_name'],
            'user_avatar_small'	=> '',
            'user_avatar_big'	=> '',
            'user_password'     =>$data['pass'],
            'salt'              =>$data['salt'],
            'user_type'         =>'user',
            'user_create_date'  => date('Y-m-d H:i:s') // use GMT aka UTC 0:00
        ));

        if($insert){
            $user_id = $this->db->lastInsertId();
            lib_session::Set('user_id',$user_id);
            lib_session::Set('user_name',$data['user_name']);
            lib_session::Set('user_email',$data['user_email']);
            lib_session::Set('user_avatar_small',URL_AVATAR_SMALL);
            lib_session::Set('user_avatar_big',URL_AVATAR_BIG);
            lib_session::Set('user_type',$data['user_type']);
            return $user_id;

        }
        return -3;
    }

}