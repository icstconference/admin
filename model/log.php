<?php
class model_log extends lib_db
{
    public function __construct() {
        parent::__construct();
    }

    public function InsertLog($data){
      $result = $this->insert('tbl_log')
            ->values($data)
            ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function UpdateLog($data,$id){
      $result = $this->update('tbl_log')
                     ->set($data)
                     ->where('log_id ='.$id)
                     ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteLogById($id){
      $result = $this->delete('tbl_log')
                     ->where('log_id ='.$id)
                     ->execute();

      if($result){
         return $result;
      }
    }

    public function SelectAllLog(){
      $result = $this->select('tbl_log')
                     ->order('log_id DESC')
                     ->execute();

      if($result){
         return $result;
      }
    }

    public function GetLogById($id){
      $result = $this->select('tbl_log')
                     ->where('log_id ='.$id)
                     ->execute();

      if($result){
        return $result;
      }
    }
}