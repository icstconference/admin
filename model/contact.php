<?php

class model_contact extends lib_db {

    function __construct() {
        parent::__construct();
    }

    public function InsertContact($data){
        $result = $this->insert('tbl_contact_user')
            ->values($data)
            ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function DeleteContact($id){
        $result = $this->delete('tbl_contact_user')
            ->where('contact_user_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectAllContact(){
    	$result = $this->select('tbl_contact_user')
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }

    public function SelectContactById($id){
    	$result = $this->select('tbl_contact_user')
    				   ->where('contact_user_id ='.$id)
    				   ->execute();

    	if($result){
    		return $result;
    	}
    }
}