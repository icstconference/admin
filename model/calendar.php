<?php

class model_calendar extends lib_db {

    function __construct() {
        parent::__construct();
    }

    public function InsertRoom($data){
    	$result = $this->insert('tbl_room')
            ->values($data)
            ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function UpdateRoom($data,$id){
    	$result = $this->update('tbl_room')
            ->set($data)
            ->where('room_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function DeleteRoom($id){
    	$result = $this->delete('tbl_room')
            ->where('room_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function GetAllRoom(){
    	$result = $this->select('tbl_room')
    			 ->order('room_create_date DESC')
           		 ->execute();

        if($result){
            return $result;
        }
    }

    public function GetRoomById($id){
    	$result = $this->select('tbl_room')
            ->where('room_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }
}