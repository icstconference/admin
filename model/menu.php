<?php
class model_menu extends lib_db
{
    public function __construct() {
        parent::__construct();
    }

    public function InsertMenu($data){
        $result = $this->insert('tbl_menu')
            ->values($data)
            ->execute();

        if($result){
            return $this->lastInsertId();
        }
    }

    public function DeleteMenu($id){
        $result = $this->delete('tbl_menu')
            ->where('menu_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function UpdateMenu($data,$id){
        $result = $this->update('tbl_menu')
            ->set($data)
            ->where('menu_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectAllMenu(){
        $result = $this->select('tbl_menu')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectMenuById($id){
        $result = $this->select('tbl_menu')
            ->where('menu_id ='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectMenuFather($id){
        $result = $this->select('tbl_menu')
            ->where('menu_id !='.$id)
            ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectAllMenuFatherOn(){
        $result = $this->query('select * from tbl_menu
            where menu_father = 0 order by menu_order ASC')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectAllChildeLv1ByFatherId($id){
        $result = $this->query('select * from tbl_menu
            where menu_father ='.$id.' order by menu_order ASC')->execute();

        if($result){
            return $result;
        }
    }

    public function SelectAllMenuFatherOnByPostion($position,$style){
        $result = $this->query('select * from tbl_menu
            where menu_father = 0 and menu_status="on" and menu_position="'.$position.'" and menu_style="'.$style.'"')
            ->execute();

        if($result){
            return $result;
        }
    }

    public function SelectMenuOrderLastest(){
        $result = $this->query('select * from tbl_menu order by menu_order DESC limit 1')->execute();

        if($result){
            return $result;
        }
    }
}