<?php

class model_account extends lib_model {

    function __construct() {
        parent::__construct();
    }

    public function GetUser() {
        return $this->db->Select("select * from tbl_user where user_name=:user_name", array(
                    ':user_name' => lib_session::Get('user_name'),
        ));
    }

    public function Index() {
        $result = $this->db->Update('tbl_user', array(
            'user_full_name'    =>$_POST['full_name'],
            'user_sex'          =>$_POST['sex'],
            'user_address'      =>$_POST['address']
        ), 'user_id='.lib_session::Get('user_id'));
        return $result;
    }
    /* 
     * Check password
     * @return string is correct password
     * @return flase is wrong password
     */
    protected function CheckPass() {
        $select = $this->db->Select("select user_password, salt from tbl_user where user_id=:user_id limit 1", array(
            ':user_id' => lib_session::Get('user_id')
        ));
        if ($select) {
            $pass = md5($_POST['pass'] . $select[0]['salt']);
            if ($pass == $select[0]['user_password']) {
                return $select;
            }
        }
        return false;
    }
    /*
     * Change password
     * @return -1 is wrong password
     * @return 1 is update password success
     */
    public function ChangePass() {
        $select = $this->CheckPass();
        if ($select === false) {
            return -1;
        }
        $in = $this->db->Update('tbl_user', array(
            'user_password' => md5($_POST['new_pass'] . $select[0]['salt'])
                ), 'user_id=' . lib_session::Get('user_id'));
        if ($in) {
            return 1;
        }
    }
    /*
     * Change email
     * @return string
     * @return -1 is wrong password
     * @return -2 is email is exist
     * @return 1 change email success
     */
    public function ChangeEmail() {
        $select = $this->CheckPass();
        if ($select === false) {
            return -1;
        }
        $s = $this->db->Select("select user_id from tbl_user where user_email=:user_email limit 1",array(
            ':user_email'=>$_POST['new_email']
        ),FALSE);
        if($s === true){
            return -2;
        }
        $in = $this->db->Update('tbl_user', array(
            'user_email' => $_POST['new_email']
                ), 'user_id=' . lib_session::Get('user_id'));
        if ($in) {
            return 1;
        }
    }
    /*
     * change avatar
     * @return boold
     */
    public function ChangeAvatar($data=array()){
        $result =   $this->db->Update('tbl_user', array(
            'user_avatar_big'   => $data['user_avatar_big'],
            'user_avatar_small' => $data['user_avatar_small']
                ), 'user_id=' . lib_session::Get('user_id'));

        if($result){
            lib_session::Set('user_avatar_small', $data['user_avatar_small']);
            lib_session::Set('user_avatar_big', $data['user_avatar_big']);
        }
    }
    function ChangeUserFullname($user_id=null){
        $result = $this->db->Select("
            SELECT user_full_name
            FROM tbl_user
            WHERE user_id=:user_id", array(
                ':user_id' => $user_id
            ));
        return $this->db->Update('o_message', array(
            'user_full_name' => $result[0]['user_full_name']
        ), 'user_id_send='.$user_id);
    }
}

?>
