<?php
/*
 * Database
 */
    define('DB_TYPE', 'mysql');
    define('DB_HOST', 'localhost');
    define('DB_NAME', 'conference');
    define('DB_USER', 'root');
    define('DB_PASS', '');
    /*
     * URL
     */
    define('URL', 'http://localhost/conference/');
    define('URL_LIB','lib/');
    define('URL_AVATAR_BIG',URL.'public/img/defavatar.png');
    define('URL_AVATAR_SMALL',URL.'public/img/defavatar.png');
    define('URL_SAVE_SMALL','public/upload/images/product/small/');
    define('URL_SAVE_BIG', 'public/upload/images/product/big/');
    /*
     * Project name
     */
    //define('NAME', 'Framework');
    //define('SOLOGAN', 'Framework');
    /*
     * set time
     */
    date_default_timezone_set('Asia/Ho_Chi_Minh');
    /*
     * Default Language on website
     * ex: en, vi...
     */
    
    define('LANG_DEF', 'vi');
    /*
     * add more values language
     * ex: vi,en,fr...
     */
    define('LANG', 'en,vi');   
    /*
     * Facebook
     */
    //define('FACEBOOK_APPID','1471585156464616');
    //define('FACEBOOK_SECRET','4f9df8ca27f0afd7dbf6a72b884fdaee');
    /*
     * email payment
     */
    //define('EMAIL_PAY','chinhdung180@yahoo.com');
    /*
     * S3
     */
    //define('AccessKey', 'AKIAJ7PG4SMGHKTMC2CQ');
    //define('SecretKey', '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');
    
    function FindFile($class)
    {
        $file       = array();
        $delimiter  = array();
        if(file_exists($class.'.php'))
        {
            $file[] = $class.'.php';
        }

        while($index=strpos($class, '_'))
        {
            $delimiter[]    = $index;
            $class[$index]  = '/';
        }

        $class = str_replace('/', '_', $class);
        $first = 0;

        while(count($delimiter) != 0)
        {
            foreach($delimiter as $key => $value)
            {
                if($value == reset($delimiter))
                {
                    $first = $key;
                }
                $string = $class;
                $string[$value] = '/';
                if(file_exists($string.".php"))
                {
                    $file[] = $string.".php";
                }
            }
            $class[reset($delimiter)] = '/';
            unset($delimiter[$first]);
        }
        return $file;
    }
?>
