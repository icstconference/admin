-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 03, 2015 at 07:55 PM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `aothun`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_categories`
--

CREATE TABLE IF NOT EXISTS `tbl_categories` (
  `categories_id` int(11) NOT NULL AUTO_INCREMENT,
  `categories_name` text COLLATE utf8_unicode_ci NOT NULL,
  `categories_father` int(11) NOT NULL,
  `categories_url` text COLLATE utf8_unicode_ci NOT NULL,
  `categories_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`categories_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `tbl_categories`
--

INSERT INTO `tbl_categories` (`categories_id`, `categories_name`, `categories_father`, `categories_url`, `categories_create_date`) VALUES
(8, 'Áo thun', 0, 'ao-thun-8', '2015-10-31 15:57:39'),
(9, 'Áo thun nam', 8, 'ao-thun-nam-9', '2015-10-31 15:57:50'),
(10, 'Áo thun nữ', 8, 'ao-thun-nu-10', '2015-10-31 15:58:00'),
(11, 'Váy đầm', 0, 'vay-dam-11', '2015-10-31 15:58:12'),
(12, 'Đồng phục công sở', 0, 'dong-phuc-cong-so-12', '2015-10-31 15:58:38');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_config`
--

CREATE TABLE IF NOT EXISTS `tbl_config` (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `config_title` text COLLATE utf8_unicode_ci NOT NULL,
  `config_description` text COLLATE utf8_unicode_ci NOT NULL,
  `config_keyword` text COLLATE utf8_unicode_ci NOT NULL,
  `config_siteurl` text COLLATE utf8_unicode_ci NOT NULL,
  `config_logo` text COLLATE utf8_unicode_ci NOT NULL,
  `config_icon` text COLLATE utf8_unicode_ci NOT NULL,
  `config_google` text COLLATE utf8_unicode_ci NOT NULL,
  `config_facebook` text COLLATE utf8_unicode_ci NOT NULL,
  `config_twitter` text COLLATE utf8_unicode_ci NOT NULL,
  `config_linkedin` text COLLATE utf8_unicode_ci NOT NULL,
  `config_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_config`
--

INSERT INTO `tbl_config` (`config_id`, `config_title`, `config_description`, `config_keyword`, `config_siteurl`, `config_logo`, `config_icon`, `config_google`, `config_facebook`, `config_twitter`, `config_linkedin`, `config_create_date`) VALUES
(1, 'La Boutique', 'La Boutique', 'la boutique', 'http://localhost/guru/thiep24h', 'http://dev2.innospeak.com/public/upload/images/563480319cb3a.png', 'http://dev2.innospeak.com/public/upload/images/5634806e8583e.png', 'https://plus.google.com/+VanAnhPham1994/', 'https://www.facebook.com/locnumber1', '', '', '2015-08-24 17:55:22');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact`
--

CREATE TABLE IF NOT EXISTS `tbl_contact` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_name` text COLLATE utf8_unicode_ci NOT NULL,
  `contact_address` text COLLATE utf8_unicode_ci NOT NULL,
  `contact_email` text COLLATE utf8_unicode_ci NOT NULL,
  `contact_phone` text COLLATE utf8_unicode_ci NOT NULL,
  `contact_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_contact`
--

INSERT INTO `tbl_contact` (`contact_id`, `contact_name`, `contact_address`, `contact_email`, `contact_phone`, `contact_create_date`) VALUES
(1, 'Thiệp 24h chi nhánh 1', '650/3a Nguyễn Kiệm, Phường 4, Quận Phú Nhuận, TPHCM', 'thiep24h@gmail.com', '0928413697', '2015-08-24 22:54:47'),
(2, 'Thiệp 24h chi nhánh 2', '650/3a Nguyễn Kiệm, Phường 4, Quận Phú Nhuận, TPHCM', 'thiep24h1@gmail.com', '0928413697', '2015-08-24 22:56:15');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_file`
--

CREATE TABLE IF NOT EXISTS `tbl_file` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` text COLLATE utf8_unicode_ci NOT NULL,
  `file_url` text COLLATE utf8_unicode_ci NOT NULL,
  `file_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tbl_file`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_media`
--

CREATE TABLE IF NOT EXISTS `tbl_media` (
  `media_id` int(11) NOT NULL AUTO_INCREMENT,
  `media_name` text COLLATE utf8_unicode_ci NOT NULL,
  `media_url` text COLLATE utf8_unicode_ci NOT NULL,
  `media_type` text COLLATE utf8_unicode_ci NOT NULL,
  `media_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`media_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tbl_media`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

CREATE TABLE IF NOT EXISTS `tbl_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` text COLLATE utf8_unicode_ci NOT NULL,
  `menu_url` text COLLATE utf8_unicode_ci NOT NULL,
  `menu_father` int(11) NOT NULL,
  `menu_style` text COLLATE utf8_unicode_ci NOT NULL,
  `menu_position` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `menu_status` text COLLATE utf8_unicode_ci NOT NULL,
  `menu_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=32 ;

--
-- Dumping data for table `tbl_menu`
--

INSERT INTO `tbl_menu` (`menu_id`, `menu_name`, `menu_url`, `menu_father`, `menu_style`, `menu_position`, `menu_status`, `menu_create_date`) VALUES
(15, 'Trang chủ', 'http://dev2.innospeak.com/', 0, 'ngang', 'all', 'on', '2015-10-31 16:04:32'),
(16, 'Áo thun', 'http://dev2.innospeak.com/category/product/ao-thun-8', 0, 'ngang', 'all', 'on', '2015-10-31 16:04:51'),
(17, 'Áo thun nam', 'http://dev2.innospeak.com/category/product/ao-thun-nam-9', 16, 'ngang', 'all', 'on', '2015-10-31 16:05:14'),
(18, 'Áo thun nữ', 'http://dev2.innospeak.com/category/product/ao-thun-8', 16, 'ngang', 'all', 'on', '2015-10-31 16:07:30'),
(19, 'Váy đầm', 'http://dev2.innospeak.com/category/product/vay-dam-11', 0, 'ngang', 'all', 'on', '2015-10-31 16:09:53'),
(22, 'Đồng phục công sở', 'http://dev2.innospeak.com/category/product/dong-phuc-cong-so-12', 0, 'ngang', 'all', 'on', '2015-10-31 16:12:02'),
(23, 'Hướng dẫn mua hàng', 'http://dev2.innospeak.com/category/news/huong-dan-su-dung-5', 0, 'ngang', 'all', 'on', '2015-10-31 16:12:24'),
(24, 'Áo thun', 'http://dev2.innospeak.com/category/product/ao-thun-8', 0, 'doc', 'product', 'on', '2015-11-01 15:15:08'),
(27, 'Váy đầm', 'http://dev2.innospeak.com/category/product/vay-dam-11', 0, 'doc', 'product', 'on', '2015-11-01 15:15:58'),
(28, 'Đồng phục công sở', 'http://dev2.innospeak.com/category/product/dong-phuc-cong-so-12', 0, 'doc', 'product', 'on', '2015-11-01 15:16:13'),
(29, 'Áo thun nam', 'http://dev2.innospeak.com/category/product/ao-thun-nam-9', 24, 'doc', 'product', 'on', '2015-11-01 15:30:47'),
(30, 'Áo thun nữ', 'http://dev2.innospeak.com/category/product/ao-thun-nu-10', 24, 'doc', 'product', 'on', '2015-11-01 15:31:04'),
(31, 'Hướng dẫn sử dụng', 'http://dev2.innospeak.com/category/news/huong-dan-su-dung-5', 0, 'doc', 'news', 'on', '2015-11-03 01:56:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news`
--

CREATE TABLE IF NOT EXISTS `tbl_news` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `news_name` text COLLATE utf8_unicode_ci NOT NULL,
  `news_image_thumb` text COLLATE utf8_unicode_ci NOT NULL,
  `news_description` text COLLATE utf8_unicode_ci NOT NULL,
  `news_categories_id` int(11) NOT NULL,
  `news_url` text COLLATE utf8_unicode_ci NOT NULL,
  `label_id` int(11) NOT NULL,
  `news_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`news_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_news`
--

INSERT INTO `tbl_news` (`news_id`, `news_name`, `news_image_thumb`, `news_description`, `news_categories_id`, `news_url`, `label_id`, `news_create_date`) VALUES
(1, 'Cold Front', 'http://dev2.innospeak.com/public/upload/images/5637aaad8b3cd.jpg', '<p>I love tart candy canes and I love donut&nbsp;<strong>cheesecake muffin</strong>&nbsp;marzipan. Sugar plum cheesecake oat cake. I love liquorice&nbsp;<a href="http://theme.laboutiquetheme.com/cold-front-3/#" style="color: rgb(26, 188, 156); transition: color 200ms ease-in-out, background-color 200ms ease-in-out, border-color 200ms ease-in-out;">lollipop</a>&nbsp;topping candy gummies carrot cake&nbsp;<em>candy fruitcake</em>&nbsp;&mdash; Cookie cookie bear claw pie.</p>\r\n\r\n<hr />\r\n<p>Topping cookie candy canes toffee cake I love tiramisu. Tiramisu&nbsp;<strong>apple pie</strong>&nbsp;chocolate. Oat cake jujubes I love toffee candy canes unerdwear.com croissant. Pudding I love tiramisu apple pie cupcake jelly beans candy canes oat cake.</p>\r\n\r\n<div style="text-align: center;"><iframe frameborder="0" height="281" src="https://player.vimeo.com/video/68161548" width="500"></iframe></div>\r\n', 5, 'cold-front-1', 0, '2015-11-03 01:05:40'),
(2, 'Summer 2014', 'http://dev2.innospeak.com/public/upload/images/5637aabab4d50.jpg', '<p><img alt="La Boutique Summer" class="alignnone size-full wp-image-296" src="http://theme.laboutiquetheme.com/wp-content/uploads/2013/12/blog-31.jpg" style="border:0px; height:auto; margin:5px 20px 20px 0px; max-width:100%; vertical-align:middle; width:830px" /></p>\r\n\r\n<p>Topping cookie candy canes toffee cake I love tiramisu. Tiramisu apple pie chocolate. Oat cake jujubes I love toffee candy canes unerdwear.com croissant. Pudding I love tiramisu apple pie cupcake jelly beans candy canes oat cake.</p>\r\n\r\n<p>Fruitcake gummies chocolate chupa chups I love lemon drops I love jelly. Pastry cheesecake chupa chups liquorice halvah drag&eacute;e jelly beans sesame snaps I love. Topping cotton candy bonbon tart marzipan cupcake. Brownie danish tootsie roll I love biscuit apple pie I love halvah liquorice.</p>\r\n\r\n<p>Candy canes I love carrot cake candy canes I love tootsie roll jelly sweet roll gummies. Jujubes&nbsp;<strong>chocolate bar</strong>&nbsp;marzipan I love. Caramels danish brownie icing I love jujubes cheesecake icing icing:</p>\r\n\r\n<ul>\r\n	<li>Sweet macaroon cookie gummies drag&eacute;e tiramisu cotton bonbon</li>\r\n	<li>Caramels danish brownie icing oat cake cheesecake candy</li>\r\n	<li>Gingerbread jelly-o lollipop liquorice topping drag&eacute;e</li>\r\n	<li>Toffee dessert gummi bears and sweet candy canes jelly-o cake</li>\r\n</ul>\r\n\r\n<p>Topping cookie candy canes toffee cake I love tiramisu.&nbsp;<em>Tiramisu apple pie</em>&nbsp;chocolate. Oat cake jujubes I love toffee candy canes unerdwear.com croissant. Pudding I love tiramisu apple pie cupcake jelly beans candy canes oat cake.</p>\r\n\r\n<p>Lollipop applicake gummi bears.&nbsp;<a href="http://theme.laboutiquetheme.com/summer-2014-3/#" style="color: rgb(26, 188, 156); transition: color 200ms ease-in-out, background-color 200ms ease-in-out, border-color 200ms ease-in-out;">Gummi bears</a>&nbsp;pudding jujubes wafer candy canes halvah gingerbread oat cake cheesecake. Gingerbread jelly-o lollipop liquorice topping drag&eacute;e.</p>\r\n\r\n<p>Candy wafer chocolate bar chocolate cake. Powder tiramisu candy canes chocolate cake.&nbsp;<strong>Toffee dessert</strong>&nbsp;gummi bears. Sweet candy canes jelly-o cake lemon drops.</p>\r\n', 5, 'summer-2014-2', 0, '2015-11-03 01:06:20'),
(3, 'New Accessories', 'http://dev2.innospeak.com/public/upload/images/5637aace56e5d.jpg', '<p>Topping cookie candy canes toffee cake I love tiramisu. Tiramisu apple pie chocolate. Oat cake jujubes I love toffee candy canes unerdwear.com croissant. Pudding I love tiramisu apple pie cupcake jelly beans candy canes oat cake.</p>\r\n\r\n<p>Fruitcake gummies chocolate chupa chups I love lemon drops I love jelly. Pastry cheesecake chupa chups liquorice halvah drag&eacute;e jelly beans sesame snaps I love. Topping cotton candy bonbon tart marzipan cupcake. Brownie danish tootsie roll I love biscuit apple pie I love halvah liquorice.</p>\r\n\r\n<p>Candy canes I love carrot cake candy canes I love tootsie roll jelly sweet roll gummies. Jujubes&nbsp;<strong>chocolate bar</strong>&nbsp;marzipan I love. Caramels danish brownie icing I love jujubes cheesecake icing icing:</p>\r\n\r\n<ul>\r\n	<li>Sweet macaroon cookie gummies drag&eacute;e tiramisu cotton bonbon</li>\r\n	<li>Caramels danish brownie icing oat cake cheesecake candy</li>\r\n	<li>Gingerbread jelly-o lollipop liquorice topping drag&eacute;e</li>\r\n	<li>Toffee dessert gummi bears and sweet candy canes jelly-o cake</li>\r\n</ul>\r\n\r\n<p>Topping cookie candy canes toffee cake I love tiramisu.&nbsp;<em>Tiramisu apple pie</em>&nbsp;chocolate. Oat cake jujubes I love toffee candy canes unerdwear.com croissant. Pudding I love tiramisu apple pie cupcake jelly beans candy canes oat cake.</p>\r\n\r\n<p>Lollipop applicake gummi bears.&nbsp;<a href="http://theme.laboutiquetheme.com/summer-2014-2/#" style="color: rgb(26, 188, 156); transition: color 200ms ease-in-out, background-color 200ms ease-in-out, border-color 200ms ease-in-out;">Gummi bears</a>&nbsp;pudding jujubes wafer candy canes halvah gingerbread oat cake cheesecake. Gingerbread jelly-o lollipop liquorice topping drag&eacute;e.</p>\r\n\r\n<p>Candy wafer chocolate bar chocolate cake. Powder tiramisu candy canes chocolate cake.&nbsp;<strong>Toffee dessert</strong>&nbsp;gummi bears. Sweet candy canes jelly-o cake lemon drops.</p>\r\n', 5, 'new-accessories-3', 0, '2015-11-03 01:06:51'),
(4, 'Tiramisu Apple Pie', 'http://dev2.innospeak.com/public/upload/images/5637aad81d4d9.jpg', '<p>I love tart candy canes and I love donut&nbsp;<strong>cheesecake muffin</strong>&nbsp;marzipan. Sugar plum cheesecake oat cake. I love liquorice&nbsp;<a href="http://theme.laboutiquetheme.com/cold-front/#" style="color: rgb(26, 188, 156); transition: color 200ms ease-in-out, background-color 200ms ease-in-out, border-color 200ms ease-in-out;">lollipop</a>&nbsp;topping candy gummies carrot cake&nbsp;<em>candy fruitcake</em>&nbsp;&mdash; Cookie cookie bear claw pie.</p>\r\n\r\n<hr />\r\n<p>Topping cookie candy canes toffee cake I love tiramisu. Tiramisu&nbsp;<strong>apple pie</strong>&nbsp;chocolate. Oat cake jujubes I love toffee candy canes unerdwear.com croissant. Pudding I love tiramisu apple pie cupcake jelly beans candy canes oat cake.</p>\r\n', 5, 'tiramisu-apple-pie-4', 0, '2015-11-03 01:07:46'),
(5, 'Boutique for WordPress', 'http://dev2.innospeak.com/public/upload/images/5637aae052816.jpg', '<p>I love tart candy canes and I love donut&nbsp;<strong>cheesecake muffin</strong>&nbsp;marzipan. Sugar plum cheesecake oat cake. I love liquorice&nbsp;<a href="http://theme.laboutiquetheme.com/cold-front-2/#" style="color: rgb(26, 188, 156); transition: color 200ms ease-in-out, background-color 200ms ease-in-out, border-color 200ms ease-in-out;">lollipop</a>&nbsp;topping candy gummies carrot cake&nbsp;<em>candy fruitcake</em>&nbsp;&mdash; Cookie cookie bear claw pie.</p>\r\n\r\n<hr />\r\n<p>Topping cookie candy canes toffee cake I love tiramisu. Tiramisu&nbsp;<strong>apple pie</strong>&nbsp;chocolate. Oat cake jujubes I love toffee candy canes unerdwear.com croissant. Pudding I love tiramisu apple pie cupcake jelly beans candy canes oat cake.</p>\r\n', 5, 'boutique-for-wordpress-5', 0, '2015-11-03 01:08:58'),
(6, 'La Boutique is Launched', 'http://dev2.innospeak.com/public/upload/images/5637aae918107.jpg', '<p>Topping cookie candy canes toffee cake I love tiramisu. Tiramisu apple pie chocolate. Oat cake jujubes I love toffee candy canes unerdwear.com croissant. Pudding I love tiramisu apple pie cupcake jelly beans candy canes oat cake.</p>\r\n\r\n<p>Fruitcake gummies chocolate chupa chups I love lemon drops I love jelly. Pastry cheesecake chupa chups liquorice halvah drag&eacute;e jelly beans sesame snaps I love. Topping cotton candy bonbon tart marzipan cupcake. Brownie danish tootsie roll I love biscuit apple pie I love halvah liquorice.</p>\r\n\r\n<p>Candy canes I love carrot cake candy canes I love tootsie roll jelly sweet roll gummies. Jujubes&nbsp;<strong>chocolate bar</strong>&nbsp;marzipan I love. Caramels danish brownie icing I love jujubes cheesecake icing icing:</p>\r\n\r\n<ul>\r\n	<li>Sweet macaroon cookie gummies drag&eacute;e tiramisu cotton bonbon</li>\r\n	<li>Caramels danish brownie icing oat cake cheesecake candy</li>\r\n	<li>Gingerbread jelly-o lollipop liquorice topping drag&eacute;e</li>\r\n	<li>Toffee dessert gummi bears and sweet candy canes jelly-o cake</li>\r\n</ul>\r\n\r\n<p>Topping cookie candy canes toffee cake I love tiramisu.&nbsp;<em>Tiramisu apple pie</em>&nbsp;chocolate. Oat cake jujubes I love toffee candy canes unerdwear.com croissant. Pudding I love tiramisu apple pie cupcake jelly beans candy canes oat cake.</p>\r\n\r\n<p>Lollipop applicake gummi bears.&nbsp;<a href="http://theme.laboutiquetheme.com/summer-2014/#" style="color: rgb(26, 188, 156); transition: color 200ms ease-in-out, background-color 200ms ease-in-out, border-color 200ms ease-in-out;">Gummi bears</a>&nbsp;pudding jujubes wafer candy canes halvah gingerbread oat cake cheesecake. Gingerbread jelly-o lollipop liquorice topping drag&eacute;e.</p>\r\n\r\n<p>Candy wafer chocolate bar chocolate cake. Powder tiramisu candy canes chocolate cake.&nbsp;<strong>Toffee dessert</strong>&nbsp;gummi bears. Sweet candy canes jelly-o cake lemon drops.</p>\r\n', 5, 'la-boutique-is-launched-6', 0, '2015-11-03 01:09:21');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news_categories`
--

CREATE TABLE IF NOT EXISTS `tbl_news_categories` (
  `news_categories_id` int(11) NOT NULL AUTO_INCREMENT,
  `news_categories_name` text COLLATE utf8_unicode_ci NOT NULL,
  `news_categories_father` int(11) NOT NULL,
  `news_categories_url` text COLLATE utf8_unicode_ci NOT NULL,
  `news_categories_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`news_categories_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_news_categories`
--

INSERT INTO `tbl_news_categories` (`news_categories_id`, `news_categories_name`, `news_categories_father`, `news_categories_url`, `news_categories_create_date`) VALUES
(5, 'Hướng dẫn sử dụng', 0, 'huong-dan-su-dung-5', '2015-10-31 16:04:01');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news_label`
--

CREATE TABLE IF NOT EXISTS `tbl_news_label` (
  `label_id` int(11) NOT NULL AUTO_INCREMENT,
  `label_name` text COLLATE utf8_unicode_ci NOT NULL,
  `label_color` text COLLATE utf8_unicode_ci NOT NULL,
  `label_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`label_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_news_label`
--

INSERT INTO `tbl_news_label` (`label_id`, `label_name`, `label_color`, `label_create_date`) VALUES
(3, 'Tin hot', '#ff0000', '2015-08-26 12:28:19'),
(4, 'Tin đặc biệt', '#eeff00', '2015-08-26 12:28:31');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news_tag`
--

CREATE TABLE IF NOT EXISTS `tbl_news_tag` (
  `news_tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `news_tag_name` text COLLATE utf8_unicode_ci NOT NULL,
  `news_id` int(11) NOT NULL,
  `news_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`news_tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tbl_news_tag`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_partner`
--

CREATE TABLE IF NOT EXISTS `tbl_partner` (
  `partner_id` int(11) NOT NULL AUTO_INCREMENT,
  `partner_name` text COLLATE utf8_unicode_ci NOT NULL,
  `partner_url` text COLLATE utf8_unicode_ci NOT NULL,
  `partner_image` text COLLATE utf8_unicode_ci NOT NULL,
  `partner_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`partner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tbl_partner`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE IF NOT EXISTS `tbl_product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` text COLLATE utf8_unicode_ci NOT NULL,
  `product_description` text COLLATE utf8_unicode_ci NOT NULL,
  `product_price_new` double NOT NULL,
  `product_price_old` double NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `product_image_thumb` text COLLATE utf8_unicode_ci NOT NULL,
  `product_image_thumb_behind` text COLLATE utf8_unicode_ci NOT NULL,
  `product_image` text COLLATE utf8_unicode_ci NOT NULL,
  `product_image_behind` text COLLATE utf8_unicode_ci NOT NULL,
  `product_url` text COLLATE utf8_unicode_ci NOT NULL,
  `categories_id` int(11) NOT NULL,
  `label_id` int(11) NOT NULL,
  `product_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`product_id`, `product_name`, `product_description`, `product_price_new`, `product_price_old`, `product_quantity`, `product_image_thumb`, `product_image_thumb_behind`, `product_image`, `product_image_behind`, `product_url`, `categories_id`, `label_id`, `product_create_date`) VALUES
(1, 'Áo thun nam 1', '', 120000, 125000, 100, 'http://dev2.innospeak.com/public/upload/images/product/big/5635c771e3bf4.jpg', 'http://dev2.innospeak.com/public/upload/images/product/big/5635c7733a5fe.jpg', 'http://dev2.innospeak.com/public/upload/images/5635c771cfe7a.jpg', 'http://dev2.innospeak.com/public/upload/images/5635c7731c928.jpg', 'ao-thun-nam-1-1', 9, 3, '2015-11-01 13:30:13'),
(2, 'Áo thun nam 2', '', 120000, 125000, 100, 'http://dev2.innospeak.com/public/upload/images/product/big/5635c7b4ee7e4.jpg', 'http://dev2.innospeak.com/public/upload/images/product/big/5635c78d92ac1.jpg', 'http://dev2.innospeak.com/public/upload/images/5635c7b4e4082.jpg', 'http://dev2.innospeak.com/public/upload/images/5635c78d78781.jpg', 'ao-thun-nam-2-2', 9, 3, '2015-11-01 14:50:11'),
(3, 'Áo thun nam 3', '', 100000, 150000, 0, 'http://dev2.innospeak.com/public/upload/images/product/big/5635c7d0da73a.jpg', 'http://dev2.innospeak.com/public/upload/images/product/big/5635c7d14582c.jpg', 'http://dev2.innospeak.com/public/upload/images/5635c7d0d0698.jpg', 'http://dev2.innospeak.com/public/upload/images/5635c7d13ca1e.jpg', 'ao-thun-nam-3-3', 9, 3, '2015-11-01 14:50:44'),
(4, 'Áo thun nam 4', '', 100000, 0, 0, 'http://dev2.innospeak.com/public/upload/images/product/big/5635c7ec15dd0.jpg', 'http://dev2.innospeak.com/public/upload/images/product/big/5635c7ed1c704.jpg', 'http://dev2.innospeak.com/public/upload/images/5635c7ec01340.jpg', 'http://dev2.innospeak.com/public/upload/images/5635c7ecee31a.jpg', 'ao-thun-nam-4-4', 8, 0, '2015-11-01 14:51:18'),
(5, 'Áo thun nam 5', '', 120000, 0, 0, 'http://dev2.innospeak.com/public/upload/images/product/big/5635c800ec5d9.jpg', 'http://dev2.innospeak.com/public/upload/images/product/big/5635c802000fb.jpg', 'http://dev2.innospeak.com/public/upload/images/5635c800d5596.jpg', 'http://dev2.innospeak.com/public/upload/images/5635c801dd9c3.jpg', 'ao-thun-nam-5-5', 8, 0, '2015-11-01 14:51:51'),
(6, 'Váy nữ', '', 120000, 0, 0, 'http://dev2.innospeak.com/public/upload/images/product/big/5635c83c72c9a.jpg', 'http://dev2.innospeak.com/public/upload/images/product/big/5635c83ce9dd2.jpg', 'http://dev2.innospeak.com/public/upload/images/5635c83c68cef.jpg', 'http://dev2.innospeak.com/public/upload/images/5635c83cdae53.jpg', 'vay-nu-6', 11, 0, '2015-11-01 15:07:25'),
(7, 'Áo thun nam 6', '', 100000, 0, 0, 'http://dev2.innospeak.com/public/upload/images/product/big/5635db310b805.jpg', 'http://dev2.innospeak.com/public/upload/images/product/big/5635db319184c.jpg', 'http://dev2.innospeak.com/public/upload/images/5635db30e14d0.jpg', 'http://dev2.innospeak.com/public/upload/images/5635db316ac8a.jpg', 'ao-thun-nam-6-7', 9, 0, '2015-11-01 16:28:17');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_images`
--

CREATE TABLE IF NOT EXISTS `tbl_product_images` (
  `images_id` int(11) NOT NULL AUTO_INCREMENT,
  `images_url` text COLLATE utf8_unicode_ci NOT NULL,
  `images_product_id` int(11) NOT NULL,
  `images_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`images_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_product_images`
--

INSERT INTO `tbl_product_images` (`images_id`, `images_url`, `images_product_id`, `images_create_date`) VALUES
(1, 'http://dev2.innospeak.com/public/upload/images/56363fb223912.jpg', 1, '2015-11-01 13:30:14'),
(2, 'http://dev2.innospeak.com/public/upload/images/563640b5ca438.jpg', 1, '2015-11-01 13:30:14'),
(3, 'http://dev2.innospeak.com/public/upload/images/563640e2296bd.jpg', 1, '2015-11-01 13:30:14'),
(6, 'http://dev2.innospeak.com/public/upload/images/56364111754c7.jpg', 1, '2015-11-01 14:38:12');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_label`
--

CREATE TABLE IF NOT EXISTS `tbl_product_label` (
  `label_id` int(11) NOT NULL AUTO_INCREMENT,
  `label_name` text COLLATE utf8_unicode_ci NOT NULL,
  `label_color` text COLLATE utf8_unicode_ci NOT NULL,
  `label_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`label_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_product_label`
--

INSERT INTO `tbl_product_label` (`label_id`, `label_name`, `label_color`, `label_create_date`) VALUES
(2, 'Hết hàng', '#ff4a4a', '2015-08-25 14:18:43'),
(3, 'Khuyến mãi', '#f7ff00', '2015-08-25 14:18:59'),
(4, 'Còn hàng', '#00ff00', '2015-08-25 14:25:03');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_order`
--

CREATE TABLE IF NOT EXISTS `tbl_product_order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_name` text COLLATE utf8_unicode_ci NOT NULL,
  `order_email` text COLLATE utf8_unicode_ci NOT NULL,
  `order_address` text COLLATE utf8_unicode_ci NOT NULL,
  `order_mobile` text COLLATE utf8_unicode_ci NOT NULL,
  `order_summoney` double NOT NULL,
  `order_require` longtext COLLATE utf8_unicode_ci NOT NULL,
  `order_information` longtext COLLATE utf8_unicode_ci NOT NULL,
  `order_status` tinyint(1) NOT NULL DEFAULT '0',
  `order_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_product_order`
--

INSERT INTO `tbl_product_order` (`order_id`, `order_name`, `order_email`, `order_address`, `order_mobile`, `order_summoney`, `order_require`, `order_information`, `order_status`, `order_create_date`) VALUES
(1, 'loc phan nguyen', 'locnumber1@gmail.com', '650/3A nguyễn Kiệm phường 4 phú nhuận', '0928413697', 445000, '<table>\r\n                <tr>\r\n                    <th>Sản phẩm</th>\r\n                    <th>Số lượng</th>\r\n                    <th>Kích cỡ</th>\r\n                    <th>Giới tính</th>\r\n                    <th>Loại</th>\r\n                </tr>\r\n                <tr><td>Áo thun nam 3</td><td>2</td><td>S</td><td>nam</td><td> Áo đơn </td></tr><tr><td>Áo thun nam 3</td><td>1</td><td>L</td><td>nam</td><td> Áo đơn </td></tr></table>', '', 1, '2015-11-04 01:12:36'),
(2, 'loc phan nguyen', 'locnumber1@gmail.com', '650/3A nguyễn Kiệm phường 4 phú nhuận', '0928413697', 265000, '<table>\r\n                <tr>\r\n                    <th>Sản phẩm</th>\r\n                    <th>Số lượng</th>\r\n                    <th>Kích cỡ</th>\r\n                    <th>Giới tính</th>\r\n                    <th>Loại</th>\r\n                </tr>\r\n                <tr><td>Áo thun nam 5</td><td style="text-align:center;">1</td><td style="text-align:center;">M</td><td style="text-align:center;">nu</td><td> Áo đơn </td></tr></table>', '', 0, '2015-11-04 01:48:34');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_tag`
--

CREATE TABLE IF NOT EXISTS `tbl_product_tag` (
  `product_tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_tag_name` text COLLATE utf8_unicode_ci NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_tag_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`product_tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tbl_product_tag`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_type`
--

CREATE TABLE IF NOT EXISTS `tbl_product_type` (
  `product_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_type_name` text COLLATE utf8_unicode_ci NOT NULL,
  `product_type_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`product_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tbl_product_type`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_slider`
--

CREATE TABLE IF NOT EXISTS `tbl_slider` (
  `slider_id` int(11) NOT NULL AUTO_INCREMENT,
  `slider_img` text COLLATE utf8_unicode_ci NOT NULL,
  `slider_caption` text COLLATE utf8_unicode_ci NOT NULL,
  `slider_title` text COLLATE utf8_unicode_ci NOT NULL,
  `slider_url` text COLLATE utf8_unicode_ci NOT NULL,
  `slider_status` text COLLATE utf8_unicode_ci NOT NULL,
  `slider_create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`slider_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tbl_slider`
--

INSERT INTO `tbl_slider` (`slider_id`, `slider_img`, `slider_caption`, `slider_title`, `slider_url`, `slider_status`, `slider_create_date`) VALUES
(8, 'http://dev2.innospeak.com/public/upload/images/slider/5634750e92b87.jpg', 'Isotope listings, price filtering, instant search and more', 'Built-in WooCommerce System', 'http://google.com.vn', 'on', '2015-10-31 15:00:14'),
(9, 'http://dev2.innospeak.com/public/upload/images/slider/56347c5041e6a.jpg', 'Isotope listings, price filtering, instant search and more', 'Built-in WooCommerce System', 'http://google.com.vn ', 'on', '2015-10-31 15:05:49'),
(10, 'http://dev2.innospeak.com/public/upload/images/slider/56347c5be15c8.jpg', 'Isotope listings, price filtering, instant search and more', 'Built-in WooCommerce System', 'http://google.com.vn ', 'on', '2015-10-31 15:06:27');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` text COLLATE utf8_unicode_ci NOT NULL,
  `user_full_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_password` text COLLATE utf8_unicode_ci NOT NULL,
  `salt` text COLLATE utf8_unicode_ci NOT NULL,
  `user_sex` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` char(128) CHARACTER SET latin1 NOT NULL,
  `user_avatar_small` text COLLATE utf8_unicode_ci NOT NULL,
  `user_avatar_big` text COLLATE utf8_unicode_ci NOT NULL,
  `user_phone` char(16) CHARACTER SET latin1 DEFAULT NULL,
  `user_address` text COLLATE utf8_unicode_ci NOT NULL,
  `user_type` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `user_create_date` datetime NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_email` (`user_email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_name`, `user_full_name`, `user_password`, `salt`, `user_sex`, `user_email`, `user_avatar_small`, `user_avatar_big`, `user_phone`, `user_address`, `user_type`, `user_create_date`) VALUES
(2, 'locnumber1', NULL, 'be39d5df87f9d62bf92b0352858bd9ab', '9aeb033785a45db484b3faab4a13cb01', '', 'locnumber1@gmail.com', 'http://localhost/guru/website-bach/public/img/defavatar.png', '', NULL, '', 'admin', '2015-08-19 11:35:10'),
(4, 'admin', NULL, 'afb471c152fab3a1c186cd4f9d9fb388', 'cd0dbe6d781282e8b0a9882555414583', '', 'locnumber@yahoo.com.vn', '', '', NULL, '', 'user', '2015-08-20 14:00:24');
