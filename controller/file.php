<?php
class controller_file extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    /*
     * Các function tạo hình ảnh
     */

    function UploadFile($data){
            if ($data["error"] == 0) {
                if($data["type"] == "application/pdf" || $data["type"] == "application/x-pdf" ){
                    $filetype = "pdf";
                }
                if($data["type"] == "application/msword"){
                    $filetype = "docx";
                }
                $path = "public/upload/file/" .$data['name'];
                move_uploaded_file($data["tmp_name"], $path);
                $array = array(
                    'name'  => $data['name'],
                    'type'  => $data['type'],
                    'size'  => $data['size'],
                    'path'  => $path
                );
                //return $path;
                return $array;
            }
    }

    protected function DeleteFile($file_path) {
        $path = str_replace(URL, '', $file_path);
        if (file_exists($path)) {
            unlink($path);
            return true;
        }
        return false;
    }

    public function categories(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        require_once 'plugin/url-seo/url-seo.php';

        $file = new model_file();

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($create[0]['permission_detail'],'file') || !strpos($delete[0]['permission_detail'],'file') || !strpos($edit[0]['permission_detail'],'file')){
                header('Location:'.URL.'admin/');
            }
        }

        if($file->GetAllFileCategories()){
            $this->view->listcategories = $file->GetAllFileCategories();
        }

        if($_POST){
            $file_type          = $_POST['file_type'];
            $file_type_english  = $_POST['file_type_english'];
            $file_fathertype    = '';

            if($_POST['file_fathertype']){
                if(count($_POST['file_fathertype']) > 1){
                    foreach($_POST['file_fathertype'] as $val){
                        $file_fathertype .= ','.$val;
                    }
                }else{
                    $arr = $_POST['file_fathertype'];
                    $file_fathertype = $arr[0];
                }
            }else{
                $file_fathertype = '';
            }

            $arr = array(
                'file_categories_name'          => $file_type,
                'file_categories_name_english'  => $file_type_english,
                'file_categories_father'        => $file_fathertype,
            );

            $result = $file->InsertFileCategories($arr);

            if($result){
                $file_categories_url            = post_slug($file_type).'-file-'.$result;
                $file_categories_url_english    = post_slug($file_type_english).'-file-'.$result;

                $arr1 = array(
                    'file_categories_url'           => $file_categories_url,
                    'file_categories_url_english'   => $file_categories_url_english
                );

                $result1 = $file->UpdateFileCategories($arr1,$result);
                $log = new model_log();
                $arr = array(
                    'log_action'        => 'create',
                    'user_id'           => $_SESSION['user_id'],
                    'log_object'        => 'chuyên mục tài nguyên',
                    'log_object_id'     => $result,
                    'log_object_name'   => $file_type,
                    'user_name'         => $_SESSION['user_name']
                );
                $log->InsertLog($arr);

                header('Location:'.URL.$this->view->lang.'/file/categories/');

            }

        }

        $this->view->js = array(
            URL.'public/js/filecat.js'
        );

        $this->view->title = 'Trang quản trị ';

        $this->view->layout('layout-admin1');
        $this->view->Render('file/categories');
        return true;
    }

    public function editcategories($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($edit[0]['permission_detail'],'file')){
                header('Location:'.URL.'admin/');
            }
        }

        require_once 'plugin/url-seo/url-seo.php';

        $file = new model_file();

        if($file->GetAllFileCategories()){
            $this->view->listcategories = $file->GetAllFileCategories();
        }

        if($path){
            if($file->GetFileCategoriesById($path)){
                $categoriesinfo = $file->GetFileCategoriesById($path);

                $this->view->categoriesinfo = $categoriesinfo;

                if($_POST){
                    $file_type                      = $_POST['file_type'];
                    $file_type_english              = $_POST['file_type_english'];
                    $file_fathertype    = '';

                    if($_POST['file_fathertype']){
                        if(count($_POST['file_fathertype']) > 1){
                            foreach($_POST['file_fathertype'] as $val){
                                $file_fathertype .= ','.$val;
                            }
                        }else{
                            $arr = $_POST['file_fathertype'];
                            $file_fathertype = $arr[0];
                        }
                    }else{
                        $file_fathertype = 0;
                    }

                    $file_categories_url            = post_slug($file_type).'-file-'.$path;
                    $file_categories_url_english    = post_slug($file_type_english).'-file-'.$path;


                    $arr = array(
                        'file_categories_name'          => $file_type,
                        'file_categories_name_english'  => $file_type_english,
                        'file_categories_father'        => $file_fathertype,
                        'file_categories_url'           => $file_categories_url,
                        'file_categories_url_english'   => $file_categories_url_english
                    );

                    $result = $file->UpdateFileCategories($arr,$path);

                    if($result){
                        $log = new model_log();
                        $arr = array(
                            'log_action'        => 'update',
                            'user_id'           => $_SESSION['user_id'],
                            'log_object'        => 'chuyên mục tài nguyên',
                            'log_object_id'     => $path,
                            'log_object_name'   => $file_type,
                            'user_name'         => $_SESSION['user_name']
                        );
                        $log->InsertLog($arr);
                        header('Location:'.URL.$this->view->lang.'/file/categories/');
                    }
                }
            }
        }else{
            header('Location:'.URL.$this->view->lang.'/file/categories/');
        }

        $this->view->js = array(
            URL.'public/js/filecat.js'
        );

        $this->view->title = 'Trang quản trị ';

        $this->view->layout('layout-admin1');
        $this->view->Render('file/editcategories');
        return true;
    }

    public function deletecategories($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($delete[0]['permission_detail'],'file')){
                header('Location:'.URL.'admin/');
            }
        }
        
        require_once 'plugin/url-seo/url-seo.php';

        $file = new model_file();

        if($path){
            if($file->GetFileCategoriesById($path)){
                $categoriesinfo = $file->GetFileCategoriesById($path);

                $AllFileCategories = $file->GetAllFileByCategoriesId($path);

                $deletecategories = $file->DeleteFileCategories($path);

                if($deletecategories){
					if($file->GetAllFileByCategoriesId($path)){
						foreach ($AllFileCategories as $value) {
							$deletefile = $file->DeleteFile($value['file_id']);
							$this->DeleteFile($value['file_url']);
						}
					}
                    $log = new model_log();
                    $arr = array(
                        'log_action'        => 'delete',
                        'user_id'           => $_SESSION['user_id'],
                        'log_object'        => 'chuyên mục tài nguyên',
                        'log_object_id'     => $categoriesinfo[0]['file_categories_id'],
                        'log_object_name'   => $categoriesinfo[0]['file_categories_name'],
                        'user_name'         => $_SESSION['user_name']
                    );
                    $log->InsertLog($arr);
                    header('Location:'.URL.$this->view->lang.'/file/categories/');
                }
            }else{
                header('Location:'.URL.$this->view->lang.'/file/categories/');
            }
        }else{
            header('Location:'.URL.$this->view->lang.'/file/categories/');
        }
    }

    public function createfile(){
        $file = new model_file();

        if($file->GetAllFileCategories()){
            $this->view->listcategories = $file->GetAllFileCategories();
        }

        if($_POST){
            $file_url               = '';
            $file_type              = '';
            $file_size              = '';
            $file_name              = $_POST['file_name'];
			$file_name_english      = $_POST['file_name_english'];
            $file_categories_id     = $_POST['file_categories_id'];
            $file_link              = $_POST['file_link'];
            $file_create_date       = date('Y-m-d h:i:s',strtotime($_POST['file_create_date']));

            if(strlen($_FILES['file_type']['name'])>1){
                $upload_file = $this->UploadFile($_FILES['file_type']);
                
                if($upload_file){
                    $file_url   = $upload_file['path'];
                    $file_type  = $upload_file['type'];
                    $file_size  = $upload_file['size'];
                }
            }

            $arr = array(
                'file_name'             => $file_name,
				'file_name_english'		=> $file_name_english,
                'file_url'              => $file_url,
                'file_link'             => $file_link,
                'file_type'             => $file_type,
                'file_volume'           => $file_size,
                'file_categories_id'    => $file_categories_id,
                'file_create_date'      => $file_create_date
            );

            $result = $file->InsertFile($arr);

            if($result){
                $log = new model_log();
                $arr = array(
                    'log_action'        => 'create',
                    'user_id'           => $_SESSION['user_id'],
                    'log_object'        => 'tài nguyên',
                    'log_object_id'     => $result,
                    'log_object_name'   => $file_name,
                    'user_name'         => $_SESSION['user_name']
                );
                $log->InsertLog($arr);
                echo '1';
            }
        }

    }
	
	public function updatefile(){
		$file = new model_file();

        if($_POST){
			$file_id 				= $_POST['file_id'];
			$file_info				= $file->GetFileById($file_id);
            $file_url               = '';
            $file_type              = '';
            $file_size              = '';
            $file_name              = $_POST['file_name'];
			$file_name_english      = $_POST['file_name_english'];
            $file_categories_id     = $_POST['file_categories_id'];
            $file_link              = $_POST['file_link'];
            $file_create_date       = date('Y-m-d h:i:s',strtotime($_POST['file_create_date']));

            if(strlen($_FILES['file_type']['name'])>1){
                $upload_file = $this->UploadFile($_FILES['file_type']);
                
                if($upload_file){
                    $file_url   = $upload_file['path'];
                    $file_type  = $upload_file['type'];
                    $file_size  = $upload_file['size'];
					$this->DeleteFile(URL.$file_info[0]['file_url']);
                }else{
					$file_url   = $file_info[0]['file_url'];
                    $file_type  = $file_info[0]['file_type'];
                    $file_size  = $file_info[0]['file_volume'];
				}
            }else{
				$file_url   = $file_info[0]['file_url'];
                $file_type  = $file_info[0]['file_type'];
                $file_size  = $file_info[0]['file_volume'];
			}

            $arr = array(
                'file_name'             => $file_name,
				'file_name_english'		=> $file_name_english,
                'file_url'              => $file_url,
                'file_link'             => $file_link,
                'file_type'             => $file_type,
                'file_volume'           => $file_size,
                'file_categories_id'    => $file_categories_id,
                'file_create_date'      => $file_create_date
            );

            $result = $file->UpdateFile($arr,$file_id);

            if($result){
                $log = new model_log();
                $arr = array(
                    'log_action'        => 'update',
                    'user_id'           => $_SESSION['user_id'],
                    'log_object'        => 'tài nguyên',
                    'log_object_id'     => $file_id,
                    'log_object_name'   => $file_name,
                    'user_name'         => $_SESSION['user_name']
                );
                $log->InsertLog($arr);
                echo '1';
            }
        }
	}
	
	public function editfiles($path){
		if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($edit[0]['permission_detail'],'file')){
                header('Location:'.URL.'admin/');
            }
        }

        $file = new model_file();

		if($file->GetAllFileCategories()){
            $this->view->listcategories = $file->GetAllFileCategories();
        }
		
        if($path){
            if($file->GetFileById($path)){
                $fileinfo = $file->GetFileById($path);
                
				$this->view->fileinfo = $fileinfo;
				
				if($_POST){
					$file_name              = $_POST['file_name'];
					$file_name_english      = $_POST['file_name_english'];
					$file_categories_id     = $_POST['file_categories_id'];
                    $file_link              = $_POST['file_link'];
					
					$arr = array(
						'file_name'             => $file_name,
						'file_name_english'		=> $file_name_english,
						'file_categories_id'    => $file_categories_id,
                        'file_link'             => $file_link
					);

					$result = $file->UpdateFile($arr,$path);

					if($result){
						header('Location:'.URL.$this->view->lang.'/file/');
					}
				}
            }else{
                header('Location:'.URL.$this->view->lang.'/file/');
            }
        }else{
            header('Location:'.URL.$this->view->lang.'/file/');
        }
		
		$this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/file.js'
        );
		
		$this->view->title = 'Trang quản trị ';

        $this->view->layout('layout-admin1');
        $this->view->Render('file/editfile');
        return true;
    }

    public function deletefiles($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($delete[0]['permission_detail'],'file')){
                header('Location:'.URL.'admin/');
            }
        }

        $file = new model_file();

        if($path){
            if($file->GetFileById($path)){
                $fileinfo = $file->GetFileById($path);
                $deletefile = $file->DeleteFile($path);

                if($deletefile){
                    $this->DeleteFile($fileinfo[0]['file_url']);
                    $log = new model_log();
                    $arr = array(
                        'log_action'        => 'delete',
                        'user_id'           => $_SESSION['user_id'],
                        'log_object'        => 'tài nguyên',
                        'log_object_id'     => $path,
                        'log_object_name'   => $fileinfo[0]['file_name'],
                        'user_name'         => $_SESSION['user_name']
                    );
                    $log->InsertLog($arr);
                    header('Location:'.URL.$this->view->lang.'/file/');
                }
            }else{
                header('Location:'.URL.$this->view->lang.'/file/');
            }
        }else{
            header('Location:'.URL.$this->view->lang.'/file/');
        }
    }

    public function deletefiles1(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($delete[0]['permission_detail'],'file')){
                header('Location:'.URL.'admin/');
            }
        }

        $file = new model_file();

        if($_POST){
            $idpost = $_POST['idpost'];
            if($file->GetFileById($idpost)){
                $fileinfo = $file->GetFileById($idpost);
                $deletefile = $file->DeleteFile($idpost);

                if($deletefile){
                    $this->DeleteFile($fileinfo[0]['file_url']);
                    $log = new model_log();
                    $arr = array(
                        'log_action'        => 'delete',
                        'user_id'           => $_SESSION['user_id'],
                        'log_object'        => 'tài nguyên',
                        'log_object_id'     => $fileinfo[0]['file_id'],
                        'log_object_name'   => $fileinfo[0]['file_name'],
                        'user_name'         => $_SESSION['user_name']
                    );
                    $log->InsertLog($arr);
                    echo '1';
                }
            }
        }
    }

    public function addfile(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($create[0]['permission_detail'],'file')){
                header('Location:'.URL.'admin/');
            }
        }

        $file = new model_file();

        if($file->GetAllFileCategories()){
            $this->view->listcategories = $file->GetAllFileCategories();
        }

        $this->view->js = array(
            URL.'public/js/file.js'
        );

        $this->view->layout('layout-admin1');
        $this->view->Render('file/addfile');
        return true;
    }

    public function index(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($create[0]['permission_detail'],'file') || !strpos($delete[0]['permission_detail'],'file') || !strpos($edit[0]['permission_detail'],'file')){
                header('Location:'.URL.'admin/');
            }
        }

        $file = new model_file();

        if($file->GetAllFileCategories()){
            $this->view->listcategories = $file->GetAllFileCategories();
        }

        if($file->GetAllFile()){
            $this->view->listfile = $file->GetAllFile();
        }

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/file.js'
        );

        $this->view->layout('layout-admin1');
        $this->view->Render('file/index');
        return true;
    }
}