<?php
class controller_google extends lib_controller{

    public function __construct() {
        parent::__construct();
        if(lib_session::Logged() == true){

        }
    }

    public function CreateElement( $URL, $Freq, $Priority )
	{
	    $ReturnXML = "<url>\n";
	    $ReturnXML .= "<loc>" . $URL . "</loc>\n";
	    $ReturnXML .= "<changefreq>" . $Freq . "</changefreq>\n";
	    $ReturnXML .= "<priority>" . $Priority . "</priority>\n";
	    $ReturnXML .= "</url>\n";
	    return $ReturnXML;
	}

	public function createsitemap(){
		$news = new model_news();
		$page = new model_page();

		$file = "sitemap.xml";

		$urlwebsite = URL;

		$StaticPages = array(
		    $urlwebsite . "vi/category/news/",
		    $urlwebsite . "vi/category/file/",
		    $urlwebsite . "vi/category/page/",
		    $urlwebsite . "en/category/news/",
		    $urlwebsite . "en/category/file/",
		    $urlwebsite . "en/category/page/"
		);

		$PageXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		$PageXML .= "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";

		$PageXML .= $this->CreateElement($urlwebsite,"always",1.0 );

		if($_POST){
			$prioritynews		= $_POST['priority_news'];
			$prioritycatgory	= $_POST['priority_category'];
			$prioritypage		= $_POST['priority_category'];

			foreach($StaticPages as $Page)
			{
			    $PageXML .= $this->CreateElement($Page,"always",1.0 );
			}

			if($news->SelectAllCategoriesNews()){
	         	$listcatgories =  $news->SelectAllCategoriesNews();
	         	foreach ($listcatgories as $value) {
	         		$Page 	= $urlwebsite ."vi/category/news/". $value["news_categories_url"].'/';
	         		$Page1 	= $urlwebsite ."en/category/news/". $value["news_categories_url_english"].'/';
				    //The date of the last modification of the content, or you can use NOW()
				    $PageXML .= $this->CreateElement($Page,"always",$prioritycatgory);
				    $PageXML .= $this->CreateElement($Page1,"always",$prioritycatgory);
	         	}
	        }

	        if($news->SelectAllNews()){
	            $listnews =  $news->SelectAllNews();
	            foreach ($listnews as $value) {
	         		$Page 	= $urlwebsite ."vi/news/detail/". $value["news_url"].'/';
	         		$Page1 	= $urlwebsite ."en/news/detail/". $value["news_url_english"].'/';
				    //The date of the last modification of the content, or you can use NOW()
				    $PageXML .= $this->CreateElement($Page,"always",$prioritycatgory);
				    $PageXML .= $this->CreateElement($Page1,"always",$prioritycatgory);
	         	}
	        }

	        if($page->SelectAllPage()){
	        	$listpage = $page->SelectAllPage();
	        	foreach ($listpage as $value) {
	         		$Page 	= $urlwebsite ."vi/page/". $value["page_url"].'/';
	         		$Page1 	= $urlwebsite ."en/page/". $value["page_url_english"].'/';
				    //The date of the last modification of the content, or you can use NOW()
				    $PageXML .= $this->CreateElement($Page,"always",$prioritycatgory);
				    $PageXML .= $this->CreateElement($Page1,"always",$prioritycatgory);
	         	}
	        }

	        $PageXML .= "</urlset>";

			$FileHandle = fopen($file,"w");
			fwrite($FileHandle,$PageXML);
			fclose($FileHandle);

			echo '1';
	    }
	}

	public function pingsitemap(){
		$CurlHandle = curl_init();
		curl_setopt($CurlHandle,CURLOPT_URL, "www.google.com/webmasters/tools/ping?sitemap=".URL.'sitemap.xml');
		curl_setopt($CurlHandle,CURLOPT_RETURNTRANSFER,1);

		curl_exec($CurlHandle);

		curl_close($CurlHandle);

		echo '1';
	}

    public function index(){
    	if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

    	$this->view->js = array(
    		URL.'public/js/google.js'
    	);


    	$this->view->layout('layout-admin1');
        $this->view->Render('google/index');
        return true;
    }
}