<?php
class controller_index extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    public function index(){
        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $partner = new model_partner();

        $library = new model_library();
		
		$video = new model_video();

        $slider = new model_slider();

        if($allsetting){
			if($this->view->lang == 'vi'){
				$this->view->title = $allsetting[0]['config_title'];
				$this->view->description = $allsetting[0]['config_description'];
				$this->view->keyword = $allsetting[0]['config_keyword'];
			}else{
				$this->view->title = 'PV Gas D';
				$this->view->description = $allsetting[0]['config_description'];
				$this->view->keyword = $allsetting[0]['config_keyword'];
			}
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
            $this->view->configurl = $allsetting[0]['config_url'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOn()){
            $allmenufather = $model1->SelectAllMenuFatherOn();

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        if($news->SelectTopNewsRelease(12)){
            $listnewsnew = $news->SelectTopNewsRelease(12);

            foreach ($listnewsnew as $key => $value) {
                if($news->GetCategoriesNewsById($value['news_categories_id'])){
                    $newsinfo = $news->GetCategoriesNewsById($value['news_categories_id']);
                    $listnewsnew[$key]['category'] = $newsinfo[0];
                }
            }

            $this->view->listnewsnew = $listnewsnew;
        }

        if($library->SelectAllLibrary()){
            $this->view->listlibrary = $library->SelectAllLibrary();
        }

        if($model->GetAllContact()){

            $listcontact = $model->GetAllContact();

            $this->view->listcontact = $listcontact;

        }

        $partner = new model_partner();

        if($partner->SelectAllPartner()){
            $listpartner = $partner->SelectAllPartner();
            $this->view->listpartner = $listpartner;
        }

        if($slider->SelectAllSliderStatusByKind(1)){
            $this->view->listslider1 = $slider->SelectAllSliderStatusByKind(1);
        }

        if($slider->SelectAllSliderStatusByKind(2)){
            $this->view->listslider2 = $slider->SelectAllSliderStatusByKind(2);
        }

        if($slider->SelectAllSliderStatusByKind(3)){
            $this->view->listslider3 = $slider->SelectAllSliderStatusByKind(3);
        }

        $this->view->css = array(
            URL.'public/flexslider/flexslider.css'
        );

        $this->view->js = array(
            URL.'public/flexslider/jquery.flexslider.js',
            URL.'public/js/config-flexider.js'
        );

        $useragent=$_SERVER['HTTP_USER_AGENT'];
        

            $this->view->layout('layout');
            $this->view->Render('index/index');
            return true;
    }
}