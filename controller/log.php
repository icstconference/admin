<?php
class controller_log extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    public function index(){
    	$log = new model_log();

    	if($log->SelectAllLog()){
    		$this->view->listlog = $log->SelectAllLog();
    	}

    	$this->view->layout('layout-admin1');
        $this->view->Render('log/index');
    }
}