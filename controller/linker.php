<?php
class controller_linker extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    public function index(){
    	if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login/');
        }

    	$linker = new model_linker();

    	if($linker->SelectAlllinker()){
    		$listlinker = $linker->SelectAlllinker();
    		$this->view->listlinker = $listlinker;
    	}

        if($_POST){
            $linker_name 		 = $_POST['linker_name'];
			$linker_name_english = $_POST['linker_name_english'];
            $linker_link 		 = $_POST['linker_link'];
			$linker_sort 		 = $_POST['linker_sort'];

            $data = array(
                'linker_name'   		=> $linker_name,
				'linker_name_english'	=> $linker_name_english,
                'linker_url'    		=> $linker_link,
				'linker_sort'			=> $linker_sort	
            );

            $result = $linker->Insertlinker($data);
            if($result){
                header('Location:'.URL.'linker');
            }
        }

        $this->view->title = 'Trang quản trị';

    	$this->view->js = array(
            URL.'public/js/email.js'
        );

    	$this->view->layout('layout-admin');
        $this->view->Render('linker/index');
        return true;
    }

    public function editlinker($path){
        if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login/');
        }

        $linker = new model_linker();
        
        if($path){
            if($linker->GetlinkerById($path)){
                $linkerinfo = $linker->GetlinkerById($path);

                $this->view->linkerinfo = $linkerinfo;

                if($_POST){
                    $linker_name 		 = $_POST['linker_name'];
					$linker_name_english = $_POST['linker_name_english'];
                    $linker_link 		 = $_POST['linker_link'];
					$linker_sort 		 = $_POST['linker_sort'];

                    $data = array(
                        'linker_name'   		=> addslashes($linker_name),
						'linker_name_english'	=> addslashes($linker_name_english),
                        'linker_url'    		=> $linker_link,
						'linker_sort'			=> $linker_sort	
						
                    );

                    $result = $linker->Updatelinker($data,$path);
                    if($result){
                        header('Location:'.URL.'linker');
                    }
                }
            }else{
                 header('Location:'.URL.'linker');
            }
        }else{
            header('Location:'.URL.'linker');
        }

        $this->view->title = 'Trang quản trị';

        $this->view->js = array(
            URL.'public/js/email.js'
        );

        $this->view->layout('layout-admin');
        $this->view->Render('linker/editlinker');
        return true;
    }

    public function deletelinker($path){
        if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login/');
        }
        $linker = new model_linker();
        
        if($path){
            if($linker->GetlinkerById($path)){
                $deletelinker = $linker->DeletelinkerById($path);
                if($deletelinker){
                    header('Location:'.URL.'linker');
                }
            }else{
                 header('Location:'.URL.'linker');
            }
        }else{
            header('Location:'.URL.'linker');
        }
    }
}