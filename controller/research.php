<?php
class controller_research extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    public function index($path){
    	$model = new model_setting();

        $allsetting = $model->GetAllSetting();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
			$this->view->aboutus = $allsetting[0]['config_url_aboutus'];
            $this->view->laboratories = $allsetting[0]['config_url_laboratories'];
			$this->view->emaillink = $allsetting[0]['config_url_email'];
            $this->view->internal = $allsetting[0]['config_url_internal'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOnByPostion('all','ngang')){
            $allmenufather = $model1->SelectAllMenuFatherOnByPostion('all','ngang');

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        if($model1->SelectAllMenuFatherOnByPostion('product','doc')){
            $allmenufatherproduct = $model1->SelectAllMenuFatherOnByPostion('product','doc');

            foreach($allmenufatherproduct as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufatherproduct[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenuproduct = $allmenufatherproduct;
        }

        $model4 = new model_news();

        if($model4->SelectAllNewsCategoriesChildByFatherId(10)){
            $this->view->listcategories = $model4->SelectAllNewsCategoriesChildByFatherId(10);
        }

        if($path){
            $part = explode('_',$path);
            $id = $part[count($part)-1];
            $result = $model4->SelectNewsById($id);

            if($result){

                $view = intval($result[0]['view']) + 1;

                $this->view->newsinfo = $result;

                $catnews = $model4->GetCategoriesById($result[0]['news_categories_id']);

                $this->view->categoriesinfo = $catnews;

				if($this->view->lang == 'vi'){
					$this->view->title = $result[0]['news_name'].'-'.$allsetting[0]['config_title'];
					$this->view->description = $result[0]['news_name'].'-'.$allsetting[0]['config_description'];
					$this->view->keyword = $result[0]['news_name'].','.$allsetting[0]['config_keyword'];
				}else{
					$this->view->title = $result[0]['news_name_english'].'-'.$allsetting[0]['config_title'];
					$this->view->description = $result[0]['news_name_english'].'-'.$allsetting[0]['config_description'];
					$this->view->keyword = $result[0]['news_name_english'].','.$allsetting[0]['config_keyword'];
				}
                if($catnews){
                    $this->view->catenews = $catnews;
                }

                $arr = array(
                    'view'  =>$view
                );

                $update = $model4->UpdateNews($arr,$id);
            }
        }

        $this->view->layout('layout');
        $this->view->Render('research/index');
        return true;
    }

    public function onlineseminar($path){
        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
			$this->view->aboutus = $allsetting[0]['config_url_aboutus'];
            $this->view->laboratories = $allsetting[0]['config_url_laboratories'];
			$this->view->emaillink = $allsetting[0]['config_url_email'];
            $this->view->internal = $allsetting[0]['config_url_internal'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOnByPostion('all','ngang')){
            $allmenufather = $model1->SelectAllMenuFatherOnByPostion('all','ngang');

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        if($model1->SelectAllMenuFatherOnByPostion('product','doc')){
            $allmenufatherproduct = $model1->SelectAllMenuFatherOnByPostion('product','doc');

            foreach($allmenufatherproduct as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufatherproduct[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenuproduct = $allmenufatherproduct;
        }

        $model4 = new model_news();

        if($model4->SelectAllNewsCategoriesChildByFatherId(10)){
            $this->view->listcategories = $model4->SelectAllNewsCategoriesChildByFatherId(10);
        }

        $form = new model_form();

        if($path){
            $result = $model4->GetCategoriesById($path);

            if($result){

                $this->view->categoriesinfo = $result;

                if($this->view->lang == 'vi'){
                    $this->view->title = 'Nghiên cứu viên '.$result[0]['news_categories_name'].'-'.$allsetting[0]['config_title'];
                    $this->view->description = 'Nghiên cứu viên '.$result[0]['news_categories_name'].'-'.$allsetting[0]['config_description'];
                    $this->view->keyword = 'Nghiên cứu viên '.$result[0]['news_categories_name'].','.$allsetting[0]['config_keyword'];
                }else{
                    $this->view->title = 'Members '.$result[0]['news_categories_name_english'].'-'.$allsetting[0]['config_title'];
                    $this->view->description = 'Members '.$result[0]['news_categories_name_english'].'-'.$allsetting[0]['config_description'];
                    $this->view->keyword = 'Members '.$result[0]['news_categories_name_english'].','.$allsetting[0]['config_keyword'];
                }

                if($form->GetForm2ByCatId($result[0]['news_categories_id'])){
                    $listform = $form->GetForm2ByCatId($result[0]['news_categories_id']);

                    $formid = $listform[0]['form_id'];

                    $listform2 = $form->SelectAllForm2ByFormId($formid);

                    $this->view->listform2 = $listform2;
                }
            }
        }

        $this->view->js = array(
            URL.'public/js/formindex.js'
        );

        $this->view->layout('layout');
        $this->view->Render('research/onlineseminar');
        return true;
    }

    public function publiccation($path){
        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
			$this->view->aboutus = $allsetting[0]['config_url_aboutus'];
            $this->view->laboratories = $allsetting[0]['config_url_laboratories'];
			$this->view->emaillink = $allsetting[0]['config_url_email'];
            $this->view->internal = $allsetting[0]['config_url_internal'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOnByPostion('all','ngang')){
            $allmenufather = $model1->SelectAllMenuFatherOnByPostion('all','ngang');

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        if($model1->SelectAllMenuFatherOnByPostion('product','doc')){
            $allmenufatherproduct = $model1->SelectAllMenuFatherOnByPostion('product','doc');

            foreach($allmenufatherproduct as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufatherproduct[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenuproduct = $allmenufatherproduct;
        }

        $model4 = new model_news();

        if($model4->SelectAllNewsCategoriesChildByFatherId(10)){
            $this->view->listcategories = $model4->SelectAllNewsCategoriesChildByFatherId(10);
        }

        $form = new model_form();

        if($path){
            $result = $model4->GetCategoriesById($path);

            if($result){

                $this->view->categoriesinfo = $result;

                if($this->view->lang == 'vi'){
                    $this->view->title = 'Nghiên cứu viên '.$result[0]['news_categories_name'].'-'.$allsetting[0]['config_title'];
                    $this->view->description = 'Nghiên cứu viên '.$result[0]['news_categories_name'].'-'.$allsetting[0]['config_description'];
                    $this->view->keyword = 'Nghiên cứu viên '.$result[0]['news_categories_name'].','.$allsetting[0]['config_keyword'];
                }else{
                    $this->view->title = 'Members '.$result[0]['news_categories_name_english'].'-'.$allsetting[0]['config_title'];
                    $this->view->description = 'Members '.$result[0]['news_categories_name_english'].'-'.$allsetting[0]['config_description'];
                    $this->view->keyword = 'Members '.$result[0]['news_categories_name_english'].','.$allsetting[0]['config_keyword'];
                }

                if($form->SelectAllFormCategories()){
                    $this->view->listformcategories = $form->SelectAllFormCategories();
                }
				
                if($form->GetForm1ByCatId($result[0]['news_categories_id'])){

                    $listform = $form->GetForm1ByCatId($result[0]['news_categories_id']);
					
                    $formid = $listform[0]['form_id'];

                    $listform1 = $form->SelectAllForm1ByFormId($formid);

                    $this->view->listform1 = $listform1;
                }

            }
        }

        $this->view->js = array(
            URL.'public/js/formindex.js'
        );

        $this->view->layout('layout');
        $this->view->Render('research/publiccation');
        return true;
    }

    public function personal($path){
        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
			$this->view->aboutus = $allsetting[0]['config_url_aboutus'];
            $this->view->laboratories = $allsetting[0]['config_url_laboratories'];
			$this->view->emaillink = $allsetting[0]['config_url_email'];
            $this->view->internal = $allsetting[0]['config_url_internal'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOnByPostion('all','ngang')){
            $allmenufather = $model1->SelectAllMenuFatherOnByPostion('all','ngang');

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        if($model1->SelectAllMenuFatherOnByPostion('product','doc')){
            $allmenufatherproduct = $model1->SelectAllMenuFatherOnByPostion('product','doc');

            foreach($allmenufatherproduct as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufatherproduct[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenuproduct = $allmenufatherproduct;
        }

        $model4 = new model_news();

        $personal = new model_personal();

        if($model4->SelectAllNewsCategoriesChildByFatherId(10)){
            $this->view->listcategories = $model4->SelectAllNewsCategoriesChildByFatherId(10);
        }

        if($path){
            $result = $model4->GetCategoriesById($path);

            if($result){

                $this->view->categoriesinfo = $result;

                if($this->view->lang == 'vi'){
                    $this->view->title = 'Nghiên cứu viên '.$result[0]['news_categories_name'].'-'.$allsetting[0]['config_title'];
                    $this->view->description = 'Nghiên cứu viên '.$result[0]['news_categories_name'].'-'.$allsetting[0]['config_description'];
                    $this->view->keyword = 'Nghiên cứu viên '.$result[0]['news_categories_name'].','.$allsetting[0]['config_keyword'];
                }else{
                    $this->view->title = 'Members '.$result[0]['news_categories_name_english'].'-'.$allsetting[0]['config_title'];
                    $this->view->description = 'Members '.$result[0]['news_categories_name_english'].'-'.$allsetting[0]['config_description'];
                    $this->view->keyword = 'Members '.$result[0]['news_categories_name_english'].','.$allsetting[0]['config_keyword'];
                }

                if($personal->GetAllPersonalLabByCatId($result[0]['news_categories_id'])){
                    $listpersonal = $personal->GetAllPersonalLabByCatId($result[0]['news_categories_id']);

                    $this->view->listpersonal = $listpersonal;
                }
            }
        }

        $this->view->layout('layout');
        $this->view->Render('research/personal');
        return true;
    }
}