<?php
class controller_event extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    /*
     * Các function tạo hình ảnh
     */

    function UploadImage(array $data) {
        if ((($data["type"] == "image/gif") || ($data["type"] == "image/jpeg")
                || ($data["type"] == "image/jpg") || ($data["type"] == "image/pjpeg")
                || ($data["type"] == "image/x-png") || ($data["type"] == "image/png"))
            && ($data["size"] < 4000000)) {
            if ($data["error"] == 0) {
                $path = "public/upload/images/" . uniqid() . '.jpg';
                move_uploaded_file($data["tmp_name"], $path);
                return $path;
            }
        }
        return false;
    }

    function SaveThumbImage($path) {
        try {
            $thumb = PhpThumbFactory::create($path);
        } catch (Exception $e) {
            // handle error here however you'd like
        }
        $id=  uniqid();
        $name_file = $id.'.jpg';
        $thumb->resize(400);
        $thumb->adaptiveResize(300, 300);
        $thumb->save('public/upload/images/event/' . $name_file, 'jpg');

        $path_result = array(
            'image' => $path,
            'thumb' => 'public/upload/images/event/' . $name_file,
        );
        return $path_result;
    }

    protected function DeleteFile($file_path) {
        $path = str_replace(URL, '', $file_path);
        if (file_exists($path)) {
            unlink($path);
            return true;
        }
        return false;
    }

    public function detail($path = null){

    	$model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $partner = new model_partner();

        $library = new model_library();

        $event = new model_event();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOnByPostion('all','ngang')){
            $allmenufather = $model1->SelectAllMenuFatherOnByPostion('all','ngang');

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        if($model1->SelectAllMenuFatherOnByPostion('product','doc')){
            $allmenufatherproduct = $model1->SelectAllMenuFatherOnByPostion('product','doc');

            foreach($allmenufatherproduct as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufatherproduct[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenuproduct = $allmenufatherproduct;
        }

        if($advertise->GetAdvertiseByPosition(1)){
            $this->view->listadvertise1 = $advertise->GetAdvertiseByPosition(1);
        }

        if($advertise->GetAdvertiseByPosition(2)){
            $this->view->listadvertise2 = $advertise->GetAdvertiseByPosition(2);
        }

        if($advertise->GetAdvertiseByPosition(3)){
            $this->view->listadvertise3 = $advertise->GetAdvertiseByPosition(3);
        }

        if($news->SelectTopNewsView(4)){
            $this->view->listnewsview = $news->SelectTopNewsView(4);
        }

        if($news->SelectTopNewsRelease(4)){
            $this->view->listnewsrelease = $news->SelectTopNewsRelease(4);
        }

        if($linker->SelectAlllinker()){
            $this->view->listlinker = $linker->SelectAlllinker();
        }

        if($news->SelectTopNewsRelease(5)){
            $this->view->listnewsnew = $news->SelectTopNewsRelease(5);
        }

        if($news->SelectAllNewsByCatId(6)){
            $newschuyenganh = $news->SelectAllNewsByCatId(6);

            foreach ($newschuyenganh as $key=>$value) {
                if($comment->NumCommentByPostId($value['news_id'])){
                    $numcomment = $comment->NumCommentByPostId($value['news_id']);
                    $newschuyenganh[$key]['news_comment'] = $numcomment[0]['numcomment'];
                }
            }

            $this->view->newschuyenganh = $newschuyenganh;
        }

        if($news->SelectAllNewsByCatId(7)){
            $newshoi = $news->SelectAllNewsByCatId(7);
            
            foreach ($newshoi as $key=>$value) {
                if($comment->NumCommentByPostId($value['news_id'])){
                    $numcomment = $comment->NumCommentByPostId($value['news_id']);
                    $newshoi[$key]['news_comment'] = $numcomment[0]['numcomment'];
                }
            }

            $this->view->newshoi = $newshoi;
        }

        if($question->SelectAllQuestion()){
            $this->view->listquestion = $question->SelectAllQuestion();
        }

        if($partner->SelectAllPartner()){
            $this->view->listpartner = $partner->SelectAllPartner();
        }

        if($library->SelectAllLibrary()){
            $this->view->listlibrary = $library->SelectAllLibrary();
        }

        if($path){
            $part = explode('-',$path);
            $id = $part[count($part)-1];
            $result = $event->GetEventById($id);

            if($result){

                $this->view->title = $result[0]['event_name'].'-'.$allsetting[0]['config_title'];
                $this->view->description = $result[0]['event_name'].'-'.$allsetting[0]['config_description'];
                $this->view->keyword = $result[0]['event_name'].','.$allsetting[0]['config_keyword'];

                foreach ($result as $key => $value) {
                    if($event->GetAllEventDetailByEventId($id)){
                        $result[$key]['eventdetail'] = $event->GetAllEventDetailByEventId($id);
                    }
                }

                $this->view->eventinfo = $result;
            }
        }

        $this->view->layout('layout');
        $this->view->Render('event/detail');
        return true;
    }

    public function createevent(){
        require_once 'plugin/phpthumb/ThumbLib.inc.php';
        require_once 'plugin/url-seo/url-seo.php';
        $event = new model_event();

        if($_POST){
            $event_name                 = $_POST['event_name'];
            $event_short_description    = $_POST['event_short_description'];
            $event_description          = $_POST['event_description'];
            $event_date_start           = date('Y-m-d',strtotime($_POST['event_date_start']));
            $event_date_end             = date('Y-m-d',strtotime($_POST['event_date_end']));

            if(strlen($_FILES['event_img']['name'])>1){
                $upload_img = $this->UploadImage($_FILES['event_img']);

                if($upload_img){
                    $event_img   = URL.$upload_img;
                }else{
                    $event_img = '';
                }
            }else{
                $event_img = '';
            }

            if(strlen($_FILES['event_img1']['name'])>1){
                $upload_img1 = $this->UploadImage($_FILES['event_img1']);

                if($upload_img1){
                    $event_img1   = URL.$upload_img1;
                }else{
                    $event_img1 = '';
                }
            }else{
                $event_img1 = '';
            }

            $data = array(
                'event_name'                => addslashes($event_name),
                'event_short_description'   => addslashes($event_short_description),
                'event_description'         => addslashes($event_description),
                'event_image'               => $event_img,
                'event_image1'              => $event_img1,
                'event_start'               => $event_date_start,
                'event_end'                 => $event_date_end,
            );


            $result = $event->InsertEvent($data);
            if($result){
                $event_url = post_slug(strip_tags($event_name)).'-event-'.$result;
                $data1 = array(
                    'event_url' => $event_url
                );
                $update = $event->UpdateEvent($data1,$result);
                if($update){
                    echo $result;
                }
            }
        }
    }

    public function updateevent(){
        require_once 'plugin/phpthumb/ThumbLib.inc.php';
        require_once 'plugin/url-seo/url-seo.php';

        $event = new model_event();

        if($_POST){
            $event_id                   = $_POST['event_id'];
            $eventinfo                  = $event->GetEventById($event_id);
            $event_name                 = $_POST['event_name'];
            $event_short_description    = $_POST['event_short_description'];
            $event_description          = $_POST['event_description'];
            $event_date_start           = date('Y-m-d',strtotime($_POST['event_date_start']));
            $event_date_end             = date('Y-m-d',strtotime($_POST['event_date_end']));
            $event_url                  = post_slug(strip_tags($event_name)).'-event-'.$event_id;

            if(strlen($_FILES['event_img']['name'])>1){
                $upload_img = $this->UploadImage($_FILES['event_img']);

                if($upload_img){
                    $event_img   = URL.$upload_img;
                    $this->DeleteFile($eventinfo[0]['event_image']);
                }else{
                    $event_img = $eventinfo[0]['event_image'];
                }
            }else{
                $event_img = $eventinfo[0]['event_image'];
            }

            if(strlen($_FILES['event_img1']['name'])>1){
                $upload_img1 = $this->UploadImage($_FILES['event_img1']);

                if($upload_img1){
                    $event_img1   = URL.$upload_img1;
                    $this->DeleteFile($eventinfo[0]['event_image1']);
                }else{
                    $event_img1 = $eventinfo[0]['event_image1'];
                }
            }else{
                $event_img1 = $eventinfo[0]['event_image1'];
            }

            $data = array(
                'event_name'                => addslashes($event_name),
                'event_short_description'   => addslashes($event_short_description),
                'event_description'         => addslashes($event_description),
                'event_image'               => $event_img,
                'event_image1'              => $event_img1,
                'event_start'               => $event_date_start,
                'event_end'                 => $event_date_end,
                'event_url'                 => $event_url,
            );

            $result = $event->UpdateEvent($data,$event_id);
            if($result){
                echo 1;
            }
        }
    }

    public function index($page = 1){

        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $partner = new model_partner();

        $library = new model_library();

        $event = new model_event();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOnByPostion('all','ngang')){
            $allmenufather = $model1->SelectAllMenuFatherOnByPostion('all','ngang');

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        if($model1->SelectAllMenuFatherOnByPostion('product','doc')){
            $allmenufatherproduct = $model1->SelectAllMenuFatherOnByPostion('product','doc');

            foreach($allmenufatherproduct as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufatherproduct[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenuproduct = $allmenufatherproduct;
        }

        if($advertise->GetAdvertiseByPosition(1)){
            $this->view->listadvertise1 = $advertise->GetAdvertiseByPosition(1);
        }

        if($advertise->GetAdvertiseByPosition(2)){
            $this->view->listadvertise2 = $advertise->GetAdvertiseByPosition(2);
        }

        if($advertise->GetAdvertiseByPosition(3)){
            $this->view->listadvertise3 = $advertise->GetAdvertiseByPosition(3);
        }

        if($news->SelectTopNewsView(4)){
            $this->view->listnewsview = $news->SelectTopNewsView(4);
        }

        if($news->SelectTopNewsRelease(4)){
            $this->view->listnewsrelease = $news->SelectTopNewsRelease(4);
        }

        if($linker->SelectAlllinker()){
            $this->view->listlinker = $linker->SelectAlllinker();
        }

        if($news->SelectTopNewsRelease(5)){
            $this->view->listnewsnew = $news->SelectTopNewsRelease(5);
        }

        if($news->SelectAllNewsByCatId(6)){
            $newschuyenganh = $news->SelectAllNewsByCatId(6);

            foreach ($newschuyenganh as $key=>$value) {
                if($comment->NumCommentByPostId($value['news_id'])){
                    $numcomment = $comment->NumCommentByPostId($value['news_id']);
                    $newschuyenganh[$key]['news_comment'] = $numcomment[0]['numcomment'];
                }
            }

            $this->view->newschuyenganh = $newschuyenganh;
        }

        if($news->SelectAllNewsByCatId(7)){
            $newshoi = $news->SelectAllNewsByCatId(7);
            
            foreach ($newshoi as $key=>$value) {
                if($comment->NumCommentByPostId($value['news_id'])){
                    $numcomment = $comment->NumCommentByPostId($value['news_id']);
                    $newshoi[$key]['news_comment'] = $numcomment[0]['numcomment'];
                }
            }

            $this->view->newshoi = $newshoi;
        }

        if($question->SelectAllQuestion()){
            $this->view->listquestion = $question->SelectAllQuestion();
        }

        if($partner->SelectAllPartner()){
            $this->view->listpartner = $partner->SelectAllPartner();
        }

        if($library->SelectAllLibrary()){
            $this->view->listlibrary = $library->SelectAllLibrary();
        }


        $this->view->title = 'Sự kiện & hội thảo '.'-'.$allsetting[0]['config_title'];
        $this->view->description = 'Sự kiện & hội thảo '.'-'.$allsetting[0]['config_description'];
        $this->view->keyword = 'Sự kiện & hội thảo '.','.$allsetting[0]['config_keyword'];
                
        if($page == 1){
            $iteminpage = 9;
        }

        if($page > 1){
            $iteminpage = 8;
        }

        $pagination = new lib_pagination();
        $pagination->SetCurrentPage($page);
        $pagination->SetRowInPage($iteminpage);
        $pagination->SetUrl(URL . 'event/index/@page');
        $pagination->SetRange(4);
        $pagination->SetLanguage(array(
            'first'     => '<<',
            'previous'  => '<',
            'next'      => '>',
            'last'      => '>>'
        ));

        $paginator  = $pagination->BuildPagination($this->GetBlogDB1());

        $this->view->paginator  = $paginator['paginator'];

        $this->view->listnew   = $event->PaginationEvent($page,$iteminpage);

        $this->view->numpage = $page;

        $this->view->layout('layout');
        $this->view->Render('category/event');
        return true;
    }

    private function GetBlogDB1() {
        $db = new lib_db();
        return $db->query('select *
            from tbl_event');
    }

    public function addevent(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $event = new model_event();

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/event.js'
        );

        if($event->GetAllEvent()){
            $this->view->listevent = $event->GetAllEvent();
        }

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin1');
        $this->view->Render('event/addevent');
        return true;
    }

    public function event(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $event = new model_event();

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/event.js'
        );

        if($event->GetAllEvent()){
            $this->view->listevent = $event->GetAllEvent();
        }

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin1');
        $this->view->Render('event/event');
        return true;
    }

    public function editeeventdetail($path){
        if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login/');
        }

        $event = new model_event();

        if($path){
            if($event->GetEventDetailById($path)){
                $eventdetailinfo = $event->GetEventDetailById($path);

                $this->view->eventdetailinfo = $eventdetailinfo;

                $eventinfo = $event->GetEventById($eventdetailinfo[0]['event_id']);

                $this->view->eventinfo = $eventinfo;

                if($_POST){
                    $event_id                   = $eventdetailinfo[0]['event_id'];
                    $event_detail_name          = $_POST['event_detail_name'];
                    $event_detail_start         = $_POST['event_detail_start'];
                    $event_detail_end           = $_POST['event_detail_end'];
                    $event_detail_description   = $_POST['event_detail_description'];
                
                    $data = array(
                        'event_id'                  => $event_id,
                        'event_detail_name'         => $event_detail_name,
                        'event_detail_start'        => $event_detail_start,
                        'event_detail_end'          => $event_detail_end,
                        'event_detail_description'  => $event_detail_description,
                    );

                    $result = $event->UpdateEventDetail($data,$path);

                    if($result){
                        header('Location:'.URL.'event/editevent/'.$path);
                    }
                }
            }else{
                header('Location:'.URL.'event');
            }
        }else{
            header('Location:'.URL.'event');
        }

        $this->view->title = 'Trang quản trị';

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/event.js'
        );

        $this->view->layout('layout-admin');
        $this->view->Render('event/editeventdetail');
        return true;
    }

    public function editevent($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $event = new model_event();

        if($path){
            if($event->GetEventById($path)){
                $eventinfo = $event->GetEventById($path);

                if($event->GetAllEventDetailByEventId($path)){
                    $this->view->listeventdetail = $event->GetAllEventDetailByEventId($path);
                }

                $this->view->eventinfo = $eventinfo;

                if($_POST){
                    $event_id                   = $path;
                    $event_detail_name          = $_POST['event_detail_name'];
                    $event_detail_start         = $_POST['event_detail_start'];
                    $event_detail_end           = $_POST['event_detail_end'];
                    $event_detail_description   = $_POST['event_detail_description'];
                
                    $data = array(
                        'event_id'                  => $event_id,
                        'event_detail_name'         => $event_detail_name,
                        'event_detail_start'        => $event_detail_start,
                        'event_detail_end'          => $event_detail_end,
                        'event_detail_description'  => $event_detail_description,
                    );

                    $result = $event->InsertEventDetail($data);

                    if($result){
                        header('Location:'.URL.'event/editevent/'.$path);
                    }
                }
            }else{
                header('Location:'.URL.'event');
            }
        }else{
            header('Location:'.URL.'event');
        }

        $this->view->title = 'Trang quản trị';

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/event.js'
        );

        $this->view->layout('layout-admin1');
        $this->view->Render('event/editevent');
        return true;
    }

    public function deleteeventdetail($path){
        if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login/');
        }

        $event = new model_event();

        if($path){
            if($event->GetEventDetailById($path)){
                $eventdetailinfo = $event->GetEventDetailById($path);

                $eventid = $eventdetailinfo[0]['event_id'];

                $delete = $event->DeleteEventDetail($path);

                if($delete){
                    header('Location:'.URL.'event/editevent/'.$eventid);
                }
            }else{
                header('Location:'.URL.'event');
            }
        }else{
            header('Location:'.URL.'event');
        }
    }

    public function deletevent($path){
        if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login/');
        }

        $event = new model_event();

        if($path){
            if($event->GetEventById($path)){
                $eventinfo = $event->GetEventById($path);

                $eventimage = $eventinfo[0]['event_image'];

                $delete = $event->DeleteEvent($path);
                if($delete){
                    $this->DeleteFile($eventimage);
                }
                header('Location:'.URL.'event/event/');
            }else{
                header('Location:'.URL.'event/event/');
            }
        }else{
            header('Location:'.URL.'event/event/');
        }
    }
}