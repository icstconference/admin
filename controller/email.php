<?php
class controller_email extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    /*
     * Các function tạo hình ảnh
     */

    public function index(){
    	if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login/');
        }

    	$email = new model_email();

    	if($email->SelectAllEmail()){
    		$listemail = $email->SelectAllEmail();
    		$this->view->listemail = $listemail;
    	}

        $this->view->title = 'Trang quản trị';

    	$this->view->js = array(
            URL.'public/js/email.js'
        );

    	$this->view->layout('layout-admin');
        $this->view->Render('email/index');
        return true;
    }

    public function deleteemail($path){
		$email = new model_email();    	

    	if($path){
    		if($email->GetEmailById($path)){
    			$delete = $email->DeleteEmailById($path);
    			if($delete){
    				header('Location:'.URL.'email');
    			}
    		}else{
    			header('Location:'.URL.'email');
    		}
    	}else{
    		header('Location:'.URL.'email');
    	}
    }

    public function createemail(){
        $email = new model_email();

        if($_POST){
            $email_register = $_POST['email_register'];

            $data = array(
                'email_address' => $email_register
            );

            $result = $email->InsertEmail($data);

            if($result){
                echo "<script>
                    alert('Đăng kí nhận email thành công !!');
                </script>";
                header('Location:'.URL);
            }
        }
    }
}