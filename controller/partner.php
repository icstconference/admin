<?php
class controller_partner extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    /*
     * Các function tạo hình ảnh
     */

    function UploadImage(array $data) {
        if ((($data["type"] == "image/gif") || ($data["type"] == "image/jpeg")
                || ($data["type"] == "image/jpg") || ($data["type"] == "image/pjpeg")
                || ($data["type"] == "image/x-png") || ($data["type"] == "image/png"))
            && ($data["size"] < 4000000)) {
            if ($data["error"] == 0) {
                $path = "public/upload/images/partner/" . uniqid() . '.jpg';
                move_uploaded_file($data["tmp_name"], $path);
                return $path;
            }
        }
        return false;
    }

    function SaveThumbImage($path) {
        try {
            $thumb = PhpThumbFactory::create($path);
        } catch (Exception $e) {
            // handle error here however you'd like
        }
        $id=  uniqid();
        $name_file = $id.'.jpg';
        $thumb->resize(400);
        $thumb->adaptiveResize(300, 300);
        $thumb->save('public/upload/images/news/' . $name_file, 'jpg');

        $path_result = array(
            'image' => $path,
            'thumb' => 'public/upload/images/news/' . $name_file,
        );
        return $path_result;
    }

    protected function DeleteFile($file_path) {
        $path = str_replace(URL, '', $file_path);
        if (file_exists($path)) {
            unlink($path);
            return true;
        }
        return false;
    }

    public function index(){
    	if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $partner = new model_partner();

        if($partner->SelectAllPartner()){
        	$listpartner = $partner->SelectAllPartner();
        	$this->view->listpartner = $listpartner;
        }

        $event = new model_event();

        if($event->GetAllEvent()){
            $this->view->listevent = $event->GetAllEvent();
        }

        if($partner->SelectAllPartnerCategories()){
            $listcategories = $partner->SelectAllPartnerCategories();
            $this->view->listcategories = $listcategories;
        }

        $this->view->js = array(
            URL.'public/js/partner.js'
        );

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin1');
        $this->view->Render('partner/index');
        return true;
    }

    public function categories(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $partner = new model_partner();

        if($partner->SelectAllPartnerCategories()){
            $listcategories = $partner->SelectAllPartnerCategories();
            $this->view->listcategories = $listcategories;
        }

        if($_POST){
            $categories_name = $_POST['sponsor_type'];

            $data = array(
                'categories_name'   => $categories_name
            );

            $result = $partner->InsertPartnerCategories($data);

            if($result){
                header('Location:'.URL.'partner/categories/');
            }
        }

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin1');
        $this->view->Render('partner/categories');
        return true;
    }

    public function editcategories($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $partner = new model_partner();

        if($partner->SelectAllPartnerCategories()){
            $listcategories = $partner->SelectAllPartnerCategories();
            $this->view->listcategories = $listcategories;
        }

        if($path){
            $cateogriesinfo = $partner->GetPartnerCategoriesById($path);

            $this->view->cateogriesinfo = $cateogriesinfo;

            if($cateogriesinfo){
                if($_POST){
                    $categories_name = $_POST['sponsor_type'];

                    $data = array(
                        'categories_name'   => $categories_name
                    );

                    $result = $partner->UpdatePartnerCategories($data,$path);

                    if($result){
                        header('Location:'.URL.'partner/categories/');
                    }
                }
            }
        }else{
            header('Location:'.URL.'partner/categories/');
        }

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin1');
        $this->view->Render('partner/editcategories');
        return true;
    }

    public function deletecategories($path){
        $partner = new model_partner();

        if($path){
            $cateogriesinfo = $partner->GetPartnerCategoriesById($path);

            if($cateogriesinfo){
                    $delete = $partner->DeletePartnerCategories($path);

                    if($delete){
                        header('Location:'.URL.'partner/categories/');
                    }
            }else{
                header('Location:'.URL.'partner/categories/');
            }
        }else{
            header('Location:'.URL.'partner/categories/');
        }
    }

    public function editpartner($path){
    	if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $partner = new model_partner();

        $event = new model_event();

        if($event->GetAllEvent()){
            $this->view->listevent = $event->GetAllEvent();
        }

        if($partner->SelectAllPartnerCategories()){
            $listcategories = $partner->SelectAllPartnerCategories();
            $this->view->listcategories = $listcategories;
        }

        if($path){
        	if($partner->GetPartnerById($path)){
        		$partnerinfo = $partner->GetPartnerById($path);

        		$this->view->partnerinfo = $partnerinfo;


        	}else{
        		header('Location:'.URL.'partner');
        	}
        }else{
        	header('Location:'.URL.'partner');
        }

        $this->view->js = array(
            URL.'public/js/partner.js'
        );

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin1');
        $this->view->Render('partner/editpartner');
        return true;
    }

    public function createpartner(){
    	require_once 'plugin/phpthumb/ThumbLib.inc.php';
        require_once 'plugin/url-seo/url-seo.php';

        $partner = new model_partner();

        if($_POST){
            $partner_name           = $_POST['partner_name'];
            $partner_url            = $_POST['partner_url'];
            $event_id               = $_POST['partner_event'];
            $partner_type           = $_POST['partner_type'];

            if(strlen($_FILES['partner_img']['name'])>1){
                $upload_img = $this->UploadImage($_FILES['partner_img']);

                if($upload_img){
                    $partner_img = URL.$upload_img;
                }else{
                    $partner_img = '';
                }
            }else{
                $partner_img = '';
            }

            $data = array(
                'partner_name'          => $partner_name,
                'event_id'              => $event_id,
                'partner_type'          => $partner_type,
                'partner_url'           => $partner_url,
                'partner_image'         => $partner_img,
            );

            $result = $partner->InsertPartner($data);
            if($result){
                echo 1;
            }
        }
    }

    public function updatepartner(){
    	require_once 'plugin/phpthumb/ThumbLib.inc.php';
        require_once 'plugin/url-seo/url-seo.php';

        $partner = new model_partner();

        if($_POST){
        	$partner_id   = $_POST['partner_id'];
        	$partnerinfo  = $partner->GetPartnerById($partner_id);
            $partner_name           = $_POST['partner_name'];
            $partner_url            = $_POST['partner_url'];
            $event_id               = $_POST['partner_event'];
            $partner_type           = $_POST['partner_type'];

            if(strlen($_FILES['partner_img']['name'])>1){
                $upload_img = $this->UploadImage($_FILES['partner_img']);

                if($upload_img){
                    $partner_img = URL.$upload_img;
                    $this->DeleteFile($partnerinfo[0]['partner_image']);
                }else{
                    $partner_img = $partnerinfo[0]['partner_image'];
                }
            }else{
                $partner_img = $partnerinfo[0]['partner_image'];
            }

            $data = array(
                'partner_name'          => $partner_name,
                'event_id'              => $event_id,
                'partner_type'          => $partner_type,
                'partner_url'           => $partner_url,
                'partner_image'         => $partner_img,
            );

            $result = $partner->UpdatePartner($data,$partner_id);
            if($result){
                echo 1;
            }
        }
    }

    function deletepartner($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $partner = new model_partner();

        if($path){
            if($partner->GetPartnerById($path)){

                $delete = $partner->DeletePartner($path);

                if($delete){
                    header('Location:'.URL.'partner/');
                }

            }else{
                header('Location:'.URL.'partner/');
            }
        }else{
            header('Location:'.URL.'partner/');
        }
    }
}