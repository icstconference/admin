<?php
class controller_videoupload extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    function UploadVideo($data) {
        $type = '';
        print_r($data);
        if ($data["error"] == 0) {
        	$name = uniqid();
        	if($data["type"] == "video/mp4"){
                $type = "mp4";
            }
            if($data["type"] == "application/octet-stream"){
                $type = "flv";
            }
            $path = "public/upload/video/" .$name.'.'.$type;
            move_uploaded_file($data["tmp_name"], $path);
            $array = array(
                'name'  => $data['name'],
                'type'  => $data['type'],
                'size'  => $data['size'],
                'path'  => $path
            );
            return $array;
        }
    }

    protected function DeleteFile($file_path) {
        $path = str_replace(URL, '', $file_path);
        if (file_exists($path)) {
            unlink($path);
            return true;
        }
        return false;
    }

    public function index(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

    	$model = new model_video();

    	if($model->SelectAllVideoUpload()){
    		$this->view->listvideo = $model->SelectAllVideoUpload();
    	}


    	$this->view->layout('layout-admin1');
        $this->view->Render('video/index1');
        return true;
    }

    public function updatevideo(){
    	$model = new model_video();

        if($_POST){

        	$videoid 	= $_POST['videoid'];
        	$videoinfo  = $model->GetVideoUploadById($videoid);
            $file_url   = '';
            $file_name	= '';	
            $video_name = $_POST['video_name'];

            if(strlen($_FILES['video_upload']['name'])>1){
                $upload_file = $this->UploadVideo($_FILES['video_upload']);
                
                //print_r($upload_file);
                if($upload_file){
                    $file_url   = $upload_file['path'];
                    $file_name	= $upload_file['name'];
                    $this->DeleteFile(URL.$videoinfo[0]['video_upload_url']);
                }else{
                	$file_url 	= $videoinfo[0]['video_upload_url'];
                	$file_name 	= $videoinfo[0]['video_name'];
                }
            }else{
               	$file_url 	= $videoinfo[0]['video_upload_url'];
               	$file_name 	= $videoinfo[0]['video_name'];
            }

            $arr = array(
                'video_upload_name'     => addslashes($video_name),
                'video_name'			=> addslashes($file_name),
				'video_upload_url'		=> URL.$file_url
            );

            $result = $model->UpdateVideoUpload($arr,$videoid);

            if($result){
            	echo '1';
            }
       }
    }

    public function delete($path){
    	if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $model = new model_video();

        if($path){
        	if($model->GetVideoUploadById($path)){
        		$videoinfo = $model->GetVideoUploadById($path);
        		$delete = $model->DeleteVideoUpload($path);
        		if($delete){
        			$this->DeleteFile(URL.$videoinfo[0]['video_upload_url']);
        			header('Location:'.URL.'videoupload/');
        		}
        	}
        }else{
        	header('Location:'.URL.'videoupload');
        }
    }

    public function delete1(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $model = new model_video();

        if($_POST){
            $id = $_POST['id'];
            if($model->GetVideoUploadById($id)){
                $videoinfo = $model->GetVideoUploadById($id);
                $delete = $model->DeleteVideoUpload($id);
                if($delete){
                    $this->DeleteFile(URL.$videoinfo[0]['video_upload_url']);
                    echo '1';
                }
            }
        }
    }

    public function edit($path){
    	if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $model = new model_video();

        if($path){
        	if($model->GetVideoUploadById($path)){
        		$videoinfo = $model->GetVideoUploadById($path);

        		$this->view->videoinfo = $videoinfo;
        	}
        }else{
        	header('Location:'.URL.'videoupload');
        }

        $this->view->js = array(
        	URL.'public/js/videoupload.js'
        );

        $this->view->layout('layout-admin1');
        $this->view->Render('video/editvideo');
        return true;
    }

    public function createvideo(){
        $model = new model_video();

        if($_POST){

            $file_url   = '';
            $file_name	= '';	
            $video_name = $_POST['video_name'];


            if(strlen($_FILES['video_upload']['name'])>1){
                $upload_file = $this->UploadVideo($_FILES['video_upload']);
                
                //print_r($upload_file);
                if($upload_file){
                    $file_url   = $upload_file['path'];
                    $file_name	= $upload_file['name'];
                }
            }

            $arr = array(
                'video_upload_name'     => addslashes($video_name),
                'video_name'			=> addslashes($file_name),
				'video_upload_url'		=> URL.$file_url
            );

            $result = $model->InsertVideoUpload($arr);

            if($result){
            	echo '1';
            }
       }

    }

    public function upload(){
    	if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $this->view->js = array(
        	URL.'public/js/videoupload.js'
        );

    	$this->view->layout('layout-admin1');
        $this->view->Render('video/upload');
        return true;
    }

}