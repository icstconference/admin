<?php
class controller_lienhe extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    public function index(){

    	$model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $partner = new model_partner();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
			$this->view->aboutus = $allsetting[0]['config_url_aboutus'];
            $this->view->laboratories = $allsetting[0]['config_url_laboratories'];
			$this->view->emaillink = $allsetting[0]['config_url_email'];
            $this->view->internal = $allsetting[0]['config_url_internal'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOnByPostion('all','ngang')){
            $allmenufather = $model1->SelectAllMenuFatherOnByPostion('all','ngang');

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        if($model1->SelectAllMenuFatherOnByPostion('product','doc')){
            $allmenufatherproduct = $model1->SelectAllMenuFatherOnByPostion('product','doc');

            foreach($allmenufatherproduct as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufatherproduct[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenuproduct = $allmenufatherproduct;
        }

        if($advertise->GetAdvertiseByPosition(1)){
            $this->view->listadvertise1 = $advertise->GetAdvertiseByPosition(1);
        }

        if($advertise->GetAdvertiseByPosition(2)){
            $this->view->listadvertise2 = $advertise->GetAdvertiseByPosition(2);
        }

        if($advertise->GetAdvertiseByPosition(3)){
            $this->view->listadvertise3 = $advertise->GetAdvertiseByPosition(3);
        }

        if($news->SelectTopNewsView(4)){
            $this->view->listnewsview = $news->SelectTopNewsView(4);
        }

        if($news->SelectTopNewsRelease(4)){
            $this->view->listnewsrelease = $news->SelectTopNewsRelease(4);
        }

        if($linker->SelectAlllinker()){
            $this->view->listlinker = $linker->SelectAlllinker();
        }

        if($news->SelectTopNewsRelease(5)){
            $this->view->listnewsnew = $news->SelectTopNewsRelease(5);
        }

        if($partner->SelectAllPartner()){
            $this->view->listpartner = $partner->SelectAllPartner();
        }

		if($this->view->lang == 'vi'){
			$this->view->title = 'Liên hệ'.'-'.$allsetting[0]['config_title'];
			$this->view->description = 'Liên hệ'.'-'.$allsetting[0]['config_description'];
			$this->view->keyword = 'Liên hệ'.','.$allsetting[0]['config_keyword'];
		}else{
			$this->view->title = 'Contact'.'-'.'Institute for Computational Science and Technology';
			$this->view->description = 'Contact'.'-'.'Institute for Computational Science and Technology';
			$this->view->keyword = 'Contact'.','.'Institute for Computational Science and Technology';
		}
		
        if($model->GetAllContact()){
            $this->view->listcontact = $model->GetAllContact();
        }

        $this->view->layout('layout');
        $this->view->Render('lienhe/index');
        return true;
    }
}