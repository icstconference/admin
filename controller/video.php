<?php
class controller_video extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    public function index(){
        if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login/');
        }
    	$model = new model_video();

    	if($model->SelectAllVideo()){
    		$this->view->listvideo = $model->SelectAllVideo();
    	}

    	$this->view->js = array(
    		URL.'public/js/youtube.js'
    	);

    	if($_POST){
    		$video_name = $_POST['youtube_name'];
    		$video_image = $_POST['youtube_image'];
    		$video_url = $_POST['youtube_url'];
			$video_sort	= $_POST['youtube_sort'];

    		$data =array(
    			'video_name'	=> $video_name,
    			'video_image'	=> $video_image,
    			'video_url'		=> $video_url,
				'video_sort'	=> $video_sort
    		);

    		$result = $model->InsertVideo($data);
    		if($result){
    			header('Location:'.URL.'video');
    		}
    	}

    	$this->view->title = 'Trang quản trị';

    	$this->view->layout('layout-admin');
        $this->view->Render('video/index');
        return true;
    }
	
	public function editvideo($path){
        if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login/');
        }
    	$model = new model_video();
		
		if($path){
			if($model->GetVideoById($path)){
				$videoinfo = $model->GetVideoById($path);
				
				$this->view->videoinfo = $videoinfo;
				
				if($_POST){
					$video_sort	= $_POST['youtube_sort'];

					$data =array(
						'video_sort'	=> $video_sort
					);

					$result = $model->UpdateVideo($data,$path);
					if($result){
						header('Location:'.URL.'video');
					}
				}
			}else{
				header('Location:'.URL.'video');
			}
		}else{
			header('Location:'.URL.'video');
		}
		
		$this->view->title = 'Trang quản trị';

    	$this->view->layout('layout-admin');
        $this->view->Render('video/editvideo');
        return true;
	}

    public function deletevideo($path){
    	if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login/');
        }
    	$model = new model_video();

    	if($model->SelectAllVideo()){
    		$this->view->listvideo = $model->SelectAllVideo();
    	}

    	$this->view->js = array(
    		URL.'public/js/youtube.js'
    	);

    	if($path){
    		if($model->GetVideoById($path)){
    			$deletevideo = $model->DeleteVideo($path);
    			if($deletevideo){
    				header('Location:'.URL.'video');
    			}
    		}
    	}else{
    		header('Location:'.URL.'video');
    	}
    }

}