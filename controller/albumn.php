<?php
class controller_albumn extends lib_controller
{
    public function __construct() {
        parent::__construct();
    }

    function UploadImage(array $data) {
        $type = '';
        if(($data["type"] == "image/jpeg")
            || ($data["type"] == "image/jpg") || ($data["type"] == "image/pjpeg")){
                $type = 'jpg';  
        }elseif($data["type"] == "image/gif"){
            $type = 'gif';
        }elseif($data["type"] == "image/png"){
            $type = 'png';
        }
        if ((($data["type"] == "image/gif") || ($data["type"] == "image/jpeg")
                || ($data["type"] == "image/jpg") || ($data["type"] == "image/pjpeg")
                || ($data["type"] == "image/x-png") || ($data["type"] == "image/png"))
            && ($data["size"] < 4000000)) {
            if ($data["error"] == 0) {
                $path = "public/upload/images/albumn/" . uniqid() . '.'.$type;
                move_uploaded_file($data["tmp_name"], $path);
                return $path;
            }
        }
        return false;
    }

    function SaveThumbImage($path) {
        try {
            $thumb = PhpThumbFactory::create($path);
        } catch (Exception $e) {
            // handle error here however you'd like
        }
        $id=  uniqid();
        $name_file = $id.'.jpg';
        $thumb->adaptiveResize(300, 300);
        $thumb->save('public/upload/images/albumn/'. $name_file, 'jpg');

        $path_result = array(
            'thumb' => URL.'public/upload/images/albumn/'. $name_file
        );
        return $path_result;
    }

    protected function DeleteFile($file_path) {
        $path = str_replace(URL, '', $file_path);
        if (file_exists($path)) {
            unlink($path);
            return true;
        }
        return false;
    }

    public function index(){
        require_once 'plugin/url-seo/url-seo.php';
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $model = new model_albumn();

        if($model->GetAllAlbumnCategories()){
            $this->view->listcategories = $model->GetAllAlbumnCategories();
        }

        if($model->GetAllAlbumn()){
            $this->view->listalbumn = $model->GetAllAlbumn();
        }

        if($_POST){
            $albumn_name                = $_POST['albumn_name'];
            $albumn_name_english        = $_POST['albumn_name_english'];
            $albumn_description         = $_POST['albumn_description'];
            $albumn_description_english = $_POST['albumn_description_english'];

            $data = array(
                'albumn_name'               => addslashes($albumn_name),
                'albumn_name_english'       => addslashes($albumn_name_english),
                'albumn_description'        => addslashes($albumn_description),
                'albumn_description_english'=> addslashes($albumn_description_english),
                'albumn_url'                => ''
            ); 

            $result = $model->InsertAlbumn($data);
            if($result){
                $albumn_url = post_slug($albumn_name).'-'.$result;
                $arr = array(
                    'albumn_url'    => $albumn_url
                );
                $update = $model->UpdateAlbumn($arr,$result);
                if($update){
                    header('Location:'.URL.'albumn/editalbumn/'.$result);
                }
            }
        }

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/albumn.js'
        );

    	$this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin');
        $this->view->Render('albumn/index');
        return true;
    }

    function uploaddrag($path){
        require_once 'plugin/phpthumb/ThumbLib.inc.php';
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $model = new model_albumn();

        $pic = $this->UploadImage($_FILES['myfile']);
        $pic_url = URL.$pic;

        $thumb = $this->SaveThumbImage($pic);
        $thumb_url = $thumb['thumb'];

        if($pic_url && $thumb_url){
            if($path){
                if($model->GetAlbumnById($path)){
                    $imagedefault = $model->GetImageDefaultById($path);

                    if($imagedefault){
                        $data = array(
                            'albumn_image_url'          => $pic_url,
                            'albumn_image_thumb_url'    => $thumb_url,
                            'albumn_id'                 => $path,
                        );
                    }else{
                        $data = array(
                            'albumn_image_url'          => $pic_url,
                            'albumn_image_thumb_url'    => $thumb_url,
                            'albumn_id'                 => $path,
                            'albumn_image_default'      => '1'
                        );
                    }

                    $result = $model->InsertImage($data);
                    if($result){
                        $allimage = $model->GetAllImageById($path);
                        $html = '';
                        $i=0;
                        foreach ($allimage as $value) {
                            $i++;
                            $html .='<tr>'.
                                        '<td>'.$i.'</td>'.
                                        '<td>'.
                                            '<img src="'.$value['albumn_image_thumb_url'].'" class="img-responsive" />'.
                                        '</td>'.
                                        '<td>'.
                                            $value['albumn_image_url'].
                                        '</td>'.
                                        '<td>'.
                                            $value['albumn_image_create_date'].
                                        '</td>'.
                                        '<td>'.
                                            '<a class="btn btn-primary" href="'.URL.'albumn/deleteimage/'.$value['albumn_image_id'].'">Delete</a>'.
                                        '</td>'.
                                    '</tr>';
                        }
                        echo $html;
                    }
                }
            }
        }
    }

    public function deleteimage($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }
        
        $model = new model_albumn();

        if($path){
            if($model->GetImageById($path)){
                $imageinfo = $model->GetImageById($path);

                $delete = $model->DeleteImage($path);
                if($delete){
                    $this->DeleteFile($imageinfo[0]['albumn_image_url']);
                    $this->DeleteFile($imageinfo[0]['albumn_image_thumb_url']);
                    header('Location:'.URL.'albumn/editalbumn/'.$imageinfo[0]['albumn_id']);
                }
            }
        }else{
            header('Location:'.URL.'albumn/');
        }
    }

    public function editalbumn($path){
        require_once 'plugin/url-seo/url-seo.php';
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $model = new model_albumn();

        if($model->GetAllAlbumnCategories()){
            $this->view->listcategories = $model->GetAllAlbumnCategories();
        }

        if ($path) {
            if($model->GetAlbumnById($path)){
                $albumninfo = $model->GetAlbumnById($path);

                $this->view->albumninfo = $albumninfo;

                $listimage = $model->GetAllImageById($path);

                $this->view->listimage = $listimage;

                if($_POST){
                    $albumn_name                = $_POST['albumn_name'];
                    $albumn_name_english        = $_POST['albumn_name_english'];
                    $albumn_description         = $_POST['albumn_description'];
                    $albumn_description_english = $_POST['albumn_description_english'];
                    $albumn_url                 = post_slug($albumn_name).'-'.$path;

                    $data = array(
                        'albumn_name'               => addslashes($albumn_name),
                        'albumn_name_english'       => addslashes($albumn_name_english),
                        'albumn_description'        => addslashes($albumn_description),
                        'albumn_description_english'=> addslashes($albumn_description_english),
                        'albumn_url'                => $albumn_url
                    ); 

                    $result = $model->UpdateAlbumn($data,$path);
                    if($result){
                        header('Location:'.URL.'albumn/');
                    }
                }
            }else{
                header('Location:'.URL.'albumn');
            }
            
        }else{
            header('Location:'.URL.'albumn');
        }

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/albumn.js',
            URL.'public/js/ajax_upload.js'
        );

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin');
        $this->view->Render('albumn/editalbumn');
        return true;
    }
	
	public function uploadimageother(){
		require_once 'plugin/phpthumb/ThumbLib.inc.php';
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $model = new model_albumn();
		
		if($_POST){
			$file 		= $_FILES['albumn_image'];
			$albumn_id	= $_POST['albumn_id'];
			$name 		= $file['name'];
			$type 		= $file['type'];
			$tmp_name	= $file['tmp_name'];
			$error		= $file['error'];
			$size		= $file['size'];
			
			$numimage = count($name);
			
			$success = 0;
			
			for($i=0;$i<$numimage;$i++){
				$data = array(
					'name'		=> $name[$i],
					'type'		=> $type[$i],
					'tmp_name'	=> $tmp_name[$i],
					'error'		=> $error[$i],
					'size'		=> $size[$i]
				);
				
				$pic = $this->UploadImage($data);
				$pic_url = URL.$pic;

				$thumb = $this->SaveThumbImage($pic);
				$thumb_url = $thumb['thumb'];
				
				$imagedefault = $model->GetImageDefaultById($albumn_id);

                if($imagedefault){
                   $arr = array(
                            'albumn_image_url'          => $pic_url,
                            'albumn_image_thumb_url'    => $thumb_url,
                            'albumn_id'                 => $albumn_id,
					);
               }else{
                   $arr = array(
                            'albumn_image_url'          => $pic_url,
                            'albumn_image_thumb_url'    => $thumb_url,
                            'albumn_id'                 => $albumn_id,
                            'albumn_image_default'      => '1'
					);
               }

               $result = $model->InsertImage($arr);
			   
			   if($result){
				   $success++;
			   }
			}
			
			if($success == $numimage){
				echo $albumn_id;
			}
		}
	}

    public function deletealbumn($path){
    	if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $model = new model_albumn();

        if($path){
            if($model->GetAlbumnById($path)){
                $albumn = $model->GetAlbumnById($path);

                $allimage = $model->GetAllImageById($path);

                $delete = $model->DeleteAlbumn($path);

                if($delete){
					if($model->GetAllImageById($path)){
						foreach ($allimage as $value) {
							$deleteimage = $model->DeleteImage($value['albumn_image_id']);
							$this->DeleteFile($value['albumn_image_url']);
							$this->DeleteFile($value['albumn_image_thumb_url']);
						}
					}

                    header('Location:'.URL.'albumn');
                }
            }
        }else{
            header('Location:'.URL.'albumn');
        }
    }

    public function deletecategories($path){
    	if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $model = new model_albumn();

        if($path){
        	if($model->GetAlbumnCategoriesById($path)){
        		$delete = $model->DeleteAlbumnCategories($path);
                $allalbumn = $model->GetAllAlbumnById($path);
        		if($delete){
                    foreach ($allalbumn as $value) {
                        $allimage = $model->GetAllImageById($value['albumn_id']);
                        $delete = $model->DeleteAlbumn($value['albumn_id']);
                        if($delete){
                            foreach ($allalbumn as $value1) {
                                $model->DeleteImage($value1['albumn_image_id']);
                                $this->DeleteFile($value1['albumn_image_url']);
                                $this->DeleteFile($value1['albumn_image_thumb_url']);
                            }
                        }
                    }
        			header('Location:'.URL.'albumn/categories/');
        		}
        	}else{
        		header('Location:'.URL.'albumn/categories/');
        	}
        }else{
        	header('Location:'.URL.'albumn/categories/');
        }
    }

    public function editcategories($path){
    	require_once 'plugin/url-seo/url-seo.php';
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

       	$model = new model_albumn();

       	if($model->GetAllAlbumnCategories()){
       		$this->view->listcategories = $model->GetAllAlbumnCategories();
       	}

       	if($path){
       		if($model->GetAlbumnCategoriesById($path)){

       			$categoriesinfo = $model->GetAlbumnCategoriesById($path);

       			$this->view->categoriesinfo = $categoriesinfo;

		       	if($_POST){
					$albumn_categories_name 			= $_POST['albumn_categories_name'];
					$albumn_categories_name_english		= $_POST['albumn_categories_name_english'];

					if($_POST['albumn_categories_father']){
						$albumn_categories_father = $_POST['albumn_categories_father'];
					}else{
						$albumn_categories_father = 0;
					}

					$albumn_categories_url = post_slug($albumn_categories_name).'-'.$path;

					$data = array(
						'albumn_categories_name'			=> $albumn_categories_name,
						'albumn_categories_name_english'	=> $albumn_categories_name_english,
						'albumn_categories_father'			=> $albumn_categories_father,
						'albumn_categories_url'				=> $albumn_categories_url
					);

					$result = $model->UpdateAlbumnCategories($data,$path);

					if($result){
						header('Location:'.URL.'albumn/categories/');
					}
		        }
		    }else{
		    	header('Location:'.URL.'albumn/categories/');
		    }
	    }else{
	    	header('Location:'.URL.'albumn/categories/');
	    }

       	$this->view->js = array(
    		URL.'public/js/albumncat.js'
    	);

    	$this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin');
        $this->view->Render('albumn/editcategories');
        return true;
    }

    public function categories(){
    	require_once 'plugin/url-seo/url-seo.php';
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

       	$model = new model_albumn();

       	if($model->GetAllAlbumnCategories()){
       		$this->view->listcategories = $model->GetAllAlbumnCategories();
       	}

        if($_POST){
			$albumn_categories_name 			= $_POST['albumn_categories_name'];
			$albumn_categories_name_english		= $_POST['albumn_categories_name_english'];

			if($_POST['albumn_categories_father']){
				$albumn_categories_father = $_POST['albumn_categories_father'];
			}else{
				$albumn_categories_father = 0;
			}

			$data = array(
				'albumn_categories_name'			=> $albumn_categories_name,
				'albumn_categories_name_english'	=> $albumn_categories_name_english,
				'albumn_categories_father'			=> $albumn_categories_father,
				'albumn_categories_url'				=> ''
			);

			$result = $model->InsertAlbumnCategories($data);
			if($result){
				$albumn_categories_url = post_slug($albumn_categories_name).'-'.$result;

				$arr = array(
					'albumn_categories_url'		=> $albumn_categories_url
				);

				$update = $model->UpdateAlbumnCategories($arr,$result);
				if($update){
					header('Location:'.URL.'albumn/categories/');
				}
			}
        }

    	$this->view->js = array(
    		URL.'public/js/albumncat.js'
    	);

    	$this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin');
        $this->view->Render('albumn/categories');
        return true;
    }
	
	public function defaultimage($path,$path1){
		$albumn = new model_albumn();
		
		if($path){
			if($albumn->GetImageById($path)){
				$imagedefault = $albumn->GetImageDefaultById($path1);
				
				$data = array(
					'albumn_image_default' => 1
				);
				
				$data1 = array(
					'albumn_image_default' => 0
				);
				
				$update = $albumn->UpdateImage($data1,$imagedefault[0]['albumn_image_id']);
				$update1 = $albumn->UpdateImage($data,$path);
				if($update1){
					header('Location:'.URL.'albumn/editalbumn/'.$path1);
				}
			}else{
				header('Location:'.URL.'albumn/editalbumn/'.$path1);
			}
		}else{
			header('Location:'.URL.'albumn/');
		}
	}

    public function detail($path){
        $albumn = new model_albumn();

        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        if($allsetting){
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
			$this->view->aboutus = $allsetting[0]['config_url_aboutus'];
            $this->view->laboratories = $allsetting[0]['config_url_laboratories'];
			$this->view->emaillink = $allsetting[0]['config_url_email'];
            $this->view->internal = $allsetting[0]['config_url_internal'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOnByPostion('all','ngang')){
            $allmenufather = $model1->SelectAllMenuFatherOnByPostion('all','ngang');

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        if($model1->SelectAllMenuFatherOnByPostion('news','doc')){
            $allmenufatherproduct = $model1->SelectAllMenuFatherOnByPostion('news','doc');

            foreach($allmenufatherproduct as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufatherproduct[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenuproduct = $allmenufatherproduct;
        }


        if($path){
            $part = explode('_',$path);
            $id = $part[count($part)-1];

            if($albumn->GetAlbumnById($id)){
                $albumninfo = $albumn->GetAlbumnById($id);

                $allimage = $albumn->GetAllImageById($id);

				if($this->view->lang == 'vi'){
					$this->view->title = $albumninfo[0]['albumn_name'].'-'.$allsetting[0]['config_title'];
					$this->view->description = $albumninfo[0]['albumn_name'].'-'.$allsetting[0]['config_description'];
					$this->view->keyword = $albumninfo[0]['albumn_name'].','.$allsetting[0]['config_keyword'];
				}else{
					$this->view->title = $albumninfo[0]['albumn_name_english'].'- Institute for Computational Science and Technology';
					$this->view->description = $albumninfo[0]['albumn_name_english'].'- Institute for Computational Science and Technology';
					$this->view->keyword = $albumninfo[0]['albumn_name_english'].',Institute for Computational Science and Technology';
				}
				
                $this->view->albumninfo = $albumninfo;
                
                $this->view->allimage = $allimage; 

            }else{
                header('Location:'.URL.$this->view->lang.'/category/galery/');
            }
        }else{
            header('Location:'.URL.$this->view->lang.'/category/galery/');
        }

        $this->view->layout('layout');
        $this->view->Render('albumn/detail');
        return true;
    }
}
