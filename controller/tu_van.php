<?php
class controller_tu_van extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    public function taokhung(){
        $model = new model_general();
        if($_POST){
            $soluongkhung = $_POST['soluongkhung'];
            $khungnext = intval($soluongkhung) + 1;
            $listproduction = $model->GetAllProduction();

            $html1 = '';
            foreach($listproduction as $lp){
                $html1.='<option value="'.$lp['product_id'].'">'.$lp['product_name'].'</option>';
            }
            $html = '<div class="row" style="margin: 10px 0;" id="khung'.$khungnext.'">
                        <select id="s2_with_tag" class="populate placeholder namecuqua" name="namecuqua'.$khungnext.'">'.
                            $html1.

                        '</select>
                        <input type="text" placeholder="Lượng tiêu thụ ..." class="quantitycuqua" name="quantitycuqua'.$khungnext.'">
                        <input type="button" value="Và" class="btn btn-primary buttonva" id="addcuqua">
                        <input type="button" value="Xóa" class="btn btn-danger buttonva" id="xoacuqua" rel="'.$khungnext.'">
                    </div>';
            echo $html;
        }
    }

    public function savechart(){
        $model = new model_general();
        if($_POST){
            $listnutritionid = $_POST['listnutritionid'];
            $listnutritionvalue = $_POST['listnutritionvalue'];
            $userid = lib_session::Get("user_id");

            $item = explode(',',$listnutritionid);
            $item1 = explode(',',$listnutritionvalue);

            $numnutrition = count($item);
            if($model->GetValueUser($userid)){
                $listnutrion = $model->GetValueUser($userid);
                foreach($listnutrion as $ls){
                    $model->DeleteValueUser($ls['user_nutrition_detail_id']);
                }
            }
            $count = 0;
            for($i=0;$i<$numnutrition;$i++){
                $data = array(
                  'user_id' => $userid,
                  'product_nutrion_id' => $item[$i],
                  'user_nutrition_quantity' => $item1[$i]
                );

                $result = $model->InsertValueUser($data);
                if($result){
                    $count++;
                }
            }
            if($count == $numnutrition){
                echo URL.'dashboard/'.lib_session::Get("user_id");
            }
        }
    }

    public function index(){
        $model = new model_general();
        if($_POST){

            $soluongkhung = $_POST['soluongkhung'];
            $listnutrition = $model->GetAllNutrition();

            for($i = 1;$i<=$soluongkhung;$i++){
                if($_POST['namecuqua'.$i]){
                    $rate = intval($_POST['quantitycuqua'.$i])/100;
                    $idcuqua = $_POST['namecuqua'.$i];
                    foreach($listnutrition as $key=>$ls){
                        if($model->GetAllValueDetailByProductIdNutritionId($idcuqua,$ls['product_nutrition_id'])){
                            $value = $model->GetAllValueDetailByProductIdNutritionId($idcuqua,$ls['product_nutrition_id']);
                            $listnutrition[$key]['value'] = intval($listnutrition[$key]['value'])+($rate*intval($value['product_nutrition_quantity']));
                        }else{
                            $listnutrition[$key]['value'] = intval($listnutrition[$key]['value'])+0;
                        }
                    }
                }
            }

            $listproduct = array();

            for($i = 1;$i<=$soluongkhung;$i++){
                if($_POST['namecuqua'.$i]){
                    $idcuqua = $_POST['namecuqua'.$i];
                    $listproduct[] = $model->GetProductionById($idcuqua);
                }
            }

            $this->view->listproduct = $listproduct;

            $this->view->listnutrition = $listnutrition;

            $this->view->title = 'Tư vấn online';

            $this->view->js = array(
              URL.'public/js/chart.js'
            );

            $this->view->layout('layout');
            $this->view->Render('tu_van/result');
            return true;
        }

        $listproduction = $model->GetAllProduction();

        $this->view->listproduction = $listproduction;

        $this->view->js = array(
          URL.'public/js/tuvan.js'
        );

        $this->view->title = 'Tư vấn online';

        $this->view->layout('layout');
        $this->view->Render('tu_van/index');
        return true;
    }
}