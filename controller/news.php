<?php
class controller_news extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    /*
     * Các function tạo hình ảnh
     */

    function UploadImage(array $data) {
        if ((($data["type"] == "image/gif") || ($data["type"] == "image/jpeg")
                || ($data["type"] == "image/jpg") || ($data["type"] == "image/pjpeg")
                || ($data["type"] == "image/x-png") || ($data["type"] == "image/png"))
            && ($data["size"] < 4000000)) {
            if ($data["error"] == 0) {
                $path = "public/upload/images/" . uniqid() . '.jpg';
                move_uploaded_file($data["tmp_name"], $path);
                return $path;
            }
        }
        return false;
    }

    function SaveThumbImage($path) {
        try {
            $thumb = PhpThumbFactory::create($path);
        } catch (Exception $e) {
            // handle error here however you'd like
        }
        $id=  uniqid();
        $name_file = $id.'.jpg';
        $thumb->resize(400);
        $thumb->adaptiveResize(300, 300);
        $thumb->save('public/upload/images/news/' . $name_file, 'jpg');

        $path_result = array(
            'image' => $path,
            'thumb' => 'public/upload/images/news/' . $name_file,
        );
        return $path_result;
    }

    protected function DeleteFile($file_path) {
        $path = str_replace(URL, '', $file_path);
        if (file_exists($path)) {
            unlink($path);
            return true;
        }
        return false;
    }


    public function updatenews(){
        require_once 'plugin/phpthumb/ThumbLib.inc.php';
        require_once 'plugin/url-seo/url-seo.php';
        $model = new model_news();
		
        if($_POST){
            $product_id = $_POST['news_id'];
            $news_info = $model->SelectNewsById($product_id);
            $product_name = $_POST['news_name'];
            $product_name_english = $_POST['news_name_english'];
            $product_type = $_POST['news_type'];
			if($_POST['news_label']){
				if(count($_POST['news_label']) >= 1){
					foreach($_POST['news_label'] as $val){
						$product_label .= ','.$val;
					}
				}else{
					$arr = $_POST['news_label'];
					$product_label = $arr[0];
				}
			}else{
				$product_label = '';
			}
            $product_description = $_POST['news_description'];
            $product_description_english = $_POST['news_description_english'];
            $news_url 		  = post_slug($product_name).'-post-'.$product_id;
            $news_url_english = post_slug($product_name_english).'-post-'.$product_id;
			$news_create_date = date('Y-m-d h:i:s',strtotime($_POST['news_create_date']));
            $news_display     = $_POST['news_display'];
            $user_id          = $_SESSION['user_id'];
			
            if(strlen($_FILES['news_img']['name'])>1){
                $upload_img = $this->UploadImage($_FILES['news_img']);
                $path_save_img = $this->SaveThumbImage($upload_img);

                if($path_save_img){
                    $product_img = URL.$path_save_img['image'];
                    $this->DeleteFile($news_info[0]['news_image_thumb']);
                }else{
                    $product_img = $news_info[0]['news_image_thumb'];
                }
            }else{
                $product_img = $news_info[0]['news_image_thumb'];
            }

            $data = array(
                'news_name'          		=> addslashes($product_name),
                'news_name_english'  		=> addslashes($product_name_english),
                'news_description'   		=> $product_description,
                'news_description_english'  => $product_description_english,
                'news_image_thumb'   		=> $product_img,
                'news_display'              => $news_display,
                'event_id'    	            => $product_type,
                'label_id'              	=> $product_label,
                'news_url'           		=> $news_url,
                'news_url_english'   		=> $news_url_english,
				'news_create_date'			=> $news_create_date
            );

			//print_r($data);
			
            $result = $model->UpdateNews($data,$product_id);

            if($result){
                $log = new model_log();
                $arr = array(
                    'log_action'        => 'update',
                    'user_id'           => $_SESSION['user_id'],
                    'log_object'        => 'tin tức',
                    'log_object_id'     => $product_id,
                    'log_object_name'   => $product_name,
                    'user_name'         => $_SESSION['user_name']
                );
                $log->InsertLog($arr);
                echo '1';
            }
        }
    }

    public function editnews($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($edit[0]['permission_detail'],'new')){
                header('Location:'.URL.'admin/');
            }
        }

        $model = new model_news();
		
		$model1 = new model_general();
        if($path){
            $result = $model->SelectNewsById($path);
            if($result){
                $this->view->newsinfo = $result;

				$list_permission = '';
		
				if($model1->GetPermissionNewsByUserId($_SESSION['user_type'])){
					$listpermission = $model1->GetPermissionNewsByUserId($_SESSION['user_type']);
					
					foreach($listpermission as $ls){
						$list_permission .= ','.$ls['categories_id'];
					}
				}
				
				$this->view->listpermission = $list_permission;
				
                if($model->SelectAllCategoriesNews()){
                    $this->view->listcategories = $model->SelectAllCategoriesNews();
                }

                if($model->SelectAllCategoriesNewsLabel()){
                    $this->view->listlabel = $model->SelectAllCategoriesNewsLabel();
                }

                $event = new model_event();

                if($event->GetAllEvent()){
                    $this->view->listevent = $event->GetAllEvent();
                }

                $this->view->title = 'Trang quản trị';

                $this->view->js = array(
                    URL.'public/ckeditor/ckeditor.js',
                    URL.'public/js/news.js'
                );

                $this->view->layout('layout-admin1');
                $this->view->Render('news/editnews');
                return true;
            }else{
                header('Location:'.URL.'news');
            }
        }else{
            header('Location:'.URL.'news');
        }
    }

    public function createnews(){
        require_once 'plugin/phpthumb/ThumbLib.inc.php';
        require_once 'plugin/url-seo/url-seo.php';
        $model = new model_news();
        if($_POST){
            $product_name = $_POST['news_name'];
            $product_name_english = $_POST['news_name_english'];
            $product_type = $_POST['news_type'];
            $news_display     = $_POST['news_display'];
			if($_POST['news_label']){
				if(count($_POST['news_label']) > 1){
					foreach($_POST['news_label'] as $val){
						$product_label .= ','.$val;
					}
				}else{
					$arr = $_POST['news_label'];
					$product_label = $arr[0];
				}
			}else{
				$product_label = '';
			}
            $product_description = $_POST['news_description'];
            $product_description_english = $_POST['news_description_english'];

            if(strlen($_FILES['news_img']['name'])>1){
                $upload_img = $this->UploadImage($_FILES['news_img']);
                $path_save_img = $this->SaveThumbImage($upload_img);

                if($path_save_img){
                    $product_img = URL.$path_save_img['image'];
                }else{
                    $product_img = '';
                }
            }else{
                $product_img = '';
            }

            $data = array(
                'news_name'          		=> addslashes($product_name),
                'news_name_english'  		=> addslashes($product_name_english),
                'news_description'   		=> $product_description,
                'news_description_english'  => $product_description_english,
                'news_image_thumb'   		=> $product_img,
                'event_id'    	            => $product_type,
                'news_display'              => $news_display,
                'label_id'              	=> $product_label,
                'news_url'           		=> '',
                'news_url_english'   		=> ''
            );
			
            $result = $model->InsertNews($data);
			
            if($result){
                $datenow = date('dmY');
                $uniqueid = uniqid();
                $product_url = post_slug($product_name).'-post-'.$result;
                $product_url_english = post_slug($product_name_english).'-post-'.$result;
                $data1 = array(
                    'news_url' => $product_url,
                    'news_url_english'  => $product_url_english
                );
				
                $update = $model->UpdateNews($data1,$result);
                if($update){
                    $log = new model_log();
                    $arr = array(
                        'log_action'        => 'create',
                        'user_id'           => $_SESSION['user_id'],
                        'log_object'        => 'tin tức',
                        'log_object_id'     => $result,
                        'log_object_name'   => $product_name,
                        'user_name'         => $_SESSION['user_name']
                    );
                    $log->InsertLog($arr);
                    echo 1;
                }
            }
        }
    }

    /*
     * Các function của news
     */

    function deletenews($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit']; 

        if($_SESSION['user_type'] != 1){
            if(!strpos($delete[0]['permission_detail'],'new')){
                header('Location:'.URL.'admin/');
            }
        }

        $model = new model_news();

        if($path){
            if($model->SelectNewsById($path)){
                $newsinfo = $model->SelectNewsById($path);
                $delete = $model->DeleteNews($path);

                if($delete){
                    $this->DeleteFile($newsinfo[0]['news_image_thumb']);
                    $log = new model_log();
                    $arr = array(
                        'log_action'        => 'delete',
                        'user_id'           => $_SESSION['user_id'],
                        'log_object'        => 'tin tức',
                        'log_object_id'     => $newsinfo[0]['news_id'],
                        'log_object_name'   => $newsinfo[0]['news_name'],
                        'user_name'         => $_SESSION['user_name']
                    );
                    $log->InsertLog($arr);
                    echo '1';
                    header('Location:'.URL.'news/');
                }

            }else{
                header('Location:'.URL.'news/');
            }
        }else{
            header('Location:'.URL.'news/');
        }
    }

    function deletenews1(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit']; 

        if($_SESSION['user_type'] != 1){
            if(!strpos($delete[0]['permission_detail'],'new')){
                header('Location:'.URL.'admin/');
            }
        }

        $model = new model_news();

        if($_POST){
            $idpost = $_POST['idpost'];
            if($model->SelectNewsById($idpost)){
                $newsinfo = $model->SelectNewsById($idpost);
                $delete = $model->DeleteNews($idpost);

                if($delete){
                    $this->DeleteFile($newsinfo[0]['news_image_thumb']);
                    $log = new model_log();
                    $arr = array(
                        'log_action'        => 'delete',
                        'user_id'           => $_SESSION['user_id'],
                        'log_object'        => 'tin tức',
                        'log_object_id'     => $newsinfo[0]['news_id'],
                        'log_object_name'   => $newsinfo[0]['news_name'],
                        'user_name'         => $_SESSION['user_name']
                    );
                    $log->InsertLog($arr);
                    echo '1';
                }
            }
        }
    }

    function addnews(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($create[0]['permission_detail'],'new')){
                header('Location:'.URL.'admin/');
            }
        }

        $model = new model_news();
        
        $model1 = new model_general();

        if($model->SelectAllNews()){
            $this->view->listnews = $model->SelectAllNews();
        }

        $list_permission = '';
        
        if($model1->GetPermissionNewsByUserId($_SESSION['user_type'])){
            $listpermission = $model1->GetPermissionNewsByUserId($_SESSION['user_type']);
            
            foreach($listpermission as $ls){
                $list_permission .= ','.$ls['categories_id'];
            }
        }
        
        $this->view->listpermission = $list_permission;
        
        if($model->SelectAllCategoriesNews()){
            $this->view->listcategories = $model->SelectAllCategoriesNews();
        }

        if($model->SelectAllCategoriesNewsLabel()){
            $this->view->listlabel = $model->SelectAllCategoriesNewsLabel();
        }

        $event = new model_event();

        if($event->GetAllEvent()){
            $this->view->listevent = $event->GetAllEvent();
        }

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/news.js'
        );

        $this->view->layout('layout-admin1');
        $this->view->Render('news/addnews');
        return true;
    }

    public function copy($path){
        require_once 'plugin/url-seo/url-seo.php';
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($create[0]['permission_detail'],'new') || !strpos($delete[0]['permission_detail'],'new') || !strpos($edit[0]['permission_detail'],'new')){
                header('Location:'.URL.'admin/');
            }
        }

        $model = new model_news();
        
        $model1 = new model_general();

        if($path){
            if($model->SelectNewsById($path)){
                $newsinfo = $model->SelectNewsById($path);
                
                $arr = array(
                    'news_name'                 => $newsinfo[0]['news_name'],
                    'news_name_english'         => $newsinfo[0]['news_name_english'],
                    'news_description'          => $newsinfo[0]['news_description'],
                    'news_description_english'  => $newsinfo[0]['news_description_english'],
                    'news_image_thumb'          => $newsinfo[0]['news_image_thumb'],
                    'news_categories_id'        => $newsinfo[0]['news_categories_id'],
                    'label_id'                  => $newsinfo[0]['label_id'],
                    'user_id'                   => $_SESSION['user_id']
                );

                $result = $model->InsertNews($arr);

                if($result){
                    $product_url         = post_slug($newsinfo[0]['news_name']).'-post-'.$result;
                    $product_url_english = post_slug($newsinfo[0]['news_name_english']).'-post-'.$result;
                    $data1 = array(
                        'news_url' => $product_url,
                        'news_url_english'  => $product_url_english
                    );
                    
                    $update = $model->UpdateNews($data1,$result);
                    $log = new model_log();
                    $arr = array(
                        'log_action'        => 'create',
                        'user_id'           => $_SESSION['user_id'],
                        'log_object'        => 'tin tức',
                        'log_object_id'     => $result,
                        'log_object_name'   => $newsinfo[0]['news_name'],
                        'user_name'         => $_SESSION['user_name']
                    );
                    $log->InsertLog($arr);
                    header('Location:'.URL.'news/');
                }

            }else{
                header('Location:'.URL.'news/');
            }
        }else{
            header('Location:'.URL.'news/');
        }
    }

    function index(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($create[0]['permission_detail'],'event') || !strpos($delete[0]['permission_detail'],'event') || !strpos($edit[0]['permission_detail'],'event')){
                header('Location:'.URL.'admin/');
            }
        }

        $model = new model_news();
		
		$model1 = new model_general();

        if($model->SelectAllNews()){
            $this->view->listnews = $model->SelectAllNews();
        }

		$list_permission = '';
		
		if($model1->GetPermissionNewsByUserId($_SESSION['user_id'])){
			$listpermission = $model1->GetPermissionNewsByUserId($_SESSION['user_id']);
			
			foreach($listpermission as $ls){
				$list_permission .= ','.$ls['categories_id'];
			}
		}
		
		$this->view->listpermission = $list_permission;
		
		if($model->SelectAllCategoriesNews()){
			$this->view->listcategories = $model->SelectAllCategoriesNews();
		}

        if($model->SelectAllCategoriesNewsLabel()){
            $this->view->listlabel = $model->SelectAllCategoriesNewsLabel();
        }

        $model1 = new model_setting();

        $allsetting = $model1->GetAllSetting();

        $this->view->allsetting = $allsetting;

        $event = new model_event();

        if($event->GetAllEvent()){
            $this->view->listevent = $event->GetAllEvent();
        }

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/news.js'
        );

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin1');
        $this->view->Render('news/index');
        return true;
    }

    /*
     * Các function của categories news
     */

    function editcategories($path){
        require_once 'plugin/url-seo/url-seo.php';
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($edit[0]['permission_detail'],'new')){
                header('Location:'.URL.'admin/');
            }
        }

        $model = new model_news();

        if($path){
            if($model->GetCategoriesById($path)){
                $result = $model->GetCategoriesById($path);

                $this->view->categoriesinfo = $result;

                if($model->SelectAllCategoriesFatherNews($path)){
                    $this->view->listcategoriesnews = $model->SelectAllCategoriesFatherNews($path);
                }

                if($_POST){
                    $product_type         = $_POST['news_type'];
                    $product_type_english = $_POST['news_type_english'];
                    $product_fathertype   = $_POST['news_fathertype'];
                    $urlnews    = explode('-',$result[0]['news_categories_url']);
                    $numarrnews = count($urlnews);
                    $urlcode = $urlnews[$numarrnews-2];
                    $product_type_url     = post_slug($product_type).'-postcategory-'.$path;
                    $product_type_url_english     = post_slug($product_type_english).'-postcategory-'.$path;

                    if($product_fathertype){
                        $product_father = $product_fathertype;
                    }else{
                        $product_father = 0;
                    }

                    $data = array(
                        'news_categories_name'          => $product_type,
                        'news_categories_name_english'  => $product_type_english,
                        'news_categories_father'        => $product_father,
                        'news_categories_url'           => $product_type_url,
                        'news_categories_url_english'   => $product_type_url_english
                    );

                    $result = $model->UpdateCategoriesNews($data,$path);
                    if($result){
                        $log = new model_log();
                        $arr = array(
                            'log_action'        => 'update',
                            'user_id'           => $_SESSION['user_id'],
                            'log_object'        => 'chuyên mục tin tức',
                            'log_object_id'     => $result,
                            'log_object_name'   => $product_type,
                            'user_name'         => $_SESSION['user_name']
                        );
                        $log->InsertLog($arr);
                        header('Location:'.URL.'news/categories/');
                    }
                }
            }else{
                header('Location:'.URL.'news/categories/');
            }
        }else{
            header('Location:'.URL.'news/categories/');
        }

        $this->view->js = array(
            URL.'public/js/newscat.js'
        );

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin1');
        $this->view->Render('news/editcategories');
        return true;
    }

    function deletecategories($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($delete[0]['permission_detail'],'new')){
                header('Location:'.URL.'admin/');
            }
        }

        $model = new model_news();

        if($path){
            if($model->GetCategoriesById($path)){
                $result = $model->GetCategoriesById($path);

                $delete = $model->DeleteCategoriesNews($path);

                if($delete){
                    $log = new model_log();
                    $arr = array(
                        'log_action'        => 'delete',
                        'user_id'           => $_SESSION['user_id'],
                        'log_object'        => 'chuyên mục tin tức',
                        'log_object_id'     => $result[0]['news_categories_id'],
                        'log_object_name'   => $result[0]['news_categories_name'],
                        'user_name'         => $_SESSION['user_name']
                    );
                    $log->InsertLog($arr);
                    header('Location:'.URL.'news/categories/');
                }
            }else{
                header('Location:'.URL.'news/categories/');
            }
        }else{
            header('Location:'.URL.'news/categories/');
        }
    }

    function categories(){
        require_once 'plugin/url-seo/url-seo.php';
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($create[0]['permission_detail'],'new') || !strpos($delete[0]['permission_detail'],'new') || !strpos($edit[0]['permission_detail'],'new')){
                header('Location:'.URL.'admin/');
            }
        }

        $model = new model_news();

        $model1 = new model_setting();

        $allsetting = $model1->GetAllSetting();

        $this->view->allsetting = $allsetting;

        if($_POST){
            $product_type         = $_POST['news_type'];
            $product_type_english = $_POST['news_type_english'];
            $product_fathertype   = $_POST['news_fathertype'];

            if($product_fathertype){
                $product_father = $product_fathertype;
            }else{
                $product_father = 0;
            }

            $data = array(
                'news_categories_name'          => $product_type,
                'news_categories_name_english'  => $product_type_english,
                'news_categories_father'        => $product_father,
                'news_categories_url'           => '',
                'news_categories_url_english'   => ''
            );

            $result = $model->InsertCategoriesNews($data);
            if($result){
                $date = date('dmY');
                $uniqueid = uniqid();
                $product_url                  = post_slug($product_type).'-postcategory-'.$result;
                $product_type_url_english     = post_slug($product_type_english).'-postcategory-'.$result;
                $data1 = array(
                    'news_categories_url'           => $product_url,
                    'news_categories_url_english'   => $product_type_url_english
                );
                $update = $model->UpdateCategoriesNews($data1,$result);
                if($update){
                    $log = new model_log();
                    $arr = array(
                        'log_action'        => 'create',
                        'user_id'           => $_SESSION['user_id'],
                        'log_object'        => 'chuyên mục tin tức',
                        'log_object_id'     => $result,
                        'log_object_name'   => $product_type,
                        'user_name'         => $_SESSION['user_name']
                    );
                    $log->InsertLog($arr);
                    header('Location:'.URL.'news/categories/');
                }
            }
        }

        if($model->SelectAllCategoriesNews()){
            $this->view->listcategoriesnews = $model->SelectAllCategoriesNews();
        }

        if($model->SelectAllNews()){
            $this->view->listnews = $model->SelectAllNews();
        }

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/newscat.js'
        );

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin1');
        $this->view->Render('news/categories');
        return true;
    }

    /*
     *  Các function của label news
     */

    function editlabel($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $model = new model_news();

        if($path){
            if($model->CategoriesNewsLabelById($path)){

                $result = $model->CategoriesNewsLabelById($path);

                $this->view->labelinfo = $result;

                if($_POST){
                    $product_label = $_POST['news_label'];
                    $product_labelcolor = $_POST['news_labelcolor'];

                    $data = array(
                        'label_name' => $product_label,
                        'label_color' => $product_labelcolor
                    );

                    $result = $model->UpdateCategoriesNewsLabel($data,$path);
                    if($result){
                        header('Location:'.URL.'news/label/');
                    }
                }

            }else{
                header('Location:'.URL.'news/label/');
            }

        }else{
            header('Location:'.URL.'news/label/');
        }

        $this->view->css = array(
            URL.'public/css/colorpicker.css'
        );

        $this->view->js = array(
            URL.'public/js/colorpicker/colorpicker.js',
            URL.'public/js/label.js'
        );

        $this->view->title = 'Trang quản trị - Thiệp 24h';

        $this->view->layout('layout-admin');
        $this->view->Render('news/editlabel');
        return true;
    }

    function deletelabel($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $model = new model_news();

        if($path){

            if($model->CategoriesNewsLabelById($path)){

                $delete = $model->DeleteCategoriesNewsLabel($path);

                if($delete){
                    header('Location:'.URL.'news/label/');
                }

            }else{
                header('Location:'.URL.'news/label/');
            }

        }else{
            header('Location:'.URL.'news/label/');
        }
    }

    function label(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $model = new model_news();

        if($model->SelectAllCategoriesNewsLabel()){
            $this->view->listlabel = $model->SelectAllCategoriesNewsLabel();
        }

        if($_POST){
            $product_label = $_POST['news_label'];
            $product_labelcolor = $_POST['news_labelcolor'];

            $data = array(
                'label_name' => $product_label,
                'label_color' => $product_labelcolor
            );

            $result = $model->InsertCategoriesNewsLabel($data);
            if($result){
                header('Location:'.URL.'news/label/');
            }
        }

        $this->view->css = array(
            URL.'public/css/colorpicker.css'
        );

        $this->view->js = array(
            URL.'public/js/colorpicker/colorpicker.js',
            URL.'public/js/label.js'
        );
        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin');
        $this->view->Render('news/label');
        return true;
    }

    function detail($path = null){
        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $partner = new model_partner();

        $library = new model_library();
		
		$video = new model_video();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOn()){
            $allmenufather = $model1->SelectAllMenuFatherOn();

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }


        $model4 = new model_news();

        if($model4->SelectAllCategoriesNews()){
            $this->view->listcategoriesnews = $model4->SelectAllCategoriesNews();
        }

        if($path){
            $part = explode('_',$path);
            $id = $part[count($part)-1];
            $result = $model4->SelectNewsById($id);

            if($result){

                foreach ($result as $key => $value) {
                    if($comment->NumCommentByPostId($id)){
                        $numcomment = $comment->NumCommentByPostId($id);
                        $result[$key]['news_comment'] = $numcomment[0]['numcomment'];
                    }
                }

                $view = intval($result[0]['view']) + 1;

                $this->view->newsinfo = $result;

                $catnews = $model4->GetCategoriesById($result[0]['news_categories_id']);

                $this->view->catinfo = $catnews;

                if($model4->SelectAllNewsByCatId($result[0]['news_categories_id'])){
                    $this->view->listrelatenews = $model4->SelectAllNewsByCatId($result[0]['news_categories_id']);
                }
					
                    $this->view->title = $result[0]['news_name'].'-'.$allsetting[0]['config_title'];
					$this->view->description = $result[0]['news_name'].'-'.$allsetting[0]['config_description'];
					$this->view->keyword = $result[0]['news_name'].','.$allsetting[0]['config_keyword'];
				

                if($catnews){
                    $this->view->catenews = $catnews;
                }

                $arr = array(
                    'view'  =>$view
                );

                $update = $model4->UpdateNews($arr,$id);

                if($news->SelectAllCategoriesNews()){
                    $this->view->listnewscategories = $news->SelectAllCategoriesNews();
                }

                if($comment->NumCommentByPostId($id)){
                    $this->view->comment = $comment->NumCommentByPostId($id);
                }

                if($comment->GetCommentByPostId($id)){
                    $this->view->listcomment = $comment->GetCommentByPostId($id);
                }
            }
        }
        
		$useragent=$_SERVER['HTTP_USER_AGENT'];
		
		$this->view->layout('layout');
		$this->view->Render('news/detail');
		return true;
    }

    function UploadMedia(array $data) {
        $type = '';

        if(($data["type"] == "image/jpeg")
            || ($data["type"] == "image/jpg") || ($data["type"] == "image/pjpeg")){
                $type = 'jpg';  
        }elseif($data["type"] == "image/gif"){
            $type = 'gif';
        }elseif($data["type"] == "image/png"){
            $type = 'png';
        }
        if ((($data["type"] == "image/gif") || ($data["type"] == "image/jpeg")
                || ($data["type"] == "image/jpg") || ($data["type"] == "image/pjpeg")
                || ($data["type"] == "image/x-png") || ($data["type"] == "image/png"))
            && ($data["size"] < 4000000)) {
            if ($data["error"] == 0) {
                $path = "public/upload/images/another/images/" . uniqid(). '.'.$type;
                move_uploaded_file($data["tmp_name"], $path);
                return $path;
            }
        }
        return false;
    }

    function media(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $directory = 'public/upload/images/another/images/';
        $imagesjpg = glob($directory . "*.jpg");
        $imagespng = glob($directory . "*.png");

        $this->view->listimagejpg = $imagesjpg;
        $this->view->listimagepng = $imagespng;

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/media.js'
        );

        $this->view->layout('layout-admin1');
        $this->view->Render('news/media');
        return true;
    }

    public function createmedia(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }
        require_once 'plugin/phpthumb/ThumbLib.inc.php';

        if($_POST){
            if(strlen($_FILES['file']['name'])>1){
                $library_type = $_FILES['file']['type'];
                $library_name = $_FILES['file']['name'];
                $upload_img = $this->UploadMedia($_FILES['file']);
            }
        }
    }

    public function deletemedia(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }
        $directory = 'public/upload/images/another/images/';
        $imagesjpg = glob($directory . "*.jpg");
        $imagespng = glob($directory . "*.png");

        if($_POST){
            $nameimage = $_POST['id'];
            foreach ($imagesjpg as $value) {
                if(strpos($value,$nameimage)){
                    $this->DeleteFile(URL.$value);
                }
            }
        }
    }
}