<?php
class controller_advertise extends lib_controller{

    public function __construct() {
        parent::__construct();
        if(lib_session::Logged() == false){
            header('Location:'.URL.'login');
        }
    }

    /*
     * Các function tạo hình ảnh
     */

    function UploadImage(array $data) {
        $type = '';
        if(($data["type"] == "image/jpeg")
            || ($data["type"] == "image/jpg") || ($data["type"] == "image/pjpeg")){
                $type = 'jpg';  
        }elseif($data["type"] == "image/gif"){
            $type = 'gif';
        }elseif($data["type"] == "image/png"){
            $type = 'png';
        }
        if ((($data["type"] == "image/gif") || ($data["type"] == "image/jpeg")
                || ($data["type"] == "image/jpg") || ($data["type"] == "image/pjpeg")
                || ($data["type"] == "image/x-png") || ($data["type"] == "image/png"))
            && ($data["size"] < 4000000)) {
            if ($data["error"] == 0) {
                $path = "public/upload/images/library/" . uniqid() . '.'.$type;
                move_uploaded_file($data["tmp_name"], $path);
                return $path;
            }
        }
        return false;
    }

    protected function DeleteFile($file_path) {
        $path = str_replace(URL, '', $file_path);
        if (file_exists($path)) {
            unlink($path);
            return true;
        }
        return false;
    }

    public function createadvertise(){
    	require_once 'plugin/phpthumb/ThumbLib.inc.php';
        require_once 'plugin/url-seo/url-seo.php';

        $advertise = new model_advertise();

        if($_POST){
        	$advertise_start 	= date('Y-m-d H:i:s',strtotime($_POST['advertise_start']));
        	$advertise_end 	 	= date('Y-m-d H:i:s',strtotime($_POST['advertise_end']));
        	$advertise_url   	= $_POST['advertise_url'];
        	$advertise_position	= $_POST['advertise_position'];
        	if($_POST['advertise_status']){
                $advertise_status   = $_POST['advertise_status'];
            }else{
                $advertise_status = 'off';
            }

        	if(strlen($_FILES['hinh_img']['name'])>1){
                $upload_img = $this->UploadImage($_FILES['hinh_img']);

                if($upload_img){
                    $advertise_img   = URL.$upload_img;
                }else{
                    $advertise_img = '';
                }
            }else{
                $advertise_img = '';
            }

            $data = array(
                'advertise_url'      => $advertise_url,
                'advertise_image'    => $advertise_img,
                'advertise_start'    => $advertise_start,
                'advertise_end'      => $advertise_end,
                'advertise_position' => $advertise_position,
                'advertise_status'	 => $advertise_status
            );

            $result = $advertise->InsertAdvertise($data);
            if($result){
            	echo 1;
            }
        }
    }

    public function updateadvertise(){
    	require_once 'plugin/phpthumb/ThumbLib.inc.php';
        require_once 'plugin/url-seo/url-seo.php';

        $advertise = new model_advertise();

        if($_POST){
        	$advertise_id		= $_POST['advertise_id'];
        	$advertiseinfo = $advertise->GetAdvertiseById($advertise_id);
        	$advertise_start 	= date('Y-m-d H:i:s',strtotime($_POST['advertise_start']));
        	$advertise_end 	 	= date('Y-m-d H:i:s',strtotime($_POST['advertise_end']));
        	$advertise_url   	= $_POST['advertise_url'];
        	$advertise_position	= $_POST['advertise_position'];
        	if($_POST['advertise_status']){
                $advertise_status	= $_POST['advertise_status'];
            }else{
                $advertise_status = 'off';
            }

        	if(strlen($_FILES['hinh_img']['name'])>1){
                $upload_img = $this->UploadImage($_FILES['hinh_img']);

                if($upload_img){
                    $advertise_img   = URL.$upload_img;
                    $this->DeleteFile($advertiseinfo[0]['advertise_image']);
                }else{
                    $advertise_img = $advertiseinfo[0]['advertise_image'];
                }
            }else{
                $advertise_img = $advertiseinfo[0]['advertise_image'];
            }

            $data = array(
                'advertise_url'      => $advertise_url,
                'advertise_image'    => $advertise_img,
                'advertise_start'    => $advertise_start,
                'advertise_end'      => $advertise_end,
                'advertise_position' => $advertise_position,
                'advertise_status'	 => $advertise_status
            );

            $result = $advertise->UpdateAdvertise($data,$advertise_id);
            if($result){
            	echo 1;
            }
        }
    }

    public function index(){
    	if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login');
        }

        $this->view->js = array(
        	URL.'public/js/admng.js'
        );

        $advertise = new model_advertise();

        if($advertise->SelectAdvertise()){
        	$this->view->listadvertise = $advertise->SelectAdvertise();
        }

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin');
        $this->view->Render('advertise/index');
        return true;
    }

    public function editadvertise($path){
    	if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login');
        }

        $this->view->js = array(
        	URL.'public/js/admng.js'
        );

        $advertise = new model_advertise();

        if($path){
        	if($advertise->GetAdvertiseById($path)){
        		$advertiseinfo = $advertise->GetAdvertiseById($path);
        		$this->view->advertiseinfo = $advertiseinfo;
        	}else{
        		header('Location:'.URL.'advertise');
        	}
        }else{
        	header('Location:'.URL.'advertise');
        }

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin');
        $this->view->Render('advertise/editadvert');
        return true;
    }

    public function deleteadvertise($path){
    	if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login');
        }

        $advertise = new model_advertise();

        if($path){
        	if($advertise->GetAdvertiseById($path)){
        		$advertiseinfo = $advertise->GetAdvertiseById($path);
    			$imagedelete = $advertiseinfo[0]['advertise_image'];
    			$delete = $advertise->DeleteAdvertise($path);
    			if($delete){
    				$this->DeleteFile($imagedelete);
    				header('Location:'.URL.'advertise');
    			}
        	}else{
        		header('Location:'.URL.'advertise');
        	}
        }else{
        	header('Location:'.URL.'advertise');
        }
    }
}