<?php
class controller_register extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    public function index(){
        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        if($allsetting){
            $this->view->title = 'Trang đăng kí'.' - '.$allsetting[0]['config_title'];
            $this->view->description = 'Trang đăng kí'.' - '.$allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        if($_SESSION['user_id']){
            if($_SESSION['user_type'] == 'admin'){
                header('Location:'.URL.'admin/');
            }else{
                header('Location:'.URL);
            }
        }

        if($_POST){
            $username = $_POST['username'];
            $password = $_POST['password'];
            $repassword = $_POST['repassword'];
            $email = $_POST['email'];
            $reemail = $_POST['reemail'];

            $model = new model_register();

            if(isset($_POST['register'])){
                if(empty($_POST['username'])||empty($_POST['email'])||empty($_POST['reemail'])||empty($_POST['password'])||empty($_POST['repassword'])){
                    $this->view->error = '* Bạn vui lòng xem các thông tin đã nhập.';
                }else{
                    if (!preg_match('/^[A-Za-z][A-Za-z0-9_\.]{2,31}$/', $_POST['username']) ){
                        $this->view->error = '* Username bạn nhập không được chứa ký tực đặc biệt.';
                    }else{
                        if($password == $repassword && $email == $reemail){
                            $salt = md5(uniqid(mt_rand(), true));
                            $pass = $_POST['password'].$salt;
                            $pass = md5($pass);
                            $data=array(
                                'user_email'=>strip_tags($email),
                                'user_name'=>strip_tags($username),
                                'salt'=>$salt,
                                'pass'=>$pass
                            );

                            $result = $model->Index($data);
                            if($result > 1){
                                header('Location: '.URL);
                                exit;
                            }else if($result==-1){
                                $this->view->error = 'Email đã được đăng kí.';
                            }else if($result==-2){
                                $this->view->error = 'Username đã được đăng kí.';
                            }else if($result==-3){
                                $this->view->error = 'Đăng kí bị lỗi, vui lòng thử lại.';
                            }
                        }else{
                            $this->view->error = '* Hai mật khẩu không giống nhau.';
                        }
                    }
                }
            }else{
                $this->view->error = '* Bạn vui lòng nhập đầy đủ thông bên dưới.';
            }
        }

        $this->view->layout('layout-admin-login');
        $this->view->Render('register/index');
        return true;
    }
}