<?php
class controller_dashboard extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    public function index(){
        if(lib_session::Logged() == false){
            header('Location:'.URL);
        }

        $model = new model_general();

        if($model->GetValueUser(lib_session::Get("user_id"))){
            $listnutrition = $model->GetValueUser(lib_session::Get("user_id"));
        }

        if($model->GetValueUserYesterday(lib_session::Get("user_id"))){
            $listnutrition1 = $model->GetValueUserYesterday(lib_session::Get("user_id"));
        }

        if($model->GetValueUserThisWeek(lib_session::Get("user_id"))){
            $listnutrition2 = $model->GetValueUserThisWeek(lib_session::Get("user_id"));
        }

        if($model->GetValueUserThisMonth(lib_session::Get("user_id"))){
            $listnutrition3 = $model->GetValueUserThisWeek(lib_session::Get("user_id"));
        }

        $this->view->listnutrition = $listnutrition;

        $this->view->listnutrition1 = $listnutrition1;

        $this->view->listnutrition2 = $listnutrition2;

        $this->view->listnutrition3 = $listnutrition3;

        $this->view->js = array(
            URL.'public/js/chart.js'
        );

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout');
        $this->view->Render('dashboard/index');
        return true;
    }
}