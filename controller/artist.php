<?php
class controller_artist extends lib_controller{

    public function __construct() {
        parent::__construct();
        if(lib_session::Logged() == true){

        }
    }

    /*
     * Các function tạo hình ảnh
     */

    function UploadImage(array $data) {
        $type = '';
        if(($data["type"] == "image/jpeg")
            || ($data["type"] == "image/jpg") || ($data["type"] == "image/pjpeg")){
                $type = 'jpg';  
        }elseif($data["type"] == "image/gif"){
            $type = 'gif';
        }elseif($data["type"] == "image/png"){
            $type = 'png';
        }
        if ((($data["type"] == "image/gif") || ($data["type"] == "image/jpeg")
                || ($data["type"] == "image/jpg") || ($data["type"] == "image/pjpeg")
                || ($data["type"] == "image/x-png") || ($data["type"] == "image/png"))
            && ($data["size"] < 4000000)) {
            if ($data["error"] == 0) {
                $path = "public/upload/images/artist/" . uniqid() . '.'.$type;
                move_uploaded_file($data["tmp_name"], $path);
                return $path;
            }
        }
        return false;
    }

    protected function DeleteFile($file_path) {
        $path = str_replace(URL, '', $file_path);
        if (file_exists($path)) {
            unlink($path);
            return true;
        }
        return false;
    }

    public function index(){

        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $partner = new model_partner();

        $library = new model_library();
        
        $video = new model_video();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
            $this->view->configurl = $allsetting[0]['config_url'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $artist = new model_artist();

        if($artist->GetAllArtist()){
            $this->view->listartist = $artist->GetAllArtist();
        }

        $this->view->layout('layout');
        $this->view->Render('artist/artist');
        return true;
    }

    public function artist(){
    	if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($edit[0]['permission_detail'],'new')){
                header('Location:'.URL.'admin/');
            }
        }

    	$artist = new model_artist();

    	if($artist->GetAllArtist()){
    		$this->view->listartist = $artist->GetAllArtist();
    	}

    	$this->view->layout('layout-admin1');
        $this->view->Render('artist/index');
       	return true;
    }

    public function addartist(){
    	if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($create[0]['permission_detail'],'artist')){
                header('Location:'.URL.'admin/');
            }
        }

        $event = new model_event();

        if($event->GetAllEvent()){
            $this->view->listevent = $event->GetAllEvent();
        }

    	$this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/artist.js'
        );

    	$this->view->layout('layout-admin1');
        $this->view->Render('artist/addartist');
       	return true;
    }

    public function editartist($path){
    	$artist = new model_artist();
    	if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($edit[0]['permission_detail'],'artist')){
                header('Location:'.URL.'admin/');
            }
        }

    	if($path){
    		if($artist->GetArtistById($path)){
    			$artistinfo = $artist->GetArtistById($path);

    			$this->view->artistinfo = $artistinfo;

    			$event = new model_event();

		        if($event->GetAllEvent()){
		            $this->view->listevent = $event->GetAllEvent();
		        }
    		}
    	}else{
    		header('Location:'.URL.'artist/artist/');
    	}

    	$this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/artist.js'
        );

    	$this->view->layout('layout-admin1');
        $this->view->Render('artist/editartist');
       	return true;
    }

    public function createartist(){
    	require_once 'plugin/phpthumb/ThumbLib.inc.php';
        require_once 'plugin/url-seo/url-seo.php';
        $artist = new model_artist();

        if($_POST){
            $artist_name                 = $_POST['artist_name'];
            $artist_description          = $_POST['artist_description'];
            $artist_event				 = '';

            if(strlen($_FILES['event_img']['name'])>1){
                $upload_img = $this->UploadImage($_FILES['event_img']);

                if($path_save_img){
                    $artist_img   = URL.$upload_img;
                }else{
                    $artist_img = '';
                }
            }else{
                $artist_img = '';
            }

            if($_POST['artist_event']){
				if(count($_POST['artist_event']) >= 1){
					foreach($_POST['artist_event'] as $val){
						$artist_event .= ','.$val;
					}
				}else{
					$arr = $_POST['artist_event'];
					$artist_event = $arr[0];
				}
			}else{
				$artist_event = '';
			}

            $data = array(
                'artist_name'          => $artist_name,
                'artist_description'   => $artist_description,
                'artist_event'		   => $artist_event,
                'artist_image' 		   => $artist_img
            );

            $result = $artist->InsertArtist($data);
            //print_r($result);
            if($result){
                $artist_url = post_slug($artist_name).'-artist-'.$result;
                $data1 = array(
                    'artist_url' => $artist_url
                );
                $update = $artist->UpdateArtist($data1,$result);
                if($update){
                    echo $result;
                }
            }
        }
    }

    public function updateartist(){
    	require_once 'plugin/phpthumb/ThumbLib.inc.php';
        require_once 'plugin/url-seo/url-seo.php';

        $artist = new model_artist();

        if($_POST){
            $artist_id             = $_POST['artist_id'];
            $artist_info           = $artist->GetArtistById($artist_id);
            $artist_name           = $_POST['artist_name'];
            $artist_description    = $_POST['artist_description'];
            $artist_url            = post_slug($artist_name).'-artist-'.$artist_id;
            $artist_event		   = '';

            if(strlen($_FILES['event_img']['name'])>1){
                $upload_img = $this->UploadImage($_FILES['event_img']);

                if($upload_img){
                    $artist_img   = URL.$upload_img;
                    $this->DeleteFile($artist_info[0]['artist_image']);
                }else{
                    $artist_img = $artist_info[0]['artist_image'];
                }
            }else{
                $artist_img = $artist_info[0]['artist_image'];
            }

            if(isset($_POST['artist_event'])){
				if(count($_POST['artist_event']) >= 1){
					foreach($_POST['artist_event'] as $val){
						$artist_event .= ','.$val;
					}
				}else{
					$artist_event = '';
				}
			}else{
				$artist_event = '';
			}

            $data = array(
                'artist_name'           => $artist_name,
                'artist_description'    => $artist_description,
                'artist_event'		    => $artist_event,
                'artist_image'          => $artist_img,
                'artist_url'            => $artist_url
            );

            $result = $artist->UpdateArtist($data,$artist_id);
            if($result){
                echo 1;
            }
        }
    }

    public function deleteartist($path){
    	$artist = new model_artist();
    	if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($delete[0]['permission_detail'],'artist')){
                header('Location:'.URL.'admin/');
            }
        }

        if($path){
        	if($artist->GetArtistById($path)){
    			$artistinfo = $artist->GetArtistById($path);

    			$deleteartist = $artist->DeleteArtist($path);

    			if($deleteartist){
    				$this->DeleteFile($artistinfo[0]['artist_image']);
    				header('Location:'.URL.'artist/artist/');
    			}
    		}
        }else{
        	header('Location:'.URL.'artist/artist/');
        }
    }

    public function detail(){
        $this->view->layout('layout');
        $this->view->Render('artist/detail');
        return true;
    }
}