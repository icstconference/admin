<?php
class controller_calendar extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    public function index(){
        $model = new model_general();

        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

    }


    public function room(){
        $calendar = new model_calendar();

        if($calendar->GetAllRoom()){
            $this->view->listroom = $calendar->GetAllRoom();
        }

        $event = new model_event();

        if($event->GetAllEvent()){
            $this->view->listevent = $event->GetAllEvent();
        }

        if($_POST){
            $room_name = $_POST['room_name'];
            $event_id  = $_POST['event_id'];

            $arr = array(
                'room_name' => $room_name,
                'event_id'  => $event_id
            );

            $result = $calendar->InsertRoom($arr);
            if($result){
                header('Location:'.URL.'calendar/room/');
            }
        }

        $this->view->layout('layout-admin1');
        $this->view->Render('room/index');
        return true;
    }

    public function editroom($path){
        $calendar = new model_calendar();

        $event = new model_event();

        if($event->GetAllEvent()){
            $this->view->listevent = $event->GetAllEvent();
        }

        if($path){
            if($calendar->GetRoomById($path)){
                $roominfo = $calendar->GetRoomById($path);
                $this->view->roominfo = $roominfo;

                if($_POST){
                    $room_name = $_POST['room_name'];
                    $event_id  = $_POST['event_id'];

                    $arr = array(
                        'room_name' => $room_name,
                        'event_id'  => $event_id
                    );

                    $result = $calendar->UpdateRoom($arr,$path);
                    if($result){
                        header('Location:'.URL.'calendar/room/');
                    }
                }
            }else{
               header('Location:'.URL.'calendar/room/'); 
            }
        }else{
            header('Location:'.URL.'calendar/room/');
        }

        $this->view->layout('layout-admin1');
        $this->view->Render('room/editroom');
        return true;
    }

    public function deleteroom($path){
        $calendar = new model_calendar();

        if($path){
            if($calendar->GetRoomById($path)){
                $delete = $calendar->DeleteRoom($path);
                if($delete){
                    header('Location:'.URL.'calendar/room/');
                }
            }else{
               header('Location:'.URL.'calendar/room/'); 
            }
        }else{
            header('Location:'.URL.'calendar/room/');
        }

        $this->view->layout('layout-admin1');
        $this->view->Render('room/editroom');
        return true;
    }
}