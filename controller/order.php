<?php
class controller_order extends lib_controller{

    public function __construct() {
        parent::__construct();
        if(lib_session::Logged() == true){

        }
    }

    public function index(){
    	if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login/');
        }
    	$cart = new model_cart();
    	$this->view->title = 'Trang quản trị';

    	if($cart->SelectAllCart()){
    		$this->view->listorder = $cart->SelectAllCart();
    	}

        $this->view->js = array(
            URL.'public/js/order.js'
        );

    	$this->view->layout('layout-admin');
        $this->view->Render('order/index');
        return true;
    }

    public function detail(){
        	$model = new model_cart();

        	if($_POST){
        		$idorder = $_POST['idorder'];

        		if($model->SelectCart($idorder)){
        			$cart = $model->SelectCart($idorder);

  					$html = '<table class="table">		     
  								<tr>
		                            <th>Tên khách hàng</th>
		                            <td>'.$cart[0]['order_name'].'</td>
		                        </tr>
		                        <tr>
		                            <th>Email</th>
		                            <td>'.$cart[0]['order_email'].'</td>
		                        </tr>
		                        <tr>
		                            <th>Số điện thoại</th>
		                            <td>'.$cart[0]['order_mobile'].'</td>
		                        </tr>
		                        <tr>
		                            <th>Địa chỉ</th>
		                            <td>'.$cart[0]['order_address'].'</td>
		                        </tr>
		                        <tr>
		                            <th>Sản phẩm cần mua</th>
		                            <td>'.$cart[0]['order_require'].'</td>
		                        </tr>
		                        <tr>
		                            <th>Tổng tiền</th>
		                            <td>'.number_format($cart[0]['order_summoney']).' VNĐ</td>
		                        </tr>
		                        <tr>
		                            <th>Thông tin thêm</th>
		                            <td>'.$cart[0]['order_information'].'</td>
		                        </tr>
		                        <tr>
		                            <th>Ngày mua</th>
		                            <td>'.date('d-m-Y',strtotime($cart[0]['order_create_date'])).'</td>
		                        </tr>';

		            if($cart[0]['order_status'] == 1){
  						$html .= '<tr>
		                            <th>Tình trạng</th>
		                            <td><a class="btn btn-danger">Chưa giao</a></td>
		                        </tr>';
  					}else{
  						$html .= '<tr>
		                            <th>Tình trạng</th>
		                            <td><a class="btn btn-success">Đã giao</a></td>
		                        </tr>';
  					}
		                        
  					$html .= '</table>';

  					echo $html;
        		}else{
        			echo "Giỏ hàng này không tồn tại";
        		}
        	}
        }

    public function check($id){
        $model = new model_cart();

        if($model->SelectCart($id)){
        	$cart = $model->SelectCart($id);

	       	if($cart[0]['order_status'] == 1){
	        	$array = array(
	        		'order_status' => 0
	        	);

	        	$result = $model->UpdateCart($array,$id);
	        	if($result){
	        		header('Location:'.URL.'order');
	        	}
	        }else{
				$array = array(
	        		'order_status' => 1
	        	);

	        	$result = $model->UpdateCart($array,$id);
	        	if($result){
	        		header('Location:'.URL.'order');
	        		}
	        }
        }else{
        	header('Location:'.URL.'order');
        }
     }

    public function delete($path){
        $model = new model_cart();

        if($path){
            if($model->SelectCart($path)){
                $delete = $model->DeleteCart($path);
                if($delete){
                    header('Location:'.URL.'contact');
                }
            }else{
                header('Location:'.URL);
            }
        }else{
            header('Location:'.URL);
        }
    }
}