<?php
class controller_comment extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    public function index(){
    	if(lib_session::Get('user_type') != 1){
            header('Location:'.URL.'login/');
        }

        $comment = new model_comment();

        $news = new model_news();

        $event = new model_event();

        if($comment->SelectAllComment()){
        	$listcomment = $comment->SelectAllComment();
        	foreach ($listcomment as $key=>$value) {
                if($value['comment_type'] == 'news'){
            		if($news->SelectNewsById($value['news_id'])){
            			$newsinfo = $news->SelectNewsById($value['news_id']);
            			$listcomment[$key]['news_info'] = $newsinfo[0];
            		}
                }else{
                    if($event->GetEventById($value['news_id'])){
                        $newsinfo = $event->GetEventById($value['news_id']);
                        $listcomment[$key]['news_info'] = $newsinfo[0];
                    }
                }
        	}

        	$this->view->listcomment = $listcomment;
        }

        $this->view->js = array(
            URL.'public/js/comment.js'
        );

        $this->view->title = 'Trang quản trị';

    	$this->view->layout('layout-admin1');
        $this->view->Render('comment/index');
        return true;
    }

    public function deletecomment($path){
    	$comment = new model_comment();

    	if($path){
    		if($comment->GetCommentById($path)){
    			$delete = $comment->DeleteCommentById($path);
    			if($delete){
    				header('Location:'.URL.'comment');
    			}
    		}else{
    			header('Location:'.URL.'comment');
    		}
    	}else{
    		header('Location:'.URL.'comment');
    	}
    }

    public function detail(){
        	$comment = new model_comment();

        	$news = new model_news();

            $event = new model_event();

        	if($_POST){
        		$idorder = $_POST['idorder'];

        		if($comment->GetCommentById($idorder)){
        			$commentinfo = $comment->GetCommentById($idorder);

                    if($commentinfo[0]['comment_type'] == 'news'){
        			    $newsinfo = $news->SelectNewsById($commentinfo[0]['news_id']);
                    }else{
                        $newsinfo = $event->GetEventById($commentinfo[0]['news_id']);
                    }

  					$html = '<table class="table">		     
  								<tr>
		                            <th>Người bình luận</th>
		                            <td>'.$commentinfo[0]['comment_name'].'</td>
		                        </tr>
		                        <tr>
		                            <th>Email</th>
		                            <td>'.$commentinfo[0]['comment_email'].'</td>
		                        </tr>
		                        <tr>
		                            <th>Lời bình luận</th>
		                            <td>'.$commentinfo[0]['comment_message'].'</td>
		                        </tr>';
                    if($commentinfo[0]['comment_type'] == 'news'){
		            $html      .='<tr>
		                            <th>Bài viết</th>
		                            <td> News: '.$newsinfo[0]['news_name'].'</td>
		                        </tr>';
                    }else{
                        $html      .='<tr>
                                    <th>Bài viết</th>
                                    <td> Event: '.$newsinfo[0]['event_name'].'</td>
                                </tr>';
                    }

		            if($cart[0]['comment_read'] == 1){
  						$html .= '<tr>
		                            <th>Tình trạng</th>
		                            <td><a class="btn btn-success"> Đã duyệt</a></td>
		                        </tr>';
  					}else{
  						$html .= '<tr>
		                            <th>Tình trạng</th>
		                            <td><a class="btn btn-danger">Duyệt</a></td>
		                        </tr>';
  					}
		                        
  					$html .= '</table>';

  					echo $html;
        		}else{
        			echo "Giỏ hàng này không tồn tại";
        		}
        	}
        }

    public function createcomment(){
        require 'plugin/recaptcha/recaptchalib.php';

        $comment = new model_comment();

        $secretKey = '6Le0IiATAAAAAKwir2giFKNYczplCzRW-8_kO09d';
        $response = false;
        $err_message = '';

        if($_POST){
            $news_id         = $_POST['news_id'];
			$news_url		 = $_POST['news_url'];
            $comment_name    = $_POST['comment_name'];
            $comment_type    = $_POST['comment_type'];
            $comment_email   = $_POST['comment_email'];
            $comment_message = $_POST['comment_message'];

            $data = array(
                'news_id'          => $news_id,
                'comment_name'     => addslashes($comment_name),
                'comment_type'     => addslashes($comment_type),
                'comment_email'    => addslashes($comment_email),
                'comment_message'  => addslashes($comment_message)
            );

            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                if(!empty($_POST['g-recaptcha-response'])){
                    $objRecaptcha = new ReCaptcha($secretKey);
                    $response = $objRecaptcha->verifyResponse($_SERVER['REMOTE_ADDR'], $_POST['g-recaptcha-response']);
                }
                if(isset($response->success) && 1 == $response->success){
                    $result = $comment->InsertComment($data);
                    if($result){
                        header('Location:'.URL.$this->view->lang.'/comment/success/'.$result);
                    }
                }
                else{
                    echo '<script type="text/javascript">alert("You are not human");</script>';
                }
            }
        }else{
            header('Location:'.URL);
        }
    }
	
	public function check($id){
        $model = new model_comment();

        if($model->GetCommentById($id)){
        	$comment = $model->GetCommentById($id);
			
	       	if($comment[0]['comment_read'] == 1){
	        	$array = array(
	        		'comment_read' => 0,
	        	);

	        	$result = $model->UpdateCommentById($array,$id);
	        	if($result){
	        		header('Location:'.URL.'comment');
	        	}
	        }else{
				$array = array(
	        		'comment_read' => 1,
	        	);

	        	$result = $model->UpdateCommentById($array,$id);
	        	if($result){
	        		header('Location:'.URL.'comment');
	        	}
	        }
        }else{
        	header('Location:'.URL.'comment');
        }
     }

    public function success($path){

        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $partner = new model_partner();

        $library = new model_library();

        $event = new model_event();

        $comment = new model_comment();
		
		$video = new model_video();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOnByPostion('all','ngang')){
            $allmenufather = $model1->SelectAllMenuFatherOnByPostion('all','ngang');

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        if($model1->SelectAllMenuFatherOnByPostion('product','doc')){
            $allmenufatherproduct = $model1->SelectAllMenuFatherOnByPostion('product','doc');

            foreach($allmenufatherproduct as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufatherproduct[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenuproduct = $allmenufatherproduct;
        }

        if($advertise->GetAdvertiseByPosition(1)){
            $this->view->listadvertise1 = $advertise->GetAdvertiseByPosition(1);
        }

        if($advertise->GetAdvertiseByPosition(2)){
            $this->view->listadvertise2 = $advertise->GetAdvertiseByPosition(2);
        }

        if($advertise->GetAdvertiseByPosition(3)){
            $this->view->listadvertise3 = $advertise->GetAdvertiseByPosition(3);
        }

        if($news->SelectTopNewsView(4)){
            $this->view->listnewsview = $news->SelectTopNewsView(4);
        }

        if($news->SelectTopNewsRelease(4)){
            $this->view->listnewsrelease = $news->SelectTopNewsRelease(4);
        }

        if($linker->SelectAlllinker()){
            $this->view->listlinker = $linker->SelectAlllinker();
        }

        if($news->SelectTopNewsRelease(5)){
            $this->view->listnewsnew = $news->SelectTopNewsRelease(5);
        }
		
		if($video->SelectAllVideo()){
			$this->view->listvideo = $video->SelectAllVideo();
		}

        if($question->SelectAllQuestion()){
            $this->view->listquestion = $question->SelectAllQuestion();
        }

        if($partner->SelectAllPartner()){
            $this->view->listpartner = $partner->SelectAllPartner();
        }

        if($library->SelectAllLibrary()){
            $this->view->listlibrary = $library->SelectAllLibrary();
        }

        $model4 = new model_news();

        if($model4->SelectAllCategoriesNews()){
            $this->view->listnewscategories = $model4->SelectAllCategoriesNews();
        }

        if($model4->SelectTopNewsRelease(4)){
            $this->view->topnews = $model4->SelectTopNewsRelease(4);
         }

        if($path){
            if($comment->GetCommentById($path)){
				
				$commentinfo = $comment->GetCommentById($path);
				
				$newsid = $commentinfo[0]['news_id'];
				
				$newsinfo = $news->SelectNewsById($newsid);
				
				$newsurl = $newsinfo[0]['news_url'];
				
				$this->view->newsurl = $newsurl;

            }else{
                header('Location:'.URL);
            }
        }else{
            header('Location:'.URL);
        }

        $this->view->layout('layout');
        $this->view->Render('comment/success');

        return true;
    }   
}