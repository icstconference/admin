<?php
class controller_logout  extends lib_controller
{
    public function __construct() {
        parent::__construct();
        session_destroy();
        //CCookie::Del('url');
        header("Location: ".URL);
        exit;
    }
}
