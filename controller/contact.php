<?php
class controller_contact extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    public function index(){
        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $partner = new model_partner();

        $library = new model_library();
        
        $video = new model_video();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
            $this->view->aboutus = $allsetting[0]['config_url_aboutus'];
            $this->view->laboratories = $allsetting[0]['config_url_laboratories'];
            $this->view->emaillink = $allsetting[0]['config_url_email'];
            $this->view->internal = $allsetting[0]['config_url_internal'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOn()){
            $allmenufather = $model1->SelectAllMenuFatherOn();

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }
        
        $contact = new model_contact();

        if($model->GetAllContact()){
            $listcontact = $model->GetAllContact();

            $this->view->listcontact = $listcontact;
        }

		$useragent=$_SERVER['HTTP_USER_AGENT'];
		
			$this->view->layout('layout');
			$this->view->Render('lienhe/index');
			return true;
    }

    public function contact(){
    	if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }
        $contact = new model_contact();

    	$this->view->title = 'Trang quản trị';

    	if($contact->SelectAllContact()){
    		$this->view->listcontact = $contact->SelectAllContact();
    	}

        $this->view->js = array(
            URL.'public/js/contactuser.js'
        );

    	$this->view->layout('layout-admin1');
        $this->view->Render('contact/index');
        return true;
    }

    public function addcontact(){
    	$contact = new model_contact();

    	if($_POST){
    		$contact_user_name    = $_POST['first-name'].$_POST['last-name'];
    		$contact_user_email   = $_POST['your-email'];
    		$contact_user_mobile  = $_POST['your-phone'];
    		$contact_user_message = $_POST['your-message'];

    		$arr = array(
    			'contact_user_name'		=> $contact_user_name,
    			'contact_user_email'	=> addslashes($contact_user_email),
    			'contact_user_mobile'	=> addslashes($contact_user_mobile),
    			'contact_user_message'	=> addslashes($contact_user_message)
    		);

    		$result = $contact->InsertContact($arr);

    		if($result){
                header('Location:'.URL.'contact/contactsuccess/'.$result);
    		}
    	}else{
    		header('Location:'.URL);
    	}
    }

    public function contactsuccess($path){
    	$contact = new model_contact();

        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $partner = new model_partner();

        $library = new model_library();
		
		$video = new model_video();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
			$this->view->aboutus = $allsetting[0]['config_url_aboutus'];
            $this->view->laboratories = $allsetting[0]['config_url_laboratories'];
			$this->view->emaillink = $allsetting[0]['config_url_email'];
            $this->view->internal = $allsetting[0]['config_url_internal'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOnByPostion('all','ngang')){
            $allmenufather = $model1->SelectAllMenuFatherOnByPostion('all','ngang');

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        if($model1->SelectAllMenuFatherOnByPostion('product','doc')){
            $allmenufatherproduct = $model1->SelectAllMenuFatherOnByPostion('product','doc');

            foreach($allmenufatherproduct as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufatherproduct[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenuproduct = $allmenufatherproduct;
        }

        if($advertise->GetAdvertiseByPosition(1)){
            $this->view->listadvertise1 = $advertise->GetAdvertiseByPosition(1);
        }

        if($advertise->GetAdvertiseByPosition(2)){
            $this->view->listadvertise2 = $advertise->GetAdvertiseByPosition(2);
        }

        if($advertise->GetAdvertiseByPosition(3)){
            $this->view->listadvertise3 = $advertise->GetAdvertiseByPosition(3);
        }

        if($news->SelectTopNewsView(4)){
            $this->view->listnewsview = $news->SelectTopNewsView(4);
        }

        if($news->SelectTopNewsRelease(4)){
            $this->view->listnewsrelease = $news->SelectTopNewsRelease(4);
        }

        if($linker->SelectAlllinker()){
            $this->view->listlinker = $linker->SelectAlllinker();
        }

        if($news->SelectTopNewsRelease(5)){
            $this->view->listnewsnew = $news->SelectTopNewsRelease(5);
        }

        if($news->SelectAllNewsByCatId(6)){
            $newschuyenganh = $news->SelectAllNewsByCatId(6);

            foreach ($newschuyenganh as $key=>$value) {
                if($comment->NumCommentByPostId($value['news_id'])){
                    $numcomment = $comment->NumCommentByPostId($value['news_id']);
                    $newschuyenganh[$key]['news_comment'] = $numcomment[0]['numcomment'];
                }
            }

            $this->view->newschuyenganh = $newschuyenganh;
        }

        if($news->SelectAllNewsByCatId(7)){
            $newshoi = $news->SelectAllNewsByCatId(7);
            
            foreach ($newshoi as $key=>$value) {
                if($comment->NumCommentByPostId($value['news_id'])){
                    $numcomment = $comment->NumCommentByPostId($value['news_id']);
                    $newshoi[$key]['news_comment'] = $numcomment[0]['numcomment'];
                }
            }

            $this->view->newshoi = $newshoi;
        }

        if($question->SelectAllQuestion()){
            $this->view->listquestion = $question->SelectAllQuestion();
        }

        if($partner->SelectAllPartner()){
            $this->view->listpartner = $partner->SelectAllPartner();
        }

        if($library->SelectAllLibrary()){
            $this->view->listlibrary = $library->SelectAllLibrary();
        }

		if($video->SelectAllVideo()){
			$this->view->listvideo = $video->SelectAllVideo();
		}
		
        $this->view->title = 'Contact us'.'-'.$allsetting[0]['config_title'];
        $this->view->description = 'Contact us'.'-'.$allsetting[0]['config_description'];
        $this->view->keyword = 'Contact us'.','.$allsetting[0]['config_keyword'];

        if($path){
            if($contact->SelectContactById($path)){
                $this->view->layout('layout');
                $this->view->Render('cart/success1');
                return true;
            }else{
                header('Location:'.URL);
            }
        }else{
            header('Location:'.URL);
        }
    }

    public function detail(){
            $model = new model_contact();

            if($_POST){
                $idorder = $_POST['idorder'];

                if($model->SelectContactById($idorder)){
                    $cart = $model->SelectContactById($idorder);

                    $html = '<table class="table">           
                                <tr>
                                    <th>Tên khách hàng</th>
                                    <td>'.$cart[0]['contact_user_name'].'</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>'.$cart[0]['contact_user_email'].'</td>
                                </tr>
                                <tr>
                                    <th>Số điện thoại</th>
                                    <td>'.$cart[0]['contact_user_mobile'].'</td>
                                </tr>
                                <tr>
                                    <th>Nôi dung</th>
                                    <td>'.$cart[0]['contact_user_message'].'</td>
                                </tr>
                                <tr>
                                    <th>Ngày nhắn</th>
                                    <td>'.date('d-m-Y',strtotime($cart[0]['contact_user_createdate'])).'</td>
                                </tr>';      
                    $html .= '</table>';

                    echo $html;
                }else{
                    echo "Giỏ hàng này không tồn tại";
                }
            }
        }

    public function delete($path){
        $model = new model_contact();

        if($path){
            if($model->SelectContactById($path)){
                $delete = $model->DeleteContact($path);
                if($delete){
                    header('Location:'.URL.'contact/contact/');
                }
            }else{
                header('Location:'.URL);
            }
        }else{
            header('Location:'.URL);
        }
    }
}
