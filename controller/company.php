<?php
class controller_company extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    public function index($path = null,$page = 1){

    	$model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        if($allsetting){
            $this->view->title = 'Company Snapshotour - '.$allsetting[0]['config_title'];
            $this->view->description = 'Company Snapshotour - '.$allsetting[0]['config_description'];
            $this->view->keyword = 'Company Snapshotour - '.$allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
			$this->view->configurl = $allsetting[0]['config_url'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOnByPostion('all','ngang')){
            $allmenufather = $model1->SelectAllMenuFatherOnByPostion('all','ngang');

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        $news = new model_news();

        if($news->SelectAllCategoriesNews()){
            $this->view->listnewscategories = $news->SelectAllCategoriesNews();
        }

        if($path){
        	$part = explode('_',$path);
			if(count($part) > 1){
				$id = $part[count($part)-1];
				$result = $news->GetCategoriesById($id);

				if($result){
					$this->view->companyinfo = $result;

					$this->view->title = $result[0]['news_categories_name'].'-'.$allsetting[0]['config_title'];
					$this->view->description = $result[0]['news_categories_name'].'-'.$allsetting[0]['config_description'];
					$this->view->keyword = $result[0]['news_categories_name'].','.$allsetting[0]['config_keyword'];
					
					
					if($page == 1){
						$iteminpage = 5;
					}

					if($page > 1){
						$iteminpage = 5;
					}

					$path = str_replace("_", "-", $path);

					$pagination = new lib_pagination();
					$pagination->SetCurrentPage($page);
					$pagination->SetRowInPage($iteminpage);
					$pagination->SetUrl(URL . 'category/company/'.$path.'/@page');
					$pagination->SetRange(4);
					$pagination->SetLanguage(array(
						'first'     => '<<',
						'previous'  => '<',
						'next'      => '>',
						'last'      => '>>'
					));
					$paginator  = $pagination->BuildPagination($this->GetBlogDB1($id));

					$this->view->paginator  = $paginator['paginator'];

					$listnew   = $news->PaginationSnapshot($page,$iteminpage,$id);

					if($listnew){
						foreach ($listnew as $key => $value) {
							if($news->GetCategoriesNewsById($value['news_id'])){
								$newsinfo = $news->GetCategoriesNewsById($value['news_id']);
								$listnew[$key]['category'] = $newsinfo[0];
							}
							if($comment->NumCommentByPostId($value['news_id'],'news')){
			                    $commentinfo = $comment->NumCommentByPostId($value['news_id'],'news');
			                    $listnew[$key]['comment'] = $commentinfo[0]['numcomment'];
			                }
			            }

			            $this->view->listnew = $listnew;
			        }

		            if($news->SelectTopNewsRelease(4)){
		                $this->view->topnews = $news->SelectTopNewsRelease(4);
		            }	
				}
			}
        }else{
        	if($page == 1){
                $iteminpage = 5;
            }

            if($page > 1){
                $iteminpage = 5;
            }

            $pagination = new lib_pagination();
            $pagination->SetCurrentPage($page);
            $pagination->SetRowInPage($iteminpage);
            $pagination->SetUrl(URL . 'category/news/@page');
            $pagination->SetRange(4);
            $pagination->SetLanguage(array(
                'first'     => '<<',
                'previous'  => '<',
                'next'      => '>',
                'last'      => '>>'
            ));
            $paginator  = $pagination->BuildPagination($this->GetBlogDB1(0));

            $this->view->paginator  = $paginator['paginator'];

            $listnew   = $news->PaginationSnapshot($page,$iteminpage,0);

            $comment = new model_comment();

            if($news->SelectAllCategoriesNews()){
                $this->view->listnewscategories = $news->SelectAllCategoriesNews();
            }
			
            foreach ($listnew as $key => $value) {
                if($news->GetCategoriesNewsById($value['news_id'])){
                    $newsinfo = $news->GetCategoriesNewsById($value['news_id']);
                    $listnew[$key]['category'] = $newsinfo[0];
                }
                if($comment->NumCommentByPostId($value['news_id'],'news')){
                    $commentinfo = $comment->NumCommentByPostId($value['news_id'],'news');
                    $listnew[$key]['comment'] = $commentinfo[0]['numcomment'];
                }
            }

            if($news->SelectTopNewsRelease(4)){
                $this->view->topnews = $news->SelectTopNewsRelease(4);
            }

            $this->view->listnew = $listnew;
        }

    	$this->view->layout('layout');
        $this->view->Render('company/index');
        return true;
    }

    private function GetBlogDB1($catid) {
        $db = new lib_db();

        if($catid != 0){
            return $db->query('select *
                from tbl_news
                where news_categories_id ='.$catid.' and news_display ="company" order by news_create_date DESC');
        }else{
            return $db->query('select *
                from tbl_news where news_display ="company" order by news_create_date DESC');
        }
    }
}