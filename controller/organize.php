<?php
class controller_organize extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    public function index(){
    	if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $organize = new model_organize();

        if($organize->SelectAllOrganize()){
        	$this->view->listorganize = $organize->SelectAllOrganize();
        }

        $event = new model_event();

        if($event->GetAllEvent()){
            $this->view->listevent = $event->GetAllEvent();
        }

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin1');
        $this->view->Render('organize/index');
        return true;
    }

    public function addorganize(){
    	if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $event = new model_event();

        if($event->GetAllEvent()){
            $this->view->listevent = $event->GetAllEvent();
        }

        $organize = new model_organize();

        if($_POST){
        	$organize_name   = $_POST['organize_name'];
        	$event_id 	   	 = $_POST['event_id'];
        	$organize_member = $_POST['organize_member'];

        	$data = array(
        		'organize_name' 	=> $organize_name,
        		'organize_member'	=> $organize_member,
        		'event_id'			=> $event_id
        	);

        	$result = $organize->InsertOrganize($data);

        	if($result){
        		header('Location:'.URL.'organize/');
        	}
        }

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/organize.js'
        );

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin1');
        $this->view->Render('organize/addorganize');
        return true;
    }

    public function editorganize($path){
    	if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $event = new model_event();

        if($event->GetAllEvent()){
            $this->view->listevent = $event->GetAllEvent();
        }

        $organize = new model_organize();

        if($path){
        	$organizeinfo = $organize->GetOrganizeById($path);

        	if($organizeinfo){
        		$this->view->organizeinfo = $organizeinfo;

        		if($_POST){
		        	$organize_name   = $_POST['organize_name'];
		        	$event_id 	   	 = $_POST['event_id'];
		        	$organize_member = $_POST['organize_member'];

		        	$data = array(
		        		'organize_name' 	=> $organize_name,
		        		'organize_member'	=> $organize_member,
		        		'event_id'			=> $event_id
		        	);

		        	$result = $organize->UpdateOrganize($data,$path);

		        	if($result){
		        		header('Location:'.URL.'organize/');
		        	}
		        }
	        }else{
	        	header('Location:'.URL.'organize/');
	        }
        }else{
        	header('Location:'.URL.'organize/');
        }

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/organize.js'
        );

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin1');
        $this->view->Render('organize/addorganize');
        return true;
    }

    public function deletorganize($path){
    	$organize = new model_organize();

        if($path){
        	$organizeinfo = $organize->GetOrganizeById($path);

        	if($organizeinfo){
        		$delete = $organize->DeleteOrganize($path);
        		if($delete){
		        	header('Location:'.URL.'organize/');
		        }
	        }else{
	        	header('Location:'.URL.'organize/');
	        }
        }else{
        	header('Location:'.URL.'organize/');
        }
    }
}