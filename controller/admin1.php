<?php
class controller_admin1 extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    public function index(){
        $model = new model_general();

        $this->view->numuser = $model->CountUser();

        $this->view->numnews= $model->CountNews();

        $this->view->numproduction= $model->CountProduct();

        $this->view->numcategoriesproduct = $model->CountCategoriesProduct();

        $this->view->numcategoriesnews = $model->CountCategoriesNews();

        $this->view->numslider = $model->CountSlider();

        $this->view->numorder = $model->CountOrder();

        $this->view->numpartner = $model->CountPartner();

        $this->view->numquestion = $model->CountQuestions();

        $this->view->numevents = $model->CountEvents();

        $this->view->numcontactuser = $model->CountContactUser();

        $this->view->numemail = $model->CountEmail();

        $this->view->numimage = $model->CountImage();

        $this->view->numadvertise = $model->CountAdvertise();
		
		$this->view->numfile = $model->CountFile();

		if($model->SelectTop5News()){
			$this->view->listnews = $model->SelectTop5News();
		}

		if($model->SelectTop5Files()){
			$this->view->listfiles = $model->SelectTop5Files();
		}

        $this->view->layout('layout-admin1');
        $this->view->Render('admin1/index');
        return true;
    }
}