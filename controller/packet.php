<?php
class controller_packet extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    public function index(){
    	if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $organize = new model_packet();

        if($organize->SelectAllPacket()){
        	$this->view->listpacket = $organize->SelectAllPacket();
        }

        $event = new model_event();

        if($event->GetAllEvent()){
            $this->view->listevent = $event->GetAllEvent();
        }

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin1');
        $this->view->Render('packet/index');
        return true;
    }

    public function addpacket(){
    	if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $event = new model_event();

        if($event->GetAllEvent()){
            $this->view->listevent = $event->GetAllEvent();
        }

        $packet = new model_packet();

        if($_POST){
        	$packet_name        = $_POST['packet_name'];
        	$event_id 	   	    = $_POST['event_id'];
        	$packet_description = $_POST['packet_description'];
            $packet_price       = $_POST['packet_price'];
            $packet_people      = $_POST['packet_people'];

        	$data = array(
        		'packet_name' 	        => $packet_name,
        		'packet_description'	=> $packet_description,
        		'event_id'			    => $event_id,
                'packet_price'          => $packet_price,
                'packet_people'         => $packet_people,
        	);

        	$result = $packet->InsertPacket($data);

        	if($result){
        		header('Location:'.URL.'packet/');
        	}
        }

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/organize.js'
        );

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin1');
        $this->view->Render('packet/addpacket');
        return true;
    }

    public function editpacket($path){
    	if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $event = new model_event();

        if($event->GetAllEvent()){
            $this->view->listevent = $event->GetAllEvent();
        }

        $packet = new model_packet();

        if($path){
        	$packetinfo = $packet->GetPacketById($path);

        	if($packetinfo){
        		$this->view->packetinfo = $packetinfo;

        		if($_POST){
		        	$packet_name        = $_POST['packet_name'];
                    $event_id           = $_POST['event_id'];
                    $packet_description = $_POST['packet_description'];
                    $packet_price       = $_POST['packet_price'];
                    $packet_people      = $_POST['packet_people'];

                    $data = array(
                        'packet_name'           => $packet_name,
                        'packet_description'    => $packet_description,
                        'event_id'              => $event_id,
                        'packet_price'          => $packet_price,
                        'packet_people'         => $packet_people,
                    );

		        	$result = $packet->UpdatePacket($data,$path);

		        	if($result){
		        		header('Location:'.URL.'packet/');
		        	}
		        }
	        }else{
	        	header('Location:'.URL.'packet/');
	        }
        }else{
        	header('Location:'.URL.'packet/');
        }

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/organize.js'
        );

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin1');
        $this->view->Render('packet/editpacket');
        return true;
    }

    public function deletepacket($path){
    	$packet = new model_packet();

        if($path){
        	$packetinfo = $packet->GetPacketById($path);

        	if($packetinfo){
        		$delete = $packet->DeletePacket($path);
        		if($delete){
		        	header('Location:'.URL.'packet/');
		        }
	        }else{
	        	header('Location:'.URL.'packet/');
	        }
        }else{
        	header('Location:'.URL.'packet/');
        }
    }
}