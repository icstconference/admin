<?php
class controller_menu extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    public function delete($path){
        $model = new model_menu();

        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($delete[0]['permission_detail'],'menu')){
                header('Location:'.URL.'admin/');
            }
        }

        if($_POST){
            $menu_id = $_POST['menu_id'];

            if($model->SelectMenuById($menu_id)){
                $menuinfo = $model->SelectMenuById($menu_id);

                if($menuinfo['menu_father'] == 0){
                    if($model->SelectAllChildeLv1ByFatherId($menu_id)){
                        $listchild = $model->SelectAllChildeLv1ByFatherId($menu_id);
                        foreach ($listchild as $value) {
                            $arr = array(
                                'menu_father'   =>  '0'
                            );

                            $update = $model->UpdateMenu($arr,$value['menu_id']);
                        }
                    }
                }
                
                $delete = $model->DeleteMenu($menu_id);
                
                echo '1';
            }
        }
    }

    public function updatemenu(){
        $model2 = new model_menu();

        if($_POST){
            $menuid = $_POST['menuid'];
            $parentid = $_POST['parentid'];

            $arr = array(
                'menu_father'   =>    $parentid
            );

            $update = $model2->UpdateMenu($arr,$menuid);
        }
    }

    public function createpage(){
        $model2 = new model_menu();
        $model3 = new model_page();

        if($_POST){
            $pageidarr = $_POST['pageid'];

            $html = '<script>
                $(".dd a").on("mousedown", function(event) { // mousedown prevent nestable click
                    event.preventDefault();
                    return false;
                });
            </script>';

            foreach ($pageidarr as $value) {
                if($model3->GetPageById($value)){
                    $pageinfo = $model3->GetPageById($value);

                    $arr = array(
                        'menu_name'             =>  $pageinfo[0]['page_name'],
                        'menu_name_english'     =>  $pageinfo[0]['page_name_english'],
                        'menu_url'              =>  $pageinfo[0]['page_url'],
                        'menu_url_english'      =>  $pageinfo[0]['page_url_english'],
                        'menu_kind'             =>  'page'
                    );

                    $result = $model2->InsertMenu($arr);
                    if($result){
                        $html1 = '
                            <li class="dd-item" data-id="'.$result.'">
                                <div class="dd-handle">
                                    <h2 class="panel-title">
                                        '.$pageinfo[0]['page_name'].'
                                        <a data-toggle="collapse" data-parent="#prod" href="#menu'.$result.'" class="" onclick="" aria-expanded="true" style="float: right;text-decoration:none;">
                                            Trang&nbsp;<i class="fa fa-angle-down"></i>                                                
                                        </a>
                                    </h2>
                                </div>
                                <div id="menu'.$result.'" class="panel-collapse collapse" aria-expanded="true" style="margin-top: 5px;">
                                    <div class="panel-body">
                                        <form>
                                            <div class="form-group">
                                                <label>Tiêu đề menu</label>
                                                <input type="text" value="'.$pageinfo[0]['page_name'].'" name="menu_name" id="menu_name'.$result.'" rel="" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Tiêu đề menu tiếng anh</label>
                                                <input type="text" value="'.$pageinfo[0]['page_name_english'].'" name="menu_name_english" id="menu_name_english'.$result.'" rel="" class="form-control">
                                            </div>
                                            <div class="pull-right" style="margin-top:10px;">
                                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-danger" rel="'.$result.'" id="delete">Xóa</button>
                                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" rel="'.$result.'" id="update">Cập nhật</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </li>
                        ';

                        $html.= $html1;
                    }
                }
            }

            echo $html;
        }
    }

    public function createnews(){
        $model1 = new model_news();
        $model2 = new model_menu();

        if($_POST){
            $newsidarr = $_POST['newsid'];

            $html = '<script>
                $(".dd a").on("mousedown", function(event) { // mousedown prevent nestable click
                    event.preventDefault();
                    return false;
                });
            </script>';

            foreach ($newsidarr as $value) {
                if($model1->SelectNewsById($value)){
                    $newsinfo = $model1->SelectNewsById($value);

                    $arr = array(
                        'menu_name'             =>  $newsinfo[0]['news_name'],
                        'menu_name_english'     =>  $newsinfo[0]['news_name_english'],
                        'menu_url'              =>  $newsinfo[0]['news_url'],
                        'menu_url_english'      =>  $newsinfo[0]['news_url_english'],
                        'menu_kind'             =>  'news'
                    );

                    $result = $model2->InsertMenu($arr);
                    if($result){
                        $html1 = '
                            <li class="dd-item" data-id="'.$result.'">
                                <div class="dd-handle">
                                    <h2 class="panel-title">
                                        '.$newsinfo[0]['news_name'].'
                                        <a data-toggle="collapse" data-parent="#prod" href="#menu'.$result.'" class="" onclick="" aria-expanded="true" style="float: right;text-decoration:none;">
                                            Bài viết&nbsp;<i class="fa fa-angle-down"></i>                                                
                                        </a>
                                    </h2>
                                </div>
                                <div id="menu'.$result.'" class="panel-collapse collapse" aria-expanded="true" style="margin-top: 5px;">
                                    <div class="panel-body">
                                        <form>
                                            <div class="form-group">
                                                <label>Tiêu đề menu</label>
                                                <input type="text" value="'.$newsinfo[0]['news_name'].'" name="menu_name" id="menu_name'.$result.'" rel="" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Tiêu đề menu tiếng anh</label>
                                                <input type="text" value="'.$newsinfo[0]['news_name_english'].'" name="menu_name_english" id="menu_name_english'.$result.'" rel="" class="form-control">
                                            </div>
                                            <div class="pull-right" style="margin-top:10px;">
                                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-danger" rel="'.$result.'" id="delete">Xóa</button>
                                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" rel="'.$result.'" id="update">Cập nhật</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </li>
                        ';

                        $html.= $html1;
                    }
                }
            }

            echo $html;
        }
    }

    public function createcustom(){
        $model2 = new model_menu();

        if($_POST){
            $menu_name = $_POST['menu_name'];
            $menu_url  = $_POST['menu_url'];

            $html = '<script>
                $(".dd a").on("mousedown", function(event) { // mousedown prevent nestable click
                    event.preventDefault();
                    return false;
                });
            </script>';

            $arr = array(
                'menu_name'             =>  $menu_name,
                'menu_url'              =>  $menu_url,
                'menu_kind'             =>  'custom'
            );

            $result = $model2->InsertMenu($arr);
                if($result){
                    $html1 = '
                        <li class="dd-item" data-id="'.$result.'">
                            <div class="dd-handle">
                                <h2 class="panel-title">
                                    '.$menu_name.'
                                    <a data-toggle="collapse" data-parent="#prod" href="#menu'.$result.'" class="" onclick="" aria-expanded="true" style="float: right;text-decoration:none;">
                                            Tùy chỉnh&nbsp;<i class="fa fa-angle-down"></i>                                                
                                    </a>
                                </h2>
                            </div>
                            <div id="menu'.$result.'" class="panel-collapse collapse" aria-expanded="true" style="margin-top: 5px;">
                                <div class="panel-body">
                                    <form>
                                        <div class="form-group">
                                            <label>Tiêu đề menu</label>
                                            <input type="text" value="'.$menu_name.'" name="menu_name" id="menu_name'.$result.'" rel="" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Tiêu đề menu tiếng anh</label>
                                            <input type="text" value="" name="menu_name_english" id="menu_name_english'.$result.'" rel="" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Link</label>
                                            <input type="text" value="'.$menu_url.'" name="menu_name_english" id="menu_url'.$result.'" rel="" class="form-control">
                                        </div>
                                        <div class="pull-right" style="margin-top:10px;">
                                            <button type="button" class="mb-xs mt-xs mr-xs btn btn-danger" rel="'.$result.'" id="delete">Xóa</button>
                                            <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" rel="'.$result.'" id="update">Cập nhật</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </li>';

                        $html.= $html1;
                    }
                echo $html;
            }
    }

    public function createnewscategory(){
        $model1 = new model_news();
        $model2 = new model_menu();

        if($_POST){
            $newscategoriesidarr = $_POST['newscategoriesid'];

            $html = '<script>
                $(".dd a").on("mousedown", function(event) { // mousedown prevent nestable click
                    event.preventDefault();
                    return false;
                });
            </script>';

            foreach ($newscategoriesidarr as $value) {
                if($model1->GetCategoriesById($value)){
                    $newsinfo = $model1->GetCategoriesById($value);

                    $arr = array(
                        'menu_name'             =>  $newsinfo[0]['news_categories_name'],
                        'menu_name_english'     =>  $newsinfo[0]['news_categories_name_english'],
                        'menu_url'              =>  $newsinfo[0]['news_categories_url'],
                        'menu_url_english'      =>  $newsinfo[0]['news_categories_url_english'],
                        'menu_kind'             =>  'news category'
                    );

                    $result = $model2->InsertMenu($arr);
                    if($result){
                        $html1 = '
                            <li class="dd-item" data-id="'.$result.'">
                                <div class="dd-handle">
                                    <h2 class="panel-title">
                                        '.$newsinfo[0]['news_categories_name'].'
                                        <a data-toggle="collapse" data-parent="#prod" href="#menu'.$result.'" class="" onclick="" aria-expanded="true" style="float: right;text-decoration:none;">
                                            Chuyên mục&nbsp;<i class="fa fa-angle-down"></i>                                                
                                        </a>
                                    </h2>
                                </div>
                                <div id="menu'.$result.'" class="panel-collapse collapse" aria-expanded="true" style="margin-top: 5px;">
                                    <div class="panel-body">
                                        <form>
                                            <div class="form-group">
                                                <label>Tiêu đề menu</label>
                                                <input type="text" value="'.$newsinfo[0]['news_categories_name'].'" name="menu_name" id="menu_name'.$result.'" rel="" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Tiêu đề menu tiếng anh</label>
                                                <input type="text" value="'.$newsinfo[0]['news_categories_name_english'].'" name="menu_name_english" id="menu_name_english'.$result.'" rel="" class="form-control">
                                            </div>
                                            <div class="pull-right" style="margin-top:10px;">
                                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-danger" rel="'.$result.'" id="delete">Xóa</button>
                                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" rel="'.$result.'" id="update">Cập nhật</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </li>
                        ';

                        $html.= $html1;
                    }
                }
            }

            echo $html;
        }
    }

    public function updatemenuselect(){
        $model2 = new model_menu();

        if($_POST){
            $menu_name          = $_POST['menu_name'];
            $menu_name_english  = $_POST['menu_name_english'];
            $menu_id            = $_POST['menu_id'];

            $arr  = array(
                'menu_name'         => $menu_name,
                'menu_name_english' => $menu_name_english,
            );

            $update = $model2->UpdateMenu($arr,$menu_id);

            if($update){
                echo '1';
            }
        }
    }

    public function updatemenucustom(){
        $model2 = new model_menu();

        if($_POST){
            $menu_name          = $_POST['menu_name'];
            $menu_name_english  = $_POST['menu_name_english'];
            $menu_id            = $_POST['menu_id'];
            $menu_url           = $_POST['menu_url'];

            $arr  = array(
                'menu_name'         => $menu_name,
                'menu_name_english' => $menu_name_english,
                'menu_url'          => $menu_url
            );

            $update = $model2->UpdateMenu($arr,$menu_id);

            if($update){
                echo '1';
            }
        }
    }

    public function updatemenusort(){
        $model2 = new model_menu();

        if($_POST){

            $listmenujson = $_POST['listmenu'];

            $listmenu = json_decode($listmenujson);

            $i=0;
            for($dem=0;$dem<count($listmenu);$dem++){
                $i++;
                $mang = $listmenu[$dem];

                $arr  = array(
                    'menu_order' => $i
                );

                $update = $model2->UpdateMenu($arr,$mang->id);
                if($mang->children){
                    $j=0;
                    $listmenuchild = $mang->children;
                    for($dem1=0;$dem1<count($listmenuchild);$dem1++){
                        $j++;

                        $mang1 = $listmenuchild[$dem1];

                        $arr1  = array(
                            'menu_order' => $j
                        );

                        $update1 = $model2->UpdateMenu($arr1,$mang1->id);
                    }
                }
            }
        }
    }

    public function index(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $model1 = new model_news();
        $model2 = new model_menu();
        $model3 = new model_page();
        
        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($create[0]['permission_detail'],'menu') || !strpos($delete[0]['permission_detail'],'menu') || !strpos($edit[0]['permission_detail'],'menu')){
                header('Location:'.URL.'admin/');
            }
        }

        if($model1->SelectAllCategoriesNews()){
            $this->view->listcagoriesnews = $model1->SelectAllCategoriesNews();
        }

        if($model1->SelectAllNews()){
            $this->view->listnews = $model1->SelectAllNews();
        }

        if($model2->SelectAllMenuFatherOn()){
            $listmenu = $model2->SelectAllMenuFatherOn();

            foreach ($listmenu as $key => $value) {
                if($model2->SelectAllChildeLv1ByFatherId($value['menu_id'])){
                    $listchild = $model2->SelectAllChildeLv1ByFatherId($value['menu_id']);
                    $listmenu[$key]['menuchild'] = $listchild;
                }
            }

            $this->view->listmenu = $listmenu;
        }

        if($model3->SelectAllPage()){
            $this->view->listpage = $model3->SelectAllPage();
        }

        $this->view->js = array(
            URL.'public/js/jquery.nestable.js',
            URL.'public/js/menuconfig.js',
            URL.'public/js/menu.js'
        );

        $this->view->layout('layout-admin1');
        $this->view->Render('menu/index');
        return true;
    }
}