
<?php
class controller_user extends lib_controller{

    public function __construct() {
        parent::__construct();
        if(lib_session::Logged() == true){

        }
    }

    public function deleteuser($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        if($_SESSION['user_type'] != 1){
            header('Location:'.URL.'admin');
        } 

        $model = new model_general();
        if($path){
            $result = $model->GetUserById($path);
            if($result){
                $del = $model->DeleteUser($path);
                if($del){
                    header('Location:'.URL.'user');
                }
            }
        }else{
            header('Location:'.URL.'user');
        }
    }

    public function edituser($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        if($_SESSION['user_type'] != 1){
            header('Location:'.URL.'admin');
        } 

        $model = new model_general();
        $model1 = new model_account();
        $user = new model_user();

        if($user->SelectAllGroup()){
            $this->view->listgroup = $user->SelectAllGroup();
        }

        if($path){
            $result = $model->GetUserById($path);
            if($result){
                $this->view->userinfo = $result;

                if($_POST){
                    if (empty($_POST['new_pass']) || empty($_POST['pass'])){
                        $data = Array(
                            'user_name'      =>   $_POST['user_name'],
                            'user_email'     =>   $_POST['user_email'],
                            'user_group_id'  =>   $_POST['user_type']
                        );
                        $update = $model->UpdateUser($data,$path);
                        if($update){
                            header('Location:'.URL.'user');
                        }
                    }else{
                        if(!empty($_POST['pass'])){
                            $salt = $result[0]['salt'];
                            $oldpass = md5($_POST['pass'].$salt);
                            if($result[0]['user_password'] == $oldpass){
                                $salt = md5(uniqid(mt_rand(), true));
                                $pass = $_POST['new_pass'].$salt;
                                $pass = md5($pass);
                                $data = array(
                                    'user_name'         =>  $_POST['user_name'],
                                    'user_email'        =>  $_POST['user_email'],
                                    'user_group_id'     =>  $_POST['user_type'],
                                    'user_password'     =>  $pass,
                                    'salt'              =>  $salt
                                );

                                $update = $model->UpdateUser($data,$path);
                                if($update){
                                    header('Location:'.URL.'user');
                                }
                            }else{
                                $this->view->error = '* Password cũ không đúng';
                            }
                        }
                    }
                }

                $this->view->js = array(
                    URL.'public/js/user.js'
                );

                $this->view->layout('layout-admin1');
                $this->view->Render('user/edituser');
                return true;
            }
        }else{
            header('Location:'.URL.'user');
        }
    }

    public function adduser(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        if($_SESSION['user_type'] != 1){
            header('Location:'.URL.'admin');
        } 

        $model1 = new model_registerr();

        $user = new model_user();

        if($user->SelectAllGroup()){
            $this->view->listgroup = $user->SelectAllGroup();
        }

        if($_POST){
            $username = $_POST['username'];
            $password = $_POST['password'];
            $repassword = $_POST['repassword'];
            $email = $_POST['email'];
            
                if(empty($_POST['username'])||empty($_POST['email'])||empty($_POST['password'])||empty($_POST['repassword'])){
                    $this->view->error = '* Bạn vui lòng xem các thông tin đã nhập.';
                }else{
                    if (!preg_match('/^[A-Za-z][A-Za-z0-9_\.]{2,31}$/', $_POST['username']) ){
                        $this->view->error = '* Username bạn nhập không được chứa ký tực đặc biệt.';
                    }else{
                        if($password == $repassword){
                            $salt = md5(uniqid(mt_rand(), true));
                            $pass = $_POST['password'].$salt;
                            $pass = md5($pass);
                            $data=array(
                                'user_email'    => strip_tags($email),
                                'user_name'     => strip_tags($username),
                                'user_group_id' => $_POST['user_type'],
                                'salt'          => $salt,
                                'pass'          => $pass
                            );

                            //print_r($data);

                            $result = $model1->Index($data);
                            if($result == 1){
                                header('Location: '.URL.'user');
                                exit;
                            }else if($result==-1){
                                $this->view->error = 'Email đã được đăng kí.';
                            }else if($result==-2){
                                $this->view->error = 'Username đã được đăng kí.';
                            }else if($result==-3){
                                $this->view->error = 'Đăng kí bị lỗi, vui lòng thử lại.';
                            }
                        }else{
                            $this->view->error = '* Hai mật khẩu không giống nhau.';
                        }
                    }
                }
        }

        $this->view->layout('layout-admin1');
        $this->view->Render('user/adduser');
        return true;
    }

    public function group(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        if($_SESSION['user_type'] != 1){
            header('Location:'.URL.'admin');
        } 

        $user = new model_user();

        if($user->SelectAllGroup()){
            $this->view->listgroup = $user->SelectAllGroup();
        }

        if($_POST){
            $group_name = $_POST['group_name'];

            $arr = array(
                'user_group_name'    =>     $group_name
            );

            $result = $user->InsertGroup($arr);

            if($result){
                header('Location:'.URL.'user/group/');
            }
        }

        $this->view->layout('layout-admin1');
        $this->view->Render('user/group');
        return true;
    }

    public function editgroup($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        if($_SESSION['user_type'] != 1){
            header('Location:'.URL.'admin');
        } 

        $user = new model_user();


        if($path){
            if($user->GetGroupById($path)){
                $groupinfo = $user->GetGroupById($path);

                $this->view->groupinfo = $groupinfo;

                if($_POST){
                    $group_name = $_POST['group_name'];

                    $arr = array(
                        'user_group_name'    =>     $group_name
                    );

                    $result = $user->UpdateGroup($arr,$path);

                    header('Location:'.URL.'user/group/');
                }

            }else{
                header('Location:'.URL.'user/group/');
            }
        }else{
            header('Location:'.URL.'user/group/');
        }

        $this->view->layout('layout-admin1');
        $this->view->Render('user/editgroup');
        return true;
    }

    public function deletegroup($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        if($_SESSION['user_type'] != 1){
            header('Location:'.URL.'admin');
        } 

        $user = new model_user();

        if($path){
            if($user->GetGroupById($path)){
                $deletegroup = $user->DeleteGroupById($path);

                if($deletegroup){
                    header('Location:'.URL.'user/group/');
                }
            }else{
                header('Location:'.URL.'user/group/');
            }
        }else{
            header('Location:'.URL.'user/group/');
        }
    }

    public function index(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        if($_SESSION['user_type'] != 1){
            header('Location:'.URL.'admin');
        } 

        $model = new model_general();

        $user = new model_user();
        

        $listuser = $model->GetUsers();

        foreach ($listuser as $key => $value) {
            if($user->GetGroupById($value['user_group_id'])){
                $usergroupinfo = $user->GetGroupById($value['user_group_id']);
                $listuser[$key]['user_type'] = $usergroupinfo[0]['user_group_name'];
            }
        }

        $this->view->listuser = $listuser;

        $this->view->layout('layout-admin1');
        $this->view->Render('user/index');
        return true;
    }

    public function admin(){
        $model = new model_general();

        $listuser = $model->GetUsers('admin');

        $this->view->listuser = $listuser;

        $this->view->title = 'Trang quản trị - Hạt cải';

        $this->view->js = array(
            URL.'public/js/user.js'
        );

        $this->view->layout('layout-admin');
        $this->view->Render('user/admin');
        return true;
    }
	
	public function permission(){        
		if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

		if($_SESSION['user_type'] != 1){
            header('Location:'.URL.'admin');
        }     
		
		$model = new model_general();     
        $user = new model_user();

		if($user->SelectAllGroup()){            
			$listadminnonedefault = $user->SelectAllGroup();            
			$listadmin = array();            
			foreach ($listadminnonedefault as $value) {                
				if(!$model->GetAllPermissionByUserId($value['user_group_id'])){
                    $listadmin[] = $value;                
				}            
			}            

			$this->view->listadmin = $listadmin;        
		}        

		if($user->SelectAllGroup()){             
			$listadminnonedefault = $user->SelectAllGroup();             
			foreach ($listadminnonedefault as $key => $value) {                
				if($model->GetUserById($value['user_group_id'])){                    
					$userinfo = $model->GetUserById($value['user_group_id']);
                    $listadminnonedefault[$key]['userinfo'] = $userinfo[0];
                }
                if($model->GetAllPermissionByUserId1($value['user_group_id'],'create')){
                    $create = $model->GetAllPermissionByUserId1($value['user_group_id'],'create');
                    $listadminnonedefault[$key]['create'] = $create[0];
                }
                if($model->GetAllPermissionByUserId1($value['user_group_id'],'delete')){ 
                   $delete = $model->GetAllPermissionByUserId1($value['user_group_id'],'delete');
                   $listadminnonedefault[$key]['delete'] = $delete[0];
                }                
				if($model->GetAllPermissionByUserId1($value['user_group_id'],'edit')){
                    $edit = $model->GetAllPermissionByUserId1($value['user_group_id'],'edit');
                    $listadminnonedefault[$key]['edit'] = $edit[0];                
				}            
			}            
			$this->view->listadmin1 = $listadminnonedefault;        
		}        

		if($_POST){            
			$user_id = $_POST['user_id'];
            $create = '';
            $edit = '';
            $delete = '';
			
            if($_POST['creatnew'] == 'on'){
                $create .= ',new';
            }
			if($_POST['creatgalleries'] == 'on'){
                $create .= ',galleries';
            }
			if($_POST['creatfile'] == 'on'){
                $create .= ',file';
            }
			if($_POST['creatvideo'] == 'on'){
                $create .= ',video';
            }
			if($_POST['creatslider'] == 'on'){
                $create .= ',slider';
            }
			if($_POST['creatpage'] == 'on'){
                $create .= ',page';
            }
            if($_POST['creatmenu'] == 'on'){
                $create .= ',menu';
            }
            if($_POST['creatcontact'] == 'on'){
                $create .= ',contact';
            }
            if($_POST['createvent'] == 'on'){
                $create .= ',event';
            }
            if($_POST['creatartist'] == 'on'){
                $create .= ',artist';
            }
            if($_POST['creatpartner'] == 'on'){
                $create .= ',partner';
            }

            $arr1 = array(
                'user_group_id'     =>  $user_id,
                'permission_name'   =>  'create',
                'permission_detail' =>  $create
            );

            $createinsert = $model->InsertPermission($arr1);
			
			if($_POST['deletenew'] == 'on'){
                $delete .= ',new';
            }
            if($_POST['deletegalleries'] == 'on'){
                $delete .= ',galleries';
            }
            if($_POST['deletefile'] == 'on'){
                $delete .= ',file';
            }
            if($_POST['deletevideo'] == 'on'){
                $delete .= ',video';
            }
            if($_POST['deleteslider'] == 'on'){
                $delete .= ',slider';
            }
            if($_POST['deletepage'] == 'on'){
                $delete .= ',page';
            }
            if($_POST['deletemenu'] == 'on'){
                $delete .= ',menu';
            }
            if($_POST['deletecontact'] == 'on'){
                $delete .= ',contact';
            }
            if($_POST['deleteevent'] == 'on'){
                $delete .= ',event';
            }
            if($_POST['deleteartist'] == 'on'){
                $delete .= ',artist';
            }
            if($_POST['deletepartner'] == 'on'){
                $delete .= ',partner';
            }

			$arr2 = array(
                'user_group_id'     =>  $user_id,
                'permission_name'   =>  'delete',
                'permission_detail' =>  $delete            
			);            

			$deleteinsert = $model->InsertPermission($arr2);
			
            if($_POST['editnew'] == 'on'){
                $edit .= ',new';
            }
            if($_POST['editgalleries'] == 'on'){
                $edit .= ',galleries';
            }
            if($_POST['editfile'] == 'on'){
                $edit .= ',file';
            }
            if($_POST['editvideo'] == 'on'){
                $edit .= ',video';
            }
            if($_POST['editslider'] == 'on'){
                $edit .= ',slider';
            }
            if($_POST['editpage'] == 'on'){
                $edit .= ',page';
            }
            if($_POST['editmenu'] == 'on'){
                $edit .= ',menu';
            }
            if($_POST['editcontact'] == 'on'){
                $edit .= ',contact';
            } 
            if($_POST['editevent'] == 'on'){
                $edit .= ',event';
            }
            if($_POST['editartist'] == 'on'){
                $edit .= ',artist';
            }
            if($_POST['editpartner'] == 'on'){
                $edit .= ',partner';
            }

            $arr3 = array(
                'user_group_id'     =>  $user_id,
                'permission_name'   =>  'edit',
                'permission_detail' =>  $edit
            );

            $editinsert = $model->InsertPermission($arr3);

			header('Location:'.URL.'user/permission/');
        }        

		$this->view->js = array(
            URL.'public/js/user.js'
        );
        $this->view->title = 'Trang quản trị';
        $this->view->layout('layout-admin1');
        $this->view->Render('user/permission');
        return true;
    }    

	public function editpermission($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        if($_SESSION['user_type'] != 1){
            header('Location:'.URL.'admin');
        } 

        $model = new model_general();
        $user = new model_user();

        if($path){
            if($user->GetGroupById($path)){
                $permissioninfo = $user->GetGroupById($path);
                foreach ($permissioninfo as $key => $value) {
                    if($model->GetAllPermissionByUserId1($value['user_group_id'],'create')){ 
						$create = $model->GetAllPermissionByUserId1($value['user_group_id'],'create');
						$permissioninfo[$key]['create'] = $create[0];
                    }
                    if($model->GetAllPermissionByUserId1($value['user_group_id'],'delete')){
                        $delete = $model->GetAllPermissionByUserId1($value['user_group_id'],'delete');
                        $permissioninfo[$key]['delete'] = $delete[0];
                    }
                    if($model->GetAllPermissionByUserId1($value['user_group_id'],'edit')){
                        $edit = $model->GetAllPermissionByUserId1($value['user_group_id'],'edit');
                        $permissioninfo[$key]['edit'] = $edit[0];
                    }
                }
                $this->view->permissioninfo = $permissioninfo;
            }else{
                header('Location:'.URL.'user/permission/');
            }        
		}else{
            header('Location:'.URL.'user/permission/');
        }
        if($user->SelectAllGroup()){
             $listadminnonedefault = $user->SelectAllGroup();
             foreach ($listadminnonedefault as $key => $value) {
                if($model->GetUserById($value['user_group_id'])){
                    $userinfo = $model->GetUserById($value['user_group_id']); 
					$listadminnonedefault[$key]['userinfo'] = $userinfo[0]; 
                }
                if($model->GetAllPermissionByUserId1($value['user_group_id'],'create')){
                    $create = $model->GetAllPermissionByUserId1($value['user_group_id'],'create');
                    $listadminnonedefault[$key]['create'] = $create[0];
                }
                if($model->GetAllPermissionByUserId1($value['user_group_id'],'delete')){
                    $delete = $model->GetAllPermissionByUserId1($value['user_group_id'],'delete');
                    $listadminnonedefault[$key]['delete'] = $delete[0];
                }
                if($model->GetAllPermissionByUserId1($value['user_group_id'],'edit')){
                    $edit = $model->GetAllPermissionByUserId1($value['user_group_id'],'edit');
                    $listadminnonedefault[$key]['edit'] = $edit[0];
                }
            }
            $this->view->listadmin1 = $listadminnonedefault;
        }

        if($_POST){
            $user_id = $_POST['user_id'];
            $editinfo = $model->GetAllPermissionByUserId1($user_id,'edit');
            $createinfo = $model->GetAllPermissionByUserId1($user_id,'create');
            $deleteinfo = $model->GetAllPermissionByUserId1($user_id,'delete');
            $create = '';
            $edit = '';
            $delete = '';

			if($_POST['creatnew'] == 'on'){
                $create .= ',new';
            }
            if($_POST['creatgalleries'] == 'on'){
                $create .= ',galleries';
            }
            if($_POST['creatfile'] == 'on'){
                $create .= ',file';
            }
            if($_POST['creatvideo'] == 'on'){
                $create .= ',video';
            }
            if($_POST['creatslider'] == 'on'){
                $create .= ',slider';
            }
            if($_POST['creatpage'] == 'on'){
                $create .= ',page';
            }
            if($_POST['creatmenu'] == 'on'){
                $create .= ',menu';
            }
            if($_POST['creatcontact'] == 'on'){
                $create .= ',contact';
            }
            if($_POST['createvent'] == 'on'){
                $create .= ',event';
            }
            if($_POST['creatartist'] == 'on'){
                $create .= ',artist';
            }
            if($_POST['creatpartner'] == 'on'){
                $create .= ',partner';
            }

            $arr1 = array(
                'permission_detail' =>  $create
            );

            $createinsert = $model->UpdatePermission($arr1,$createinfo[0]['user_permission_id']);

            if($_POST['deletenew'] == 'on'){
                $delete .= ',new';
            }
            if($_POST['deletegalleries'] == 'on'){
                $delete .= ',galleries';
            }
            if($_POST['deletefile'] == 'on'){
                $delete .= ',file';
            }
            if($_POST['deletevideo'] == 'on'){
                $delete .= ',video';
            }
            if($_POST['deleteslider'] == 'on'){
                $delete .= ',slider';
            }
            if($_POST['deletepage'] == 'on'){
                $delete .= ',page';
            }
            if($_POST['deletemenu'] == 'on'){
                $delete .= ',menu';
            }
            if($_POST['deletecontact'] == 'on'){
                $delete .= ',contact';
            }
            if($_POST['deleteevent'] == 'on'){
                $delete .= ',event';
            }
            if($_POST['deleteartist'] == 'on'){
                $delete .= ',artist';
            }
            if($_POST['deletepartner'] == 'on'){
                $delete .= ',partner';
            }

            $arr2 = array(
                'permission_detail' =>  $delete
            );

            $deleteinsert = $model->UpdatePermission($arr2,$deleteinfo[0]['user_permission_id']);

            if($_POST['editnew'] == 'on'){
                $edit .= ',new';
            }
            if($_POST['editgalleries'] == 'on'){
                $edit .= ',galleries';
            }
            if($_POST['editfile'] == 'on'){
                $edit .= ',file';
            }
            if($_POST['editvideo'] == 'on'){
                $edit .= ',video';
            }
            if($_POST['editslider'] == 'on'){
                $edit .= ',slider';
            }
            if($_POST['editpage'] == 'on'){
                $edit .= ',page';
            }
            if($_POST['editmenu'] == 'on'){
                $edit .= ',menu';
            }
            if($_POST['editcontact'] == 'on'){
                $edit .= ',contact';
            } 
            if($_POST['editevent'] == 'on'){
                $edit .= ',event';
            }
            if($_POST['editartist'] == 'on'){
                $edit .= ',artist';
            }
            if($_POST['editpartner'] == 'on'){
                $edit .= ',partner';
            }

            $arr3 = array(
                'permission_detail' =>  $edit
            );

            $editinsert = $model->UpdatePermission($arr3,$editinfo[0]['user_permission_id']);
            header('Location:'.URL.'user/permission/');
        }

        $this->view->js = array(
            URL.'public/js/user.js'
        );

        $this->view->title = 'Trang quản trị';
        $this->view->layout('layout-admin1');
        $this->view->Render('user/editpermission');
        return true;
    }
	
	public function permissionnews(){
		if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }
        if($_SESSION['user_id'] != 1){
            header('Location:'.URL.'admin');
        } 

		$model = new model_general();
		
		$model1 = new model_news();

        $user = new model_user();
		
		if($user->SelectAllGroup()){
             $listadminnonedefault = $user->SelectAllGroup();
             foreach ($listadminnonedefault as $key => $value) {
                if($model->GetUserById($value['user_group_id'])){
                    $userinfo = $model->GetUserById($value['user_group_id']); 
                    $listadminnonedefault[$key]['userinfo'] = $userinfo[0]; 
                }
                if($model->GetAllPermissionByUserId1($value['user_group_id'],'create')){
                    $create = $model->GetAllPermissionByUserId1($value['user_group_id'],'create');
                    $listadminnonedefault[$key]['create'] = $create[0];
                }
                if($model->GetAllPermissionByUserId1($value['user_group_id'],'delete')){
                    $delete = $model->GetAllPermissionByUserId1($value['user_group_id'],'delete');
                    $listadminnonedefault[$key]['delete'] = $delete[0];
                }
                if($model->GetAllPermissionByUserId1($value['user_group_id'],'edit')){
                    $edit = $model->GetAllPermissionByUserId1($value['user_group_id'],'edit');
                    $listadminnonedefault[$key]['edit'] = $edit[0];
                }
            }
            $this->view->listadmin1 = $listadminnonedefault;
        }

		if($model1->SelectAllCategoriesNews()){
            $this->view->listcategories = $model1->SelectAllCategoriesNews();
        }
		
		if($model->SelectAllPermissionNews()){
			$listpermissionews = $model->SelectAllPermissionNews();
			
			foreach($listpermissionews as $key=>$value){
				$permissionnewsid = $value['permission_news_id'];
				if($model->GetUserById($value['user_id'])){
					$userinfo = $model->GetUserById($value['user_id']);
					$listpermissionews[$key]['userinfo'] = $userinfo[0];
				}
				if($model1->GetCategoriesNewsById($value['categories_id'])){
					$categoryinfo = $model1->GetCategoriesNewsById($value['categories_id']);
					$listpermissionews[$key]['categoryinfo'] = $categoryinfo[0];
				}
			}
			
			$this->view->listpermissionews = $listpermissionews;
		}
		
		if($_POST){
			$user_id 		= $_POST['user_id'];
			$categories_id	= $_POST['categories_id'];
			
			$arr = array(
				'user_group_id'			=> $user_id,
				'categories_id'		=> $categories_id
			);
			
			$result = $model->InsertPermissionNews($arr);
			
			if($result){
				header('Location:'.URL.$this->view->lang.'/user/permissionnews/');
			}
		}
		
		$this->view->js = array(
            URL.'public/js/user.js'
        );
		
		$this->view->title = 'Trang quản trị';
        $this->view->layout('layout-admin1');
        $this->view->Render('user/permissionnews');
        return true;
	}
	
	public function deletepermissionnews($path){
		if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }
        if($_SESSION['user_id'] != 1){
            header('Location:'.URL.'admin');
        } 
		
		$model = new model_general();
		
		if($path){
			if($model->GetPermissionNewsById($path)){
				$delete = $model->DeletePermissionNews($path);
				
				if($delete){
					header('Location:'.URL.$this->view->lang.'/user/permissionnews/');
				}
			}
		}else{
			header('Location:'.URL.$this->view->lang.'/user/permissionnews/');
		}
	}
	
	public function editpermissionnews($path){
		if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        if($_SESSION['user_id'] != 1){
            header('Location:'.URL.'admin');
        } 

		$model = new model_general();
		
		$model1 = new model_news();

        $user = new model_user();
		
        if($user->SelectAllGroup()){
             $listadminnonedefault = $user->SelectAllGroup();
             foreach ($listadminnonedefault as $key => $value) {
                if($model->GetUserById($value['user_group_id'])){
                    $userinfo = $model->GetUserById($value['user_group_id']); 
                    $listadminnonedefault[$key]['userinfo'] = $userinfo[0]; 
                }
                if($model->GetAllPermissionByUserId1($value['user_group_id'],'create')){
                    $create = $model->GetAllPermissionByUserId1($value['user_group_id'],'create');
                    $listadminnonedefault[$key]['create'] = $create[0];
                }
                if($model->GetAllPermissionByUserId1($value['user_group_id'],'delete')){
                    $delete = $model->GetAllPermissionByUserId1($value['user_group_id'],'delete');
                    $listadminnonedefault[$key]['delete'] = $delete[0];
                }
                if($model->GetAllPermissionByUserId1($value['user_group_id'],'edit')){
                    $edit = $model->GetAllPermissionByUserId1($value['user_group_id'],'edit');
                    $listadminnonedefault[$key]['edit'] = $edit[0];
                }
            }
            $this->view->listadmin1 = $listadminnonedefault;
        }


		if($model1->SelectAllCategoriesNews()){
            $this->view->listcategories = $model1->SelectAllCategoriesNews();
        }
		
		if($path){
			if($model->GetPermissionNewsById($path)){
				
				$permissionnewsinfo = $model->GetPermissionNewsById($path);
				
				$permissionnewsid = $permissionnewsinfo[0]['permission_news_id'];
				
				if($model->GetUserById($permissionnewsinfo[0]['user_id'])){
					$userinfo = $model->GetUserById($permissionnewsinfo[0]['user_id']);
					$this->view->userinfo = $userinfo;
				}
				
				$this->view->permissioninfo = $permissionnewsinfo;
				
				if($_POST){
					$user_id 		= $_POST['user_id'];
					$categories_id	= $_POST['categories_id'];
					
					$arr = array(
						'user_group_id'			=> $user_id,
						'categories_id'		    => $categories_id
					);
					
					$result = $model->UpdatePermissionNews($arr,$permissionnewsid);
					
					if($result){
						header('Location:'.URL.$this->view->lang.'/user/permissionnews/');
					}
				}
			}else{
				header('Location:'.URL.$this->view->lang.'/user/permissionnews/');
			}
		}else{
			header('Location:'.URL.$this->view->lang.'/user/permissionnews/');
		}
		
		$this->view->js = array(
            URL.'public/js/user.js'
        );
		
		$this->view->title = 'Trang quản trị';
        $this->view->layout('layout-admin1');
        $this->view->Render('user/editpermissionnews');
        return true;
	}
}