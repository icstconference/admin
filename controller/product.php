<?php
class controller_product extends lib_controller{

    public function __construct() {
        parent::__construct();
        if(lib_session::Logged() == true){

        }
    }

    /*
     * Các function tạo hình ảnh
     */

    function UploadImage(array $data) {
        if ((($data["type"] == "image/gif") || ($data["type"] == "image/jpeg")
                || ($data["type"] == "image/jpg") || ($data["type"] == "image/pjpeg")
                || ($data["type"] == "image/x-png") || ($data["type"] == "image/png"))
            && ($data["size"] < 4000000)) {
            if ($data["error"] == 0) {
                $path = "public/upload/images/" . uniqid() . '.jpg';
                move_uploaded_file($data["tmp_name"], $path);
                return $path;
            }
        }
        return false;
    }

    function SaveThumbImage($path) {
        try {
            $thumb = PhpThumbFactory::create($path);
        } catch (Exception $e) {
            // handle error here however you'd like
        }
        $id=  uniqid();
        $name_file = $id.'.jpg';
        $thumb->resize(400);
        $thumb->adaptiveResize(300, 300);
        $thumb->save('public/upload/images/product/big/' . $name_file, 'jpg');

        $thumb->resize(60);
        $thumb->save('public/upload/images/product/small/' . $name_file, 'jpg');
        $path_result = array(
            'image' => $path,
            'thumb' => 'public/upload/images/product/small/' . $name_file,
            'big' => 'public/upload/images/product/big/' . $name_file
        );
        return $path_result;
    }

    protected function DeleteFile($file_path) {
        $path = str_replace(URL, '', $file_path);
        if (file_exists($path)) {
            unlink($path);
            return true;
        }
        return false;
    }

    /*
     * Tạo 1 product
     */

    public function createproduct(){
        require_once 'plugin/phpthumb/ThumbLib.inc.php';
        require_once 'plugin/url-seo/url-seo.php';
        $model = new model_product();
        if($_POST){
            $product_name = $_POST['product_name'];
            $product_type = $_POST['product_type'];
            $product_quantity = $_POST['product_quantity'];
            $product_newprice = $_POST['product_newprice'];
            $product_oldprice = $_POST['product_oldprice'];
            $product_description = $_POST['product_description'];
            $arr_image = array();

            if(strlen($_FILES['product_img']['name'])>1){
                $upload_img = $this->UploadImage($_FILES['product_img']);
                $path_save_img = $this->SaveThumbImage($upload_img);

                if($path_save_img){
                    $product_img_thumb = URL.$path_save_img['big'];
                    $product_img = URL.$path_save_img['image'];
                }else{
                    $product_img_thumb = '';
                    $product_img = '';
                }
            }else{
                $product_img_thumb = '';
                $product_img = '';
            }

            if(strlen($_FILES['product_img_behind']['name'])>1){
                $upload_img = $this->UploadImage($_FILES['product_img_behind']);
                $path_save_img = $this->SaveThumbImage($upload_img);

                if($path_save_img){
                    $product_img_thumb_behind = URL.$path_save_img['big'];
                    $product_img_behind = URL.$path_save_img['image'];
                }else{
                    $product_img_thumb_behind = '';
                    $product_img_behind = '';
                }
            }else{
                $product_img_thumb_behind = '';
                $product_img_behind = '';
            }

            if(strlen($_FILES['product_img1']['name'])>1){
                $path_save_img = $this->UploadImage($_FILES['product_img1']);

                if($path_save_img){
                    $img = array(
                        'images_url'   => URL.$path_save_img
                    );

                    $arr_image[] = $img;
                }
            }

            if(strlen($_FILES['product_img2']['name'])>1){
                $path_save_img = $this->UploadImage($_FILES['product_img2']);

                if($path_save_img){
                    $img = array(
                        'images_url'   => URL.$path_save_img
                    );

                    $arr_image[] = $img;
                }
            }

            if(strlen($_FILES['product_img3']['name'])>1){
                $path_save_img = $this->UploadImage($_FILES['product_img3']);

                if($path_save_img){
                    $img = array(
                        'images_url'   => URL.$path_save_img
                    );

                    $arr_image[] = $img;
                }
            }

            if(strlen($_FILES['product_img4']['name'])>1){
                $path_save_img = $this->UploadImage($_FILES['product_img4']);

                if($path_save_img){
                    $img = array(
                        'images_url'   => URL.$path_save_img
                    );

                    $arr_image[] = $img;
                }
            }

            $data = array(
                'product_name'                  => $product_name,
                'product_description'           => $product_description,
                'product_price_new'             => $product_newprice,
                'product_price_old'             => $product_oldprice,
                'product_quantity'              => $product_quantity,
                'product_image_thumb'           => $product_img_thumb,
                'product_image'                 => $product_img,
                'product_image_thumb_behind'    => $product_img_thumb_behind,
                'product_image_behind'          => $product_img_behind,
                'product_url'                   => '',
                'categories_id'                 => $product_type
            );

            $result = $model->InsertProduct($data);
            if($result){
                $date = date('dmY');
                $uniqueid = uniqid();
                $product_url = post_slug($product_name).'-product-'.$result;
                $data1 = array(
                    'product_url' => $product_url
                );
                for($i=0;$i<count($arr_image);$i++){
                    $arr_img = array(
                        'images_url'           => $arr_image[$i]['images_url'],
                        'images_product_id'    => $result,
                    );

                    $model->InsertImageProduct($arr_img);
                }
                $update = $model->UpdateProduct($data1,$result);
                if($update){
                    echo 1;
                }
            }
        }
    }

    /*
     * Cập nhật 1 product
     */

    public function updateproduct(){
        require_once 'plugin/phpthumb/ThumbLib.inc.php';
        require_once 'plugin/url-seo/url-seo.php';
        $model = new model_product();
        if($_POST){
            $product_id = $_POST['product_id'];
            $result = $model->GetProductById($product_id);
            $product_name = $_POST['product_name'];
            $product_kind = $_POST['product_kind'];
            $product_type = $_POST['product_type'];
            $product_label = $_POST['product_label'];
            $product_quantity = $_POST['product_quantity'];
            $product_newprice = $_POST['product_newprice'];
            $product_oldprice = $_POST['product_oldprice'];
            $product_description = $_POST['product_description'];
            $urlnews          = explode('-',$result[0]['product_url']);
            $numarrnews       = count($urlnews);
            $urlcode          = $urlnews[$numarrnews-2];
            $product_url      = post_slug($product_name).'-product-'.$result;
            $arr_image = array();

            if(strlen($_FILES['product_img']['name'])>1){
                $upload_img = $this->UploadImage($_FILES['product_img']);
                $path_save_img = $this->SaveThumbImage($upload_img);

                if($path_save_img){
                    $product_img_thumb = URL.$path_save_img['big'];
                    $product_img = URL.$path_save_img['image'];
                    $this->DeleteFile($result[0]['product_image_thumb']);
                    $this->DeleteFile($result[0]['product_image']);
                }else{
                    $product_img_thumb = $result[0]['product_image_thumb'];
                    $product_img = $result[0]['product_image'];
                }
            }else{
                $product_img_thumb = $result[0]['product_image_thumb'];
                $product_img = $result[0]['product_image'];
            }

            if(strlen($_FILES['product_img_behind']['name'])>1){
                $upload_img = $this->UploadImage($_FILES['product_img_behind']);
                $path_save_img = $this->SaveThumbImage($upload_img);

                if($path_save_img){
                    $product_img_thumb_behind = URL.$path_save_img['big'];
                    $product_img_behind = URL.$path_save_img['image'];
                    $this->DeleteFile($result[0]['product_image_thumb_behind']);
                    $this->DeleteFile($result[0]['product_image_behind']);
                }else{
                    $product_img_thumb_behind = $result[0]['product_image_thumb_behind'];
                    $product_img_behind = $result[0]['product_image_behind'];
                }
            }else{
                $product_img_thumb_behind = $result[0]['product_image_thumb_behind'];
                $product_img_behind = $result[0]['product_image_behind'];
            }

            if(strlen($_FILES['product_img1']['name'])>1){
                $path_save_img = $this->UploadImage($_FILES['product_img1']);

                if($path_save_img){
                    $img = array(
                        'images_url'   => URL.$path_save_img
                    );

                    $arr_image[] = $img;
                }
            }

            if(strlen($_FILES['product_img2']['name'])>1){
                $path_save_img = $this->UploadImage($_FILES['product_img2']);

                if($path_save_img){
                    $img = array(
                        'images_url'   => URL.$path_save_img
                    );

                    $arr_image[] = $img;
                }
            }

            if(strlen($_FILES['product_img3']['name'])>1){
                $path_save_img = $this->UploadImage($_FILES['product_img3']);

                if($path_save_img){
                    $img = array(
                        'images_url'   => URL.$path_save_img
                    );

                    $arr_image[] = $img;
                }
            }

            if(strlen($_FILES['product_img4']['name'])>1){
                $path_save_img = $this->UploadImage($_FILES['product_img4']);

                if($path_save_img){
                    $img = array(
                        'images_url'   => URL.$path_save_img
                    );

                    $arr_image[] = $img;
                }
            }

            $data = array(
                'product_name'                  => $product_name,
                'product_type'                  => $product_kind,
                'product_description'           => $product_description,
                'product_price_new'             => $product_newprice,
                'product_price_old'             => $product_oldprice,
                'product_quantity'              => $product_quantity,
                'product_image_thumb'           => $product_img_thumb,
                'product_image'                 => $product_img,
                'product_image_thumb_behind'    => $product_img_thumb_behind,
                'product_image_behind'          => $product_img_behind,
                'categories_id'                 => $product_type,
                'label_id'                      => $product_label
            );

            $update = $model->UpdateProduct($data,$product_id);
            for($i=0;$i<count($arr_image);$i++){
                $arr_img = array(
                    'images_url'           => $arr_image[$i]['images_url'],
                    'images_product_id'    => $product_id,
                );

                $model->InsertImageProduct($arr_img);
            }
            
            echo '1';
        }
    }

    /*
     * Các function product
     */

    public function deleteproduct($path){
        $model = new model_product();
        if($path){
            $result = $model->GetProductById($path);
            if($result){
                $delete = $model->DeleteProduct($path);
                if($delete){
                    header('Location:'.URL.'product');
                }
            }else{
                header('Location:'.URL.'product');
            }
        }else{
            header('Location:'.URL.'product');
        }
    }

    public function editproduct($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }
        $model = new model_product();
        if($path){
            $result = $model->GetProductById($path);
            if($result){
                $this->view->productinfo = $result;

                if($model->GetAllLabelProduct()){
                    $this->view->listlabel = $model->GetAllLabelProduct();
                }

                if($model->GetAllCategoriesProduct()){
                    $this->view->listcategories = $model->GetAllCategoriesProduct();
                }

                if($model->GetImagesProduct($path)){
                    $this->view->listimgproduct = $model->GetImagesProduct($path);
                }

                $this->view->title = 'Trang quản trị';

                $this->view->js = array(
                    URL.'public/ckeditor/ckeditor.js',
                    URL.'public/js/product.js'
                );

                $this->view->layout('layout-admin1');
                $this->view->Render('product/editproduct');
                return true;
            }else{
                header('Location:'.URL.'product');
            }
        }else{
            header('Location:'.URL.'product');
        }
    }

    public function adddproduct(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }
        $model = new model_product();

        if($model->GetAllLabelProduct()){
            $this->view->listlabel = $model->GetAllLabelProduct();
        }

        if($model->GetAllProduct()){
            $this->view->listproduction = $model->GetAllProduct();
        }

        if($model->GetAllCategoriesProduct()){
            $this->view->listcategories = $model->GetAllCategoriesProduct();
        }

        $this->view->title = 'Trang quản trị';

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/product.js'
        );

        $this->view->layout('layout-admin1');
        $this->view->Render('product/addproduct');
        return true;
    }

    public function index(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }
        $model = new model_product();

        if($model->GetAllLabelProduct()){
            $this->view->listlabel = $model->GetAllLabelProduct();
        }

        if($model->GetAllProduct()){
            $this->view->listproduction = $model->GetAllProduct();
        }

        if($model->GetAllCategoriesProduct()){
            $this->view->listcategories = $model->GetAllCategoriesProduct();
        }

        $model1 = new model_setting();

        $allsetting = $model1->GetAllSetting();

        $this->view->allsetting = $allsetting;

        $this->view->title = 'Trang quản trị';

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/product.js'
        );

        $this->view->layout('layout-admin1');
        $this->view->Render('product/index');
        return true;
    }

    /*
     * Các function label của product
     */

    function editlabel($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $model = new model_product();

        if($path){
            $result = $model->GetLabelProductById($path);
            if($result){
                $this->view->labelinfo = $result;

                if($_POST){
                    $product_label = $_POST['product_label'];
                    $product_labelcolor = $_POST['product_labelcolor'];

                    $data = array(
                        'label_name' => $product_label,
                        'label_color' => $product_labelcolor
                    );

                    $result = $model->UpdateLabelProduct($data,$path);
                    if($result){
                        header('Location:'.URL.'product/label/');
                    }
                }

                $this->view->title = 'Trang quản trị';

                $this->view->css = array(
                    URL.'public/css/colorpicker.css'
                );

                $this->view->js = array(
                    URL.'public/js/colorpicker/colorpicker.js',
                    URL.'public/js/label.js'
                );

                $this->view->layout('layout-admin11');
                $this->view->Render('product/editlabel');
                return true;
            }else{
                header('Location:'.URL.'product/label/');
            }
        }else{
            header('Location:'.URL.'product/label/');
        }
    }

    function deletelabel($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $model = new model_product();

        if($path){
            $result = $model->GetLabelProductById($path);
            if($result){
                $this->view->labelinfo = $result;

                $delete = $model->DeleteLabelProduct($path);

                if($delete){
                    header('Location:'.URL.'product/label/');
                }
            }else{
                header('Location:'.URL.'product/label/');
            }
        }else{
            header('Location:'.URL.'product/label/');
        }
    }

    function label(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $model = new model_product();

        if($model->GetAllLabelProduct()){
            $this->view->listlabel = $model->GetAllLabelProduct();
        }

        if($_POST){
            $product_label = $_POST['product_label'];
            $product_labelcolor = $_POST['product_labelcolor'];

            $data = array(
                'label_name' => $product_label,
                'label_color' => $product_labelcolor
            );

            $result = $model->InsertLabelProduct($data);
            if($result){
                header('Location:'.URL.'product/label/');
            }
        }

        $this->view->css = array(
            URL.'public/css/colorpicker.css'
        );

        $this->view->js = array(
            URL.'public/js/colorpicker/colorpicker.js',
            URL.'public/js/label.js'
        );

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin1');
        $this->view->Render('product/label');
        return true;
    }

    /*
     * Các function categories của product
     */

    function editcategories($path){
        require_once 'plugin/url-seo/url-seo.php';
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $model = new model_product();

        if($model->GetCategoriesProductById($path)){
            $result = $model->GetCategoriesProductById($path);

            $this->view->categoriesinfo = $result;

            if($model->GetAllCategoriesFather($path)){
                $listcategories = $model->GetAllCategoriesFather($path);

                $this->view->listcategories = $listcategories;
            }

            if($_POST){

                $urlnews          = explode('-',$result[0]['categories_url']);
                $numarrnews       = count($urlnews);
                $urlcode          = $urlnews[$numarrnews-2];
                $product_url      = post_slug($_POST['product_type']).'-productcategory-'.$result;

                $data = array(
                    'categories_name'   => $_POST['product_type'],
                    'categories_father' => $_POST['product_fathertype'],
                    'categories_url'    => $product_url
                );

                $update = $model->UpdateCategoriesProduct($data,$path);
                if($update){
                    header('Location:'.URL.'product/categories/');
                }
            }

            $this->view->js = array(
                URL.'public/js/user.js'
            );

            $this->view->title = 'Trang quản trị';

            $this->view->layout('layout-admin1');
            $this->view->Render('product/editcategories');
        }else{
            header('Location:'.URL.'product/categories/');
        }
        return true;
    }

    function deletecategories($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $model = new model_product();

        if($model->GetCategoriesProductById($path)){
            $result = $model->GetCategoriesProductById($path);

            $this->view->categoriesinfo = $result;

            $delete = $model->DeleteCategoriesProduct($path);

            if($delete){
                header('Location:'.URL.'product/categories/');
            }
        }else{
            header('Location:'.URL.'product/categories/');
        }
        return true;
    }

    function categories(){
        require_once 'plugin/url-seo/url-seo.php';
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $model = new model_product();

        if($model->GetAllCategoriesProduct()){
            $listcategories = $model->GetAllCategoriesProduct();

            $this->view->listcategories = $listcategories;
        }

        $model1 = new model_setting();

        $allsetting = $model1->GetAllSetting();

        $this->view->allsetting = $allsetting;

        if($_POST){
            $product_type = $_POST['product_type'];
            $product_fathertype = $_POST['product_fathertype'];

            if($product_fathertype){
                $product_father = $product_fathertype;
            }else{
                $product_father = 0;
            }

            $data = array(
                'categories_name' => $product_type,
                'categories_father' => $product_father,
                'categories_url'    => ''
            );

            $result = $model->InsertCategoriesProduct($data);
            if($result){
                $date = date('dmY');
                $uniqueid = uniqid();
                $product_url = post_slug($product_type).'-productcategory-'.$result;
                $data1 = array(
                    'categories_url' => $product_url
                );
                $update = $model->UpdateCategoriesProduct($data1,$result);
                if($update){
                    header('Location:'.URL.'product/categories/');
                }
            }
        }

        $this->view->js = array(
            URL.'public/js/user.js'
        );

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin1');
        $this->view->Render('product/categories');
        return true;
    }

    function detail($path){
        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        if($allsetting){
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
            $this->view->configurl = $allsetting[0]['config_url'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOnByPostion('all','ngang')){
            $allmenufather = $model1->SelectAllMenuFatherOnByPostion('all','ngang');

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        $model2 = new model_slider();

        if($model2->SelectAllSliderStatus()){

            $allslider = $model2->SelectAllSliderStatus();

            $this->view->allslider = $allslider;

        }

        $model3 = new model_product();

        if($model->GetAllContact()){

            $listcontact = $model->GetAllContact();

            $this->view->listcontact = $listcontact;

        }

        if($model3->GetAllCategoriesProduct()){
            $this->view->listcategoriesproduct = $model3->GetAllCategoriesProduct();
        }

        $model4 = new model_news();

        if($model4->SelectAllCategoriesNews()){
            $this->view->listcategoriesnews = $model4->SelectAllCategoriesNews();
        }

        if($model4->SelectTopNews(5)){
            $this->view->topnew = $model4->SelectTopNews(5);
        }

        if($path){
            $part = explode('-',$path);
            $id = $part[count($part)-1];
            $result = $model3->GetProductById($id);

            if($result){
                $this->view->productinfo = $result;

                $catproduct = $model3->GetCategoriesProductById($result[0]['categories_id']);
                $labelinfo = $model3->GetLabelProductById($id);

                $this->view->title = $result[0]['product_name'].'-'.$allsetting[0]['config_title'];
                $this->view->description = $result[0]['product_name'].'-'.$allsetting[0]['config_description'];
                $this->view->keyword = $result[0]['product_name'].','.$allsetting[0]['config_keyword'];

                $this->view->labelinfo = $labelinfo;

                $this->view->breadcrumb = '
                    <li>
                        <a class="home" href="'.URL.'">Trang chủ</a>
                    </li>
                    <li>
                        <a href="'.URL.'category/product/'.$catproduct[0]['categories_url'].'">'.$catproduct[0]['categories_name'].'</a>
                    </li>
                    <li>'.
                        $result[0]['product_name'].
                    '</li>
                ';

                if($catproduct){
                    $this->view->categories = $catproduct;
                }

                if($model3->GetAllImagesProductByImageId($id)){
                    $this->view->listimgproduct = $model3->GetAllImagesProductByImageId($id);
                }

                if($model3->RelatedProduct(4,$result[0]['categories_id'],$id)){
                    $listrelatedproduct = $model3->RelatedProduct(4,$result[0]['categories_id'],$id);
                    foreach ($listrelatedproduct as $key=>$value) {
                        if($model3->GetLabelProductById($value['product_id'])){
                            $label = $model3->GetLabelProductById($value['product_id']);
                            $listrelatedproduct[$key]['labelinfo'] = $label[0]['label_name'];
                        }
                    }
                    $this->view->listrelatedproduct = $listrelatedproduct;
                }
            }
        }else{
            header('Location:'.URL);
        }

		$this->view->css = array(
            URL.'public/css/colorbox.css'
        );
		
        $this->view->js = array(
            URL.'public/js/jquery.colorbox.js',
            URL.'public/js/detail.js',
            URL.'public/js/add-cart.js'
        );

        $this->view->layout('layout');
        $this->view->Render('product/detail');
        return true;
    }

    function deleteimage(){
        $model = new model_product();

        if($_POST){
            $imgid = $_POST['idimage'];
            $images_url = $_POST['urlimage'];
            $result = $model->DeleteImagesProduct($imgid);
            if($result){
                $this->DeleteFile($images_url);
            }
        }
    }

    function changeimage(){
        $model = new model_product();
        if($_POST){
            $imgproid = '';
            $arr_image = array();

            if(strlen($_FILES['product_imgedit1']['name'])>1){
                $path_save_img = $this->UploadImage($_FILES['product_imgedit1']);

                if($path_save_img){
                    $img = array(
                        'images_url'   => URL.$path_save_img
                    );

                    $arr_image[] = $img;
                }
                $imgproid = $_POST['imgproductid1'];

                $imginfo = $model->GetImagesProductByImageId($imgproid);
                $imgoldlink = $imginfo[0]['images_url'];
            }

            if(strlen($_FILES['product_imgedit2']['name'])>1){
                $path_save_img = $this->UploadImage($_FILES['product_imgedit2']);

                if($path_save_img){
                    $img = array(
                        'images_url'   => URL.$path_save_img
                    );

                    $arr_image[] = $img;
                }
                $imgproid = $_POST['imgproductid2'];

                $imginfo = $model->GetImagesProductByImageId($imgproid);
                $imgoldlink = $imginfo[0]['images_url'];
            }

            if(strlen($_FILES['product_imgedit3']['name'])>1){
                $path_save_img = $this->UploadImage($_FILES['product_imgedit3']);

                if($path_save_img){
                    $img = array(
                        'images_url'   => URL.$path_save_img
                    );

                    $arr_image[] = $img;
                }
                $imgproid = $_POST['imgproductid3'];

                $imginfo = $model->GetImagesProductByImageId($imgproid);
                $imgoldlink = $imginfo[0]['images_url'];
            }

            if(strlen($_FILES['product_imgedit4']['name'])>1){
                $path_save_img = $this->UploadImage($_FILES['product_imgedit4']);

                if($path_save_img){
                    $img = array(
                        'images_url'   => URL.$path_save_img
                    );

                    $arr_image[] = $img;
                }
                $imgproid = $_POST['imgproductid4'];

                $imginfo = $model->GetImagesProductByImageId($imgproid);
                $imgoldlink = $imginfo[0]['images_url'];
            }

            for($i=0;$i<count($arr_image);$i++){
                $arr_img = array(
                    'images_url'    => $arr_image[$i]['images_url']
                );

                $result = $model->UpdateImagesProduct($arr_img,$imgproid);
                
                if($result){
                    $this->DeleteFile($imgoldlink);
                    echo $arr_image[$i]['images_url'];
                }
            }
        }
    }

    public function checkshipfee($type){
        switch ($type) {
            case 'don':
                return 20000;
                break;
            case 'doi':
                return 25000;
                break;
            case 'giadinh':
                return 30000;
                break;
        }
    }

    /*
    *   Gio hang
    */

    public function addproduct(){
        $model = new model_product();
        $items = array();
        if($_POST){
            $idproduct = $_POST['idproduct'];
            $type = $_POST['typeclothes'];
            $quantity = $_POST['quantity'];

            if($quantity < 1){
                $quantity = 1;
            }

            if(isset($_POST['manclothes'])){
                $size = $_POST['manclothes'];
                $style = 'nam';
            }elseif(isset($_POST['womanclothes'])){
                $size = $_POST['womanclothes'];
                $style = 'nu';
            }elseif(isset($_POST['babyclothes'])){
                $size = $_POST['babyclothes'];
                $style = 'embe';
            }

            $fee = $this->checkshipfee($type);
            $result = $model->GetProductById($idproduct);
            $salt = md5(uniqid(mt_rand(), true));

            foreach ($result as $key => $value) {
                if($type == 'doi'){
                    $result[$key]['quantity'] = $quantity;
                    $result[$key]['totalsum'] = intval($result[$key]['quantity'])*(intval($result[$key]['product_price_new'])*2);
                    $result[$key]['shipfee'] = $fee;
                    $result[$key]['sex']    = $style;
                    $result[$key]['size']   = $size;
                    $result[$key]['key'] = $salt;
                }elseif($type == 'don'){
                    $result[$key]['quantity'] = $quantity;
                    $result[$key]['totalsum'] = intval($result[$key]['quantity'])*intval($result[$key]['product_price_new']);
                    $result[$key]['shipfee'] = $fee;
                    $result[$key]['sex']    = $style;
                    $result[$key]['size']   = $size;
                    $result[$key]['key'] = $salt;
                }elseif ($type == 'giadinh'){
                    $result[$key]['quantity'] = $quantity;
                    $result[$key]['totalsum'] = intval($result[$key]['quantity'])*intval($result[$key]['product_price_new']);
                    $result[$key]['shipfee'] = $fee;
                    $result[$key]['sex']    = $style;
                    $result[$key]['size']   = $size;
                    $result[$key]['key'] = $salt;
                }
            }

            if($result){
                if(isset($_SESSION['cart'])){
                    if($type == 'don'){
                        
                        $_SESSION['cart']['don'][$salt] = $result;

                        foreach ($_SESSION['cart']['don'] as $key=>$value) {
                            $items[] = $value;
                        }

                    }elseif($type == 'doi'){

                        $_SESSION['cart']['doi'][$salt] = $result;

                        foreach ($_SESSION['cart']['doi'] as $key=>$value) {
                            $items[] = $value;
                        }
                    }elseif($type == 'giadinh'){

                        $_SESSION['cart']['giadinh'][$salt] = $result;

                        foreach ($_SESSION['cart']['giadinh'] as $key=>$value) {
                            $items[] = $value;
                        }
                    }
                }

                header('Location:'.URL.'cart');
            }
        }else{
            header('Location:'.URL);
        }
    }

    public function deleteproductcart(){
        $model = new model_product();
        $items = array();
        if($_POST){
            $idproduct = $_POST['idproduct'];
            $result = $model->GetProductById($idproduct);
            $key = $_POST['key'];

            if($result){
                if($_SESSION['cart']['don'][$key]){
                    unset($_SESSION['cart']['don'][$key]);
                }elseif($_SESSION['cart']['doi'][$key]){
                    unset($_SESSION['cart']['doi'][$key]);
                }elseif($_SESSION['cart']['giadinh'][$key]){
                    unset($_SESSION['cart']['giadinh'][$key]);
                }
            }

            if(isset($_SESSION['cart'])){
                foreach ($_SESSION['cart'] as $key=>$value) {
                        $items[] = $value;
            }

                echo '1';
            }else{
                echo 'Giỏ hàng của bạn chưa có sản phẩm nào.';
            }
        }
    }

    public function addquantity(){
        $model = new model_product();
        $items = array();
        if($_POST){
            $idproduct = $_POST['idproduct'];
            $result = $model->GetProductById($idproduct);

            foreach ($result as $key => $value) {
                $result[$key]['quantity'] = 1;
                $result[$key]['totalsum'] = intval($result[$key]['quantity'])*intval($result[$key]['product_price_new']);
            }

            if($result){
                if(isset($_SESSION['cart'])){
                    foreach ($_SESSION['cart'] as $key=>$value) {
                        if($value[0]['product_id'] == $idproduct){
                            $result[0]['quantity'] = intval($value[0]['quantity'])+1;
                            $result[0]['totalsum'] = intval($result[0]['quantity'])*intval($result[0]['product_price_new']);
                        }
                    }

                    $_SESSION['cart'][$idproduct] = $result;

                    foreach ($_SESSION['cart'] as $key=>$value) {
                        $items[] = $value;
                    }
                }
            }
        }
    }

    public function addquantitybyvalue(){
        $model = new model_product();
        $items = array();
        if($_POST){
            $idproduct = $_POST['idproduct'];
            $result = $model->GetProductById($idproduct);
            $type = $_POST['typeclothes'];
            $fee = $this->checkshipfee($type);
            $token = $_POST['key'];
            foreach ($result as $key => $value) {
                if($_SESSION['cart']['don'][$token]){
                    $checktok = $_SESSION['cart']['don'][$token];
                    if(intval($_POST['value']) > 0){
                        $result[$key]['quantity'] = $_POST['value'];
                        $result[$key]['totalsum'] = intval($result[$key]['quantity'])*intval($result[$key]['product_price_new']);
                    }else{
                        $result[$key]['quantity'] = 1;
                        $result[$key]['totalsum'] = intval($result[$key]['quantity'])*intval($result[$key]['product_price_new']);
                    }
                    $result[$key]['shipfee'] = $checktok[0]['shipfee'];
                    $result[$key]['sex']     = $checktok[0]['sex'];
                    $result[$key]['size']    = $checktok[0]['size'];
                    $result[$key]['key']     = $token;
                }elseif ($_SESSION['cart']['doi'][$token]) {
                    $checktok = $_SESSION['cart']['doi'][$token];
                    if(intval($_POST['value']) > 0){
                        $result[$key]['quantity'] = $_POST['value'];
                        $result[$key]['totalsum'] = intval($result[$key]['quantity'])*intval($result[$key]['product_price_new']);
                    }else{
                        $result[$key]['quantity'] = 1;
                        $result[$key]['totalsum'] = intval($result[$key]['quantity'])*intval($result[$key]['product_price_new']);
                    }
                    $result[$key]['shipfee'] = $checktok[0]['shipfee'];
                    $result[$key]['sex']     = $checktok[0]['sex'];
                    $result[$key]['size']    = $checktok[0]['size'];
                    $result[$key]['key']     = $token;
                }elseif ($_SESSION['cart']['giadinh'][$token]){
                    $checktok = $_SESSION['cart']['giadinh'][$token];
                    if(intval($_POST['value']) > 0){
                        $result[$key]['quantity'] = $_POST['value'];
                        $result[$key]['totalsum'] = intval($result[$key]['quantity'])*intval($result[$key]['product_price_new']);
                    }else{
                        $result[$key]['quantity'] = 1;
                        $result[$key]['totalsum'] = intval($result[$key]['quantity'])*intval($result[$key]['product_price_new']);
                    }
                    $result[$key]['shipfee'] = $this->checkshipfee('giadinh');
                    $result[$key]['shipfee'] = $checktok[0]['shipfee'];
                    $result[$key]['sex']     = $checktok[0]['sex'];
                    $result[$key]['size']    = $checktok[0]['size'];
                    $result[$key]['key']     = $token;
                }
            }

            if($result){
                if(isset($_SESSION['cart'])){
                    if($_SESSION['cart']['don'][$token]){
                        $_SESSION['cart']['don'][$token] = $result;

                        foreach ($_SESSION['cart']['don'] as $key=>$value) {
                            $items[] = $value;
                        }
                    }elseif($_SESSION['cart']['doi'][$token]){
                        $_SESSION['cart']['doi'][$token] = $result;

                        foreach ($_SESSION['cart']['doi'] as $key=>$value) {
                            $items[] = $value;
                        }
                    }elseif($_SESSION['cart']['giadinh'][$token]){
                        $_SESSION['cart']['giadinh'][$token] = $result;

                        foreach ($_SESSION['cart']['giadinh'] as $key=>$value) {
                            $items[] = $value;
                        }
                    }

                    echo '1';
                }
            }
        }
    }

    public function deletequantity(){
        $model = new model_product();
        $items = array();
        if($_POST){
            $idproduct = $_POST['idproduct'];
            $result = $model->GetProductById($idproduct);

            foreach ($result as $key => $value) {
                $result[$key]['quantity'] = 1;
                $result[$key]['totalsum'] = intval($result[$key]['quantity'])*intval($result[$key]['product_price_new']);
            }

            if($result){
                if(isset($_SESSION['cart'])){
                    foreach ($_SESSION['cart'] as $key=>$value) {
                        if($value[0]['product_id'] == $idproduct){
                            if($value[0]['quantity'] >1){
                                $result[0]['quantity'] = intval($value[0]['quantity'])-1;
                                $result[0]['totalsum'] = intval($result[0]['quantity'])*intval($result[0]['product_price_new']);
                            }else{
                                $result[0]['quantity'] = 1;
                                $result[0]['totalsum'] = intval($result[0]['quantity'])*intval($result[0]['product_price_new']);
                            }
                        }
                    }

                    $_SESSION['cart'][$idproduct] = $result;

                    foreach ($_SESSION['cart'] as $key=>$value) {
                        $items[] = $value;
                    }
                }
            }
        }
    }
}