<?php
class controller_safe extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    public function index(){

    	$model = new model_setting();

        $allsetting = $model->GetAllSetting();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $this->view->breadcrumb = '';

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOnByPostion('all','ngang')){
            $allmenufather = $model1->SelectAllMenuFatherOnByPostion('all','ngang');

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        if($model1->SelectAllMenuFatherOnByPostion('product','doc')){
            $allmenufatherproduct = $model1->SelectAllMenuFatherOnByPostion('product','doc');

            foreach($allmenufatherproduct as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufatherproduct[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenuproduct = $allmenufatherproduct;
        }
    	
    	$this->view->css = array(
            URL.'public/flexslider/flexslider.css'
        );

        $this->view->js = array(
            URL.'public/flexslider/jquery.flexslider.js',
            URL.'public/js/config-flexider.js',
        );

        $this->view->layout('layout');
        $this->view->Render('safe/index');
        return true;
    }
}