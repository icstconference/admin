<?php
class controller_personal extends lib_controller{

    public function __construct() {
        parent::__construct();
        if(lib_session::Logged() == true){

        }
    }

    /*
    * Các function tạo hình ảnh
    */

    function UploadImage(array $data) {
    	$type = '';
        if(($data["type"] == "image/jpeg")
            || ($data["type"] == "image/jpg") || ($data["type"] == "image/pjpeg")){
                $type = 'jpg';  
        }elseif($data["type"] == "image/gif"){
            $type = 'gif';
        }elseif($data["type"] == "image/png"){
            $type = 'png';
        }
        if ((($data["type"] == "image/gif") || ($data["type"] == "image/jpeg")
                || ($data["type"] == "image/jpg") || ($data["type"] == "image/pjpeg")
                || ($data["type"] == "image/x-png") || ($data["type"] == "image/png"))
            && ($data["size"] < 4000000)) {
            if ($data["error"] == 0) {
                $path = "public/upload/images/personal/" . uniqid() . '.'.$type;
                move_uploaded_file($data["tmp_name"], $path);
                return $path;
            }
        }
        return false;
    }

    protected function DeleteFile($file_path) {
        $path = str_replace(URL, '', $file_path);
        if (file_exists($path)) {
            unlink($path);
            return true;
        }
        return false;
    }

    /*
    *	Tao nhan su
    */

    public function editpersonal($path){
    	if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login/');
        }
        $model = new model_personal();

        if($model->GetAllPersonalCategories()){
            $this->view->listcategories = $model->GetAllPersonalCategories();
        }

        if($path){
        	if($model->GetPersonalById($path)){
        		$personalinfo = $model->GetPersonalById($path);

        		$this->view->personalinfo = $personalinfo;
        	}
        }else{
        	header('Location:'.URL.'personal');
        }


    	$this->view->title = 'Trang quản trị';

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/personal.js'
        );

        $this->view->layout('layout-admin');
        $this->view->Render('personal/editpersonal');
        return true;
    }

    public function deletepersonal($path){
    	$model = new model_personal();

    	if($path){
    		if($model->GetPersonalById($path)){
        		$personalinfo = $model->GetPersonalById($path);

        		$this->view->personalinfo = $personalinfo;

        		$delete = $model->DeletePersonal($path);

        		if($delete){
        			$this->DeleteFile($personalinfo[0]['personal_image']);
        			header('Location:'.URL.'personal');
        		}
        	}
    	}else{
    		header('Location:'.URL.'personal');
    	}
    }

    public function updatepersonal(){
    	require_once 'plugin/phpthumb/ThumbLib.inc.php';
    	$model = new model_personal();

        if($_POST){
        	$personal_id			= $_POST['personal_id'];
        	$personalinfo			= $model->GetPersonalById($personal_id);
        	$personal_name 			= $_POST['personal_name'];
            $personal_name_english  = $_POST['personal_name_english'];
        	$personal_categories  	= $_POST['personal_categories'];
        	$personal_position		= $_POST['personal_position'];
            $personal_position_english = $_POST['personal_position_english'];
			$personal_sort 			= $_POST['personal_sort'];

        	if(strlen($_FILES['personal_img']['name'])>1){
                $upload_img = $this->UploadImage($_FILES['personal_img']);

                if($upload_img){
                    $personal_image = URL.$upload_img;
                    $this->DeleteFile($personalinfo[0]['personal_image']);
                }else{
                    $personal_image = $personalinfo[0]['personal_image'];
                }
            }else{
                $personal_image = $personalinfo[0]['personal_image'];
            }
			
			//print_r($personal_categories);

            $data = array(
            	'personal_name'			=> addslashes($personal_name),
                'personal_name_english' => addslashes($personal_name_english),
            	'categories_id'			=> $personal_categories,
            	'personal_position'		=> addslashes($personal_position),
            	'personal_image'		=> $personal_image,
                'personal_position_english' => addslashes($personal_position_english),
				'personal_sort'			=> $personal_sort
            );
			
			//print_r($data);

            $result = $model->UpdatePersonal($data,$personal_id);

            if($result){
            	echo '1';
            }
        }
    }

    public function index(){
    	if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login/');
        }
        $model = new model_personal();

        if($model->GetAllPersonal()){
            $this->view->listpersonal = $model->GetAllPersonal();
        }

        if($model->GetAllPersonalCategories()){
            $this->view->listcategories = $model->GetAllPersonalCategories();
        }

        $this->view->title = 'Trang quản trị';

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/personal.js'
        );

        $this->view->layout('layout-admin');
        $this->view->Render('personal/index');
        return true;
    }

    public function createpersonal(){
    	require_once 'plugin/phpthumb/ThumbLib.inc.php';
    	$model = new model_personal();
        if($_POST){
        	$personal_name 			= $_POST['personal_name'];
            $personal_name_english  = $_POST['personal_name_english'];
        	$personal_categories  	= $_POST['personal_categories'];
        	$personal_position		= $_POST['personal_position'];
            $personal_position_english = $_POST['personal_position_english'];
			$personal_sort			= $_POST['personal_sort'];

        	if(strlen($_FILES['personal_img']['name'])>1){
                $upload_img = $this->UploadImage($_FILES['personal_img']);

                if($upload_img){
                    $personal_image = URL.$upload_img;
                }else{
                    $personal_image = '';
                }
            }else{
                $personal_image = '';
            }

            $data = array(
            	'personal_name'			=> $personal_name,
                'personal_name_english' => $personal_name_english,
            	'categories_id'			=> $personal_categories,
            	'personal_position'		=> $personal_position,
                'personal_position_english' => $personal_position_english,
            	'personal_image'		=> $personal_image,
				'personal_sort'			=> $personal_sort
            );

            $result = $model->InsertPersonal($data);

            if($result){
            	echo '1';
            }
        }
    }

    /*
    *	Tao loai nhan su
    */

    function editcategories($path){
    	require_once 'plugin/url-seo/url-seo.php';
        if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login/');
        }

        $model = new model_personal();

        if($model->GetPersonalCategoriesById($path)){
            $result = $model->GetPersonalCategoriesById($path);

            $this->view->categoriesinfo = $result;

            if($model->GetAllPersonalCategories()){
                $listcategories = $model->GetAllPersonalCategories();

                $this->view->listcategories = $listcategories;
            }

            if($_POST){
				$personal_type 		   = $_POST['personal_type'];
                $personal_type_english = $_POST['personal_type_english'];
            	$personal_fathertype   = $_POST['personal_fathertype'];
            	$personal_url 		   = post_slug($personal_type).'-'.$path;
                $personal_url_english  = post_slug($personal_type_english).'-'.$path;
				
	            if($personal_fathertype){
	                $personal_father = $personal_fathertype;
	            }else{
	                $personal_father = 0;
	            }

	            $data = array(
	                'personal_categories_name' 		   => $personal_type,
                    'personal_categories_name_english' => $personal_type_english,
	                'personal_categories_father' 	   => $personal_father,
	                'personal_categories_url'    	   => $personal_url,
                    'personal_categories_url_english'  => $personal_url_english
	            );

                $update = $model->UpdatePersonalCategories($data,$path);
                if($update){
                    header('Location:'.URL.'personal/categories/');
                }
            }

            $this->view->js = array(
                URL.'public/js/user.js'
            );

            $this->view->title = 'Trang quản trị';

            $this->view->layout('layout-admin');
            $this->view->Render('personal/editcategories');
        }else{
            header('Location:'.URL.'personal/categories/');
        }
        return true;
    }

    function deletecategories($path){
        if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login/');
        }

        $model = new model_personal();

        if($model->GetPersonalCategoriesById($path)){
            $result = $model->GetPersonalCategoriesById($path);

            $this->view->categoriesinfo = $result;

            $delete = $model->DeletePersonalCategories($path);

            if($delete){
                header('Location:'.URL.'personal/categories/');
            }
        }else{
            header('Location:'.URL.'personal/categories/');
        }
        return true;
    }

    function categories(){
        require_once 'plugin/url-seo/url-seo.php';
        if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login/');
        }

        $model = new model_personal();

        if($model->GetAllPersonalCategories()){
            $listcategories = $model->GetAllPersonalCategories();

            $this->view->listcategories = $listcategories;
        }

        if($_POST){
            $personal_type 		   = $_POST['personal_type'];
            $personal_type_english = $_POST['personal_type_english'];
            $personal_fathertype   = $_POST['personal_fathertype'];


            if($personal_fathertype){
                $personal_father = $personal_fathertype;
            }else{
                $personal_father = 0;
            }

            $data = array(
                'personal_categories_name' 		   => $personal_type,
                'personal_categories_name_english' => $personal_type_english,
                'personal_categories_father' 	   => $personal_father,
                'personal_categories_url'    	   => ''
            );

            $result = $model->InsertPersonalCategories($data);
            if($result){
                $personal_url = post_slug($personal_type).'-'.$result;
                $personal_url_english = post_slug($personal_type_english).'-'.$result;
                $data1 = array(
                    'personal_categories_url'           => $personal_url,
                    'personal_categories_url_english'   => $personal_url_english
                );
                $update = $model->UpdatePersonalCategories($data1,$result);
                if($update){
                    header('Location:'.URL.'personal/categories/');
                }
            }
        }

        $this->view->js = array(
            URL.'public/js/user.js'
        );

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin');
        $this->view->Render('personal/categories');
        return true;
    }

    public function createpersonal1(){
        require_once 'plugin/phpthumb/ThumbLib.inc.php';
        $model = new model_personal();
        if($_POST){
            $personal_lab_name             = $_POST['personal_lab_name'];
            $personal_lab_name_english     = $_POST['personal_lab_name_english'];
            $personal_lab_categories       = $_POST['personal_lab_categories'];
            $personal_lab_position         = $_POST['personal_lab_position'];
            $personal_lab_position_english = $_POST['personal_lab_position_english'];
			$personal_lab_sort			   = $_POST['personal_lab_sort'];
				
            if(strlen($_FILES['personal_lab_img']['name'])>1){
                $upload_img = $this->UploadImage($_FILES['personal_lab_img']);

                if($upload_img){
                    $personal_lab_img = URL.$upload_img;
                }else{
                    $personal_lab_img = '';
                }
            }else{
                $personal_lab_img = '';
            }

            $data = array(
                'personal_lab_name'             => $personal_lab_name,
                'personal_lab_name_english'     => $personal_lab_name_english,
                'personal_lab_categories'       => $personal_lab_categories,
                'personal_lab_position'         => $personal_lab_position,
                'personal_lab_position_english' => $personal_lab_position_english,
                'personal_lab_image'            => $personal_lab_img,
				'personal_lab_sort'				=> $personal_lab_sort
            );

            $result = $model->InsertPersonalLab($data);

            if($result){
                echo '1';
            }
        }
    }

    public function updatepersonal1(){
        require_once 'plugin/phpthumb/ThumbLib.inc.php';
        $model = new model_personal();

        if($_POST){
            $personal_id                   = $_POST['personal_id'];
            $personalinfo                  = $model->GetPersonalLabById($personal_id);
            $personal_lab_name             = $_POST['personal_lab_name'];
            $personal_lab_name_english     = $_POST['personal_lab_name_english'];
            $personal_lab_categories       = $_POST['personal_lab_categories'];
            $personal_lab_position         = $_POST['personal_lab_position'];
            $personal_lab_position_english = $_POST['personal_lab_position_english'];
			$personal_lab_sort			   = $_POST['personal_lab_sort'];

            if(strlen($_FILES['personal_lab_img']['name'])>1){
                $upload_img = $this->UploadImage($_FILES['personal_lab_img']);

                if($upload_img){
                    $personal_image = URL.$upload_img;
                    $this->DeleteFile($personalinfo[0]['personal_lab_image']);
                }else{
                    $personal_image = $personalinfo[0]['personal_lab_image'];
                }
            }else{
                $personal_image = $personalinfo[0]['personal_lab_image'];
            }

            //print_r($personal_categories);

            $data = array(
                'personal_lab_name'             => $personal_lab_name,
                'personal_lab_name_english'     => $personal_lab_name_english,
                'personal_lab_categories'       => $personal_lab_categories,
                'personal_lab_position'         => $personal_lab_position,
                'personal_lab_position_english' => $personal_lab_position_english,
                'personal_lab_image'            => $personal_image,
				'personal_lab_sort'				=> $personal_lab_sort
            );
            
            //print_r($data);

            $result = $model->UpdatePersonalLab($data,$personal_id);

            if($result){
                echo '1';
            }
        }
    }

    public function deletepersonal1($path){
        $model = new model_personal();

        if($path){
            if($model->GetPersonalLabById($path)){
                $personalinfo = $model->GetPersonalLabById($path);

                $this->view->personalinfo = $personalinfo;

                $delete = $model->DeletePersonalLab($path);

                if($delete){
                    $this->DeleteFile($personalinfo[0]['personal_lab_image']);
                    header('Location:'.URL.'personal/lab/');
                }
            }
        }else{
            header('Location:'.URL.'personal/lab/');
        }
    }

    public function editpersonal1($path){
        if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login/');
        }

        $model = new model_personal();

        $news = new model_news();

        $model1 = new model_general();

        $list_permission = '';
        
        if($model1->GetPermissionNewsByUserId($_SESSION['user_id'])){
            $listpermission = $model1->GetPermissionNewsByUserId($_SESSION['user_id']);
            
            foreach($listpermission as $ls){
                $list_permission .= ','.$ls['categories_id'];
            }
        }
        
        $this->view->listpermission = $list_permission;
        

        if($news->SelectAllCategoriesNews()){
            $this->view->listcategories = $news->SelectAllCategoriesNews();
        }


        if($path){
            if($model->GetPersonalLabById($path)){
                $personalinfo = $model->GetPersonalLabById($path);

                $this->view->personalinfo = $personalinfo;
            }
        }else{
            header('Location:'.URL.'personal');
        }

        $this->view->title = 'Trang quản trị';

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/personal.js'
        );

        $this->view->layout('layout-admin');
        $this->view->Render('personal/editlab');
        return true;
    }

    public function lab(){
        if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login/');
        }

        $model = new model_personal();

        $news = new model_news();

        $model1 = new model_general();

        $list_permission = '';
        
        if($model1->GetPermissionNewsByUserId($_SESSION['user_id'])){
            $listpermission = $model1->GetPermissionNewsByUserId($_SESSION['user_id']);
            
            foreach($listpermission as $ls){
                $list_permission .= ','.$ls['categories_id'];
            }
        }
        
        $this->view->listpermission = $list_permission;
        

        if($news->SelectAllCategoriesNews()){
            $this->view->listcategories = $news->SelectAllCategoriesNews();
        }

        if($model->GetAllPersonalLab()){
            $this->view->listpersonal = $model->GetAllPersonalLab();
        }


        $this->view->title = 'Trang quản trị';

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/personal.js'
        );

        $this->view->layout('layout-admin');
        $this->view->Render('personal/lab');
        return true;
    }
}