<?php
class controller_mobile extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    public function index(){
    	$model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $partner = new model_partner();

        $library = new model_library();
		
		$video = new model_video();

		$slider = new model_slider();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
			$this->view->aboutus = $allsetting[0]['config_url_aboutus'];
            $this->view->laboratories = $allsetting[0]['config_url_laboratories'];
			$this->view->emaillink = $allsetting[0]['config_url_email'];
            $this->view->internal = $allsetting[0]['config_url_internal'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOn()){
            $allmenufather = $model1->SelectAllMenuFatherOn();

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        if($slider->SelectAllSliderStatusByKind(1)){
            $this->view->listslider1 = $slider->SelectAllSliderStatusByKind(1);
        }

        $this->view->layout('layout-mobile');
        $this->view->Render('mobile/index');
        return true;
    }

    public function news($path = null,$page = 1){

        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $partner = new model_partner();

        $library = new model_library();
		
		$video = new model_video();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
			$this->view->aboutus = $allsetting[0]['config_url_aboutus'];
            $this->view->laboratories = $allsetting[0]['config_url_laboratories'];
			$this->view->emaillink = $allsetting[0]['config_url_email'];
            $this->view->internal = $allsetting[0]['config_url_internal'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOn()){
            $allmenufather = $model1->SelectAllMenuFatherOn();

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        $model4 = new model_news();

        if($path){
            $part = explode('_',$path);
            $id = $part[count($part)-1];
            $result = $model4->GetCategoriesById($id);

            if($result){
                $this->view->categoriesinfo = $result;

				if($this->view->lang == 'vi'){
					$this->view->title = $result[0]['news_categories_name'].'-'.$allsetting[0]['config_title'];
					$this->view->description = $result[0]['news_categories_name'].'-'.$allsetting[0]['config_description'];
					$this->view->keyword = $result[0]['news_categories_name'].','.$allsetting[0]['config_keyword'];
				}else{
					$this->view->title = $result[0]['news_categories_name_english'].'-'.'Institute for Computational Science and Technology';
					$this->view->description = $result[0]['news_categories_name_english'].'-'.$allsetting[0]['config_description'];
					$this->view->keyword = $result[0]['news_categories_name_english'].','.$allsetting[0]['config_keyword'];
				}
				
                if($page == 1){
                    $iteminpage = 12;
                }

                if($page > 1){
                    $iteminpage = 12;
                }

                $pagination = new lib_pagination();
                $pagination->SetCurrentPage($page);
                $pagination->SetRowInPage($iteminpage);
                $pagination->SetUrl(URL . 'category/news/'.$path.'/@page');
                $pagination->SetRange(4);
                $pagination->SetLanguage(array(
                    'first'     => '<<',
                    'previous'  => '<',
                    'next'      => '>',
                    'last'      => '>>'
                ));
                $paginator  = $pagination->BuildPagination($this->GetBlogDB1($id));

                $this->view->paginator  = $paginator['paginator'];

                $listnew   = $model4->PaginationNews($page,$iteminpage,$id);

                foreach ($listnew as $key => $value) {
                    if($news->GetCategoriesNewsById($value['news_categories_id'])){
                        $newsinfo = $news->GetCategoriesNewsById($value['news_categories_id']);
                        $listnew[$key]['category'] = $newsinfo[0];
                    }
                }

                $this->view->listnew = $listnew;
            }
        }else{
            if($page == 1){
                $iteminpage = 12;
            }

            if($page > 1){
                $iteminpage = 12;
            }

            $pagination = new lib_pagination();
            $pagination->SetCurrentPage($page);
            $pagination->SetRowInPage($iteminpage);
            $pagination->SetUrl(URL . 'category/news/'.$path.'/@page');
            $pagination->SetRange(4);
            $pagination->SetLanguage(array(
                'first'     => '<<',
                'previous'  => '<',
                'next'      => '>',
                'last'      => '>>'
            ));
            $paginator  = $pagination->BuildPagination($this->GetBlogDB1(0));

            $this->view->paginator  = $paginator['paginator'];

            $listnew   = $model4->PaginationNews($page,$iteminpage,0);

            foreach ($listnew as $key => $value) {
                if($news->GetCategoriesNewsById($value['news_categories_id'])){
                    $newsinfo = $news->GetCategoriesNewsById($value['news_categories_id']);
                    $listnew[$key]['category'] = $newsinfo[0];
                }
            }

            $this->view->listnew = $listnew;
        }

        $this->view->numpage = $page;

        $this->view->layout('layout-mobile');
        $this->view->Render('mobile/news');
        return true;
    }

    private function GetBlogDB1($catid) {
        $db = new lib_db();

        if($catid != 0){
            return $db->query('select *
                from tbl_news
                where news_categories_id ='.$catid.' order by news_create_date DESC');
        }else{
            return $db->query('select *
                from tbl_news order by news_create_date DESC');
        }
    }

    public function contact(){
    	$model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $partner = new model_partner();

        $library = new model_library();
        
        $video = new model_video();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
            $this->view->aboutus = $allsetting[0]['config_url_aboutus'];
            $this->view->laboratories = $allsetting[0]['config_url_laboratories'];
            $this->view->emaillink = $allsetting[0]['config_url_email'];
            $this->view->internal = $allsetting[0]['config_url_internal'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOn()){
            $allmenufather = $model1->SelectAllMenuFatherOn();

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }
        
        $contact = new model_contact();

        if($model->GetAllContact()){
            $listcontact = $model->GetAllContact();

            $this->view->listcontact = $listcontact;
        }

        $this->view->layout('layout-mobile');
        $this->view->Render('mobile/contact');
        return true;
    }

    public function file($path = null){
        $file = new model_file();

        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
			$this->view->aboutus = $allsetting[0]['config_url_aboutus'];
            $this->view->laboratories = $allsetting[0]['config_url_laboratories'];
			$this->view->emaillink = $allsetting[0]['config_url_email'];
            $this->view->internal = $allsetting[0]['config_url_internal'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOn()){
            $allmenufather = $model1->SelectAllMenuFatherOn();

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        if($file->GetAllFileCategoriesFather()){
            $listcategoriesfather = $file->GetAllFileCategoriesFather();

            $this->view->listcategoriesfather = $listcategoriesfather;
        }

        if($path){
            $part = explode('_',$path);
            $id = $part[count($part)-1];
            $result = $file->GetFileCategoriesById($id);

            if($result){
                $listallcategories = $file->GetAllFileCategories1();

                foreach ($result as $key => $value) {
                    if($file->GetAllFileCategoriesChildByFatherId1($value['file_categories_id'])){
                        $child = $file->GetAllFileCategoriesChildByFatherId1($value['file_categories_id']);
                        $filechild = $file->GetAllFileByCategoriesId($value['file_categories_id']);

                        foreach ($child as $key1 => $value1) {
                            $child1 = array();
                            $filechild1 = $file->GetAllFileByCategoriesId($value1['file_categories_id']);

                            foreach ($listallcategories as $value2) {
                                if(strpos($value2['file_categories_father'], $value1['file_categories_id'])){
                                    $child1[] = $value2;
                                }

                                if($value2['file_categories_father'] == $value1['file_categories_id']){
                                    $child1[] = $value2;
                                }
                            }

                            foreach ($child1 as $key2 => $value2) {
                                $child2 = array();
                                $filechild2 = $file->GetAllFileByCategoriesId($value2['file_categories_id']);

                                foreach ($listallcategories as $value3) {
                                    if(strpos($value3['file_categories_father'], $value2['file_categories_id'])){
                                        $child2[] = $value3;
                                    }

                                    if($value3['file_categories_father'] == $value2['file_categories_id']){
                                        $child2[] = $value3;
                                    }
                                }

                                foreach ($child2 as $key3 => $value3) {
                                    $filechild3 = $file->GetAllFileByCategoriesId($value3['file_categories_id']);

                                    $child2[$key3]['childfile'] = $filechild3;
                                }


                                $child1[$key2]['child'] = $child2;
                                $child1[$key2]['childfile'] = $filechild2;
                            }

                            $child[$key1]['child'] = $child1;
                            $child[$key1]['childfile'] = $filechild1;
                        }
                    }

                    $result[$key]['child'] = $child;
                    $result[$key]['childfile'] = $filechild;
                }

                $this->view->filecategoriseinfo = $result;

				if($this->view->lang == 'vi'){
					$this->view->title = $result[0]['file_categories_name'].'-'.$allsetting[0]['config_title'];
					$this->view->description = $result[0]['file_categories_name'].'-'.$allsetting[0]['config_description'];
					$this->view->keyword = $result[0]['file_categories_name'].','.$allsetting[0]['config_keyword'];
				}else{
					$this->view->title = $result[0]['file_categories_name_english'].'-'.'Institute for Computational Science and Technology';
					$this->view->description = $result[0]['file_categories_name_english'].'-'.$allsetting[0]['config_description'];
					$this->view->keyword = $result[0]['file_categories_name_english'].','.$allsetting[0]['config_keyword'];
				}
				
				$listfile = array();
			
                if($file->GetAllFileByCategoriesId($result[0]['file_categories_id'])){
                    $files = $file->GetAllFileByCategoriesId($result[0]['file_categories_id']);
					foreach($files as $fl){
						$listfile[] = $fl;
					}
				}
				
				if($file->GetAllFileCategoriesChildByFatherId($result[0]['file_categories_id'])){
					$child = $file->GetAllFileCategoriesChildByFatherId($result[0]['file_categories_id']);
					foreach($child as $ch){
						if($file->GetAllFileByCategoriesId($ch['file_categories_id'])){
							$files1 = $file->GetAllFileByCategoriesId($ch['file_categories_id']);
							foreach($files1 as $fl1){
								$listfile[] = $fl1;
							}
						}
						if($file->GetAllFileCategoriesChildByFatherId($ch['file_categories_id'])){
							$child1 = $file->GetAllFileCategoriesChildByFatherId($ch['file_categories_id']);
							foreach($child1 as $ch1){
								if($file->GetAllFileByCategoriesId($ch1['file_categories_id'])){
									$files2 = $file->GetAllFileByCategoriesId($ch1['file_categories_id']);
									foreach($files2 as $fl2){
										$listfile[] = $fl2;
									}
								}
							}
						}
					}
				}
				
				$this->view->listfile = $listfile;
            }
        }else{
            $result = $file->GetFirstFileCategoriesFather();

            $this->view->title = $result[0]['file_categories_name'].'-'.$allsetting[0]['config_title'];
            $this->view->description = $result[0]['file_categories_name'].'-'.$allsetting[0]['config_description'];
            $this->view->keyword = $result[0]['file_categories_name'].','.$allsetting[0]['config_keyword'];

            $this->view->filecategoriseinfo = $result;

			$listfile = array();
			
                if($file->GetAllFileByCategoriesId($result[0]['file_categories_id'])){
                    $files = $file->GetAllFileByCategoriesId($result[0]['file_categories_id']);
					foreach($files as $fl){
						$listfile[] = $fl;
					}
				}

				$this->view->listfile = $listfile;
        }

        $this->view->layout('layout-mobile');
        $this->view->Render('mobile/file');
        return true;
    }

    function newsdetail($path){
        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $partner = new model_partner();

        $library = new model_library();
		
		$video = new model_video();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
			$this->view->aboutus = $allsetting[0]['config_url_aboutus'];
            $this->view->laboratories = $allsetting[0]['config_url_laboratories'];
			$this->view->emaillink = $allsetting[0]['config_url_email'];
            $this->view->internal = $allsetting[0]['config_url_internal'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOn()){
            $allmenufather = $model1->SelectAllMenuFatherOn();

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }


        $model4 = new model_news();

        if($model4->SelectAllCategoriesNews()){
            $this->view->listcategoriesnews = $model4->SelectAllCategoriesNews();
        }

        if($path){
            $part = explode('_',$path);
            $id = $part[count($part)-1];
            $result = $model4->SelectNewsById($id);

            if($result){

                foreach ($result as $key => $value) {
                    if($comment->NumCommentByPostId($id)){
                        $numcomment = $comment->NumCommentByPostId($id);
                        $result[$key]['news_comment'] = $numcomment[0]['numcomment'];
                    }
                }

                $view = intval($result[0]['view']) + 1;

                $this->view->newsinfo = $result;

                $catnews = $model4->GetCategoriesById($result[0]['news_categories_id']);

                $this->view->catinfo = $catnews;

                if($model4->SelectAllNewsByCatId($result[0]['news_categories_id'])){
                    $this->view->listrelatenews = $model4->SelectAllNewsByCatId($result[0]['news_categories_id']);
                }
				
				if($this->view->lang == 'vi'){
					$this->view->title = $result[0]['news_name'].'-'.$allsetting[0]['config_title'];
					$this->view->description = $result[0]['news_name'].'-'.$allsetting[0]['config_description'];
					$this->view->keyword = $result[0]['news_name'].','.$allsetting[0]['config_keyword'];
				}else{
					$this->view->title = $result[0]['news_name_english'].'-'.'Institute for Computational Science and Technology';
					$this->view->description = $result[0]['news_name_english'].'-'.$allsetting[0]['config_description'];
					$this->view->keyword = $result[0]['news_name_english'].','.$allsetting[0]['config_keyword'];
				}

                if($catnews){
                    $this->view->catenews = $catnews;
                }

                $arr = array(
                    'view'  =>$view
                );

                $update = $model4->UpdateNews($arr,$id);

                if($comment->GetCommentByPostId($id)){
                    $this->view->listcomment = $comment->GetCommentByPostId($id);
                }
            }
        }
        
        $this->view->layout('layout-mobile');
        $this->view->Render('mobile/newsdetail');
        return true;
    }

    function page($path){
   		$page = new model_page();

        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $partner = new model_partner();

        $library = new model_library();
        
        $video = new model_video();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
            $this->view->aboutus = $allsetting[0]['config_url_aboutus'];
            $this->view->laboratories = $allsetting[0]['config_url_laboratories'];
            $this->view->emaillink = $allsetting[0]['config_url_email'];
            $this->view->internal = $allsetting[0]['config_url_internal'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOn()){
            $allmenufather = $model1->SelectAllMenuFatherOn();

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        $page = new model_page();

        if($path){
            $part = explode('_',$path);
            $id = $part[count($part)-1];

            if($page->GetPageById($id)){

                $pageinfo = $page->GetPageById($id);

                $this->view->pageinfo = $pageinfo;

                if($this->view->lang == 'vi'){
                    $this->view->title = $pageinfo[0]['page_name'].'-'.$allsetting[0]['config_title'];
                    $this->view->description = $pageinfo[0]['page_name'].'-'.$allsetting[0]['config_description'];
                    $this->view->keyword = $pageinfo[0]['page_name'].','.$allsetting[0]['config_keyword'];
                }else{
                    $this->view->title = $result[0]['page_name_english'].'-'.'Institute for Computational Science and Technology';
                    $this->view->description = $result[0]['page_name_english'].'-'.$allsetting[0]['config_description'];
                    $this->view->keyword = $result[0]['page_name_english'].','.$allsetting[0]['config_keyword'];
                }


                if($id == 6 || $id == 7 || $id == 8 || $id == 9){


                    $this->view->layout('layout-mobile');
                    $this->view->Render('mobile/page1');
                }elseif($id == 10 || $id == 11 || $id == 12 || $id == 13){
                    

                    $this->view->layout('layout-mobile');
                    $this->view->Render('mobile/page2');
                }elseif($id == 14 || $id == 15 || $id == 16 || $id == 17){

                    $this->view->layout('layout-mobile');
                    $this->view->Render('mobile/page3');
                }else{
                    
                    $this->view->layout('layout-mobile');
                    $this->view->Render('mobile/page');
                }
            }
        }else{
            header('Location:'.URL);
        }
    }

    public function image(){
    	$model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $partner = new model_partner();

        $library = new model_library();
		
		$video = new model_video();

        $slider = new model_slider();

        if($allsetting){
			if($this->view->lang == 'vi'){
				$this->view->title = $allsetting[0]['config_title'];
				$this->view->description = $allsetting[0]['config_description'];
				$this->view->keyword = $allsetting[0]['config_keyword'];
			}else{
				$this->view->title = 'PV Gas D';
				$this->view->description = $allsetting[0]['config_description'];
				$this->view->keyword = $allsetting[0]['config_keyword'];
			}
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
			$this->view->aboutus = $allsetting[0]['config_url_aboutus'];
            $this->view->laboratories = $allsetting[0]['config_url_laboratories'];
			$this->view->emaillink = $allsetting[0]['config_url_email'];
            $this->view->internal = $allsetting[0]['config_url_internal'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOn()){
            $allmenufather = $model1->SelectAllMenuFatherOn();

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        if($news->SelectTopNewsRelease(12)){
            $listnewsnew = $news->SelectTopNewsRelease(12);

            foreach ($listnewsnew as $key => $value) {
                if($news->GetCategoriesNewsById($value['news_categories_id'])){
                    $newsinfo = $news->GetCategoriesNewsById($value['news_categories_id']);
                    $listnewsnew[$key]['category'] = $newsinfo[0];
                }
            }

            $this->view->listnewsnew = $listnewsnew;
        }

        if($library->SelectAllLibrary()){
            $this->view->listlibrary = $library->SelectAllLibrary();
        }

        $this->view->layout('layout-mobile');
        $this->view->Render('mobile/image');
    }
}