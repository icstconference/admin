<?php
class controller_question extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    public function index(){
    	if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login');
        }

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/question.js'
        );
        
        $question = new model_question();

        if($question->SelectAllQuestion()){
        	$this->view->listquestion = $question->SelectAllQuestion();
        }

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin');
        $this->view->Render('question/index');
        return true;
    }

    public function editquestion($path){
    	require_once 'plugin/url-seo/url-seo.php';
    	if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login');
        }

        $question = new model_question();

        if($path){
        	if($question->GetQuestionById($path)){
        		$questioninfo = $question->GetQuestionById($path);

        		$this->view->questioninfo = $questioninfo;

        		if($_POST){
        			$questionaire_name		= $_POST['questionaire_name'];
		        	$questionaire_email   	= $_POST['questionaire_email'];
		        	$questionaire_phone	= $_POST['questionaire_phone'];
		        	$questionaire_question	= $_POST['questionaire_question'];
		        	$questionaire_answer	= $_POST['questionaire_answer'];
		        	$questionaire_status	= $_POST['questionaire_status'];

		        	$questionaire_url  		= post_slug($questionaire_question).'-'.$path;

		        	$data = array(
		        		'questionaire_name' 	=> $questionaire_name,
		        		'questionaire_email'	=> $questionaire_email,
		        		'questionaire_phone'	=> $questionaire_phone,
		        		'questionaire_question'	=> $questionaire_question,
		        		'questionaire_answer'	=> $questionaire_answer,
		        		'questionaire_status'	=> $questionaire_status,
		        		'questionaire_url'		=> $questionaire_url
		        	);

		        	$result = $question->UpdateQuestion($data,$path);

		        	if($result){
		        		header('Location:'.URL.'question');
		        	}
        		}
        	}else{
        		header('Location:'.URL.'question');
        	}
        }else{
        	header('Location:'.URL.'question');
        }

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/question.js'
        );

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin');
        $this->view->Render('question/editquestion');
        return true;
    }

    public function check($id){
    	require_once 'plugin/url-seo/url-seo.php';
        $question = new model_question();

        if($question->GetQuestionById($id)){
        	$questioninfo = $question->GetQuestionById($id);

	       	if($questioninfo[0]['questionaire_status'] == 'on'){
	        	$array = array(
	        		'questionaire_status' => 'off'
	        	);

	        	$result = $question->UpdateQuestion($array,$id);
	        	if($result){
	        		header('Location:'.URL.'question');
	        	}
	        }else{
	        	$questionaire_url  		= post_slug($questioninfo[0]['questionaire_question']).'-'.$id;
				$array = array(
	        		'questionaire_status' => 'on',
	        		'questionaire_url'	  => $questionaire_url
	        	);

	        	$result = $question->UpdateQuestion($array,$id);
	        	if($result){
	        		header('Location:'.URL.'question');
	        	}
	        }
        }else{
        	header('Location:'.URL.'question');
        }
    }

    public function deletequestion($path){
    	$question = new model_question();

    	if($path){
    		if($question->GetQuestionById($path)){
    			$deletequestion = $question->DeleteQuestion($path);
    			if($deletequestion){
    				header('Location:'.URL.'question');
    			}
    		}else{
    			header('Location:'.URL.'question');
    		}
    	}else{
    		header('Location:'.URL.'question');
    	}
    }

    public function createquestion(){
        require_once('plugin/mail/class.phpmailer.php');
    	$question = new model_question();

        $setting = new model_setting();

        if($setting->GetAllSetting()){
            $allsetting = $setting->GetAllSetting();

            $email = $allsetting[0]['config_email'];
        }

    	if($_POST){
    		$questionaire_name 	   = $_POST['questionaire_name'];
    		$questionaire_email    = $_POST['questionaire_email'];
    		$questionaire_phone    = $_POST['questionaire_phone'];
    		$questionaire_question = $_POST['questionaire_question'];

    		$data = array(
    			'questionaire_name'		=> addslashes($questionaire_name),
    			'questionaire_email'	=> $questionaire_email,
    			'questionaire_phone'	=> $questionaire_phone,
    			'questionaire_question' => addslashes($questionaire_question)
    		);

    		$result = $question->InsertQuestion($data);
    		if($result){
                $mail             = new PHPMailer();
                $body             = "Email liên hệ từ: {$_POST['questionaire_name']} ({$_POST['questionaire_email']})\n<br>Nội dung: ".$_POST['questionaire_question'];
                $body             = preg_replace("/[\\\]/",'',$body);

                $mail->IsSMTP();
                $mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
                // 1 = errors and messages
                // 2 = messages only
                $mail->SMTPAuth   = true;                  // enable SMTP authentication
                $mail->SMTPSecure = "tls";                 // sets the prefix to the servier
                $mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
                $mail->Port       = 587;                   // set the SMTP port for the GMAIL server
                $mail->Username   = "contacticst123@gmail.com";  // GMAIL username
                $mail->Password   = "iloveu92";            // GMAIL password

                $mail->SetFrom($_POST['questionaire_email'], $_POST['questionaire_name']); //Định danh người gửi
                $mail->AddReplyTo($_POST['questionaire_email'],$_POST['questionaire_name']); //Định danh người sẽ nhận trả lời
                $mail->Subject    = $_POST['questionaire_question']; //Tiêu đề Mail
                $mail->AltBody    = "Để xem tin này, vui lòng bật tương thích chế độ hiển thị mã HTML!"; // optional, comment out and test
                $mail->MsgHTML($body);

                $address = $email; //Địa chỉ mail cần gửi tới
                $mail->AddAddress("contact@icst.org.vn","contact@icst.org.vn"); //Gửi tới ai ?
                if(!$mail->Send())
                {
                    echo "<script type='text/javascript'>
                    alert('Những thông tin bạn điền không đúng. !!');
                    </script>";
                }
                else
                {
                    header('Location:'.URL.'question/success/'.$result);
                }
    		}
    	}else{
    		header('Location:'.URL);
    	}
    }

    public function success($path){

    	$model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $partner = new model_partner();

        $library = new model_library();
		
		$video = new model_video();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOnByPostion('all','ngang')){
            $allmenufather = $model1->SelectAllMenuFatherOnByPostion('all','ngang');

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        if($model1->SelectAllMenuFatherOnByPostion('product','doc')){
            $allmenufatherproduct = $model1->SelectAllMenuFatherOnByPostion('product','doc');

            foreach($allmenufatherproduct as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufatherproduct[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenuproduct = $allmenufatherproduct;
        }

        if($advertise->GetAdvertiseByPosition(1)){
            $this->view->listadvertise1 = $advertise->GetAdvertiseByPosition(1);
        }

        if($advertise->GetAdvertiseByPosition(2)){
            $this->view->listadvertise2 = $advertise->GetAdvertiseByPosition(2);
        }

        if($advertise->GetAdvertiseByPosition(3)){
            $this->view->listadvertise3 = $advertise->GetAdvertiseByPosition(3);
        }

        if($news->SelectTopNewsView(4)){
            $this->view->listnewsview = $news->SelectTopNewsView(4);
        }

        if($news->SelectTopNewsRelease(4)){
            $this->view->listnewsrelease = $news->SelectTopNewsRelease(4);
        }

        if($linker->SelectAlllinker()){
            $this->view->listlinker = $linker->SelectAlllinker();
        }

        if($news->SelectTopNewsRelease(5)){
            $this->view->listnewsnew = $news->SelectTopNewsRelease(5);
        }
		
		if($video->SelectAllVideo()){
			$this->view->listvideo = $video->SelectAllVideo();
		}

        if($news->SelectAllNewsByCatId(6)){
            $newschuyenganh = $news->SelectAllNewsByCatId(6);

            foreach ($newschuyenganh as $key=>$value) {
                if($comment->NumCommentByPostId($value['news_id'])){
                    $numcomment = $comment->NumCommentByPostId($value['news_id']);
                    $newschuyenganh[$key]['news_comment'] = $numcomment[0]['numcomment'];
                }
            }

            $this->view->newschuyenganh = $newschuyenganh;
        }

        if($news->SelectAllNewsByCatId(7)){
            $newshoi = $news->SelectAllNewsByCatId(7);
            
            foreach ($newshoi as $key=>$value) {
                if($comment->NumCommentByPostId($value['news_id'])){
                    $numcomment = $comment->NumCommentByPostId($value['news_id']);
                    $newshoi[$key]['news_comment'] = $numcomment[0]['numcomment'];
                }
            }

            $this->view->newshoi = $newshoi;
        }

        if($question->SelectAllQuestion()){
            $this->view->listquestion = $question->SelectAllQuestion();
        }

        if($partner->SelectAllPartner()){
            $this->view->listpartner = $partner->SelectAllPartner();
        }

        if($library->SelectAllLibrary()){
            $this->view->listlibrary = $library->SelectAllLibrary();
        }

    	if($path){
    		if($question->GetQuestionById($path)){

    		}else{
    			header('Location:'.URL);
    		}
    	}else{
    		header('Location:'.URL);
    	}

    	$this->view->layout('layout');
		$this->view->Render('question/success');

		return true;
    }

    public function detail($path){
        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $partner = new model_partner();

        $library = new model_library();

        $event = new model_event();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOnByPostion('all','ngang')){
            $allmenufather = $model1->SelectAllMenuFatherOnByPostion('all','ngang');

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        if($model1->SelectAllMenuFatherOnByPostion('product','doc')){
            $allmenufatherproduct = $model1->SelectAllMenuFatherOnByPostion('product','doc');

            foreach($allmenufatherproduct as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufatherproduct[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenuproduct = $allmenufatherproduct;
        }

        if($advertise->GetAdvertiseByPosition(1)){
            $this->view->listadvertise1 = $advertise->GetAdvertiseByPosition(1);
        }

        if($advertise->GetAdvertiseByPosition(2)){
            $this->view->listadvertise2 = $advertise->GetAdvertiseByPosition(2);
        }

        if($advertise->GetAdvertiseByPosition(3)){
            $this->view->listadvertise3 = $advertise->GetAdvertiseByPosition(3);
        }

        if($news->SelectTopNewsView(4)){
            $this->view->listnewsview = $news->SelectTopNewsView(4);
        }

        if($news->SelectTopNewsRelease(4)){
            $this->view->listnewsrelease = $news->SelectTopNewsRelease(4);
        }

        if($linker->SelectAlllinker()){
            $this->view->listlinker = $linker->SelectAlllinker();
        }

        if($news->SelectTopNewsRelease(5)){
            $this->view->listnewsnew = $news->SelectTopNewsRelease(5);
        }

        if($news->SelectAllNewsByCatId(6)){
            $newschuyenganh = $news->SelectAllNewsByCatId(6);

            foreach ($newschuyenganh as $key=>$value) {
                if($comment->NumCommentByPostId($value['news_id'])){
                    $numcomment = $comment->NumCommentByPostId($value['news_id']);
                    $newschuyenganh[$key]['news_comment'] = $numcomment[0]['numcomment'];
                }
            }

            $this->view->newschuyenganh = $newschuyenganh;
        }

        if($news->SelectAllNewsByCatId(7)){
            $newshoi = $news->SelectAllNewsByCatId(7);
            
            foreach ($newshoi as $key=>$value) {
                if($comment->NumCommentByPostId($value['news_id'])){
                    $numcomment = $comment->NumCommentByPostId($value['news_id']);
                    $newshoi[$key]['news_comment'] = $numcomment[0]['numcomment'];
                }
            }

            $this->view->newshoi = $newshoi;
        }

        if($question->SelectAllQuestion()){
            $this->view->listquestion = $question->SelectAllQuestion();
        }

        if($partner->SelectAllPartner()){
            $this->view->listpartner = $partner->SelectAllPartner();
        }

        if($library->SelectAllLibrary()){
            $this->view->listlibrary = $library->SelectAllLibrary();
        }

        if($path){
            $part = explode('-',$path);
            $id = $part[count($part)-1];
            $result = $question->GetQuestionById($id);

            if($result){

                $this->view->title = $result[0]['questionaire_question'].'-'.$allsetting[0]['config_title'];
                $this->view->description = $result[0]['questionaire_question'].'-'.$allsetting[0]['config_description'];
                $this->view->keyword = $result[0]['questionaire_question'].','.$allsetting[0]['config_keyword'];

                $this->view->questioninfo = $result;
            }
        }else{
            header('Location:'.URL);
        }

        $this->view->layout('layout');
        $this->view->Render('question/detail');

        return true;
    }
}
