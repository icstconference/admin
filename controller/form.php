<?php
class controller_form extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    /*
     * Các function tạo hình ảnh
     */

    function UploadFile($data){
            if ($data["error"] == 0) {
                if($data["type"] == "application/pdf" || $data["type"] == "application/x-pdf" ){
                    $filetype = "pdf";
                }
                if($data["type"] == "application/msword"){
                    $filetype = "docx";
                }
                $path = "public/upload/file/" .$data['name'];
                move_uploaded_file($data["tmp_name"], $path);
                $array = array(
                    'name'  => $data['name'],
                    'type'  => $data['type'],
                    'size'  => $data['size'],
                    'path'  => $path
                );
                //return $path;
                return $array;
            }
    }

    protected function DeleteFile($file_path) {
        $path = str_replace(URL, '', $file_path);
        if (file_exists($path)) {
            unlink($path);
            return true;
        }
        return false;
    }

    public function categories(){
        if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login');
        }

        $form = new model_form();

        if($form->SelectAllFormCategories()){
            $this->view->listformcategories = $form->SelectAllFormCategories();
        }

        $this->view->js = array(
            URL.'public/js/formcat.js'
        );

        if($_POST){
            $form1_categories_name     = $_POST['form1_categories_name'];

            $arr = array(
                'form1_categories_name' => $form1_categories_name
            );

            $result = $form->InsertFormCategories($arr);

            if($result){
                header('Location:'.URL.'form/categories/');
            }
        }

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin');
        $this->view->Render('form/categories');
        return true;
    }

    public function editcategories($path){
        if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login');
        }

        $form = new model_form();

        if($path){
            if($form->GetFormCategoriesById($path)){
                $formcategoriesinfo = $form->GetFormCategoriesById($path);

                $this->view->formcategoriesinfo = $formcategoriesinfo;

                if($_POST){
                    $form1_categories_name     = $_POST['form1_categories_name'];

                    $arr = array(
                        'form1_categories_name' => $form1_categories_name
                    );

                    $result = $form->UpdateFormCategories($arr,$path);

                    if($result){
                        header('Location:'.URL.'form/categories/');
                    }
                }
            }else{
                header('Location:'.URL.'form/categories/');
            }
        }else{
            header('Location:'.URL.'form/categories/');
        }

        $this->view->js = array(
            URL.'public/js/formcat.js'
        );

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin');
        $this->view->Render('form/editcategories');
        return true;
    }

    public function deletecategories($path){
        if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login');
        }

        $form = new model_form();

        if($path){
            if($form->GetFormCategoriesById($path)){
                $deletecategories = $form->DeleteFormCategories($path);

                if($deletecategories){
                    header('Location:'.URL.'form/categories/');
                }
            }else{
                header('Location:'.URL.'form/categories/');
            }
        }else{
            header('Location:'.URL.'form/categories/');
        }

        
    }

    public function index(){
    	if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login');
        }

        $form = new model_form();
        $news = new model_news();

        if($news->SelectAllCategoriesNews()){
            $this->view->listcategories = $news->SelectAllCategoriesNews();
        } 

        if($form->SelectAllForm()){
            $this->view->listform = $form->SelectAllForm();
        }
		
		$model1 = new model_general();

        $list_permission = '';
        
        if($model1->GetPermissionNewsByUserId($_SESSION['user_id'])){
            $listpermission = $model1->GetPermissionNewsByUserId($_SESSION['user_id']);
            
            foreach($listpermission as $ls){
                $list_permission .= ','.$ls['categories_id'];
            }
        }
        
        $this->view->listpermission = $list_permission;

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/form.js'
        );

        if($_POST){
            $form_name                  = $_POST['form_name'];
            $form_name_english          = $_POST['form_name_english'];
            $form_description           = $_POST['form_description'];
            $form_description_english   = $_POST['form_description_english'];
            $form_type                  = $_POST['form_type'];
            $form_categories_id         = $_POST['form_categories_id'];

            $arr = array(
                'form_name'                 => addslashes($form_name),
                'form_name_english'         => addslashes($form_name_english),
                'form_description'          => addslashes($form_description),
                'form_description_english'  => addslashes($form_description_english),
                'form_type'                 => $form_type,
                'form_categories_id'        => $form_categories_id
            );

            $result = $form->InsertForm($arr);

            if($result){
                header('Location:'.URL.'form/editform/'.$result);
            }
        }

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin');
        $this->view->Render('form/index');
        return true;
    }

    public function createform(){
        if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login');
        }

        $form = new model_form();

        if($_POST){

            $form1_author          = $_POST['form1_author'];
            $form1_title           = $_POST['form1_title'];
            $form1_journal         = $_POST['form1_journal'];
            $form1_year            = $_POST['form1_year'];
            $form1_category_id     = $_POST['form1_category_id'];
            $form1_url             = $_POST['form1_url'];
            $form_id               = $_POST['form_id'];
            $form1_file            = '';

            if(strlen($_FILES['form1_file']['name'])>1){
                $upload_file = $this->UploadFile($_FILES['form1_file']);

                if($upload_file){
                    $form1_file = $upload_file['path'];
                }
            }


            $arr = array(
                'form1_author'          => $form1_author,
                'form1_title'           => $form1_title,
                'form1_journal'         => $form1_journal,
                'form1_year'            => $form1_year,
                'form1_category_id'     => $form1_category_id,
                'form1_file'            => $form1_file,
                'form1_url'             => $form1_url,
                'form_id'               => $form_id
            );

            $result = $form->InsertForm1($arr);

            if($result){
                echo $form_id;
            }
        }
    }

    public function createform1(){
        if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login');
        }

        $form = new model_form();

        if($_POST){

            $form2_date          = $_POST['form2_date'];
            $form2_hours         = $_POST['form2_hours'];
            $form2_topic         = $_POST['form2_topic'];
            $form2_topic_english = $_POST['form2_topic_english'];
            $form2_people        = $_POST['form2_people'];
            $form_id             = $_POST['form_id'];

            $arr = array(
                'form2_date'            => $form2_date,
                'form2_hours'           => $form2_hours,
                'form2_topic'           => $form2_topic,
                'form2_topic_english'   => $form2_topic_english,
                'form2_people'          => $form2_people,
                'form_id'               => $form_id
            );

            $result = $form->InsertForm2($arr);

            if($result){
                echo $form_id;
            }
        }
    }

    public function deleteform1($path){
        if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login');
        }

        $form = new model_form();

        if($path){
            if($form->GetForm1ById($path)){
                $form1info = $form->GetForm1ById($path);

                $deleteform1 = $form->DeleteForm1($path);

                if($deleteform1){
                    $this->DeleteFile($form1info[0]['form1_file']);
                    header('Location:'.URL.'form/editform/'.$form1info[0]['form_id']);
                }
            }else{
                header('Location:'.URL.'form');
            }
        }else{
            header('Location:'.URL.'form');
        }
    }

    public function deleteform2($path){
        if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login');
        }

        $form = new model_form();

        if($path){
            if($form->GetForm2ById($path)){
                $form2info = $form->GetForm2ById($path);

                $deleteform2 = $form->DeleteForm2($path);

                if($deleteform2){
                    header('Location:'.URL.'form/editform/'.$form2info[0]['form_id']);
                }
            }else{
                header('Location:'.URL.'form');
            }
        }else{
            header('Location:'.URL.'form');
        }
    }
	
	public function deleteform($path){
		if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login');
        }

        $form = new model_form();

        if($path){
            if($form->GetFormById($path)){
                $forminfo = $form->GetFormById($path);

                $deleteform = $form->DeleteForm($path);

                if($deleteform){
					if($forminfo[0]['form_type'] == 1){
						$listform1 = $form->GetForm1ByIdForm($path);
						
						if($form->GetForm1ByIdForm($path)){
							foreach($listform1 as $fr){
								$form1file = $fr['form1_file'];
								$deleteform1 = $form->DeleteForm1($fr['form1_id']);
								$this->DeleteFile($form1file);
							}
						}
					}else{
						$listform2 = $form->GetForm2ByIdForm($path);
						
						if($form->GetForm2ByIdForm($path)){
							foreach($listform2 as $fr){
								$form1file = $fr['form1_file'];
								$deleteform1 = $form->DeleteForm1($fr['form1_id']);
								$this->DeleteFile($form1file);
							}
						}
					}
					
                    header('Location:'.URL.'form/');
                }
            }else{
                header('Location:'.URL.'form');
            }
        }else{
            header('Location:'.URL.'form');
        }
	}

    public function editform1($path){
        if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login');
        }

        $form = new model_form();

        if($form->SelectAllFormCategories()){
            $this->view->listformcategories = $form->SelectAllFormCategories();
        }

        if($path){
            if($form->GetForm1ById($path)){
                $forminfo = $form->GetForm1ById($path);

                $this->view->forminfo = $forminfo;

                if($_POST){
                    $form1_author          = $_POST['form1_author'];
                    $form1_title           = $_POST['form1_title'];
                    $form1_journal         = $_POST['form1_journal'];
                    $form1_year            = $_POST['form1_year'];
                    $form1_category_id     = $_POST['form1_category_id'];
                    $form1_url             = $_POST['form1_url'];


                    $arr = array(
                        'form1_author'          => $form1_author,
                        'form1_title'           => $form1_title,
                        'form1_journal'         => $form1_journal,
                        'form1_year'            => $form1_year,
                        'form1_category_id'     => $form1_category_id,
                        'form1_url'             => $form1_url
                    );

                    $result = $form->UpdateForm1($arr,$path);

                    if($result){
                        header('Location:'.URL.'form/editform/'.$forminfo[0]['form_id']);
                    }
                }
            }
        }else{
            header('Location:'.URL.'form/');
        }

        $this->view->js = array(
            URL.'public/js/user.js',
        );

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin');
        $this->view->Render('form/editform1');
        return true;
    }

    public function editform2($path){
        if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login');
        }

        $form = new model_form();

        if($path){
            if($form->GetForm2ById($path)){
                $forminfo = $form->GetForm2ById($path);

                $this->view->forminfo = $forminfo;

                if($_POST){
                    $form2_date          = $_POST['form2_date'];
                    $form2_hours         = $_POST['form2_hours'];
                    $form2_topic         = $_POST['form2_topic'];
                    $form2_topic_english = $_POST['form2_topic_english'];
                    $form2_people        = $_POST['form2_people'];

                    $arr = array(
                        'form2_date'            => $form2_date,
                        'form2_hours'           => $form2_hours,
                        'form2_topic'           => $form2_topic,
                        'form2_topic_english'   => $form2_topic_english,
                        'form2_people'          => $form2_people,
                    );

                    $result = $form->UpdateForm2($arr,$path);

                    if($result){
                        header('Location:'.URL.'form/editform/'.$forminfo[0]['form_id']);
                    }
                }
            }
        }else{
            header('Location:'.URL.'form/');
        }

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin');
        $this->view->Render('form/editform2');
        return true;
    }

    public function editform($path){
        if(lib_session::Get('user_type') != 'admin'){
            header('Location:'.URL.'login');
        }

        $form = new model_form();

        $news = new model_news();

        if($news->SelectAllCategoriesNews()){
            $this->view->listcategories = $news->SelectAllCategoriesNews();
        }

        if($form->SelectAllFormCategories()){
            $this->view->listformcategories = $form->SelectAllFormCategories();
        }

		$model1 = new model_general();

        $list_permission = '';
        
        if($model1->GetPermissionNewsByUserId($_SESSION['user_id'])){
            $listpermission = $model1->GetPermissionNewsByUserId($_SESSION['user_id']);
            
            foreach($listpermission as $ls){
                $list_permission .= ','.$ls['categories_id'];
            }
        }
        
        $this->view->listpermission = $list_permission;
		
        if($path){
            if($form->GetFormById($path)){
                $forminfo = $form->GetFormById($path);

                $this->view->forminfo = $forminfo;

                if($form->SelectAllForm1ByFormId($path)){
                    $this->view->listform1 = $form->SelectAllForm1ByFormId($path);
                }

                if($form->SelectAllForm2ByFormId($path)){
                    $this->view->listform2 = $form->SelectAllForm2ByFormId($path);
                }

                if($_POST){
                    $form_name                  = $_POST['form_name'];
                    $form_name_english          = $_POST['form_name_english'];
                    $form_description           = $_POST['form_description'];
                    $form_description_english   = $_POST['form_description_english'];
                    $form_type                  = $_POST['form_type'];
                    $form_categories_id         = $_POST['form_categories_id'];

                    $arr = array(
                        'form_name'                 => addslashes($form_name),
                        'form_name_english'         => addslashes($form_name_english),
                        'form_description'          => addslashes($form_description),
                        'form_description_english'  => addslashes($form_description_english),
                        'form_type'                 => $form_type,
                        'form_categories_id'        => $form_categories_id
                    );

                    $result = $form->UpdateForm($arr,$path);

                    if($result){
                        header('Location:'.URL.'form/');
                    }
                }
            }
        }else{
            header('Location:'.URL.'form/');
        }

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/form.js',
        );

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin');
        $this->view->Render('form/editform');
        return true;
    }


}