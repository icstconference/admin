<?php
class controller_cart extends lib_controller
{
    public function __construct() {
        parent::__construct();
    }

    public function index(){
    	$model = new model_cart();

        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        if($allsetting){
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
            if($model1->SelectAllMenuFatherOnByPostion('all','ngang')){
                $allmenufather = $model1->SelectAllMenuFatherOnByPostion('all','ngang');

                foreach($allmenufather as $key=>$menu){
                    if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                        $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                    }
                }

                $this->view->allmenu = $allmenufather;
            }

        $model3 = new model_product();

        if($model3->GetAllCategoriesProduct()){
            $this->view->listcategoriesproduct = $model3->GetAllCategoriesProduct();
        }

        $model4 = new model_news();

        if($model4->SelectAllCategoriesNews()){
            $this->view->listcategoriesnews = $model4->SelectAllCategoriesNews();
        }

        if($model4->SelectTopNews(5)){
            $this->view->topnew = $model4->SelectTopNews(5);
        }

        $this->view->title = 'Giỏ hàng'.'-'.$allsetting[0]['config_title'];
        $this->view->description = 'Giỏ hàng'.'-'.$allsetting[0]['config_description'];
        $this->view->keyword = 'Giỏ hàng'.','.$allsetting[0]['config_keyword'];

        $this->view->js = array(
		  URL.'public/js/add-cart.js'
		);

	    $this->view->layout('layout');
	    $this->view->Render('cart/index');
	    return true;
    }

    public function checkout(){
        $cart = new model_cart();

        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        if($allsetting){
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
            if($model1->SelectAllMenuFatherOnByPostion('all','ngang')){
                $allmenufather = $model1->SelectAllMenuFatherOnByPostion('all','ngang');

                foreach($allmenufather as $key=>$menu){
                    if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                        $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                    }
                }

                $this->view->allmenu = $allmenufather;
            }

        $this->view->title = 'Thanh toán'.'-'.$allsetting[0]['config_title'];
        $this->view->description = 'Thanh toán'.'-'.$allsetting[0]['config_description'];
        $this->view->keyword = 'Thanh toán'.','.$allsetting[0]['config_keyword'];

        if($_POST){
            $fullname       = $_POST['fullname'];
            $email          = $_POST['email'];
            $address        = $_POST['address'];
            $phone          = $_POST['phone'];
            $information    = $_POST['information'];
            $summoney = intval($_POST['summoney']);
            $required = '<table>
                <tr>
                    <th>Sản phẩm</th>
                    <th>Số lượng</th>
                    <th>Kích cỡ</th>
                    <th>Giới tính</th>
                    <th>Loại</th>
                </tr>
                ';

            if(count($_SESSION['cart']['don']) > 0){
                foreach ($_SESSION['cart']['don'] as $value) {
                    $required.='<tr>'.
                        '<td>'.$value[0]['product_name'].'</td>'.
                        '<td style="text-align:center;">'.$value[0]['quantity'].'</td>'.
                        '<td style="text-align:center;">'.$value[0]['size'].'</td>'.
                        '<td style="text-align:center;">'.$value[0]['sex'].'</td>'.
                        '<td> Áo đơn </td>'.
                        '</tr>';
                }
            }

            if(count($_SESSION['cart']['doi']) > 0){
                foreach ($_SESSION['cart']['doi'] as $value) {
                    $required.='<tr>'.
                        '<td>'.$value[0]['product_name'].'</td>'.
                        '<td style="text-align:center;">'.$value[0]['quantity'].'</td>'.
                        '<td style="text-align:center;">'.$value[0]['size'].'</td>'.
                        '<td style="text-align:center;">'.$value[0]['sex'].'</td>'.
                        '<td> Áo đơn </td>'.
                        '</tr>';
                }
            }

            if(count($_SESSION['cart']['giadinh']) > 0){
                foreach ($_SESSION['cart']['giadinh'] as $value) {
                    $required.='<tr>'.
                        '<td>'.$value[0]['product_name'].'</td>'.
                        '<td style="text-align:center;">'.$value[0]['quantity'].'</td>'.
                        '<td style="text-align:center;">'.$value[0]['size'].'</td>'.
                        '<td style="text-align:center;">'.$value[0]['sex'].'</td>'.
                        '<td> Áo đơn </td>'.
                        '</tr>';
                }
            }

            $required .= '</table>';

            if(strlen($fullname) > 0 && strlen($email) > 0 && strlen($address) > 0 && strlen($phone) > 0){
                $data = array(
                    'order_name'        => addslashes($fullname),
                    'order_email'       => addslashes($email),
                    'order_address'     => addslashes($address),
                    'order_mobile'      => addslashes($phone),
                    'order_summoney'    => $summoney,
                    'order_require'     => addslashes($required),
                    'order_information' => addslashes($information)
                ); 

                $result = $cart->InsertCart($data);

                if($result){
                    unset($_SESSION['cart']);
                    unset($_SESSION['cart']['don']);
                    unset($_SESSION['cart']['doi']);
                    unset($_SESSION['cart']['giadinh']);
                    header('Location:'.URL.'cart/successcheckout/'.$result);
                }
            }
        }
            

        $this->view->layout('layout');
        $this->view->Render('cart/checkout');
        return true;
    }

    public function successcheckout($path){
        $cart = new model_cart();   

        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        if($allsetting){
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
            if($model1->SelectAllMenuFatherOnByPostion('all','ngang')){
                $allmenufather = $model1->SelectAllMenuFatherOnByPostion('all','ngang');

                foreach($allmenufather as $key=>$menu){
                    if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                        $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                    }
                }

                $this->view->allmenu = $allmenufather;
            }

        $this->view->title = 'Thanh toán'.'-'.$allsetting[0]['config_title'];
        $this->view->description = 'Thanh toán'.'-'.$allsetting[0]['config_description'];
        $this->view->keyword = 'Thanh toán'.','.$allsetting[0]['config_keyword'];

        if($path){
            if($cart->SelectCart($path)){
                $this->view->layout('layout');
                $this->view->Render('cart/success');
                return true;
            }else{
                header('Location:'.URL);
            }
        }else{
            header('Location:'.URL);
        }
    }
}