<?php
class controller_login extends lib_controller{

    private $facebook;
    private $f_user;
    private $openid;

    public function __construct() {
        parent::__construct();
    }

    public function index(){
        $login = new model_login();
        $model = new model_setting();
		$permission = new model_general();

        $allsetting = $model->GetAllSetting();

        if($allsetting){
            $this->view->title = 'Trang đăng nhập'.' - '.$allsetting[0]['config_title'];
            $this->view->description = 'Trang đăng nhập'.' - '.$allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        if($_SESSION['user_id']){
            if($_SESSION['user_type'] != 0){
                header('Location:'.URL.'admin/');
            }else{
                header('Location:'.URL);
            }
        }else{
            if(isset($_POST['login'])){
                $username = $_POST['username'];
                $password = $_POST['password'];

                if (empty($_POST['username']) || empty($_POST['password'])) {
                    $this->view->error = '* Tên đăng nhập hoặc mật khẩu không đúng';
                }else{
                    $username_data = $login->GetUserbyUsername($username);
                    if($username_data){
                        $salt_data = $username_data[0]['salt'];
                        $pass_data = $username_data[0]['user_password'];
                        $pass_login = md5($password.$salt_data);

                        if($pass_login == $pass_data){
                            $groupid = $username_data[0]['user_group_id'];
                            if($groupid != 0){
    							$create = $permission->GetAllPermissionByUserId1($username_data[0]['user_group_id'],'create');                        
    							$delete = $permission->GetAllPermissionByUserId1($username_data[0]['user_group_id'],'delete');                        
    							$edit   = $permission->GetAllPermissionByUserId1($username_data[0]['user_group_id'],'edit');                        
    							if($create){                            
    								lib_session::Set('user_permission_create',$create);
    							}                        
    							if($delete){
    								lib_session::Set('user_permission_delete',$delete);
    							}
    							if($edit){
    								lib_session::Set('user_permission_edit',$edit);
    							}
                            }else{
                                lib_session::Set('user_permission_create','');
                                lib_session::Set('user_permission_delete','');
                                lib_session::Set('user_permission_edit','');
                            }
                            $login->SetSession($username_data[0]);
                            
                            if(isset($_POST['remember']))
                            {
                                $login->SetCookie($username_data[0]);
                            }
                            if($_SESSION['user_type'] != 0){
                                header('Location:'.URL.'admin/');
                            }else{
                                header('Location:'.URL);
                            }
                        }else{
                            $this->view->error = '* Password bạn nhập không đúng';
                        }
                    }else{
                        $this->view->error = '* Username bạn nhập không tồn tại';
                    }
                }
            }
        }

        $this->view->layout('layout-admin-login1');
        $this->view->Render('login/index');
        return true;
    }

    //login with google
    protected function Google() {
        //include google api files
        require_once 'plugin/google-login-api/Google_Client.php';
        require_once 'plugin/google-login-api/contrib/Google_Oauth2Service.php';
        $gClient = new Google_Client();
        $gClient->setApplicationName(NAME);
        $gClient->setApprovalPrompt('auto');
        $gClient->setClientId(G_CLIENT_ID);
        $gClient->setClientSecret(G_CLIENT_SECRET);
        $gClient->setRedirectUri(G_REDIRECT_URL);
        $gClient->setDeveloperKey(G_DEVELOPER_KEY);

        $google_oauthV2 = new Google_Oauth2Service($gClient);
        $this->view->g_login_url = null;
        //If user wish to log out, we just unset Session variable
        if (isset($_REQUEST['reset'])) {
            unset($_SESSION['token']);
            $gClient->revokeToken();
            header('Location: ' . filter_var(G_REDIRECT_URL, FILTER_SANITIZE_URL)); //redirect user back to page
        }

        if (isset($_GET['code'])) {
            $result = $gClient->authenticate($_GET['code']);
            if ($result) {
                $_SESSION['token'] = $gClient->getAccessToken();
                header('Location: ' . filter_var(G_REDIRECT_URL, FILTER_SANITIZE_URL));
                return;
            }
        }


        if (isset($_SESSION['token'])) {
            $gClient->setAccessToken($_SESSION['token']);
        }


        if ($gClient->getAccessToken()) {
            //For logged in user, get details from google using access token
            $user = $google_oauthV2->userinfo->get();
            $user_id = $user['id'];
            $user_name = filter_var($user['name'], FILTER_SANITIZE_SPECIAL_CHARS);
            $email = filter_var($user['email'], FILTER_SANITIZE_EMAIL);
            $profile_url = filter_var($user['link'], FILTER_VALIDATE_URL);
            $profile_image_url = filter_var($user['picture'], FILTER_VALIDATE_URL);
            $personMarkup = "<div><img src='$profile_image_url?sz=50'></div>";
            $_SESSION['token'] = $gClient->getAccessToken();
        } else {
            //For Guest user, get google login url
            $authUrl = $gClient->createAuthUrl();
        }

        if (isset($authUrl)) { //user is not logged in, show login button
            $this->view->g_login_url = $authUrl;
        } else { // user logged in

            $model=new model_login1();
            $check_login = $model->LoginGYF($user['email']);

            if ($check_login == false) {
                $user_name = preg_split('/@/', $user['email']);
                $data = array(
                    'user_full_name' => $user['name'],
                    'user_email' => $user['email'],
                    'user_sex' => '',
                    'user_name' => $user_name[0],
                    'user_avatar_small' => URL_AVATAR_SMALL,
                    'user_avatar_big' => URL_AVATAR_BIG,
                    'user_verified' => '1',
                    'user_birthday' => ''
                );
                $check_user_name = $model->CheckUserName($user_name[0]);
                if ($check_user_name) {
                    $data['user_name'] = $this->FixUserName($user_name[0]);
                }
                $singup = $model->SignupGY($data);
                if ($singup) {
                    //$this->SendMailNewUser($data);
                    $check_login = $model->LoginGYF($user['email']);
                    if (!$check_login) {
                        $this->view->error = $this->view->translate->__('Error, please try again');
                        return false;
                    }
                }
            }
            $this->view->error = $this->view->translate->__('Error, please try again');
            return false;
        }
    }

    /*
     * Facebook login
     */

    protected function Facebook() {
        include 'plugin/facebook-php-sdk/src/facebook.php';
        $this->FInit();
        $user = $this->FGetUser();
        $this->view->f_login_url = null;
        $this->view->f_logout_url = null;
        if ($user) {
            $f_user_info = $this->FGetUserProfile();
            //user info facebook
            $user_name = preg_split('/@/', $f_user_info['email']);
            $user_name = $user_name[0];
            $data = array(
                'user_full_name' => $f_user_info['last_name'] . ' ' . $f_user_info['first_name'],
                'user_email' => $f_user_info['email'],
                'user_birthday' => '',
                'user_sex' => $f_user_info['gender'],
                'user_name' => $user_name,
                'user_id' => $f_user_info['id'],
                'user_bio' => '',
                'user_verified' => $f_user_info['verified'],
                'user_link' => $f_user_info['link']
            );
            //login with facebook
            $model = new model_login1();
            $check_login = $model->LoginGYF($data['user_email']);

            //new user
            if ($check_login == FALSE) {
                $new_user_id = $model->CheckUserName($data['user_name']);
                if ($new_user_id) {
                    //fix username insert database
                    $data['user_name'] = $this->FixUserName($data['user_name']);
                }
                print_r($data);

                $new_user_id = $model->SignupFacebook($data);
                if (!$new_user_id) {
                    $this->view->error = $this->view->translate->__('Error, please try again');
                    return false;
                }
                //get and save avatar user facebook
                $url = $this->FGetUrlAvatar($data['user_id']);
                $result = $this->FSaveAvatar($url, $new_user_id);
                $avatar_big = URL . 'avatar/big/' . $new_user_id . '.jpg';
                $avatar_small = URL . 'avatar/small/' . $new_user_id . '.jpg';
                //update info new user
                $data_update = array(
                    'user_avatar_big' => $avatar_big,
                    'user_avatar_small' => $avatar_small,
                    'new_user_id' => $new_user_id
                );
                $result = $model->CompleteSignupFacebook($data_update);
                if ($result) {
                    //$this->SendMailNewUser($data);
                    $result = $model->LoginGYF($data['user_email']);
                    if (!$result) {
                        $this->view->error = $this->view->translate->__('Error, please try again');
                        return false;
                    }
                }
            }
        } else {
            $this->view->f_login_url = $this->FGetLoginUrl();
        }
    }

    /*
     * Facebook save avatar
     */

    protected function FSaveAvatar($url, $user_id) {
        include 'plugin/phpthumb/ThumbLib.inc.php';
        try {
            $thumb = PhpThumbFactory::create($url);
        } catch (Exception $e) {
            // handle error here however you'd like
        }
        $path_save = 'avatar/big/' . $user_id . '.jpg';
        $result = $thumb->save($path_save, 'jpg');

        $thumb->resize(60, 60);
        $path_save = 'avatar/small/' . $user_id . '.jpg';
        $thumb->save($path_save, 'jpg');
    }

    /*
     * Facebook get url avatar
     */

    protected function FGetUrlAvatar($f_user_id) {
        $url = 'http://graph.facebook.com/' . $f_user_id . '/picture?redirect=false&width=160&height=160';
        $content = file_get_contents($url);

        $img = json_decode($content);
        $img_t = preg_replace('/https/', 'http', $img->data->url);
        return $img_t;
    }

    /*
     * Install facebook
     */

    protected function FInit() {
        $this->facebook = new Facebook(array(
            'appId' => FACEBOOK_APPID,
            'secret' => FACEBOOK_SECRET,
        ));
        return true;
    }

    /*
     * Delete facebook
     */

    protected function FDel() {
        unset($this->facebook);
        unset($this->f_user);
    }

    /*
     * Check user facebook
     */

    protected function FGetUser() {
        $this->f_user = $this->facebook->getUser();
        return $this->f_user;
    }

    /*
     * Get link login facebook
     */

    protected function FGetLoginUrl() {
        $login_url = $this->facebook->getLoginUrl(array('scope' => 'email,public_profile,user_friends'));
        return $login_url;
    }

    /*
     * Get link logout facebook
     */

    protected function FGetLogoutUrl() {
        $login_url = $this->facebook->getLogoutUrl();
        return $login_url;
    }

    /*
     * Get user info facebook
     */

    protected function FGetUserProfile() {
        try {
            // Proceed knowing you have a logged in user who's authenticated.
            $user_profile = $this->facebook->api('/me');
        } catch (FacebookApiException $e) {
            error_log($e);
            $this->f_user = null;
        }
        return $user_profile;
    }
}