<?php

class controller_account extends lib_controller {

    function __construct() {
        parent::__construct();
    }

    /*
     * function use
     */

    public function GetUser() {
        $model = new model_account();
        $this->view->user = $model->GetUser();
        return $this->view->user;
    }

    function UploadImage(array $data) {
        $temp = explode(".", $data["file"]["name"]);
        $extension = end($temp);
        if ((($data["file"]["type"] == "image/gif") || ($data["file"]["type"] == "image/jpeg") 
		|| ($data["file"]["type"] == "image/jpg") || ($data["file"]["type"] == "image/pjpeg") 
		|| ($data["file"]["type"] == "image/x-png") || ($data["file"]["type"] == "image/png")) 
		&& ($data["file"]["size"] < 4000000)) {
            if ($data["file"]["error"] == 0) {
                $path = "public/upload/" . uniqid() . '.' . $extension;
                move_uploaded_file($data["file"]["tmp_name"], $path);
                return $path;
            }
        }
        return false;
    }

    function SaveThumbImage($path, $user_id) {
        try {
            $thumb = PhpThumbFactory::create($path);
        } catch (Exception $e) {
            // handle error here however you'd like
        }
        $path_big = URL_SAVE_BIG;
        $path_small = URL_SAVE_SMALL;
        $id=  uniqid();
        $name_file = $user_id . '_'.$id.'.jpg';
        $thumb->resize(400);
        $thumb->adaptiveResize(160, 160);
        $thumb->save($path_big . $name_file, 'jpg');

        $thumb->resize(60);
        $thumb->save($path_small . $name_file, 'jpg');
        $path_result = array(
            'thumb' => 'public/images/avatar/small/' . $name_file,
            'big' => 'public/images/avatar/big/' . $name_file
        );
        return $path_result;
    }

    public function Index() {
        $this->Location($this->Actual_Link());
        $this->view->error = null;
        $this->view->success = null;
        $result = null;
        $model = new model_account();
        if (isset($_POST['btn_account'])) {            
            $result = $model->Index();
        }
        if ($result) {
            lib_session::Set('user_full_name',$_POST['full_name']);
            $this->view->success = 'Success';
        } else if ($result === false) {
            $this->view->error = 'Lỗi, yêu cầu bạn kiểm tra lại';
        }
        /*lib_session::Set('user_full_name',
         * get user info
         */
        $this->GetUser();
        /*if($result){
                        lib_session::Set('user_avatar_small',$data['user_avatar_small']);
                }
         * birthday
         */
        $model->ChangeUserFullname(lib_session::Get('user_id'));
        $this->view->title = 'Account';
        $this->view->layout('layout-dashboard');
        $this->view->Render('account/index');
    }

    public function ChangePass() {
        $this->Location($this->Actual_Link());
        $this->view->error = null;
        $this->view->success = null;
        $result = null;
        /*
         * change info
         */
        $model = new model_account();
        if (isset($_POST['btn_change_pass'])) {
            if (empty($_POST['pass']) && empty($_POST['new_pass'])) {
                $this->view->error = 'Please complete all information';
            } else {
                $result = $model->ChangePass();
                if ($result == 1) {
                    $this->view->success = 'Success';
                } else if ($result == -1) {
                    $this->view->error = 'Old password wrong, please try again';
                }
            }
        }

        $this->view->title = $this->view->translate->__('Change password');
        $this->view->layout('layout-dashboard');
        $this->view->Render('account/change-pass', array(
            'account/menu'
        ));
    }

    public function ChangeEmail() {
        $this->Location($this->Actual_Link());
        $result = null;
        $this->view->error = null;
        $this->view->success = null;
        $model = new model_account();
        if (isset($_POST['btn_change_email'])) {
            if (empty($_POST['email']) && empty($_POST['pass'])) {
                $this->view->error = 'Please complete all information';
            } else {
                $result = $model->ChangeEmail();
            }
        }
        if ($result == -1) {
            $this->view->error = 'Password wrong, please try again';
        } else if ($result == -2) {
            $this->view->error = 'Email registered';
        } else if ($result == 1) {
            $this->view->success = 'Success';
        }
        /*
         * user info
         */
        $this->GetUser();
        
        $this->view->title = $this->view->translate->__('Change email');
        $this->view->layout('layout-dashboard');
        $this->view->Render('account/change-email');
    }

    public function ChangeAvatar() {
        $this->Location($this->Actual_Link());
        $result = null;
        $this->view->error = null;
        $this->view->success = null;
        $user = $this->GetUser();
        $model = new model_account();
        if (isset($_POST['btn_avatar'])) {
            require_once 'plugin/phpthumb/ThumbLib.inc.php';
            $upload_img = $this->UploadImage($_FILES);
            if ($upload_img != false) {
                $path_save_img = $this->SaveThumbImage($upload_img, lib_session::Get('user_id'));
                $data['user_avatar_small'] = URL.$path_save_img['thumb'];
                $data['user_avatar_big'] = URL.$path_save_img['big'];
                if($user){
                    $avatar_tmp = preg_split('/\//', $user[0]['user_avatar_big']);
                    $avatar_b = 'avatar/big/'.end($avatar_tmp);
                    if(file_exists($avatar_b)){
                        unlink($avatar_b);
                    }
                    $avatar_tmp = preg_split('/\//', $user[0]['user_avatar_small']);
                    $avatar_s = 'avatar/small/'.end($avatar_tmp);
                    if(file_exists($avatar_s)){
                        unlink($avatar_s);
                    }
                    
                }
                $result = $model->ChangeAvatar($data);
                if($result){
                        lib_session::Set('user_avatar_small',$data['user_avatar_small']);
                        lib_session::Set('user_avatar_big',$data['user_avatar_big']);
                }
                $this->view->success = 'Success';
            } else {
                $this->view->error = 'Error, please try again';
            }
        }
        
        $this->view->title = $this->view->translate->__('Change avatar');
        $this->view->layout('layout-dashboard');
        $this->view->Render('account/change-avatar');
    }

}

?>
