<?php
class controller_slider extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    /*
     * Các function tạo hình ảnh
     */

    function UploadImage(array $data) {
        if ((($data["type"] == "image/gif") || ($data["type"] == "image/jpeg")
                || ($data["type"] == "image/jpg") || ($data["type"] == "image/pjpeg")
                || ($data["type"] == "image/x-png") || ($data["type"] == "image/png"))
            && ($data["size"] < 4000000)) {
            if ($data["error"] == 0) {
                $path = "public/upload/images/" . uniqid() . '.jpg';
                move_uploaded_file($data["tmp_name"], $path);
                return $path;
            }
        }
        return false;
    }

    function SaveThumbImage($path) {
        try {
            $thumb = PhpThumbFactory::create($path);
        } catch (Exception $e) {
            // handle error here however you'd like
        }
        $id=  uniqid();
        $name_file = $id.'.jpg';
        $thumb->save('public/upload/images/slider/' . $name_file, 'jpg');

        $path_result = array(
            'thumb' => 'public/upload/images/slider/' . $name_file,
        );
        return $path_result;
    }

    public function deleteslider($path){
        $model = new model_slider();

        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($delete[0]['permission_detail'],'slider')){
                header('Location:'.URL.'admin/');
            }
        }

        if($path){
            if($model->SelectSliderById($path)){
                $delete = $model->DeleteSlider($path);
                if($delete){
                    header('Location:'.URL.'slider');
                }
            }else{
                header('Location:'.URL.'slider');
            }
        }else{
            header('Location:'.URL.'slider');
        }
    }

    public function updateslider(){
        require_once 'plugin/phpthumb/ThumbLib.inc.php';
        $model = new model_slider();
        if($_POST){
            $slider_id      = $_POST['slider_id'];
            $sliderinfo     = $model->SelectSliderById($slider_id);
            $slider_caption         = $_POST['slider_caption'];
            $slider_title           = $_POST['slider_title'];
            $slider_title_english   = $_POST['slider_title_english'];
            $slider_caption_english = $_POST['slider_caption_english'];
            $slider_url             = $_POST['slider_url'];
            $slider_type            = $_POST['slider_type'];
            if($_POST['slider_status']){
                $slider_status = 'on';
            }else{
                $slider_status = 'off';
            }

            if(strlen($_FILES['slider_img']['name'])>1){
                $upload_img = $this->UploadImage($_FILES['slider_img']);
                $path_save_img = $this->SaveThumbImage($upload_img);

                if($path_save_img){
                    $slider_img = URL.$path_save_img['thumb'];
                }else{
                    $slider_img = $sliderinfo[0]['slider_img'];
                }
            }else{
                $slider_img = $sliderinfo[0]['slider_img'];
            }

            $data = array(
                'slider_img'            => $slider_img,
                'slider_title'          => $slider_title,
                'slider_title_english'  => $slider_title_english,
                'slider_caption'        => $slider_caption,
                'slider_caption_english'=> $slider_caption_english,
                'slider_url'            => $slider_url,
                'slider_status'         => $slider_status,
                'slider_type'           => $slider_type
            );

            $result = $model->UpdateSlider($data,$slider_id);
            if($result){
                echo 1;
            }
        }
    }

    public function createslider(){
        require_once 'plugin/phpthumb/ThumbLib.inc.php';
        $model = new model_slider();
        if($_POST){
            $slider_caption         = $_POST['slider_caption'];
            $slider_title           = $_POST['slider_title'];
            $slider_url             = $_POST['slider_url'];
            $slider_type            = $_POST['slider_type'];
            $slider_title_english   = $_POST['slider_title_english'];
            $slider_caption_english = $_POST['slider_caption_english'];

            if($_POST['slider_status']){
                $slider_status = 'on';
            }else{
                $slider_status = 'off';
            }

            if(strlen($_FILES['slider_img']['name'])>1){
                $upload_img = $this->UploadImage($_FILES['slider_img']);
                $path_save_img = $this->SaveThumbImage($upload_img);

                if($path_save_img){
                    $slider_img = URL.$path_save_img['thumb'];
                }else{
                    $slider_img = '';
                }
            }else{
                $slider_img = '';
            }

            $data = array(
                'slider_img'            => $slider_img,
                'slider_title'          => $slider_title,
                'slider_title_english'  => $slider_title_english,
                'slider_caption'        => $slider_caption,
                'slider_caption_english'=> $slider_caption_english,
                'slider_url'            => $slider_url,
                'slider_status'         => $slider_status,
                'slider_type'           => $slider_type
            );

            $result = $model->InsertSlider($data);
            if($result){
                echo 1;
            }
        }
    }

    public function editslider($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($edit[0]['permission_detail'],'slider')){
                header('Location:'.URL.'admin/');
            }
        }

        $model = new model_slider();
        if($path){
            if($model->SelectSliderById($path)){

                $this->view->sliderinfo = $model->SelectSliderById($path);



            }else{
                header('Location:'.URL.'slider');
            }
        }else{
            header('Location:'.URL.'slider');
        }

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/slider.js'
        );

        $this->view->layout('layout-admin1');
        $this->view->Render('slider/editslider');
        return true;
    }

    public function addslider(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($create[0]['permission_detail'],'slider')){
                header('Location:'.URL.'admin/');
            }
        }

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/slider.js'
        );

        $this->view->layout('layout-admin1');
        $this->view->Render('slider/addslider');
        return true;
    }

    public function index(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($create[0]['permission_detail'],'slider') || !strpos($delete[0]['permission_detail'],'slider') || !strpos($edit[0]['permission_detail'],'slider')){
                header('Location:'.URL.'admin/');
            }
        }

        $model = new model_slider();

        if($model->SelectAllSlider()){
            $this->view->listslider = $model->SelectAllSlider();
        }

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/slider.js'
        );

        $this->view->layout('layout-admin1');
        $this->view->Render('slider/index');
        return true;
    }
}