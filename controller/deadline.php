<?php
class controller_deadline extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    function index(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $deadline = new model_deadline();

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($create[0]['permission_detail'],'event') || !strpos($delete[0]['permission_detail'],'event') || !strpos($edit[0]['permission_detail'],'event')){
                header('Location:'.URL.'admin/');
            }
        }

        $event = new model_event();

        if($event->GetAllEvent()){
            $this->view->listevent = $event->GetAllEvent();
        }

        if($deadline->SelectAllDeadline()){
        	$this->view->listdeadline = $deadline->SelectAllDeadline();
        }

        if($_POST){
        	$event_id	   = $_POST['event_id'];
        	$deadline_name = $_POST['deadline_name'];
        	$deadline_date = date('Y-m-d',strtotime($_POST['deadline_date']));

        	$data = array(
                'event_id'          => $event_id,
                'deadline_name'  	=> $deadline_name,
                'deadline_date'   	=> $deadline_date
            );

            $result = $deadline->InsertDeadline($data);

            if($result){
            	header('Location:'.URL.'deadline');
            }
        }

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
        );

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin1');
        $this->view->Render('deadline/index');
        return true;

    }

    function editdeadline($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $deadline = new model_deadline();

        $event = new model_event();

        if($event->GetAllEvent()){
            $this->view->listevent = $event->GetAllEvent();
        }

        if($path){
        	$deadlineinfo = $deadline->GetDeadlineinfo($path);

        	$this->view->deadlineinfo = $deadlineinfo;

        	if($deadlineinfo)
        	{
        		if($_POST){
		        	$event_id	   = $_POST['event_id'];
		        	$deadline_name = $_POST['deadline_name'];
		        	$deadline_date = date('Y-m-d',strtotime($_POST['deadline_date']));

		        	$data = array(
		                'event_id'          => $event_id,
		                'deadline_name'  	=> $deadline_name,
		                'deadline_date'   	=> $deadline_date
		            );

		            $result = $deadline->UpdateDeadline($data,$path);

		            if($result){
		            	header('Location:'.URL.'deadline');
		            }
		        }
        	}else{
        		header('location:'.URL.'admin');
        	}

        }else{
        	header('location:'.URL.'admin');
        }


        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
        );

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin1');
        $this->view->Render('deadline/editdeadline');
        return true;
    }

    function deletedeadline($path){
    	if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $deadline = new model_deadline();

    	if($path){
        	$deadlineinfo = $deadline->GetDeadlineinfo($path);

        	if($deadlineinfo){
        		$delete = $deadline->DeleteDeadline($path);

        		if($delete){
        			header('location:'.URL.'deadline');
        		}
        	}else{
        		header('location:'.URL.'admin');
        	}

        }else{
        	header('location:'.URL.'admin');
        }


        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
        );

    }
}