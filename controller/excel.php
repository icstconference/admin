<?php
class controller_excel extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    public function index(){
    	$model = new model_general();
    	if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        if($model->SelectAllTableDatabase()){
            $listtable = $model->SelectAllTableDatabase();

            foreach ($listtable as $key => $value) {
                if($model->CountRowTable($value['table_name'])){
                    $numrow = $model->CountRowTable($value['table_name']);
                    $listtable[$key]['table_num_row'] = $numrow[0]['numrow'];
                }
            }

            $this->view->listtable = $listtable;
        }

        $this->view->js = array(
            URL.'public/js/excel.js'
        );

        $this->view->layout('layout-admin1');
        $this->view->Render('excel/index');
        return true;
    }

    public function import(){
    	$model = new model_general();
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        if($model->SelectAllTableDatabase()){
            $listtable = $model->SelectAllTableDatabase();

            foreach ($listtable as $key => $value) {
                if($model->CountRowTable($value['table_name'])){
                    $numrow = $model->CountRowTable($value['table_name']);
                    $listtable[$key]['table_num_row'] = $numrow[0]['numrow'];
                }
            }

            $this->view->listtable = $listtable;
        }

        if($_POST){
            $table_name = $_POST['table_name'];

            $listcolumn = $model->SelectAllColumnTable($table_name);

            $str = '';

            foreach ($listcolumn as $value) {
                $str .= $value['Field'].',';
            }

            $str = rtrim($str,',');

            $file = $_FILES['file_csv']['tmp_name'];
            $handle = fopen($file, "r");
            $c = 0;
            while(!feof($handle)){
                $filesop = fgetcsv($handle);

                $item = explode(',',$str);

                $numcol = count($item);

                $str1 = '';

                foreach ($filesop as $value) {
                    $str1 .= "'".$value ."',";
                }

                $str1 = rtrim($str1,',');

                $str2 = '';

                for($i=1;$i<$numcol;$i++) {
                    $str2 .= $item[$i].',';
                }

                $str2 = rtrim($str2,',');

                $query = 'INSERT INTO '.$table_name.'('.$str2.') VALUES('.$str1.')';

                $insert = $model->InsertColumnTable($query);
            };  
            header('Location:'.URL.'excel/export');
        }

        $this->view->js = array(
            URL.'public/js/excel.js'
        );

        $this->view->layout('layout-admin1');
        $this->view->Render('excel/import');
        return true;
    }

    public function export(){
    	$model = new model_general();
    	if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        if($model->SelectAllTableDatabase()){
        	$listtable = $model->SelectAllTableDatabase();

            foreach ($listtable as $key => $value) {
                if($model->CountRowTable($value['table_name'])){
                    $numrow = $model->CountRowTable($value['table_name']);
                    $listtable[$key]['table_num_row'] = $numrow[0]['numrow'];
                }
            }

            $this->view->listtable = $listtable;
        }

        if($_POST){
            $table_name = $_POST['table_name'];

            $listcolumn = $model->SelectAllColumnTable($table_name);

            $date = date('dmYhij');

            $filename = $table_name.$date.'.csv';

            $query = "SELECT * FROM ".$table_name." INTO OUTFILE 'C:\ ".$filename."' FIELDS TERMINATED BY ','";

            $export = $model->InsertColumnTable($query);
        }

        $this->view->js = array(
        	URL.'public/js/excel.js'
        );

        $this->view->layout('layout-admin1');
        $this->view->Render('excel/export');
        return true;
    }

    public function deletedata($path){
        $model = new model_general();

        if($path){
            $deletedata = $model->DeleteTableByName($path);

            header('Location:'.URL.'excel');
        }else{
            header('Location:'.URL.'excel');
        }
    }
}