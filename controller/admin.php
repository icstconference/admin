<?php
class controller_admin extends lib_controller{

    public function __construct() {
        parent::__construct();
        if(lib_session::Logged() == false){
            header('Location:'.URL.'login');
        }
    }

    public function index(){
        $model = new model_general();
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $this->view->numuser = $model->CountUser();

        $this->view->numnews= $model->CountNews();

        $this->view->numproduction= $model->CountProduct();

        $this->view->numcategoriesproduct = $model->CountCategoriesProduct();

        $this->view->numcategoriesnews = $model->CountCategoriesNews();

        $this->view->numslider = $model->CountSlider();

        $this->view->numorder = $model->CountOrder();

        $this->view->numpartner = $model->CountPartner();

        $this->view->numquestion = $model->CountQuestions();

        $this->view->numevents = $model->CountEvents();

        $this->view->numcontactuser = $model->CountContactUser();

        $this->view->numemail = $model->CountEmail();

        $this->view->numimage = $model->CountImage();

        $this->view->numadvertise = $model->CountAdvertise();
		
		$this->view->numfile = $model->CountFile();

        $this->view->numevent = $model->CountEvents();
        
        $this->view->numartist = $model->CountArtist();

        if($model->SelectTop5News()){
            $this->view->listnews = $model->SelectTop5News();
        }

        if($model->SelectTop5Files()){
            $this->view->listfiles = $model->SelectTop5Files();
        }

        if($model->SelectTop5Events()){
            $this->view->listevent = $model->SelectTop5Events();
        }

        if($model->SelectTop5Artist()){
            $this->view->listartist = $model->SelectTop5Artist();
        }

        $this->view->layout('layout-admin1');
        $this->view->Render('admin/index');
        return true;
    }

    function UploadImage(array $data) {
        $type = '';
        if(($data["type"] == "image/jpeg")
            || ($data["type"] == "image/jpg") || ($data["type"] == "image/pjpeg")){
                $type = 'jpg';  
        }elseif($data["type"] == "image/gif"){
            $type = 'gif';
        }elseif($data["type"] == "image/png"){
            $type = 'png';
        }
        if ((($data["type"] == "image/gif") || ($data["type"] == "image/jpeg")
                || ($data["type"] == "image/jpg") || ($data["type"] == "image/pjpeg")
                || ($data["type"] == "image/x-png") || ($data["type"] == "image/png"))
            && ($data["size"] < 4000000)) {
            if ($data["error"] == 0) {
                $path = "public/upload/images/" . uniqid() . '.'.$type;
                move_uploaded_file($data["tmp_name"], $path);
                return $path;
            }
        }
        return false;
    }

    function SaveThumbImage($path) {
        try {
            $thumb = PhpThumbFactory::create($path);
        } catch (Exception $e) {
            // handle error here however you'd like
        }
        $id=  uniqid();
        $name_file = $id.'.jpg';
        $thumb->resize(400);
        $thumb->adaptiveResize(300, 300);
        $thumb->save('public/upload/images/site/'. $name_file, 'jpg');

        $path_result = array(
            'thumb' => $path,
        );
        return $path_result;
    }

    public function dashboard(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin1');
        $this->view->Render('admin/dashboard');
        return true;
    }

    public function deletecontact($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($delete[0]['permission_detail'],'contact')){
                header('Location:'.URL.'admin/');
            }
        }

        $model = new model_setting();
        if($path){
            if($model->GetContactById($path)){
                $result = $model->DeleteContact($path);

                if($result){
                    header('Location:'.URL.'admin/contact/');
                }
            }else{
                header('Location:'.URL.'admin/contact/');
            }
        }else{
            header('Location:'.URL.'admin/contact/');
        }
    }

    public function editcontact($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($edit[0]['permission_detail'],'contact')){
                header('Location:'.URL.'admin/');
            }
        }
        
        $model = new model_setting();
        if($path){
            if($model->GetContactById($path)){
                $result = $model->GetContactById($path);
                $this->view->contactinfo = $result;

                if($_POST){
                    $brand_name = $_POST['brand_name'];
					$brand_name_english = $_POST['brand_name_english'];
					$email = $_POST['email'];
					$phone = $_POST['phone'];
					$address = $_POST['address'];
					$address_english = $_POST['address_english'];
					$fax = $_POST['fax'];

                    $array = array(
						'contact_name'    		=> $brand_name,
						'contact_name_english'    => $brand_name_english,
						'contact_address' 		=> $address,
						'contact_address_english' => $address_english,
						'contact_email'   		=> $email,
						'contact_phone'   		=> $phone,
						'contact_fax'			=> $fax
                    );

                    $result = $model->UpdateContact($array,$path);

                    if($result){
                        header('Location:'.URL.'admin/contact/');
                    }
                }

                $this->view->layout('layout-admin1');
                $this->view->Render('admin/editcontact');
                return true;
            }else{
                header('Location:'.URL.'admin/contact/');
            }
        }else{
            header('Location:'.URL.'admin/contact/');
        }
    }

    public function contact(){
        $model = new model_setting();
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($create[0]['permission_detail'],'contact') || !strpos($delete[0]['permission_detail'],'contact') || !strpos($edit[0]['permission_detail'],'contact')){
                header('Location:'.URL.'admin/');
            }
        }
        

        if($model->GetAllContact()){
            $listcontact = $model->GetAllContact();

            $this->view->listcontact = $listcontact;
        }

        if($_POST){
            $brand_name = $_POST['brand_name'];
			$brand_name_english = $_POST['brand_name_english'];
            $email = $_POST['email'];
            $phone = $_POST['phone'];
            $address = $_POST['address'];
			$address_english = $_POST['address_english'];
			$fax = $_POST['fax'];

            $array = array(
              'contact_name'    		=> $brand_name,
			  'contact_name_english'    => $brand_name_english,
              'contact_address' 		=> $address,
			  'contact_address_english' => $address_english,
              'contact_email'   		=> $email,
              'contact_phone'   		=> $phone,
			  'contact_fax'				=> $fax
            );

            $result = $model->InsertContact($array);

            if($result){
                header('Location:'.URL.'admin/contact/');
            }
        }
        
        $this->view->js = array(
            URL.'public/js/contact.js'
        );


        $this->view->layout('layout-admin1');
        $this->view->Render('admin/contact');
        return true;
    }

    public function updatesetting(){
        require_once 'plugin/phpthumb/ThumbLib.inc.php';
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        if($_SESSION['user_type'] != 1){
            header('Location:'.URL.'admin');
        } 

        $model = new model_setting();

        if($_POST){

            $allsetting = $model->GetAllSetting();

            $website_title          = $_POST['website_title'];
            $website_description    = $_POST['website_description'];
            $website_keyword        = $_POST['website_keyword'];
            $website_url            = $_POST['website_url'];
            $website_facebook       = $_POST['website_facebook'];
            $website_google         = $_POST['website_google'];
            $website_linkedin       = $_POST['website_linkedin'];
            $website_twitter        = $_POST['website_twitter'];
            $website_email          = $_POST['website_email'];
            $config_url             = $_POST['config_url'];

            if(strlen($_FILES['logo-img']['name']) >= 1){
                $upload_img = $this->UploadImage($_FILES['logo-img']);
                $path_save_img = $this->SaveThumbImage($upload_img);

                if($path_save_img){
                    $website_logo = URL.$path_save_img['thumb'];
                }else{
                    $website_logo = '';
                }
            }else{
                $website_logo = $allsetting[0]['config_logo'];
            }

            if(strlen($_FILES['icon-img']['name']) >= 1){
                $upload_img1 = $this->UploadImage($_FILES['icon-img']);
                $path_save_img1 = $this->SaveThumbImage($upload_img1);

                if($path_save_img1){
                    $website_icon = URL.$path_save_img1['thumb'];
                }else{
                    $website_icon = '';
                }
            }else{
                $website_icon = $allsetting[0]['config_icon'];
            }

            $data = array(
                'config_title'       		=> $website_title,
                'config_description' 		=> $website_description,
                'config_keyword'     		=> $website_keyword,
                'config_siteurl'     		=> $website_url,
                'config_logo'        		=> $website_logo,
                'config_icon'        		=> $website_icon,
                'config_google'     		=> $website_google,
                'config_facebook'    		=> $website_facebook,
                'config_twitter'     		=> $website_twitter,
                'config_linkedin'    		=> $website_linkedin,
                'config_email'       		=> $website_email,
                'config_url'                => $config_url
            );

            $result = $model->UpdateAllSetting($data);

            if($result){
                echo '1';
            }
        }
    }

    public function setting(){
        require_once 'plugin/phpthumb/ThumbLib.inc.php';

        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }
        
        if($_SESSION['user_type'] != 1){
            header('Location:'.URL.'admin');
        } 

        $model = new model_setting();

        if($model->GetAllSetting()){
            $allsetting = $model->GetAllSetting();

            $this->view->allsetting = $allsetting;
        }

        $this->view->title = 'Trang quản trị';

        $this->view->js = array(
            URL.'public/js/setting.js'
        );

        $this->view->layout('layout-admin1');
        $this->view->Render('admin/setting');
        return true;
    }
	
	public function check(){
        if(lib_session::Get('user_type') != '0'){
            header('Location:'.URL.'login/');
        }

		$user = new model_general();
		
		if($_POST){
			$userid = $_POST['user_id'];
			
			$userinfo = $user->GetUserById($userid);
			
			if($userinfo){
				$create = $user->GetAllPermissionByUserId1($userid,'create');                        
				$delete = $user->GetAllPermissionByUserId1($userid,'delete');                        
				$edit   = $user->GetAllPermissionByUserId1($userid,'edit');  
				
				$permissioncreate = $_SESSION['user_permission_create'];
				$permissiondelete = $_SESSION['user_permission_delete'];
				$permissionedit   = $_SESSION['user_permission_edit'];
				
				if($create != $permissioncreate){
					lib_session::Set('user_permission_create',$create);
				}
				if($delete != $permissiondelete){
					lib_session::Set('user_permission_delete',$delete);
				}
				if($edit != $permissionedit){
					lib_session::Set('user_permission_edit',$edit);
				}
			}else{
				echo '1';
			}
		}
	}
}