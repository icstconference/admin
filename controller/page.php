<?php
class controller_page extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    function UploadImage(array $data) {
        if ((($data["type"] == "image/gif") || ($data["type"] == "image/jpeg")
                || ($data["type"] == "image/jpg") || ($data["type"] == "image/pjpeg")
                || ($data["type"] == "image/x-png") || ($data["type"] == "image/png"))
            && ($data["size"] < 4000000)) {
            if ($data["error"] == 0) {
                $path = "public/upload/images/page/" . uniqid() . '.jpg';
                move_uploaded_file($data["tmp_name"], $path);
                return $path;
            }
        }
        return false;
    }

    function SaveThumbImage($path) {
        try {
            $thumb = PhpThumbFactory::create($path);
        } catch (Exception $e) {
            // handle error here however you'd like
        }
        $id=  uniqid();
        $name_file = $id.'.jpg';
        $thumb->resize(400);
        $thumb->adaptiveResize(300, 300);
        $thumb->save('public/upload/images/page/' . $name_file, 'jpg');

        $path_result = array(
            'image' => $path,
            'thumb' => 'public/upload/images/page/' . $name_file,
        );
        return $path_result;
    }

    protected function DeleteFile($file_path) {
        $path = str_replace(URL, '', $file_path);
        if (file_exists($path)) {
            unlink($path);
            return true;
        }
        return false;
    }

    public function index(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($create[0]['permission_detail'],'page') || !strpos($delete[0]['permission_detail'],'page') || !strpos($edit[0]['permission_detail'],'page')){
                header('Location:'.URL.'admin/');
            }
        }

    	$page = new model_page();

    	if($page->SelectAllPage()){
    		$this->view->listpage = $page->SelectAllPage();
    	}

    	$this->view->layout('layout-admin1');
        $this->view->Render('page/index');
    }

    public function updatepage(){
        require_once 'plugin/phpthumb/ThumbLib.inc.php';
        require_once 'plugin/url-seo/url-seo.php';
        $model = new model_page();
		
        if($_POST){
            $product_id 				 = $_POST['news_id'];
            $news_info 					 = $model->GetPageById($product_id);
            $product_name 				 = $_POST['news_name'];
            $product_name_english 		 = $_POST['news_name_english'];
            $product_description 		 = $_POST['news_description'];
            $product_description_english = $_POST['news_description_english'];
            $urlnews                     = explode('-',$news_info[0]['page_url']);
            $numarrnews                  = count($urlnews);
            $urlcode                     = $urlnews[$numarrnews-2];
            $news_url 		  			 = post_slug($product_name).'-page-'.$product_id;
            $news_url_english 			 = post_slug($product_name_english).'-page-'.$product_id;
            $page_type                   = $_POST['page_type'];

            if(strlen($_FILES['news_img']['name'])>1){
                $upload_img = $this->UploadImage($_FILES['news_img']);
                $path_save_img = $this->SaveThumbImage($upload_img);

                if($path_save_img){
                    $product_img = URL.$path_save_img['image'];
                    $this->DeleteFile($news_info[0]['page_thumbnail']);
                }else{
                    $product_img = $news_info[0]['page_thumbnail'];
                }
            }else{
                $product_img = $news_info[0]['page_thumbnail'];
            }

            $data = array(
                'page_name'          		=> addslashes($product_name),
                'page_name_english'  		=> addslashes($product_name_english),
                'page_description'   		=> $product_description,
                'page_description_english'  => $product_description_english,
                'page_thumbnail'   			=> $product_img,
                'page_url'           		=> $news_url,
                'page_type'                 => $page_type,
                'page_url_english'   		=> $news_url_english
            );

			//print_r($data);
			
            $result = $model->UpdatePage($data,$product_id);

            if($result){
                $log = new model_log();
                $arr = array(
                    'log_action'        => 'update',
                    'user_id'           => $_SESSION['user_id'],
                    'log_object'        => 'trang',
                    'log_object_id'     => $product_id,
                    'log_object_name'   => $product_name,
                    'user_name'         => $_SESSION['user_name']
                );
                $log->InsertLog($arr);
                echo '1';
            }
        }
    }

    public function createpage(){
        require_once 'plugin/phpthumb/ThumbLib.inc.php';
        require_once 'plugin/url-seo/url-seo.php';
        $model = new model_page();
        if($_POST){
            $product_name 				 = $_POST['news_name'];
            $product_name_english 		 = $_POST['news_name_english'];
            $product_description 		 = $_POST['news_description'];
            $product_description_english = $_POST['news_description_english'];
            $page_type                   = $_POST['page_type'];

            if(strlen($_FILES['news_img']['name'])>1){
                $upload_img = $this->UploadImage($_FILES['news_img']);
                $path_save_img = $this->SaveThumbImage($upload_img);

                if($path_save_img){
                    $product_img = URL.$path_save_img['image'];
                }else{
                    $product_img = '';
                }
            }else{
                $product_img = '';
            }

            $data = array(
                'page_name'          		=> addslashes($product_name),
                'page_name_english'  		=> addslashes($product_name_english),
                'page_description'   		=> $product_description,
                'page_description_english'  => $product_description_english,
                'page_thumbnail'   			=> $product_img,
                'page_type'                 => $page_type,
                'user_id'					=> $_SESSION['user_id']
            );

            $result = $model->InsertPage($data);
			
            if($result){
                $date                = date('dmY');
                $uniqueid            = uniqid();
                $product_url 		 = post_slug($product_name).'-page-'.$result;
                $product_url_english = post_slug($product_name_english).'-page-'.$result;
                $data1 = array(
                    'page_url' 			=> $product_url,
                    'page_url_english'  => $product_url_english
                );
				
                $update = $model->UpdatePage($data1,$result);
                if($update){
                    $log = new model_log();
                    $arr = array(
                        'log_action'        => 'create',
                        'user_id'           => $_SESSION['user_id'],
                        'log_object'        => 'trang',
                        'log_object_id'     => $result,
                        'log_object_name'   => $product_name,
                        'user_name'         => $_SESSION['user_name']
                    );
                    $log->InsertLog($arr);
                    echo 1;
                }
            }
        }
    }

    public function addpage(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($create[0]['permission_detail'],'page')){
                header('Location:'.URL.'admin/');
            }
        }

    	$page = new model_page();

    	$this->view->js = array(
    		URL.'public/ckeditor/ckeditor.js',
    		URL.'public/js/page.js'
    	);

    	$this->view->layout('layout-admin1');
        $this->view->Render('page/addpage');
    }

    public function edit($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($edit[0]['permission_detail'],'page')){
                header('Location:'.URL.'admin/');
            }
        }

    	$page = new model_page();

    	if($path){
    		if($page->GetPageById($path)){
    			$pageinfo = $page->GetPageById($path);

    			$this->view->pageinfo = $pageinfo;
    		}else{
    			header('Location:'.URL.'page');
    		}
    	}else{
    		header('Location:'.URL.'page');
    	}

    	$this->view->js = array(
    		URL.'public/ckeditor/ckeditor.js',
    		URL.'public/js/page.js'
    	);

    	$this->view->layout('layout-admin1');
        $this->view->Render('page/editpage');
    }

    public function delete($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($delete[0]['permission_detail'],'page')){
                header('Location:'.URL.'admin/');
            }
        }

    	$page = new model_page();

    	if($path){
    		if($page->GetPageById($path)){
    			$pageinfo = $page->GetPageById($path);

    			$deletepage = $page->DeletePageById($path);

    			if($deletepage){
    				$this->DeleteFile($pageinfo[0]['page_thumbnail']);
                    $log = new model_log();
                    $arr = array(
                        'log_action'        => 'delete',
                        'user_id'           => $_SESSION['user_id'],
                        'log_object'        => 'trang',
                        'log_object_id'     => $pageinfo[0]['page_id'],
                        'log_object_name'   => $pageinfo[0]['page_name'],
                        'user_name'         => $_SESSION['user_name']
                    );
                    $log->InsertLog($arr);
    				header('Location:'.URL.'page');
    			}
    		}else{
    			header('Location:'.URL.'page');
    		}
    	}else{
    		header('Location:'.URL.'page');
    	}
    }

    public function delete1(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($delete[0]['permission_detail'],'page')){
                header('Location:'.URL.'admin/');
            }
        }

        $page = new model_page();

        if($_POST){
            $idpost = $_POST['idpost'];
            if($page->GetPageById($idpost)){
                $pageinfo = $page->GetPageById($idpost);
                $deletepage = $page->DeletePageById($idpost);

                if($deletepage){
                    $this->DeleteFile($pageinfo[0]['page_thumbnail']);
                    $log = new model_log();
                    $arr = array(
                        'log_action'        => 'delete',
                        'user_id'           => $_SESSION['user_id'],
                        'log_object'        => 'trang',
                        'log_object_id'     => $pageinfo[0]['page_id'],
                        'log_object_name'   => $pageinfo[0]['page_name'],
                        'user_name'         => $_SESSION['user_name']
                    );
                    $log->InsertLog($arr);
                    echo '1';
                }
            }
        }
    }


    public function copy($path){
        require_once 'plugin/url-seo/url-seo.php';
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($create[0]['permission_detail'],'page') || !strpos($delete[0]['permission_detail'],'page') || !strpos($edit[0]['permission_detail'],'page')){
                header('Location:'.URL.'admin/');
            }
        }

        $page = new model_page();

        if($path){
            if($page->GetPageById($path)){
                $pageinfo = $page->GetPageById($path);

                $data = array(
                    'page_name'                 => $pageinfo[0]['page_name'],
                    'page_name_english'         => $pageinfo[0]['page_name_english'],
                    'page_description'          => $pageinfo[0]['page_description'],
                    'page_description_english'  => $pageinfo[0]['page_description_english'],
                    'page_thumbnail'            => $pageinfo[0]['page_thumbnail'],
                    'page_type'                 => $pageinfo[0]['page_type'],
                    'user_id'                   => $_SESSION['user_id']
                );

                $result = $page->InsertPage($data);
                
                if($result){
                    $product_url         = post_slug($pageinfo[0]['page_name']).'-page-'.$result;
                    $product_url_english = post_slug($pageinfo[0]['page_name_english']).'-page-'.$result;
                    $data1 = array(
                        'page_url'          => $product_url,
                        'page_url_english'  => $product_url_english
                    );
                    
                    $update = $page->UpdatePage($data1,$result);

                    $log = new model_log();
                    $arr = array(
                        'log_action'        => 'create',
                        'user_id'           => $_SESSION['user_id'],
                        'log_object'        => 'trang',
                        'log_object_id'     => $pageinfo[0]['page_id'],
                        'log_object_name'   => $pageinfo[0]['page_name'],
                        'user_name'         => $_SESSION['user_name']
                    );
                    $log->InsertLog($arr);
                    header('Location:'.URL.'page');
                }
            }else{
                header('Location:'.URL.'page');
            }
        }else{
            header('Location:'.URL.'page');
        }
    }

    public function detail($path){
        $page = new model_page();

        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $partner = new model_partner();

        $library = new model_library();
        
        $video = new model_video();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
            $this->view->configurl = $allsetting[0]['config_url'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOn()){
            $allmenufather = $model1->SelectAllMenuFatherOn();

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }

        if($path){
            $part = explode('_',$path);
            $id = $part[count($part)-1];

            if($page->GetPageById($id)){

                $pageinfo = $page->GetPageById($id);

                $this->view->pageinfo = $pageinfo;

                if($this->view->lang == 'vi'){
                    $this->view->title = $pageinfo[0]['page_name'].'-'.$allsetting[0]['config_title'];
                    $this->view->description = $pageinfo[0]['page_name'].'-'.$allsetting[0]['config_description'];
                    $this->view->keyword = $pageinfo[0]['page_name'].','.$allsetting[0]['config_keyword'];
                }else{
                    $this->view->title = $result[0]['page_name_english'].'-'.'Institute for Computational Science and Technology';
                    $this->view->description = $result[0]['page_name_english'].'-'.$allsetting[0]['config_description'];
                    $this->view->keyword = $result[0]['page_name_english'].','.$allsetting[0]['config_keyword'];
                }


                $useragent=$_SERVER['HTTP_USER_AGENT'];
        
                if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
                    
                    if($id == 6 || $id == 7 || $id == 8 || $id == 9){

                        $this->view->layout('layout-mobile');
                        $this->view->Render('mobile/page1');
                    }elseif($id == 10 || $id == 11 || $id == 12 || $id == 13){
                        

                        $this->view->layout('layout-mobile');
                        $this->view->Render('mobile/page2');
                    }elseif($id == 14 || $id == 15 || $id == 16 || $id == 17){

                        $this->view->layout('layout-mobile');
                        $this->view->Render('mobile/page3');
                    }else{
                        
                        $this->view->layout('layout-mobile');
                        $this->view->Render('mobile/page');
                    }
                }else{
                    if($pageinfo[0]['page_type'] == 0){
                        $this->view->layout('layout');
                        $this->view->Render('page/detail');
                    }elseif($pageinfo[0]['page_type'] == 1){
                        if($page->SelectAllPageByTypeId(1)){
                            $this->view->listpage = $page->SelectAllPageByTypeId(1);
                        }

                        $this->layout('layout');
                        $this->Render('intro/index');
                    }elseif($pageinfo[0]['page_type'] == 2){
                        if($page->SelectAllPageByTypeId(2)){
                            $this->view->listpage = $page->SelectAllPageByTypeId(2);
                        }

                        $this->view->layout('layout');
                        $this->view->Render('intro/index');
                    }elseif($pageinfo[0]['page_type'] == 3){
                        if($page->SelectAllPageByTypeId(2)){
                            $this->view->listpage = $page->SelectAllPageByTypeId(3);
                        }

                        $this->view->layout('layout');
                        $this->view->Render('intro/index');
                    }
                }
            }else{
                header('Location:'.URL);
            }
        }else{
            header('Location:'.URL);
        }

        
    }
}