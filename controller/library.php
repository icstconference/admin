<?php
class controller_library extends lib_controller{

    public function __construct() {
        parent::__construct();
        if(lib_session::Logged() == false){
            header('Location:'.URL.'login');
        }
    }

     /*
     * Các function tạo hình ảnh
     */

    function UploadImage(array $data) {
        $type = '';
        if(($data["type"] == "image/jpeg")
            || ($data["type"] == "image/jpg") || ($data["type"] == "image/pjpeg")){
                $type = 'jpg';  
        }elseif($data["type"] == "image/gif"){
            $type = 'gif';
        }elseif($data["type"] == "image/png"){
            $type = 'png';
        }
        if ((($data["type"] == "image/gif") || ($data["type"] == "image/jpeg")
                || ($data["type"] == "image/jpg") || ($data["type"] == "image/pjpeg")
                || ($data["type"] == "image/x-png") || ($data["type"] == "image/png"))
            && ($data["size"] < 4000000)) {
            if ($data["error"] == 0) {
                $path = "public/upload/images/library/" . uniqid() . '.'.$type;
                move_uploaded_file($data["tmp_name"], $path);
                return $path;
            }
        }
        return false;
    }

    protected function DeleteFile($file_path) {
        $path = str_replace(URL, '', $file_path);
        if (file_exists($path)) {
            unlink($path);
            return true;
        }
        return false;
    }

    public function createlibrary(){
    	require_once 'plugin/phpthumb/ThumbLib.inc.php';
        require_once 'plugin/url-seo/url-seo.php';


        $library = new model_library();

        if($_POST){
            if(strlen($_FILES['file']['name'])>1){
            	$library_type = $_FILES['file']['type'];
                $library_name = $_FILES['file']['name'];
                $upload_img = $this->UploadImage($_FILES['file']);

                if($upload_img){
                    $library_img = URL.$upload_img;
                }


                $arr = array(
                    'media_name'    => $library_name,
                	'media_url'		=> $library_img,
                	'media_type'	=> $library_type
                );

                $result = $library->InsertLibrary($arr);
                if($result){
                    $arr1 = array(
                        'image_id'  => $result
                    );

                    echo json_encode($arr1);
                } 
            }
        }
    }

    public function deletelibrary($path){
        $library = new model_library();

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if(!strpos($delete[0]['permission_detail'],'galleries') || $_SESSION['user_type'] != 1){
            header('Location:'.URL.'admin/');
        }

        if($path){
            if($library->GetImageById($path)){
                $libraryinfo = $library->GetImageById($path);
                $imagedelete = $libraryinfo[0]['media_url'];
                $delete = $library->DeleteLibrary($path);
                if($delete){
                    $this->DeleteFile($imagedelete);
                    header('Location:'.URL.'library');
                }
            }
        }else{
            header('Location:'.URL.'library');
        }
    }

    public function deletelibrary1(){

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($delete[0]['permission_detail'],'galleries')){
                header('Location:'.URL.'admin/');
            }
        }

        $library = new model_library();

    	if($_POST){
            $name = $_POST['id'];

    		if($library->GetImageByName($name)){
    			$libraryinfo = $library->GetImageByName($name);
    			$imagedelete = $libraryinfo[0]['media_url'];
    			$delete = $library->DeleteLibrary($libraryinfo[0]['media_id']);
                if($delete){
                    $this->DeleteFile($imagedelete);
                }
    		}
    	}
    }

     public function deletelibrary2(){

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($delete[0]['permission_detail'],'galleries')){
                header('Location:'.URL.'admin/');
            }
        }

        $library = new model_library();

        if($_POST){
            $name = $_POST['id'];
            if($library->GetImageById($name)){
                $libraryinfo = $library->GetImageById($name);
                $imagedelete = $libraryinfo[0]['media_url'];
                $delete = $library->DeleteLibrary($name);
                if($delete){
                    $this->DeleteFile($imagedelete);
                    echo '1';
                }
            }
        }
    }

    public function createimage(){

    }

    public function addimage(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($create[0]['permission_detail'],'galleries')){
                header('Location:'.URL.'admin/');
            }
        }

        $this->view->layout('layout-admin1');
        $this->view->Render('library/addimage');
        return true;
    }

    public function index(){
        $library = new model_library();

        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($create[0]['permission_detail'],'galleries') || !strpos($delete[0]['permission_detail'],'galleries') || !strpos($edit[0]['permission_detail'],'galleries')){
                header('Location:'.URL.'admin/');
            }
        }

        if($library->SelectAllLibrary()){
        	$this->view->listlibrary = $library->SelectAllLibrary();
        }

        $this->view->js = array(
        	URL.'public/js/library.js'
        );

        $this->view->layout('layout-admin1');
        $this->view->Render('library/index');
        return true;
    }
}
