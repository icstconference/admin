<!DOCTYPE html>
<html lang="en-US"><!--<![endif]-->
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Conference</title>
    <meta name="description" content="Conference" />
    <meta name="keywords" content="Conference" />
    <meta name="robots" content="index, follow" />
    <meta name="generator" content="Conference" />
    <meta name="author" content="Conference" />
    <link rel="shortcut icon" href="" />
    <meta name="revisit-after" content="1 days" />

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    
    <!-- Font Awesome CSS -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    
    
    <!-- Animate CSS -->
    <link href="css/animate.css" rel="stylesheet" >
    
    <!-- Owl-Carousel -->
    <link rel="stylesheet" href="css/owl.carousel.css" >
    <link rel="stylesheet" href="css/owl.theme.css" >
    <link rel="stylesheet" href="css/owl.transitions.css" >

    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style2.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    
    
    <!-- Modernizer js -->
    <script src="js/modernizr.custom.js"></script>

    <style type="text/css">
    	@font-face {
            font-family: 'MarkSimonson';
            src: url('font/MarkSimonsonRegular.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }
        @font-face {
            font-family: 'MarkSimonson';
            src: url('font/MarkSimonsonThinItalic.ttf') format('truetype');
            font-weight: normal;
            font-style: italic;
        }
        @font-face {
            font-family: 'MarkSimonson';
            src: url('font/MarkSimonsonSemibold.ttf') format('truetype');
            font-weight: bold;
            font-style: normal;
        }

        body{
            font-family: 'MarkSimonson', sans-serif;
            color: #333333;
        }
        h1, h2, h3, h4, h5, h6, a, {
        	font-family: 'MarkSimonson', sans-serif !important;
        }
        a:focus{
        	text-decoration:none;
        }
        .buttonblue{
            background: #2969b0;
            font-size: 30px;
            padding: 20px 40px;
            margin-right: 10px;
            font-weight: 300;
            width: 230px;
            color: #fff;
            border-radius:10px !important;
        }
        .buttonblue:hover{
            background: #fff;
            color: #333;
        }
        .buttonblue2{
            background: #2969b0;
            font-size: 16px;
            margin-right: 10px;
            font-weight: 300;
            color: #fff;
        }
        .buttonblue2:hover{
            background: #fff;
            color: #333;
        }
        .buttonblue1{
            background: #2969b0;
            font-size: 30px;
            padding: 20px 40px;
            margin-right: 10px;
            font-weight: 300;
            width: 230px;
            color: #fff;
            border-radius:10px !important;
            text-transform: none !important;
    		margin-top: 20px;
        }
        .buttonblue1:hover{
            background: #fff;
            color: #333;
        }
        .buttonorange{
            background: #ff7733;
            color:#fff;
            font-size: 30px;
            padding: 21px 41px;
            margin-right: 10px;
            font-weight: 300;
            border-radius:4px !important;
        }
        .buttonorange:hover{
            background: #fff;
            color: #ff7733;
        }
        .footertitle{
            font-size: 18px;
            color: #fff;
            font-weight: 300;
            border-bottom: 1px solid #ff7733;
            padding-bottom: 10px;
        }
        .bodyfooter{
            margin: 20px 0;
            color: #fff;
            font-size: 18px;
            font-weight: 300;
            line-height: 30px;
        }
        .footergray{
            color:#aaaaaa;
        }
        .buttonsubcribe{
            background: #ff7733;
            color: #fff;
            margin-top:10px;
            border-radius:0;
        }
        .indextitle{
            font-size: 30px;
            color: #333;
            font-weight: 400;
            border-bottom: 1px solid #ff7733;
            padding-bottom: 0px;
            text-transform: uppercase;
        }
        .indextitle1{
            font-size: 35px !important;
            border-bottom: 1px solid #ff7733 !important;
        }
        .h3news{
            margin: 6px 0;
            text-transform: none;
            font-size: 20px;
        }
        h1, .h1, h2, .h2, h3, .h3{
            margin: 10px 0;
        }
        p {
            margin: 0 0 5px;
        }
        a{
            color: #333;
            text-decoration:none;
        }
        a:hover{
            color: #ff7733;
            text-decoration:none;
        }
        .frame {
            height: 64px;
            white-space: nowrap;
            text-align: center;
            margin: 1em 0;
            display: inline-block;
            width: 192px;
        }
        .helper {
            display: inline-block;
            height: 100%;
            vertical-align: middle;
        }
        .frame img{
            vertical-align: middle;
            max-height: 85px;      /* equals max image height */
            max-width: 146px;
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%);
        }
        .frame img:hover{
            -webkit-filter: grayscale(0%);
            filter: grayscale(0%);
        }
        h1.animated3{
            font-size:60px;
        }
        .navbar-default{
            background: #fff;
        	height: 70px;
        	box-shadow: 0px 0 2px #555;
        }
        .navbar-brand {
            padding: 7px 15px;
        }
        .navbar-default .nav li a{
        	color: #2969b0;
        	font-weight: 400;
        	padding-bottom: 22px;
        }
        .navbar-default .navbar-nav>li>a:hover, .navbar-default .navbar-nav>li>a:focus{
        	color: #333;
        	padding-bottom: 19px;
    		border-bottom: 3px solid #333;
    		background: transparent;
        }
        .navbar-default .navbar-nav>.active>a{
        	color: #2969b0;
        	padding-bottom: 19px;
    		border-bottom: 3px solid #2969b0;
    		background: transparent;
        }
        .conference{
        	color: #2969b0;
        	text-transform:uppercase;
        	font-weight: 400;
    		font-size: 28px;
    		font-family: 'MarkSimonson', sans-serif !important;
        }
        .line{
        	width:20px;
        	border:1px solid #2969b0;
        	margin-bottom:10px;
        }
       	.textunderline{
       		font-size: 16px;
		    line-height: 24px;
		    color: #888;
		    font-weight: 300;
       	}
       	.listhighlight{
       		list-style-type:none;
       		margin-top: 5px;
       	}
       	.listhighlight >li{
       		line-height: 24px;
		    font-size: 16px;
		    font-weight: 400;
       	}
       	.listhighlight a:hover{
       		color: #2969b0;
       	}
       	.navbar-default.navbar-shrink {
		    padding:0;
		    background-color: #fff;
		}
		.deadline{
			background: url(img/deadline.png) no-repeat center;
    		height: 225px;
    		text-align:center;
		}
		.deadlinetext{
			padding: 30px 0 10px;
		    font-weight: 300;
		    font-size: 28px;
		}
		.deadlinecontainlink{
			max-width: 154px;
		    margin: 0 auto;
		    text-align: center;
		}
		.deadlinelink{
			font-size: 24px;
		    text-decoration: none;
		    text-transform: uppercase;
		    font-weight: 500;
		}
		.deadlinelink:hover,.deadlinelink:focus{
			color: #2969b0;
			text-decoration:none;
		}
		.wpb_tabs .wpb_tour_tabs_wrapper ul.wpb_tabs_nav.tz_meetup_tabs li a{
			color: #2969b0;
			border: 1px solid #2969b0;
		}
		.wpb_tabs .wpb_tour_tabs_wrapper ul.wpb_tabs_nav.tz_meetup_tabs li a:hover{
			color: #333;
			background: #fff;
		}
		.wpb_tabs ul.wpb_tabs_nav.tz_meetup_tabs li:hover{
			background-color: transparent;
		}
		.ui-widget-content{
			background: transparent;
			border:0;
		}
		.panel-group .panel{
			border-radius: 0;
		}
		.panel-default>.panel-heading{
			background-color: #cddced;
		}
		#feature .faq-toggle.collapsed::before {
			color: #2969b0;
			font-family: FontAwesome;
		    content:"\f067";
		    -webkit-transition: 0.5s linear ease-out;
		    transition: 0.5s linear ease-out;
		    position: absolute;
		    left: 25px;
		}
		#feature .faq-toggle::before {
		    position: absolute;
		    left: 25px;
		    font-family: FontAwesome;
		    content: "\f068";
		    color: #2969b0;
		}
		.panel-title>a:hover{
			color: #2969b0;
		}
		.panel-title>a {
			margin-left: 15px;
			font-weight: 400;
		}
		#packetleft{
			padding-left:0;
		}
		#packetright{
			padding-right:0;
		}
		.addtocart{
			font-size: 22px;
		    color: #fff;
		    line-height: 50px;
		    font-weight: 500;
		}
		.addtocart:hover{
			color:#333;
		}
		.frame {
		    height: 85px;
		    white-space: nowrap;
		    text-align: center;
		    margin: 1em 0;
		    display: inline-block;
		    width: 192px;
		}
		.frame img {
		    vertical-align: middle;
		    max-height: 100px;
		    max-width: 200px;
		    -webkit-filter: grayscale(100%);
		    filter: grayscale(100%);
		}
		.google-maps {
	        position: relative;
	        height: 375px;
	    }
	    .google-maps iframe {
	        top: 0;
	        left: 0;
	        width: 100% !important;
	        height: 375px;
	        z-index:0;
	    }
	    .menufooter{
	    	list-style-type:none;
	    }
	    .menufooter> li{
	    	padding: 0 10px;
	    	display:inline;
	    }
	    .menufooter a{
	    	text-transform: uppercase;
		    color: #fff;
		    font-size: 20px;
		    font-weight: 300;
		    text-decoration:none;
	    }
	    .menufooter a:hover{
	    	color:#be926f;
	    }
    </style>

</head>    
<body class="index">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container" style="padding-left: 0px;">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#">
                    <img class="img-responsive" src="img/icstlogo.png" />
                </a>

            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="padding: 10px 0px;">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active">
                        <a class="page-scroll" href="#">Home</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#">Deadlines</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#">Invited Talks</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#">Schedule</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#">Organizers</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#">Contact us</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#">Submission</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#">Support</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <section id="page-top">
        <div id="main-slide1" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner">
                <div class="item active">
                    <img class="img-responsive" src="img/slider1.png" alt="slider">
                    <div class="slider-content">
                        <div class="col-md-12 text-center">
                            <h1  style="margin-bottom:25px;font-size:50px;color:#fff;" id="sliderlon">
                                <div style="line-height: 80px;font-weight: 300;font-size: 36px;">
                                	November 28-30 2016<br/>
									<div style="font-weight:400;">
									The Third  International Conference on<br/>
									Computational Science and Engineering (ICCSE-3)
									</div>
								</div>
                            </h1>
                            <a class="slider btn buttonblue btn-min-block" href="#">REGISTER NOW</a>
                             <a class="slider btn buttonblue btn-min-block" href="#">SIGN IN</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="feature" class="feature-section">
        <div class="container" style="padding:0;">
        	<div class="col-sm-6 col-md-6 col-xs-12">
        		<h3 class="conference">
        			about conference
        		</h3>
        		<div class="line"></div>
        		<div class="row textunderline" style="margin: 0 0 10px;">
        			Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
        		</div>
        		<div class="row textunderline" style="margin: 0 0 10px;">
        			<center>
        				<img src="img/about.png" class="img-responsive" />
        			</center>
        		</div>
        	</div>
        	<div class="col-sm-6 col-md-6 col-xs-12">
        		<h3 class="conference">
        			highlight submissions
        		</h3>
        		<div class="line"></div>
        		<div class="row textunderline" style="margin: 0 0 10px;">
        			Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
        		</div>
        		<div class="row textunderline" style="margin: 0 0 10px;">
        			<ul class="listhighlight">
        				<li>
        					<i class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a>
        				</li>
        				<li>
        					<i class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="#">You've inspired new consumer, racked up click-thru's, blown-up brand enes.</a>
        				</li>
        				<li>
        					<i class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a>
        				</li>
        				<li>
        					<i class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="#">You've inspired new consumer, racked up click-thru's, blown-up brand enes.</a>
        				</li>
        				<li>
        					<i class="fa fa-angle-right"></i>&nbsp;&nbsp;<a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a>
        				</li>
        			</ul>
        		</div>
        	</div>
        </div>
    </section>
    <section id="latest-news" class="latest-news-section" style="background:#f1f4f4;padding: 30px 0;" >
        <div class="container" style="padding:0;">
        	<div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom:10px;">
	        	<h3 class="conference">
	        		deadlines
	        	</h3>
        		<div class="line"></div>
	        </div>
            <div class="row" style="margin:0;">
                <div class="latest-news" id="partner3">
                	<div class="col-md-12 col-sm-12 col-xs-12 deadline">
                		<h3 class="deadlinetext">October 10, 2016</h3>
                		<div class="deadlinecontainlink">
	                		<a href="#" class="deadlinelink">
	                			Submission deadline
	                		</a>
	                	</div>
                	</div>
                	<div class="col-md-12 col-sm-12 col-xs-12 deadline">
                		<h3 class="deadlinetext">October 25, 2016</h3>
                		<div class="deadlinecontainlink">
	                		<a href="#" class="deadlinelink">
	                			Acceptance notification
	                		</a>
	                	</div>
                	</div>
                	<div class="col-md-12 col-sm-12 col-xs-12 deadline">
                		<h3 class="deadlinetext">November 8, 2016</h3>
                		<div class="deadlinecontainlink">
	                		<a href="#" class="deadlinelink">
	                			Registration deadline
	                		</a>
	                	</div>
                	</div>
               	</div>
            </div>
        </div>
    </section>

    <section id="feature" class="feature-section">
        <div class="container" style="padding:0;">
        	<div class="row" style="margin:0 0 10px;">
        		<div class="col-md-12 col-sm-12 col-xs-12">
        			<h3 class="conference">
		        		invited talks
		        	</h3>
	        		<div class="line"></div>
        		</div>
        	</div>
        	<div class="row" style="margin:0;">
	        	<div class="col-md-4 col-sm-4 col-xs-12" style="font-weight: 300;font-size:16px;line-height:24px;color:#888;">
	        		Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.lobortis nisl ut aliquip ex ea commodo consequat aliquip ex ea commodo consequat.
	        		<br/>
	        		<a class="slider btn buttonblue1 btn-min-block" href="#">View all speakers</a>
	        	</div>
	        	<div class="col-md-4 col-sm-4 col-xs-12" style="font-weight: 300;">
	        		<center>
	        			<img src="img/invitedtalks.png" class="img-responsive"/>
	        		</center>
	        		<div class="row" style="margin:10px 0;">
	        			<h3>Prof. Dave Thirumalai</h3>
	        			<p style="color: #888;font-weight: 400;">
	        				Robert A. Welch Chair in Chemistry Department of Chemistry, University of Texas at Austin, USA
	        			</p>
	        		</div>
	        	</div>
	        	<div class="col-md-4 col-sm-4 col-xs-12" style="font-weight: 300;">
	        		<center>
	        			<img src="img/invitedtalks.png" class="img-responsive"/>
	        		</center>
	        		<div class="row" style="margin:10px 0;">
	        			<h3>Prof. Dave Thirumalai</h3>
	        			<p style="color: #888;font-weight: 400;">
	        				Robert A. Welch Chair in Chemistry Department of Chemistry, University of Texas at Austin, USA
	        			</p>
	        		</div>
	        	</div>
	        </div>
        </div>
    </section>

    <section id="latest-news" class="latest-news-section" style="background:#f1f4f4;padding: 30px 0;" >
        <div class="container" style="padding:0;">
        	<div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom:10px;">
	        	<h3 class="conference">
	        		Schedule
	        	</h3>
        		<div class="line"></div>
	        </div>
            <div class="row" style="margin:0;">
				<div class="wpb_tabs wpb_content_element widget" data-interval="0">
					<div class="wpb_wrapper wpb_tour_tabs_wrapper tz_meetup_wpb_tour_tabs ui-tabs vc_clearfix">
						<div class="tz_tabs_meetup">
							<ul class="wpb_tabs_nav ui-tabs-nav vc_clearfix tz_meetup_tabs text-center">
								<li><a href="#tab1">day 1 - november 28</a></li>
								<li><a href="#tab2">day 2 - november 29</a></li>
								<li><a href="#tab3">day 3 - november 30</a></li>
							</ul>
						</div>
						<div id="tab1" class="wpb_tab ui-tabs-panel wpb_ui-tabs-hide vc_clearfix">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="tz_event_meetup">
								    <div class="tz_box_event_meetup">
								        <h3>
								            day 1       
								        </h3>
								        <h3 class="tz_event_meetup_subtitle">
								            <em>november 28, 2016</em>        
								        </h3>
								        <div class="tz_event_meettup_box_content">
								            <div class="tz_event_meetup_content">
								                <div class="tz_meetup_box_detail">
												    <div class="tz_meetup_box_detail_custom">
												        <span class="tz_meetup_start_time">
												            8:00AM        
												        </span>
												        <h4>
												            Opening and two plenary talks (Big room)      
												        </h4>
												        <p class="tz_event_time">
															<i class="fa fa-clock-o"></i>
												            9:00AM -
												            12:00AM
												            <p>Everyone in meeting will summaries ideas which gave during the conference. The closing of conference is the end. The secretary writes all things to manager and process in future</p>
												        </p>
												    </div>
												</div>
												<div class="tz_meetup_box_detail">
												    <div class="tz_meetup_box_detail_custom">
												        <span class="tz_meetup_start_time">
												            12:00AM        
												        </span>
												        <h4>
												            Lunch break     
												        </h4>
												        <p class="tz_event_time">
															<i class="fa fa-clock-o"></i>
												            12:00AM -
												            14:00PM
												            <p>Everyone in meeting will summaries ideas which gave during the conference. The closing of conference is the end. The secretary writes all things to manager and process in future</p>
												        </p>
												    </div>
												</div>
												<div class="tz_meetup_box_detail">
												    <div class="tz_meetup_box_detail_custom">
												        <span class="tz_meetup_start_time">
												            14:00PM        
												        </span>
												        <h4>
												            Four parallel  sections (4 small rooms)      
												        </h4>
												        <p class="tz_event_time">
															<i class="fa fa-clock-o"></i>
												            14:00PM -
												            16:00PM
												            <p>Everyone in meeting will summaries ideas which gave during the conference. The closing of conference is the end. The secretary writes all things to manager and process in future</p>
												        </p>
												    </div>
												</div>
												<div class="tz_meetup_box_detail">
												    <div class="tz_meetup_box_detail_custom">
												        <span class="tz_meetup_start_time">
												            16:00PM        
												        </span>
												        <h4>
												            Poster section    
												        </h4>
												        <p class="tz_event_time">
															<i class="fa fa-clock-o"></i>
												            16:00PM -
												            17:30PM
												            <p>Everyone in meeting will summaries ideas which gave during the conference. The closing of conference is the end. The secretary writes all things to manager and process in future</p>
												        </p>
												    </div>
												</div>
												<div class="tz_meetup_box_detail">
												    <div class="tz_meetup_box_detail_custom">
												        <span class="tz_meetup_start_time">
												            18:30PM        
												        </span>
												        <h4>
												            Banquet    
												        </h4>
												        <p class="tz_event_time">
															<i class="fa fa-clock-o"></i>
												            18:30PM -
												            22:00PM
												            <p>Everyone in meeting will summaries ideas which gave during the conference. The closing of conference is the end. The secretary writes all things to manager and process in future</p>
												        </p>
												    </div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
            			</div>
            			<div id="tab2" class="wpb_tab ui-tabs-panel wpb_ui-tabs-hide vc_clearfix">
            				<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="tz_event_meetup">
								    <div class="tz_box_event_meetup">
								        <h3>
								            day 2       
								        </h3>
								        <h3 class="tz_event_meetup_subtitle">
								            <em>november 29, 2016</em>        
								        </h3>
								        <div class="tz_event_meettup_box_content">
								            <div class="tz_event_meetup_content">
								                <div class="tz_meetup_box_detail">
												    <div class="tz_meetup_box_detail_custom">
												        <span class="tz_meetup_start_time">
												            8:00AM        
												        </span>
												        <h4>
												            Opening and two plenary talks (Big room)      
												        </h4>
												        <p class="tz_event_time">
															<i class="fa fa-clock-o"></i>
												            9:00AM -
												            12:00AM
												            <p>Everyone in meeting will summaries ideas which gave during the conference. The closing of conference is the end. The secretary writes all things to manager and process in future</p>
												        </p>
												    </div>
												</div>
												<div class="tz_meetup_box_detail">
												    <div class="tz_meetup_box_detail_custom">
												        <span class="tz_meetup_start_time">
												            12:00AM        
												        </span>
												        <h4>
												            Lunch break     
												        </h4>
												        <p class="tz_event_time">
															<i class="fa fa-clock-o"></i>
												            12:00AM -
												            14:00PM
												            <p>Everyone in meeting will summaries ideas which gave during the conference. The closing of conference is the end. The secretary writes all things to manager and process in future</p>
												        </p>
												    </div>
												</div>
												<div class="tz_meetup_box_detail">
												    <div class="tz_meetup_box_detail_custom">
												        <span class="tz_meetup_start_time">
												            14:00PM        
												        </span>
												        <h4>
												            Four parallel  sections (4 small rooms)      
												        </h4>
												        <p class="tz_event_time">
															<i class="fa fa-clock-o"></i>
												            14:00PM -
												            16:00PM
												            <p>Everyone in meeting will summaries ideas which gave during the conference. The closing of conference is the end. The secretary writes all things to manager and process in future</p>
												        </p>
												    </div>
												</div>
												<div class="tz_meetup_box_detail">
												    <div class="tz_meetup_box_detail_custom">
												        <span class="tz_meetup_start_time">
												            16:00PM        
												        </span>
												        <h4>
												            Poster section    
												        </h4>
												        <p class="tz_event_time">
															<i class="fa fa-clock-o"></i>
												            16:00PM -
												            17:30PM
												            <p>Everyone in meeting will summaries ideas which gave during the conference. The closing of conference is the end. The secretary writes all things to manager and process in future</p>
												        </p>
												    </div>
												</div>
												<div class="tz_meetup_box_detail">
												    <div class="tz_meetup_box_detail_custom">
												        <span class="tz_meetup_start_time">
												            18:30PM        
												        </span>
												        <h4>
												            Banquet    
												        </h4>
												        <p class="tz_event_time">
															<i class="fa fa-clock-o"></i>
												            18:30PM -
												            22:00PM
												            <p>Everyone in meeting will summaries ideas which gave during the conference. The closing of conference is the end. The secretary writes all things to manager and process in future</p>
												        </p>
												    </div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
            			</div>
            			<div id="tab3" class="wpb_tab ui-tabs-panel wpb_ui-tabs-hide vc_clearfix">
            				<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="tz_event_meetup">
								    <div class="tz_box_event_meetup">
								        <h3>
								            day 2       
								        </h3>
								        <h3 class="tz_event_meetup_subtitle">
								            <em>november 30, 2016</em>        
								        </h3>
								        <div class="tz_event_meettup_box_content">
								            <div class="tz_event_meetup_content">
								                <div class="tz_meetup_box_detail">
												    <div class="tz_meetup_box_detail_custom">
												        <span class="tz_meetup_start_time">
												            8:00AM        
												        </span>
												        <h4>
												            Opening and two plenary talks (Big room)      
												        </h4>
												        <p class="tz_event_time">
															<i class="fa fa-clock-o"></i>
												            9:00AM -
												            12:00AM
												            <p>Everyone in meeting will summaries ideas which gave during the conference. The closing of conference is the end. The secretary writes all things to manager and process in future</p>
												        </p>
												    </div>
												</div>
												<div class="tz_meetup_box_detail">
												    <div class="tz_meetup_box_detail_custom">
												        <span class="tz_meetup_start_time">
												            12:00AM        
												        </span>
												        <h4>
												            Lunch break     
												        </h4>
												        <p class="tz_event_time">
															<i class="fa fa-clock-o"></i>
												            12:00AM -
												            14:00PM
												            <p>Everyone in meeting will summaries ideas which gave during the conference. The closing of conference is the end. The secretary writes all things to manager and process in future</p>
												        </p>
												    </div>
												</div>
												<div class="tz_meetup_box_detail">
												    <div class="tz_meetup_box_detail_custom">
												        <span class="tz_meetup_start_time">
												            14:00PM        
												        </span>
												        <h4>
												            Four parallel  sections (4 small rooms)      
												        </h4>
												        <p class="tz_event_time">
															<i class="fa fa-clock-o"></i>
												            14:00PM -
												            16:00PM
												            <p>Everyone in meeting will summaries ideas which gave during the conference. The closing of conference is the end. The secretary writes all things to manager and process in future</p>
												        </p>
												    </div>
												</div>
												<div class="tz_meetup_box_detail">
												    <div class="tz_meetup_box_detail_custom">
												        <span class="tz_meetup_start_time">
												            16:00PM        
												        </span>
												        <h4>
												            Poster section    
												        </h4>
												        <p class="tz_event_time">
															<i class="fa fa-clock-o"></i>
												            16:00PM -
												            17:30PM
												            <p>Everyone in meeting will summaries ideas which gave during the conference. The closing of conference is the end. The secretary writes all things to manager and process in future</p>
												        </p>
												    </div>
												</div>
												<div class="tz_meetup_box_detail">
												    <div class="tz_meetup_box_detail_custom">
												        <span class="tz_meetup_start_time">
												            18:30PM        
												        </span>
												        <h4>
												            Banquet    
												        </h4>
												        <p class="tz_event_time">
															<i class="fa fa-clock-o"></i>
												            18:30PM -
												            22:00PM
												            <p>Everyone in meeting will summaries ideas which gave during the conference. The closing of conference is the end. The secretary writes all things to manager and process in future</p>
												        </p>
												    </div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
            			</div>
            		</div>
            	</div>
            </div>
        </div>
    </section>

    <section id="feature" class="feature-section">
        <div class="container" style="padding:0;">
        	<div class="row" style="margin:0 0 10px;">
        		<div class="col-md-12 col-sm-12 col-xs-12">
        			<h3 class="conference">
		        		organizers
		        	</h3>
	        		<div class="line"></div>
        		</div>
        	</div>
        	<div class="row" style="margin:0;">
        		<div class="col-md-6 col-sm-6 col-xs-12">
        			<div class="row textunderline" style="margin: 0 0 10px;">
	        			<center>
	        				<img src="img/about.png" class="img-responsive" />
	        			</center>
	        		</div>
        		</div>
	        	<div class="col-md-6 col-sm-6 col-xs-12" style="font-weight: 300;font-size:16px;line-height:24px;color:#888;">
	        		Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo.
	        	</div>
	        </div>
	        <div class="row" style="margin: 10px 0 0;">
	        	<div class="col-md-12 col-sm-12 col-xs-12">
	        		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a class="faq-toggle collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne"> I. Organising Committee </a>
                                </h4>
                            </div>

                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    1. Prof. Phan Minh Tan, Honorary Chair<br/>
									Department of Science and Technology, Ho Chi Minh City, Vietnam.<br/>
									<br/>
									2. Prof. Tran Cong Thanh, Chair.<br/>
									University of Southern Queensland, Toowoomba, QLD, Australia.<br/>
									<br/>
									3. Prof. Truong Nguyen Thanh.<br/>
									Department of Chemistry, University of Utah, USA.<br/>
									<br/>
									4. Prof. Nguyen Van Thinh.<br/>
									Department of Civil and Environmental Engineering, Seoul National University, Seoul, Korea.<br/>
									<br/>
									5. Prof. Nguyen Van Hien.<br/>
									Department of Mathematics, University of Namur, Namur, Belgium.<br/>
									<br/>
									6. Prof. Vuong Thanh Son.<br/>
									University of British Columbia.<br/>
									<br/>
									7. Prof. Tran Quoc Nam.<br/>
									University of South Dakota.
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="faq-toggle collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">II. Scientific Committee</a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                	1. Prof. Phan Minh Tan, Honorary Chair<br/>
									Department of Science and Technology, Ho Chi Minh City, Vietnam.<br/>
									<br/>
									2. Prof. Tran Cong Thanh, Chair.<br/>
									University of Southern Queensland, Toowoomba, QLD, Australia.<br/>
									<br/>
									3. Prof. Truong Nguyen Thanh.<br/>
									Department of Chemistry, University of Utah, USA.<br/>
									<br/>
									4. Prof. Nguyen Van Thinh.<br/>
									Department of Civil and Environmental Engineering, Seoul National University, Seoul, Korea.<br/>
									<br/>
									5. Prof. Nguyen Van Hien.<br/>
									Department of Mathematics, University of Namur, Namur, Belgium.<br/>
									<br/>
									6. Prof. Vuong Thanh Son.<br/>
									University of British Columbia.<br/>
									<br/>
									7. Prof. Tran Quoc Nam.<br/>
									University of South Dakota.
                                </div>
                            </div>
                        </div>
  
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="faq-toggle collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> III. Scientific Committee Associates</a>
                                </h4>
                            </div>

                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                	1. Prof. Phan Minh Tan, Honorary Chair<br/>
									Department of Science and Technology, Ho Chi Minh City, Vietnam.<br/>
									<br/>
									2. Prof. Tran Cong Thanh, Chair.<br/>
									University of Southern Queensland, Toowoomba, QLD, Australia.<br/>
									<br/>
									3. Prof. Truong Nguyen Thanh.<br/>
									Department of Chemistry, University of Utah, USA.<br/>
									<br/>
									4. Prof. Nguyen Van Thinh.<br/>
									Department of Civil and Environmental Engineering, Seoul National University, Seoul, Korea.<br/>
									<br/>
									5. Prof. Nguyen Van Hien.<br/>
									Department of Mathematics, University of Namur, Namur, Belgium.<br/>
									<br/>
									6. Prof. Vuong Thanh Son.<br/>
									University of British Columbia.<br/>
									<br/>
									7. Prof. Tran Quoc Nam.<br/>
									University of South Dakota.
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFour">
                                <h4 class="panel-title">
                                    <a class="faq-toggle collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">IV. Local Organizing Committee</a>
                                </h4>
                            </div>

                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                	1. Prof. Phan Minh Tan, Honorary Chair<br/>
									Department of Science and Technology, Ho Chi Minh City, Vietnam.<br/>
									<br/>
									2. Prof. Tran Cong Thanh, Chair.<br/>
									University of Southern Queensland, Toowoomba, QLD, Australia.<br/>
									<br/>
									3. Prof. Truong Nguyen Thanh.<br/>
									Department of Chemistry, University of Utah, USA.<br/>
									<br/>
									4. Prof. Nguyen Van Thinh.<br/>
									Department of Civil and Environmental Engineering, Seoul National University, Seoul, Korea.<br/>
									<br/>
									5. Prof. Nguyen Van Hien.<br/>
									Department of Mathematics, University of Namur, Namur, Belgium.<br/>
									<br/>
									6. Prof. Vuong Thanh Son.<br/>
									University of British Columbia.<br/>
									<br/>
									7. Prof. Tran Quoc Nam.<br/>
									University of South Dakota.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
	        </div>
        </div>
    </section>

    <section id="latest-news" class="latest-news-section" style="background:url('img/cart.png') no-repeat center;background-size:cover;min-height:480px;padding: 30px 0;" >
        <div class="container" style="padding:0;">
            <div class="row" style="margin:0;">
            	<div class="col-md-12 col-sm-12 col-xs-12" >
	            	<div class="col-sm-6 col-md-6 col-xs-12" id="packetleft">
	            		<div class="row" style="margin:40px 0 20px;background: #212430;">
	            			<div class="col-sm-8 col-md-8 col-xs-8" style="padding:20px;min-height:170px;">
	            				<h3 style="color:#fff;">Only Conference</h3>
	            				<p style="color:#fff;font-size: 16px;line-height: 24px;">This is a conference which is celebrated within University. All members belong university can participate in this conference for discussing</p>
	            			</div>
	            			<div class="col-sm-4 col-md-4 col-xs-4" style="padding:0;min-height:170px;background:#2969b0;">
	            				<div style="text-align:center;">
	            					<br/><br/>
	            					<b style="color:#fff;font-size:60px;">80$</b><br/>
	            					<a href="#" class="addtocart">ADD TO CART</a>
	            				</div>
	            			</div>
	            		</div>
	            		<div class="row" style="margin:0;background: #212430;">
	            			<div class="col-sm-8 col-md-8 col-xs-8" style="padding:20px;min-height:170px;">
	            				<h3 style="color:#fff;">Only Conference</h3>
	            				<p style="color:#fff;font-size: 16px;line-height: 24px;">This is a conference which is celebrated within University. All members belong university can participate in this conference for discussing</p>
	            			</div>
	            			<div class="col-sm-4 col-md-4 col-xs-4" style="padding:0;min-height:170px;background:#2969b0;">
	            				<div style="text-align:center;">
	            					<br/><br/>
	            					<b style="color:#fff;font-size:60px;">180$</b><br/>
	            					<a href="#" class="addtocart">ADD TO CART</a>
	            				</div>
	            			</div>
	            		</div>
	            	</div>
	            	<div class="col-sm-6 col-md-6 col-xs-12" id="packetright">
	            		<div class="row" style="margin:40px 0 20px;background: #212430;">
	            			<div class="col-sm-8 col-md-8 col-xs-8" style="padding:20px;min-height:170px;">
	            				<h3 style="color:#fff;">Only Conference</h3>
	            				<p style="color:#fff;font-size: 16px;line-height: 24px;">This is a conference which is celebrated within University. All members belong university can participate in this conference for discussing</p>
	            			</div>
	            			<div class="col-sm-4 col-md-4 col-xs-4" style="padding:0;min-height:170px;background:#2969b0;">
	            				<div style="text-align:center;">
	            					<br/><br/>
	            					<b style="color:#fff;font-size:60px;">100$</b><br/>
	            					<a href="#" class="addtocart">ADD TO CART</a>
	            				</div>
	            			</div>
	            		</div>
	            		<div class="row" style="margin:0;background: #212430;">
	            			<div class="col-sm-8 col-md-8 col-xs-8" style="padding:20px;min-height:170px;">
	            				<h3 style="color:#fff;">Only Conference</h3>
	            				<p style="color:#fff;font-size: 16px;line-height: 24px;">This is a conference which is celebrated within University. All members belong university can participate in this conference for discussing</p>
	            			</div>
	            			<div class="col-sm-4 col-md-4 col-xs-4" style="padding:0;min-height:170px;background:#2969b0;">
	            				<div style="text-align:center;">
	            					<br/><br/>
	            					<b style="color:#fff;font-size:60px;">200$</b><br/>
	            					<a href="#" class="addtocart">ADD TO CART</a>
	            				</div>
	            			</div>
	            		</div>
	            	</div>
	            </div>
            </div>
        </div>
    </section>

    <section id="feature" class="feature-section" style="padding: 10px 0;">
        <div class="container" style="padding:0;">
        	<div class="row" style="margin:0;text-align:center;">
        		<div class="col-md-12 col-sm-12 col-xs-12">
        			<h3 class="conference">
		        		VIEW YOUR CARD (1) AND PAYMENT NOW!
		        	</h3>
        		</div>
        	</div>
        </div>
    </section>

    <section id="latest-news" class="latest-news-section" style="background:url('img/sponsor.png') no-repeat center;background-size:cover;min-height:480px;padding: 50px 0;" >
    	<div class="container">
    		<div class="row" style="margin:20px 0;text-align:center;">
    			<h3 class="conference" style="color: #fff;">
		        	DIAMOND SPONSOR
		        </h3>
		        <center>
		        	<img src="img/linesponsor.png" class="img-responsive" />
		        </center>
    		</div>
    		<div class="row" style="margin:20px 0;">
    			<div class="latest-news1">
                	<div class="col-md-12 col-sm-12 col-xs-12">
                		<div class="frame">
	                		<center>
	                			<img src="img/logo-meetup-1.png" class="img-responsive" />
	                		</center>
	                	</div>
                	</div>
                	<div class="col-md-12 col-sm-12 col-xs-12">
                		<div class="frame">
	                		<center>
	                			<img src="img/logo-meetup-3.png" class="img-responsive" />
	                		</center>
	                	</div>
                	</div>
                	<div class="col-md-12 col-sm-12 col-xs-12">
                		<div class="frame">
	                		<center>
	                			<img src="img/logo-meetup-4.png" class="img-responsive" />
	                		</center>
	                	</div>
                	</div>
                	<div class="col-md-12 col-sm-12 col-xs-12">
                		<div class="frame">
	                		<center>
	                			<img src="img/logo-meetup-5.png" class="img-responsive" />
	                		</center>
	                	</div>
                	</div>
                	<div class="col-md-12 col-sm-12 col-xs-12">
                		<div class="frame">
	                		<center>
	                			<img src="img/logo-meetup-6.png" class="img-responsive" />
	                		</center>
	                	</div>
                	</div>
                	<div class="col-md-12 col-sm-12 col-xs-12">
                		<div class="frame">
	                		<center>
	                			<img src="img/logo-meetup-7.png" class="img-responsive" />
	                		</center>
	                	</div>
                	</div>
               	</div>
    		</div>
    		<div class="row" style="margin:20px 0;text-align:center;">
    			<h3 class="conference" style="color: #fff;">
		        	GOLD SPONSOR
		        </h3>
		        <center>
		        	<img src="img/linesponsor.png" class="img-responsive" />
		        </center>
    		</div>
    		<div class="row" style="margin:20px 0;">
    			<div class="latest-news2">
                	<div class="col-md-12 col-sm-12 col-xs-12">
                		<div class="frame">
	                		<center>
	                			<img src="img/logo-meetup-1.png" class="img-responsive" />
	                		</center>
	                	</div>
                	</div>
                	<div class="col-md-12 col-sm-12 col-xs-12">
                		<div class="frame">
	                		<center>
	                			<img src="img/logo-meetup-3.png" class="img-responsive" />
	                		</center>
	                	</div>
                	</div>
                	<div class="col-md-12 col-sm-12 col-xs-12">
                		<div class="frame">
	                		<center>
	                			<img src="img/logo-meetup-4.png" class="img-responsive" />
	                		</center>
	                	</div>
                	</div>
                	<div class="col-md-12 col-sm-12 col-xs-12">
                		<div class="frame">
	                		<center>
	                			<img src="img/logo-meetup-5.png" class="img-responsive" />
	                		</center>
	                	</div>
                	</div>
                	<div class="col-md-12 col-sm-12 col-xs-12">
                		<div class="frame">
	                		<center>
	                			<img src="img/logo-meetup-6.png" class="img-responsive" />
	                		</center>
	                	</div>
                	</div>
                	<div class="col-md-12 col-sm-12 col-xs-12">
                		<div class="frame">
	                		<center>
	                			<img src="img/logo-meetup-7.png" class="img-responsive" />
	                		</center>
	                	</div>
                	</div>
               	</div>
    		</div>
    		<div class="row" style="margin:20px 0;text-align:center;">
    			<h3 class="conference" style="color: #fff;">
		        	SILVER SPONSOR
		        </h3>
		        <center>
		        	<img src="img/linesponsor.png" class="img-responsive" />
		        </center>
    		</div>
    		<div class="row" style="margin:20px 0;">
    			<div class="latest-news3">
                	<div class="col-md-12 col-sm-12 col-xs-12">
                		<div class="frame">
	                		<center>
	                			<img src="img/logo-meetup-1.png" class="img-responsive" />
	                		</center>
	                	</div>
                	</div>
                	<div class="col-md-12 col-sm-12 col-xs-12">
                		<div class="frame">
	                		<center>
	                			<img src="img/logo-meetup-3.png" class="img-responsive" />
	                		</center>
	                	</div>
                	</div>
                	<div class="col-md-12 col-sm-12 col-xs-12">
                		<div class="frame">
	                		<center>
	                			<img src="img/logo-meetup-4.png" class="img-responsive" />
	                		</center>
	                	</div>
                	</div>
                	<div class="col-md-12 col-sm-12 col-xs-12">
                		<div class="frame">
	                		<center>
	                			<img src="img/logo-meetup-5.png" class="img-responsive" />
	                		</center>
	                	</div>
                	</div>
                	<div class="col-md-12 col-sm-12 col-xs-12">
                		<div class="frame">
	                		<center>
	                			<img src="img/logo-meetup-6.png" class="img-responsive" />
	                		</center>
	                	</div>
                	</div>
                	<div class="col-md-12 col-sm-12 col-xs-12">
                		<div class="frame">
	                		<center>
	                			<img src="img/logo-meetup-7.png" class="img-responsive" />
	                		</center>
	                	</div>
                	</div>
               	</div>
    		</div>
    	</div>
    </section>

    <section id="feature" class="feature-section">
        <div class="container" style="padding:0;">
        	<div class="row" style="margin:0;text-align:center;">
        		<div class="col-md-6 col-sm-6 col-xs-12" style="padding:0;">
        			<div class="google-maps">
    					<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3919.4651133438456!2d106.7010005!3d10.7756446!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752f479e4f95db%3A0xcae127d5b519b548!2zS2jDoWNoIFPhuqFuIFJleA!5e0!3m2!1svi!2s!4v1468235007170" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        			</div>
        		</div>
        		<div class="col-md-6 col-sm-6 col-xs-12" style="border:1px solid #e6e6e6;border-left:0;padding: 0;">
        			<div class="row" style="margin:0;border-bottom:1px solid #e6e6e6;">
        				<h3>Contact form</h3>
        			</div>
        			<div class="row" style="margin:0;padding:20px 0;background:#f7f7f7;">
        				<div class="col-sm-12 col-md-12 col-xs-12">
	        				<form action="#" method="post">
		                        <input type="text" name="first-name" style="width: 49%;border: 0;height:35px;margin-right: 5px;padding: 5px;margin-bottom:10px;background: #fff;" placeholder="Name" required="">
		                        <input type="text" name="last-name" style="width: 49%;border: 0;height:35px;padding: 5px;margin-bottom:10px;background: #fff;" placeholder="Email" required="">
		                        <input type="email" name="your-email" style="width: 100%;border: 0;height:35px;margin-right: 5px;padding: 5px;margin-bottom:10px;background: #fff;" placeholder="Website" required="">
		                        <textarea name="your-message" style="width:100%;padding: 5px;border: 0;height: 120px;resize: none;background: #fff;" placeholder="Message" required=""></textarea>
		                        <br><br>
		                        <center>
		                            <button type="submit" class="btn buttonblue2 btn-min-block" style="text-align:center;border-radius:3px;">
		                                Send message
		                            </button>
		                        </center>
		                    </form>
		                </div>
        			</div>
        		</div>
        	</div>
        </div>
    </section>

    <section id="feature" class="feature-section" style="background: #24293a;padding:0;border-bottom:1px solid #888;">
        <div class="container" style="padding:0;">
        	<div class="row" style="margin:0;padding: 30px 0;">
        		<div class="col-sm-4 col-md-4 col-xs-12">
        			<div class="row" style="margin:0;">
        				<div class="col-sm-2 col-md-2 col-xs-6">
        					<img src="img/iphone.png" class="img-responsive"/>
        				</div>
        				<div class="col-sm-6 col-md-6 col-xs-6" style="color:#fff;">
        					Call us<br/>
        					+84 1665426965
        				</div>
        			</div>
        		</div>
        		<div class="col-sm-4 col-md-4 col-xs-12">
        			<div class="row" style="margin:0;">
        				<div class="col-sm-2 col-md-2 col-xs-6">
        					<img src="img/location.png" class="img-responsive"/>
        				</div>
        				<div class="col-sm-6 col-md-6 col-xs-6" style="color:#fff;">
        					Location<br/>
        					+84 1665426965
        				</div>
        			</div>
        		</div>
        		<div class="col-sm-4 col-md-4 col-xs-12">
        			<div class="row" style="margin:0;">
        				<div class="col-sm-2 col-md-2 col-xs-6">
        					<img src="img/mail.png" class="img-responsive"/>
        				</div>
        				<div class="col-sm-6 col-md-6 col-xs-6" style="color:#fff;">
        					Mail<br/>
        					<span style="color:#be926f;">icstconference@icst.org.vn</span>
        				</div>
        			</div>
        		</div>
        	</div>
    	</div>
   	</section>
   	<section id="feature" class="feature-section" style="background: #24293a;padding:0;border-bottom:1px solid #888;">
   		<div class="container" style="padding:0;">
        	<div class="row" style="margin:0;padding: 30px 0;">
        		<div class="col-sm-9 col-md-9 col-xs-12">
        			<ul class="menufooter">
        				<li>
        					<a href="#">Summision</a>
        				</li>
        				<li>
        					<a href="#">support</a>
        				</li>
        				<li>
        					<a href="#">payment</a>
        				</li>
        				<li>
        					<a href="#">history</a>
        				</li>
        			</ul>
        		</div>
        		<div class="col-sm-3 col-md-33 col-xs-12" style="text-align:right;">
        			<center>
        				<ul class="menufooter" style="text-align: right;">
	        				<li>
	        					<a href="#"><i class="fa fa-facebook"></i></a>
	        				</li>
	        				<li>
	        					<a href="#"><i class="fa fa-google"></i></a>
	        				</li>
	        				<li>
	        					<a href="#"><i class="fa fa-twitter"></i></a>
	        				</li>
	        			</ul>
        			</center>
        		</div>
        	</div>
        </div>
    </section>

    <section id="feature" class="feature-section" style="background: #24293a;padding:0;">
    	<div class="container" style="padding:0;">
    		<div class="row" style="margin:0;padding: 30px 0;text-align:center;color:#fff;">
    			2016 ICST conference. All rights reserved
    		</div>
    	</div>
    </section>

    <script src="js/jquery-2.1.1.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/jquery.appear.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.fitvids.js"></script>

    <script type='text/javascript' src='js/js_composer_front.min.js'></script>
    <script type='text/javascript' src='js/widget.min.js'></script>
	<script type='text/javascript' src='js/tabs.min.js'></script>
	<script type='text/javascript' src='js/jquery-ui-tabs-rotate.min.js'></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/script.js"></script>

    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

  <script type="text/javascript">
	$(document).ready(function(){
		
		//Check to see if the window is top if not then display button
		$(window).scroll(function(){
			if ($(this).scrollTop() > 100) {
				$('.scrollToTop').fadeIn();
			} else {
				$('.scrollToTop').fadeOut();
			}
		});
		
		//Click event to scroll to top
		$('.scrollToTop').click(function(){
			$('html, body').animate({scrollTop : 0},800);
			return false;
		});
		
	});
  </script>
    <script type="text/javascript">
        $(document).ready(function(){
                //menu
                var url = String(window.location);
                url = url.split("?");
                url = url[0];
                url = url.replace("#","");
				url = url.replace("/vi","");
				url = url.replace("/en","");
                // Will only work if string in href matches with location
                var nodeParent = $('#bs-example-navbar-collapse-1 li a[href="'+ url +'"]').parents();
                if(nodeParent[2].nodeName.toLowerCase() !== 'li')
                {
					console.log(1);
                    $('#bs-example-navbar-collapse-1 li a[href="'+ url +'"]').parent().addClass('active');
                }
                else {
					console.log(2);
                    $('#bs-example-navbar-collapse-1 li a[href="'+ url +'"]').closest('li.has-sub').addClass('active');
                }
                // Will also work for relative and absolute hrefs
            });
    </script>
</body>
</html>