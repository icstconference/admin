<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>Trang quản trị Conference</title>

		<meta name="keywords" content="" />
		<meta name="description" content="Trang quản trị Conference">
		<link rel="shortcut icon" href="<?php echo URL;?>public/img/favicon.ico" />

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo URL;?>public/assets/vendor/bootstrap/css/bootstrap.css" />

		<link rel="stylesheet" href="<?php echo URL;?>public/assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="<?php echo URL;?>public/assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="<?php echo URL;?>public/assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo URL;?>public/assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?php echo URL;?>public/assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo URL;?>public/assets/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="<?php echo URL;?>public/assets/vendor/modernizr/modernizr.js"></script>
		<style type="text/css">
			.body-sign .panel-sign .panel-title-sign .title {
			    background-color: #2969b0;
			}
			.body-sign .panel-sign .panel-body {
			    border-top-color: #2969b0;
			}
			body .btn-primary {
			    color: #ffffff;
			    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
			    background-color: #2969b0;
			    border-color: #2969b0;
			}
		</style>
	</head>
	<body style="background:#dfdfdf;">
		<?php 
			echo $this->content();
		?>
		<!-- end: page -->

		<!-- Vendor -->
		<script src="<?php echo URL;?>public/assets/vendor/jquery/jquery.js"></script>
		<script src="<?php echo URL;?>public/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="<?php echo URL;?>public/assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="<?php echo URL;?>public/assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="<?php echo URL;?>public/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="<?php echo URL;?>public/assets/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="<?php echo URL;?>public/assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo URL;?>public/assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo URL;?>public/assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo URL;?>public/assets/javascripts/theme.init.js"></script>

	</body>
</html>