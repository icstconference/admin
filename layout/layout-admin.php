<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo $this->title;?></title>
    <meta name="description" content="description">
    <meta name="author" content="<?php echo $this->title;?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?php echo URL;?>public/plugins/bootstrap/bootstrap.css" rel="stylesheet">
    <link href="<?php echo URL;?>public/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
    <link href="<?php echo URL;?>public/plugins/select2/select2.css" rel="stylesheet">
    <link href="<?php echo URL;?>public/css/style-admin.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="http://getbootstrap.com/docs-assets/js/html5shiv.js"></script>
    <script src="http://getbootstrap.com/docs-assets/js/respond.min.js"></script>
    <![endif]-->
	<!-- cdn for modernizr, if you haven't included it already -->
		<script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
		<!-- polyfiller file to detect and load polyfills -->
		<script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
		<script>
		  webshims.setOptions('waitReady', false);
		  webshims.setOptions('forms-ext', {types: 'date'});
		  webshims.polyfill('forms forms-ext');
		</script>
    <?php
        $this->appendArrayStyleSheet($this->css);
    ?>
    <script>
        var URL_G = '<?php echo URL;?>';
        var lang = '<?php echo $this->lang;?>';
    </script>
</head>
<body>
	<span id="checkadmin" rel="<?php echo $_SESSION['user_id'];?>"></span>
    <!--Start Header-->
    <div id="screensaver">
        <canvas id="canvas"></canvas>
        <i class="fa fa-lock" id="screen_unlock"></i>
    </div>
    <div id="modalbox">
        <div class="devoops-modal">
            <div class="devoops-modal-inner">
            </div>
            <div class="devoops-modal-bottom">
            </div>
        </div>
    </div>
    <header class="navbar">
        <div class="container-fluid expanded-panel">
            <div class="row">
                <div id="logo" class="col-xs-12 col-sm-2">
                    <a href="<?php echo URL;?>">
                        Trang quản trị
                    </a>
                </div>
                <div id="top-panel" class="col-xs-12 col-sm-10">
                    <div class="row">
                        <div class="col-xs-8 col-sm-4">
                            <a href="#" class="show-sidebar">
                                <i class="fa fa-bars"></i>
                            </a>
                        </div>
                        <div class="col-xs-4 col-sm-8 top-panel-right">
                            <ul class="nav navbar-nav pull-right panel-menu">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle account" data-toggle="dropdown">
                                        <div class="avatar">
                                            <img src="<?php echo URL;?>public/img/defavatar.png" class="img-rounded" alt="avatar" />
                                        </div>
                                        <i class="fa fa-angle-down pull-right"></i>
                                        <div class="user-mini pull-right">
                                            <span class="welcome">Chào,</span>
                                            <span><?php echo lib_session::Get("user_name");?></span>
                                        </div>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="<?php echo URL;?>">
                                                <i class="fa fa-user"></i>
                                                <span>Xem website</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo URL;?>logout">
                                                <i class="fa fa-power-off"></i>
                                                <span>Đăng xuất</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--End Header-->
    <!--Start Container-->
    <div id="main" class="container-fluid" style="background: #666666;">
        <div class="row">
            <div id="sidebar-left" class="col-xs-2 col-sm-2">
                <ul class="nav main-menu">
					<?php
						if($_SESSION['user_id'] == 1){
					?>
                    <li>
                        <a href="<?php echo URL;?>admin/">
                            <i class="fa fa-dashboard"></i>
                            <span class="hidden-xs">Thống kê</span>
                        </a>
                    </li>
					<?php } ?>
                    <?php 
						$create = $_SESSION['user_permission_create'];
						$delete = $_SESSION['user_permission_delete'];
						$edit = $_SESSION['user_permission_edit'];
					
                         if($_SESSION['user_id'] == 1){
                    ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle">
                            <i class="fa fa-users"></i>
                            <span class="hidden-xs">Quản trị người dùng</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!--<li><a href="<?php echo URL;?>user/admin">Danh sách admin</a></li>-->
                            <li><a href="<?php echo URL;?>user/">Danh sách admin</a></li>
                            <li><a href="<?php echo URL.$this->lang;?>/user/permission">Danh sách quyền</a></li>
							<li><a href="<?php echo URL.$this->lang;?>/user/permissionnews">Phân quyền viết bài</a></li>
                        </ul>
                    </li>
					<?php } ?>
					<?php if(strpos($create[0]['permission_detail'],'new') || strpos($edit[0]['permission_detail'],'new') || strpos($delete[0]['permission_detail'],'new') || $_SESSION['user_id'] == 1){ ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle">
                            <i class="fa fa-file-text"></i>
                            <span class="hidden-xs">Quản trị tin tức</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo URL;?>news/categories">Danh sách loại tin tức</a></li>
                            <li><a href="<?php echo URL;?>news/">Danh sách tin tức</a></li>
                        </ul>
                    </li>
					<?php } ?>
					<?php 
						if($_SESSION['user_id'] == 1){
					?>
					<li class="dropdown">
                        <a href="#" class="dropdown-toggle">
                            <i class="fa fa-user"></i>
                            <span class="hidden-xs">Quản trị nhân sự</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo URL;?>personal/categories">Danh sách loại nhân sự</a></li>
                            <li><a href="<?php echo URL;?>personal/lab">Danh sách nhân sự lab</a></li>
							<li><a href="<?php echo URL;?>personal/">Danh sách nhân sự</a></li>
                        </ul>
                    </li>
					<?php } ?>
					<?php if(strpos($create[0]['permission_detail'],'personal') || strpos($edit[0]['permission_detail'],'personal') || strpos($delete[0]['permission_detail'],'personal')){ ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle">
                            <i class="fa fa-user"></i>
                            <span class="hidden-xs">Quản trị nhân sự</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo URL;?>personal/categories">Danh sách loại nhân sự</a></li>
                            <?php 
								if(strpos($create[0]['permission_detail'],'lab') || strpos($edit[0]['permission_detail'],'lab') || strpos($delete[0]['permission_detail'],'lab')){
							?>
							<li><a href="<?php echo URL;?>personal/lab">Danh sách nhân sự lab</a></li>
							<?php } ?>
							<li><a href="<?php echo URL;?>personal/">Danh sách nhân sự</a></li>
                        </ul>
                    </li>
					<?php } ?>
					<?php 
						if((strpos($create[0]['permission_detail'],'lab') || strpos($edit[0]['permission_detail'],'lab') || strpos($delete[0]['permission_detail'],'lab')) && (!strpos($create[0]['permission_detail'],'personal') && !strpos($edit[0]['permission_detail'],'personal') && !strpos($delete[0]['permission_detail'],'personal'))){
					?>
					<li class="dropdown">
                        <a href="#" class="dropdown-toggle">
                            <i class="fa fa-user"></i>
                            <span class="hidden-xs">Quản trị nhân sự</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo URL;?>personal/lab/">Danh sách nhân sự lab</a></li>
                        </ul>
                    </li>
					<?php } ?>
                    <!-- <li>
                        <a href="<?php echo URL;?>event/event/">
                            <i class="fa fa-lightbulb-o"></i>
                            <span class="hidden-xs">Quản trị sự kiện</span>
                        </a>
                    </li> -->
					<?php if(strpos($create[0]['permission_detail'],'partner') || strpos($edit[0]['permission_detail'],'partner') || strpos($delete[0]['permission_detail'],'partner') || $_SESSION['user_id'] == 1){ ?>
                    <li>
                        <a href="<?php echo URL;?>partner/">
                            <i class="fa fa-globe"></i>
                            <span class="hidden-xs">Quản trị đối tác</span>
                        </a>
                    </li>
					<?php } ?>
					<?php if(strpos($create[0]['permission_detail'],'library') || strpos($edit[0]['permission_detail'],'library') || strpos($delete[0]['permission_detail'],'library') || $_SESSION['user_id'] == 1){ ?>
                    <li>
                        <a href="<?php echo URL;?>library/">
                            <i class="fa fa-picture-o"></i>
                            <span class="hidden-xs">Quản trị thư viện</span>
                        </a>
                    </li>
					<?php } ?>
					<?php if(strpos($create[0]['permission_detail'],'file') || strpos($edit[0]['permission_detail'],'file') || strpos($delete[0]['permission_detail'],'file') || $_SESSION['user_id'] == 1){ ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle">
                            <i class="fa fa-file"></i>
                            <span class="hidden-xs">Quản trị tài nguyên</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo URL;?>file/categories">Danh sách loại tài nguyên</a></li>
                            <li><a href="<?php echo URL;?>file/">Danh sách tài nguyên</a></li>
                        </ul>
                    </li>
					<?php } ?>
					<?php if(strpos($create[0]['permission_detail'],'video') || strpos($edit[0]['permission_detail'],'video') || strpos($delete[0]['permission_detail'],'video') || $_SESSION['user_id'] == 1){ ?>
                    <li>
                        <a href="<?php echo URL;?>video/">
                            <i class="fa fa-video-camera"></i>
                            <span class="hidden-xs">Quản trị video</span>
                        </a>
                    </li>
					<?php } ?>
					<?php if(strpos($create[0]['permission_detail'],'new') || strpos($edit[0]['permission_detail'],'new') || strpos($delete[0]['permission_detail'],'new') || $_SESSION['user_id'] == 1){ ?>
					<li class="dropdown">
                        <a href="#" class="dropdown-toggle">
                            <i class="fa fa-list-alt"></i>
                            <span class="hidden-xs">Quản trị form</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo URL;?>form/categories">Danh mục bài báo khoa học</a></li>
                            <li><a href="<?php echo URL;?>form/">Danh sách form</a></li>
                        </ul>
                    </li>
					<?PHP } ?>
                    <!-- <li>
                        <a href="<?php echo URL;?>advertise/">
                            <i class="fa fa-adn"></i>
                            <span class="hidden-xs">Quản trị quảng cáo</span>
                        </a>
                    </li> -->
					<?php if(strpos($create[0]['permission_detail'],'linker') || strpos($edit[0]['permission_detail'],'linker') || strpos($delete[0]['permission_detail'],'linker') || $_SESSION['user_id'] == 1){ ?>
                    <li>
                        <a href="<?php echo URL;?>linker/">
                            <i class="fa fa-link"></i>
                            <span class="hidden-xs">Quản trị liên kết</span>
                        </a>
                    </li>
					<?php } ?>
					<?php if(strpos($create[0]['permission_detail'],'menu') || strpos($edit[0]['permission_detail'],'menu') || strpos($delete[0]['permission_detail'],'menu') || $_SESSION['user_id'] == 1){ ?>
                    <li>
                        <a href="<?php echo URL;?>menu/">
                            <i class="fa fa-tasks"></i>
                            <span class="hidden-xs">Quản trị menu</span>
                        </a>
                    </li>
					<?php } ?>
					<?php if(strpos($create[0]['permission_detail'],'comment') || strpos($edit[0]['permission_detail'],'comment') || strpos($delete[0]['permission_detail'],'comment') || $_SESSION['user_id'] == 1){ ?>
                    <li>
                        <a href="<?php echo URL;?>comment/">
                            <i class="fa fa-comment"></i>
                            <span class="hidden-xs">Quản trị bình luận</span>
                        </a>
                    </li>
					<?php } ?>
                    <!-- <li>
                        <a href="<?php echo URL;?>email/">
                            <i class="fa fa-envelope-o"></i>
                            <span class="hidden-xs">Quản trị email đăng kí</span>
                        </a>
                    </li> -->
					<?php if(strpos($create[0]['permission_detail'],'question') || strpos($edit[0]['permission_detail'],'question') || strpos($delete[0]['permission_detail'],'question') || $_SESSION['user_id'] == 1){ ?>
                    <li>
                        <a href="<?php echo URL;?>contact/">
                            <i class="fa fa-rss"></i>
                            <span class="hidden-xs">Quản trị hỏi đáp</span>
                        </a>
                    </li>
					<?php } ?>
					<?php if(strpos($create[0]['permission_detail'],'contact') || strpos($edit[0]['permission_detail'],'contact') || strpos($delete[0]['permission_detail'],'contact') || $_SESSION['user_id'] == 1){ ?>
                    <li>
                        <a href="<?php echo URL;?>admin/contact/" >
                            <i class="fa fa-phone"></i>
                            <span class="hidden-xs">Liên hệ</span>
                        </a>
                    </li>
					<?php } ?>
					<?php
						if($_SESSION['user_id'] == 1){
					?>
                    <li>
                        <a href="<?php echo URL;?>admin/setting/" >
                            <i class="fa fa-cog"></i>
                            <span class="hidden-xs">Cấu hình website</span>
                        </a>
                    </li>
					<?php } ?>
                    <!--<li class="dropdown">
                        <a href="#" class="dropdown-toggle">
                            <i class="fa fa-table"></i>
                            <span class="hidden-xs">Tables</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="ajax-link" href="ajax/tables_simple.html">Simple Tables</a></li>
                            <li><a class="ajax-link" href="ajax/tables_datatables.html">Data Tables</a></li>
                            <li><a class="ajax-link" href="ajax/tables_beauty.html">Beauty Tables</a></li>
                        </ul>
                    </li>-->
                </ul>
            </div>
            <!--Start Content-->
            <div id="content" class="col-xs-12 col-sm-10" style="background: #fff;">
                <?php echo $this->content();?>
            </div>
            <!--End Content-->
        </div>
    </div>
    <!--End Container-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!--<script src="http://code.jquery.com/jquery.js"></script>-->
    <script src="<?php echo URL;?>public/plugins/jquery/jquery-2.1.0.min.js"></script>
    <script src="<?php echo URL;?>public/plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo URL;?>public/plugins/bootstrap/bootstrap.min.js"></script>
    <!-- All functions for this theme + document.ready processing -->
    <script src="<?php echo URL;?>public/js/devoops.js"></script>
	<script src="<?php echo URL;?>public/js/checkadmin.js"></script>
    <?php
    $this->appendArrayScript($this->js);
    ?>
</body>
</html>
