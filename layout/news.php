<?php
class controller_news extends lib_controller{

    public function __construct() {
        parent::__construct();
    }

    /*
     * Các function tạo hình ảnh
     */

    function UploadImage(array $data) {
        if ((($data["type"] == "image/gif") || ($data["type"] == "image/jpeg")
                || ($data["type"] == "image/jpg") || ($data["type"] == "image/pjpeg")
                || ($data["type"] == "image/x-png") || ($data["type"] == "image/png"))
            && ($data["size"] < 4000000)) {
            if ($data["error"] == 0) {
                $path = "public/upload/images/" . uniqid() . '.jpg';
                move_uploaded_file($data["tmp_name"], $path);
                return $path;
            }
        }
        return false;
    }

    function SaveThumbImage($path) {
        try {
            $thumb = PhpThumbFactory::create($path);
        } catch (Exception $e) {
            // handle error here however you'd like
        }
        $id=  uniqid();
        $name_file = $id.'.jpg';
        $thumb->resize(400);
        $thumb->adaptiveResize(300, 300);
        $thumb->save('public/upload/images/news/' . $name_file, 'jpg');

        $path_result = array(
            'image' => $path,
            'thumb' => 'public/upload/images/news/' . $name_file,
        );
        return $path_result;
    }

    protected function DeleteFile($file_path) {
        $path = str_replace(URL, '', $file_path);
        if (file_exists($path)) {
            unlink($path);
            return true;
        }
        return false;
    }


    public function updatenews(){
        require_once 'plugin/phpthumb/ThumbLib.inc.php';
        require_once 'plugin/url-seo/url-seo.php';
        $model = new model_news();
		
        if($_POST){
            $product_id = $_POST['news_id'];
            $news_info = $model->SelectNewsById($product_id);
            $product_name = $_POST['news_name'];
            $product_name_english = $_POST['news_name_english'];
            $product_type = $_POST['news_type'];
			if($_POST['news_label']){
				if(count($_POST['news_label']) >= 1){
					foreach($_POST['news_label'] as $val){
						$product_label .= ','.$val;
					}
				}else{
					$arr = $_POST['news_label'];
					$product_label = $arr[0];
				}
			}else{
				$product_label = '';
			}
            $product_description = $_POST['news_description'];
            $product_description_english = $_POST['news_description_english'];
            $news_url 		  = post_slug($product_name).'-post-'.$product_id;
            $news_url_english = post_slug($product_name_english).'-post-'.$product_id;
			$news_create_date = date('Y-m-d h:i:s',strtotime($_POST['news_create_date']));
            $user_id          = $_SESSION['user_id'];
			
            if(strlen($_FILES['news_img']['name'])>1){
                $upload_img = $this->UploadImage($_FILES['news_img']);
                $path_save_img = $this->SaveThumbImage($upload_img);

                if($path_save_img){
                    $product_img = URL.$path_save_img['image'];
                    $this->DeleteFile($news_info[0]['news_image_thumb']);
                }else{
                    $product_img = $news_info[0]['news_image_thumb'];
                }
            }else{
                $product_img = $news_info[0]['news_image_thumb'];
            }

            $data = array(
                'news_name'          		=> addslashes($product_name),
                'news_name_english'  		=> addslashes($product_name_english),
                'news_description'   		=> addslashes($product_description),
                'news_description_english'  => addslashes($product_description_english),
                'news_image_thumb'   		=> $product_img,
                'news_categories_id'    	=> $product_type,
                'label_id'              	=> $product_label,
                'news_url'           		=> $news_url,
                'news_url_english'   		=> $news_url_english,
				'news_create_date'			=> $news_create_date
            );

			//print_r($data);
			
            $result = $model->UpdateNews($data,$product_id);

            if($result){
                $log = new model_log();
                $arr = array(
                    'log_action'        => 'update',
                    'user_id'           => $_SESSION['user_id'],
                    'log_object'        => 'tin tức',
                    'log_object_id'     => $product_id,
                    'log_object_name'   => $product_name,
                    'user_name'         => $_SESSION['user_name']
                );
                $log->InsertLog($arr);
                echo '1';
            }
        }
    }

    public function editnews($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($edit[0]['permission_detail'],'new')){
                header('Location:'.URL.'admin/');
            }
        }

        $model = new model_news();
		
		$model1 = new model_general();
        if($path){
            $result = $model->SelectNewsById($path);
            if($result){
                $this->view->newsinfo = $result;

				$list_permission = '';
		
				if($model1->GetPermissionNewsByUserId($_SESSION['user_type'])){
					$listpermission = $model1->GetPermissionNewsByUserId($_SESSION['user_type']);
					
					foreach($listpermission as $ls){
						$list_permission .= ','.$ls['categories_id'];
					}
				}
				
				$this->view->listpermission = $list_permission;
				
                if($model->SelectAllCategoriesNews()){
                    $this->view->listcategories = $model->SelectAllCategoriesNews();
                }

                if($model->SelectAllCategoriesNewsLabel()){
                    $this->view->listlabel = $model->SelectAllCategoriesNewsLabel();
                }

                $this->view->title = 'Trang quản trị';

                $this->view->js = array(
                    URL.'public/ckeditor/ckeditor.js',
                    URL.'public/js/news.js'
                );

                $this->view->layout('layout-admin1');
                $this->view->Render('news/editnews');
                return true;
            }else{
                header('Location:'.URL.'news');
            }
        }else{
            header('Location:'.URL.'news');
        }
    }

    public function createnews(){
        require_once 'plugin/phpthumb/ThumbLib.inc.php';
        require_once 'plugin/url-seo/url-seo.php';
        $model = new model_news();
        if($_POST){
            $product_name = $_POST['news_name'];
            $product_name_english = $_POST['news_name_english'];
            $product_type = $_POST['news_type'];
			if($_POST['news_label']){
				if(count($_POST['news_label']) > 1){
					foreach($_POST['news_label'] as $val){
						$product_label .= ','.$val;
					}
				}else{
					$arr = $_POST['news_label'];
					$product_label = $arr[0];
				}
			}else{
				$product_label = '';
			}
            $product_description = $_POST['news_description'];
            $product_description_english = $_POST['news_description_english'];

            if(strlen($_FILES['news_img']['name'])>1){
                $upload_img = $this->UploadImage($_FILES['news_img']);
                $path_save_img = $this->SaveThumbImage($upload_img);

                if($path_save_img){
                    $product_img = URL.$path_save_img['image'];
                }else{
                    $product_img = '';
                }
            }else{
                $product_img = '';
            }

            $data = array(
                'news_name'          		=> addslashes($product_name),
                'news_name_english'  		=> addslashes($product_name_english),
                'news_description'   		=> $product_description,
                'news_description_english'  => $product_description_english,
                'news_image_thumb'   		=> $product_img,
                'news_categories_id'    	=> $product_type,
                'label_id'              	=> $product_label,
                'news_url'           		=> '',
                'news_url_english'   		=> ''
            );
			
            $result = $model->InsertNews($data);
			
            if($result){
                $datenow = date('dmY');
                $uniqueid = uniqid();
                $product_url = post_slug($product_name).'-post-'.$result;
                $product_url_english = post_slug($product_name_english).'-post-'.$result;
                $data1 = array(
                    'news_url' => $product_url,
                    'news_url_english'  => $product_url_english
                );
				
                $update = $model->UpdateNews($data1,$result);
                if($update){
                    $log = new model_log();
                    $arr = array(
                        'log_action'        => 'create',
                        'user_id'           => $_SESSION['user_id'],
                        'log_object'        => 'tin tức',
                        'log_object_id'     => $result,
                        'log_object_name'   => $product_name,
                        'user_name'         => $_SESSION['user_name']
                    );
                    $log->InsertLog($arr);
                    echo 1;
                }
            }
        }
    }

    /*
     * Các function của news
     */

    function deletenews($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit']; 

        if($_SESSION['user_type'] != 1){
            if(!strpos($delete[0]['permission_detail'],'new')){
                header('Location:'.URL.'admin/');
            }
        }

        $model = new model_news();

        if($path){
            if($model->SelectNewsById($path)){
                $newsinfo = $model->SelectNewsById($path);
                $delete = $model->DeleteNews($path);

                if($delete){
                    $this->DeleteFile($newsinfo[0]['news_image_thumb']);
                    $log = new model_log();
                    $arr = array(
                        'log_action'        => 'delete',
                        'user_id'           => $_SESSION['user_id'],
                        'log_object'        => 'tin tức',
                        'log_object_id'     => $newsinfo[0]['news_id'],
                        'log_object_name'   => $newsinfo[0]['news_name'],
                        'user_name'         => $_SESSION['user_name']
                    );
                    $log->InsertLog($arr);
                    header('Location:'.URL.'news/');
                }

            }else{
                header('Location:'.URL.'news/');
            }
        }else{
            header('Location:'.URL.'news/');
        }
    }

    function deletenews1(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit']; 

        if($_SESSION['user_type'] != 1){
            if(!strpos($delete[0]['permission_detail'],'new')){
                header('Location:'.URL.'admin/');
            }
        }

        $model = new model_news();

        if($_POST){
            $idpost = $_POST['idpost'];
            if($model->SelectNewsById($idpost)){
                $newsinfo = $model->SelectNewsById($idpost);
                $delete = $model->DeleteNews($idpost);

                if($delete){
                    $this->DeleteFile($newsinfo[0]['news_image_thumb']);
                    $log = new model_log();
                    $arr = array(
                        'log_action'        => 'delete',
                        'user_id'           => $_SESSION['user_id'],
                        'log_object'        => 'tin tức',
                        'log_object_id'     => $newsinfo[0]['news_id'],
                        'log_object_name'   => $newsinfo[0]['news_name'],
                        'user_name'         => $_SESSION['user_name']
                    );
                    $log->InsertLog($arr);
                    echo '1';
                }
            }
        }
    }

    function addnews(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($create[0]['permission_detail'],'new')){
                header('Location:'.URL.'admin/');
            }
        }

        $model = new model_news();
        
        $model1 = new model_general();

        if($model->SelectAllNews()){
            $this->view->listnews = $model->SelectAllNews();
        }

        $list_permission = '';
        
        if($model1->GetPermissionNewsByUserId($_SESSION['user_type'])){
            $listpermission = $model1->GetPermissionNewsByUserId($_SESSION['user_type']);
            
            foreach($listpermission as $ls){
                $list_permission .= ','.$ls['categories_id'];
            }
        }
        
        $this->view->listpermission = $list_permission;
        
        if($model->SelectAllCategoriesNews()){
            $this->view->listcategories = $model->SelectAllCategoriesNews();
        }

        if($model->SelectAllCategoriesNewsLabel()){
            $this->view->listlabel = $model->SelectAllCategoriesNewsLabel();
        }

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/news.js'
        );

        $this->view->layout('layout-admin1');
        $this->view->Render('news/addnews');
        return true;
    }

    public function copy($path){
        require_once 'plugin/url-seo/url-seo.php';
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($create[0]['permission_detail'],'new') || !strpos($delete[0]['permission_detail'],'new') || !strpos($edit[0]['permission_detail'],'new')){
                header('Location:'.URL.'admin/');
            }
        }

        $model = new model_news();
        
        $model1 = new model_general();

        if($path){
            if($model->SelectNewsById($path)){
                $newsinfo = $model->SelectNewsById($path);
                
                $arr = array(
                    'news_name'                 => $newsinfo[0]['news_name'],
                    'news_name_english'         => $newsinfo[0]['news_name_english'],
                    'news_description'          => $newsinfo[0]['news_description'],
                    'news_description_english'  => $newsinfo[0]['news_description_english'],
                    'news_image_thumb'          => $newsinfo[0]['news_image_thumb'],
                    'news_categories_id'        => $newsinfo[0]['news_categories_id'],
                    'label_id'                  => $newsinfo[0]['label_id'],
                    'user_id'                   => $_SESSION['user_id']
                );

                $result = $model->InsertNews($arr);

                if($result){
                    $product_url         = post_slug($newsinfo[0]['news_name']).'-post-'.$result;
                    $product_url_english = post_slug($newsinfo[0]['news_name_english']).'-post-'.$result;
                    $data1 = array(
                        'news_url' => $product_url,
                        'news_url_english'  => $product_url_english
                    );
                    
                    $update = $model->UpdateNews($data1,$result);
                    $log = new model_log();
                    $arr = array(
                        'log_action'        => 'create',
                        'user_id'           => $_SESSION['user_id'],
                        'log_object'        => 'tin tức',
                        'log_object_id'     => $result,
                        'log_object_name'   => $newsinfo[0]['news_name'],
                        'user_name'         => $_SESSION['user_name']
                    );
                    $log->InsertLog($arr);
                    header('Location:'.URL.'news/');
                }

            }else{
                header('Location:'.URL.'news/');
            }
        }else{
            header('Location:'.URL.'news/');
        }
    }

    function index(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($create[0]['permission_detail'],'new') || !strpos($delete[0]['permission_detail'],'new') || !strpos($edit[0]['permission_detail'],'new')){
                header('Location:'.URL.'admin/');
            }
        }

        $model = new model_news();
		
		$model1 = new model_general();

        if($model->SelectAllNews()){
            $this->view->listnews = $model->SelectAllNews();
        }

		$list_permission = '';
		
		if($model1->GetPermissionNewsByUserId($_SESSION['user_id'])){
			$listpermission = $model1->GetPermissionNewsByUserId($_SESSION['user_id']);
			
			foreach($listpermission as $ls){
				$list_permission .= ','.$ls['categories_id'];
			}
		}
		
		$this->view->listpermission = $list_permission;
		
		if($model->SelectAllCategoriesNews()){
			$this->view->listcategories = $model->SelectAllCategoriesNews();
		}

        if($model->SelectAllCategoriesNewsLabel()){
            $this->view->listlabel = $model->SelectAllCategoriesNewsLabel();
        }

        $model1 = new model_setting();

        $allsetting = $model1->GetAllSetting();

        $this->view->allsetting = $allsetting;

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/news.js'
        );

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin1');
        $this->view->Render('news/index');
        return true;
    }

    /*
     * Các function của categories news
     */

    function editcategories($path){
        require_once 'plugin/url-seo/url-seo.php';
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($edit[0]['permission_detail'],'new')){
                header('Location:'.URL.'admin/');
            }
        }

        $model = new model_news();

        if($path){
            if($model->GetCategoriesById($path)){
                $result = $model->GetCategoriesById($path);

                $this->view->categoriesinfo = $result;

                if($model->SelectAllCategoriesFatherNews($path)){
                    $this->view->listcategoriesnews = $model->SelectAllCategoriesFatherNews($path);
                }

                if($_POST){
                    $product_type         = $_POST['news_type'];
                    $product_type_english = $_POST['news_type_english'];
                    $product_fathertype   = $_POST['news_fathertype'];
                    $urlnews    = explode('-',$result[0]['news_categories_url']);
                    $numarrnews = count($urlnews);
                    $urlcode = $urlnews[$numarrnews-2];
                    $product_type_url     = post_slug($product_type).'-postcategory-'.$path;
                    $product_type_url_english     = post_slug($product_type_english).'-postcategory-'.$path;

                    if($product_fathertype){
                        $product_father = $product_fathertype;
                    }else{
                        $product_father = 0;
                    }

                    $data = array(
                        'news_categories_name'          => $product_type,
                        'news_categories_name_english'  => $product_type_english,
                        'news_categories_father'        => $product_father,
                        'news_categories_url'           => $product_type_url,
                        'news_categories_url_english'   => $product_type_url_english
                    );

                    $result = $model->UpdateCategoriesNews($data,$path);
                    if($result){
                        $log = new model_log();
                        $arr = array(
                            'log_action'        => 'update',
                            'user_id'           => $_SESSION['user_id'],
                            'log_object'        => 'chuyên mục tin tức',
                            'log_object_id'     => $result,
                            'log_object_name'   => $product_type,
                            'user_name'         => $_SESSION['user_name']
                        );
                        $log->InsertLog($arr);
                        header('Location:'.URL.'news/categories/');
                    }
                }
            }else{
                header('Location:'.URL.'news/categories/');
            }
        }else{
            header('Location:'.URL.'news/categories/');
        }

        $this->view->js = array(
            URL.'public/js/newscat.js'
        );

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin1');
        $this->view->Render('news/editcategories');
        return true;
    }

    function deletecategories($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($delete[0]['permission_detail'],'new')){
                header('Location:'.URL.'admin/');
            }
        }

        $model = new model_news();

        if($path){
            if($model->GetCategoriesById($path)){
                $result = $model->GetCategoriesById($path);

                $delete = $model->DeleteCategoriesNews($path);

                if($delete){
                    $log = new model_log();
                    $arr = array(
                        'log_action'        => 'delete',
                        'user_id'           => $_SESSION['user_id'],
                        'log_object'        => 'chuyên mục tin tức',
                        'log_object_id'     => $result[0]['news_categories_id'],
                        'log_object_name'   => $result[0]['news_categories_name'],
                        'user_name'         => $_SESSION['user_name']
                    );
                    $log->InsertLog($arr);
                    header('Location:'.URL.'news/categories/');
                }
            }else{
                header('Location:'.URL.'news/categories/');
            }
        }else{
            header('Location:'.URL.'news/categories/');
        }
    }

    function categories(){
        require_once 'plugin/url-seo/url-seo.php';
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $create = $_SESSION['user_permission_create'];
        $delete = $_SESSION['user_permission_delete'];
        $edit = $_SESSION['user_permission_edit'];  

        if($_SESSION['user_type'] != 1){
            if(!strpos($create[0]['permission_detail'],'new') || !strpos($delete[0]['permission_detail'],'new') || !strpos($edit[0]['permission_detail'],'new')){
                header('Location:'.URL.'admin/');
            }
        }

        $model = new model_news();

        $model1 = new model_setting();

        $allsetting = $model1->GetAllSetting();

        $this->view->allsetting = $allsetting;

        if($_POST){
            $product_type         = $_POST['news_type'];
            $product_type_english = $_POST['news_type_english'];
            $product_fathertype   = $_POST['news_fathertype'];

            if($product_fathertype){
                $product_father = $product_fathertype;
            }else{
                $product_father = 0;
            }

            $data = array(
                'news_categories_name'          => $product_type,
                'news_categories_name_english'  => $product_type_english,
                'news_categories_father'        => $product_father,
                'news_categories_url'           => '',
                'news_categories_url_english'   => ''
            );

            $result = $model->InsertCategoriesNews($data);
            if($result){
                $date = date('dmY');
                $uniqueid = uniqid();
                $product_url                  = post_slug($product_type).'-postcategory-'.$result;
                $product_type_url_english     = post_slug($product_type_english).'-postcategory-'.$result;
                $data1 = array(
                    'news_categories_url'           => $product_url,
                    'news_categories_url_english'   => $product_type_url_english
                );
                $update = $model->UpdateCategoriesNews($data1,$result);
                if($update){
                    $log = new model_log();
                    $arr = array(
                        'log_action'        => 'create',
                        'user_id'           => $_SESSION['user_id'],
                        'log_object'        => 'chuyên mục tin tức',
                        'log_object_id'     => $result,
                        'log_object_name'   => $product_type,
                        'user_name'         => $_SESSION['user_name']
                    );
                    $log->InsertLog($arr);
                    header('Location:'.URL.'news/categories/');
                }
            }
        }

        if($model->SelectAllCategoriesNews()){
            $this->view->listcategoriesnews = $model->SelectAllCategoriesNews();
        }

        if($model->SelectAllNews()){
            $this->view->listnews = $model->SelectAllNews();
        }

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/newscat.js'
        );

        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin1');
        $this->view->Render('news/categories');
        return true;
    }

    /*
     *  Các function của label news
     */

    function editlabel($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $model = new model_news();

        if($path){
            if($model->CategoriesNewsLabelById($path)){

                $result = $model->CategoriesNewsLabelById($path);

                $this->view->labelinfo = $result;

                if($_POST){
                    $product_label = $_POST['news_label'];
                    $product_labelcolor = $_POST['news_labelcolor'];

                    $data = array(
                        'label_name' => $product_label,
                        'label_color' => $product_labelcolor
                    );

                    $result = $model->UpdateCategoriesNewsLabel($data,$path);
                    if($result){
                        header('Location:'.URL.'news/label/');
                    }
                }

            }else{
                header('Location:'.URL.'news/label/');
            }

        }else{
            header('Location:'.URL.'news/label/');
        }

        $this->view->css = array(
            URL.'public/css/colorpicker.css'
        );

        $this->view->js = array(
            URL.'public/js/colorpicker/colorpicker.js',
            URL.'public/js/label.js'
        );

        $this->view->title = 'Trang quản trị - Thiệp 24h';

        $this->view->layout('layout-admin');
        $this->view->Render('news/editlabel');
        return true;
    }

    function deletelabel($path){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $model = new model_news();

        if($path){

            if($model->CategoriesNewsLabelById($path)){

                $delete = $model->DeleteCategoriesNewsLabel($path);

                if($delete){
                    header('Location:'.URL.'news/label/');
                }

            }else{
                header('Location:'.URL.'news/label/');
            }

        }else{
            header('Location:'.URL.'news/label/');
        }
    }

    function label(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $model = new model_news();

        if($model->SelectAllCategoriesNewsLabel()){
            $this->view->listlabel = $model->SelectAllCategoriesNewsLabel();
        }

        if($_POST){
            $product_label = $_POST['news_label'];
            $product_labelcolor = $_POST['news_labelcolor'];

            $data = array(
                'label_name' => $product_label,
                'label_color' => $product_labelcolor
            );

            $result = $model->InsertCategoriesNewsLabel($data);
            if($result){
                header('Location:'.URL.'news/label/');
            }
        }

        $this->view->css = array(
            URL.'public/css/colorpicker.css'
        );

        $this->view->js = array(
            URL.'public/js/colorpicker/colorpicker.js',
            URL.'public/js/label.js'
        );
        $this->view->title = 'Trang quản trị';

        $this->view->layout('layout-admin');
        $this->view->Render('news/label');
        return true;
    }

    function detail($path){
        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $partner = new model_partner();

        $library = new model_library();
		
		$video = new model_video();

        if($allsetting){
            $this->view->title = $allsetting[0]['config_title'];
            $this->view->description = $allsetting[0]['config_description'];
            $this->view->keyword = $allsetting[0]['config_keyword'];
            $this->view->logo = $allsetting[0]['config_logo'];
            $this->view->favicon = $allsetting[0]['config_icon'];
            $this->view->google = $allsetting[0]['config_google'];
            $this->view->facebook = $allsetting[0]['config_facebook'];
            $this->view->twitter = $allsetting[0]['config_twitter'];
            $this->view->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOn()){
            $allmenufather = $model1->SelectAllMenuFatherOn();

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->view->allmenu = $allmenufather;
        }


        $model4 = new model_news();

        if($model4->SelectAllCategoriesNews()){
            $this->view->listcategoriesnews = $model4->SelectAllCategoriesNews();
        }

        if($path){
            $part = explode('_',$path);
            $id = $part[count($part)-1];
            $result = $model4->SelectNewsById($id);

            if($result){

                foreach ($result as $key => $value) {
                    if($comment->NumCommentByPostId($id)){
                        $numcomment = $comment->NumCommentByPostId($id);
                        $result[$key]['news_comment'] = $numcomment[0]['numcomment'];
                    }
                }

                $view = intval($result[0]['view']) + 1;

                $this->view->newsinfo = $result;

                $catnews = $model4->GetCategoriesById($result[0]['news_categories_id']);

                $this->view->catinfo = $catnews;

                if($model4->SelectAllNewsByCatId($result[0]['news_categories_id'])){
                    $this->view->listrelatenews = $model4->SelectAllNewsByCatId($result[0]['news_categories_id']);
                }
				
				if($this->view->lang == 'vi'){
					$this->view->title = $result[0]['news_name'].'-'.$allsetting[0]['config_title'];
					$this->view->description = $result[0]['news_name'].'-'.$allsetting[0]['config_description'];
					$this->view->keyword = $result[0]['news_name'].','.$allsetting[0]['config_keyword'];
				}else{
					$this->view->title = $result[0]['news_name_english'].'-'.'Institute for Computational Science and Technology';
					$this->view->description = $result[0]['news_name_english'].'-'.$allsetting[0]['config_description'];
					$this->view->keyword = $result[0]['news_name_english'].','.$allsetting[0]['config_keyword'];
				}

                if($catnews){
                    $this->view->catenews = $catnews;
                }

                $arr = array(
                    'view'  =>$view
                );

                $update = $model4->UpdateNews($arr,$id);

                if($comment->GetCommentByPostId($id)){
                    $this->view->listcomment = $comment->GetCommentByPostId($id);
                }
            }
        }
        
		$useragent=$_SERVER['HTTP_USER_AGENT'];
		
		if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
			$this->view->layout('layout-mobile');
			$this->view->Render('mobile/newsdetail');
			return true;
		}else{
			$this->view->layout('layout');
			$this->view->Render('news/detail');
			return true;
		}
    }

    function UploadMedia(array $data) {
        $type = '';
        if(($data["type"] == "image/jpeg")
            || ($data["type"] == "image/jpg") || ($data["type"] == "image/pjpeg")){
                $type = 'jpg';  
        }elseif($data["type"] == "image/gif"){
            $type = 'gif';
        }elseif($data["type"] == "image/png"){
            $type = 'png';
        }
        if ((($data["type"] == "image/gif") || ($data["type"] == "image/jpeg")
                || ($data["type"] == "image/jpg") || ($data["type"] == "image/pjpeg")
                || ($data["type"] == "image/x-png") || ($data["type"] == "image/png"))
            && ($data["size"] < 4000000)) {
            if ($data["error"] == 0) {
                $path = "public/upload/images/another/images/" . $data["name"] . '.'.$type;
                move_uploaded_file($data["tmp_name"], $path);
                return $path;
            }
        }
        return false;
    }

    function media(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        $directory = 'public/upload/images/another/images/';
        $imagesjpg = glob($directory . "*.jpg");
        $imagespng = glob($directory . "*.png");

        $this->view->listimagejpg = $imagesjpg;
        $this->view->listimagepng = $imagespng;

        $this->view->js = array(
            URL.'public/ckeditor/ckeditor.js',
            URL.'public/js/media.js',
            URL.'public/js/filemedia.js'
        );

        $this->view->layout('layout-admin1');
        $this->view->Render('news/media');
        return true;
    }

    public function createmedia(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }
        require_once 'plugin/phpthumb/ThumbLib.inc.php';

        if($_POST){
            if(strlen($_FILES['file']['name'])>1){
                $library_type = $_FILES['file']['type'];
                $library_name = $_FILES['file']['name'];
                $upload_img = $this->UploadMedia($_FILES['file']);

                echo 1;
            }
        }
    }

    public function deletemedia1(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }

        //print_r($_POST);

        if($_POST){
            $file1 = $_POST['file1'];

            foreach($file1 as $vl){
                $this->DeleteFile($vl);
            }

            echo '1';
        }
    }

    public function deletemedia(){
        if(lib_session::Get('user_type') == '0'){
            header('Location:'.URL.'login/');
        }
        $directory = 'public/upload/images/another/images/';
        $imagesjpg = glob($directory . "*.jpg");
        $imagespng = glob($directory . "*.png");

        if($_POST){
            $nameimage = $_POST['id'];
            foreach ($imagesjpg as $value) {
                if(strpos($value,$nameimage)){
                    $this->DeleteFile(URL.$value);
                }
            }
        }
    }
}