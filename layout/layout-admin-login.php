<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo $this->title;?></title>
    <meta name="description" content="<?php echo $this->description;?>" />
    <meta name="keywords" content="<?php echo $this->keyword;?>" />
    <meta name="robots" content="index, follow" />
    <meta name="generator" content="<?php echo $this->title;?>" />
    <meta name="author" content="<?php echo $this->title;?>" />
    <link rel="shortcut icon" href="<?php echo $this->favicon;?>" />
    
    <link href="<?php echo URL;?>public/plugins/bootstrap/bootstrap.css" rel="stylesheet">
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
    <link href="<?php echo URL;?>public/css/style-admin.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="http://getbootstrap.com/docs-assets/js/html5shiv.js"></script>
    <script src="http://getbootstrap.com/docs-assets/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container-fluid">
    <div id="page-login" class="row">
        <?php
            $this->content();
        ?>

    </div>
</div>
</body>
</html>
