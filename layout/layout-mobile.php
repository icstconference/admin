<html lang="en"><!--<![endif]-->
<head id="Head1">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $this->title;?></title>
    <meta name="description" content="<?php echo $this->description;?>" />
    <meta name="keywords" content="<?php echo $this->keyword;?>" />
    <meta name="robots" content="index, follow" />
    <meta name="generator" content="<?php echo $this->title;?>" />
    <meta name="author" content="<?php echo $this->title;?>" />
    <link rel="shortcut icon" href="<?php echo $this->favicon;?>" />
    <meta name="revisit-after" content="1 days" />

    <link rel="stylesheet" href="<?php echo URL;?>public/template/css/global.min.css" media="screen">
    <link href="<?php echo URL;?>public/template/css/mobile.min.css" rel="stylesheet" type="text/css"/>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>
    <link rel='stylesheet' type='text/css' href='<?php echo URL;?>public/flexslider/flexslider.css' />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script>
        var URL_G = '<?php echo URL;?>';
		var lang = '<?php echo $this->lang;?>';
    </script>
    <style>
    	@font-face {
            font-family: 'helveticaneuelight';
            src: url('<?php echo URL;?>public/template/font/helveticaneuelight.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }
        @font-face {
            font-family: 'helveticaneue';
            src: url('<?php echo URL;?>public/template/font/helveticaneue.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }
        @font-face {
            font-family: 'helveticaneue';
            src: url('<?php echo URL;?>public/template/font/helveticaneueitalic.ttf') format('truetype');
            font-weight: normal;
            font-style: italic;
        }
        @font-face {
            font-family: 'helveticaneue';
            src: url('<?php echo URL;?>public/template/font/helveticaneuebold.ttf') format('truetype');
            font-weight: bold;
            font-style: normal;
        }
        @font-face {
            font-family: 'helveticaneue';
            src: url('<?php echo URL;?>public/template/font/helveticaneuebolditalic.ttf') format('truetype');
            font-weight: bold;
            font-style: italic;
        }
        @font-face {
            font-family: 'UVFDidot';
            src: url('<?php echo URL;?>public/template/font/UVFDidot.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }

        body h1,h2,h3,h4,h5,h6,div,p,a{
            font-family: 'helveticaneue', sans-serif !important;
        }

        .home_menu a{
        	color:#65554e !important;
        }

        .home_menu h1{
        	color:#65554e !important;
        }

        #footer-copyright{
        	padding: 15px 0 0 18px;
        }

        #footer-copyright li {
    		margin: 0 15px 0 0;
    	}

        .slide-caption-1 {padding-top: 10px !important;}
        .slide-caption-2 {padding-top: 10px !important;}
        .slide-caption-3 {padding-top: 10px !important;}
        .slide-caption-4 {padding-top: 10px !important;}

		#header{
			background-color: #65554e;
		}
		#home-carousel-wrap #pager-caption{
			background-color: #65554e;
		}
		#page-wrap .hat{
			background-color: #453a35;
		}
		.hat.active .current-lang, .hat.active .menu-label{
			background-color: #65554e;
		}
		.hat .lang-menu-list li, .hat .nav-menu-list li {
		    border-top: solid #65554e 1px;
		    border-bottom: none;
		}
		#header #info {
		    float: left;
		    width: 100%;
		}
    </style>
</head>
<body class="homepage mobile" data-twttr-rendered="true" cz-shortcut-listen="true">
	<script src="<?php echo URL;?>public/template/js/header.js"></script>
	<div id="page-wrap">
		<nav>
			<div class="hat">
		        <div class="nav-menu">
					<a href="<?php echo URL;?>" class="menu-label" style="font-family:'helveticaneuelight' !important;">
						Menu
					</a>
					<ul class="nav-menu-list">
						<?php 
	                        if($this->allmenu){
	                            $i=0;
	                            foreach ($this->allmenu as $value) {
	                                $i++;
	                    ?>
	                    <li>
	                    <?php
                            if($value['menu_kind'] == 'news'){
                        ?>
                            <a href="<?php echo URL;?>detail/<?php echo $value['menu_url'];?>" style="font-family: 'Roboto', sans-serif;font-weight:500;text-transform:uppercase;">
                                <?php 
                                    if($this->lang == 'vi'){
                                ?>
                                    <?php echo $value['menu_name'];?>
                                <?php }else{ ?>
                                    <?php echo $value['menu_name_english'];?>
                                <?php } ?>
                            </a>
                        <?php 
                            }elseif($value['menu_kind'] == 'news category'){
                        ?>
                            <a href="<?php echo URL;?>news/detail/<?php echo $value['menu_url'];?>" style="font-family: 'Roboto', sans-serif;font-weight:500;text-transform:uppercase;">
                                <?php 
                                    if($this->lang == 'vi'){
                                ?>
                                    <?php echo $value['menu_name'];?>
                                <?php }else{ ?>
                                    <?php echo $value['menu_name_english'];?>
                                <?php } ?>
                            </a>
                        <?php 
                            }elseif ($value['menu_kind'] == 'page') { 
                        ?>
                            <a href="<?php echo URL;?>page/detail/<?php echo $value['menu_url'];?>" style="font-family: 'Roboto', sans-serif;font-weight:500;text-transform:uppercase;">
                                <?php 
                                    if($this->lang == 'vi'){
                                ?>
                                    <?php echo $value['menu_name'];?>
                                <?php }else{ ?>
                                    <?php echo $value['menu_name_english'];?>
                                <?php } ?>
                            </a>
                        <?php 
                            }else{
                        ?>
                            <a href="<?php echo $value['menu_url'];?>" style="font-family: 'Roboto', sans-serif;font-weight:500;text-transform:uppercase;">
                                <?php 
                                    if($this->lang == 'vi'){
                                ?>
                                    <?php echo $value['menu_name'];?>
                                <?php }else{ ?>
                                    <?php echo $value['menu_name_english'];?>
                                <?php } ?>
                            </a>
                        <?php } ?>
                        </li>
						<?php }} ?>
					</ul>
				</div>
				<div class="lang-menu">
					<?php 
                        if($this->lang == 'vi'){
                    ?>
					<a href="<?php echo URL . 'vi/' . $this->link_lang; ?>" class="current-lang" style="font-family:'helveticaneuelight' !important;">VI</a>
					<ul class="lang-menu-list">
						<li><a href="<?php echo URL . 'en/' . $this->link_lang; ?>" style="font-family:'helveticaneuelight' !important;">EN</a></li>
					</ul>
					<?php }else{ ?>
					<a href="<?php echo URL . 'en/' . $this->link_lang; ?>" class="current-lang" style="font-family:'helveticaneuelight' !important;">EN</a>
					<ul class="lang-menu-list">
						<li><a href="<?php echo URL . 'vi/' . $this->link_lang; ?>" style="font-family:'helveticaneuelight' !important;">VI</a></li>
					</ul>
					<?php } ?>
				</div>
			</div>
		</nav>
		<header>
			<div id="header">
				<div id="info">
					<div id="logo" style="padding-top: 2px;margin-left: 12px;">
						<a href="<?php echo URL;?>">
							<img src="<?php echo URL;?>public/img/logomobile.png" alt="<?php echo $this->title;?>">
						</a>
					</div>
				</div>
			</div>	<!-- end of #header -->
		</header>

		<?php 
			echo $this->content();
		?>
	           
		<footer id="footer-wrap" class="clearfix" style="min-height:0px;margin-bottom:0px;padding-bottom:20px;">
			<ul style="margin-top: 10px;">
				<?php 
                    if($this->lang == 'vi'){
                ?>
				<li class="first" style="margin-right: 0px;display: inline;"><span style="float: left;">©Bản quyền thuộc về PVGAS D 2015</span> <a href="<?php echo URL;?>" style="float:right;margin-right:15px;" class="scrollToTop">Lên đầu trang&nbsp;<i class="fa fa-chevron-up"></i></a></li>		        			
				<?php }else{ ?>
				<li class="first" style="margin-right: 0px;display: inline;"><span style="float: left;">©Copyright by PVGAS D 2015</span> <a href="<?php echo URL;?>" style="float:right;margin-right:15px;" class="scrollToTop">Go top&nbsp;<i class="fa fa-chevron-up"></i></a></li>		
				<?php } ?>
			</ul>
		</footer>
	</div>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="<?php echo URL;?>public/template/js/jquery_files.js"></script>
    <script src="<?php echo URL;?>public/template/js/footer.js"></script>

    <script src="<?php echo URL;?>public/template/js/lightbox/jquery.lightbox.js" type="text/javascript"></script>
    <script src="<?php echo URL;?>public/template/js/lightbox/lightbox.js" type="text/javascript"></script>
    <!-- End lightbox -->
    <script src="http://www.mandarinoriental.com/static/scripts/libs/s_code.js" type="text/javascript"></script>
    <script type='text/javascript' src='<?php echo URL;?>public/flexslider/jquery.flexslider.js'></script>
    <script type='text/javascript' src='<?php echo URL;?>public/js/config-flexider.js'></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
  $(function() {
    $( "#tabs" ).tabs();
  });
  </script>
  <script type="text/javascript">
	$(document).ready(function(){
		
		//Check to see if the window is top if not then display button
		$(window).scroll(function(){
			if ($(this).scrollTop() > 100) {
				$('.scrollToTop').fadeIn();
			} else {
				$('.scrollToTop').fadeOut();
			}
		});
		
		//Click event to scroll to top
		$('.scrollToTop').click(function(){
			$('html, body').animate({scrollTop : 0},800);
			return false;
		});
		
	});
  </script>

	    
</body>
</html>