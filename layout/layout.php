<!DOCTYPE html>
<html lang="en-US"><!--<![endif]-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Conference</title>
    <meta name="description" content="Conference" />
    <meta name="keywords" content="Conference" />
    <meta name="robots" content="index, follow" />
    <meta name="generator" content="Conference" />
    <meta name="author" content="Conference" />
    <link rel="shortcut icon" href="" />
    <meta name="revisit-after" content="1 days" />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo URL;?>public/css/bootstrap.min.css" rel="stylesheet">
    
    <!-- Font Awesome CSS -->
    <link href="<?php echo URL;?>public/css/font-awesome.min.css" rel="stylesheet">
    
    
    <!-- Animate CSS -->
    <link href="<?php echo URL;?>public/css/animate.css" rel="stylesheet" >
    
    <!-- Owl-Carousel -->
    <link rel="stylesheet" href="<?php echo URL;?>public/css/owl.carousel.css" >
    <link rel="stylesheet" href="<?php echo URL;?>public/css/owl.theme.css" >
    <link rel="stylesheet" href="<?php echo URL;?>public/css/owl.transitions.css" >

    <!-- Custom CSS -->
    <link href="<?php echo URL;?>public/css/style.css" rel="stylesheet">
    <link href="<?php echo URL;?>public/css/style2.css" rel="stylesheet">
    <link href="<?php echo URL;?>public/css/responsive.css" rel="stylesheet">
    
    
    <!-- Modernizer js -->
    <script src="js/modernizr.custom.js"></script>

    <style type="text/css">
        @font-face {
            font-family: 'MarkSimonson';
            src: url('<?php echo URL;?>public/font/MarkSimonsonRegular.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }
        @font-face {
            font-family: 'MarkSimonson';
            src: url('<?php echo URL;?>public/font/MarkSimonsonThinItalic.ttf') format('truetype');
            font-weight: normal;
            font-style: italic;
        }
        @font-face {
            font-family: 'MarkSimonson';
            src: url('<?php echo URL;?>public/font/MarkSimonsonSemibold.ttf') format('truetype');
            font-weight: bold;
            font-style: normal;
        }

        body{
            font-family: 'MarkSimonson', sans-serif;
            color: #333333;
        }
        h1, h2, h3, h4, h5, h6, a, {
            font-family: 'MarkSimonson', sans-serif !important;
        }
        a:focus{
            text-decoration:none;
        }
        .buttonblue{
            background: #2969b0;
            font-size: 30px;
            padding: 20px 40px;
            margin-right: 10px;
            font-weight: 300;
            width: 230px;
            color: #fff;
            border-radius:10px !important;
        }
        .buttonblue:hover{
            background: #fff;
            color: #333;
        }
        .buttonblue2{
            background: #2969b0;
            font-size: 16px;
            margin-right: 10px;
            font-weight: 300;
            color: #fff;
        }
        .buttonblue2:hover{
            background: #fff;
            color: #333;
        }
        .buttonblue1{
            background: #2969b0;
            font-size: 30px;
            padding: 20px 40px;
            margin-right: 10px;
            font-weight: 300;
            width: 230px;
            color: #fff;
            border-radius:10px !important;
            text-transform: none !important;
            margin-top: 20px;
        }
        .buttonblue1:hover{
            background: #fff;
            color: #333;
        }
        .buttonorange{
            background: #ff7733;
            color:#fff;
            font-size: 30px;
            padding: 21px 41px;
            margin-right: 10px;
            font-weight: 300;
            border-radius:4px !important;
        }
        .buttonorange:hover{
            background: #fff;
            color: #ff7733;
        }
        .footertitle{
            font-size: 18px;
            color: #fff;
            font-weight: 300;
            border-bottom: 1px solid #ff7733;
            padding-bottom: 10px;
        }
        .bodyfooter{
            margin: 20px 0;
            color: #fff;
            font-size: 18px;
            font-weight: 300;
            line-height: 30px;
        }
        .footergray{
            color:#aaaaaa;
        }
        .buttonsubcribe{
            background: #ff7733;
            color: #fff;
            margin-top:10px;
            border-radius:0;
        }
        .indextitle{
            font-size: 30px;
            color: #333;
            font-weight: 400;
            border-bottom: 1px solid #ff7733;
            padding-bottom: 0px;
            text-transform: uppercase;
        }
        .indextitle1{
            font-size: 35px !important;
            border-bottom: 1px solid #ff7733 !important;
        }
        .h3news{
            margin: 6px 0;
            text-transform: none;
            font-size: 20px;
        }
        h1, .h1, h2, .h2, h3, .h3{
            margin: 10px 0;
        }
        p {
            margin: 0 0 5px;
        }
        a{
            color: #333;
            text-decoration:none;
        }
        a:hover{
            color: #ff7733;
            text-decoration:none;
        }
        .frame {
            height: 64px;
            white-space: nowrap;
            text-align: center;
            margin: 1em 0;
            display: inline-block;
            width: 192px;
        }
        .helper {
            display: inline-block;
            height: 100%;
            vertical-align: middle;
        }
        .frame img{
            vertical-align: middle;
            max-height: 85px;      /* equals max image height */
            max-width: 146px;
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%);
        }
        .frame img:hover{
            -webkit-filter: grayscale(0%);
            filter: grayscale(0%);
        }
        h1.animated3{
            font-size:60px;
        }
        .navbar-default{
            background: #fff;
            height: 70px;
            box-shadow: 0px 0 2px #555;
        }
        .navbar-brand {
            padding: 7px 15px;
        }
        .navbar-default .nav li a{
            color: #2969b0;
            font-weight: 400;
            padding-bottom: 22px;
        }
        .navbar-default .navbar-nav>li>a:hover, .navbar-default .navbar-nav>li>a:focus{
            color: #333;
            padding-bottom: 19px;
            border-bottom: 3px solid #333;
            background: transparent;
        }
        .navbar-default .navbar-nav>.active>a{
            color: #2969b0;
            padding-bottom: 19px;
            border-bottom: 3px solid #2969b0;
            background: transparent;
        }
        .conference{
            color: #2969b0;
            text-transform:uppercase;
            font-weight: 400;
            font-size: 28px;
            font-family: 'MarkSimonson', sans-serif !important;
        }
        .line{
            width:20px;
            border:1px solid #2969b0;
            margin-bottom:10px;
        }
        .textunderline{
            font-size: 16px;
            line-height: 24px;
            color: #888;
            font-weight: 300;
        }
        .listhighlight{
            list-style-type:none;
            margin-top: 5px;
        }
        .listhighlight >li{
            line-height: 24px;
            font-size: 16px;
            font-weight: 400;
        }
        .listhighlight a:hover{
            color: #2969b0;
        }
        .navbar-default.navbar-shrink {
            padding:0;
            background-color: #fff;
        }
        .deadline{
            background: url(<?php echo URL;?>public/img/deadline.png) no-repeat center;
            height: 225px;
            text-align:center;
        }
        .deadlinetext{
            padding: 30px 0 10px;
            font-weight: 300;
            font-size: 28px;
        }
        .deadlinecontainlink{
            max-width: 154px;
            margin: 0 auto;
            text-align: center;
        }
        .deadlinelink{
            font-size: 24px;
            text-decoration: none;
            text-transform: uppercase;
            font-weight: 500;
        }
        .navbar-default .navbar-nav>.active>a, .navbar-default .navbar-nav>.active>a:hover, .navbar-default .navbar-nav>.active>a:focus{
            background:transparent;
            color:#2969b0;
        }
        .deadlinelink:hover,.deadlinelink:focus{
            color: #2969b0;
            text-decoration:none;
        }
        .wpb_tabs .wpb_tour_tabs_wrapper ul.wpb_tabs_nav.tz_meetup_tabs li a{
            color: #2969b0;
            border: 1px solid #2969b0;
        }
        .wpb_tabs .wpb_tour_tabs_wrapper ul.wpb_tabs_nav.tz_meetup_tabs li a:hover{
            color: #333;
            background: #fff;
        }
        .wpb_tabs ul.wpb_tabs_nav.tz_meetup_tabs li:hover{
            background-color: transparent;
        }
        .ui-widget-content{
            background: transparent;
            border:0;
        }
        .panel-group .panel{
            border-radius: 0;
        }
        .panel-default>.panel-heading{
            background-color: #cddced;
        }
        #feature .faq-toggle.collapsed::before {
            color: #2969b0;
            font-family: FontAwesome;
            content:"\f067";
            -webkit-transition: 0.5s linear ease-out;
            transition: 0.5s linear ease-out;
            position: absolute;
            left: 25px;
        }
        #feature .faq-toggle::before {
            position: absolute;
            left: 25px;
            font-family: FontAwesome;
            content: "\f068";
            color: #2969b0;
        }
        .panel-title>a:hover{
            color: #2969b0;
        }
        .panel-title>a {
            margin-left: 15px;
            font-weight: 400;
        }
        #packetleft{
            padding-left:0;
        }
        #packetright{
            padding-right:0;
        }
        .addtocart{
            font-size: 22px;
            color: #fff;
            line-height: 50px;
            font-weight: 500;
        }
        .addtocart:hover{
            color:#333;
        }
        .frame {
            height: 85px;
            white-space: nowrap;
            text-align: center;
            margin: 1em 0;
            display: inline-block;
            width: 192px;
        }
        .frame img {
            vertical-align: middle;
            max-height: 100px;
            max-width: 200px;
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%);
        }
        .google-maps {
            position: relative;
            height: 375px;
        }
        .google-maps iframe {
            top: 0;
            left: 0;
            width: 100% !important;
            height: 375px;
            z-index:0;
        }
        .menufooter{
            list-style-type:none;
        }
        .menufooter> li{
            padding: 0 10px;
            display:inline;
        }
        .menufooter a{
            text-transform: uppercase;
            color: #fff;
            font-size: 20px;
            font-weight: 300;
            text-decoration:none;
        }
        .menufooter a:hover{
            color:#be926f;
        }
    </style>

</head>    
<body class="index">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container" style="padding-left: 0px;">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="<?php echo URL;?>">
                    <img class="img-responsive" src="<?php echo URL;?>public/img/icstlogo.png" />
                </a>

            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="padding: 10px 0px;">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="<?php echo URL;?>">Home</a>
                    </li>
                    <li>
                        <a data-scroll href="#deadline">Important dates</a>
                    </li>
                    <li>
                        <a data-scroll href="#invitetalk">Invited Talks</a>
                    </li>
                    <li>
                        <a data-scroll href="#schedule">PROGRAMME</a>
                    </li>
                    <li>
                        <a data-scroll href="#organizer">Organizers</a>
                    </li>
                    <li>
                        <a data-scroll href="#sponsor">Sponsors</a>
                    </li>
                    <li>
                        <a href="<?php echo URL;?>submission-page-3.html">Submission</a>
                    </li>
                    <li>
                        <a href="<?php echo URL;?>information-page-2.html">INFORMATION</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <?php echo $this->content();?>

    <section id="feature" class="feature-section" style="background: #24293a;padding:0;border-bottom:1px solid #323232;">
        <div class="container" style="padding:0;">
            <div class="row" style="margin:0;padding: 30px 0;">
                <div class="col-sm-3 col-md-3 col-xs-12">
                    <div class="row" style="margin:0;">
                        <div class="col-sm-3 col-md-3 col-xs-6">
                            <img src="<?php echo URL;?>public/img/iphone.png" class="img-responsive"/>
                        </div>
                        <div class="col-sm-9 col-md-9 col-xs-6" style="color:#fff;">
                            Call us<br/>
                            +84 1665426965
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-md-3 col-xs-12">
                    <div class="row" style="margin:0;">
                        <div class="col-sm-3 col-md-3 col-xs-6">
                            <img src="<?php echo URL;?>public/img/location.png" class="img-responsive"/>
                        </div>
                        <div class="col-sm-9 col-md-9 col-xs-6" style="color:#fff;">
                            Location<br/>
                            +84 1665426965
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-md-3 col-xs-12">
                    <div class="row" style="margin:0;">
                        <div class="col-sm-3 col-md-3 col-xs-6">
                            <img src="<?php echo URL;?>public/img/mail.png" class="img-responsive" style="height:41px;"/>
                        </div>
                        <div class="col-sm-9 col-md-9 col-xs-6" style="color:#fff;">
                            Mail<br/>
                            <span style="color:#be926f;">icstconference@icst.org.vn</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-md-3 col-xs-12">
                    <div class="row" style="margin:0;">
                        <div class="col-sm-2 col-md-2 col-xs-6">
                            <a href="https://www.facebook.com/vientinhtoan/" style="color:#be926f;font-size:41px;">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="col-sm-9 col-md-9 col-xs-6" style="color:#fff;">
                            Fanpage<br/>
                            <span style="color:#be926f;">fb.com/vientinhtoan/</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="feature" class="feature-section" style="background: #24293a;padding:0;">
        <div class="container" style="padding:0;">
            <div class="row" style="margin:0;padding: 30px 0;text-align:center;color:#fff;">
                Copyright © 2016 ICST. All Rights Reserved.
            </div>
        </div>
    </section>

    <script src="<?php echo URL;?>public/js/jquery-2.1.1.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo URL;?>public/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo URL;?>public/js/jquery.easing.1.3.js"></script>
    <script src="<?php echo URL;?>public/js/classie.js"></script>
    <script src="<?php echo URL;?>public/js/jquery.appear.js"></script>
    <script src="<?php echo URL;?>public/js/cbpAnimatedHeader.js"></script>
    <script src="<?php echo URL;?>public/js/owl.carousel.min.js"></script>
    <script src="<?php echo URL;?>public/js/jquery.fitvids.js"></script>

    <script type='text/javascript' src='<?php echo URL;?>public/js/js_composer_front.min.js'></script>
    <script type='text/javascript' src='<?php echo URL;?>public/js/widget.min.js'></script>
    <script type='text/javascript' src='<?php echo URL;?>public/js/tabs.min.js'></script>
    <script type='text/javascript' src='<?php echo URL;?>public/js/jquery-ui-tabs-rotate.min.js'></script>

    <!-- Contact Form JavaScript -->
    <script src="<?php echo URL;?>public/js/jqBootstrapValidation.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo URL;?>public/js/script.js"></script>

    <script src="<?php echo URL;?>public/js/smooth-scroll.min.js"></script>

    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

  <script type="text/javascript">
    $(document).ready(function(){
        $('.nav a[href^="#"]').on('click',function (e) {
                e.preventDefault();

                var target = this.hash;
                var $target = $(target);

                $('html, body').stop().animate({
                    'scrollTop': $target.offset().top - 50
                }, 900, 'swing', function () {
                    window.location.hash = target;
                });
                
                $('nav li.active').removeClass('active');
                $('nav li').eq(e).addClass('active');
            });
        
        //Check to see if the window is top if not then display button
        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('.scrollToTop').fadeIn();
            } else {
                $('.scrollToTop').fadeOut();
            }
        });
        
        //Click event to scroll to top
        $('.scrollToTop').click(function(){
            $('html, body').animate({scrollTop : 0},800);
            return false;
        });
        
    });
  </script>
    <script type="text/javascript">
        $(document).ready(function(){
                //menu
                var url = String(window.location);
                url = url.split("?");
                url = url[0];
                url = url.replace("#","");
                url = url.replace("/vi","");
                url = url.replace("/en","");
                // Will only work if string in href matches with location
                var nodeParent = $('#bs-example-navbar-collapse-1 li a[href="'+ url +'"]').parents();
                if(nodeParent[2].nodeName.toLowerCase() !== 'li')
                {
                    console.log(1);
                    $('#bs-example-navbar-collapse-1 li a[href="'+ url +'"]').parent().addClass('active');
                }
                else {
                    console.log(2);
                    $('#bs-example-navbar-collapse-1 li a[href="'+ url +'"]').closest('li.has-sub').addClass('active');
                }
                // Will also work for relative and absolute hrefs
            });
    </script>
</body>
</html>