<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= isset($this->title)?$this->title:'Nha Thuoc'?></title>
    <link href="<?= URL?>public/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= URL?>public/bootstrap/css/plugins/morris.css" rel="stylesheet">
    <link href="<?= URL?>public/bootstrap/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body style="background-color: #222">

    <?php echo $this->content();?>

    <script src="<?= URL?>public/bootstrap/js/jquery.js"></script>
    <script src="<?= URL?>public/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>
