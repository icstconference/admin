<!doctype html>
<html class="fixed">
	<head>
		<!-- Basic -->
		<meta charset="UTF-8">

		<title>Trang quản trị Conference</title>

		<meta name="keywords" content="" />
		<meta name="description" content="Trang quản trị Conference">
		<link rel="shortcut icon" href="<?php echo URL;?>public/img/favicon.ico" />

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo URL;?>public/assets/vendor/bootstrap/css/bootstrap.css" />

		<link rel="stylesheet" href="<?php echo URL;?>public/assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="<?php echo URL;?>public/assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="<?php echo URL;?>public/assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?php echo URL;?>public/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />
		<link rel="stylesheet" href="<?php echo URL;?>public/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<link rel="stylesheet" href="<?php echo URL;?>public/assets/vendor/morris/morris.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="<?php echo URL;?>public/assets/vendor/select2/select2.css" />
		<link rel="stylesheet" href="<?php echo URL;?>public/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css" />

		<link rel="stylesheet" href="<?php echo URL;?>public/assets/vendor/dropzone/css/dropzone.css" />
		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo URL;?>public/assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?php echo URL;?>public/assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo URL;?>public/assets/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="<?php echo URL;?>public/assets/vendor/modernizr/modernizr.js"></script>
		<?php
	        $this->appendArrayStyleSheet($this->css);
	    ?>
		<script>
	        var URL_G = '<?php echo URL;?>';
	        var lang = '<?php echo $this->lang;?>';
	        var idimage = 0;
	    </script>
	</head>
	<body>
		<span id="checkadmin" rel="<?php echo $_SESSION['user_type'];?>"></span>
		<section class="body">
			<header class="header">
				<div class="logo-container">
					<a href="<?php echo URL;?>admin" class="logo">
						<img src="<?php echo URL;?>public/img/icstlogo.png" height="35" />
					</a>
					<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>

				<div class="header-right">
					<div id="userbox" class="userbox" style="margin-top: 10px;">
						<a href="#" data-toggle="dropdown">
							<figure class="profile-picture">
								<img src="<?php echo URL;?>public/img/defavatar.png" alt="<?php echo lib_session::Get("user_name");?>" class="img-circle" data-lock-picture="<?php echo URL;?>public/assets/images/!logged-user.jpg" />
							</figure>
							<div class="profile-info" data-lock-name="<?php echo lib_session::Get("user_name");?>" data-lock-email="johndoe@okler.com">
								<span class="name"><?php echo lib_session::Get("user_name");?></span>
								<span class="role">Quản trị viên</span>
							</div>
					
							<i class="fa custom-caret"></i>
						</a>
					
						<div class="dropdown-menu">
							<ul class="list-unstyled">
								<li class="divider"></li>
								<li>
									<a role="menuitem" tabindex="-1" target="_blank" href="<?php echo URL;?>"><i class="fa fa-user"></i> Xem website</a>
								</li>
								<li>
									<a role="menuitem" tabindex="-1" href="<?php echo URL;?>logout"><i class="fa fa-power-off"></i> Đăng xuất</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</header>

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<aside id="sidebar-left" class="sidebar-left">
				
					<div class="sidebar-header">
						<div class="sidebar-title">
							Navigation
						</div>
						<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
							<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
						</div>
					</div>
				
					<div class="nano has-scrollbar">
						<div class="nano-content">
							<nav id="menu" class="nav-main" role="navigation">
								<ul class="nav nav-main">
									<li class="nav-active">
										<a href="<?php echo URL;?>admin">
											<i class="fa fa-home" aria-hidden="true"></i>
											<span>Bảng điều khiển</span>
										</a>
									</li>
									<?php 
										$create = $_SESSION['user_permission_create'];
										$delete = $_SESSION['user_permission_delete'];
										$edit = $_SESSION['user_permission_edit'];
				                    ?>
									<?php                                         
										if($_SESSION['user_type'] == 1 || strpos($create[0]['permission_detail'],'event') || strpos($edit[0]['permission_detail'],'event') || strpos($delete[0]['permission_detail'],'event')){                                    
									?>
									<li class="nav-parent">
										<a>
											<i class="fa fa-recycle" aria-hidden="true"></i>
											<span>Sự kiện</span>
										</a>
										<ul class="nav nav-children">
											<li>
												<a href="<?php echo URL;?>event/event/">
													Tất cả sự kiện
												</a>
											</li>
											<li>
												<a href="<?php echo URL;?>event/addevent/">
													Thêm sự kiện
												</a>
											</li>
											<li class="nav-parent">
												<a>
													<span>Tin tức sự kiện</span>
												</a>
												<ul class="nav nav-children">
													<li>
														<a href="<?php echo URL;?>news/">
															Tất cả bài viết
														</a>
													</li>
													<li>
														<a href="<?php echo URL;?>news/addnews/">
															Viết bài mới
														</a>
													</li>
													<li>
														<a href="<?php echo URL;?>news/media/">
															Hình ảnh bài viết
														</a>
													</li>
												</ul>
											</li>
											<li class="nav-parent">
												<a>
													<span>Deadline sự kiện</span>
												</a>
												<ul class="nav nav-children">
													<li>
														<a href="<?php echo URL;?>deadline/">
															Tất cả deadline
														</a>
													</li>
												</ul>
											</li>
											<li class="nav-parent">
						                        <a href="<?php echo URL;?>partner/" >
						                            <span class="hidden-xs">Sponsor sự kiện</span>
						                        </a>
						                        <ul class="nav nav-children">
													<li>
														<a href="<?php echo URL;?>partner/">
															Tất cả ponsor
														</a>
													</li>
													<li>
														<a href="<?php echo URL;?>partner/categories/">
															Loại sponsor
														</a>
													</li>
												</ul>
						                    </li>
						                    <li class="nav-parent">
												<a>
													<span>Organizer sự kiện</span>
												</a>
												<ul class="nav nav-children">
													<li>
														<a href="<?php echo URL;?>organize/">
															Tất cả organizer
														</a>
													</li>
													<li>
														<a href="<?php echo URL;?>organize/addorganize/">
															Thêm organizer
														</a>
													</li>
												</ul>
											</li>
										</ul>
									</li>
									<?php } ?>
									<?php                                         
										if($_SESSION['user_type'] == 1 || strpos($create[0]['permission_detail'],'artist') || strpos($edit[0]['permission_detail'],'artist') || strpos($delete[0]['permission_detail'],'artist')){                                    
									?>
									<li class="nav-parent">
										<a>
											<i class="fa fa-user-secret" aria-hidden="true"></i>
											<span>Diễn giả</span>
										</a>
										<ul class="nav nav-children">
											<li>
												<a href="<?php echo URL;?>artist/artist/">
													Tất cả diễn giả
												</a>
											</li>
											<li>
												<a href="<?php echo URL;?>artist/addartist/">
													Thêm diễn giả
												</a>
											</li>
											<li>
												<a href="<?php echo URL;?>artist/listartist/">
													List diễn giả
												</a>
											</li>
										</ul>
									</li>
									<?php } ?>
									<?php                                         
										if($_SESSION['user_type'] == 1 || strpos($create[0]['permission_detail'],'page') || strpos($edit[0]['permission_detail'],'page') || strpos($delete[0]['permission_detail'],'page')){                                    
									?>
									<li class="nav-parent">
				                        <a>
				                            <i class="fa fa-file-o"></i>
				                            <span class="hidden-xs">Trang</span>
				                        </a>
				                        <ul class="nav nav-children">
											<li>
												<a href="<?php echo URL;?>page/">
													Tất cả các trang
												</a>
											</li>
											<li>
												<a href="<?php echo URL;?>page/addpage/">
													Thêm trang mới
												</a>
											</li>
											<li>
										</ul>
				                    </li>
									<?php } ?>
									<?php                                         
										if($_SESSION['user_type'] == 1){                                    
									?>
									<li class="nav-parent">
				                        <a>
											<i class="fa fa-calendar" aria-hidden="true"></i>
											<span>Lập lịch</span>
										</a>
										<ul class="nav nav-children">
											<li>
												<a href="<?php echo URL;?>calendar/room/">
													Quản lý phòng
												</a>
											</li>
											<li>
												<a href="<?php echo URL;?>calendar/">
													Lập schedule
												</a>
											</li>
										</ul>
				                    </li>
									<?php } ?>
									<?php                                         
										if($_SESSION['user_type'] == 1){                                    
									?>
									<li class="nav-parent">
										<a>
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Người dùng</span>
										</a>
										<ul class="nav nav-children">
											<li>
												<a href="<?php echo URL;?>user/">
													Toàn bộ người dùng
												</a>
											</li>
											<li>
												<a href="<?php echo URL;?>user/group/">
													Nhóm người dùng
												</a>
											</li>
											<li>
												<a href="<?php echo URL;?>user/adduser/">
													Thêm người dùng mới
												</a>
											</li>
											<li>
												<a href="<?php echo URL;?>user/permission">
													Danh sách quyền
												</a>
											</li>
										</ul>
									</li>
									<?php } ?>
									<?php                                         
										if($_SESSION['user_type'] == 1){                                    
									?>
									<li class="nav-parent">
										<a>
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Người dùng</span>
										</a>
										<ul class="nav nav-children">
											<li>
												<a href="<?php echo URL;?>user/">
													Toàn bộ người dùng
												</a>
											</li>
											<li>
												<a href="<?php echo URL;?>user/group/">
													Nhóm người dùng
												</a>
											</li>
											<li>
												<a href="<?php echo URL;?>user/adduser/">
													Thêm người dùng mới
												</a>
											</li>
											<li>
												<a href="<?php echo URL;?>user/permission">
													Danh sách quyền
												</a>
											</li>
										</ul>
									</li>
									<?php } ?>
									<?php                                         
										if($_SESSION['user_type'] == 1 || strpos($create[0]['permission_detail'],'contact') || strpos($edit[0]['permission_detail'],'contact') || strpos($delete[0]['permission_detail'],'contact')){                                    
									?>
									<li>
				                        <a href="<?php echo URL;?>admin/contact/" >
				                            <i class="fa fa-phone"></i>
				                            <span class="hidden-xs">Liên hệ</span>
				                        </a>
				                    </li>
									<?php } ?>
									<?php                                         
										if($_SESSION['user_type'] == 1){                                    
									?>
									<li>
				                        <a href="<?php echo URL;?>contact/contact/" >
				                            <i class="fa fa-rss"></i>
				                            <span class="hidden-xs">Danh sách liên hệ</span>
				                        </a>
				                    </li>
									<?php } ?>
									<?php                                         
										if($_SESSION['user_type'] == 1){                                    
									?>
									<li class="nav-parent">
										<a>
				                            <i class="fa fa-exchange"></i>
				                            <span class="hidden-xs">Chức năng mở rộng</span>
				                        </a>
										<ul class="nav nav-children">
											<li>
												<a href="<?php echo URL;?>menu/">
													<i class="fa fa-tasks" aria-hidden="true"></i>
													<span>Menu</span>
												</a>
											</li>
											<li>
												<a href="<?php echo URL;?>log/">
													<i class="fa fa-history" aria-hidden="true"></i>
													<span>Lịch sử quản trị</span>
												</a>
											</li>
											<li>
												<a href="<?php echo URL;?>google/">
													<i class="fa fa-sitemap" aria-hidden="true"></i>
													<span>Google XML</span>
												</a>
											</li>
											<li>
												<a href="<?php echo URL;?>admin/setting/">
													<i class="fa fa-cog" aria-hidden="true"></i>
													<span>Cấu hình website</span>
												</a>
											</li>
											<li class="nav-parent">
												<a>
													<i class="fa fa-support"></i>
													<span class="hidden-xs">Backup dữ liệu</span>
												</a>
												<ul class="nav nav-children">
													<li>
														<a href="<?php echo URL;?>excel/export/">
															Xuất dữ liệu
														</a>
													</li>
													<li>
														<a href="<?php echo URL;?>excel/import/">
															Thêm dữ liệu
														</a>
													</li>
													<li>
												</ul>
											</li>
										</ul>
									</li>
									<?php } ?>
								</ul>
							</nav>
				
							<hr class="separator" />
						</div>
				
					</div>
				
				</aside>

				<section role="main" class="content-body">
					<?php echo $this->content();?>
				</section>

		</section>

		<script src="<?php echo URL;?>public/assets/vendor/jquery/jquery.js"></script>
		<script src="<?php echo URL;?>public/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="<?php echo URL;?>public/assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="<?php echo URL;?>public/assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="<?php echo URL;?>public/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="<?php echo URL;?>public/assets/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="<?php echo URL;?>public/assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>

		<script src="<?php echo URL;?>public/assets/vendor/isotope/jquery.isotope.js"></script>
		<script src="<?php echo URL;?>public/assets/vendor/ios7-switch/ios7-switch.js"></script>
	

		<!-- Specific Page Vendor -->
		<script src="<?php echo URL;?>public/assets/vendor/select2/select2.js"></script>
		<script src="<?php echo URL;?>public/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
		<script src="<?php echo URL;?>public/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
		<script src="<?php echo URL;?>public/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo URL;?>public/assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo URL;?>public/assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo URL;?>public/assets/javascripts/theme.init.js"></script>

		<script src="<?php echo URL;?>public/assets/javascripts/tables/examples.datatables.default.js"></script>
		<script src="<?php echo URL;?>public/assets/javascripts/tables/examples.datatables.row.with.details.js"></script>
		<script src="<?php echo URL;?>public/assets/javascripts/tables/examples.datatables.tabletools.js"></script>

		<script src="<?php echo URL;?>public/assets/javascripts/ui-elements/examples.modals.js"></script>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

		<script src="<?php echo URL;?>public/assets/vendor/dropzone/dropzone.js"></script>

		<script src="<?php echo URL;?>public/js/image.js"></script>

		<?php
    		$this->appendArrayScript($this->js);
    	?>
		<script src="<?php echo URL;?>public/ckeditor/config.js"></script>
	</body>
</html>
