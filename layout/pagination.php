<ul class="pagination">
    <?= $paginator['first']?>
    <?= $paginator['previous']?>
    <?php foreach($paginator['items'] as $item):?>
    <?= $item?>
    <?php endforeach;?>
    <?= $paginator['next']?>
    <?= $paginator['last']?>
</ul>