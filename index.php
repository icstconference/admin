<?php
ini_set('display_errors', 1);
error_reporting(E_ERROR | E_WARNING | E_PARSE);
include_once 'config/config.php';
/*
 * include autoload
 */
function __autoload($class){
    $array_file = FindFile($class);
    if(count($array_file) > 1)
    {
        echo "<b>ERROR: </b>You have two files with the same structure :<br />";
        foreach($array_file as $file)
        {
            echo $file."<br/>";
        }
    }
    else
    {
        if(count($array_file) <= 0)
        {
            echo "<b>ERROR: </b>File does not exist : ".$class.".php<br />";
        }
        else {
            include(reset($array_file));
        }
    }
}

$app = new lib_app();
?>