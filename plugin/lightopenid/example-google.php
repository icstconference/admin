<?php
# Logging in with Google accounts requires setting special identity, so this example shows how to do it.
require 'openid.php';
try {
    # Change 'localhost' to your domain name.
    $openid = new LightOpenID('localhost');
    if(!$openid->mode) {
        if(isset($_GET['login'])) {
            //$openid->identity = 'https://www.google.com/accounts/o8/id';
            $openid->identity = 'https://me.yahoo.com/';
            $openid->required = array(
                'namePerson/first',
                'namePerson/last',
                'contact/email',
                'birthDate',
                'person/gender',
            );
           //$openid->returnUrl='';
            header('Location: ' . $openid->authUrl());
        }
?>
<form action="?login" method="post">
    <button>Login with Google</button>
</form>
<?php
    } elseif($openid->mode == 'cancel') {
        echo 'User has canceled authentication!';
    } else {
        echo 'User ' . ($openid->validate() ? $openid->identity . ' has ' : 'has not ') . 'logged in.';
        print_r($openid->getAttributes());
    }
} catch(ErrorException $e) {
    echo $e->getMessage();
}
