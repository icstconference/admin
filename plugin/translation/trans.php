<?php

class Trans {
    private $lang = null;
    public function __construct($lang) {
        $this->lang = $lang;
    }
    public function GetValueTrans(){
        $file_lang = 'plugin/translation/'.$this->lang.'.php';
        if (file_exists($file_lang)) {
            return include_once $file_lang;            
        }
        return false;
    }

}
