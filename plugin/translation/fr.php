<?php
return array(
    'SETTING'       => 'Réglage',
    'PROFILE'       => 'Profil',
    'FULL_SCREEN'   => 'Plein Écran',
    'LOG_OUT'       => 'Déconnexion',
    'DASHBOARD'     => 'Tableau de bord',
    'PRODUCT_FINANCE'   => 'Finances de produit',
    'PRICING_TABLES'=> 'Echéancier',
    'NEWS'          => 'Nouvelles',
    'LIST_NEWS'     => 'Liste Nouvelles',
    'CUSTOMER'      => 'Clientèle',
    'NEW_CUSTOMER'  => 'Nouveau Client',
    'LIST_CUSTOMER' => 'Liste Clientèle',
    'ANALYZE'       => 'Analyser',
    'FLOT_CHART'    => 'Flot Graphique',
    'CUSTOMER_INVOICE' => 'Customer Invoice',
    'FILE_MANAGER'  => 'Gestionnaire de fichiers',
    'FILES'         => 'Fichiers',
    'CHOOSE_SKIN'   => 'Choisissez peau'
);
?>