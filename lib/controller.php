<?php

class lib_controller {

    function __construct() {
        if (!isset($_SESSION)) {
            lib_session::Init();
        }
        $this->view = new lib_view();
        //auto load
        $this->AutoLoad();
    }

    /*
     * Function Load config page
     */

    protected function AutoLoad() {

        //auto load translate
        $this->AddLinkLang(LANG_DEF);
        /*
         * Auto load CSRF security
         * And check Token method $_POST, $_GET
         */
        if (!isset($_SESSION['token'])) {
            $this->GetToken();
        }
        //check Token author method $_POST
        $this->CheckPostToken();
        //check Token autor method $_GET
        $this->CheckGetToken();
        /*
         * Security session Hijacking
         */
        if (isset($_SESSION['HTTP_USER_AGENT']) && isset($_SESSION['REMOTE_ADDR'])) {
            if ($_SESSION['HTTP_USER_AGENT'] != md5($_SERVER['HTTP_USER_AGENT']) && $_SESSION['REMOTE_ADDR'] != md5($_SERVER['REMOTE_ADDR'])) {
                header('Location: ' . URL . 'protect');
                exit;
            }
        } else {
            $_SESSION['REMOTE_ADDR'] = md5($_SERVER['REMOTE_ADDR']);
            $_SESSION['HTTP_USER_AGENT'] = md5($_SERVER['HTTP_USER_AGENT']);
            return true;
        }
        //load data
        $this->GetData();
    }

    /*
     * Load translate class
     * @return view translate
     */

    protected function LoadTranslate() {
        include_once 'plugin/translation/trans.php';
        if (isset($_COOKIE['lang_curr'])) {
            $trans = new Trans($_COOKIE['lang_curr']);
            $this->view->translate = $trans->GetValueTrans();
            return true;
        } else {
            $trans = new Trans(LANG_DEF);
            $this->view->translate = $trans->GetValueTrans();
            return true;
        }
    }

    /*
     * add link language
     */

    protected function AddLinkLang($country_lang) {
        $this->view->lang = '';
        $lang_curr = $country_lang;
        $str_link = $_GET['r'];

        $url_q = explode('/', $str_link);
        if ($url_q[0] == '') {
            // if (isset($_COOKIE['lang_curr'])) {
            //     $lang = preg_split('/,/', LANG);
            //     if (in_array($_COOKIE['lang_curr'], $lang)) {
            //         header("HTTP/1.1 301 Moved Permanently");
            //         header("Location: " . URL . $_COOKIE['lang_curr']);
            //         exit();
            //     }
            // }else{
            //     setcookie("lang_curr", $country_lang, time() + 100 * 3600, "/");
            //     header("HTTP/1.1 301 Moved Permanently");
            //     header("Location: " . URL . $country_lang);
            //     exit();
            // }
            
        }
        $lang = preg_split('/,/', LANG);
        if (in_array($url_q[0], $lang)) {
            // $lang_curr = $url_q[0];
            // $str_link = preg_replace('/' . $lang_curr . '\//', '', $_GET['r']);
            // if (in_array($str_link, $lang)) {
            //     $str_link = '';
            // }
            // //set again lang
            // $check_url = strtolower($url_q[0]);
            // if($check_url!=$_COOKIE['lang_curr']){
            //     setcookie("lang_curr", $check_url, time() + 100 * 3600, "/");
            //     header("HTTP/1.1 301 Moved Permanently");
            //     header("Location: " . URL .$check_url.'/'. $str_link);
            //     exit();
            // }
        }else{
            // if(isset($_COOKIE['lang_curr'])){
            //     $lang_curr = $_COOKIE['lang_curr'];
            // }else{
            //     $lang_curr=LANG_DEF;
            // }
            // $str_link = preg_replace('/' . $lang_curr . '\//', '', $_GET['r']);
            // if (in_array($str_link, $lang)) {
            //     $str_link = '';
            // }
            // header("HTTP/1.1 301 Moved Permanently");
            // header("Location: " . URL .$lang_curr.'/'. $str_link);
            // exit();
        }

        $this->view->lang = $lang_curr;

        $this->view->link_lang = $str_link;


        //
        $this->LoadTranslate();
    }

    /*
     * CSRF protect form submit
     */

    function GetToken() {
        return $_SESSION["token"] = md5(uniqid(mt_rand(), true));
    }

    /*
     * And check CSRF protect
     * True is right
     * False is wrong
     */

    function CheckGetToken() {
        if (isset($_GET["token"])) {
            if ($_GET["token"] == $_SESSION["token"]) {
                return true;
            }
        }
        return false;
    }

    function CheckPostToken() {
        if (isset($_POST['token'])) {
            if ($_POST['token'] == $_SESSION['token']) {
                return true;
            } else {
                header('Location: ' . URL . 'protect');
                exit();
            }
        }
        return false;
    }

    /*
     * Get subject id
     */

    public function GetUnitId($unit_path) {
        $unit_parse = preg_split('/-/', $unit_path);
        $unit_id = end($unit_parse);
        return $unit_id;
    }

    //Get subject id from course path
    public function GetSubjectId($subject_path) {
        $subject_parse = preg_split('/-/', $subject_path);
        $subject_id = end($subject_parse);
        return $subject_id;
    }

    //location
    function Location($url) {
        if (lib_session::Logged() === FALSE) {
            lib_session::Set('url', $url);
            header('Location: ' . URL . 'login?url=' . urlencode($url));
            exit();
        }
        return true;
    }

    function Actual_Link() {
        return "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }

    function SetViewVariable($key, $value) {
        $this->view->{$key} = $value;
    }

    protected function GetData() {

    }

}

?>
