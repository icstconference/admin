<?php
    class lib_messenger {
        
        public static function SetErrorMessenger($messenger) {
            $_SESSION['messenger']['error'] = $messenger;
        }
        
        public static function SetSuccessMessenger($messenger) {
            $_SESSION['messenger']['success'] = $messenger;
        }
        
        public static function GetErrorMessenger() {
            if(isset($_SESSION['messenger']['error'])) {
                return $_SESSION['messenger']['error'];
            }
            return false;
        }
        
        public static function GetSuccessMessenger() {
            if(isset($_SESSION['messenger']['success'])) {
                return $_SESSION['messenger']['success'];
            }
            return false;
        }
        
        public static function GetErrorMessengerBootstrap() {
            if(isset($_SESSION['messenger']['error'])) {
                $string     = '<div class="alert alert-danger alert-dismissible fade in" role="alert">';
                $string     = $string . '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
                $string     = $string . $_SESSION['messenger']['error'];
                $string     = $string . ' </div>';
                    
                return $string;
            }
            return false;
        }
        
        public static function GetSuccessMessengerBootstrap() {
            if(isset($_SESSION['messenger']['success'])) {
                $string     = '<div class="alert alert-success alert-dismissible fade in" role="alert">';
                $string     = $string . '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
                $string     = $string . $_SESSION['messenger']['success'];
                $string     = $string . ' </div>';
                    
                return $string;
            }
            return false;
        }
        
        public static function DestroyMessenger() {
            if(isset($_SESSION['messenger'])) {
                unset($_SESSION['messenger']);
                return true;
            }
            return false;
        }
    }
?>