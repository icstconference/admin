<?php

class lib_session {

    public static function Init() {
        session_start();
//        session_name('web');
    }

    public static function Set($key, $value) {
        //set security session Fixation
        session_regenerate_id();
        $_SESSION[$key] = $value;
    }

    public static function Get($key) {
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        }
        return false;
    }

    public static function Del($key) {
        unset($_SESSION['$key']);
    }

    public static function Destroy() {
        session_destroy();
    }

    public static function Logged() {
        if (empty($_SESSION['user_id']) && empty($_SESSION['user_name'])) {
            return false;
        }
        return true;
    }

    public static function IsUser($user_id) {
        if (self::Logged() === true) {
            if (self::Get('user_id') == $user_id) {
                return true;
            }
        }
        return false;
    }

    public static function IsAdmin() {
        if (self::Get('user_type') == 'admin') {
            return true;
        }
        return false;
    }

}

?>
