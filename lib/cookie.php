<?php
class lib_cookie{
    public static function Set($key, $value,$time=null){
        setcookie($key,$value,$time==null?time()+3600:$time);
        return true;
    }
    public static function Get($key){
        if(isset($_COOKIE[$key])){
            return $_COOKIE[$key];
        }
        return false;
    }
    public static function Destroy(){
        unset($_COOKIE);
        return true;
    }
    public static function Del($key, $time=null){
        unset($_COOKIE[$key]);
        setcookie($key, null, $time==null?time() - 3600:$time);
        return true;
    }

}
?>
