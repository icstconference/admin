<?php

/*
 * Lib app in MVC
 * Custom link request
 */

class lib_app{

    public $content;
    public $layout;
    public $layoutDirectory;
    public $viewDirectory;
    public $layoutBaseDirectory;
    public $viewBaseDirectory;

    function __construct() {
        $url = isset($_GET['r']) ? $_GET['r'] : null;
        $url_lower = strtolower($url);
        $url_rel = preg_split("/.html/", $url_lower);
        $url_query = preg_replace('/-/', '_', $url_rel[0]);
        $url_q = explode('/', $url_query);
        $lang = preg_split('/,/', LANG);

        $this->layoutDirectory      = 'layout/';
        $this->layoutBaseDirectory  = 'layout/';
        $this->viewDirectory        = 'view/';
        $this->viewBaseDirectory    = 'view/';
        $this->layout               = 'layout';


        if(in_array($url_q[0],$lang)){           
            $this->QueryLang($url_q);
            return true;
        }
        $this->QueryNormal($url_q);
        return true;
    }

    function Render($name) {
        $this->content = $this->viewDirectory . $name . '.php';
        include_once $this->layoutDirectory.$this->layout.'.php';
        $this->viewDirectory    = $this->viewBaseDirectory;
        $this->layoutDirectory  = $this->layoutBaseDirectory;
        lib_messenger::DestroyMessenger();
    }
    
    
    // hàm này duoc goi trong layout de the hien noi dung
    public function content()
    {
        include_once $this->content;
    }
    
    // dùng để set layout
    public function layout($name)
    {
        $this->layout = $name;
        return $this;
    }

    // Thêm array Script
    public function appendArrayScript($data)
    {
        if(gettype($data) == 'array' && count($data) > 0)
        {
            foreach($data as $item)
            {
                if(gettype($item) == 'array') {
                    $this->appendScript($item[0], end($item));
                }
                else {
                    $str = "<script type='text/javascript' src='".$item."'></script>\n";
                    echo $str;
                }
            }
        }
        return $this;
    }
    
    // Thêm array style sheet
    public function appendArrayStyleSheet($data)
    {
        if(gettype($data) == 'array' && count($data) > 0)
        {
            foreach($data as $item)
            {
                $str = "<link rel='stylesheet' type='text/css' href='".$item."' />\n";
                echo $str;
            }
        }
        return $this;
    }

    // Dùng để thêm một thể hiện vào trang
    public function partial($name, $vars = array())
    {
        if(count($vars) != 0)
        {
            foreach($vars as $key => $value)
            {
                ${$key}     = $value;
            }
        }
        include_once $this->layoutDirectory.$name.'.php';
        $this->layoutDirectory = $this->layoutBaseDirectory;
    }
    

    private function QueryNormal($url_q) {        
        if (empty($url_q[0])) {
            include_once 'controller/index.php';
            $controler = new controller_index();
            $controler->Index();
            return false;
        }

        if(strpos($url_q[0],'_') !== false){

            $urlcheck = str_replace('_', '-', $url_q[0]);

            $parts = explode('_',$url_q[0]);

            $news = new model_news();

            $page = new model_page();

            $product = new model_product();

            $file = new model_file();

            $artist = new model_artist();

            $event = new model_event();

            if($parts[count($parts)-2] == 'post'){
                if($news->SelectNewByUrl($urlcheck)){
                    $newsinfo = $news->SelectNewByUrl($urlcheck);

                    $this->news($url_q[0]);
                }else{
                    header('Location:'.URL);
                }
            }

            if($parts[count($parts)-2] == 'page'){
                if($page->GetPageByUrl($urlcheck)){
                    $pageinfo = $page->GetPageByUrl($urlcheck);

                    $this->page($url_q[0]);
                }else{
                    header('Location:'.URL);
                }
            }

            if($parts[count($parts)-2] == 'product'){
                if($product->GetProductByUrl($urlcheck)){
                    $productinfo = $product->GetProductByUrl($urlcheck);

                    $this->product($url_q[0]);
                }else{
                    header('Location:'.URL);
                }
            }

            if($parts[count($parts)-2] == 'postcategory'){
                if($news->GetCategoriesNewsByUrl($urlcheck)){
                    $news = $news->GetCategoriesNewsByUrl($urlcheck);

                    $this->newscategory($url_q[0]);
                }else{
                    header('Location:'.URL);
                }
            }

            if($parts[count($parts)-2] == 'productcategory'){
                if($product->GetCategoriesProductByUrl($urlcheck)){
                    $productinfo = $product->GetCategoriesProductByUrl($urlcheck);

                    $this->productcategory($url_q[0]);
                }else{
                    header('Location:'.URL);
                }
            }

            if($parts[count($parts)-2] == 'file'){
                if($file->GetFileCategoriesByUrl($urlcheck)){
                    $fileinfo = $file->GetFileCategoriesByUrl($urlcheck);

                    $this->file($url_q[0]);
                }else{
                    header('Location:'.URL);
                }
            }

            if($parts[count($parts)-2] == 'artist'){
                if($artist->GetArtistByUrl($urlcheck)){
                    $artistinfo = $artist->GetArtistByUrl($urlcheck);

                    $this->artist($url_q[0]);
                }else{
                    header('Location:'.URL);
                }
            }

            if($parts[count($parts)-2] == 'event'){
                if($event->GetEventByUrl($urlcheck)){
                    $eventinfo = $event->GetEventByUrl($urlcheck);

                    $this->event($url_q[0]);
                }else{
                    header('Location:'.URL);
                }
            }
        }
        $file_name = 'controller/' . $url_q[0] . '.php';
        if (!file_exists($file_name)) {
            //header('Location: ' . URL . 'error');
            return false;
        }
        include_once $file_name;

        $class_name = 'controller_' . $url_q[0];
        $controler = new $class_name;

        if (isset($url_q[1]) && $url_q[1] != null) {
            if (method_exists($controler, $url_q[1])) {
                if (isset($url_q[2]) && $url_q[2] != null) {
                    $params = $url_q;
                    unset($params[0]);
                    unset($params[1]);
                    call_user_func_array(array($controler, $url_q[1]), $params);
                    return false;
                } else {
                    $controler->{$url_q[1]}();
                    return false;
                }
            } else {
                $params = $url_q;
                unset($params[0]);
                call_user_func_array(array($controler, 'index'), $params);
                return false;
            }
        } else {
            $controler->Index();
        }
    }

    private function QueryLang($url_q){       
        if (empty($url_q[1])) {
            include_once 'controller/index.php';
            $controler = new controller_index();
            $controler->Index();
            return false;
        }
        if(strpos($url_q[1],'_') > 1){
            $lang = preg_split('/,/', LANG);
            if (in_array($url_q[0], $lang)) {
                $lang_curr = $url_q[0];
                $_COOKIE['lang_curr'] = $lang_curr;
            }

            $urlcheck = str_replace('_', '-', $url_q[1]);

            $parts = explode('_',$url_q[1]);

            $news = new model_news();

            $page = new model_page();

            $product = new model_product();

            $file = new model_file();

            if($parts[count($parts)-2] == 'post'){
                if($news->SelectNewByUrl($urlcheck)){
                    $newsinfo = $news->SelectNewByUrl($urlcheck);

                    $this->news($url_q[1]);
                }else{
                    header('Location:'.URL);
                }
            }

            if($parts[count($parts)-2] == 'page'){
                if($page->GetPageByUrl($urlcheck)){
                    $pageinfo = $page->GetPageByUrl($urlcheck);

                    $this->page($url_q[1]);
                }else{
                    header('Location:'.URL);
                }
            }

            if($parts[count($parts)-2] == 'product'){
                if($product->GetProductByUrl($urlcheck)){
                    $productinfo = $product->GetProductByUrl($urlcheck);

                    $this->product($url_q[1]);
                }else{
                    header('Location:'.URL);
                }
            }

            if($parts[count($parts)-2] == 'postcategory'){
                if($news->GetCategoriesNewsByUrl($urlcheck)){
                    $news = $news->GetCategoriesNewsByUrl($urlcheck);

                    $this->newscategory($url_q[1]);
                }else{
                    header('Location:'.URL);
                }
            }

            if($parts[count($parts)-2] == 'productcategory'){
                if($product->GetCategoriesProductByUrl($urlcheck)){
                    $productinfo = $product->GetCategoriesProductByUrl($urlcheck);

                    $this->productcategory($url_q[1]);
                }else{
                    header('Location:'.URL);
                }
            }

            if($parts[count($parts)-2] == 'file'){
                if($file->GetFileCategoriesByUrl($urlcheck)){
                    $fileinfo = $file->GetFileCategoriesByUrl($urlcheck);

                    $this->file($url_q[1]);
                }else{
                    header('Location:'.URL);
                }
            }
        }
         $file_name = 'controller/' . $url_q[1] . '.php';
        if (!file_exists($file_name)) {
            //header('Location: ' . URL . 'error');
            return false;
        }
        include_once $file_name;

        $class_name = 'controller_' . $url_q[1];
        $controler = new $class_name;

        if (isset($url_q[2]) && $url_q[2] != null) {
            if (method_exists($controler, $url_q[2])) {
                if (isset($url_q[3]) && $url_q[3] != null) {
                    $params = $url_q;
                    unset($params[0]);
                    unset($params[1]);
                    unset($params[2]);
                    call_user_func_array(array($controler, $url_q[2]), $params);
                    return false;
                } else {
                    $controler->{$url_q[2]}();
                    return false;
                }
            } else {
                $params = $url_q;
                unset($params[0]);
                unset($params[1]);
                call_user_func_array(array($controler, 'index'), $params);
                return false;
            }
        } else {
            $controler->Index();
        }
    }

    public function news($path){
        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $partner = new model_partner();

        $library = new model_library();
        
        $video = new model_video();

        $urlcheck = str_replace('_', '-', $path);

        $this->link_lang = $urlcheck.'.html'; 

        $this->lang = $_COOKIE['lang_curr'];

        if($allsetting){
            $this->title = $allsetting[0]['config_title'];
            $this->description = $allsetting[0]['config_description'];
            $this->keyword = $allsetting[0]['config_keyword'];
            $this->logo = $allsetting[0]['config_logo'];
            $this->favicon = $allsetting[0]['config_icon'];
            $this->google = $allsetting[0]['config_google'];
            $this->facebook = $allsetting[0]['config_facebook'];
            $this->twitter = $allsetting[0]['config_twitter'];
            $this->linkedin = $allsetting[0]['config_linkedin'];
            $this->configurl = $allsetting[0]['config_url'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOn()){
            $allmenufather = $model1->SelectAllMenuFatherOn();

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->allmenu = $allmenufather;
        }


        $model4 = new model_news();

        if($model4->SelectAllCategoriesNews()){
            $this->listcategoriesnews = $model4->SelectAllCategoriesNews();
        }

        if($path){
            $part = explode('_',$path);
            $id = $part[count($part)-1];
            $result = $model4->SelectNewsById($id);

            if($result){

                foreach ($result as $key => $value) {
                    if($comment->NumCommentByPostId($id,'news')){
                        $numcomment = $comment->NumCommentByPostId($id,'news');
                        $result[$key]['news_comment'] = $numcomment[0]['numcomment'];
                    }
                }

                $view = intval($result[0]['view']) + 1;

                $this->newsinfo = $result;

                $catnews = $model4->GetCategoriesById($result[0]['news_categories_id']);

                $this->catinfo = $catnews;

                if($model4->SelectAllNewsByCatId($result[0]['news_categories_id'])){
                    $this->listrelatenews = $model4->SelectAllNewsByCatId($result[0]['news_categories_id']);
                }

                    $this->title = $result[0]['news_name'].'-'.$allsetting[0]['config_title'];
                    $this->description = $result[0]['news_name'].'-'.$allsetting[0]['config_description'];
                    $this->keyword = $result[0]['news_name'].','.$allsetting[0]['config_keyword'];
                

                if($catnews){
                    $this->catenews = $catnews;
                }

                $arr = array(
                    'view'  =>$view
                );

                $update = $model4->UpdateNews($arr,$id);

                if($news->SelectAllCategoriesNews()){
                    $this->listnewscategories = $news->SelectAllCategoriesNews();
                }

                if($comment->NumCommentByPostId($id,'news')){
                    $this->comment = $comment->NumCommentByPostId($id,'news');
                }

                if($comment->GetCommentByPostId($id,'news')){
                    $this->listcomment = $comment->GetCommentByPostId($id,'news');
                }

                if($news->SelectTopNewsRelease(4)){
                    $this->topnews = $news->SelectTopNewsRelease(4);
                }
            }
        }
        
        $useragent=$_SERVER['HTTP_USER_AGENT'];
        
 
        $this->layout('layout');
        $this->Render('news/detail');
        return true;
    }

    function event($path){
        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $partner = new model_partner();

        $library = new model_library();

        $event = new model_event();

        $urlcheck = str_replace('_', '-', $path);

        $this->link_lang = $urlcheck.'.html'; 

        $this->lang = $_COOKIE['lang_curr'];        

        if($allsetting){
            $this->title = $allsetting[0]['config_title'];
            $this->description = $allsetting[0]['config_description'];
            $this->keyword = $allsetting[0]['config_keyword'];
            $this->logo = $allsetting[0]['config_logo'];
            $this->favicon = $allsetting[0]['config_icon'];
            $this->google = $allsetting[0]['config_google'];
            $this->facebook = $allsetting[0]['config_facebook'];
            $this->twitter = $allsetting[0]['config_twitter'];
            $this->linkedin = $allsetting[0]['config_linkedin'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOnByPostion('all','ngang')){
            $allmenufather = $model1->SelectAllMenuFatherOnByPostion('all','ngang');

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->allmenu = $allmenufather;
        }

        if($model1->SelectAllMenuFatherOnByPostion('product','doc')){
            $allmenufatherproduct = $model1->SelectAllMenuFatherOnByPostion('product','doc');

            foreach($allmenufatherproduct as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufatherproduct[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->allmenuproduct = $allmenufatherproduct;
        }

        if($advertise->GetAdvertiseByPosition(1)){
            $this->listadvertise1 = $advertise->GetAdvertiseByPosition(1);
        }

        if($advertise->GetAdvertiseByPosition(2)){
            $this->listadvertise2 = $advertise->GetAdvertiseByPosition(2);
        }

        if($advertise->GetAdvertiseByPosition(3)){
            $this->listadvertise3 = $advertise->GetAdvertiseByPosition(3);
        }

        if($news->SelectTopNewsView(4)){
            $this->listnewsview = $news->SelectTopNewsView(4);
        }

        if($news->SelectTopNewsRelease(4)){
            $this->listnewsrelease = $news->SelectTopNewsRelease(4);
        }

        if($linker->SelectAlllinker()){
            $this->listlinker = $linker->SelectAlllinker();
        }

        if($news->SelectTopNewsRelease(5)){
            $this->listnewsnew = $news->SelectTopNewsRelease(5);
        }

        if($question->SelectAllQuestion()){
            $this->listquestion = $question->SelectAllQuestion();
        }

        if($partner->SelectAllPartner()){
            $this->listpartner = $partner->SelectAllPartner();
        }

        if($library->SelectAllLibrary()){
            $this->listlibrary = $library->SelectAllLibrary();
        }

        $comment = new model_comment();

        if($path){
            $part = explode('_',$path);
            $id = $part[count($part)-1];
            $result = $event->GetEventById($id);

            if($result){

                $this->title = $result[0]['event_name'].'-'.$allsetting[0]['config_title'];
                $this->description = $result[0]['event_name'].'-'.$allsetting[0]['config_description'];
                $this->keyword = $result[0]['event_name'].','.$allsetting[0]['config_keyword'];

                foreach ($result as $key => $value) {
                    if($event->GetAllEventDetailByEventId($id)){
                        $result[$key]['eventdetail'] = $event->GetAllEventDetailByEventId($id);
                    }
                }

                $artist = new model_artist();

                if($artist->GetAllArtist()){
                    $this->listartist = $artist->GetAllArtist();
                }

                if($event->GetAllEventAnother($id)){
                    $this->listanother = $event->GetAllEventAnother($id);
                }

                if($news->SelectTopNewsRelease(4)){
                    $listnewsnew = $news->SelectTopNewsRelease(4);

                    $this->listnewsnew = $listnewsnew;
                }

                if($comment->NumCommentByPostId($id,'event')){
                    $this->comment = $comment->NumCommentByPostId($id,'event');
                }

                if($comment->GetCommentByPostId($id,'event')){
                    $this->listcomment = $comment->GetCommentByPostId($id,'event');
                }

                $this->eventinfo = $result;
            }
        }

        $this->layout('layout');
        $this->Render('event/detail');
        return true;
    }

    function artist($path){
        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $partner = new model_partner();

        $library = new model_library();
        
        $video = new model_video();

        $urlcheck = str_replace('_', '-', $path);

        $this->link_lang = $urlcheck.'.html'; 

        if($allsetting){
            $this->title = $allsetting[0]['config_title'];
            $this->description = $allsetting[0]['config_description'];
            $this->keyword = $allsetting[0]['config_keyword'];
            $this->logo = $allsetting[0]['config_logo'];
            $this->favicon = $allsetting[0]['config_icon'];
            $this->google = $allsetting[0]['config_google'];
            $this->facebook = $allsetting[0]['config_facebook'];
            $this->twitter = $allsetting[0]['config_twitter'];
            $this->linkedin = $allsetting[0]['config_linkedin'];
            $this->configurl = $allsetting[0]['config_url'];
        }

        $artist = new model_artist();

        $event = new model_event();

        if($path){
            $part = explode('_',$path);
            $id = $part[count($part)-1];
            $result = $artist->GetArtistById($id);

            if($result){
                $this->title = $result[0]['artist_name'].'-'.$allsetting[0]['config_title'];
                $this->description = $result[0]['artist_name'].'-'.$allsetting[0]['config_description'];
                $this->keyword = $result[0]['artist_name'].','.$allsetting[0]['config_keyword'];

                $this->artistinfo = $result;

                if($event->GetAllEvent()){
                    $this->listevent = $event->GetAllEvent();
                }
            }

        }else{
            header('Location:'.URL.'artist');
        }

        $this->layout('layout');
        $this->Render('artist/detail');
        return true;
    }

    function product($path){
        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        if($allsetting){
            $this->logo = $allsetting[0]['config_logo'];
            $this->favicon = $allsetting[0]['config_icon'];
            $this->google = $allsetting[0]['config_google'];
            $this->facebook = $allsetting[0]['config_facebook'];
            $this->twitter = $allsetting[0]['config_twitter'];
            $this->linkedin = $allsetting[0]['config_linkedin'];
            $this->configurl = $allsetting[0]['config_url'];
        }

        $urlcheck = str_replace('_', '-', $path);

        $this->link_lang = $urlcheck.'.html'; 

        $this->lang = $_COOKIE['lang_curr'];

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOnByPostion('all','ngang')){
            $allmenufather = $model1->SelectAllMenuFatherOnByPostion('all','ngang');

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->allmenu = $allmenufather;
        }

        $model2 = new model_slider();

        if($model2->SelectAllSliderStatus()){

            $allslider = $model2->SelectAllSliderStatus();

            $this->allslider = $allslider;

        }

        $model3 = new model_product();

        if($model->GetAllContact()){

            $listcontact = $model->GetAllContact();

            $this->listcontact = $listcontact;

        }

        if($model3->GetAllCategoriesProduct()){
            $this->listcategoriesproduct = $model3->GetAllCategoriesProduct();
        }

        $model4 = new model_news();

        if($model4->SelectAllCategoriesNews()){
            $this->listcategoriesnews = $model4->SelectAllCategoriesNews();
        }

        if($model4->SelectTopNews(5)){
            $this->topnew = $model4->SelectTopNews(5);
        }

        if($path){
            $part = explode('-',$path);
            $id = $part[count($part)-1];
            $result = $model3->GetProductById($id);

            if($result){
                $this->productinfo = $result;

                $catproduct = $model3->GetCategoriesProductById($result[0]['categories_id']);
                $labelinfo = $model3->GetLabelProductById($id);

                $this->title = $result[0]['product_name'].'-'.$allsetting[0]['config_title'];
                $this->description = $result[0]['product_name'].'-'.$allsetting[0]['config_description'];
                $this->keyword = $result[0]['product_name'].','.$allsetting[0]['config_keyword'];

                $this->labelinfo = $labelinfo;

                $this->breadcrumb = '
                    <li>
                        <a class="home" href="'.URL.'">Trang chủ</a>
                    </li>
                    <li>
                        <a href="'.URL.'category/product/'.$catproduct[0]['categories_url'].'">'.$catproduct[0]['categories_name'].'</a>
                    </li>
                    <li>'.
                        $result[0]['product_name'].
                    '</li>
                ';

                if($catproduct){
                    $this->categories = $catproduct;
                }

                if($model3->GetAllImagesProductByImageId($id)){
                    $this->listimgproduct = $model3->GetAllImagesProductByImageId($id);
                }

                if($model3->RelatedProduct(4,$result[0]['categories_id'],$id)){
                    $listrelatedproduct = $model3->RelatedProduct(4,$result[0]['categories_id'],$id);
                    foreach ($listrelatedproduct as $key=>$value) {
                        if($model3->GetLabelProductById($value['product_id'])){
                            $label = $model3->GetLabelProductById($value['product_id']);
                            $listrelatedproduct[$key]['labelinfo'] = $label[0]['label_name'];
                        }
                    }
                    $this->listrelatedproduct = $listrelatedproduct;
                }
            }
        }else{
            header('Location:'.URL);
        }

        $this->css = array(
            URL.'public/css/colorbox.css'
        );
        
        $this->js = array(
            URL.'public/js/jquery.colorbox.js',
            URL.'public/js/detail.js',
            URL.'public/js/add-cart.js'
        );

        $this->layout('layout');
        $this->Render('product/detail');
        return true;
    }

    public function page($path){
        $page = new model_page();

        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $partner = new model_partner();

        $library = new model_library();
        
        $video = new model_video();

        $urlcheck = str_replace('_', '-', $path);

        $this->link_lang = $urlcheck.'.html'; 

        $this->lang = $_COOKIE['lang_curr'];

        if($allsetting){
            $this->title = $allsetting[0]['config_title'];
            $this->description = $allsetting[0]['config_description'];
            $this->keyword = $allsetting[0]['config_keyword'];
            $this->logo = $allsetting[0]['config_logo'];
            $this->favicon = $allsetting[0]['config_icon'];
            $this->google = $allsetting[0]['config_google'];
            $this->facebook = $allsetting[0]['config_facebook'];
            $this->twitter = $allsetting[0]['config_twitter'];
            $this->linkedin = $allsetting[0]['config_linkedin'];
            $this->configurl = $allsetting[0]['config_url'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOn()){
            $allmenufather = $model1->SelectAllMenuFatherOn();

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->allmenu = $allmenufather;
        }

        if($path){
            $part = explode('_',$path);
            $id = $part[count($part)-1];

            if($page->GetPageById($id)){

                $pageinfo = $page->GetPageById($id);

                $this->pageinfo = $pageinfo;

                    $this->title = $pageinfo[0]['page_name'].'-'.$allsetting[0]['config_title'];
                    $this->description = $pageinfo[0]['page_name'].'-'.$allsetting[0]['config_description'];
                    $this->keyword = $pageinfo[0]['page_name'].','.$allsetting[0]['config_keyword'];

                $useragent=$_SERVER['HTTP_USER_AGENT'];
        
                $this->layout('layout');
                $this->Render('page/detail');
            }else{
                header('Location:'.URL);
            }
        }else{
            header('Location:'.URL);
        }
    }

    public function newscategory($path = null,$page = 1){

        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $partner = new model_partner();

        $library = new model_library();
        
        $video = new model_video();

        $urlcheck = str_replace('_', '-', $path);

        $this->link_lang = $urlcheck.'.html'; 

        $this->lang = $_COOKIE['lang_curr'];

        if($allsetting){
            $this->title = $allsetting[0]['config_title'];
            $this->description = $allsetting[0]['config_description'];
            $this->keyword = $allsetting[0]['config_keyword'];
            $this->logo = $allsetting[0]['config_logo'];
            $this->favicon = $allsetting[0]['config_icon'];
            $this->google = $allsetting[0]['config_google'];
            $this->facebook = $allsetting[0]['config_facebook'];
            $this->twitter = $allsetting[0]['config_twitter'];
            $this->linkedin = $allsetting[0]['config_linkedin'];
            $this->configurl = $allsetting[0]['config_url'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOn()){
            $allmenufather = $model1->SelectAllMenuFatherOn();

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->allmenu = $allmenufather;
        }

        $model4 = new model_news();
        
        if($path){
            $part = explode('_',$path);
            if(count($part) > 1){
                $id = $part[count($part)-1];
                $result = $model4->GetCategoriesById($id);

                if($result){
                    $this->categoriesinfo = $result;

                        $this->title = $result[0]['news_categories_name'].'-'.$allsetting[0]['config_title'];
                        $this->description = $result[0]['news_categories_name'].'-'.$allsetting[0]['config_description'];
                        $this->keyword = $result[0]['news_categories_name'].','.$allsetting[0]['config_keyword'];
                    
                    if($page == 1){
                        $iteminpage = 5;
                    }

                    if($page > 1){
                        $iteminpage = 5;
                    }

                    $pagination = new lib_pagination();
                    $pagination->SetCurrentPage($page);
                    $pagination->SetRowInPage($iteminpage);
                    $pagination->SetUrl(URL . 'category/news/'.$path.'/@page');
                    $pagination->SetRange(4);
                    $pagination->SetLanguage(array(
                        'first'     => '<<',
                        'previous'  => '<',
                        'next'      => '>',
                        'last'      => '>>'
                    ));
                    $paginator  = $pagination->BuildPagination($this->GetBlogDB1($id));

                    $this->paginator  = $paginator['paginator'];

                    $listnew   = $model4->PaginationNews($page,$iteminpage,$id);

                    if($news->SelectAllCategoriesNews()){
                        $this->listnewscategories = $news->SelectAllCategoriesNews();
                    }

                    if($listnew){
                        foreach ($listnew as $key => $value) {
                            if($news->GetCategoriesNewsById($value['news_categories_id'])){
                                $newsinfo = $news->GetCategoriesNewsById($value['news_categories_id']);
                                $listnew[$key]['category'] = $newsinfo[0];
                            }
                            if($comment->NumCommentByPostId($value['news_categories_id'])){
                                $commentinfo = $comment->NumCommentByPostId($value['news_categories_id']);
                                $listnew[$key]['comment'] = $commentinfo[0]['numcomment'];
                            }
                        }

                        $this->listnew = $listnew;
                    }

                    if($news->SelectTopNewsRelease(4)){
                        $this->topnews = $news->SelectTopNewsRelease(4);
                    }
                }
            }else{
                
                $page = $path;
                
                $iteminpage = 5;
                
                $pagination = new lib_pagination();
                $pagination->SetCurrentPage($page);
                $pagination->SetRowInPage($iteminpage);
                $pagination->SetUrl(URL . 'category/news/@page');
                $pagination->SetRange(4);
                $pagination->SetLanguage(array(
                    'first'     => '<<',
                    'previous'  => '<',
                    'next'      => '>',
                    'last'      => '>>'
                ));
                $paginator  = $pagination->BuildPagination($this->GetBlogDB1(0));

                $this->paginator  = $paginator['paginator'];

                $listnew   = $model4->PaginationNews($page,$iteminpage,0);

                if($news->SelectAllCategoriesNews()){
                    $this->listnewscategories = $news->SelectAllCategoriesNews();
                }
                
                foreach ($listnew as $key => $value) {
                    if($news->GetCategoriesNewsById($value['news_categories_id'])){
                        $newsinfo = $news->GetCategoriesNewsById($value['news_categories_id']);
                        $listnew[$key]['category'] = $newsinfo[0];
                    }
                    if($comment->NumCommentByPostId($value['news_categories_id'])){
                        $commentinfo = $comment->NumCommentByPostId($value['news_categories_id']);
                        $listnew[$key]['comment'] = $commentinfo[0]['numcomment'];
                    }
                }

                $this->listnew = $listnew;

                if($news->SelectTopNewsRelease(4)){
                        $this->topnews = $news->SelectTopNewsRelease(4);
                    }
            }
        }else{
            if($page == 1){
                $iteminpage = 5;
            }

            if($page > 1){
                $iteminpage = 5;
            }

            $pagination = new lib_pagination();
            $pagination->SetCurrentPage($page);
            $pagination->SetRowInPage($iteminpage);
            $pagination->SetUrl(URL . 'category/news/@page');
            $pagination->SetRange(4);
            $pagination->SetLanguage(array(
                'first'     => '<<',
                'previous'  => '<',
                'next'      => '>',
                'last'      => '>>'
            ));
            $paginator  = $pagination->BuildPagination($this->GetBlogDB1(0));

            $this->paginator  = $paginator['paginator'];

            $listnew   = $model4->PaginationNews($page,$iteminpage,0);

            if($news->SelectAllCategoriesNews()){
                $this->listnewscategories = $news->SelectAllCategoriesNews();
            }
            
            foreach ($listnew as $key => $value) {
                if($news->GetCategoriesNewsById($value['news_categories_id'])){
                    $newsinfo = $news->GetCategoriesNewsById($value['news_categories_id']);
                    $listnew[$key]['category'] = $newsinfo[0];
                }
                if($comment->NumCommentByPostId($value['news_categories_id'])){
                    $commentinfo = $comment->NumCommentByPostId($value['news_categories_id']);
                    $listnew[$key]['comment'] = $commentinfo[0]['numcomment'];
                }
            }

            $this->listnew = $listnew;

            if($news->SelectTopNewsRelease(4)){
                        $this->topnews = $news->SelectTopNewsRelease(4);
                    }
        }

        $this->numpage = $page;

        $useragent=$_SERVER['HTTP_USER_AGENT'];

        $this->layout('layout');
        $this->Render('category/news');
        return true;
    }

    private function GetBlogDB1($catid) {
        $db = new lib_db();

        if($catid != 0){
            return $db->query('select *
                from tbl_news
                where news_categories_id ='.$catid.' order by news_create_date DESC');
        }else{
            return $db->query('select *
                from tbl_news order by news_create_date DESC');
        }
    }

    public function productcategory($path = null,$page = 1){
        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $advertise = new model_advertise();

        $news = new model_news();

        $linker = new model_linker();

        $comment = new model_comment();

        $question = new model_question();

        $model3 = new model_product();

        $urlcheck = str_replace('_', '-', $path);

        $this->link_lang = $urlcheck.'.html'; 

        $this->lang = $_COOKIE['lang_curr'];

        if($allsetting){
            $this->title = $allsetting[0]['config_title'];
            $this->description = $allsetting[0]['config_description'];
            $this->keyword = $allsetting[0]['config_keyword'];
            $this->logo = $allsetting[0]['config_logo'];
            $this->favicon = $allsetting[0]['config_icon'];
            $this->google = $allsetting[0]['config_google'];
            $this->facebook = $allsetting[0]['config_facebook'];
            $this->twitter = $allsetting[0]['config_twitter'];
            $this->linkedin = $allsetting[0]['config_linkedin'];
            $this->configurl = $allsetting[0]['config_url'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOn()){
            $allmenufather = $model1->SelectAllMenuFatherOn();

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->allmenu = $allmenufather;
        }

        if($model1->SelectAllMenuFatherOnByPostion('product','doc')){
            $allmenufatherproduct = $model1->SelectAllMenuFatherOnByPostion('product','doc');

            foreach($allmenufatherproduct as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufatherproduct[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->allmenuproduct = $allmenufatherproduct;
        }

        if($advertise->GetAdvertiseByPosition(1)){
            $this->listadvertise1 = $advertise->GetAdvertiseByPosition(1);
        }

        if($advertise->GetAdvertiseByPosition(2)){
            $this->listadvertise2 = $advertise->GetAdvertiseByPosition(2);
        }

        if($advertise->GetAdvertiseByPosition(3)){
            $this->listadvertise3 = $advertise->GetAdvertiseByPosition(3);
        }

        if($news->SelectTopNewsView(4)){
            $this->listnewsview = $news->SelectTopNewsView(4);
        }

        if($news->SelectTopNewsRelease(4)){
            $this->listnewsrelease = $news->SelectTopNewsRelease(4);
        }

        if($linker->SelectAlllinker()){
            $this->listlinker = $linker->SelectAlllinker();
        }

        if($news->SelectTopNewsRelease(5)){
            $this->listnewsnew = $news->SelectTopNewsRelease(5);
        }

        $model4 = new model_news();

        if($model4->SelectAllCategoriesNews()){
            $this->listcategoriesnews = $model4->SelectAllCategoriesNews();
        }

        if($model4->SelectTopNews(5)){
            $this->topnew = $model4->SelectTopNews(5);
        }

        if($path){
            $part = explode('_',$path);
            $id = $part[count($part)-1];
            $result = $model3->GetCategoriesProductById($id);

            if($result){
                $this->categoriesinfo = $result;

                $this->title = $result[0]['categories_name'].'-'.$allsetting[0]['config_title'];
                $this->description = $result[0]['categories_name'].'-'.$allsetting[0]['config_description'];
                $this->keyword = $result[0]['categories_name'].','.$allsetting[0]['config_keyword'];

                $iteminpage = 9;

                $pagination = new lib_pagination();
                $pagination->SetCurrentPage($page);
                $pagination->SetRowInPage($iteminpage);
                $pagination->SetUrl(URL . 'category/product/'.$path.'/@page');
                $pagination->SetRange(4);
                $pagination->SetLanguage(array(
                    'first'     => '<<',
                    'previous'  => '<',
                    'next'      => '>',
                    'last'      => '>>'
                ));
                $paginator  = $pagination->BuildPagination($this->GetBlogDB($id));

                $this->paginator  = $paginator['paginator'];

                $this->listnewsproduct   = $model3->PaginationProductByCatId($page,$iteminpage,$id);
            }
        }

        $this->layout('layout');
        $this->Render('category/product');
        return true;
    }

    private function GetBlogDB($catid) {
        $db = new lib_db();
        return $db->query('select pr.*,cat.*
            from tbl_product as pr
            join tbl_categories as cat on cat.categories_id = pr.categories_id
            where pr.categories_id ='.$catid.' order by pr.product_id DESC');
    }

    public function file($path = null){
        $file = new model_file();

        $model = new model_setting();

        $allsetting = $model->GetAllSetting();

        $urlcheck = str_replace('_', '-', $path);

        $this->link_lang = $urlcheck.'.html'; 

        $this->lang = $_COOKIE['lang_curr'];

        if($allsetting){
            $this->title = $allsetting[0]['config_title'];
            $this->description = $allsetting[0]['config_description'];
            $this->keyword = $allsetting[0]['config_keyword'];
            $this->logo = $allsetting[0]['config_logo'];
            $this->favicon = $allsetting[0]['config_icon'];
            $this->google = $allsetting[0]['config_google'];
            $this->facebook = $allsetting[0]['config_facebook'];
            $this->twitter = $allsetting[0]['config_twitter'];
            $this->linkedin = $allsetting[0]['config_linkedin'];
            $this->configurl = $allsetting[0]['config_url'];
        }

        $model1 = new model_menu();
        if($model1->SelectAllMenuFatherOn()){
            $allmenufather = $model1->SelectAllMenuFatherOn();

            foreach($allmenufather as $key=>$menu){
                if($model1->SelectAllChildeLv1ByFatherId($menu['menu_id'])){
                    $allmenufather[$key]['menu_child'] =  $model1->SelectAllChildeLv1ByFatherId($menu['menu_id']);
                }
            }

            $this->allmenu = $allmenufather;
        }

        if($file->GetAllFileCategoriesFather()){
            $listcategoriesfather = $file->GetAllFileCategoriesFather();

            $this->listcategoriesfather = $listcategoriesfather;
        }

        if($path){
            $part = explode('_',$path);
            $id = $part[count($part)-1];
            $result = $file->GetFileCategoriesById($id);

            if($result){
                $listallcategories = $file->GetAllFileCategories1();

                foreach ($result as $key => $value) {
                    if($file->GetAllFileCategoriesChildByFatherId1($value['file_categories_id'])){
                        $child = $file->GetAllFileCategoriesChildByFatherId1($value['file_categories_id']);
                        $filechild = $file->GetAllFileByCategoriesId($value['file_categories_id']);

                        foreach ($child as $key1 => $value1) {
                            $child1 = array();
                            $filechild1 = $file->GetAllFileByCategoriesId($value1['file_categories_id']);

                            foreach ($listallcategories as $value2) {
                                if(strpos($value2['file_categories_father'], $value1['file_categories_id'])){
                                    $child1[] = $value2;
                                }

                                if($value2['file_categories_father'] == $value1['file_categories_id']){
                                    $child1[] = $value2;
                                }
                            }

                            foreach ($child1 as $key2 => $value2) {
                                $child2 = array();
                                $filechild2 = $file->GetAllFileByCategoriesId($value2['file_categories_id']);

                                foreach ($listallcategories as $value3) {
                                    if(strpos($value3['file_categories_father'], $value2['file_categories_id'])){
                                        $child2[] = $value3;
                                    }

                                    if($value3['file_categories_father'] == $value2['file_categories_id']){
                                        $child2[] = $value3;
                                    }
                                }

                                foreach ($child2 as $key3 => $value3) {
                                    $filechild3 = $file->GetAllFileByCategoriesId($value3['file_categories_id']);

                                    $child2[$key3]['childfile'] = $filechild3;
                                }


                                $child1[$key2]['child'] = $child2;
                                $child1[$key2]['childfile'] = $filechild2;
                            }

                            $child[$key1]['child'] = $child1;
                            $child[$key1]['childfile'] = $filechild1;
                        }
                    }

                    $result[$key]['child'] = $child;
                    $result[$key]['childfile'] = $filechild;
                }

                $this->filecategoriseinfo = $result;

                if($this->lang == 'vi'){
                    $this->title = $result[0]['file_categories_name'].'-'.$allsetting[0]['config_title'];
                    $this->description = $result[0]['file_categories_name'].'-'.$allsetting[0]['config_description'];
                    $this->keyword = $result[0]['file_categories_name'].','.$allsetting[0]['config_keyword'];
                }else{
                    $this->title = $result[0]['file_categories_name_english'].'-'.'Institute for Computational Science and Technology';
                    $this->description = $result[0]['file_categories_name_english'].'-'.$allsetting[0]['config_description'];
                    $this->keyword = $result[0]['file_categories_name_english'].','.$allsetting[0]['config_keyword'];
                }
                
                $listfile = array();
            
                if($file->GetAllFileByCategoriesId($result[0]['file_categories_id'])){
                    $files = $file->GetAllFileByCategoriesId($result[0]['file_categories_id']);
                    foreach($files as $fl){
                        $listfile[] = $fl;
                    }
                }
                
                if($file->GetAllFileCategoriesChildByFatherId($result[0]['file_categories_id'])){
                    $child = $file->GetAllFileCategoriesChildByFatherId($result[0]['file_categories_id']);
                    foreach($child as $ch){
                        if($file->GetAllFileByCategoriesId($ch['file_categories_id'])){
                            $files1 = $file->GetAllFileByCategoriesId($ch['file_categories_id']);
                            foreach($files1 as $fl1){
                                $listfile[] = $fl1;
                            }
                        }
                        if($file->GetAllFileCategoriesChildByFatherId($ch['file_categories_id'])){
                            $child1 = $file->GetAllFileCategoriesChildByFatherId($ch['file_categories_id']);
                            foreach($child1 as $ch1){
                                if($file->GetAllFileByCategoriesId($ch1['file_categories_id'])){
                                    $files2 = $file->GetAllFileByCategoriesId($ch1['file_categories_id']);
                                    foreach($files2 as $fl2){
                                        $listfile[] = $fl2;
                                    }
                                }
                            }
                        }
                    }
                }
                
                $this->listfile = $listfile;
            }
        }else{
            $result = $file->GetFirstFileCategoriesFather();

            $this->title = $result[0]['file_categories_name'].'-'.$allsetting[0]['config_title'];
            $this->description = $result[0]['file_categories_name'].'-'.$allsetting[0]['config_description'];
            $this->keyword = $result[0]['file_categories_name'].','.$allsetting[0]['config_keyword'];

            $this->filecategoriseinfo = $result;

            $listfile = array();
            
                if($file->GetAllFileByCategoriesId($result[0]['file_categories_id'])){
                    $files = $file->GetAllFileByCategoriesId($result[0]['file_categories_id']);
                    foreach($files as $fl){
                        $listfile[] = $fl;
                    }
                }
                
                if($file->GetAllFileCategoriesChildByFatherId($result[0]['file_categories_id'])){
                    $child = $file->GetAllFileCategoriesChildByFatherId($result[0]['file_categories_id']);
                    foreach($child as $ch){
                        if($file->GetAllFileByCategoriesId($ch['file_categories_id'])){
                            $files1 = $file->GetAllFileByCategoriesId($ch['file_categories_id']);
                            foreach($files1 as $fl1){
                                $listfile[] = $fl1;
                            }
                        }
                        if($file->GetAllFileCategoriesChildByFatherId($ch['file_categories_id'])){
                            $child1 = $file->GetAllFileCategoriesChildByFatherId($ch['file_categories_id']);
                            foreach($child1 as $ch1){
                                if($file->GetAllFileByCategoriesId($ch1['file_categories_id'])){
                                    $files2 = $file->GetAllFileByCategoriesId($ch1['file_categories_id']);
                                    foreach($files2 as $fl2){
                                        $listfile[] = $fl2;
                                    }
                                }
                            }
                        }
                    }
                }
                
                $this->listfile = $listfile;
        }

        $useragent=$_SERVER['HTTP_USER_AGENT'];

            $this->layout('layout');
            $this->Render('category/file');
            return true;
    }
}

?>
