<?php
    class lib_transfer {
        public function UploadImageContent($file, $destination)
        {
            $file_name = uniqid() . "_" . $file['name'];
            if(move_uploaded_file($file['tmp_name'], $destination.$file_name)) {
                return $file_name;
            }
            return false;
        }
        
        public function CheckExtensionImage($file)
        {
            $extensions = array(
                'jpg', 'png', 'gif', 'jpeg'
            );
            
            $types      = array(
                "image/gif", 
                "image/jpeg", 
                "image/jpg", 
                "image/pjpeg", 
                "image/x-png", 
                "image/png"
            );
            
            $extension  = end(explode('.', $file['name']));
            $type       = $file['type'];
            if(in_array($extension, $extensions) && in_array($type, $types))
            {
                return true;
            }
            return false;
        }

        public function ReArrayFiles($file_post) {
            //print_r($file_post);
            $name   = current(array_keys($file_post));
            $count  = count(current($file_post[$name]));
            $files  = array();
            for($i=0; $i<$count; $i++)
            {
                foreach($file_post[$name] as $key => $value)
                {
                    $files[$i][$key] = $value[$i];
                }
            }
            
            return array(
                $name => $files
            );
        }
        
        public function DeleteFile($file_name, $destination) {
            if(gettype($file_name) == 'array') {
                if(count($file_name) != 0) {
                    foreach($file_name as $file) {
                        if(!file_exists($destination . $file)) {
                            continue;
                        }
                        if(!unlink($destination . $file)) {
                            return false;
                        }
                    }
                    return true;
                }
                return true;
            }
            if(!file_exists($destination . $file_name)) {
                return true;
            }
            
            if(unlink($destination . $file_name)) {
                return true;
            }
            return false;
        }
    }
?>