<?php
    class lib_pagination
    {
        private $currentPage,
                $rowInPage,
                $url,
                $range,
                $total_row;
        
        private $language = array(
            'first'     => 'First',
            'previous'  => 'Previous',
            'next'      => 'Next',
            'last'      => 'Last'
        );
        
        public function __construct() {
            $this->currentPage  = null;
            $this->rowInPage    = null;
            $this->url          = null;
            $this->range        = 0;
        }
        
        public function SetCurrentPage($currentPage)
        {
            $this->currentPage = $currentPage;
        }
        
        public function SetRowInPage($rowInPage)
        {
            $this->rowInPage = $rowInPage;
        }
        
        public function SetUrl($url)
        {
            $this->url = $url;
        }
        
        public function SetRange($range)
        {
            $this->range = $range;
        }
        
        public function SetLanguage($language) {
            $this->language = $language;
        }
        
        public function BuildPagination(lib_db $db)
        {
            $offset = $this->currentPage*$this->rowInPage - $this->rowInPage;
            
            $tmp_db             = new lib_db();
            $tmp_db->copy($db);
            $this->total_row    = count($tmp_db->execute());
            
            $data               = $db->limit($this->rowInPage, $offset)->execute();
            $paginator          = $this->BuildLink();
            return array(
                'data'      =>  $data,
                'paginator' => $paginator
            );
        }
        
        private function BuildLink()
        {
            $total_page = ceil($this->total_row/$this->rowInPage);
            $paginator  = array();
            $paginator['first']     = "<li><a href='" . str_replace('@page', '1', $this->url) ."1'>First</a></li>";
            $paginator['previous']  = "<li><a href='" . str_replace('@page', ($this->currentPage-1), $this->url) . "'>Previous</a></li>";
            if($this->currentPage == 1)
            {
                $paginator['first']     = "<li class='disabled'><a>" . $this->language['first'] . "</a></li>";
                $paginator['previous']  = "<li class='disabled'><a>" . $this->language['previous'] . "</a></li>";
            }
            
            $paginator['last']      = "<li><a href='" . str_replace('@page', $total_page, $this->url) . "'>" . $this->language['last'] . "</a></li>";
            $paginator['next']      = "<li><a href='" . str_replace('@page', ($this->currentPage+1), $this->url) . "'>" . $this->language['next'] . "</a></li>";
            if($this->currentPage == $total_page)
            {
                $paginator['last']      = "<li  class='disabled'><a>" . $this->language['last'] . "</a></li>";
                $paginator['next']      = "<li  class='disabled'><a>" . $this->language['next'] . "</a></li>";
            }
                
            $paginator['items']     = array();
            
            if($this->range != 0 && $this->range*2+1 < $total_page)
            {
                $start  = $this->currentPage - $this->range;
                $end    = $this->currentPage + $this->range;
                $show   = $this->range*2+1;
                
                if($start <= 1)
                {
                    for($i=1; $i<=$show; $i++)
                    {
                        if($this->currentPage == $i)
                        {
                            $paginator['items'][] = "<li class='active'><a>".$i."</a></li>";
                            continue;
                        }
                        $paginator['items'][] = "<li><a href='" . str_replace('@page', $i, $this->url) . "'>".$i."</a></li>";
                    }
                    $paginator['items'][] = "<li><a>......</a></li>";
                }
                elseif($end >= $total_page)
                {
                    $paginator['items'][] = "<li><a>......</a></li>";
                    for($i=$total_page-$show+1; $i<=$total_page; $i++)
                    {
                        if($this->currentPage == $i)
                        {
                            $paginator['items'][] = "<li class='active'><a>".$i."</a></li>";
                            continue;
                        }
                        $paginator['items'][] = "<li><a href='" . str_replace('@page', $i, $this->url) . "'>".$i."</a></li>";
                    }
                }
                else
                {
                    $paginator['items'][] = "<li><a>......</a></li>";
                    for($i=$start; $i<=$end; $i++)
                    {
                        if($this->currentPage == $i)
                        {
                            $paginator['items'][] = "<li class='active'><a>".$i."</a></li>";
                            continue;
                        }
                        $paginator['items'][] = "<li><a href='" . str_replace('@page', $i, $this->url) . "'>".$i."</a></li>";
                    }
                    $paginator['items'][] = "<li><a>......</a></li>";
                }
            }
            else{
                for($i=1; $i<=$total_page; $i++)
                {
                    if($this->currentPage == $i)
                    {
                        $paginator['items'][] = "<li class='active'><a>".$i."</a></li>";
                        continue;
                    }
                    $paginator['items'][] = "<li><a href='" . str_replace('@page', $i, $this->url) . "'>".$i."</a></li>";
                }
            }
            
            return $paginator;
        }
    }
?>