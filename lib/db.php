<?php

class lib_db extends PDO {

    protected $fetch_mode = PDO::FETCH_ASSOC;
    private $table_name,
            $where,
            $order,
            $group,
            $set,
            $limit,
            $offset,
            $query,
            $type,
            $where_operator,
            $join = array(),
            $colums = array(),
            $values = array();

    public function __construct() {
        parent::__construct(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
        $this->exec('SET CHARACTER SET utf8');

        $this->table_name = NULL;
        $this->where = NULL;
        $this->order = NULL;
        $this->group = NULL;
        $this->value = NULL;
        $this->set = NULL;
        $this->offset = NULL;
        $this->where_operator = "AND";
        $this->limit = 0;

        $this->type = array(
            'select' => 'SELECT BuildColumns FROM BuildTableName BuildJoin BuildWhere BuildGroup BuildOrder BuildLimit',
            'insert' => 'INSERT INTO BuildTableName BuildValues',
            'update' => 'UPDATE BuildTableName BuildSet BuildWhere',
            'delete' => 'DELETE FROM BuildTableName BuildWhere BuildLimit'
        );

        //parent::setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTIONS);
    }

    public function select($table_name) {
        $this->query = $this->type['select'];
        $this->table_name = $table_name;
        return $this;
    }

    public function columns($columns) {
        $this->colums = $columns;
        return $this;
    }

    public function where($where, $where_opeator = "AND") {
        $this->where = $where;
        $this->where_operator = $where_opeator;
        return $this;
    }

    public function limit($limit = 0, $offset = null) {
        $this->limit = $limit;
        $this->offset = $offset;
        return $this;
    }

    public function order($order) {
        $this->order = $order;
        return $this;
    }

    public function group($column) {
        $this->group = $column;
        return $this;
    }

    public function insert($table_name) {
        $this->query = $this->type['insert'];
        $this->table_name = $table_name;
        return $this;
    }

    public function values($data = array()) {
        $this->values = $data;
        return $this;
    }

    public function update($table_name) {
        $this->query = $this->type['update'];
        $this->table_name = $table_name;
        return $this;
    }

    public function set($data) {
        $this->set = $data;
        return $this;
    }

    public function delete($table_name) {
        $this->query = $this->type['delete'];
        $this->table_name = $table_name;
        return $this;
    }

    public function join($table = array(), $on = "") {
        if (count($table) != 0 && $on != "") {
            array_push($this->join, array_merge_recursive($table, array('on' => $on)));
            return $this;
        }
        return $this;
    }

    public function query($query) {
        $this->query = $query;
        return $this;
    }

    public function execute($show_controller = false) {
        $statement = $this->prepare($this->Build());

        if ($show_controller) {
            print_r($statement);
            exit();
        }

        $result = $statement->execute();

        if (strtoupper(current(explode(" ", $this->query))) == "DELETE") {
            return $result;
        }

        if ($statement->rowCount() > 0) {
            
            if (strtoupper(current(explode(" ", trim(preg_replace('/\s+/', ' ', $this->query))))) == "SELECT") {
                return $statement->fetchAll($this->fetch_mode);
            }
            if (strtoupper(current(explode(" ", trim(preg_replace('/\s+/', ' ', $this->query))))) == "INSERT") {
                return $this->lastInsertId();
            }
            if(strtoupper(current(explode(" ", $this->query))) == "SHOW")
            {
                return $statement->fetchAll($this->fetch_mode);
            }
            return true;
        }
        return false;
    }

    public function copy(lib_db $db) {
        $this->table_name = NULL;
        $this->where = NULL;
        $this->order = NULL;
        $this->group = NULL;
        $this->value = NULL;
        $this->set = NULL;
        $this->offset = NULL;
        $this->where_operator = "AND";
        $this->limit = 0;
        unset($this->join);
        unset($this->colums);
        unset($this->values);

        $this->table_name = $db->GetTableName();
        $this->where = $db->GetWhere();
        $this->order = $db->GetOrder();
        $this->group = $db->GetGroup();
        $this->set = $db->GetGroup();
        $this->limit = $db->GetLimit();
        $this->offset = $db->GetOffset();
        $this->query = $db->GetQuery();
        $this->where_operator = $db->GetWhereOperator();
        $this->join = $db->GetJoin();
        $this->colums = $db->GetColumns();
        $this->values = $db->values;
    }

    /**
     * select
     * @param string $sql An SQL string
     * @param array $array Paramters to bind
     * @param constant $fetchMode A PDO Fetch mode
     * @return mixed
     */
    public function SelectQuery($sql, $array = array()) {
        $sth = $this->prepare($sql);
        foreach ($array as $key => $value) {
            $sth->bindValue("$key", $value);
        }
        $sth->execute();
        if ($sth->rowCount() > 0) {
            return $sth->fetchAll($this->fetch_mode);
        }
        return false;
    }

    /**
     * insert
     * @param string $table A name of table to insert into
     * @param string $data An associative array
     */
    public function InsertQuery($table, $data) {
        ksort($data);

        $fieldNames = implode('`, `', array_keys($data));
        $fieldValues = ':' . implode(', :', array_keys($data));

        $sth = $this->prepare("INSERT INTO $table (`$fieldNames`) VALUES ($fieldValues)");

        foreach ($data as $key => $value) {
            $sth->bindValue(":$key", $value);
        }
        $sth->execute();
        if ($sth->rowCount() > 0)
            return true;
        return false;
    }

    /**
     * update
     * @param string $table A name of table to insert into
     * @param string $data An associative array
     * @param string $where the WHERE query part
     */
    public function UpdateQuery($table, $data, $where) {
        ksort($data);

        $fieldDetails = NULL;
        foreach ($data as $key => $value) {
            $fieldDetails .= "`$key`=:$key,";
        }
        $fieldDetails = rtrim($fieldDetails, ',');

        $sth = $this->prepare("UPDATE $table SET $fieldDetails WHERE $where");

        foreach ($data as $key => $value) {
            $sth->bindValue(":$key", $value);
        }

        $sth->execute();
        if ($sth->rowCount() > 0)
            return true;
        return false;
    }

    /**
     * delete
     * 
     * @param string $table
     * @param string $where
     * @param integer $limit
     * @return integer Affected Rows
     */
    public function DeleteQuery($table, $where, $limit = 1) {
        return $this->exec("DELETE FROM $table WHERE $where LIMIT $limit");
    }

    private function BuildTableName() {
        if ($this->table_name == NULL) {
            return '';
        }

        if (gettype($this->table_name) == "string") {
            return $this->table_name;
        }

        $string = "";
        foreach ($this->table_name as $key => $value) {
            $string = $string . $key . " AS " . $value;
        }
        return $string;
    }

    private function BuildColumns() {
        if (count($this->colums) == 0) {
            return "*";
        }

        return implode(",", $this->colums);
    }

    private function BuildJoin() {
        if (count($this->join) == 0) {
            return '';
        }

        $string = "";
        foreach ($this->join as $item) {
            $string = $string . "JOIN ";
            foreach ($item as $key => $value) {
                if ($key != "on") {
                    $string = $string . $key . " AS " . $value . " ";
                    continue;
                }
                $string = $string . "ON " . $value . " ";
            }
        }

        return rtrim($string, " ");
    }

    private function BuildWhere() {
        if ($this->where == NULL) {
            return '';
        }

        $string = "WHERE ";

        if (gettype($this->where) == 'string') {
            return $string . $this->where;
        }

        foreach ($this->where as $key => $value) {
            $string = $string . str_replace("?", $value, $key)
                    . " " . $this->where_operator . " ";
        }

        return rtrim($string, " " . $this->where_operator . " ");
    }

    private function BuildOrder() {
        if ($this->order == NULL) {
            return '';
        }

        $string = "ORDER BY ";

        if (gettype($this->order) == 'string') {
            return $string . $this->order;
        }

        foreach ($this->order as $item) {
            $string = $string . $item . ", ";
        }

        return rtrim($string, ", ");
    }

    private function BuildGroup() {
        if ($this->group == NULL) {
            return '';
        }
        $string = "GROUP BY " . $this->group;
        return $string;
    }

    private function BuildLimit() {
        if ($this->limit == 0) {
            return "";
        }

        if ($this->offset == null) {
            return "LIMIT " . $this->limit;
        }

        return "LIMIT " . $this->offset . ", " . $this->limit;
    }

    private function BuildValues() {
        $string = "";
        $key_string = "";
        $value_string = "";
        foreach ($this->values as $key => $value) {
            $key_string = $key_string . $key . ",";
            $value_string = $value_string . "'" . $value . "',";
        }

        $string = "(" . rtrim($key_string, ",") . ") VALUES (" . rtrim($value_string, ",") . ")";
        return $string;
    }

    private function BuildSet() {
        if ($this->set == NULL) {
            return '';
        }

        if (gettype($this->set) == 'string') {
            return $this->set;
        }

        $string = "SET ";
        foreach ($this->set as $key => $value) {
            $string = $string . $key . "='" . $value . "', ";
        }
        return rtrim($string, ", ");
    }

    private function Build() {
        $query = str_replace(array(
            'BuildTableName',
            'BuildColumns',
            'BuildWhere',
            'BuildOrder',
            'BuildLimit',
            'BuildGroup',
            'BuildValues',
            'BuildSet',
            'BuildJoin'
                ), array(
            $this->BuildTableName(),
            $this->BuildColumns(),
            $this->BuildWhere(),
            $this->BuildOrder(),
            $this->BuildLimit(),
            $this->BuildGroup(),
            $this->BuildValues(),
            $this->BuildSet(),
            $this->BuildJoin()
                ), $this->query);
        $this->Clear();
        return $query;
    }

    private function Clear() {
        $this->table_name = NULL;
        $this->where = NULL;
        $this->order = NULL;
        $this->group = NULL;
        $this->value = NULL;
        $this->set = NULL;
        $this->offset = NULL;
        $this->where_operator = "AND";
        $this->limit = 0;

        unset($this->join);
        unset($this->colums);
        unset($this->values);
        $this->join = array();
        $this->colums = array();
        $this->values = array();
    }

    protected function GetTableName() {
        return $this->table_name;
    }

    protected function GetWhere() {
        return $this->where;
    }

    protected function GetOrder() {
        return $this->order;
    }

    protected function GetGroup() {
        return $this->group;
    }

    protected function GetSet() {
        return $this->set;
    }

    protected function GetLimit() {
        return $this->limit;
    }

    protected function GetOffset() {
        return $this->offset;
    }

    protected function GetQuery() {
        return $this->query;
    }

    protected function GetWhereOperator() {
        return $this->where_operator;
    }

    protected function GetJoin() {
        return $this->join;
    }

    protected function GetColumns() {
        return $this->colums;
    }

    protected function GetValues() {
        return $this->values;
    }

}

?>