<?php

class lib_view {
    
    public $content;
    public $layout;
    public $layoutDirectory;
    public $viewDirectory;
    public $layoutBaseDirectory;
    public $viewBaseDirectory;
    
    function __construct() {
        $this->layoutDirectory      = 'layout/';
        $this->layoutBaseDirectory  = 'layout/';
        $this->viewDirectory        = 'view/';
        $this->viewBaseDirectory    = 'view/';
        $this->layout               = 'layout';
    }
	
    function Render($name) {
        $this->content = $this->viewDirectory . $name . '.php';
        include_once $this->layoutDirectory.$this->layout.'.php';
        $this->viewDirectory    = $this->viewBaseDirectory;
        $this->layoutDirectory  = $this->layoutBaseDirectory;
        lib_messenger::DestroyMessenger();
    }
    
    
    // hàm này duoc goi trong layout de the hien noi dung
    public function content()
    {
        include_once $this->content;
    }
    
    // dùng để set layout
    public function layout($name)
    {
        $this->layout = $name;
        return $this;
    }
    
    
    // Dùng để thêm một thể hiện vào trang
    public function partial($name, $vars = array())
    {
        if(count($vars) != 0)
        {
            foreach($vars as $key => $value)
            {
                ${$key}     = $value;
            }
        }
        include_once $this->layoutDirectory.$name.'.php';
        $this->layoutDirectory = $this->layoutBaseDirectory;
    }
    
    public function setLayoutBaseDirectory($newDirectory)
    {
        $this->layoutBaseDirectory  = $newDirectory;
        $this->layoutDirectory      = $newDirectory;
        return $this;
    }

    // Dùng để định nghĩa lại thư mục chứa layout
    public function setLayoutDirectory($newDirectory)
    {
        $this->layoutDirectory = $newDirectory;
        return $this;
    }
    
    public function setViewBaseDirectory($newDirectory)
    {
        $this->viewBaseDirectory    = $newDirectory;
        $this->viewDirectory        = $newDirectory;
    }
    
    // Dùng để định nghịa lại thư mục chứ view
    public function setViewDirectory($newDirectory)
    {
        $this->viewDirectory = $newDirectory;
        return $this;
    }
    
    // Thêm srcript
    public function appendScript($path, $attributes=array())
    {
        $str = "<script ";
        if(count($attributes) > 0) {
            foreach($attributes as $key => $value) {
                $str = $str . $key . "='" . $value . "' ";
            }
        }
        $str = $str . "type='text/javascript' src='".$path."'></script>\n";
        echo $str;
        return $this;
    }
    
    // Thêm stylesheet
    public function appendStyleSheet($path)
    {
        $str = "<link rel='stylesheet' type='text/css' href='".$path."' />\n";
        echo $str;
        return $this;
    }
    
    // Thêm array Script
    public function appendArrayScript($data)
    {
        if(gettype($data) == 'array' && count($data) > 0)
        {
            foreach($data as $item)
            {
                if(gettype($item) == 'array') {
                    $this->appendScript($item[0], end($item));
                }
                else {
                    $str = "<script type='text/javascript' src='".$item."'></script>\n";
                    echo $str;
                }
            }
        }
        return $this;
    }
    
    // Thêm array style sheet
    public function appendArrayStyleSheet($data)
    {
        if(gettype($data) == 'array' && count($data) > 0)
        {
            foreach($data as $item)
            {
                $str = "<link rel='stylesheet' type='text/css' href='".$item."' />\n";
                echo $str;
            }
        }
        return $this;
    }
    
    // Thêm Favicon
    
    public function appendShortCutIcon($data)
    {
        echo "<link rel='shortcut icons' href='".$data."'>\n";
        return $this;
    }
}

?>
