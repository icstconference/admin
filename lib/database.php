<?php

class lib_database extends PDO
{
    
    public function __construct($DB_TYPE, $DB_HOST, $DB_NAME, $DB_USER, $DB_PASS)
    {
        parent::__construct($DB_TYPE.':host='.$DB_HOST.';dbname='.$DB_NAME, $DB_USER, $DB_PASS);
        
        //parent::setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTIONS);
    }
    
    /**
     * select
     * @param string $sql An SQL string
     * @param array $array Paramters to bind
     * @param constant $fetchMode A PDO Fetch mode
     * @return mixed
     */
    public function Select($sql, $array = array(), $return=true, $fetchMode = PDO::FETCH_ASSOC, $strip=true)
    {
        $sth = $this->prepare($sql);
        foreach ($array as $key => $value) {
            if($strip==true){
                $sth->bindValue("$key", strip_tags($value));
            }else{
                $sth->bindValue("$key", $value);
            }
            
        }
        /*echo '<pre>';
        print_r($sth);
        echo '</pre>';*/
        $sth->execute();
        if($sth->rowCount()>0){
            if($return ===true){
                return $sth->fetchAll($fetchMode);
            }
            return true;
        }
        return false;
    }
    
    /**
     * insert
     * @param string $table A name of table to insert into
     * @param string $data An associative array
     */
    public function Insert($table, $data, $strip = true,$strip_allow=null)
    {
        ksort($data);
        
        $fieldNames = implode('`, `', array_keys($data));
        $fieldValues = ':' . implode(', :', array_keys($data));
        
        $sth = $this->prepare("INSERT INTO $table (`$fieldNames`) VALUES ($fieldValues)");
        
        foreach ($data as $key => $value) {
            if($strip==true){
                $sth->bindValue(":$key", strip_tags($value,$strip_allow));
            }else{
                $sth->bindValue(":$key", $value);
            }
            
        }
        $sth->execute();
        if($sth->rowCount()>0)
            return true;
        return false;
    }
    
    /**
     * update
     * @param string $table A name of table to insert into
     * @param string $data An associative array
     * @param string $where the WHERE query part
     */
    public function Update($table, $data, $where, $strip=true,$strip_allow=null)
    {
        ksort($data);
        
        $fieldDetails = NULL;
        foreach($data as $key=> $value) {
            $fieldDetails .= "`$key`=:$key,";
        }
        $fieldDetails = rtrim($fieldDetails, ',');
        
        $sth = $this->prepare("UPDATE $table SET $fieldDetails WHERE $where");
        
        foreach ($data as $key => $value) {
            if($strip==true){
                $sth->bindValue(":$key", strip_tags($value,$strip_allow));
            }else{
                $sth->bindValue(":$key", $value);
            }
            
        }
        
        $sth->execute();
        if($sth->rowCount()>0)
            return true;
        return false;
    }
    
    /**
     * delete
     * 
     * @param string $table
     * @param string $where
     * @param integer $limit
     * @return integer Affected Rows
     */
    public function Delete($table, $where, $limit = 1)
    {
        return $this->exec("DELETE FROM $table WHERE $where LIMIT $limit");
    }
    
    public function Query($sql)
    {
        $statement = $this->prepare($sql);
        return $statement->execute();
    }
    
}
